<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('subsredm_order_export_filter.html');

$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$picker = '<label><input type="radio" name="transactionType" value="1">Subscribe</label> &nbsp;<label><input type="radio" name="transactionType" value="2">Redemption</label>';
$btnView = '<input type="submit" name="btnExport" value="Export">';

if ($_POST['btnExport']=='Export'){
  	$transaction_date =  trim(htmlentities($_POST['transactionDate']));
	  if (empty($transaction_date))
		  $transaction_date = date("Y-m-d");

    $transaction_type = trim(htmlentities($_POST['transactionType']));

    header("location:subsredm_order_export.php?transaction_date=$transaction_date&transaction_type=$transaction_type");
    exit(0);
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','picker',$picker);
$tmpl->addVar('page','view', $btnView);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>