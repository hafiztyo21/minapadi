<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = date('Y-m-d');
$filename = $transaction_date."_fund_code.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

//$add_date = $_GET['transactionDate'];


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT fund_code, fund_name, fund_ccy, im_name, cb_code FROM tbl_kr_allocation WHERE is_deleted='0' ";
$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

//-------------STATIC FIELD-----------
$saCode = 'MU002';
//------------------------------------

$str = "Fund Code|Fund Name|Fund CCY|IM Name|CB Code|CB Name";
$str .= "|SA 01|SA 02|SA 03|SA 04|SA 05|SA 06|SA 07|SA 08|SA 09|SA 10|SA 11|SA 12|SA 13|SA 14|SA 15|SA 16|SA 17|SA 18|SA 19|SA 20\r\n";

fwrite($output, $str);

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $str = "";
    $arr = array();
    $var = $rows[$i];
    
    $idx = 0;
    $arr[$idx] = $var['fund_code'];                                   $idx++;     //0. fund code
    $arr[$idx] = $var['fund_name'];                                   $idx++;     //1. fund name
    $arr[$idx] = $var['fund_ccy'];                              $idx++;     //2. fund short name 
    $arr[$idx] = $var['im_name'];                                    $idx++;     //3. customer first name
    $arr[$idx] = $var['cb_code'];                                 $idx++;     //4. customer middle name

    $cb_name = '';
    if(strtoupper($var['cb_code']) == 'BII01')
        $cb_name = 'BANK INTERNATIONAL INDONESIA, PT';
    else if(strtoupper($var['cb_code']) == 'BMAN1')
        $cb_name = 'BANK MANDIRI, PT - CUSTODY';

    $arr[$idx] = $cb_name;                                   $idx++;     //5. CB Name ??

    $query = "SELECT sa_code FROM tbl_kr_allocation_sa WHERE fund_code = '".$var['fund_code']."'";
    $rows2 = $data->get_rows2($query);
    for($j = 0; $j<20; $j++){
        if(count($rows2) <= $j){
            $arr[$idx] = '';    $idx++;
        }else{
            $arr[$idx] = $rows2[$j]['sa_code'];    $idx++;
        }
    }
    

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
}

fclose($output);

?>
