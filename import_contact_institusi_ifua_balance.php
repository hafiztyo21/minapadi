<?php
session_start();
include 'Classes/excel_reader2.php';
//include 'Classes/PHPExcel/Writer/Excel2007.php';
//include 'Classes/PHPExcel/IOFactory.php';
//include "excel_reader2.php";
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$datadb = new globalFunction();
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('import_contact_institusi_ifua.html');
$tablename = 'tbl_kr_cus_institusi';
$tablename_detail = 'tbl_kr_cus_institusi_ifua';


if($_POST[Submit]) {
    set_time_limit(0);
    $myfile = fopen($_FILES['uploadedfile']['tmp_name'], "r") or die("Unable to open file!");
    // Output one line until end-of-file
    $sukses = 0;
    $gagal = 0;
    

    $errorResult = "";

    while(!feof($myfile)) {
        //echo fgets($myfile) . "<br>";
        $line = fgets($myfile);        
        if(strpos(strtolower($line), 'Booking Date') !== false){

        }else{
            $param = explode("|",$line);
            if(count($param) == 1) continue;

            $tradedate = substr($param[0],0,4)."-".substr($param[0],4,2)."-".substr($param[0],6,2);

            $ifuano = $param[1];
            $ifuaname = $param[2];
            $sid = $param[3];
            $fundcode = $param[4];
            $fundname = $param[5];
            $unitbalance = $param[12];
            $amountbalance = $param[13];

            $cek_data=$datadb->get_value("select count(balance_id) from tbl_kr_cus_institusi_ifua_balance where ifua_code='".$ifuano."' AND trade_date = '".$tradedate."'");
            if ($cek_data > 0){
                $query = "UPDATE tbl_kr_cus_institusi_ifua_balance SET unit_balance = '".$unitbalance."', amount_balance='".$amountbalance."' WHERE ifua_code='".$ifuano."' AND trade_date = '".$tradedate."'";
                if ($datadb->inpQueryReturnBool($query))
                {	//echo "<script>alert('SUKSES');</script>";	
                    $sukses++;
                }
                else
                {	//echo "<script>alert('GAGAL');</script>";
                    $errorResult .= "Ifua Balance '$ifuano' gagal disimpan ke dalam Database (".$datadb->queryError().")<br/>";
                    $gagal++;
                }
            }else{

                $query = "INSERT INTO tbl_kr_cus_institusi_ifua_balance (ifua_code, cus_ins_sid, unit_balance, amount_balance, trade_date, fund_code, fund_name)
                VALUES ('".$ifuano."','".$sid."','".$unitbalance."','".$amountbalance."', '".$tradedate."', '".$fundcode."', '".$fundname."')";  
                if ($datadb->inpQueryReturnBool($query))
                {	//echo "<script>alert('SUKSES');</script>";	
                    $sukses++;
                }
                else
                {	//echo "<script>alert('GAGAL');</script>";
                    $errorResult .= "Ifua Balance '$ifuano' gagal disimpan ke dalam Database (".$datadb->queryError().")<br/>";
                    $gagal++;
                }
            }

        }
        
        

    }
    fclose($myfile);
        
    // tampilan status sukses dan gagal
    echo "<h3>Proses import data selesai.</h3>";
    echo "<p>Jumlah data yang sukses diimport : ".$suksesInsertIfua."<br>";
    echo "Jumlah data yang gagal diimport : ".$gagalInsertIfua."</p>";
    if($errorResult != "")
	    echo "Error Log : <br/>".$errorResult;
	
}



$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->addVar('page', 'tittle',$tittle);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>