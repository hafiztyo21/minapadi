<?php
session_start();
ob_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'trading_customer.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
 header("Content-type: application/vnd.ms-excel");
 header("Content-Disposition: attachment; filename=Data_nasabah_personal.xls");
 header("Content-Transfer-Encoding: binary");    
$data = new trading_customer;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('export_contact.html');





####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='cus_name';//default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

############tambahan tanggal 13 okt 2010

if ($_SESSION['pajak'] =='P'){
	$filter_pajak = " and tbl_kr_employee.tax_status='".$_SESSION['pajak']."'";
}
$cus_name = trim($_POST['cus_name']);
$cus_city = trim($_POST['cus_city']);
$txt_agama = trim($_POST['txt_agama']);
$txt_bulan = trim($_POST['txt_bulan']);
$txt_tanggal = trim($_POST['txt_tanggal']);
 $date = $data->get_value("SELECT CURDATE()");
if ($_POST['btn_search'] ){
	if($_POST['cus_name']==''){
		$filter_cus_name = "";
	}else{
		$filter_cus_name = " and cus_id ='$cus_name'  ";
	}
	if($_POST['txt_agama']==''){
		$filter_country = "";
	}else{
		$filter_country = " and cus_agama ='$txt_agama'  ";
}
	if($_POST['cus_city']==''){
		$filter_city = "";
	}else{
		$filter_city = " and upper(cus_city) like upper('%".$cus_city."%')  ";
	}
	if($_POST['txt_tanggal']== $date){
		$filter_bulan = "";
	}else{
		$filter_bulan= " and cus_tgl_lahir ='".$txt_tanggal."'";
	}

	$sql  = "SELECT tbl_kr_cus_sup. * , DATE_FORMAT(  `cus_tgl_lahir` ,  '%d-%M-%Y' ) AS vcus_tgl_lahir
FROM tbl_kr_cus_sup
			where cus_actived = 1
					$filter_cus_name
					$filter_city
					$filter_country
					$filter_bulan
			order by $order_by $sort_order";
//print ($sql);
	$_SESSION['sql']=$sql;
}else if (($_SESSION['sql']) and (count($_GET)>1)){
    $sql = $_SESSION['sql'];
}else{
	$_SESSION['sql']='';
	$sql = "SELECT tbl_kr_cus_sup. * , DATE_FORMAT(  `cus_tgl_lahir` ,  '%d-%M-%Y' ) AS vcus_tgl_lahir
FROM tbl_kr_cus_sup
		order by $order_by $sort_order";
//print($sql);
}
//print($sql);
//$print="<input type='button' name='btnPrint' value='Print' onclick=popup('contact_print.php?cus=".$cus_name."&city=".$cus_city."&agama=".$txt_agama."&bulan=".$txt_bulan."','PrintContact')>";
	
		$tmpl->addVar('page','print',$print);
$arrFields = array(
		'cus_name'=>'NAME',
		'cus_main_contact_person'=>'MAIN CONTACT PERSON'
);
	
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridContactImport($sql,'cus_no_identity','cus_id',$data->ResultsPerPage,$pg,'view','contact_main.php','tambah',$link,'edit',$link,'delete',$link);

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();
$page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
$result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";
if($InfoArray["CURRENT_PAGE"]!= 1) {
	$paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
}else{
	$paging_no = "<img src='image/ar_left.png' border='0' /> ";
}
if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
}else{
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
}
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
		$paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }


###############################################################################################
$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);

$tmpl->addVar('page','txt_name',$data->cb_client_contract('cus_name',$_POST[cus_name]));
$tmpl->addVar('page','txt_agama',$data->cb_religion('txt_agama',$_POST[txt_agama]));
$tmpl->addVar('page','city',"<input type='text' name='cus_city' value='".$_POST[cus_city]."'");
//$tmpl->addVar('page','txt_bulan',$data->cb_month('txt_bulan',$_POST[bulan]));
$tmpl->addVar('page', 'date',$data->datePicker('txt_tanggal',$_POST['txt_tanggal']));

$tmpl->addRows('loopData',$DG);


$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

$tmpl->addVar('page', 'print',$print);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>
