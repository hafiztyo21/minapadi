<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('liquidation_add.html');

$id = '0';
$allocation = '';
$description = '';
$bank = '';
$up = '';
$cd_client = '';
$description1 = '';
$fund_code = '';
$fund_name = '';
$fund_shortname = '';
$im_code = '';
$im_name = '';
$fund_type = 'MM';
$fund_sub_type = 'MM';
$fund_ccy = 'IDR';
$sharia_compliance = '';
$ojk_effective_statement_letter_no = '';
$ojk_effective_statement_letter_date = date('Y-m-d');
$launching_date = ''; //date('Y-m-d');
$offering_period_begin = ''; //date('Y-m-d');
$offering_period_end = ''; //date('Y-m-d');
$maturity_year = ''; //date('Y')."";
$liquidation_date = ''; //date('Y-m-d');
$fiscal_year = '';
$initial_nav_per_unit = '';
$distribution_income_frequency = '';
$distribution_income_frequency_other = '';
$valuation_frequency = '1';
$valuation_frequency_other = '';
$number_of_decimals_in_funds_nav = '0';
$number_of_decimals_in_funds_unit = '0';
$rounding_method_for_unit = '1';
$isin_code = '';
$cb_code = '';
$fund_operational_ac_name = '';
$fund_operational_ac_no = '';
$fund_subscription_ac_name = '';
$fund_subscription_ac_no = '';
$minimum_nav_value = '0';
$minimum_initial_subscription_amount = '0';
$minimum_subsequent_subscription_amount = '0';
$minimum_redemption_amount = '0';
$maximum_total_redemption_per_day = '0';
$maximum_redemption_payment_date = '7';
$expected_redemption_payment_date = '7';
$max_equity = '0';
$max_fixed_income = '0';
$max_money = '0';
$min_equity = '0';
$min_fixed_income = '0';
$min_money = '0';
$cbest_securities_ac_no = '';
$cbest_cash_ac_no = '';
$availability_for_switching = '';
$sid = '';
$npwp_no = '';
$npwp_registration_date = '';
$cfi_category = 'E';
$cfi_group = 'U';
$cfi_1_attribute = 'C';
$cfi_2_attribute = 'I';
$cfi_3_attribute = 'R';
$cfi_4_attribute = 'R';
$correspondence_address1 = '';
$correspondence_address2 = '';
$correspondence_address3 = '';
$correspondence_postal_code = '';
$correspondence_city = '';
$correspondence_city_other = '';
$correspondence_state = '';
$correspondence_state_other = '';
$correspondence_country = '';
$billing_address1 = '';
$billing_address2 = '';
$billing_address3 = '';
$billing_postal_code = '';
$billing_city = '';
$billing_city_other = '';
$billing_state = '';
$billing_state_other = '';
$billing_country = '';
$npwp_address1 = '';
$npwp_address2 = '';
$npwp_address3 = '';
$npwp_postal_code = '';
$npwp_city = '';
$npwp_city_other = '';
$npwp_state = '';
$npwp_state_other = '';
$npwp_country = '';
$add_date = date('Y-m-d');

$errorArr = array();
for($i=0;$i<85;$i++)
	$errorArr[$i] = '';
$otherError='';

if($_GET['edit']==1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_allocation WHERE pk_id = $id";
    $result = $data->get_row($query);
	$allocation = $result['allocation'];
	$description = $result['description'];
	$bank = $result['bank'];
	$up = $result['up'];
	$cd_client = $result['cd_client'];
	$description1 = $result['description1'];
	$fund_code = $result['fund_code'];
	$fund_name = $result['fund_name'];
	$fund_shortname = $result['fund_shortname'];
	$im_code = $result['im_code'];
	$im_name = $result['im_name'];
	$fund_type = $result['fund_type'];
	$fund_sub_type = $result['fund_sub_type'];
	$fund_ccy = $result['fund_ccy'];
	$sharia_compliance = $result['sharia_compliance'];
	$ojk_effective_statement_letter_no = $result['ojk_effective_statement_letter_no'];
	$ojk_effective_statement_letter_date = $result['ojk_effective_statement_letter_date'];
	$launching_date = $result['launching_date']; //date('Y-m-d');
	$offering_period_begin = $result['offering_period_begin'];//date('Y-m-d');
	$offering_period_end = $result['offering_period_end']; //date('Y-m-d');
	$maturity_year = $result['maturity_year']; //date('Y')."";
	$liquidation_date = $result['liquidation_date'];//date('Y-m-d');
	$fiscal_year = $result['fiscal_year'];
	$initial_nav_per_unit = $result['initial_nav_per_unit'];
	$distribution_income_frequency = $result['distribution_income_frequency'];
	$distribution_income_frequency_other = $result['distribution_income_frequency_other'];
	$valuation_frequency = $result['valuation_frequency'];
	$valuation_frequency_other = $result['valuation_frequency_other'];
	$number_of_decimals_in_funds_nav = $result['number_of_decimals_in_funds_nav'];
	$number_of_decimals_in_funds_unit = $result['number_of_decimals_in_funds_unit'];
	$rounding_method_for_unit = $result['rounding_method_for_unit'];
	$isin_code = $result['isin_code'];
	$cb_code = $result['cb_code'];
	$fund_operational_ac_name = $result['fund_operational_ac_name'];
	$fund_operational_ac_no = $result['fund_operational_ac_no'];
	$fund_subscription_ac_name = $result['fund_subscription_ac_name'];
	$fund_subscription_ac_no = $result['fund_subscription_ac_no'];
	$minimum_nav_value = $result['minimum_nav_value'];
	$minimum_initial_subscription_amount = $result['minimum_initial_subscription_amount'];
	$minimum_subsequent__subscription_amount = $result['minimum_subsequent_subscription_amount'];
	$minimum_redemption_amount = $result['minimum_redemption_amount'];
	$maximum_total_redemption_per_day = $result['maximum_total_redemption_per_day'];
	$maximum_redemption_payment_date = $result['maximum_redemption_payment_date'];
	$expected_redemption_payment_date = $result['expected_redemption_payment_date'];
	$max_equity = $result['max_equity'];
	$max_fixed_income = $result['max_fixed_income'];
	$max_money = $result['max_money'];
	$min_equity = $result['min_equity'];
	$min_fixed_income = $result['min_fixed_income'];
	$min_money = $result['min_money'];
	$cbest_securities_ac_no = $result['cbest_securities_ac_no'];
	$cbest_cash_ac_no = $result['cbest_cash_ac_no'];
	$availability_for_switching = $result['availability_for_switching'];
	$sid = $result['sid'];
	$npwp_no = $result['npwp_no'];
	$npwp_registration_date = $result['npwp_registration_date'];
	$cfi_category = $result['cfi_category'];
	$cfi_group = $result['cfi_group'];
	$cfi_1_attribute = $result['cfi_1_attribute'];
	$cfi_2_attribute = $result['cfi_2_attribute'];
	$cfi_3_attribute = $result['cfi_3_attribute'];
	$cfi_4_attribute = $result['cfi_4_attribute'];
	$correspondence_address1 = $result['correspondence_address1'];
	$correspondence_address2 = $result['correspondence_address2'];
	$correspondence_address3 = $result['correspondence_address3'];
	$correspondence_postal_code = $result['correspondence_postal_code'];
	$correspondence_city = $result['correspondence_city'];
	$correspondence_city_other = $result['correspondence_city_other'];
	$correspondence_state = $result['correspondence_state'];
	$correspondence_state_other = $result['correspondence_state_other'];
	$correspondence_country = $result['correspondence_country'];
	$billing_address1 = $result['billing_address1'];
	$billing_address2 = $result['billing_address2'];
	$billing_address3 = $result['billing_address3'];
	$billing_postal_code = $result['billing_postal_code'];
	$billing_city = $result['billing_city'];
	$billing_city_other = $result['billing_city_other'];
	$billing_state = $result['billing_state'];
	$billing_state_other = $result['billing_state_other'];
	$billing_country = $result['billing_country'];
	$npwp_address1 = $result['npwp_address1'];
	$npwp_address2 = $result['npwp_address2'];
	$npwp_address3 = $result['npwp_address3'];
	$npwp_postal_code = $result['npwp_postal_code'];
	$npwp_city = $result['npwp_city'];
	$npwp_city_other = $result['npwp_city_other'];
	$npwp_state = $result['npwp_state'];
	$npwp_state_other = $result['npwp_state_other'];
	$npwp_country = $result['npwp_country'];
	$add_date = $result['add_date'];
}


if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
		$id = trim(htmlentities($_POST['inputId']));
		$allocation = trim(htmlentities($_POST['allocation']));
		$description = trim(htmlentities($_POST['description']));
		$bank = trim(htmlentities($_POST['bank']));
		$up = trim(htmlentities($_POST['up']));
		$cd_client = trim(htmlentities($_POST['cd_client']));
		$description1 = trim(htmlentities($_POST['description1']));
		$fund_code = trim(htmlentities($_POST['fund_code']));
		$fund_name = trim(htmlentities($_POST['fund_name']));
		$fund_shortname = trim(htmlentities($_POST['fund_shortname']));
		$im_code = trim(htmlentities($_POST['im_code']));
		$im_name = trim(htmlentities($_POST['im_name']));
		$fund_type = trim(htmlentities($_POST['fund_type']));
		$fund_sub_type = trim(htmlentities($_POST['fund_sub_type']));
		$fund_ccy = trim(htmlentities($_POST['fund_ccy']));
		$sharia_compliance = trim(htmlentities($_POST['sharia_compliance']));
		$ojk_effective_statement_letter_no = trim(htmlentities($_POST['ojk_effective_statement_letter_no']));
		$ojk_effective_statement_letter_date = trim(htmlentities($_POST['ojk_effective_statement_letter_date']));
		$launching_date = trim(htmlentities($_POST['launching_date']));
		$offering_period_begin = trim(htmlentities($_POST['offering_period_begin']));
		$offering_period_end = trim(htmlentities($_POST['offering_period_end']));
		$maturity_year = trim(htmlentities($_POST['maturity_year']));
		$liquidation_date = trim(htmlentities($_POST['liquidation_date']));
		$fiscal_year = trim(htmlentities($_POST['fiscal_year']));
		$initial_nav_per_unit = trim(htmlentities($_POST['initial_nav_per_unit']));
		$distribution_income_frequency = trim(htmlentities($_POST['distribution_income_frequency']));
		$distribution_income_frequency_other = trim(htmlentities($_POST['distribution_income_frequency_other']));
		$valuation_frequency = trim(htmlentities($_POST['valuation_frequency']));
		$valuation_frequency_other = trim(htmlentities($_POST['valuation_frequency_other']));
		$number_of_decimals_in_funds_nav = trim(htmlentities($_POST['number_of_decimals_in_funds_nav']));
		$number_of_decimals_in_funds_unit = trim(htmlentities($_POST['number_of_decimals_in_funds_unit']));
		$rounding_method_for_unit = trim(htmlentities($_POST['rounding_method_for_unit']));
		$isin_code = trim(htmlentities($_POST['isin_code']));
		$cb_code = trim(htmlentities($_POST['cb_code']));
		$fund_operational_ac_name = trim(htmlentities($_POST['fund_operational_ac_name']));
		$fund_operational_ac_no = trim(htmlentities($_POST['fund_operational_ac_no']));
		$fund_subscription_ac_name = trim(htmlentities($_POST['fund_subscription_ac_name']));
		$fund_subscription_ac_no = trim(htmlentities($_POST['fund_subscription_ac_no']));
		$minimum_nav_value = trim(htmlentities($_POST['minimum_nav_value']));
		$minimum_initial_subscription_amount = trim(htmlentities($_POST['minimum_initial_subscription_amount']));
		$minimum_subsequent_subscription_amount = trim(htmlentities($_POST['minimum_subsequent_subscription_amount']));
		$minimum_redemption_amount = trim(htmlentities($_POST['minimum_redemption_amount']));
		$maximum_total_redemption_per_day = trim(htmlentities($_POST['maximum_total_redemption_per_day']));
		$maximum_redemption_payment_date = trim(htmlentities($_POST['maximum_redemption_payment_date']));
		$expected_redemption_payment_date = trim(htmlentities($_POST['expected_redemption_payment_date']));
		$max_equity = trim(htmlentities($_POST['max_equity']));
		$max_fixed_income = trim(htmlentities($_POST['max_fixed_income']));
		$max_money = trim(htmlentities($_POST['max_money']));
		$min_equity = trim(htmlentities($_POST['min_equity']));
		$min_fixed_income = trim(htmlentities($_POST['min_fixed_income']));
		$min_money = trim(htmlentities($_POST['min_money']));
		$cbest_securities_ac_no = trim(htmlentities($_POST['cbest_securities_ac_no']));
		$cbest_cash_ac_no = trim(htmlentities($_POST['cbest_cash_ac_no']));
		$availability_for_switching = trim(htmlentities($_POST['availability_for_switching']));
		$sid = trim(htmlentities($_POST['sid']));
		$npwp_no = trim(htmlentities($_POST['npwp_no']));
		$npwp_registration_date = trim(htmlentities($_POST['npwp_registration_date']));
		$cfi_category = trim(htmlentities($_POST['cfi_category']));
		$cfi_group = trim(htmlentities($_POST['cfi_group']));
		$cfi_1_attribute = trim(htmlentities($_POST['cfi_1_attribute']));
		$cfi_2_attribute = trim(htmlentities($_POST['cfi_2_attribute']));
		$cfi_3_attribute = trim(htmlentities($_POST['cfi_3_attribute']));
		$cfi_4_attribute = trim(htmlentities($_POST['cfi_4_attribute']));
		$correspondence_address1 = trim(htmlentities($_POST['correspondence_address1']));
		$correspondence_address2 = trim(htmlentities($_POST['correspondence_address2']));
		$correspondence_address3 = trim(htmlentities($_POST['correspondence_address3']));
		$correspondence_postal_code = trim(htmlentities($_POST['correspondence_postal_code']));
		$correspondence_city = trim(htmlentities($_POST['correspondence_city']));
		$correspondence_city_other = trim(htmlentities($_POST['correspondence_city_other']));
		$correspondence_state = trim(htmlentities($_POST['correspondence_state']));
		$correspondence_state_other = trim(htmlentities($_POST['correspondence_state_other']));
		$correspondence_country = trim(htmlentities($_POST['correspondence_country']));
		$billing_address1 = trim(htmlentities($_POST['billing_address1']));
		$billing_address2 = trim(htmlentities($_POST['billing_address2']));
		$billing_address3 = trim(htmlentities($_POST['billing_address3']));
		$billing_postal_code = trim(htmlentities($_POST['billing_postal_code']));
		$billing_city = trim(htmlentities($_POST['billing_city']));
		$billing_city_other = trim(htmlentities($_POST['billing_city_other']));
		$billing_state = trim(htmlentities($_POST['billing_state']));
		$billing_state_other = trim(htmlentities($_POST['billing_state_other']));
		$billing_country = trim(htmlentities($_POST['billing_country']));
		$npwp_address1 = trim(htmlentities($_POST['npwp_address1']));
		$npwp_address2 = trim(htmlentities($_POST['npwp_address2']));
		$npwp_address3 = trim(htmlentities($_POST['npwp_address3']));
		$npwp_postal_code = trim(htmlentities($_POST['npwp_postal_code']));
		$npwp_city = trim(htmlentities($_POST['npwp_city']));
		$npwp_city_other = trim(htmlentities($_POST['npwp_city_other']));
		$npwp_state = trim(htmlentities($_POST['npwp_state']));
		$npwp_state_other = trim(htmlentities($_POST['npwp_state_other']));
		$npwp_country = trim(htmlentities($_POST['npwp_country']));
		$add_date = trim(htmlentities($_POST['add_date']));

		$gotError = false;
		if($allocation==''){
			$errorArr[0] = "Allocation must be filled";
			$gotError = true;
		}
		if($fund_code==''){
			$errorArr[5] = "Fund Code must be filled";
			$gotError = true;
		}
		if($fund_name==''){
			$errorArr[6] = "Fund Name must be filled";
			$gotError = true;
		}
		if($fund_shortname==''){
			$errorArr[7] = "Fund Short Name must be filled";
			$gotError = true;
		}
		if($im_code==''){
			$errorArr[8] = "IM Code must be filled";
			$gotError = true;
		}
		if($im_name==''){
			$errorArr[9] = "IM Name must be filled";
			$gotError = true;
		}
		if($sharia_compliance==''){
			$errorArr[13] = "Sharia Compliance must be filled";
			$gotError = true;
		}
		if($fund_type=='MM' || $fund_type=='FI' || $fund_type=='EQ'
		|| $fund_type=='MX' || $fund_type=='IF' || $fund_type=='PF'
		|| $fund_type=='GR' || $fund_type=='PE' || $fund_type=='ET'
		|| $fund_type=='RE' || $fund_type=='AB'){
			if($ojk_effective_statement_letter_no == ''){
				$errorArr[14] = "OJK Effective Statement Letter No must be filled";
				$gotError = true;
			}
			if($initial_nav_per_unit == ''){
				$errorArr[22] = "Initial NAV per Unit must be filled";
				$gotError = true;
			}
			if($distribution_income_frequency == 7 && $distribution_income_frequency_other == ''){
				$errorArr[23] = "Other must be filled";
				$gotError = true;
			}
			if($valuation_frequency == 7 && $valuation_frequency_other == ''){
				$errorArr[24] = "Other must be filled";
				$gotError = true;
			}
			if($number_of_decimals_in_funds_nav == ''){
				$errorArr[25] = "Number of Decimals in Funds NAV must be filled";
				$gotError = true;
			}
			if($number_of_decimals_in_funds_unit == ''){
				$errorArr[26] = "Number of Decimals in Funds Unit must be filled";
				$gotError = true;
			}
			if($rounding_method_for_unit == ''){
				$errorArr[27] = "Rounding Method for Unit must be filled";
				$gotError = true;
			}

			if($fund_operational_ac_name == ''){
				$errorArr[30] = "Fund Operational A/C Name must be filled";
				$gotError = true;
			}

			if($fund_operational_ac_no == ''){
				$errorArr[31] = "Fund Operational A/C No must be filled";
				$gotError = true;
			}
			if($fund_subscription_ac_name == ''){
				$errorArr[32] = "Fund Subscription A/C Name must be filled";
				$gotError = true;
			}

			if($fund_subscription_ac_no == ''){
				$errorArr[33] = "Fund Subscription A/C No must be filled";
				$gotError = true;
			}
			if($maximum_redemption_payment_date == ''){
				$errorArr[39] = "Maximum Redemption Payment Date must be filled";
				$gotError = true;
			}
			if($expected_redemption_payment_date == ''){
				$errorArr[40] = "Expected Redemption Payment Date must be filled";
				$gotError = true;
			}
			
		}
		if($cb_code == ''){
			$errorArr[29] = "CB Code must be filled";
			$gotError = true;
		}
		if($max_equity == ''){
			$errorArr[41] = "Max Equity must be filled";
			$gotError = true;
		}
		if($max_fixed_income == ''){
			$errorArr[42] = "Max Fixed Income must be filled";
			$gotError = true;
		}
		if($max_money == ''){
			$errorArr[43] = "Max Money must be filled";
			$gotError = true;
		}
		if($min_equity == ''){
			$errorArr[44] = "Min Equity must be filled";
			$gotError = true;
		}
		if($min_fixed_income == ''){
			$errorArr[45] = "Min Fixed Income must be filled";
			$gotError = true;
		}
		if($min_money == ''){
			$errorArr[46] = "Min Money must be filled";
			$gotError = true;
		}
		if($cbest_securities_ac_no == ''){
			$errorArr[47] = "C-BEST Security A/C No must be filled";
			$gotError = true;
		}
		if($cbest_cash_ac_no == ''){
			$errorArr[48] = "C-BEST Cash A/C No must be filled";
			$gotError = true;
		}
		if($correspondence_address1 == ''){
			$errorArr[57] = "Correspondence Address Line 1 must be filled";
			$gotError = true;
		}
		if($correspondence_country == ''){
			$errorArr[65] = "Correspondence Country must be filled";
			$gotError = true;
		}
		if($billing_address1 == ''){
			$errorArr[66] = "Billing Address Line 1 must be filled";
			$gotError = true;
		}
		
		if($billing_country == ''){
			$errorArr[74] = "Billing Country must be filled";
			$gotError = true;
		}
		
		if($npwp_address1 == ''){
			$errorArr[75] = "NPWP Address Line 1 must be filled";
			$gotError = true;
		}
		if($npwp_country == ''){
			$errorArr[83] = "NPWP Country must be filled";
			$gotError = true;
		}
		
		
        
		if (!$gotError){
			if($id == 0){
				$query = "INSERT INTO tbl_kr_allocation (
					allocation,
					description,
					bank,
					up,
					cd_client,
					description1,
					fund_code,
					fund_name,
					fund_shortname,
					im_code ,
					im_name ,
					fund_type ,
					fund_sub_type ,
					fund_ccy ,
					sharia_compliance ,
					ojk_effective_statement_letter_no ,
					ojk_effective_statement_letter_date ,
					launching_date ,
					offering_period_begin ,
					offering_period_end ,
					maturity_year ,
					liquidation_date ,
					fiscal_year,
					initial_nav_per_unit ,
					distribution_income_frequency,
					distribution_income_frequency_other ,
					valuation_frequency ,
					valuation_frequency_other ,
					number_of_decimals_in_funds_nav ,
					number_of_decimals_in_funds_unit ,
					rounding_method_for_unit ,
					isin_code ,
					cb_code ,
					fund_operational_ac_name ,
					fund_operational_ac_no ,
					fund_subscription_ac_name ,
					fund_subscription_ac_no ,
					minimum_nav_value ,
					minimum_initial_subscription_amount,
					minimum_subsequent_subscription_amount,
					minimum_redemption_amount ,
					maximum_total_redemption_per_day ,
					maximum_redemption_payment_date,
					expected_redemption_payment_date ,
					max_equity,
					max_fixed_income ,
					max_money ,
					min_equity ,
					min_fixed_income ,
					min_money ,
					cbest_securities_ac_no ,
					cbest_cash_ac_no ,
					availability_for_switching ,
					sid ,
					npwp_no ,
					npwp_registration_date ,
					cfi_category ,
					cfi_group ,
					cfi_1_attribute ,
					cfi_2_attribute ,
					cfi_3_attribute ,
					cfi_4_attribute ,
					correspondence_address1 ,
					correspondence_address2 ,
					correspondence_address3 ,
					correspondence_postal_code ,
					correspondence_city ,
					correspondence_city_other ,
					correspondence_state ,
					correspondence_state_other ,
					correspondence_country ,
					billing_address1 ,
					billing_address2,
					billing_address3 ,
					billing_postal_code ,
					billing_city ,
					billing_city_other ,
					billing_state ,
					billing_state_other ,
					billing_country ,
					npwp_address1 ,
					npwp_address2 ,
					npwp_address3 ,
					npwp_postal_code ,
					npwp_city ,
					npwp_city_other ,
					npwp_state ,
					npwp_state_other ,
					npwp_country,
					is_deleted,
					add_date
				)VALUES(
					'$allocation',
					'$description',
					'$bank',
					'$up',
					'$cd_client',
					'$description1',
					'$fund_code',
					'$fund_name',
					'$fund_shortname',
					'$im_code' ,
					'$im_name' ,
					'$fund_type' ,
					'$fund_sub_type' ,
					'$fund_ccy' ,
					'$sharia_compliance' ,
					'$ojk_effective_statement_letter_no' ,
					'$ojk_effective_statement_letter_date' ,
					'$launching_date' ,
					'$offering_period_begin' ,
					'$offering_period_end' ,
					'$maturity_year' ,
					'$liquidation_date' ,
					'$fiscal_year',
					'$initial_nav_per_unit' ,
					'$distribution_income_frequency',
					'$distribution_income_frequency_other' ,
					'$valuation_frequency' ,
					'$valuation_frequency_other' ,
					'$number_of_decimals_in_funds_nav' ,
					'$number_of_decimals_in_funds_unit' ,
					'$rounding_method_for_unit' ,
					'$isin_code' ,
					'$cb_code' ,
					'$fund_operational_ac_name' ,
					'$fund_operational_ac_no' ,
					'$fund_subscription_ac_name' ,
					'$fund_subscription_ac_no' ,
					'$minimum_nav_value' ,
					'$minimum_initial_subscription_amount',
					'$minimum_subsequent_subscription_amount',
					'$minimum_redemption_amount' ,
					'$maximum_total_redemption_per_day' ,
					'$maximum_redemption_payment_date',
					'$expected_redemption_payment_date' ,
					'$max_equity',
					'$max_fixed_income' ,
					'$max_money' ,
					'$min_equity' ,
					'$min_fixed_income' ,
					'$min_money' ,
					'$cbest_securities_ac_no' ,
					'$cbest_cash_ac_no' ,
					'$availability_for_switching' ,
					'$sid' ,
					'$npwp_no' ,
					'$npwp_registration_date' ,
					'$cfi_category' ,
					'$cfi_group' ,
					'$cfi_1_attribute' ,
					'$cfi_2_attribute' ,
					'$cfi_3_attribute' ,
					'$cfi_4_attribute' ,
					'$correspondence_address1' ,
					'$correspondence_address2' ,
					'$correspondence_address3' ,
					'$correspondence_postal_code' ,
					'$correspondence_city' ,
					'$correspondence_city_other' ,
					'$correspondence_state' ,
					'$correspondence_state_other' ,
					'$correspondence_country' ,
					'$billing_address1' ,
					'$billing_address2',
					'$billing_address3' ,
					'$billing_postal_code' ,
					'$billing_city' ,
					'$billing_city_other' ,
					'$billing_state' ,
					'$billing_state_other' ,
					'$billing_country' ,
					'$npwp_address1' ,
					'$npwp_address2' ,
					'$npwp_address3' ,
					'$npwp_postal_code' ,
					'$npwp_city' ,
					'$npwp_city_other' ,
					'$npwp_state' ,
					'$npwp_state_other' ,
					'$npwp_country',
				'0','$add_date')";
			}else{
				$query = "UPDATE tbl_kr_allocation SET
					allocation='$allocation',
					description='$description',
					bank='$bank',
					up='$up',
					cd_client='$cd_client',
					description1='$fund_name',
					fund_code='$fund_code',
					fund_name='$fund_name',
					fund_shortname='$fund_shortname',
					im_code ='$im_code ',
					im_name='$im_name',
					fund_type='$fund_type',
					fund_sub_type='$fund_sub_type',
					fund_ccy='$fund_ccy',
					sharia_compliance='$sharia_compliance',
					ojk_effective_statement_letter_no='$ojk_effective_statement_letter_no',
					ojk_effective_statement_letter_date='$ojk_effective_statement_letter_date',
					launching_date='$launching_date',
					offering_period_begin='$offering_period_begin',
					offering_period_end='$offering_period_end',
					maturity_year='$maturity_year',
					liquidation_date='$liquidation_date',
					fiscal_year='$fiscal_year',
					initial_nav_per_unit='$initial_nav_per_unit',
					distribution_income_frequency='$distribution_income_frequency',
					distribution_income_frequency_other='$distribution_income_frequency_other',
					valuation_frequency='$valuation_frequency',
					valuation_frequency_other='$valuation_frequency_other',
					number_of_decimals_in_funds_nav='$number_of_decimals_in_funds_nav',
					number_of_decimals_in_funds_unit='$number_of_decimals_in_funds_unit',
					rounding_method_for_unit='$rounding_method_for_unit',
					isin_code='$isin_code',
					cb_code='$cb_code',
					fund_operational_ac_name='$fund_operational_ac_name',
					fund_operational_ac_no='$fund_operational_ac_no',
					fund_subscription_ac_name='$fund_subscription_ac_name',
					fund_subscription_ac_no='$fund_subscription_ac_no',
					minimum_nav_value='$minimum_nav_value',
					minimum_initial_subscription_amount='$minimum_initial_subscription_amount',
					minimum_subsequent_subscription_amount='$minimum_subsequent_subscription_amount',
					minimum_redemption_amount='$minimum_redemption_amount',
					maximum_total_redemption_per_day='$maximum_total_redemption_per_day',
					maximum_redemption_payment_date='$maximum_redemption_payment_date',
					expected_redemption_payment_date='$expected_redemption_payment_date',
					max_equity='$max_equity',
					max_fixed_income='$max_fixed_income',
					max_money='$max_money',
					min_equity='$min_equity',
					min_fixed_income='$min_fixed_income',
					min_money='$min_money',
					cbest_securities_ac_no='$cbest_securities_ac_no',
					cbest_cash_ac_no='$cbest_cash_ac_no',
					availability_for_switching='$availability_for_switching',
					sid='$sid',
					npwp_no='$npwp_no',
					npwp_registration_date='$npwp_registration_date',
					cfi_category='$cfi_category',
					cfi_group='$cfi_group',
					cfi_1_attribute='$cfi_1_attribute',
					cfi_2_attribute='$cfi_2_attribute',
					cfi_3_attribute='$cfi_3_attribute',
					cfi_4_attribute='$cfi_4_attribute',
					correspondence_address1='$correspondence_address1',
					correspondence_address2='$correspondence_address2',
					correspondence_address3='$correspondence_address3',
					correspondence_postal_code='$correspondence_postal_code',
					correspondence_city='$correspondence_city',
					correspondence_city_other='$correspondence_city_other',
					correspondence_state='$correspondence_state',
					correspondence_state_other='$correspondence_state_other',
					correspondence_country='$correspondence_country',
					billing_address1='$billing_address1',
					billing_address2='$billing_address2',
					billing_address3='$billing_address3',
					billing_postal_code='$billing_postal_code',
					billing_city='$billing_city',
					billing_city_other='$billing_city_other',
					billing_state='$billing_state',
					billing_state_other='$billing_state_other',
					billing_country='$billing_country',
					npwp_address1='$npwp_address1',
					npwp_address2='$npwp_address2',
					npwp_address3='$npwp_address3',
					npwp_postal_code='$npwp_postal_code',
					npwp_city='$npwp_city',
					npwp_city_other='$npwp_city_other',
					npwp_state='$npwp_state',
					npwp_state_other='$npwp_state_other',
					npwp_country='$npwp_country',
					add_date = '$add_date'
				WHERE pk_id = '$id'";
			}
			

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='product.php?view=1';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = ($id == '0' ? "ADD" : "EDIT")." - FUND INFO";
$dataRows = array (
	    'TEXT' => array(
				'Allocation <span class="redstar">*</span>',
				'Description',
				'Bank',
				'Up',
				'Cd Client',	//5
				'Fund Code <span class="redstar">*</span>',
				'Fund Name <span class="redstar">*</span>',
				'Fund Short Name <span class="redstar">*</span>',
				'IM Code <span class="redstar">*</span>',
				'IM Name <span class="redstar">*</span>',		//10
				'Fund Type <span class="redstar">*</span>',	//11
				'Fund Sub Type',
				'Fund CCY <span class="redstar">*</span>',
				'Sharia Compliance <span class="redstar">*</span>',
				'OJK Effective Statement Letter No.',
				'OJK Effective Statement Letter Date', //16
				'Launching Date',
				'Offering Periode (Begin)',
				'Offering Periode (End)',
				'Maturity Year',
				'Liquidation Date',
				'Fiscal Year',
				'Initial NAV per Unit',
				'Distribution Income Frequency',
				'Valuation Frequency',	//23
				'Number of Decimals in Funds NAV',	
				'Number of Decimals in Funds Unit',
				'Rounding Method for Unit',
				'ISIN Code',
				'CB Code <span class="redstar">*</span>',
				'Fund Operatinal A/C Name',
				'Fund Operatinal A/C No',		//30
				'Fund Subscription A/C Name <span class="redstar">*</span>',
				'Fund Subscription A/C No <span class="redstar">*</span>',
				'Minimum NAV(AUM) Value (in IDR)',
				'Minimum Initial Subscription Amount',
				'Minimum Subsequent Subscription Amount',
				'Minimum Redemption Amount',
				'Maximum Total Redemption per Day',
				'Maximum Redemption Payment Date',
				'Expected Redemption Payment Date',
				'Max Equity <span class="redstar">*</span>',	//31
				'Max Fixed Income <span class="redstar">*</span>',
				'Max Money <span class="redstar">*</span>', 
				'Min Equity <span class="redstar">*</span>',	//43
				'Min Fixed Income <span class="redstar">*</span>',
				'Min Money <span class="redstar">*</span>',	//45
				'C-BEST Securities A/C No. <span class="redstar">*</span>',
				'C-BEST Cash A/C No. <span class="redstar">*</span>',
				'Avalability for Switching',
				'SID',
				'NPWP No.',	//50
				'NPWP Registration Date',
				'CFI 1st Attribute <span class="redstar">*</span>',
				'CFI 2nd Attribute <span class="redstar">*</span>',
				'CFI 3rd Attribute <span class="redstar">*</span>',
				'CFI 4th Attribute <span class="redstar">*</span>',	//55
				'Correspondence Address Line 1 <span class="redstar">*</span>', //56
				'Correspondence Address Line 2',
				'Correspondence Address Line 3',
				'Correspondence Postal Code',
				'Correspondence City (C-BEST City Code)',
				'Correspondence City Other',	//51
				'Correspondence State/Province (C-BEST State/Province Code)',
				'Correspondence State/Province Other',
				'Correspondence Country <span class="redstar">*</span>',
				'Billing Address Line 1 <span class="redstar">*</span>',
				'Billing Address Line 2',	//56
				'Billing Address Line 3',
				'Billing Postal Code',
				'Billing City (C-BEST City Code)',
				'Billing City Other',
				'Billing State/Province (C-BEST State/Province Code)',	//61
				'Billing State/Province Other',
				'Billing Country <span class="redstar">*</span>',
				'NPWP Address Line 1 <span class="redstar">*</span>',
				'NPWP Address Line 2',
				'NPWP Address Line 3',	//66
				'NPWP Postal Code',
				'NPWP City (C-BEST City Code)',
				'NPWP City Other',
				'NPWP State/Province (C-BEST State/Province Code)',
				'NPWP State/Province Other',	//71
				'NPWP Country <span class="redstar">*</span>',
				'Add Date',
			),
	    'FIELD' => array (
			"<input type=hidden name=inputId id=inputId value='$id'>".
            "<input type=text name=allocation id=allocation value='$allocation'>",
			"<input type=text name=description id=description value='$description' size=50>",
			"<input type=text name=bank id=bank value='$bank' size=50>",
		    "<input type=text name=up id=up value='$up' size=50>",
			"<input type=text name=cd_client id=cd_client value='$cd_client' size=20>",
			"<input type=text name=fund_code id=fund_code value='$fund_code' size=20 maxlength=16>",
			"<input type=text name=fund_name id=fund_name value='$fund_name' size=50>",
			"<input type=text name=fund_shortname id=fund_shortname value='$fund_shortname' size=6 maxlength=6>",
			"<input type=text name=im_code id=im_code value='$im_code' size=6 maxlength=5>",
			"<input type=text name=im_name id=im_name value='$im_name' size=50 maxlength=100>",
			$data->cb_fundtype('fund_type', $fund_type,''),
			$data->cb_fundsubtype('fund_sub_type', $fund_sub_type,''),
			$data->cb_accountccy('fund_ccy', $fund_ccy, ''),
			$data->cb_shariacompliance('sharia_compliance', $sharia_compliance, ''),
			//"<input type=text name=sharia_compliance id=sharia_compliance value='$sharia_compliance' size=1 maxlength=1>",
			"<input type=text name=ojk_effective_statement_letter_no id=ojk_effective_statement_letter_no value='$ojk_effective_statement_letter_no' size=50 maxlength=60>",
			$data->datePicker('ojk_effective_statement_letter_date', $ojk_effective_statement_letter_date,''),
			$data->datePicker('launching_date', $launching_date,''),
			$data->datePicker('offering_period_begin', $offering_period_begin,''),
			$data->datePicker('offering_period_end', $offering_period_end,''),
			"<input type=number name=maturity_year id=maturity_year value='$maturity_year' size=5> Format : YYYY",
			$data->datePicker('liquidation_date', $liquidation_date,''),
			"<input type=number name=fiscal_year id=fiscal_year value='$fiscal_year'> Format : MMDD",
			"<input type=number name=initial_nav_per_unit id=initial_nav_per_unit value='$initial_nav_per_unit'>",
			$data->cb_distributionincomefrequency('distribution_income_frequency', $distribution_income_frequency, '')."&nbsp;&nbsp;Other : <input type=text name=distribution_income_frequency_other id=distribution_income_frequency_other value='".$distribution_income_frequency_other."'>",
			$data->cb_valuationfrequency('valuation_frequency', $valuation_frequency, '')."&nbsp;&nbsp;Other : <input type=text name=valuation_frequency_other id=valuation_frequency_other value='".$valuation_frequency_other."'>",
			"<input type=number name=number_of_decimals_in_funds_nav id=number_of_decimals_in_funds_nav value='$number_of_decimals_in_funds_nav'>",
			"<input type=number name=number_of_decimals_in_funds_unit id=number_of_decimals_in_funds_unit value='$number_of_decimals_in_funds_unit'>",
			$data->cb_roundingmethodforunit('rounding_method_for_unit', $rounding_method_for_unit, ''),
			"<input type=text name=isin_code id=isin_code value='$isin_code' size=15 maxlength=12>",
			"<input type=text name=cb_code id=cb_code value='$cb_code' size=5 maxlength=5>",
			"<input type=text name=fund_operational_ac_name id=fund_operational_ac_name value='$fund_operational_ac_name' size=50 maxlength=60>",
			"<input type=text name=fund_operational_ac_no id=fund_operational_ac_no value='$fund_operational_ac_no' size=30 maxlength=30>",
			"<input type=text name=fund_subscription_ac_name id=fund_subscription_ac_name value='$fund_subscription_ac_name' size=50 maxlength=60>",
			"<input type=text name=fund_subscription_ac_no id=fund_subscription_ac_no value='$fund_subscription_ac_no' size=30 maxlength=30>",
			"<input type=number name=minimum_nav_value id=minimum_nav_value value='$minimum_nav_value'>",
			"<input type=number name=minimum_initial_subscription_amount id=minimum_initial_subscription_amount value='$minimum_initial_subscription_amount'>",
			"<input type=number name=minimum_subsequent_subscription_amount id=minimum_subsequent_subscription_amount value='$minimum_subsequent_subscription_amount'>",
			"<input type=number name=minimum_redemption_amount id=minimum_redemption_amount value='$minimum_redemption_amount'>",
			"<input type=number name=maximum_total_redemption_per_day id=maximum_total_redemption_per_day value='$maximum_total_redemption_per_day'>",
			"<input type=number name=maximum_redemption_payment_date id=maximum_redemption_payment_date value='$maximum_redemption_payment_date'>",
			"<input type=number name=expected_redemption_payment_date id=expected_redemption_payment_date value='$expected_redemption_payment_date'>",
			"<input type=number name=max_equity id=max_equity value='$max_equity'>",
			"<input type=number name=max_fixed_income id=max_fixed_income value='$max_fixed_income'>",
			"<input type=number name=max_money id=max_money value='$max_money'>",
			"<input type=number name=min_equity id=min_equity value='$min_equity'>",
			"<input type=number name=min_fixed_income id=min_fixed_income value='$min_fixed_income'>",
			"<input type=number name=min_money id=min_money value='$min_money'>",
			"<input type=text name=cbest_securities_ac_no id=cbest_securities_ac_no value='$cbest_securities_ac_no' size=20 maxlength=16>",
			"<input type=text name=cbest_cash_ac_no id=cbest_cash_ac_no value='$cbest_cash_ac_no' size=20 maxlength=16>",
			$data->cb_availabilityforswitching('availability_for_switching', $availability_for_switching, ''),
			"<input type=text name=sid id=sid value='$sid' size=20 maxlength=15>",
			"<input type=text name=npwp_no id=npwp_no value='$npwp_no' size=20 maxlength=15>",
			$data->datePicker('npwp_registration_date', $npwp_registration_date,''),
			$data->cb_cfi1attribute('cfi_1_attribute', $cfi_1_attribute,''),
			$data->cb_cfi2attribute('cfi_2_attribute', $cfi_2_attribute,''),
			$data->cb_cfi3attribute('cfi_3_attribute', $cfi_3_attribute,''),
			$data->cb_cfi4attribute('cfi_4_attribute', $cfi_4_attribute,''),
			"<input type=text name=correspondence_address1 id=correspondence_address1 value='$correspondence_address1' size=50 maxlength=70>",
			"<input type=text name=correspondence_address2 id=correspondence_address2 value='$correspondence_address2' size=50 maxlength=70>",
			"<input type=text name=correspondence_address3 id=correspondence_address3 value='$correspondence_address3' size=50 maxlength=70>",
			"<input type=text name=correspondence_postal_code id=correspondence_postal_code value='$correspondence_postal_code' size=10 maxlength=10>",
			"<input type=text name=correspondence_city id=correspondence_city value='$correspondence_city' size=5 maxlength=4>",
			"<input type=text name=correspondence_city_other id=correspondence_city_other value='$correspondence_city_other' size=20 maxlength=35>",
			"<input type=text name=correspondence_state id=correspondence_state value='$correspondence_state' size=5 maxlength=4>",
			"<input type=text name=correspondence_state_other id=correspondence_state_other value='$correspondence_state_other' size=20 maxlength=35>",
			$data->cb_isocountry('correspondence_country', $correspondence_country,''),
			"<input type=text name=billing_address1 id=billing_address1 value='$billing_address1' size=50 maxlength=70>",
			"<input type=text name=billing_address2 id=billing_address2 value='$billing_address2' size=50 maxlength=70>",
			"<input type=text name=billing_address3 id=billing_address3 value='$billing_address3' size=50 maxlength=70>",
			"<input type=text name=billing_postal_code id=billing_postal_code value='$billing_postal_code' size=10 maxlength=10>",
			"<input type=text name=billing_city id=billing_city value='$billing_city' size=5 maxlength=4>",
			"<input type=text name=billing_city_other id=billing_city_other value='$billing_city_other' size=20 maxlength=35>",
			"<input type=text name=billing_state id=billing_state value='$billing_state' size=5 maxlength=4>",
			"<input type=text name=billing_state_other id=billing_state_other value='$billing_state_other' size=20 maxlength=35>",
			$data->cb_isocountry('billing_country', $billing_country,''),
			"<input type=text name=npwp_address1 id=npwp_address1 value='$npwp_address1' size=50 maxlength=70>",
			"<input type=text name=npwp_address2 id=npwp_address2 value='$npwp_address2' size=50 maxlength=70>",
			"<input type=text name=npwp_address3 id=npwp_address3 value='$npwp_address3' size=50 maxlength=70>",
			"<input type=text name=npwp_postal_code id=npwp_postal_code value='$npwp_postal_code' size=10 maxlength=10>",
			"<input type=text name=npwp_city id=npwp_city value='$npwp_city' size=5 maxlength=4>",
			"<input type=text name=npwp_city_other id=npwp_city_other value='$npwp_city_other' size=20 maxlength=35>",
			"<input type=text name=npwp_state id=npwp_state value='$npwp_state' size=5 maxlength=4>",
			"<input type=text name=npwp_state_other id=npwp_state_other value='$npwp_state_other' size=20 maxlength=35>",
			$data->cb_isocountry('npwp_country', $npwp_country,''),
			$data->datePicker('add_date', $add_date,''),
				
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='product.php?view=1';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>