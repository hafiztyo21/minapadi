<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('me_dealer_deal.html');

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_dealer.pk_id';
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$back ="<input type=button name=back value='Back' onclick=\"window.location='me_dealer_open.php?detail=1&id=$_GET[id]&fk=$_GET[fk]';\">";
$tmpl->addVar('page','back',$back);
$id = $_GET[id];
$tmp = $_GET[tmp];
$fk = $_GET[fk];
if ($_POST['btn_add']=='Add Deal'){
	$tmpl->setAttribute("deal","visibility","show");
}
if ($_GET['detail']=='1'){
	$sql1  = "SELECT tbl_kr_dealer.*, FORMAT((tbl_kr_dealer.lembar/100),2) AS LMBR,
	FORMAT(tbl_kr_dealer.harga,0) AS HRG, fullname, stk_cd, securitas_type, FORMAT(tbl_kr_dealer.total,0) AS TTL,
	format(tbl_kr_dealer_tmp.total,0) as TTL2,
	FORMAT((tbl_kr_dealer_tmp.lembar/100),0) AS LMBR_DEAL, RIGHT(tbl_kr_dealer.create_dt,8) AS CRT_DT FROM tbl_kr_dealer
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_dealer.me_id
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_dealer.code
	LEFT JOIN tbl_kr_securitas ON tbl_kr_securitas.securitas_id = tbl_kr_dealer.securitas_id
	LEFT JOIN tbl_kr_dealer_tmp ON tbl_kr_dealer_tmp.pk_id = tbl_kr_dealer.id_temp
	WHERE tbl_kr_dealer.id_temp = '".$tmp."' order by $order_by $sort_order";
$DG= $data->dataGridDeal($sql1,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link);
$rowa = $data->get_row($sql1);
	$sql2  = "SELECT tbl_kr_dealer_tmp.*,
	format((tbl_kr_dealer_tmp.lembar/100),2) as LMBR2, format(tbl_kr_dealer_tmp.harga,0) as HRG2,
	fullname, stk_cd, securitas_type,format(tbl_kr_dealer_tmp.total,0) as TTL2,
	(select sum(lembar)/100 from tbl_kr_dealer where id_temp = tbl_kr_dealer_tmp.pk_id) as TOTAL_LMBR,
	(select format(sum(total),0) from tbl_kr_dealer where id_temp = tbl_kr_dealer_tmp.pk_id) as TOTAL FROM tbl_kr_dealer_tmp
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_dealer_tmp.me_id
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_dealer_tmp.code
	LEFT JOIN tbl_kr_securitas ON tbl_kr_securitas.securitas_id = tbl_kr_dealer_tmp.securitas_id
	LEFT JOIN tbl_kr_dealer ON tbl_kr_dealer_tmp.pk_id = tbl_kr_dealer.id_temp
   	where tbl_kr_dealer_tmp.fk_id = '".$fk."' AND tbl_kr_dealer_tmp.pk_id='".$tmp."'";
$rows = $data->get_row($sql2);
if (!empty($rowa)){
	$total_lembar = number_format($rows['TOTAL_LMBR'],0);
	$lembar_deal = "Total&nbsp;".$total_lembar."&nbsp;/&nbsp;".$rows['LMBR2']."";
	$total_harga = "Rp.&nbsp;".$rows['TOTAL']."";
	$total_dari = "/&nbsp;Rp.&nbsp;".$rows['TTL2']."";
	$tmpl->addVar('page','lembar2',$lembar_deal);
	$tmpl->addVar('page', 'total2',$total_harga);
	$tmpl->addVar('page', 'dari',$total_dari);
}
$code = $rows['stock_name'];
$type_buy = $rows['type_buy'];
$lmbr_deal = $rows['LMBR2'];
$harga = $rows['HRG2'];
$total = $rows['TTL2'];
$securitas = $rows['securitas_type'];
$fullname = $rows['fullname'];
$text_box = "<input type=text size=10 name=txt_lembar>";
$save ="<input type=submit name=btn_save value='Save'>";
if ($rows['type_buy']=='Sell'){
	$color = "#f07477";
}else{
    $color = "#8ef660";
}
$add ="<input type=submit name=btn_add value='Add Deal'>";
$tmpl->addVar('add', 'code',$code);
$tmpl->addVar('add', 'type',$type_buy);
$tmpl->addVar('add', 'lembar_deal',$lmbr_deal);
$tmpl->addVar('add', 'harga',$harga);
$tmpl->addVar('add', 'total',$total);
$tmpl->addVar('add', 'securitas',$securitas);
$tmpl->addVar('add', 'nama',$fullname);
$tmpl->addVar('add', 'box',$text_box);
$tmpl->addVar('add', 'color',$color);
$tmpl->addVar('add', 'save',$save);
}

if ($_POST['btn_save']=='Save'){
	$id = $_GET['id'];
	$fk = $_GET['fk'];
	$rowo = $data->get_row("select allocation as A from tbl_kr_me where pk_id = '".$fk."'");
	$allocation = $rowo['A'];
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
	$txt_lembar = trim(htmlentities($_POST['txt_lembar']));
	$txt_lembar = str_replace(',','',$txt_lembar);
	if($txt_lembar==''){
			echo "<script>alert('Lembar is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
	}
		$txt_securitas = trim(htmlentities($_POST['txt_securitas']));
		$txt_levy = trim(htmlentities($_POST['txt_levy']));
		$txt_kpei = trim(htmlentities($_POST['txt_kpei']));
        $txt_pajak = trim(htmlentities($_POST['txt_pajak']));

		$txt_pajak_jual = trim(htmlentities($_POST['txt_pajak_jual']));
		$txt_pajak_komisi = trim(htmlentities($_POST['txt_pajak_komisi']));
		$txt_nilai_beli = trim(htmlentities($_POST['txt_nilai_beli']));
		$txt_nilai_jual = trim(htmlentities($_POST['txt_nilai_jual']));
		$txt_average_cost = trim(htmlentities($_POST['txt_average_cost']));
		$txt_komisi = trim(htmlentities($_POST['txt_komisi']));
		
	$total_lembar = $rows['TOTAL_LMBR'] * 100;
	$txt_lembar = $txt_lembar * 100;
	$lembar = $total_lembar + $txt_lembar;
    if ($lembar > $rows['lembar']){
    	echo "<script>alert('Lembar melebihi order!');</script>";
		throw new Exception($data->err_report('s02'));
    }
    //warning---------------------
		$total_deal = $rows['harga'] * $txt_lembar;
		$value_shares = $data->get_row("select sum(total) as jumlah from tbl_kr_mst_saham");
		$value_bonds = $data->get_row("select sum(market_value) as jumlah from tbl_kr_me_bonds");
		$value_pn = $data->get_row("select sum(price_val) as jumlah from tbl_kr_pn");
		$value = ($value_shares['jumlah'] + $value_bonds['jumlah']) + $value_pn['jumlah'];
		$stock = $data->get_row("select total from tbl_kr_mst_saham where code = '".$txt_code."'");
		$total_all = $stock['total'] + $total_deal;
        if ($value>0){
        	$jumlah = $total_all * (100/$value);
			$warning = $data->get_rows("SELECT * FROM tbl_kr_warning");
			$valred = ($warning[0][batas]);
			$valyellow = ($warning[1][batas]);
			$valgreen = ($warning[2][batas]);
			if ($jumlah >= $valred){
				$status_war = 1;
			}elseif ($jumlah >= $valyellow){
				$status_war = 2;
			}elseif ($jumlah < $valyellow){
				$status_war = 3;
			}
		}else{
			$status_war = 1;
		}
	//end warning -------

	$rowd = $data->get_row("select * from tbl_kr_me where pk_id='".$_GET['fk']."'");
	$lembar2 = $rowd['lembar'] - $txt_lembar;
	$sql3= "UPDATE tbl_kr_me SET lembar = '".$lembar2."' WHERE pk_id = '".$fk."'";
    $sql4 = "INSERT INTO tbl_kr_dealer(
    code,
    type_buy,
    lembar,
    harga,
    total,
    me_id,
    dealer_id,
    fk_id,
    warning,
    persen,
    securitas_id,
    id_temp,
	allocation,
    create_dt
    )VALUE(
    '$rows[code]',
    '$rows[type_buy]',
    '$txt_lembar',
    '$rows[harga]',
    '$total_deal',
    '$rows[me_id]',
    '$rows[dealer_id]',
    '$rows[fk_id]',
    '$status_war',
    '$jumlah',
    '$rows[securitas_id]',
    '$rows[pk_id]',
	'$allocation',
    now())";

		if (!$data->inpQueryReturnBool($sql3)){
			throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql4)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='me_dealer_open.php?detail=1&id=".$id."&fk=".$fk."&tmp=".$tmp."';</script>";
	}catch (Exception $e1){
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
	}
}
$tmpl->addVar('page','add',$add);
$tmpl->addRows('loopData',$DG);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);
$tmpl->displayParsedTemplate('page');
?>