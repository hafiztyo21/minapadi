<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('deposit_hist.html');

if($_GET['del']==1){

    $sql = "DELETE FROM tbl_kr_deposit_hist where pk_id='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');document.form1.submit();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}

}

 ############################
 $allocation 	= $data->cb_allocation('txt_allocation',$_POST[txt_allocation]," ");
$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
$rowo = $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$txt_allocation."'");
$allocation2 = $rowo['A'];
$tombol			= "<input type=submit name=btn_view value=View>";
 #######################

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_deposit_hist.pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
//$linkdel = 'deposit.php';
$linkEdit = 'deposit_hist_edit.php';
$link = 'deposit_add.php';
	//$addLink = "<input type=button name=add value='Add Transaction' onclick=\"window.location='".$link."?add=1';\">";
	//$tmpl->addVar('page','add',$addLink);

//$linkExport = 'deposit_export_filter.php';
//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'\',\'Export\',\'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=350,height=300 \')">';
//$tmpl->addVar('page','export',$btnExport);

if ($_POST['btn_search'] )
{
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql  = "select tbl_kr_deposit_hist.*, format(face_value,0) as QTY from tbl_kr_deposit_hist
      where 1 AND allocation ='".$txt_allocation."'  AND upper($id_serach) like upper('%$q_serach%') order by $order_by $sort_order";
//print_r($sql);
	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else

{
	$_SESSION['sql']='';

	$sql  = "select tbl_kr_deposit_hist.*, format(face_value,0) as QTY from tbl_kr_deposit_hist
      where 1  AND allocation ='".$txt_allocation."' order by $order_by $sort_order";
//print_r($sql);

}

if ($_POST['btn_view']=='View'){
  $transaction_date =  trim(htmlentities($_POST['transactionDate']));
  $date = date('Y-m-d', strtotime($transaction_date));
  $id_serach =  $_POST['cb_search'];
  $q_serach =  $_POST['txt_search'];
  if($transaction_date!=''){
    $tr_date = " AND create_dt='".$transaction_date."' ";
  }
  
  $sql = "select tbl_kr_deposit_hist.*, format(face_value,0) as QTY from tbl_kr_deposit_hist
      where 1 AND allocation ='".$txt_allocation."' AND upper($id_serach) like upper('%$q_serach%') $tr_date order by $order_by $sort_order";
//echo $sql;
}
		 
$arrFields = array(
		'tbl_kr_deposit_hist.name_bank'=>'Bank'

);
//print_r($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridDepositHist($sql,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$linkEdit,'delete',$link);

  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);
$path = array
    (
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
    'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
        );
$tmpl->addVars('path',$path);




$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page', 'search',$searchCB);
$tmpl->addVar('page', 'allocation',$allocation);
$tmpl->addVar('page', 'allocation2',$allocation2);
$tmpl->addVar('page', 'tombol',$tombol);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>