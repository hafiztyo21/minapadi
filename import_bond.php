<?php
session_start();
include 'Classes/excel_reader2.php';
//include 'Classes/PHPExcel/Writer/Excel2007.php';
//include 'Classes/PHPExcel/IOFactory.php';
//include "excel_reader2.php";
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$datadb = new globalFunction();
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('import_contact_personal.html');
$tablename = 'tbl_kr_stock_bonds';


if($_POST[Submit]) {
						$data = new Spreadsheet_Excel_Reader($_FILES['uploadedfile']['tmp_name']);
					 
					// membaca jumlah baris dari data excel
					$baris = $data->rowcount($sheet_index=0);
					 
					// nilai awal counter untuk jumlah data yang sukses dan yang gagal diimport
					$sukses = 0;
					$gagal = 0;
					 
					// import data excel mulai baris ke-2 (karena baris pertama adalah nama kolom)
					for ($i=2; $i<=$baris; $i++)
					{
						$bond_code=$data->val($i, 1);
					  // membaca data nim (kolom ke-1)
					  $isin= $data->val($i, 2);
					  // membaca data nama (kolom ke-2)
					  
				
					  $bond_name=$data->val($i, 3);
					  $bond_type=$data->val($i, 4);
					  $other_bond_code=$data->val($i, 5);
					  
					  $country_code=$data->val($i, 6);
					  $issuer_code=$data->val($i, 7);
					  $snsp=$data->val($i, 8);
					  $abs_type=$data->val($i, 9);
					  $bond_seniority=$data->val($i, 10);
					  
					$issue_rate=$data->val($i, 11);
					$issue_yield=$data->val($i, 12);
					$issue_price=$data->val($i, 13);
					$issue_date=$data->val($i, 14);
					$maturity_date=$data->val($i, 15);
					
					$listing_status=$data->val($i, 16);
					$listed_date=$data->val($i, 17);
					$issue_amount=$data->val($i, 18);
					$scripless=$data->val($i, 19);
					$rate_exemptedgh=$data->val($i, 20);
					$curency_code=$data->val($i, 21);
					$int_current_code=$data->val($i, 22);
					$amount_outstanding=$data->val($i, 23);
					$coupon_nominal=$data->val($i, 24);
					$payment_endperiod=$data->val($i, 25);
					$day_count_basis=$data->val($i, 26);
					
					$irreg_cash_flow=$data->val($i, 27);
					$coupon_pay_method=$data->val($i, 28);
					$coupon_rate=$data->val($i, 29);
					$coupon_prorated=$data->val($i, 30);
					$coupon_compounding_period=$data->val($i, 31);
	
					$coupon_term_month=$data->val($i, 32);
					$coupon_generation_method=$data->val($i, 33);
					$fix_coupon_day=$data->val($i, 34);
					$odd_lastcoupon_type=$data->val($i, 35);
					$spread_rate=$data->val($i, 36);
					
					$rate_composition=$data->val($i, 37);
					$coupon_prepay=$data->val($i, 38);
					$cap_type=$data->val($i, 39);
					$cap_rate=$data->val($i, 40);
					$floor_rate=$data->val($i, 41);
					
					$calendar_code=$data->val($i, 42);
					$instrument_type=$data->val($i, 43); 
					 $Tenor=$data->val($i, 44);
					$Issue_tenor=$data->val($i, 45);
					$Tier=$data->val($i, 46);
					
					$Originated_amount=$data->val($i, 47);
					$Bond_class_node=$data->val($i, 48);
					$Issue_type=$data->val($i, 49);
					
					  // setelah data dibaca, sisipkan ke dalam tabel mhs
					  $query = 
					  "INSERT INTO $tablename(
							bond_code, 
						  isin, 
						  bond_name, 
						  bond_type, 
						  other_bond_code, 
						  country_code, 
						  issuer_code, 
						  snsp, 
						  abs_type, 
						  bond_seniority, 
						  issue_rate, 
						  issue_yield, 
						  issue_price, 
						  issue_date, 
						  maturity_date, 
						  listing_status, 
						  listed_date, 
						  issue_amount, 
						  scripless, 
						  rate_exemptedgh, 
						  curency_code,
						  int_current_code, 
						  amount_outstanding, 
						  coupon_nominal,
						  payment_endperiod, 
						  day_count_basis, 
						  irreg_cash_flow, 
						  coupon_pay_method, 
						  coupon_rate, 
						  coupon_prorated, 
						  coupon_compounding_period, 
						  coupon_term_month, 
						  coupon_generation_method, 
						  fix_coupon_day, 
						  odd_lastcoupon_type, 
						  spread_rate, 
						  rate_composition, 
						  coupon_prepay, 
						  cap_type, 
						  cap_rate, 
						  floor_rate, 
						  calendar_code, 
						  instrument_type, 
						  Tenor,
						  Issue_tenor, 
						  Tier, 
						  Originated_amount, 
						  Bond_class_node, 
						  Issue_type,
						create_dt

						) VALUES 
					  ('$bond_code',
					  '$isin',
					  '$bond_name',
					  '$bond_type',
					  '$other_bond_code',
					  
					  '$country_code',
					  '$issuer_code',
					  '$snsp',
					  
					  '$abs_type',
					  '$bond_seniority',
					  
					  '$issue_rate',
					  '$issue_yield',
					  '$issue_price',
					  
					  
					  '$issue_date',
					  '$maturity_date',
					  '$listing_status',
					  '$listed_date',
					  '$issue_amount',
					  
					  
					  '$scripless',
					  '$rate_exemptedgh',
					  '$curency_code',
					  '$int_current_code',
					  '$amount_outstanding',
					  '$coupon_nominal',
					  
					  '$payment_endperiod',
					  '$day_count_basis',
					  '$irreg_cash_flow',
					  '$coupon_pay_method',
					  '$coupon_rate',
					  
					  '$coupon_prorated',
					  '$coupon_compounding_period',
					  '$coupon_term_month',
					  '$coupon_generation_method',
					  '$fix_coupon_day',
					  
					  '$odd_lastcoupon_type',
					  '$spread_rate',
					  '$rate_composition',
					  '$coupon_prepay',
					  '$cap_type',
					  '$cap_rate',
					  
					   '$floor_rate',
					  '$calendar_code',
					  '$instrument_type',
					  '$Tenor',
					  '$Issue_tenor',
					  
					  
					  
					   '$Tier',
					  '$Originated_amount',
					  '$Bond_class_node',
					  '$Issue_type',
					  now()
					  
					  )";
				//	print($query);
						if ($datadb->inpQueryReturnBool($query))
						{	echo "<script>alert('SUKSES');</script>";	
						 $sukses++;}
						else
						{	echo "<script>alert('GAGAL');</script>";
						$gagal++;}
			
					 
					  // jika proses insert data sukses, maka counter $sukses bertambah
					  // jika gagal, maka counter $gagal yang bertambah
					
				
					}
					 
					// tampilan status sukses dan gagal
					echo "<h3>Proses import data selesai.</h3>";
					echo "<p>Jumlah data yang sukses diimport : ".$sukses."<br>";
					echo "Jumlah data yang gagal diimport : ".$gagal."</p>";
	
	
	
}



$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->addVar('page', 'tittle',$tittle);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>