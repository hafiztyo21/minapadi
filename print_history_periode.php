<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('print_sub_settlement2a.html');

//header("Content-type: application/vnd.msword");
//header("Content-Disposition: attachment; filename=Instruksi_Jual_Beli_Saham".$prev.".doc");
//header("Pragma: no-cache");
//header("Expires: 0");
$back = "<input type=button name=add value='Back' onclick=\"window.location='settlement.php';\">";
$tmpl->addVar('page','back',$back);
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_dealer_tmp.code'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);


###########################end of sorting##################################
$tanggal = $_GET['tgl'];
$cd = $_GET['code'];
$allocation = $_GET['all'];

$rowo = $data->get_row("select cd_client as D, description as C, up as UP, bank as BANK from tbl_kr_allocation where pk_id = '".$allocation."'");

$up 	= $rowo['UP'];
$bank	= $rowo['BANK'];
$c	= $rowo['C'];
$d	= $rowo['D'];
$type = $_GET['type_buy'];
$nama = $_SESSION['username'];
$settlement_id = $_SESSION['pk_id'];
$_SESSION['sql']='';
		
				if ($cd==''){
				$sql  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram2,FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type,
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
			
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		

				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
		
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		
				
				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram2, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
			
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
			
				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";

				}
				else{
				$sql  = "SELECT tbl_kr_dealer_tmp.*, LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram2, FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";

				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";


				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram2, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";


				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
				}
				
$tombol= "<input type=submit name=btn_view value=View>";
$tgl = $_POST[txt_tanggal];
$z   = $data->cb_day('txt_hari',$_POST[txt_hari],"");
$txt_hari 	= trim(htmlentities($_POST['txt_hari']));
if($allocation==1){
$link 	= 'print_settlement.php?tgl='.$tanggal.'&all='.$allocation.'&code='.$cd.'&day='.$txt_hari.'';
}else{
$link 	= 'print_settlement2.php?tgl='.$tanggal.'&all='.$allocation.'&code='.$cd.'&day='.$txt_hari.'';
}
$b		=		$allocation2;


$addLink = "<input type=button name=add value='Print'  onclick=\"window.location='".$link."';\">";
$tmpl->addVar('page','addLink',$addLink);
$tmpl->addVar('page','tombol',$tombol);
$tmpl->addVar('page','txt_hari',$txt_hari);
$tmpl->addVar('page','z',$z);
 if ($_POST['btn_view']=='View'){
			if ($cd==''){
				$sql  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram2 ,FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type,
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		

				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		
				
				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram2, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
			
				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";

				}
				else{
				$sql  = "SELECT tbl_kr_dealer_tmp.*, LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei) AS suram2,  FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";

				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";


				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram2, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";


				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND tbl_kr_dealer_tmp.code = '".$cd."'
				AND allocation ='".$allocation."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
				}
				

}





//$share = "SELECT lembar FROM tbl_kr_mst_saham WHERE code='".$cd."'";
//print_r ($sqlsell);
$DG= $data->dataGridDeal($sql,'pk_id',$data->ResultsPerPage);
$DG2= $data->dataGridDeal($sqlsell,'pk_id',$data->ResultsPerPage);

$rowLembar = $data->get_rows($sql2);
$rowLembar2 = $data->get_rows($sqlsell2);

$buy_l = 0;
$buy_t = 0;
for ($i=0;$i<count($rowLembar);$i++)
{
$buy_l = $buy_l + $rowLembar[$i][lembar];
$buy_t = $buy_t + $rowLembar[$i][nilai_beli];
}

$buy_l = number_format($buy_l,0);
$buy_t = number_format($buy_t,0);

$sell_l = 0;
$sell_t = 0;
for ($i=0;$i<count($rowLembar2);$i++)
{
$sell_l = $sell_l + $rowLembar2[$i][lembar];
$sell_t = $sell_t + $rowLembar2[$i][nilai_jual];


}
$sell_l = number_format($sell_l,0);
$sell_t = number_format($sell_t,0);


$tmpl->addVar('page','nama',$nama);
$tmpl->addVar('page','up',$up);
$tmpl->addVar('page','bank',$bank);
$tmpl->addVar('page','c',$c);
$tmpl->addVar('page','d',$d);
$tmpl->addVar('page','date',$tanggal);
$tmpl->addVar('page','code',$cd);
$tmpl->addVar('page','type_buy',$type);
$tmpl->addVar('page','tottrans',$buy_l);
$tmpl->addVar('page','totnilai',$buy_t);
$tmpl->addVar('page','tottrans2',$sell_l);
$tmpl->addVar('page','totnilai2',$sell_t);
$tmpl->addRows('loopData',$DG);
$tmpl->addRows('loopData2',$DG2);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->displayParsedTemplate('page');
?>