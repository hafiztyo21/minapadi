<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$filename = $transaction_date."_liquidation_data.txt";

// output headers so that the file is downloaded rather than displayed
//header('Content-Type: text/csv; charset=utf-8');
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
try{


$output = fopen('php://output', 'w');

/*$myfile = fopen("newfile.txt", "w");
$txt = "Mickey Mouse\n";
fwrite($myfile, $txt);
$txt = "Minnie Mouse\n";
fwrite($myfile, $txt);
fclose($myfile);*/

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    im_code,
    fund_code,
    DATE_FORMAT(last_nav_date, '%Y%m%d') as last_nav_date, 
    DATE_FORMAT(payment_date, '%Y%m%d') as payment_date     
    FROM tbl_kr_liquidation
    WHERE DATE_FORMAT(tbl_kr_liquidation.created_date, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0";

$rows = $data->get_rows2($query);
    
fwrite($output, "\r\n");

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    if($rows[$i]['payment_date'] == '0000-00-00')
        $rows[$i]['payment_date'] = '';

    $str = $rows[$i]['im_code']."|".$rows[$i]['fund_code']."|".$rows[$i]['last_nav_date']
        ."|".$rows[$i]['payment_date']."\r\n";

    fwrite($output, $str);
    //fputcsv($output, $rows[$i]);
}

fclose($output);
} catch(Exception $e){
    echo $e->getMessage();
}
?>
