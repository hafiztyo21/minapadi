<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('me_dealer_repo_add.html');

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_dealer_tmp.pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$back ="<input type=button name=add value='Back' onclick=\"window.location='me_dealer.php';\">";
$add ="<input type=button name=add value='Add Transaction' onclick=\"window.location='me_dealer_add.php?add=1&id=$_GET[id]&fk=$_GET[fk]';\">";
$tmpl->addVar('page','add',$add);
$tmpl->addVar('page','back',$back);
$id = $_GET[id];
$fk = $_GET[fk];
if ($_GET['detail']=='1'){
	$sql  = "SELECT tbl_kr_dealer_tmp.pk_id,fk_id,dealer_id,type_buy,warning, format(lembar,0) as LMBR, format(harga,0) as HRG, fullname, stock_name, securitas_type,
			format(total,0) as TTL,
			(select sum(lembar) from tbl_kr_dealer where id_temp = tbl_kr_dealer_tmp.pk_id)as LMBR_DEAL FROM tbl_kr_dealer_tmp
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_dealer_tmp.me_id
	LEFT JOIN tbl_kr_stock ON tbl_kr_stock.stock_id = tbl_kr_dealer_tmp.code
	LEFT JOIN tbl_kr_securitas ON tbl_kr_securitas.securitas_id = tbl_kr_dealer_tmp.securitas_id
   where tbl_kr_dealer_tmp.fk_id = '".$fk."' order by $order_by $sort_order";
$DG= $data->dataGridDealer($sql,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link);
}

$tmpl->addRows('loopData',$DG);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);
$tmpl->displayParsedTemplate('page');
?>