<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('saham_add.html');
$tablename = 'tbl_kr_saham';





if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
 		$txt_code = trim(htmlentities($_POST['txt_code']));
		$txt_lembar = trim(htmlentities(str_replace(",","",$_POST['txt_lembar'])));
		$txt_harga = trim(htmlentities(str_replace(",","",$_POST['txt_harga'])));
		$txt_status = trim(htmlentities($_POST['txt_status']));
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
		$total = ($txt_harga * $txt_lembar);
		if($txt_lembar==''){
			echo "<script>alert('Number of Shares is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga==''){
			echo "<script>alert('Price is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
		$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
		$txt_settlement_date = trim(htmlentities($_POST['txt_settlement_date']));
		$txt_commision = trim(htmlentities(str_replace(",","",$_POST['txt_commision'])));
		$txt_sales_tax = trim(htmlentities(str_replace(",","",$_POST['txt_sales_tax'])));
		$txt_levy = trim(htmlentities(str_replace(",","",$_POST['txt_levy'])));
		$txt_vat = trim(htmlentities(str_replace(",","",$_POST['txt_vat'])));
		$txt_other_charges = trim(htmlentities(str_replace(",","",$_POST['txt_other_charges'])));
		$txt_wht_on_commision = trim(htmlentities(str_replace(",","",$_POST['txt_wht_on_commision'])));
		$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
		$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
		$txt_cancelation_reason = trim(htmlentities($_POST['txt_cancelation_reason']));

		//warning---------------------
		$value_shares = $data->get_row("select sum(total) as jumlah from tbl_kr_mst_saham");
		$value_bonds = $data->get_row("select sum(market_value) as jumlah from tbl_kr_me_bonds");
		$value_pn = $data->get_row("select sum(price_val) as jumlah from tbl_kr_pn");
		$value = $value_shares['jumlah'] + $value_bonds['jumlah'] + $value_pn['jumlah'];
        if ($value>0){
        	$jumlah = $total * (100/$value);
			$warning = $data->get_rows("SELECT * FROM tbl_kr_warning");
			$valred = ($warning[0][batas]);
			$valyellow = ($warning[1][batas]);
			$valgreen = ($warning[2][batas]);
			if ($jumlah >= $valred){
				$status_war = 1;
			}elseif ($jumlah >= $valyellow){
				$status_war = 2;
			}elseif ($jumlah >= $valgreen){
				$status_war = 3;
			}
		}else{
			$status_war = 1;
		}
		//end warning -------
        $rows_c = $data->get_row("select * from tbl_kr_mst_saham where code='".$txt_code."'");
		$query2= "select * from tbl_kr_saham where code='".$txt_code."'";
		$hasil = mysql_query($query2);
		while ($data2 = mysql_fetch_array($hasil))
		{
			$total_all = ($total_all + $data2[total]);
		}
		if (empty($rows_c[code])){
			$total_all = $total_all + $total;
			$sql_saham1 = "INSERT INTO tbl_kr_mst_saham (code,lembar,avg,total,allocation)VALUES('$txt_code','$txt_lembar','$txt_harga','$total_all','$txt_allocation')";
			$sql_saham2 = "INSERT INTO tbl_kr_saham (
				code,
				lembar,
				harga,
				total,
				status,
				allocation,
				create_dt,
				transaction_status,
				ta_reference_id,
				ta_reference_no,
				settlement_date,
				im_code,
				br_code,
				commission,
				sales_tax,
				levy,
				vat,
				other_charges,
				wht_on_commission,
				settlement_type,
				remarks,
				cancellation_reason
			)VALUES('$txt_code',
				'$txt_lembar',
				'$txt_harga',
				'$total',
				'$txt_status',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_ta_reference_id',
				'$txt_ta_reference_no',
				'$txt_settlement_date',
				'MU002',
				'MU001',
				'$txt_commision',
				'$txt_sales_tax',
				'$txt_levy',
				'$txt_vat',
				'$txt_other_charges',
				'$txt_wht_on_commision',
				'$txt_settlement_type',
				'$txt_remarks',
				'$txt_cancelation_reason'
			)";
		}else{
			if ($txt_status=='Buy'){
				$total_all = $total_all + $total;
				$lembar = $rows_c[lembar] + $txt_lembar;
				$avg = $total_all/$lembar;
			}elseif ($txt_status=='Sell'){
				if ($txt_lembar > $rows_c[lembar]){
					echo "<script>alert('Lembar tidak mencukupi!');</script>";
					throw new Exception($data->err_report('s02'));
				}
				elseif ($txt_lembar <= $rows_c[lembar]){
					$total_all = $total_all - $total;
					$lembar = $rows_c[lembar] - $txt_lembar;
					$avg = $total_all/$lembar;
				}
			}
			$sql_saham1 = "INSERT INTO tbl_kr_saham (
				code,
				lembar,
				harga,
				total,
				status,
				allocation,
				create_dt,
				transaction_status,
				ta_reference_id,
				ta_reference_no,
				settlement_date,
				im_code,
				br_code,
				commission,
				sales_tax,
				levy,
				vat,
				other_charges,
				wht_on_commission,
				settlement_type,
				remarks,
				cancellation_reason
				)VALUES('$txt_code',
				'$txt_lembar',
				'$txt_harga',
				'$total',
				'$txt_status',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_ta_reference_id',
				'$txt_ta_reference_no',
				'$txt_settlement_date',
				'MU002',
				'MU001',
				'$txt_commision',
				'$txt_sales_tax',
				'$txt_levy',
				'$txt_vat',
				'$txt_other_charges',
				'$txt_wht_on_commision',
				'$txt_settlement_type',
				'$txt_remarks',
				'$txt_cancelation_reason'
				)";
			$sql_saham2 = "UPDATE tbl_kr_mst_saham SET lembar = '".$lembar."',avg = '".$avg."',total = '".$total_all."'
			WHERE code = '".$txt_code."'";
		}
		//---------------------------------------------------------------------------------
		if (!$data->inpQueryReturnBool($sql_saham1)){
			throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='saham_mst.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}



if ($_GET['add']==1){
$rows = $data->get_row("select * from tbl_kr_stock_rate where stk_cd='".$_POST[txt_code]."'");
$sql_C = "SELECT stk_date,stk_cd,stk_id FROM tbl_kr_stock_rate where stk_date = (select max(stk_date) from tbl_kr_stock_rate) ORDER BY stk_cd asc ";
  	   	#print_r($sql_C);
		$result = mysql_query($sql_C);
  		$saham_contract ="<select size=1 name=txt_code id=txt_code OnChange= \"document.form1.submit();\">";
		$saham_contract .= "<option value=''></option>";
		while ($row = mysql_fetch_array($result))
		{
		$cek_code=$data->get_value("SELECT code FROM tbl_kr_mst_saham WHERE code='".$row['stk_cd']."' AND allocation='".$_POST[txt_allocation]."'");
			if($cek_code<>""){
			$selected = ($_POST[txt_code] == $row['stk_cd'])? ' selected': '';
			$saham_contract .=  "<option value=".$row['stk_cd']." ".$selected.">".$row['stk_cd']."</option>";
			}
			
		}

	//set default value		
		$harga = $_POST['txt_harga'] == "" ? 0 : $_POST['txt_harga'];
		$lembar = $_POST['txt_lembar'] == "" ? 0 : $_POST['txt_lembar'];
		$commision = $_POST['txt_commision'] == "" ? 0 : $_POST['txt_commision'];
		$sales_tax = $_POST['txt_sales_tax'] == "" ? 0 : $_POST['txt_sales_tax'];
		$levy = $_POST['txt_levy'] == "" ? 0 : $_POST['txt_levy'];
		$vat = $_POST['txt_vat'] == "" ? 0 : $_POST['txt_vat'];
		$other_charges = $_POST['txt_other_charges'] == "" ? 0 : $_POST['txt_other_charges'];
		$gross_settlement_amount = $_POST['txt_gross_settlement_amount'] == "" ? 0 : $_POST['txt_gross_settlement_amount'];
		$wht_on_commision = $_POST['txt_wht_on_commision'] == "" ? 0 : $_POST['txt_wht_on_commision'];
		$net_settlement_amount = $_POST['txt_net_sttlement_amount'] == "" ? 0 : $_POST['txt_net_sttlement_amount'];
	//===========

          $saham_contract .="</select>";
$tittle = "ADD TRANSACTION - SHARES";
$dataRows = array (
	'TEXT' =>  array(
		'Allocation',
		'Code',
		'Name of Shares',
		'Price (Rp.)',
		'Number of Shares',
		'Status',
		'&nbsp;',
		'Transaction Status',
		'TA Reference ID',
		'TA Reference No.',
		'Settlement Date',
		'Commision',
		'Sales Tax',
		'Levy',
		'VAT',
		'Other Charges',
		'Gross Settlement Amount',
		'WHT on Commision',
		'Net Settlement Amount',
		'Settlement Type',
		'Remarks',
		'Cancelation Reason'
		),
  	'DOT'  => array (':',':',':',':',':'),
	'FIELD' => array ($data->cb_allocation('txt_allocation',$_POST[txt_allocation]," OnChange= \"document.form1.submit();\" "),
		$saham_contract,
		"<input type=text size='50' readonly='readonly' name=txt_saham value='".$rows[stk_name]."'>",
		"<input type=text size='50' name=txt_harga id='txt_harga' value='".$_POST[txt_harga]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
		"<input type=text size='30' name=txt_lembar id='txt_lembar' value='".$rows[lembar]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
		$data->cb_saham_status('txt_status',$_POST[txt_status], "onchange='calculate()'")." ",
		'',
		"<select name='txt_transaction_status' onchange='changeStatus()'><option value='NEWM'>New Trade</option><option value='CANC'>Cancel Trade</option></select>",
		"<input type=text size='50' name=txt_ta_reference_id readonly='readonly' value='".$_POST[txt_ta_reference_id]."' placeholder='will be assign by S-INVEST'>",
		"<input type=text size='50' name=txt_ta_reference_no value='".$_POST[txt_ta_reference_no]."'>",
		$data->datePicker('txt_settlement_date', $_POST[txt_settlement_date],''),
		"<input type=text size='25' name=txt_commision id='txt_commision' value='".$commision."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
		"<input type=text size='25' name=txt_sales_tax id='txt_sales_tax' value='".$sales_tax."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()' readonly='readonly'>",
		"<input type=text size='25' name=txt_levy id='txt_levy' value='".$levy."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
		"<input type=text size='25' name=txt_vat id='txt_vat' value='".$vat."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
		"<input type=text size='25' name=txt_other_charges id='txt_other_charges' value='".$other_charges."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
		"<input type=text size='25' name=txt_gross_settlement_amount id='txt_gross_settlement_amount' value='".$gross_settlement_amount."' readonly='readonly'>",
		"<input type=text size='25' name=txt_wht_on_commision id='txt_wht_on_commision' value='".$wht_on_commision."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
		"<input type=text size='25' name=txt_net_settlement_amount id='txt_net_settlement_amount' value='".$net_settlement_amount."' readonly='readonly'>",
		"<select name='txt_settlement_type'><option value='1'>DVP</option><option value='2'>RVP</option><option value='3'>DFOP</option><option value='4'>RFOP</option></select>",
		"<input type=text size='50' name=txt_remarks value='".$_POST[txt_remarks]."'>",
		"<input type=text size='50' name=txt_cancelation_reason value='".$_POST[txt_cancelation_reason]."'>",
	)
		);
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='saham_mst.php';\">");
}

$javascript = "<script type='text/javascript'>
	
	function calculate(){
		var type = document.getElementById('txt_status').value;

		var doc = document;

		var harga = parseFloat(doc.getElementById('txt_harga').value.toString().replace(',',''));
		var lembar = parseFloat(doc.getElementById('txt_lembar').value.toString().replace(',',''));

		var tradeAmount = harga * lembar;

		var commision = parseFloat(doc.getElementById('txt_commision').value.toString().replace(',',''));
		var salesTax = parseFloat(doc.getElementById('txt_sales_tax').value.toString().replace(',',''));
		var levy = parseFloat(doc.getElementById('txt_levy').value.toString().replace(',',''));
		var vat = parseFloat(doc.getElementById('txt_vat').value.toString().replace(',',''));
		var otherCharges = parseFloat(doc.getElementById('txt_other_charges').value.toString().replace(',',''));

		var whtOnCommision = parseFloat(doc.getElementById('txt_wht_on_commision').value.toString().replace(',',''));

		if(type == 'Buy'){

			salesTax = 0;

			doc.getElementById('txt_sales_tax').value = 0;
			doc.getElementById('txt_sales_tax').readOnly = true;

			var gross = tradeAmount + commision + levy + salesTax + vat + otherCharges;
			var net = gross - whtOnCommision;

		}else{
			doc.getElementById('txt_sales_tax').readOnly = false;

			var gross = tradeAmount - commision - levy - salesTax - vat - otherCharges;
			var net = gross + whtOnCommision;
		}
		doc.getElementById('txt_gross_settlement_amount').value = reformat2(gross);
		doc.getElementById('txt_net_settlement_amount').value = reformat2(net);
	}

	function changeStatus(){
		if(document.getElementById('txt_transaction_status').value == 'NEWN'){
			document.getElementById('txt_reference_id').value = '';
			document.getElementById('txt_reference_id').readOnly = true;
		}else{
			document.getElementById('txt_reference_id').readOnly = false;
		}
	}
	
</script>";


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css',
	  'PATHCUSTOMJS' => $GLOBALS['JS'].'custom.js',
      	);

$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>