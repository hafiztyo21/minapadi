<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('print_settlement.html');

header("Content-type: application/vnd.msword");
header("Content-Disposition: attachment; filename=Instruksi_Jual_Beli_Saham ".$_GET['tgl']."".$prev.".doc");
header("Pragma: no-cache");
header("Expires: 0");

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_dealer_tmp.pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='desc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);


###########################end of sorting##################################

$tanggal = $_GET['tgl'];
$cd = $_GET['code'];
$allocation = $_GET['all'];
$txt_hari = $_GET['day'];
$rowo = $data->get_row("select cd_client as D, description as C, up as UP, bank as BANK from tbl_kr_allocation where pk_id = '".$allocation."'");

$up 	= $rowo['UP'];
$bank	= $rowo['BANK'];
$c	= $rowo['C'];
$d	= $rowo['D'];
$type = $_GET['type_buy'];
$nama = $_SESSION['username'];
$settlement_id = $_SESSION['pk_id'];
$_SESSION['sql']='';
//print_r ($txt_hari);
if ($cd=='' && $txt_hari =='' ){
			
				$sql  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram,FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type,
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		

				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		
				
				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
			
				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";

				}
				else{
					$sql  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram,FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type,
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
	
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
			
				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total, komisi,levy,kpei,pajak, pajak_jual,pajak_komisi,nilai_jual,nilai_beli,
				( total + komisi + pajak +levy + kpei) AS suram,
				tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";

				
				$sql3  = "SELECT sum(nilai_beli)as total, code,type_buy FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				GROUP BY code";

				//print($sql3);
				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";


				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,harga,total,komisi,levy,kpei, pajak, pajak_jual, pajak_komisi,nilai_jual,nilai_beli,
				( total - komisi - pajak -levy - kpei-pajak_jual) AS suram,
				tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
			
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
				
				$sqlsell3  = "SELECT sum(nilai_jual) as total,code,type_buy FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				GROUP BY code";
//print($sqlsell3);
				}


//$share = "SELECT lembar FROM tbl_kr_mst_saham WHERE code='".$cd."'";
//print_r ($sqlsell);
$DG= $data->dataGridDeal($sql,'pk_id',$data->ResultsPerPage);
$DG2= $data->dataGridDeal($sqlsell,'pk_id',$data->ResultsPerPage);


$rowLembar = $data->get_rows($sql2);
$rowLembar2 = $data->get_rows($sqlsell2);

$buy_l = 0;
$buy_t = 0;
for ($i=0;$i<count($rowLembar);$i++)
{

$buy_l = $buy_l + $rowLembar[$i][lembar];
$buy_t = $buy_t + $rowLembar[$i][nilai_beli];
$buy_levy += $rowLembar[$i][levy];
$buy_kpei += $rowLembar[$i][kpei];
$buy_pajak += $rowLembar[$i][pajak];
$buy_komisi += $rowLembar[$i][komisi];
$buy_pajak_komisi += $rowLembar[$i][pajak_komisi];
$buy_total += $rowLembar[$i][total];
$buy_pajak_jual += $rowLembar[$i][pajak_jual];
$buy_suram += $rowLembar[$i][suram];
}

$buy_l = number_format($buy_l,0);
$buy_t = number_format($buy_t,0);

$sell_l = 0;
$sell_t = 0;
for ($i=0;$i<count($rowLembar2);$i++)
{
$sell_l = $sell_l + $rowLembar2[$i][lembar];
$sell_t = $sell_t + $rowLembar2[$i][nilai_jual];
 $sell_total += $rowLembar2[$i][total];
 $sell_komisi += $rowLembar2[$i][komisi];
$sell_pajak += $rowLembar2[$i][pajak];
$sell_levy += $rowLembar2[$i][levy];
$sell_kpei += $rowLembar2[$i][kpei];
$sell_pajak_jual += $rowLembar2[$i][pajak_jual];
$sell_pajak_komisi += $rowLembar2[$i][pajak_komisi];
$sell_suram += $rowLembar2[$i][suram];


}
$sell_l = number_format($sell_l,0);
$sell_t = number_format($sell_t,0);


######################## NETT #######################################
$nett_buy = $data->get_rows($sql3);
$nett_sell = $data->get_rows($sqlsell3);
$detail.="<table width='100%' cellpadding='0' cellspacing='0' border='0' style='font-size:11px;'>";

for ($b=0;$b<count($nett_buy);$b++)
{

	
	for ($s=0;$s<count($nett_sell);$s++)
	{
	$DG3[$s] = $nett_sell[$s];
			if ($nett_buy[$b][code]== $nett_sell[$s][code])
			{
			$nett_total = $nett_buy[$b][total]-$nett_sell[$s][total];
				if ($nett_total < 0){
				$type = "SELL";
				$nett_total = $nett_total *-1;
				}
				else
				{
				$type ="BUY";
				$nett_total = $nett_total;
				}
				$nett_total = number_format($nett_total,0);
			$detail.="<tr>";
			$detail.="<td align='left' width=94%><b>NETTING ".$type." ".$nett_sell[$s][code]."</td>";
			$detail.="<td align='left'><b>".$nett_total."</b></td>";
			
			$detail.="</tr>";
			}
		
	}
	
	

	
}
$detail.= "</table>";



###################################################################
$tmpl->addVar('page','nama',$nama);
$tmpl->addVar('page','up',$up);
$tmpl->addVar('page','bank',$bank);
$tmpl->addVar('page','c',$c);
$tmpl->addVar('page','d',$d);
$tmpl->addVar('page','detail',$detail);
$tmpl->addVar('page','date',$tanggal);
$tmpl->addVar('page','code',$cd);

$tmpl->addVar('page','type_buy',$type);
$tmpl->addVar('page','tottrans',$buy_l);
$tmpl->addVar('page','totnilai',$buy_t);
$tmpl->addVar('page','totlevy_buy',number_format($buy_levy,0));
$tmpl->addVar('page','totkpei_buy',number_format($buy_kpei,0));
$tmpl->addVar('page','totpajak_buy',number_format($buy_pajak,0));
$tmpl->addVar('page','totpajak_komisi_buy',number_format($buy_pajak_komisi,0));
$tmpl->addVar('page','tottotal_buy',number_format($buy_total,0));
$tmpl->addVar('page','totkomisi_buy',number_format($buy_komisi,0));
$tmpl->addVar('page','totpajak_jual_buy',number_format($buy_pajak_jual,0));
$tmpl->addVar('page','totsuram_buy',number_format($buy_suram,0));
$tmpl->addVar('page','tottrans2',$sell_l);
$tmpl->addVar('page','totnilai2',$sell_t);
$tmpl->addVar('page','totsell_total',number_format($sell_total,0));
$tmpl->addVar('page','totsell_komisi',number_format($sell_komisi,0));
$tmpl->addVar('page','totsell_pajak',number_format($sell_pajak,0));
$tmpl->addVar('page','totsell_levy',number_format($sell_levy,0));
$tmpl->addVar('page','totsell_kpei',number_format($sell_kpei,0));
$tmpl->addVar('page','totsell_pajak_jual',number_format($sell_pajak_jual,0));
$tmpl->addVar('page','totsell_pajak_komisi',number_format($sell_pajak_komisi,0));
$tmpl->addVar('page','totsell_suram',number_format($sell_suram,0));
$tmpl->addRows('loopData',$DG);
$tmpl->addRows('loopData2',$DG2);
//$tmpl->addRows('loopData3',$DG3);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->displayParsedTemplate('page');
?>