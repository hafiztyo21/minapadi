<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$allocation = $_GET['allocation'];
$filename = $transaction_date."_deposit.txt";

// output headers so that the file is downloaded rather than displayed
//header('Content-Type: text/csv; charset=utf-8');
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
try{


$output = fopen('php://output', 'w');

/*$myfile = fopen("newfile.txt", "w");
$txt = "Mickey Mouse\n";
fwrite($myfile, $txt);
$txt = "Minnie Mouse\n";
fwrite($myfile, $txt);
fclose($myfile);*/

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');

/*
$query = "SELECT 
    A.transaction_status,
    A.action_type,
    A.im_code,
    B.fund_code,
    A.placement_bank_code,
    A.branch_code,
    A.placement_bank_cash_name,
    A.placement_bank_cash_no,
    A.ccy,
    A.face_value as principle,
    A.int_rate as interest_rate,
    DATE_FORMAT(A.placement, '%Y%m%d') as placement_date, 
    DATE_FORMAT(A.maturity, '%Y%m%d') as maturity_date, 
    A.interest_frequency,
    A.interest_type,
    A.sharia_deposit,
    DATE_FORMAT(A.withdrawal_date, '%Y%m%d') as withdrawal_date, 
    A.adjusted_interest_rate,
    A.withdrawal_principle,
    A.withdrawal_interest,
    A.total_withdrawal_amount,
    A.rollover_type,
    A.new_principle_amount,
    A.new_interest_rate,
    DATE_FORMAT(A.new_maturity_date, '%Y%m%d') as new_maturity_date, 
    A.amount_to_be_transferred,
    A.statutory_type,
    A.contact_person,
    A.telephone_no,
    A.fax_no,
    A.reference_no,
    A.parent_reference_no,
    A.description,
    A.cancellation_reason
    FROM tbl_kr_deposit_hist A JOIN tbl_kr_allocation B ON A.allocation = B.pk_id
    WHERE DATE_FORMAT(A.create_dt, '%Y-%m-%d') = '$transaction_date'
    AND (IF(A.action_type='1',IF(A.deposit_id!='0',A.deposit_id,'0'),'-')='0'
    OR IF(A.action_type='2',IF(A.deposit_id!='0',A.deposit_id,'0'),'-')='0'
    OR IF(A.action_type='3',IF(A.deposit_id!='0',A.deposit_id,'0'),'-')='0')
    ";
*/
$query = "SELECT 
    A.transaction_status,
    A.action_type,
    A.im_code,
    B.fund_code,
    A.placement_bank_code,
    A.branch_code,
    A.placement_bank_cash_name,
    A.placement_bank_cash_no,
    A.ccy,
    A.face_value as principle,
    A.int_rate as interest_rate,
    DATE_FORMAT(A.placement, '%Y%m%d') as placement_date, 
    DATE_FORMAT(A.maturity, '%Y%m%d') as maturity_date, 
    A.interest_frequency,
    A.interest_type,
    A.sharia_deposit,
    DATE_FORMAT(A.withdrawal_date, '%Y%m%d') as withdrawal_date, 
    A.adjusted_interest_rate,
    A.withdrawal_principle,
    A.withdrawal_interest,
    A.total_withdrawal_amount,
    A.rollover_type,
    A.new_principle_amount,
    A.new_interest_rate,
    DATE_FORMAT(A.new_maturity_date, '%Y%m%d') as new_maturity_date, 
    A.amount_to_be_transferred,
    A.statutory_type,
    A.contact_person,
    A.telephone_no,
    A.fax_no,
    A.reference_no,
    A.parent_reference_no,
    A.description,
    A.cancellation_reason
    FROM tbl_kr_deposit_hist A JOIN tbl_kr_allocation B ON A.allocation = B.pk_id
    WHERE DATE_FORMAT(A.create_dt, '%Y-%m-%d') = '$transaction_date'
    ";

if($allocation != null && $allocation != ''){
    $query .= " AND A.allocation = '$allocation'";
}

$rows = $data->get_rows2($query);


fwrite($output, "\r\n");
// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    if($rows[$i]['action_type']==1){
        $rows[$i]['withdrawal_date'] = "";
        $rows[$i]['adjusted_interest_rate'] = "";
        $rows[$i]['withdrawal_principle'] = "";
        $rows[$i]['withdrawal_interest'] = "";
        $rows[$i]['total_withdrawal_amount'] = "";

        $rows[$i]['rollover_type'] = "";
        $rows[$i]['new_principle_amount'] = "";
        $rows[$i]['new_interest_rate'] = "";
        $rows[$i]['new_maturity_date'] = "";
        
        $rows[$i]['parent_reference_no'] = "";

    }else if($rows[$i]['action_type']==2){
        $rows[$i]['interest_frequency'] = "";
        $rows[$i]['interest_type'] = "";
        $rows[$i]['sharia_deposit'] = "";

        $rows[$i]['rollover_type'] = "";
        $rows[$i]['new_principle_amount'] = "";
        $rows[$i]['new_interest_rate'] = "";
        $rows[$i]['new_maturity_date'] = "";

    }else{
        $rows[$i]['interest_type'] = "";
        $rows[$i]['sharia_deposit'] = "";
        $rows[$i]['withdrawal_date'] = "";
        $rows[$i]['adjusted_interest_rate'] = "";
        $rows[$i]['withdrawal_principle'] = "";
        $rows[$i]['withdrawal_interest'] = "";
        $rows[$i]['total_withdrawal_amount'] = "";
    }

    if($rows[$i]['transaction_status']=="NEWM"){
        $rows[$i]['cancellation_reason'] = "";
    }

    
    $str = $rows[$i]['transaction_status']."|".$rows[$i]['action_type']
        ."|".$rows[$i]['im_code']
        ."|".$rows[$i]['fund_code']
        ."|".$rows[$i]['placement_bank_code']
        ."|".$rows[$i]['branch_code']
        ."|".$rows[$i]['placement_bank_cash_name']
        ."|".$rows[$i]['placement_bank_cash_no']
        ."|".$rows[$i]['ccy']
        ."|".$rows[$i]['principle']
        ."|".$rows[$i]['interest_rate']
        ."|".$rows[$i]['placement_date']
        ."|".$rows[$i]['maturity_date']
        ."|".$rows[$i]['interest_frequency']
        ."|".$rows[$i]['interest_type']
        ."|".$rows[$i]['sharia_deposit']
        ."|".$rows[$i]['withdrawal_date']
        ."|".$rows[$i]['adjusted_interest_rate']
        ."|".$rows[$i]['withdrawal_principle']
        ."|".$rows[$i]['withdrawal_interest']
        ."|".$rows[$i]['total_withdrawal_amount']
        ."|".$rows[$i]['rollover_type']
        ."|".$rows[$i]['new_principle_amount']
        ."|".$rows[$i]['new_interest_rate']
        ."|".$rows[$i]['new_maturity_date']
        ."|".$rows[$i]['amount_to_be_transferred']
        ."|".$rows[$i]['statutory_type']
        ."|".$rows[$i]['contact_person']
        ."|".$rows[$i]['telephone_no']
        ."|".$rows[$i]['fax_no']
        ."|".$rows[$i]['reference_no']
        ."|".$rows[$i]['parent_reference_no']
        ."|".$rows[$i]['description']
        ."|".$rows[$i]['cancellation_reason']
        ."\r\n";

    fwrite($output, $str);
    //fputcsv($output, $rows[$i]);
}

fclose($output);
} catch(Exception $e){
    echo $e->getMessage();
}
?>
