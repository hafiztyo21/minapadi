<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('distributed_income_im_edit.html');

$id = 0;
$data_type = '1';
$fundCode = '';
$cumDate = date('Y-m-d');
$exDate = date('Y-m-d');
$perUnit = '0';
$policy	= '1';
$paymentDate = date('Y-m-d');
$no	= '';
$errorArr = array('','','','','','','');
$otherError='';

if($_GET['edit']==1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_dist_inc_im WHERE dist_inc_im_id = $id";
    $result = $data->get_row($query);
	$data_type = $result['data_type'];
    $fundCode = $result['fund_code'];
    $cumDate = $result['cum_date'];
    $exDate = $result['ex_date'];
    $perUnit = $result['dist_inc_per_unit'];
    $policy = $result['dist_inc_policy'];
    $paymentDate = $result['payment_date'];
    $no = $result['dist_inc_no'];
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
		$data_type = trim(htmlentities($_POST['data_type']));
 		$fundCode = trim(htmlentities($_POST['inputFundCode']));
		$cumDate = trim(htmlentities($_POST['inputCumDate']));
		$exDate = trim(htmlentities($_POST['inputExDate']));
		$perUnit = trim(htmlentities($_POST['inputPerUnit']));
		$policy	= trim(htmlentities($_POST['inputPolicy']));
        $paymentDate = trim(htmlentities($_POST['inputPaymentDate']));
        $no	= trim(htmlentities($_POST['inputNo']));
		
		$gotError = false;
		if($fundCode==''){
			$errorArr[1] = "Fund Code must be filled";
			$gotError = true;
		}
		if($data_type == 1 && $perUnit == ''){
			$errorArr[4] = "Distributed Income Per Unit must be filled if Data Type is Confirmed";
			$gotError = true;
		}
		
        
		if (!$gotError){
			
			$query = "UPDATE tbl_kr_dist_inc_im SET
					fund_code = '$fundCode',
					cum_date = '$cumDate',
					ex_date = '$exDate',
					dist_inc_per_unit = '$perUnit',
					dist_inc_policy = '$policy',
					payment_date = '$paymentDate',
					dist_inc_no = '$no',
					last_updated_time = now(),
					last_updated_by = '".$_SESSION['pk_id']."'
				WHERE dist_inc_im_id = '$id'";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Edit Success');window.location='distributed_income_im.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}



$tittle = "EDIT - DISTRIBUTED INCOME DATA";
$dataRows = array (
	    'TEXT' => array('Data Type <span class="redstar">*</span>','Fund Code <span class="redstar">*</span>','Cum-Date <span class="redstar">*</span>','Ex-Date <span class="redstar">*</span>','Dist. Income per Unit <span class="redstar">*</span>','Dist. Income Policy <span class="redstar">*</span>','Payment Date <span class="redstar">*</span>', 'Dist. Income No.'),
  	    'DOT'  => array (':',':',':',':',':',':',':'),
	    'FIELD' => array (
			$data->cb_distIncDataType('data_type', $data_type,''),
            $data->cb_fundcode('inputFundCode', $fundCode, '')."<input type=hidden name=inputId value=$id>",
		    $data->datePicker('inputCumDate', $cumDate,''),
            $data->datePicker('inputExDate', $exDate,''),
		    "<input type=number step=1 size='50' name=inputPerUnit value='$perUnit'>",
		    "<select name=inputPolicy><option value=1 ". ($policy == 1 ? "selected=selected" : "") .">Cash</option><option value=2 ".($policy == 2 ? "selected=selected" : "").">Reinvesment</option><option value=3 ".($policy == 3 ? "selected=selected" : "").">Optional</option>",
		    $data->datePicker('inputPaymentDate', $paymentDate,''),
		    "<input type=text size='50' name=inputNo value='$no'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='distributed_income_im.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');

?>