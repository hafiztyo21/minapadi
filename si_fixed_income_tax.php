<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'si.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new si;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('si_fixed_income_tax.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_si_fixed_income_tax.si_fixed_income_tax_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

//$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$linkAdd = 'si_fixed_income_tax_form.php';
//$linkExport = 'si_fixed_income_unregistered_export.php';
$tablename = 'tbl_kr_si_fixed_income_tax';

$headerid = $_GET['headerid'];
//$btnView = '<input type="submit" name="btnView" value="View">';
$btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'?headerid='.$headerid.'\'">';
//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="alert(\'Please choose transaction date first\');">';

$datagrid = array();

if ($_GET['detail']==1){
  	/*$transaction_date =  trim(htmlentities($_POST['transactionDate']));
	if (empty($transaction_date))
		$transaction_date = date("Y-m-d");*/

	//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';
	
  	$sql = "SELECT 
	  			si_fixed_income_tax_id
				, B.trade_id as TRADEID
				, $tablename.face_value as FACEVALUE
				, acquisition_date as ACQUISITIONDATE
				, acquisition_price_persen as ACQUISITIONPRICEPERSEN
				, acquisition_amount as ACQUISITIONAMOUNT
				, capital_gain as CAPITALGAIN
                , days_of_holding_interest as DAYSOFHOLDINGINTEREST
                , holding_interest_amount as HOLDINGINTERESTAMOUNT
                , total_taxable_income as TOTALTAXABLEINCOME
                , tax_rate_in_persen as TAXRATEINPERSEN
                , tax_amount as TAXAMOUNT
			FROM $tablename LEFT JOIN tbl_kr_si_fixed_income B ON $tablename.si_fixed_income_id = B.si_fixed_income_id
			WHERE $tablename.is_deleted=0 AND $tablename.si_fixed_income_id= $headerid
			ORDER BY $order_by $sort_order";
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->siDataTax($sql, 'edit', 'si_fixed_income_tax_form.php', 'delete', 'si_fixed_income_tax.php', 'si_fixed_income_tax_id', $headerid);
}

if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "UPDATE $tablename SET is_deleted=1 WHERE si_fixed_income_tax_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='si_fixed_income_tax.php?detail=1&headerid=$headerid';</script>";
	}
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>