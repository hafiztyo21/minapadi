<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('contact_add.html');
$tablename = 'tbl_kr_cus_sup';
$tablenameDetail = 'tbl_kr_cus_sup_detail';
$tablenameHistory = 'tbl_kr_kyc_individual_history';

########################################

#######################################

if ($_POST['btn_save']){
	#print_r($_POST);
	$name = trim(htmlentities($_POST['txt_cus_name']));
	$sql = "";
	$query2="";
	$gotError = false;
	$errorMessage = '';
	if ($name!=''){
		$country = trim(htmlentities($_POST['txt_country']));
		if($country == ''){
			$errorMessage .= "NEGARA KEWARGANEGARAAN must be filled\\n";
			$gotError = true;
		}

		$idno = trim(htmlentities($_POST['txt_no']));
		if($idno == ''){
			$errorMessage .= "NOMOR IDENTITAS must be filled\\n";
			$gotError = true;
		}
		$placeofbirth = trim(htmlentities($_POST['txt_tmp_lahir']));
		if($placeofbirth == ''){
			$errorMessage .= "TEMPAT LAHIR must be filled\\n";
			$gotError = true;
		}
		$gender = trim(htmlentities($_POST['txt_cus_jnskel']));
		if($gender == ''){
			$errorMessage .= "JENIS KELAMIN must be filled\\n";
			$gotError = true;
		}
		$education = trim(htmlentities($_POST['txt_pendidikan']));
		if($education == ''){
			$errorMessage .= "PENDIDIKAN must be filled\\n";
			$gotError = true;
		}
		$religion = trim(htmlentities($_POST['txt_agama']));
		if($religion == ''){
			$errorMessage .= "AGAMA must be filled\\n";
			$gotError = true;
		}
		$occupation = trim(htmlentities($_POST['txt_pekerjaan']));
		if($occupation == ''){
			$errorMessage .= "PEKERJAAN must be filled\\n";
			$gotError = true;
		}
		$incomelevel = trim(htmlentities($_POST['txt_cus_penghasilan']));
		if($incomelevel == ''){
			$errorMessage .= "PENGHASILAN must be filled\\n";
			$gotError = true;
		}
		$marital = trim(htmlentities($_POST['txt_status_kawin']));
		if($marital == ''){
			$errorMessage .= "STATUS PERKAWINAN must be filled\\n";
			$gotError = true;
		}
		$objective = trim(htmlentities($_POST['txt_cus_tujuan']));
		if($objective == ''){
			$errorMessage .= "MAKSUD DAN TUJUAN INVESTASI must be filled\\n";
			$gotError = true;
		}
		$source = trim(htmlentities($_POST['txt_cus_sumber_dana']));
		if($source == ''){
			$errorMessage .= "SUMBER PENGHASILAN must be filled\\n";
			$gotError = true;
		}
		$ktpaddress = trim(htmlentities($_POST['txt_address']));
		if( $country=='ID' && $ktpaddress == ''){
			$errorMessage .= "ALAMAT SESUAI IDENTITAS must be filled\\n";
			$gotError = true;
		}
		$kantoraddress = trim(htmlentities($_POST['txt_address_tinggal']));
		if( $kantoraddress == ''){
			$errorMessage .= "ALAMAT TMPT TINGGAL must be filled\\n";
			$gotError = true;
		}
		$ascorrespondence = 0;
		if(isset($_POST['ascorrespondence'])){
			$ascorrespondence = $_POST['ascorrespondence'];
		}
		
		$cek_ktp = $data->get_row("select * from tbl_kr_cus_sup where cus_no_identity='".$_POST['txt_no']."'");	
		$no_ktp=$_POST['txt_no'];
		if($cek_ktp['cus_no_identity']!=$no_ktp){
			$sql = "INSERT INTO $tablename(
					cus_code,
					cus_tgl_subscribe,
					cus_name,
					cus_card_identity,
					cus_no_identity,
					
					cus_berlaku_card,
					cus_status_identity,
					cus_tempat_lahir,
					
					
					cus_tgl_lahir,
					cus_sex,
					cus_mother_name,
					cus_agama,
					cus_national,
					
					cus_status_domisili,
					cus_status_kawin,
					cus_jumlah_tanggungan,
					cus_address,
					cus_city,
					
					cus_kode_pos,
					cus_propinsi,
					cus_telp,
					cus_phone,
					cus_fax,
					
					cus_email,
					cus_address_t,
					cus_city_t,
					cus_kode_pos_t,
					cus_propinsi_t,
					
					cus_status_rt,
					cus_menempati_t,
					cus_pendidikan,
					cus_pekerjaan,
					cus_company_name,
					
					cus_bd_usaha,
					cus_jabatan,
					cus_address_c,
					cus_city_c,
					cus_kode_pos_c,
					
					cus_propinsi_c,
					cus_telp_c,
					cus_phone_c,
					cus_fax_c,
					cus_email_c,
					cus_masa_usaha,
					cus_other,
					
					cus_actived,
					cus_no_npwp_1,
					cus_no_npwp_2,
					cus_no_npwp_3,
					cus_no_npwp_4,
					
					cus_no_npwp_5,
					cus_no_npwp_6,
					cus_bank_name,
					cus_account_name,
					cus_account_number,
					
					cus_bank_name2,
					cus_account_name2,
					cus_account_number2,
					cus_sumber_dana,
					cus_maksud_tujuan,
					cus_penghasilan,
					cus_sid,
					middle_name
					, last_name
					, country_of_nationality
					, npwp_regis_date

					, country_of_birth
					, spouses_name
					, investors_risk_profile
					, asset_owner
					, bank_bic_code1

					, bank_bi_member_code1
					, bank_country1
					, bank_branch1
					, bank_ccy1
					, bank_bic_code2

					, bank_bi_member_code2
					, bank_country2
					, bank_branch2
					, bank_ccy2
					, bank_bic_code3

					, bank_bi_member_code3
					, bank_name3
					, bank_country3
					, bank_branch3
					, bank_ccy3

					, bank_acc_no3
					, bank_acc_name3
					, statement_type
					, fatca
					, tin
					, tin_issuance_country
					, country_of_correspondence
					, country_of_domicily
					, client_code
					, created_date
					, correspondence_type
				)VALUES(
					'".$_POST['txt_cus_id']."',
					'".$_POST['txt_tgl_subscribe']."',
					'".$_POST['txt_cus_name']."',
					'".$_POST['txt_cus_kartu']."',
					'".$_POST['txt_no']."',
					'".$_POST['txt_tgl_berlaku']."',
					'".$_POST['txt_status_ktp']."',
					'".$_POST['txt_tmp_lahir']."',
					
					'".$_POST['txt_tgl_lahir']."',
					'".$_POST['txt_cus_jnskel']."',
					'".$_POST['txt_ibu']."',
					'".$_POST['txt_agama']."',
					'".$_POST['txt_kewarganegaraan']."',
					
					'".$_POST['txt_domisili']."',
					'".$_POST['txt_status_kawin']."',
					'".$_POST['txt_jml_tanggungan']."',
					'".$_POST['txt_address']."',
					'".$_POST['txt_city']."',
					
					'".$_POST['txt_kode']."',
					'".$_POST['txt_propinsi']."',
					'".$_POST['txt_telp']."',
					'".$_POST['txt_phone']."',
					'".$_POST['txt_fax']."',
					
					'".$_POST['txt_email']."',
					'".$_POST['txt_address_tinggal']."',
					'".$_POST['txt_city_tinggal']."',
					'".$_POST['txt_kode_tinggal']."',
					'".$_POST['txt_propinsi_tinggal']."',
					
					'".$_POST['txt_status_rt']."',
					'".$_POST['txt_menempati']."',
					'".$_POST['txt_pendidikan']."',
					'".$_POST['txt_pekerjaan']."',
					'".$_POST['txt_nm_perusahaan']."',
					
					'".$_POST['txt_bd_usaha']."',
					'".$_POST['txt_jabatan']."',
					'".$_POST['txt_address_kantor']."',
					'".$_POST['txt_city_kantor']."',
					'".$_POST['txt_kode_kantor']."',
					
					'".$_POST['txt_propinsi_kantor']."',
					'".$_POST['txt_telp_kantor']."',
					'".$_POST['txt_phone_kantor']."',
					'".$_POST['txt_fax_kantor']."',
					'".$_POST['txt_email_kantor']."',
					'".$_POST['txt_lama_kerja']."',
					'".$_POST['txt_other']."',
					
					'".$_POST['txt_cus_active']."',
					'".$_POST['txt_npwp1']."',
					'".$_POST['txt_npwp2']."',
					'".$_POST['txt_npwp3']."',
					'".$_POST['txt_npwp4']."',
					
					'".$_POST['txt_npwp5']."',
					'".$_POST['txt_npwp6']."',
					'".$_POST['txt_name_bank']."',
					'".$_POST['txt_account_name']."',
					'".$_POST['txt_account_number']."',
					
					'".$_POST['txt_name_bank1']."',
					'".$_POST['txt_account_name1']."',
					'".$_POST['txt_account_number1']."',
					
					'".$_POST['txt_cus_sumber_dana']."',
					'".$_POST['txt_cus_tujuan']."',
					'".$_POST['txt_cus_penghasilan']."',
					'".$_POST['txt_cus_sid']."'

					, '".$_POST['txt_middle_name']."'
					, '".$_POST['txt_last_name']."'
					, '".$_POST['txt_country']."'
					, '".$_POST['txt_tgl_npwp']."'

					, '".$_POST['txt_country_birth']."'
					, '".$_POST['txt_spousename']."'
					, '".$_POST['txt_risk_profile']."'
					, '".$_POST['txt_assetowner']."'
					, '".$_POST['txt_bankbiccode']."'

					, '".$_POST['txt_bankbimembercode']."'
					, '".$_POST['txt_bankcountry']."'
					, '".$_POST['txt_bankbranch']."'
					, '".$_POST['txt_accccy']."'
					, '".$_POST['txt_bankbiccode1']."'

					, '".$_POST['txt_bankbimembercode1']."'
					, '".$_POST['txt_bankcountry1']."'
					, '".$_POST['txt_bankbranch1']."'
					, '".$_POST['txt_accccy1']."'
					, '".$_POST['txt_bankbiccode2']."'

					, '".$_POST['txt_bankbimembercode2']."'
					, '".$_POST['txt_name_bank2']."'
					, '".$_POST['txt_bankcountry2']."'
					, '".$_POST['txt_bankbranch2']."'
					, '".$_POST['txt_accccy2']."'

					, '".$_POST['txt_account_number2']."'
					, '".$_POST['txt_account_name2']."'
					, '".$_POST['txt_statementtype']."'
					, '".$_POST['txt_fatca']."'
					, '".$_POST['txt_tin']."'
					, '".$_POST['txt_tincountry']."'
					, '".$_POST['country_of_correspondence']."'
					, '".$_POST['country_of_domicily']."'
					, '".$_POST['client_code']."'
					, now()
					, '".$ascorrespondence."'
				)";
		}else{
			echo "<script>alert('Duplicate Customer Identity Number!');</script>";
		}
	}else{
		echo "<script>alert('Name is Empty!');</script>";
	};
   
   	if($gotError){
		   echo "<script>alert('".$errorMessage."Save Failed.');</script>";
	   }else{
		   if($data->inpQueryReturnBool($sql)){
				echo "<script>alert('".$data->err_report('s01')."');opener.document.form1.submit();window.parent.close();</script>";
			}else{
				//echo $data->queryError();
				echo "<script>alert('".$data->err_report('s02')."');</script>";
			}
	   }

	

}

if ($_GET['add'] == 1){
	#$data->auth('09030101',$_SESSION['user_id']);
	#$value_kode=$data->get_value("select max(kode_akses) from tbl_upx_akses");
	#$new_id = $value_kode+1;
	#print_r($_POST);
	$date = $_POST[txt_tgl_subscribe];
	$pecah1 = explode("-", $date);
	$date1 = $pecah1[2];
	$month = $pecah1[1];
	$year = $pecah1[0];
	$name = $_POST['txt_cus_name'];
	$pecah_name = substr($name, 0,1);
	$pecah_year = substr($year,2,2);
	$id = "$pecah_name$pecah_year$month ";
	//print($_POST[txt_tgl_berlaku]);
	//echo $id;
	$cek_data=$data->get_value("select count(cus_id) from tbl_kr_cus_sup where left(cus_code,5) ='".$id."'");
	$cek_data = $cek_data + 1;
	if ($cek_data < 10 ){
		$no_auto= "0$cek_data";
		//print($no_auto);
	}
	else
	{
		$no_auto=$cek_data;
	}
	$id2 = "$pecah_name$pecah_year$month$no_auto";
	//print($id2);
	$dataRows = array (
				'TEXT' => array(
					'CUSTOMER ID / CLIENT NO',
					'TANGGAL SUBSCRIBE',
					'NAMA DEPAN <span class="redstar">*</span>',
					'NAMA TENGAH',
					'NAMA BELAKANG',

					'JENIS KARTU IDENTITAS',
					'NOMOR IDENTITAS <span class="redstar">*</span>',
					'NEGARA LAHIR',
					'TEMPAT LAHIR <span class="redstar">*</span>',
					'NPWP',

					'TGL PENDAFTARAN NPWP',
					'JENIS KELAMIN <span class="redstar">*</span>',
					'NAMA GADIS IBU KANDUNG',
					'AGAMA <span class="redstar">*</span>',
					'NEGARA KEWARGANEGARAAN <span class="redstar">*</span>',

					'KEWARGANEGARAAN',
					'STATUS PERKAWINAN <span class="redstar">*</span>',
					'NAMA PASANGAN',
					'JUMLAH TANGGUNGAN',
					'ALAMAT SESUAI IDENTITAS <span class="redstar">*</span>','',

					'TELEPON RUMAH',
					'FAX',
					'ALAMAT TEMPAT TINGGAL <span class="redstar">*</span>','',
					'NEGARA TEMPAT TINGGAL',
					'STATUS RUMAH TINGGAL',
					'MENEMPATI SEJAK',
					'PENDIDIKAN <span class="redstar">*</span>'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
					"<input type=text name=txt_cus_id id=txt_cus_id value='".$_POST[txt_cus_id]."'>",
					$data->datePicker('txt_tgl_subscribe', $_POST[txt_tgl_subscribe],''),
					"<input type=text name=txt_cus_name id=txt_cus_name  value='".$_POST['txt_cus_name']."'>",
					"<input type=text name=txt_middle_name id=txt_middle_name  value='".$_POST['txt_middle_name']."'>",
					"<input type=text name=txt_last_name id=txt_last_name  value='".$_POST['txt_last_name']."'>",

					$data->cb_kartu_identitas('txt_cus_kartu',$_POST[txt_cus_kartu]),
					"<input type=text name=txt_no id=txt_no value='".$_POST['txt_no']."'>".$data->cb_ktp('txt_status_ktp',$_POST[txt_status_ktp], 'onchange="changeSeumurHidup(this.value)"')." <br>BERLAKU S/D".$data->datePicker('txt_tgl_berlaku', $_POST[txt_tgl_berlaku],''),
					$data->cb_isocountry('txt_country_birth',$_POST[txt_country_birth]),
					"<input type=text name=txt_tmp_lahir id=txt_tmp_lahir value='".$_POST['txt_tmp_lahir']."'><br> TANGGAL LAHIR".$data->datePicker('txt_tgl_lahir', $_POST[txt_tgl_lahir],''),
					"<input type=text name=txt_npwp1 id=txt_npwp1 size='2' onKeyPress='return checkIt(event)' maxlength='2' onKeyUp='valid_length1()' value='".$_POST['txt_npwp1']."'>.
					<input type=text name=txt_npwp2 id=txt_npwp2 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length2()' value='".$_POST['txt_npwp2']."'>.
					<input type=text name=txt_npwp3 id=txt_npwp3 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length3()' value='".$_POST['txt_npwp3']."'>.
					<input type=text name=txt_npwp4 id=txt_npwp4 size='2'  onKeyPress='return checkIt(event)' maxlength='1' onKeyUp='valid_length4()' value='".$_POST['txt_npwp4']."'>.
					<input type=text name=txt_npwp5 id=txt_npwp5 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length5()' value='".$_POST['txt_npwp5']."'>.
					<input type=text name=txt_npwp6 id=txt_npwp6 size='5'  onKeyPress='return checkIt(event)' maxlength='3' value='".$_POST['txt_npwp6']."'>",

					$data->datePicker('txt_tgl_npwp', $_POST[txt_tgl_npwp],''),
					$data->cb_jns_kelamin('txt_cus_jnskel',$_POST[txt_cus_jnskel]),
					"<input type=text name=txt_ibu id=txt_ibu value='".$_POST['txt_ibu']."'>",
					$data->cb_religion('txt_agama',$_POST[txt_agama]),
					$data->cb_isocountry('txt_country',$_POST[txt_country]),

					$data->cb_kewarganegaraan('txt_kewarganegaraan',$_POST[txt_kewarganegaraan])."Status Domisili".$data->cb_status_domisili('txt_domisili',$_POST[txt_domisili]),
					$data->cb_status_perkawinan('txt_status_kawin',$_POST[txt_status_kawin]),
					"<input type=text name=txt_spousename id=txt_spousename value='".$_POST['txt_spousename']."'>",
					"<input type=text name=txt_jml_tanggungan id=txt_jml_tanggungan value='".$_POST['txt_jml_tanggungan']."'>",
					"<textarea rows=2 cols=27 name=txt_address>".$_POST['txt_address']."</textarea>",
					"KOTA : ".$data->cb_kota('txt_city',$_POST[txt_city])."
					KODE POS : <input type=text name=txt_kode id=txt_kode value='".$_POST['txt_kode']."'>
					PROPINSI : <input type=text name=txt_propinsi id=txt_propinsi value='".$_POST['txt_propinsi']."'>",

					"<input type=text name=txt_telp id=txt_telp value='".$_POST['txt_telp']."'> HANDPHONE :<input type=text name=txt_phone id=txt_phone value='".$_POST['txt_phone']."'>",
					"<input type=text name=txt_fax id=txt_fax value='".$_POST['txt_fax']."'> EMAIL :<input type=text name=txt_email id=txt_email value='".$_POST['txt_email']."'>",
					"<textarea rows=2 cols=27 name=txt_address_tinggal>".$_POST['txt_address_tinggal']."</textarea>",
					"KOTA : ".$data->cb_kota('txt_city_tinggal',$_POST[txt_city_tinggal])."
					KODE POS : <input type=text name=txt_kode_tinggal id=txt_kode_tinggal value='".$_POST['txt_kode_tinggal']."'>
					PROPINSI : <input type=text name=txt_propinsi_tinggal id=txt_propinsi_tinggal value='".$_POST['txt_propinsi_tinggal']."'>",
					$data->cb_isocountry('country_of_domicily', $_POST['country_of_domicily'],''),
					$data->cb_status_rt('txt_status_rt',$_POST[txt_status_rt]),
					"<input type=text name=txt_menempati id=txt_menempati value='".$_POST['txt_menempati']."'>",
					$data->cb_pendidikan('txt_pendidikan',$_POST[txt_pendidikan]),
				)
			);
	$dataRows2 = array (
				'TEXT' => array(
					'PEKERJAAN <span class="redstar">*</span>',
					'NAMA PERUSAHAAN/KANTOR',
					'KEGIATAN / BIDANG USAHA',
					'ALAMAT PERUSAHAAN/KANTOR ',
					'',
					'NEGARA PERUSAHAAN',
					'NO. TELP. KANTOR',
					'FAX',
					'LAMA BEKERJA/USAHA',
					'ALAMAT KANTOR SEBAGAI KORESPONDEN',
				),
				'DOT'  => array (':',':',':',':',':',':',':'),
				'FIELD' => array (
					$data->cb_pekerjaan('txt_pekerjaan',$_POST[txt_pekerjaan]),
					"<input type=text name=txt_nm_perusahaan id=txt_nm_perusahaan value='".$_POST['txt_nm_perusahaan']."'>",				
					"<input type=text name=txt_bd_usaha id=txt_bd_usaha value='".$_POST['txt_bd_usaha']."'> POSISI/JABATAN : <input type=text name=txt_jabatan id=txt_jabatan value='".$_POST['txt_jabatan']."'>",
					"<textarea rows=2 cols=30 name=txt_address_kantor>".$_POST['txt_address_kantor']."</textarea>",
					"KOTA : ".$data->cb_kota('txt_city_kantor',$_POST[txt_city_kantor])."
					KODE POS : <input type=text name=txt_kode_kantor id=txt_kode_kantor value='".$_POST['txt_kode_kantor']."'>
					PROPINSI : <input type=text name=txt_propinsi_kantor id=txt_propinsi_kantor value='".$_POST['txt_propinsi_kantor']."'>",
					$data->cb_isocountry('country_of_correspondence', $_POST['country_of_correspondence'],''),
					"<input type=text name=txt_telp_kantor id=txt_telp_kantor value='".$_POST['txt_telp_kantor']."'> HANDPHONE :<input type=text name=txt_phone_kantor id=txt_phone_kantor value='".$_POST['txt_phone_kantor']."'>",
					"<input type=text name=txt_fax_kantor id=txt_fax_kantor value='".$_POST['txt_fax_kantor']."'> EMAIL :<input type=text name=txt_email_kantor id=txt_email_kantor value='".$_POST['txt_email']."'>",
					"<input type=text name=txt_lama_kerja id=txt_lama_kerja value='".$_POST['txt_lama_kerja']."'>",
					"<input type=checkbox name=ascorrespondence id=ascorrespondence value='1' >",
				)
			);		  
					  
	$dataRows3 = array (
				'TEXT' => array(
					'BANK BIC CODE', 'BANK BI MEMBER CODE','BANK <span class="redstar">*</span>', 'BANK COUNTRY <span class="redstar">*</span>', 'BANK BRANCH', 'ACCOUNT CCY','ACCOUNT NAME <span class="redstar">*</span>','ACCOUNT NUMBER <span class="redstar">*</span>','',
					'BANK BIC CODE 2', 'BANK BI MEMBER CODE 2', 'BANK 2 <span class="redstar">*</span>','BANK COUNTRY 2 <span class="redstar">*</span>', 'BANK BRANCH 2', 'ACCOUNT CCY 2','ACCOUNT NAME 2 <span class="redstar">*</span>','ACCOUNT NUMBER 2 <span class="redstar">*</span>','',
					'BANK BIC CODE 3', 'BANK BI MEMBER CODE 3', 'BANK 3 <span class="redstar">*</span>','BANK COUNTRY 3 <span class="redstar">*</span>', 'BANK BRANCH 3', 'ACCOUNT CCY 3','ACCOUNT NAME 3 <span class="redstar">*</span>','ACCOUNT NUMBER 3 <span class="redstar">*</span>','',
					'OTHER','CUSTOMER ACTIVED',
					'PENGHASILAN UTAMA/KOTOR/THN <span class="redstar">*</span>',
					'SUMBER PENGHASILAN <span class="redstar">*</span>',
					'ASSET OWNER',
					'MAKSUD DAN TUJUAN INVESTASI <span class="redstar">*</span>',
					'INVESTOR RISK PROFILE',
					'STATEMENT TYPE <span class="redstar">*</span>',
					'FATCA',
					'TIN/FOREIGN TIN',
					'TIN/FOREIGN TIN ISSUANCE COUNTRY',
					'NO SID',
					'CLIENT CODE'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':'),
				'FIELD' => array (
					"<input type=text name=txt_bankbiccode id=txt_bankbiccode value='".$_POST[bank_bic_code1]."'>",
					//"<input type=text name=txt_bankbimembercode id=txt_bankbimembercode value='".$_POST[bank_bi_member_code1]."'>",
					$data->cb_bimembercode('txt_bankbimembercode', $_POST[bank_bi_member_code1], 'onchange="changeBi1(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_name_bank id=txt_name_bank size=50 value='".$_POST[cus_bank_name]."'>",
					$data->cb_isocountry('txt_bankcountry',$_POST[bank_country1]),
					"<input type=text name=txt_bankbranch id=txt_bankbranch value='".$_POST[bank_branch1]."'>",
					$data->cb_accountccy('txt_accccy',$_POST[bank_ccy1]),
					"<input type=text name=txt_account_name id=txt_account_name value='".$_POST[cus_account_name]."'>",
					"<input type=text name=txt_account_number id=txt_account_number value='".$_POST[cus_account_number]."'>","",

					"<input type=text name=txt_bankbiccode1 id=txt_bankbiccode1 value='".$_POST[bank_bic_code2]."'>",
					//"<input type=text name=txt_bankbimembercode1 id=txt_bankbimembercode1 value='".$_POST[bank_bi_member_code2]."'>",
					$data->cb_bimembercode('txt_bankbimembercode1', $_POST[bank_bi_member_code2], 'onchange="changeBi2(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_name_bank1 id=txt_name_bank1 size=50 value='".$_POST[cus_bank_name2]."'>",
					$data->cb_isocountry('txt_bankcountry1',$_POST[bank_country2]),
					"<input type=text name=txt_bankbranch1 id=txt_bankbranch1 value='".$_POST[bank_branch2]."'>",
					$data->cb_accountccy('txt_accccy1',$_POST[bank_ccy2]),
					"<input type=text name=txt_account_name1 id=txt_account_name1 value='".$_POST[cus_account_name2]."'>",
					"<input type=text name=txt_account_number1 id=txt_account_number1 value='".$_POST[cus_account_number2]."'>","",
		
					"<input type=text name=txt_bankbiccode2 id=txt_bankbiccode2 value='".$_POST[bank_bic_code3]."'>",
					//"<input type=text name=txt_bankbimembercode2 id=txt_bankbimembercode2 value='".$_POST[bank_bi_member_code3]."'>",
					$data->cb_bimembercode('txt_bankbimembercode2', $_POST[bank_bi_member_code3], 'onchange="changeBi3(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_name_bank2 id=txt_name_bank2 size=50 value='".$_POST[cus_bank_name3]."'>",
					$data->cb_isocountry('txt_bankcountry2',$_POST[bank_country3]),
					"<input type=text name=txt_bankbranch2 id=txt_bankbranch2 value='".$_POST[bank_branch3]."'>",
					$data->cb_accountccy('txt_accccy2',$_POST[bank_ccy3]),
					"<input type=text name=txt_account_name2 id=txt_account_name2 value='".$_POST[cus_account_name3]."'>",
					"<input type=text name=txt_account_number2 id=txt_account_number2 value='".$_POST[cus_account_number3]."'>","",
					
					"<textarea rows=3 cols=17 name=txt_other>".$_POST[txt_other]."</textarea>",
					$data->cb_customer_status('txt_cus_active',$_POST[txt_cus_active]),
					$data->cb_penghasilan('txt_cus_penghasilan',$_POST[txt_cus_penghasilan]),
					$data->cb_sumber_dana('txt_cus_sumber_dana',$_POST[txt_cus_sumber_dana]),
					$data->cb_assetowner('txt_assetowner',$_POST[txt_assetowner]),
					$data->cb_tujuan('txt_cus_tujuan',$_POST[txt_cus_tujuan]),
					$data->cb_riskprofile('txt_risk_profile',$_POST[txt_risk_profile]),
					$data->cb_statementtype('txt_statementtype',$_POST[txt_statementtype]),
					$data->cb_fatca('txt_fatca',$_POST[txt_fatca]),
					"<input type=text name=txt_tin id=txt_tin value='".$_POST[txt_tin]."'>",
					$data->cb_isocountry('txt_tincountry',$_POST[txt_tincountry]),
					"<input type=text name=txt_cus_sid id=txt_cus_sid value='".$_POST[txt_cus_sid]."'>",
					"<input type=text maxlength=6 name=client_code id=client_code value='".$_POST[client_code]."'>"
				)
			);		  

    $tittle = "CUSTOMER / CLIENT ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"opener.document.form1.submit();window.parent.close();\">"
					);
}

if ($_GET['detail']==1){
	$query = "select * from ".$tablename." where cus_no_identity ='".$_GET['id']."'";
	$rows = $data->selectQuery($query);
	if($rows[cus_actived]==1){
		$act = "Active";
	}else{
		$act = "Non-Active";
	}
	if ($rows[cus_status_identity]==""){
		$masa_ktp = $rows[cus_berlaku_card];
	}
	else
	{
		$masa_ktp = $rows[cus_status_identity];
	}
	
	
	$addLink = "<a href='contact_rd_detail.php?add=1&id=".$_GET[id]."' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:400px')>ADD REKSADANA</a>";
	//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
	$tmpl->addVar('page','add',$addLink);
	$dataRows = array (
				'TEXT' => array(
				'CUSTOMER ID',
				'TANGGAL SUBSCRIBE',
				'NAMA SESUAI IDENTITAS',
				'JENIS KARTU IDENTITAS',
				'NOMOR IDENTITAS',
				'TEMPAT LAHIR',
				'NPWP',
				'JENIS KELAMIN',
				'NAMA GADIS IBU KANDUNG',
				'AGAMA',
				'KEWARGANEGARAAN',
				'STATUS PERKAWINAN',
				'JUMLAH TANGGUNGAN',
				'ALAMAT SESUAI IDENTITAS','',
				'TELEPON RUMAH',
				'FAX',
				'ALAMAT TEMPAT TINGGAL','',
				'STATUS RUMAH TINGGAL',
				'MENEMPATI SEJAK',
				'PENDIDIKAN'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				$rows[cus_code],
				$rows[cus_tgl_subscribe],
				$rows[cus_name],
				$rows[cus_card_identity],
				$rows[cus_no_identity]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BERLAKU S/D &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$masa_ktp,
				$rows[cus_tempat_lahir]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL LAHIR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_tgl_lahir],
				$rows[cus_no_npwp_1]."-"
				.$rows[cus_no_npwp_2]."-"
				.$rows[cus_no_npwp_3]."-"
				.$rows[cus_no_npwp_4]."-"
				.$rows[cus_no_npwp_5]."-"
				.$rows[cus_no_npwp_6]."-",
				$rows[cus_sex],
	
				$rows[cus_mother_name],
				$rows[cus_agama],
				$rows[cus_national]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status Domisili :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_status_domisili],
				$rows[cus_status_kawin],
				$rows[cus_jumlah_tanggungan],
				$rows[cus_address],
				"
				KOTA : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_city]."
				KODE POS :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$rows[cus_kode_pos]."
				PROPINSI : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_propinsi]."
				",
		
				$rows[cus_telp]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HANDPHONE  :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_phone],
	
				
				$rows[cus_fax]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; EMAIL :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_email],
				$rows[cus_address_t],
				"
				KOTA : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_city_t]."
				KODE POS :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_kode_pos_t]."
				PROPINSI : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_propinsi_t]."
				",
				$rows[cus_status_rt],
				$rows[cus_menempati_t],
				$rows[cus_pendidikan],
				
				)
          			  );
			$dataRows2 = array (
				'TEXT' => array('PEKERJAAN',
				'NAMA PERUSAHAAN/KANTOR',
				'KEGIATAN / BIDANG USAHA',
				
				'ALAMAT PERUSAHAAN/KANTOR',
				'',
				'NO. TELP. KANTOR',
				'FAX',
				'LAMA BEKERJA/USAHA',
				),
				'DOT'  => array (':',':',':',':',':',':',':'),
				'FIELD' => array (
				
				$rows[cus_pekerjaan],
				$rows[cus_company_name],
				
			$rows[cus_bd_usaha]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;POSISI/JABATAN : ".$rows[cus_jabatan],
				
				$rows[cus_address_c],
				"
				KOTA : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$rows[cus_city_c]."
				KODE POS : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$rows[cus_kode_pos_c]."
				PROPINSI : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$rows[cus_propinsi_c]
				,
				
				$rows[cus_telp_c]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HANDPHONE :".$rows[cus_phone_c],
	
				
				$rows[cus_fax_c]." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EMAIL ".$rows[cus_email_c],
				$rows[cus_masa_usaha],
				)
          			  );		  
					  
	
		
	
			$dataRows3 = array (
				'TEXT' => array(
				'BANK','ACCOUNT NAME','ACCOUNT NUMBER',
				'BANK 2','ACCOUNT NAME 2','ACCOUNT NUMBER 2',
				'OTHER','CUSTOMER ACTIVED',
				'PENGHASILAN UTAMA/KOTOR/THN',
				'SUMBER PENGHASILAN',
				'MAKSUD DAN TUJUAN INVESTASI'),
				'DOT'  => array (':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				"$rows[cus_account_name]",
				"$rows[cus_account_number]",
				"$rows[cus_bank_name]",
				"$rows[cus_bank_name2]",
				"$rows[cus_account_name2]",
				"$rows[cus_account_number2]",
	
				
				"$rows[cus_other]",
				$act,
				"$rows[cus_penghasilan]",
				"$rows[cus_sumber_dana]",
				
				"$rows[cus_maksud_tujuan]"
				//$data->cb_customer_status2('txt_cus_status2',$_POST[txt_cus_status2]),cus_other,
		
		
				)
          			  );		  


	$tittle = "CUSTOMER / CLIENT DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}

if ($_POST['btn_save_edit'])
{
	$gotError = false;
	$errorMessage = '';

	$name = trim(htmlentities($_POST['txt_cus_name']));
	if($name == ''){
		$errorMessage .= "NAMA CUSTOMER must be filled\\n";
		$gotError = true;
	}
	$country = trim(htmlentities($_POST['txt_country']));
	if($country == ''){
		$errorMessage .= "NEGARA KEWARGANEGARAAN must be filled\\n";
		$gotError = true;
	}

	$idno = trim(htmlentities($_POST['txt_no']));
	if($idno == ''){
		$errorMessage .= "NOMOR IDENTITAS must be filled\\n";
		$gotError = true;
	}
	$placeofbirth = trim(htmlentities($_POST['txt_tmp_lahir']));
	if($placeofbirth == ''){
		$errorMessage .= "TEMPAT LAHIR must be filled\\n";
		$gotError = true;
	}
	$gender = trim(htmlentities($_POST['txt_cus_jnskel']));
	if($gender == ''){
		$errorMessage .= "JENIS KELAMIN must be filled\\n";
		$gotError = true;
	}
	$education = trim(htmlentities($_POST['txt_pendidikan']));
	if($education == ''){
		$errorMessage .= "PENDIDIKAN must be filled\\n";
		$gotError = true;
	}
	$religion = trim(htmlentities($_POST['txt_agama']));
	if($religion == ''){
		$errorMessage .= "AGAMA must be filled\\n";
		$gotError = true;
	}
	$occupation = trim(htmlentities($_POST['txt_pekerjaan']));
	if($occupation == ''){
		$errorMessage .= "PEKERJAAN must be filled\\n";
		$gotError = true;
	}
	$incomelevel = trim(htmlentities($_POST['txt_cus_penghasilan']));
	if($incomelevel == ''){
		$errorMessage .= "PENGHASILAN must be filled\\n";
		$gotError = true;
	}
	$marital = trim(htmlentities($_POST['txt_status_kawin']));
	if($marital == ''){
		$errorMessage .= "STATUS PERKAWINAN must be filled\\n";
		$gotError = true;
	}
	$objective = trim(htmlentities($_POST['txt_cus_tujuan']));
	if($objective == ''){
		$errorMessage .= "MAKSUD DAN TUJUAN INVESTASI must be filled\\n";
		$gotError = true;
	}
	$source = trim(htmlentities($_POST['txt_cus_sumber_dana']));
	if($source == ''){
		$errorMessage .= "SUMBER PENGHASILAN must be filled\\n";
		$gotError = true;
	}
	$ktpaddress = trim(htmlentities($_POST['txt_address']));
	if( $country=='ID' && $ktpaddress == ''){
		$errorMessage .= "ALAMAT SESUAI IDENTITAS must be filled\\n";
		$gotError = true;
	}
	$kantoraddress = trim(htmlentities($_POST['txt_address_tinggal']));
	if( $kantoraddress == ''){
		$errorMessage .= "ALAMAT TMPT TINGGAL must be filled\\n";
		$gotError = true;
	}

	$ascorrespondence = 0;
	if(isset($_POST['ascorrespondence'])){
		$ascorrespondence = $_POST['ascorrespondence'];
	}

	$sql= "UPDATE ".$tablename." SET 
			cus_code = '".$_POST['txt_cus_id']."',
			cus_tgl_subscribe = '".$_POST['txt_tgl_subscribe']."',
			cus_name 	= '".$_POST['txt_cus_name']."',
			cus_card_identity='".$_POST['txt_cus_kartu']."',
			cus_no_identity='".$_POST['txt_no']."',
			cus_berlaku_card='".$_POST['txt_tgl_berlaku']."',
			cus_status_identity='".$_POST['txt_status_ktp']."',
			cus_tempat_lahir='".$_POST['txt_tmp_lahir']."',
		
	    cus_tgl_lahir=	'".$_POST['txt_tgl_lahir']."',
		cus_sex='".$_POST['txt_cus_jnskel']."',
		cus_mother_name='".$_POST['txt_ibu']."',
		cus_agama=	'".$_POST['txt_agama']."',
		cus_national='".$_POST['txt_kewarganegaraan']."',
		
		cus_status_domisili='".$_POST['txt_domisili']."',
		cus_status_kawin='".$_POST['txt_status_kawin']."',
		cus_jumlah_tanggungan='".$_POST['txt_jml_tanggungan']."',
		cus_address='".$_POST['txt_address']."',
		cus_city='".$_POST['txt_city']."',
		
		
		cus_kode_pos='".$_POST['txt_kode']."',
		cus_propinsi='".$_POST['txt_propinsi']."',
		cus_telp='".$_POST['txt_telp']."',
		cus_phone='".$_POST['txt_phone']."',
		cus_fax='".$_POST['txt_fax']."',
		
		cus_email=	'".$_POST['txt_email']."',
		cus_address_t='".$_POST['txt_address_tinggal']."',
		cus_city_t='".$_POST['txt_city_tinggal']."',
		cus_kode_pos_t='".$_POST['txt_kode_tinggal']."',
		cus_propinsi_t='".$_POST['txt_propinsi_tinggal']."',
		
		cus_status_rt='".$_POST['txt_status_rt']."',
		cus_menempati_t='".$_POST['txt_menempati']."',
		cus_pendidikan='".$_POST['txt_pendidikan']."',
		cus_pekerjaan='".$_POST['txt_pekerjaan']."',
		cus_company_name='".$_POST['txt_nm_perusahaan']."',
		
		cus_bd_usaha='".$_POST['txt_bd_usaha']."',
		cus_jabatan='".$_POST['txt_jabatan']."',
		cus_address_c='".$_POST['txt_address_kantor']."',
		cus_city_c='".$_POST['txt_city_kantor']."',
		cus_kode_pos_c='".$_POST['txt_kode_kantor']."',
		
		
		cus_propinsi_c='".$_POST['txt_propinsi_kantor']."',
		cus_telp_c='".$_POST['txt_telp_kantor']."',
		cus_phone_c='".$_POST['txt_phone_kantor']."',
		cus_fax_c='".$_POST['txt_fax_kantor']."',
		cus_email_c='".$_POST['txt_email_kantor']."',
		cus_masa_usaha='".$_POST['txt_lama_kerja']."',
		cus_other='".$_POST['txt_other']."',
		
		cus_actived=	'".$_POST['txt_cus_active']."',
		cus_no_npwp_1='".$_POST['txt_npwp1']."',
		cus_no_npwp_2='".$_POST['txt_npwp2']."',
		cus_no_npwp_3='".$_POST['txt_npwp3']."',
		cus_no_npwp_4='".$_POST['txt_npwp4']."',
		
		cus_no_npwp_5='".$_POST['txt_npwp5']."',
		cus_no_npwp_6='".$_POST['txt_npwp6']."',
		cus_bank_name='".$_POST['txt_name_bank']."',
		cus_account_name='".$_POST['txt_account_name']."',
		cus_account_number='".$_POST['txt_account_number']."',
		
		cus_bank_name2='".$_POST['txt_name_bank1']."',
		cus_account_name2='".$_POST['txt_account_name1']."',
		cus_account_number2='".$_POST['txt_account_number1']."',
		
		
		cus_penghasilan='".$_POST['txt_cus_penghasilan']."',
		cus_sumber_dana='".$_POST['txt_cus_sumber_dana']."',
		cus_maksud_tujuan='".$_POST['txt_cus_tujuan']."',

		middle_name = '".$_POST['txt_middle_name']."',
		last_name = '".$_POST['txt_last_name']."',
		country_of_nationality = '".$_POST['txt_country']."',
		npwp_regis_date = '".$_POST['txt_tgl_npwp']."',
		country_of_birth = '".$_POST['txt_country_birth']."',
		spouses_name = '".$_POST['txt_spousename']."',
		investors_risk_profile = '".$_POST['txt_risk_profile']."',
		asset_owner = '".$_POST['txt_assetowner']."',
		bank_bic_code1 = '".$_POST['txt_bankbiccode']."',
		bank_bi_member_code1 = '".$_POST['txt_bankbimembercode']."',
		bank_country1 = '".$_POST['txt_bankcountry']."',
		bank_branch1 = '".$_POST['txt_bankbranch']."',
		bank_ccy1 = '".$_POST['txt_accccy']."',
		bank_bic_code2 = '".$_POST['txt_bankbiccode1']."',
		bank_bi_member_code2 = '".$_POST['txt_bankbimembercode1']."',
		bank_country2 = '".$_POST['txt_bankcountry1']."',
		bank_branch2 = '".$_POST['txt_bankbranch1']."',
		bank_ccy2 = '".$_POST['txt_accccy1']."',
		bank_bic_code3 = '".$_POST['txt_bankbiccode2']."',
		bank_bi_member_code3 = '".$_POST['txt_bankbimembercode2']."',
		bank_name3 = '".$_POST['txt_name_bank2']."',
		bank_country3 = '".$_POST['txt_bankcountry2']."',
		bank_branch3 = '".$_POST['txt_bankbranch2']."',
		bank_ccy3 = '".$_POST['txt_accccy2']."',
		bank_acc_no3 = '".$_POST['txt_account_number2']."',
		bank_acc_name3 = '".$_POST['txt_account_name2']."',
		statement_type = '".$_POST['txt_statementtype']."',
		fatca = '".$_POST['txt_fatca']."',
		tin = '".$_POST['txt_tin']."',
		tin_issuance_country = '".$_POST['txt_tincountry']."',
		country_of_correspondence = '".$_POST['country_of_correspondence']."',
		country_of_domicily = '".$_POST['country_of_domicily']."',
		client_code = '".$_POST['client_code']."',
		correspondence_type = '".$ascorrespondence."'
			WHERE cus_no_identity = '".$_GET['id']."'";
		//print($sql);die();
   # $data->showsql($sql);
   /*$query = "SELECT * FROM $tablenameDetail WHERE cust_id = '".$_POST['inputId']."'";
   if($data->queryReturnExist($query)){
	   $query2 = "UPDATE $tablenameDetail SET
			middle_name = '".$_POST['txt_middle_name']."',
			last_name = '".$_POST['txt_last_name']."',
			country_of_nationality = '".$_POST['txt_country']."',
			npwp_regis_date = '".$_POST['txt_tgl_npwp']."',
			country_of_birth = '".$_POST['txt_country_birth']."',
			spouses_name = '".$_POST['txt_spousename']."',
			investors_risk_profile = '".$_POST['txt_risk_profile']."',
			asset_owner = '".$_POST['txt_assetowner']."',
			bank_bic_code1 = '".$_POST['txt_bankbiccode']."',
			bank_bi_member_code1 = '".$_POST['txt_bankbimembercode']."',
			bank_country1 = '".$_POST['txt_bankcountry']."',
			bank_branch1 = '".$_POST['txt_bankbranch']."',
			bank_ccy1 = '".$_POST['txt_accccy']."',
			bank_bic_code2 = '".$_POST['txt_bankbiccode1']."',
			bank_bi_member_code2 = '".$_POST['txt_bankbimembercode1']."',
			bank_country2 = '".$_POST['txt_bankcountry1']."',
			bank_branch2 = '".$_POST['txt_bankbranch1']."',
			bank_ccy2 = '".$_POST['txt_accccy1']."',
			bank_bic_code3 = '".$_POST['txt_bankbiccode2']."',
			bank_bi_member_code3 = '".$_POST['txt_bankbimembercode2']."',
			bank_name3 = '".$_POST['txt_name_bank2']."',
			bank_country3 = '".$_POST['txt_bankcountry2']."',
			bank_branch3 = '".$_POST['txt_bankbranch2']."',
			bank_ccy3 = '".$_POST['txt_accccy2']."',
			bank_acc_no3 = '".$_POST['txt_account_number2']."',
			bank_acc_name3 = '".$_POST['txt_account_name2']."',
			statement_type = '".$_POST['txt_statementtype']."',
			fatca = '".$_POST['txt_fatca']."',
			tin = '".$_POST['txt_tin']."',
			tin_issuance_country = '".$_POST['txt_tincountry']."'
			WHERE cust_id='".$_POST['inputId']."'
	";
   }else{
	   $query2 = "INSERT INTO $tablenameDetail (
				cust_id
				, middle_name
				, last_name
				, country_of_nationality
				, npwp_regis_date

				, country_of_birth
				, spouses_name
				, investors_risk_profile
				, asset_owner
				, bank_bic_code1

				, bank_bi_member_code1
				, bank_country1
				, bank_branch1
				, bank_ccy1
				, bank_bic_code2

				, bank_bi_member_code2
				, bank_country2
				, bank_branch2
				, bank_ccy2
				, bank_bic_code3

				, bank_bi_member_code3
				, bank_name3
				, bank_country3
				, bank_branch3
				, bank_ccy3

				, bank_acc_no3
				, bank_acc_name3
				, statement_type
				, fatca
				, tin
				, tin_issuance_country
			) VALUES (
				'".$_POST['inputId']."'
				, '".$_POST['middle_name']."'
				, '".$_POST['last_name']."'
				, '".$_POST['txt_country']."'
				, '".$_POST['txt_tgl_npwp']."'

				, '".$_POST['txt_country_birth']."'
				, '".$_POST['txt_spousename']."'
				, '".$_POST['txt_risk_profile']."'
				, '".$_POST['txt_assetowner']."'
				, '".$_POST['txt_bankbiccode']."'

				, '".$_POST['txt_bankbimembercode']."'
				, '".$_POST['txt_bankcountry']."'
				, '".$_POST['txt_bankbranch']."'
				, '".$_POST['txt_accccy']."'
				, '".$_POST['txt_bankbiccode1']."'

				, '".$_POST['txt_bankbimembercode1']."'
				, '".$_POST['txt_bankcountry1']."'
				, '".$_POST['txt_bankbranch1']."'
				, '".$_POST['txt_accccy1']."'
				, '".$_POST['txt_bankbiccode2']."'

				, '".$_POST['txt_bankbimembercode2']."'
				, '".$_POST['txt_name_bank2']."'
				, '".$_POST['txt_bankcountry2']."'
				, '".$_POST['txt_bankbranch2']."'
				, '".$_POST['txt_accccy2']."'

				, '".$_POST['txt_account_number2']."'
				, '".$_POST['txt_account_name2']."'
				, '".$_POST['txt_statementtype']."'
				, '".$_POST['txt_fatca']."'
				, '".$_POST['txt_tin']."'
				, '".$_POST['txt_tincountry']."'
			)";
   }*/
   
   if($gotError){
		echo "<script>alert('".$errorMessage."Save Failed.');</script>";
	}else{
		if ($data->inpQueryReturnBool($sql))
		{	
			//if($data->inpQueryReturnBool($query2)){
				echo "<script>alert('".$data->err_report('s01')."');opener.document.form1.submit();window.parent.close();</script>";
			//}else{
				
				//echo "<script>alert('".$data->err_report('s02')."');</script>";	
			//}
				
		}else
		{
			echo $data->queryError();	
			echo "<script>alert('".$data->err_report('s02')."');</script>";	
		}
	}
}

if ($_GET['edit'] == 1)
{   #$data->auth('09030102',$_SESSION['user_id']);
	$rows = $data->selectQuery("select * from ".$tablename." where cus_no_identity ='".$_GET['id']."'");
	$date = $_POST[txt_tgl_subscribe];
	$pecah1 = explode("-", $date);
	$date1 = $pecah1[2];
	$month = $pecah1[1];
	$year = $pecah1[0];
	$name = $_POST['txt_cus_name'];
	$pecah_name = substr($name, 0,1);
	$pecah_year = substr($year,2,2);
	$id = "$pecah_name$pecah_year$month ";
    
    if($rows['cus_penghasilan'] == '< 10 Juta/Tahun') $rows['cus_penghasilan'] = 1;
    else if($rows['cus_penghasilan'] == '10 - 50 Juta/Tahun') $rows['cus_penghasilan'] = 2;
    else if($rows['cus_penghasilan'] == '50 - 100 Juta/Tahun') $rows['cus_penghasilan'] = 3;
    else if($rows['cus_penghasilan'] == '100 - 500 Juta/Tahun') $rows['cus_penghasilan'] = 4;
    else if($rows['cus_penghasilan'] == '500 Juta - 1 Milyar/Tahun') $rows['cus_penghasilan'] = 5;
    else if($rows['cus_penghasilan'] == '> 1 Milyar/Tahun') $rows['cus_penghasilan'] = 6;
	
	//print($name);
	
	$cek_data=$data->get_value("select count(cus_id) from tbl_kr_cus_sup where left(cus_code,5) ='".$id."'");
	$cek_data = $cek_data + 1;
	if ($cek_data < 10 ){
		$no_auto= "0$cek_data";
		//print($no_auto);
	}
	else
	{
		$no_auto=$cek_data;
	}
	
	$id2 = "$pecah_name$pecah_year$month$no_auto";
	$dataRows = array (
				'TEXT' => array(
					'CUSTOMER ID / CLIENT NO',
					'TANGGAL SUBSCRIBE',
					'NAMA DEPAN <span class="redstar">*</span>',
					'NAMA TENGAH',
					'NAMA BELAKANG',

					'JENIS KARTU IDENTITAS',
					'NOMOR IDENTITAS <span class="redstar">*</span>',
					'NEGARA LAHIR',
					'TEMPAT LAHIR <span class="redstar">*</span>',
					'NPWP',

					'TGL PENDAFTARAN NPWP',
					'JENIS KELAMIN <span class="redstar">*</span>',
					'NAMA GADIS IBU KANDUNG',
					'AGAMA <span class="redstar">*</span>',
					'NEGARA KEWARGANEGARAAN <span class="redstar">*</span>',

					'KEWARGANEGARAAN',
					'STATUS PERKAWINAN <span class="redstar">*</span>',
					'NAMA PASANGAN',
					'JUMLAH TANGGUNGAN',
					'ALAMAT SESUAI IDENTITAS <span class="redstar">*</span>','',

					'TELEPON RUMAH',
					'FAX',
					'ALAMAT TEMPAT TINGGAL <span class="redstar">*</span>','',
					'NEGARA TEMPAT TINGGAL',
					'STATUS RUMAH TINGGAL',
					'MENEMPATI SEJAK',
					'PENDIDIKAN <span class="redstar">*</span>'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
					/*"<input type=text name=txt_cus_id id=txt_cus_id value='".$rows[cus_code]."'>",
					$data->datePicker('txt_tgl_subscribe', $rows[cus_tgl_subscribe],''),
					"<input type=text name=txt_cus_name id=txt_cus_name  value='".$rows[cus_name]."' onChange='document.formaddType.submit();'>",
					$data->cb_kartu_identitas('txt_cus_kartu',$rows[cus_card_identity]),
					"<input type=text name=txt_no id=txt_no value='".$rows[cus_no_identity]."'>".$data->cb_ktp('txt_status_ktp',$rows[cus_status_identity])." <br>BERLAKU S/D".$data->datePicker('txt_tgl_berlaku', $rows[cus_berlaku_card],''),
			
					"<input type=text name=txt_tmp_lahir id=txt_tmp_lahir value='".$rows[cus_tempat_lahir]."'><br> TANGGAL LAHIR".$data->datePicker('txt_tgl_lahir', $rows[cus_tgl_lahir],''),
					"<input type=text name=txt_npwp1 id=txt_npwp1 size='2' onKeyPress='return checkIt(event)' maxlength='2' onKeyUp='valid_length1()' value='".$rows[cus_no_npwp_1]."'>.
					<input type=text name=txt_npwp2 id=txt_npwp2 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length2()' value='".$rows[cus_no_npwp_2]."'>.
					<input type=text name=txt_npwp3 id=txt_npwp3 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length3()' value='".$rows[cus_no_npwp_3]."'>.
					<input type=text name=txt_npwp4 id=txt_npwp4 size='2'  onKeyPress='return checkIt(event)' maxlength='1' onKeyUp='valid_length4()' value='".$rows[cus_no_npwp_4]."'>.
					<input type=text name=txt_npwp5 id=txt_npwp5 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length5()' value='".$rows[cus_no_npwp_5]."'>.
					<input type=text name=txt_npwp6 id=txt_npwp6 size='5'  onKeyPress='return checkIt(event)' maxlength='3' value='".$rows[cus_no_npwp_6]."'>",
					$data->cb_jns_kelamin('txt_cus_jnskel',$rows[cus_sex]),
	
					"<input type=text name=txt_ibu id=txt_ibu value='".$rows[cus_mother_name]."'>",
					$data->cb_religion('txt_agama',$rows[cus_agama]),
					$data->cb_kewarganegaraan('txt_kewarganegaraan',$rows[cus_national])."Status Domisili".$data->cb_status_domisili('txt_domisili',$rows[cus_status_domisili]),
					$data->cb_status_perkawinan('txt_status_kawin',$rows[cus_status_kawin]),
					"<input type=text name=txt_jml_tanggungan id=txt_jml_tanggungan value='".$rows[cus_jumlah_tanggungan]."'>",
					"<textarea rows=2 cols=27 name=txt_address>".$rows[cus_address]."</textarea>",
					"
						KOTA : ".$data->cb_kota('txt_city',$rows[cus_city])."
					
					KODE POS : <input type=text name=txt_kode id=txt_kode value='".$rows[cus_kode_pos]."'>
					PROPINSI : <input type=text name=txt_propinsi id=txt_propinsi value='".$rows[cus_propinsi]."'>
					",
			
					"<input type=text name=txt_telp id=txt_telp value='".$rows[cus_telp]."'> HANDPHONE :<input type=text name=txt_phone id=txt_phone value='".$rows[cus_phone]."'>",
		
					
					"<input type=text name=txt_fax id=txt_fax value='".$rows[cus_fax]."'> EMAIL :<input type=text name=txt_email id=txt_email value='".$rows[cus_email]."'>",
					"<textarea rows=2 cols=27 name=txt_address_tinggal>".$rows[cus_address_t]."</textarea>",
					"
					KOTA : ".$data->cb_kota('txt_city_tinggal',$rows[cus_city_t])."
					
					KODE POS : <input type=text name=txt_kode_tinggal id=txt_kode_tinggal value='".$rows[cus_kode_pos_t]."'>
					PROPINSI : <input type=text name=txt_propinsi_tinggal id=txt_propinsi_tinggal value='".$rows[cus_propinsi_t]."'>
					",
					$data->cb_status_rt('txt_status_rt',$rows[cus_status_rt]),
					"<input type=text name=txt_menempati id=txt_menempati value='".$rows[cus_menempati_t]."'>",
					$data->cb_pendidikan('txt_pendidikan',$rows[cus_pendidikan]),*/
					"<input type=hidden name=inputId id=inputId value='".$rows[cus_id]."'><input type=text name=txt_cus_id id=txt_cus_id value='".$rows[cus_code]."'>",
					$data->datePicker('txt_tgl_subscribe', $rows[cus_tgl_subscribe],''),
					"<input type=text name=txt_cus_name id=txt_cus_name  value='".$rows[cus_name]."'>",
					"<input type=text name=txt_middle_name id=txt_middle_name  value='".$rows[middle_name]."'>",
					"<input type=text name=txt_last_name id=txt_last_name  value='".$rows[last_name]."'>",

					$data->cb_kartu_identitas('txt_cus_kartu',$rows[cus_card_identity]),
					"<input type=text name=txt_no id=txt_no value='".$rows[cus_no_identity]."'>".$data->cb_ktp('txt_status_ktp',$rows[cus_status_identity], 'onchange="changeSeumurHidup(this.value)"')." <br>BERLAKU S/D".$data->datePicker('txt_tgl_berlaku', $rows[cus_berlaku_card],''),
					$data->cb_isocountry('txt_country_birth',$rows[country_of_birth]),
					"<input type=text name=txt_tmp_lahir id=txt_tmp_lahir value='".$rows[cus_tempat_lahir]."'><br> TANGGAL LAHIR".$data->datePicker('txt_tgl_lahir', $rows[cus_tgl_lahir],''),
					"<input type=text name=txt_npwp1 id=txt_npwp1 size='2' onKeyPress='return checkIt(event)' maxlength='2' onKeyUp='valid_length1()' value='".$rows[cus_no_npwp_1]."'>.
					<input type=text name=txt_npwp2 id=txt_npwp2 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length2()' value='".$rows[cus_no_npwp_2]."'>.
					<input type=text name=txt_npwp3 id=txt_npwp3 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length3()' value='".$rows[cus_no_npwp_3]."'>.
					<input type=text name=txt_npwp4 id=txt_npwp4 size='2'  onKeyPress='return checkIt(event)' maxlength='1' onKeyUp='valid_length4()' value='".$rows[cus_no_npwp_4]."'>.
					<input type=text name=txt_npwp5 id=txt_npwp5 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length5()' value='".$rows[cus_no_npwp_5]."'>.
					<input type=text name=txt_npwp6 id=txt_npwp6 size='5'  onKeyPress='return checkIt(event)' maxlength='3' value='".$rows[cus_no_npwp_6]."'>",

					$data->datePicker('txt_tgl_npwp', $rows[npwp_regis_date],''),
					$data->cb_jns_kelamin('txt_cus_jnskel',$rows[cus_sex]),
					"<input type=text name=txt_ibu id=txt_ibu value='".$rows['cus_mother_name']."'>",
					$data->cb_religion('txt_agama',$rows[cus_agama]),
					$data->cb_isocountry('txt_country',$rows[country_of_nationality]),

					$data->cb_kewarganegaraan('txt_kewarganegaraan',$rows[cus_national])."Status Domisili".$data->cb_status_domisili('txt_domisili',$rows[cus_status_domisili]),
					$data->cb_status_perkawinan('txt_status_kawin',$rows[cus_status_kawin]),
					"<input type=text name=txt_spousename id=txt_spousename value='".$rows[spouses_name]."'>",
					"<input type=text name=txt_jml_tanggungan id=txt_jml_tanggungan value='".$rows[cus_jumlah_tanggungan]."'>",
					"<textarea rows=2 cols=27 name=txt_address>".$rows[cus_address]."</textarea>",
					"KOTA : ".$data->cb_kota('txt_city',$rows[cus_city])."
					KODE POS : <input type=text name=txt_kode id=txt_kode value='".$rows[cus_kode_pos]."'>
					PROPINSI : <input type=text name=txt_propinsi id=txt_propinsi value='".$rows[cus_propinsi]."'>",

					"<input type=text name=txt_telp id=txt_telp value='".$rows[cus_telp]."'> HANDPHONE :<input type=text name=txt_phone id=txt_phone value='".$rows[cus_phone]."'>",
					"<input type=text name=txt_fax id=txt_fax value='".$rows[cus_fax]."'> EMAIL :<input type=text name=txt_email id=txt_email value='".$rows[cus_email]."'>",
					"<textarea rows=2 cols=27 name=txt_address_tinggal>".$rows[cus_address_t]."</textarea>",
					"KOTA : ".$data->cb_kota('txt_city_tinggal',$rows[cus_city_t])."
					KODE POS : <input type=text name=txt_kode_tinggal id=txt_kode_tinggal value='".$rows[cus_kode_pos_t]."'>
					PROPINSI : <input type=text name=txt_propinsi_tinggal id=txt_propinsi_tinggal value='".$rows[cus_propinsi_t]."'>",
					$data->cb_isocountry('country_of_domicily',$rows[country_of_domicily],''),
					$data->cb_status_rt('txt_status_rt',$rows[cus_status_rt]),
					"<input type=text name=txt_menempati id=txt_menempati value='".$rows[cus_menempati_t]."'>",
					$data->cb_pendidikan('txt_pendidikan',$rows[cus_pendidikan]),
				)
			);
			
	$dataRows2 = array (
				'TEXT' => array(
					'PEKERJAAN <span class="redstar">*</span>',
					'NAMA PERUSAHAAN/KANTOR',
					'KEGIATAN / BIDANG USAHA',
					'ALAMAT PERUSAHAAN/KANTOR ',
					'',
					'NEGARA PERUSAHAAN',
					'NO. TELP. KANTOR',
					'FAX',
					'LAMA BEKERJA/USAHA',
					'ALAMAT KANTOR SEBAGAI KORESPONDEN',
				),
				'DOT'  => array (':',':',':',':',':',':',':'),
				'FIELD' => array (
					$data->cb_pekerjaan('txt_pekerjaan',$rows[cus_pekerjaan]),
					"<input type=text name=txt_nm_perusahaan id=txt_nm_perusahaan value='".$rows[cus_company_name]."'>",
					"<input type=text name=txt_bd_usaha id=txt_bd_usaha value='".$rows[cus_bd_usaha]."'> POSISI/JABATAN : <input type=text name=txt_jabatan id=txt_jabatan value='".$rows[cus_jabatan]."'>",
					"<textarea rows=2 cols=30 name=txt_address_kantor>".$rows[cus_address_c]."</textarea>",
					"KOTA : ".$data->cb_kota('txt_city_kantor',$rows[cus_city_c])."
					KODE POS : <input type=text name=txt_kode_kantor id=txt_kode_kantor value='".$rows[cus_kode_pos_c]."'>
					PROPINSI : <input type=text name=txt_propinsi_kantor id=txt_propinsi_kantor value='".$rows[cus_propinsi_c]."'>",
					$data->cb_isocountry('country_of_correspondence',$rows[country_of_correspondence],''),
					"<input type=text name=txt_telp_kantor id=txt_telp_kantor value='".$rows[cus_telp_c]."'> HANDPHONE :<input type=text name=txt_phone_kantor id=txt_phone_kantor value='".$rows[cus_phone_c]."'>",
					"<input type=text name=txt_fax_kantor id=txt_fax_kantor value='".$rows[cus_fax_c]."'> EMAIL :<input type=text name=txt_email_kantor id=txt_email_kantor value='".$rows[cus_email_c]."'>",
					"<input type=text name=txt_lama_kerja id=txt_lama_kerja value='".$rows[cus_masa_usaha]."'>",
					"<input type=checkbox name=ascorrespondence id=ascorrespondence value='1' ".($rows[correspondence_type] == 0 ? '' : 'checked')." >",
				)
			);		  
	
	$dataRows3 = array (
				'TEXT' => array(
					'BANK BIC CODE', 'BANK BI MEMBER CODE','BANK <span class="redstar">*</span>', 'BANK COUNTRY <span class="redstar">*</span>', 'BANK BRANCH', 'ACCOUNT CCY','ACCOUNT NAME <span class="redstar">*</span>','ACCOUNT NUMBER <span class="redstar">*</span>','',
					'BANK BIC CODE 2', 'BANK BI MEMBER CODE 2', 'BANK 2 <span class="redstar">*</span>','BANK COUNTRY 2 <span class="redstar">*</span>', 'BANK BRANCH 2', 'ACCOUNT CCY 2','ACCOUNT NAME 2 <span class="redstar">*</span>','ACCOUNT NUMBER 2 <span class="redstar">*</span>','',
					'BANK BIC CODE 3', 'BANK BI MEMBER CODE 3', 'BANK 3 <span class="redstar">*</span>','BANK COUNTRY 3 <span class="redstar">*</span>', 'BANK BRANCH 3', 'ACCOUNT CCY 3','ACCOUNT NAME 3 <span class="redstar">*</span>','ACCOUNT NUMBER 3 <span class="redstar">*</span>','',
					'OTHER','CUSTOMER ACTIVED',
					'PENGHASILAN UTAMA/KOTOR/THN <span class="redstar">*</span>',
					'SUMBER PENGHASILAN <span class="redstar">*</span>',
					'ASSET OWNER',
					'MAKSUD DAN TUJUAN INVESTASI <span class="redstar">*</span>',
					'INVESTOR RISK PROFILE',
					'STATEMENT TYPE <span class="redstar">*</span>',
					'FATCA',
					'TIN/FOREIGN TIN',
					'TIN/FOREIGN TIN ISSUANCE COUNTRY',
					//'NO SID'
					'CLIENT CODE'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':'),
				'FIELD' => array (
					"<input type=text name=txt_bankbiccode id=txt_bankbiccode value='".$rows[bank_bic_code1]."'>",
					//"<input type=text name=txt_bankbimembercode id=txt_bankbimembercode value='".$rows[bank_bi_member_code1]."'>",
					$data->cb_bimembercode('txt_bankbimembercode', $rows[bank_bi_member_code1], 'onchange="changeBi1(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_name_bank id=txt_name_bank size=50 value='".$rows[cus_bank_name]."'>",
					$data->cb_isocountry('txt_bankcountry',$rows[bank_country1]),
					"<input type=text name=txt_bankbranch id=txt_bankbranch value='".$rows[bank_branch1]."'>",
					$data->cb_accountccy('txt_accccy',$rows[bank_ccy1]),
					"<input type=text name=txt_account_name id=txt_account_name value='".htmlspecialchars($rows[cus_account_name], ENT_QUOTES)."'>",
					"<input type=text name=txt_account_number id=txt_account_number value='".$rows[cus_account_number]."'>","",

					"<input type=text name=txt_bankbiccode1 id=txt_bankbiccode1 value='".$rows[bank_bic_code2]."'>",
					//"<input type=text name=txt_bankbimembercode1 id=txt_bankbimembercode1 value='".$rows[bank_bi_member_code2]."'>",
					$data->cb_bimembercode('txt_bankbimembercode1', $rows[bank_bi_member_code2], 'onchange="changeBi2(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_name_bank1 id=txt_name_bank1 size=50 value='".$rows[cus_bank_name2]."'>",
					$data->cb_isocountry('txt_bankcountry1',$rows[bank_country2]),
					"<input type=text name=txt_bankbranch1 id=txt_bankbranch1 value='".$rows[bank_branch2]."'>",
					$data->cb_accountccy('txt_accccy1',$rows[bank_ccy2]),
					"<input type=text name=txt_account_name1 id=txt_account_name1 value='".$rows[cus_account_name2]."'>",
					"<input type=text name=txt_account_number1 id=txt_account_number1 value='".$rows[cus_account_number2]."'>","",
		
					"<input type=text name=txt_bankbiccode2 id=txt_bankbiccode1 value='".$rows[bank_bic_code3]."'>",
					//"<input type=text name=txt_bankbimembercode2 id=txt_bankbimembercode1 value='".$rows[bank_bi_member_code3]."'>",
					$data->cb_bimembercode('txt_bankbimembercode2', $rows[bank_bi_member_code3], 'onchange="changeBi3(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_name_bank2 id=txt_name_bank2 size=50 value='".$rows[bank_name3]."'>",
					$data->cb_isocountry('txt_bankcountry2',$rows[bank_country3]),
					"<input type=text name=txt_bankbranch2 id=txt_bankbranch2 value='".$rows[bank_branch3]."'>",
					$data->cb_accountccy('txt_accccy2',$rows[bank_ccy3]),
					"<input type=text name=txt_account_name2 id=txt_account_name2 value='".$rows[bank_acc_name3]."'>",
					"<input type=text name=txt_account_number2 id=txt_account_number2 value='".$rows[bank_acc_no3]."'>","",
					
					"<textarea rows=3 cols=17 name=txt_other>".$rows[cus_other]."</textarea>",
					$data->cb_customer_status('txt_cus_active',$rows[cus_actived]),
					$data->cb_penghasilan('txt_cus_penghasilan',$rows[cus_penghasilan]),
					$data->cb_sumber_dana('txt_cus_sumber_dana',$rows[cus_sumber_dana]),
					$data->cb_assetowner('txt_assetowner',$rows[asset_owner]),
					$data->cb_tujuan('txt_cus_tujuan',$rows[cus_maksud_tujuan]),
					$data->cb_riskprofile('txt_risk_profile',$rows[investors_risk_profile]),
					$data->cb_statementtype('txt_statementtype',$rows[statement_type]),
					$data->cb_fatca('txt_fatca',$rows[fatca]),
					"<input type=text name=txt_tin id=txt_tin value='".$_rows[tin]."'>",
					$data->cb_isocountry('txt_tincountry',$rows[tin_issuance_country]),
					//"<input type=text name=txt_cus_sid id=txt_cus_sid value='".$_rows[cus_sid]."'>"
					//$data->cb_customer_status2('txt_cus_status2',$_POST[txt_cus_status2]),cus_other,
					"<input type=text maxlength=6 name=client_code id=client_code value='".$rows[client_code]."'>"
				)
			);		  

    $tittle = "CUSTOMER / CLIENT EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"opener.document.form1.submit();window.parent.close();\">"
					);
}

$javascript = "<script type='text/javascript'>
	function changeBi1(name, account){
		document.getElementById('txt_name_bank').value = name;
		
	}
	function changeBi2(name, account){
		document.getElementById('txt_name_bank1').value = name;
		
	}
	function changeBi3(name, account){
		document.getElementById('txt_name_bank2').value = name;
		
	}

	function changeSeumurHidup(obj){
		if(obj == 'SEUMUR HIDUP'){
			var arr = document.getElementById('txt_tgl_berlaku').value.split('-');
			var newdate = '2099-'+arr[1]+'-'+arr[2];
			//document.getElementById('txt_tgl_berlaku').value = newdate;
		}
	}
</script>";
	
	$tmpl->addRows('loopDataPayment',$DG);
	
	if($_GET['del']==1){
		$id = trim($_GET['id_del']);
		$sql = "Delete from tbl_kr_cus_reksadetail where pk_id='".$id."'";
		$data->inpQueryReturnBool($sql);
		
		//print($sql);
	
		if ($data->inpQueryReturnBool($sql)){
			echo "<script>alert('".$data->err_report('d01')."');window.location='contact_add.php?detail=1&id=".$_GET[id_con]."'</script>";
		}else{
			echo "<script>alert('".$data->err_report('d02')."');window.location='contact_add.php?detail=1&id=".$_GET[id_con]."'</script>";
		}
	}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
#$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));		
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('row2',$dataRows2 );
$tmpl->addVars('row3',$dataRows3 );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>