<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'product.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new liquidation;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('product_sa.html');

$tablename = 'tbl_kr_allocation_sa';
$tablealias = 'A';
$primarykey = 'allocation_sa_id';

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by=$tablealias.".".$primarykey; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$keySearch = "<input type=text name=keySearch id=keySearch value=''>";
$linkAdd = 'product_sa_form.php';
$linkExport = 'product__sa_export.php';

//$show_field = array('allocation', 'fund_code', 'sa_name', 'fund_shortname', 'description', 'description1' );
//$search_field = array('allocation', 'description', 'description1', 'fundCode', 'fundName', 'fundShortName');

$btnView = '<input type="submit" name="btnView" value="View">';
$btnAdd = '<input type="button" name="btnAdd" value="Add Fund SA" onclick="window.location=\''.$linkAdd.'?add=1\'">';
$btnExport = '<input type="button" name="btnExport" value="Export" onclick="">';

$datagrid = array();

if ($_POST['btnView']=='View' || $_GET['view'] == '1'){
  	$key_search =  trim(htmlentities($_POST['keySearch']));
	if (empty($key_search))
		$key_search = '';

    $btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?keySearch='.$key_search.'\');">';
	
    //$sql = $data->generateQuery($tablename, $show_field, $search_field, $key_search, $primarykey);
    $sql = "SELECT 
                allocation_sa_id ,
                B.allocation as ALLOCATION,
                A.fund_code as FUNDCODE,
                B.fund_name as FUNDNAME,
                A.sa_code as SACODE,
                C.sa_name as SANAME
            FROM tbl_kr_allocation_sa A LEFT JOIN tbl_kr_allocation B ON A.fund_code = B.fund_code
                LEFT JOIN tbl_kr_ms_sa C ON A.sa_code = C.sa_code
            WHERE A.fund_code LIKE '%$key_search%' OR B.fund_name LIKE '%$key_search%'
                OR A.sa_code LIKE '%$key_search%' OR C.sa_name LIKE '%$key_search%' 
        ";
    $sql.= " ORDER BY ".$order_by." ".$sort_order;
  	
	$datagrid= $data->getFundSaData($sql, 'edit', 'product_sa_form.php', 'delete', 'product_sa.php', $primarykey);
}


if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "DELETE FROM tbl_kr_allocation_sa WHERE allocation_sa_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='product_sa.php?view=1';</script>";
	}
}

$tmpl->addVar('page','keySearch',$keySearch);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>