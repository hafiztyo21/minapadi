-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2017 at 10:39 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `keraton`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kr_cus_ifua_balance`
--

CREATE TABLE IF NOT EXISTS `tbl_kr_cus_ifua_balance` (
  `balance_id` bigint(20) NOT NULL,
  `ifua_code` varchar(30) NOT NULL,
  `cus_sid` varchar(20) NOT NULL,
  `unit_balance` decimal(16,4) NOT NULL,
  `amount_balance` decimal(16,4) NOT NULL,
  `trade_date` date NOT NULL,
  `fund_code` varchar(30) NOT NULL,
  `fund_name` varchar(50) NOT NULL,
  `last_change_date` varchar(8) NOT NULL,
  `nav` decimal(16,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_kr_cus_ifua_balance`
--
ALTER TABLE `tbl_kr_cus_ifua_balance`
  ADD PRIMARY KEY (`balance_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_kr_cus_ifua_balance`
--
ALTER TABLE `tbl_kr_cus_ifua_balance`
  MODIFY `balance_id` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
