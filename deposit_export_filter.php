<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('deposit_export_filter.html');

$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
//$picker = '<label><input type="radio" name="transactionType" value="1">Subscribe</label> &nbsp;<label><input type="radio" name="transactionType" value="2">Redemption</label>';
$select =  $data->cb_allocation('allocation',$_POST[allocation],"");

$btnView = '<input type="submit" name="btnExport" value="Export">';

if ($_POST['btnExport']=='Export'){
  	$transaction_date =  trim(htmlentities($_POST['transactionDate']));
	  if (empty($transaction_date))
		  $transaction_date = date("Y-m-d");

    $allocation = trim(htmlentities($_POST['allocation']));

    header("location:deposit_export.php?transaction_date=$transaction_date&allocation=$allocation");
    exit(0);
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','select',$select);
$tmpl->addVar('page','view', $btnView);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>