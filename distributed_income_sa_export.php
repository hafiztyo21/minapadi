<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$filename = $transaction_date."_distributed_income_option.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    sa_code,  
    fund_code, 
    investor_ac_no, 
    dist_inc_opt
    
    FROM tbl_kr_dist_inc_sa
    WHERE DATE_FORMAT(tbl_kr_dist_inc_sa.created_time, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0";

$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $arr = array();
    $var = $rows[$i];

    $idx = 0;
    $arr[$idx] = $var['sa_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['fund_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['investor_ac_no'];                                               $idx++;     //0.
    $arr[$idx] = $var['dist_inc_opt'];                                               $idx++;     //0.

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
    
}

fclose($output);
?>
