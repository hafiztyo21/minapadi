<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('si_fixed_income_unregistered_form.html');

$id = 0;
$headerid = $_GET['headerid'];
$trade_id = '';
$face_value = 0;
$acquisition_date = date('Y-m-d');
$acquisition_price_persen = 0;
$acquisition_amount = 0;
$capital_gain = 0;
$days_of_holding_interest = 0;
$holding_interest_amount = 0;
$total_taxable_income = 0;
$tax_rate_in_persen = 0;
$tax_amount = 0;

$errorArr = array();
for($i =0; $i<11;$i++){
    $errorArr[$i] = '';
}
$otherError='';

$query = "SELECT trade_id FROM tbl_kr_si_fixed_income WHERE si_fixed_income_id = $headerid";
$result = $data->get_row($query);
$trade_id = $result['trade_id'];

if($_GET['edit'] == 1){
    $id = $_GET['id'];
    $query = "SELECT A.*, B.trade_id FROM tbl_kr_si_fixed_income_tax A LEFT JOIN tbl_kr_si_fixed_income B ON A.si_fixed_income_id = B.si_fixed_income_id WHERE A.si_fixed_income_tax_id='$id'";
    $result = $data->get_row($query);

    $trade_id = $result['trade_id'];
    $face_value = $result['face_value'];
    $acquisition_date = $result['acquisition_date'];
    $acquisition_price_persen = $result['acquisition_price_persen'];
    $acquisition_amount = $result['acquisition_amount'];
    $capital_gain = $result['capital_gain'];
    $days_of_holding_interest = $result['days_of_holding_interest'];
    $holding_interest_amount = $result['holding_interest_amount'];
    $total_taxable_income = $result['total_taxable_income'];
    $tax_rate_in_persen = $result['tax_rate_in_persen'];
    $tax_amount = $result['tax_amount'];
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $headerid =trim(htmlentities($_POST['headerId'])); 
        $id = trim(htmlentities($_POST['inputId']));
        $trade_id = trim(htmlentities($_POST['trade_id']));
        $face_value = trim(htmlentities($_POST['face_value']));
        $acquisition_date = trim(htmlentities($_POST['acquisition_date']));
        $acquisition_price_persen = trim(htmlentities($_POST['acquisition_price_persen']));

        $acquisition_amount = floatval($face_value) * floatval($acquisition_price_persen) / 100;
        $capital_gain = trim(htmlentities($_POST['capital_gain']));
        $days_of_holding_interest = trim(htmlentities($_POST['days_of_holding_interest']));
        $holding_interest_amount = trim(htmlentities($_POST['holding_interest_amount']));
        $total_taxable_income = trim(htmlentities($_POST['total_taxable_income']));
        $tax_rate_in_persen = trim(htmlentities($_POST['tax_rate_in_persen']));
        $tax_amount = trim(htmlentities($_POST['tax_amount']));
		
		$gotError = false;
		if($face_value==''){
			$errorArr[1] = "Fce Value must be filled";
			$gotError = true;
		}
		/*if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}*/
        
        if($acquisition_price_persen==''){
			$errorArr[3] = "Acquisition Price (%) must be filled";
			$gotError = true;
		}
        
        if($capital_gain==''){
			$errorArr[5] = "Capital Gain must be filled";
			$gotError = true;
		}
        
        if($days_of_holding_interest==''){
			$errorArr[6] = "Days of Holding Interest must be filled";
			$gotError = true;
		}
        if($holding_interest_amount==''){
			$errorArr[7] = "Holding Interest Amount must be filled";
			$gotError = true;
		}
       
        if($total_taxable_income==''){
			$errorArr[8] = "Total Taxable Income must be filled";
			$gotError = true;
		}
        if($tax_rate_in_persen==''){
			$errorArr[9] = "Tax Rate must be filled";
			$gotError = true;
		}
        if($tax_amount==''){
			$errorArr[10] = "Tax Amount must be filled";
			$gotError = true;
		}
        
		if (!$gotError){
			if($id == 0){
                $query = "INSERT INTO tbl_kr_si_fixed_income_tax (
                        si_fixed_income_id,
                        face_value,
                        acquisition_date,
                        acquisition_price_persen,
                        acquisition_amount,
                        capital_gain,
                        days_of_holding_interest,
                        holding_interest_amount,
                        total_taxable_income,
                        tax_rate_in_persen,
                        tax_amount ,
                        created_date,
                        created_by,
                        last_updated_date,
                        last_updated_by,
                        is_deleted
                    )VALUES(
                        '$headerid',
                        '$face_value',
                        '$acquisition_date',
                        '$acquisition_price_persen',
                        '$acquisition_amount',
                        '$capital_gain',
                        '$days_of_holding_interest',
                        '$holding_interest_amount',
                        '$total_taxable_income',
                        '$tax_rate_in_persen',
                        '$tax_amount',
                        now(),
                        '".$_SESSION['pk_id']."',
                        now(),
                        '".$_SESSION['pk_id']."',
                        '0')";
            }else{
                $query = "UPDATE tbl_kr_si_fixed_income_tax SET 
                    face_value= '$face_value',
                    acquisition_date= '$acquisition_date',
                    acquisition_price_persen= '$acquisition_price_persen',
                    acquisition_amount= '$acquisition_amount',
                    capital_gain= '$capital_gain',
                    days_of_holding_interest= '$days_of_holding_interest',
                    holding_interest_amount= '$holding_interest_amount',
                    total_taxable_income= '$total_taxable_income',
                    tax_rate_in_persen= '$tax_rate_in_persen',
                    tax_amount= '$tax_amount',
                    last_updated_date= now(),
                    last_updated_by= '".$_SESSION['pk_id']."'
                    WHERE si_fixed_income_tax_id = '$id'
                ";
            }
			

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='si_fixed_income_tax.php?detail=1&headerid=$headerid';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = ($id == '0' ? "ADD" : "EDIT")." - SI FIXED INCOME TAX";
$dataRows = array (
	    'TEXT' => array('Trade ID'
            ,'Face Value'
            ,'Acquisition Date'
            ,'Acquisition Price(%)'
            ,'Acquisition Amount'
            ,'Capital Gain'
            ,'Days of Holding Interest'
            ,'Holding Interest Amount'
            ,'Total Taxable Income'
            ,'Tax Rate in %'
            ,'Tax Amount'
            ),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            "<input type=hidden id=inputId name=inputId value='$id'><input type=hidden id=headerId name=headerId value='$headerid'>
            <input type=text maxlength=20 size=20 id=trade_id name=trade_id value='$trade_id' readonly>",
            "<input type=number id=face_value name=face_value value='$face_value' onkeyup='calc_proceeds()' step='0.01'>",
		    $data->datePicker('acquisition_date', $acquisition_date,''),
            "<input type=number id=acquisition_price_persen name=acquisition_price_persen value='$acquisition_price_persen' onkeyup='calc_proceeds()' step='0.01'>",
            "<input type=number id=acquisition_amount name=acquisition_amount value='$acquisition_amount' readonly step='0.01'>",
            "<input type=number id=capital_gain name=capital_gain value='$capital_gain' onkeyup='calc_total()'  step='0.01'>",
            "<input type=number id=days_of_holding_interest name=days_of_holding_interest value='$days_of_holding_interest'  step='1'>",
            "<input type=number id=holding_interest_amount name=holding_interest_amount value='$holding_interest_amount' onkeyup='calc_total()' step='1'>",
            "<input type=number id=total_taxable_income name=total_taxable_income value='$total_taxable_income' readonly step='0.01'>",
            "<input type=number id=tax_rate_in_persen name=tax_rate_in_persen value='$tax_rate_in_persen' step='0.01'>",
            "<input type=number id=tax_amount name=tax_amount value='$tax_amount'  step='0.01'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='si_fixed_income_tax.php?detail=1&headerid=$headerid';\">");

$javascript = "
    <script type='text/javascript'>
        function calc_proceeds(){
            var facevalue = document.getElementById('face_value').value;
            var acquisitionprice = document.getElementById('acquisition_price_persen').value;
            document.getElementById('acquisition_amount').value = (parseFloat(facevalue) * parseFloat(acquisitionprice)) / 100;
        }

        function calc_total(){
            var capitalgain = document.getElementById('capital_gain').value;
            var holding = document.getElementById('holding_interest_amount').value;
            
            document.getElementById('total_taxable_income').value = parseFloat(capitalgain) + parseFloat(holding);
            
        }
    </script>
";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path','javascript',$javascript );
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>