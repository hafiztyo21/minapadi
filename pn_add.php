<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('pn_add.html');
$tablename = 'tbl_kr_pn';


if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		//$$txt_placement = strtotime($_POST['txt_placement']);
		//$txt_maturity = strtotime($_POST['txt_maturity']);
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
 		$txt_issuer = trim(htmlentities($_POST['txt_issuer']));
		$txt_placement = trim(htmlentities($_POST['txt_placement']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
		$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_int = trim(htmlentities($_POST['txt_int']));
		$txt_quantity = trim(htmlentities($_POST['txt_quantity']));
		
		if($txt_issuer==''){
			echo "<script>alert('Issuer is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_placement=''){
			echo "<script>alert('{Placement Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_maturity==''){
			echo "<script>alert('Maturity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_total_aktiv==''){
			echo "<script>alert('Total aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_int==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_quantity==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
		$sql = "INSERT INTO tbl_kr_pn (
			issuer,
			placement,
			maturity,
			total_aktiv,interest,
			price_val,
			allocation,
			create_dt
		)VALUES(
			'$txt_issuer',
			'$_POST[txt_placement]',
			'$txt_maturity',
			'$txt_total_aktiv',
			'$txt_int',
			'$txt_quantity',
			'$txt_allocation',
			now()
			)";
			//print_r($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='pn_repo.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}


if ($_GET['add']==1){

	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Issuer','Placement Date',
						'Maturity Date','Total Aktiv','interest',
						'Quantity (Rp)'),
		'DOT'  => array (':',':',':',':'),
		'FIELD' => array (
			$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
			"<input type=text name=txt_issuer value='".$_POST[txt_issuer]."' >",
			$data->datePicker('txt_placement', $_POST[txt_placement],''),
			$data->datePicker('txt_maturity', $_POST[txt_maturity],''),
			"<input type=text name=txt_total_aktiv value='".$_POST[txt_total_aktiv]."' >",
			"<input type=text name=txt_int value='".$_POST[txt_int]."' >",
			"<input type=text name=txt_quantity value='".$_POST[txt_quantity]."' >"
			)
		);
		//print_r ($_POST[txt_placement]);
		//print_r($date1);
		//print_r($date2);
	$tittle = "ADD TRANSACTION";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='pn_repo.php';\">"
	);
}

if ($_GET['edit']==1){
	$rows=$data->get_row("select * from tbl_kr_pn where pn_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];
	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Issuer','Placement Date',
						'Maturity Date','Total Aktiv','interest',
						'Quantity (Rp)'),
		'DOT'  => array (':',':'),
		'FIELD' => array (
		"&nbsp;&nbsp;".$allocation2."",
			"<input type=text size=75 name=txt_issuer value='".$rows[issuer]."' >",
			$data->datePicker('txt_placement', $rows[placement],''),
			$data->datePicker('txt_maturity', $rows[maturity],''),
				"<input type=text name=txt_total_aktiv value='".$rows[total_aktiv]."' >",
				"<input type=text name=txt_int value='".$rows[interest]."' >",
			"<input type=text size=20 name=txt_quantity value='".$rows[price_val]."' >"
			)
		);
	$tittle = "EDIT TRANSACTION";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='pn_repo.php';\">"
	);
}

if ($_POST['btn_save_edit']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
 		$txt_issuer = trim(htmlentities($_POST['txt_issuer']));
		$txt_placement = trim(htmlentities($_POST['txt_placement']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
	$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
	$txt_int = trim(htmlentities($_POST['txt_int']));
		$txt_quantity = trim(htmlentities($_POST['txt_quantity']));
		if($txt_issuer==''){
			echo "<script>alert('Issuer is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_placement=''){
			echo "<script>alert('{Placement Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_maturity==''){
			echo "<script>alert('Maturity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_total_aktiv==''){
			echo "<script>alert('Total aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_int==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_quantity==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

		$sql = "update tbl_kr_pn set
			issuer = '".$txt_issuer."',
			price_val = '".$txt_quantity."',
			placement = '".$_POST[txt_placement]."',
			maturity = '".$txt_maturity."',
			total_aktiv= '".$txt_total_aktiv."',
			interest='".$txt_int."',
			create_dt = now()
			where pn_id = '".$_GET[id]."'";
			#print_r($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='pn_repo.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['detail']=='1'){
	$id = trim(htmlentities($_GET['id']));
	$date1= date("d-m-y",strtotime($rows[placement]));
	$date2= date("d-m-y",strtotime($rows[maturity]));
	$rows = $data->get_row("select * from tbl_kr_pn where pn_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];
	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Issuer',
						'Placement Date',
						'Maturity Date',
						'Total Aktiv',
						'Interest Rate %',
						'Quantity'),
		'DOT'  => array (':',':',':',':',':',':'),
		'FIELD' => array (
			'&nbsp;'.$allocation2,
			'&nbsp;'.$rows[issuer],
			'&nbsp;'.$rows[placement],
			'&nbsp;'.$rows[maturity],
			'&nbsp;'.$rows[total_aktiv],
			'&nbsp;'.$rows[interest],
			'&nbsp;'.$rows[price_val]
			)
		);
		
	$tittle = "PROMMISORY NOTE DETAIL ".$allocation2."";
    $button = array ('SUBMIT' => "",
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"window.location='pn_repo.php';\">"
	);
}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>