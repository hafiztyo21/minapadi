<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('si_fixed_income_unregistered_form.html');

$id = 0;
$type = '2';    //unregistered
$trade_id = '';
$trade_date = date('Y-m-d');
$settlement_date = date('Y-m-d');
$im_code = 'MU002';
$br_code = '';
//$br_name = '';
//$counterparty_code = '';
//$counterparty_name = '';
//$place_of_settlement = '';
$fund_code = '';
$security_type = '7';
$security_code_type = '2';
$security_code = '';
$security_name = '';
$buy_sell = '1';
$ccy = '';
$price = 0;
$face_value = 0;
$proceeds = 0;
$interest_rate=0;
$maturity_date = date('Y-m-d');
$last_coupon_date = date('Y-m-d');
$next_coupon_date = date('Y-m-d');
$accrued_days = 0;
$accrued_interest_amount =0;
$other_fee=0;
$capital_gain_tax = 0;
$interest_income_tax = 0;
$withholding_tax = 0;
$net_proceeds = 0;
$instruction_type = '1';
$seller_tax_id = '';
$purpose_of_transaction = '1';
$remarks = '';

$errorArr = array();
for($i =0; $i<29;$i++){
    $errorArr[$i] = '';
}
$otherError='';

if($_GET['edit'] == 1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_si_fixed_income WHERE si_fixed_income_id='$id'";
    $result = $data->get_row($query);

    $trade_id = $result['trade_id'];
    $trade_date = $result['trade_date'];
    $settlement_date = $result['settlement_date'];
    $br_code = $result['br_code'];
    //$br_name = $result['br_name'];
    //$counterparty_code = $result['counterparty_code'];
    //$counterparty_name = $result['counterparty_name'];
    //$place_of_settlement = $result['place_of_settlement'];
    $fund_code = $result['fund_code'];
    $security_type = $result['security_type'];
    $security_code_type = $result['security_code_type'];
    $security_code = $result['security_code'];
    $security_name = $result['security_name'];
    $buy_sell = $result['buy_sell'];
    $ccy = $result['ccy'];
    $price = $result['price'];
    $face_value = $result['face_value'];
    $proceeds = $result['proceeds'];
    $interest_rate= $result['interest_rate'];
    $maturity_date = $result['maturity_date'];
    $last_coupon_date = $result['last_coupon_date'];
    $next_coupon_date = $result['next_coupon_date'];
    $accrued_days = $result['accrued_days'];
    $accrued_interest_amount = $result['accrued_interest_amount'];
    $other_fee = $result['other_fee'];
    $capital_gain_tax = $result['capital_gain_tax'];
    $interest_income_tax = $result['interest_income_tax'];
    $withholding_tax = $result['withholding_tax'];
    $net_proceeds = $result['net_proceeds'];
    $instruction_type = $result['instruction_type'];
    $seller_tax_id = $result['seller_tax_id'];
    $purpose_of_transaction = $result['purpose_of_transaction'];
    $remarks = $result['remarks'];
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
        $trade_id = trim(htmlentities($_POST['trade_id']));
        $trade_date = trim(htmlentities($_POST['trade_date']));
        $settlement_date = trim(htmlentities($_POST['settlement_date']));
        //$im_code = 'MU002';
        $br_code = trim(htmlentities($_POST['br_code']));
        $br_name = trim(htmlentities($_POST['br_name']));
        $counterparty_code = trim(htmlentities($_POST['counterparty_code']));
        $counterparty_name = trim(htmlentities($_POST['counterparty_name']));
        $place_of_settlement = trim(htmlentities($_POST['place_of_settlement']));
        $fund_code = trim(htmlentities($_POST['fund_code']));
        $security_type = trim(htmlentities($_POST['security_type']));
        $security_code_type = trim(htmlentities($_POST['security_code_type']));
        $security_code = trim(htmlentities($_POST['security_code']));
        $security_name = trim(htmlentities($_POST['security_name']));
        $buy_sell = trim(htmlentities($_POST['buy_sell']));
        $ccy = trim(htmlentities($_POST['ccy']));
        $price = trim(htmlentities($_POST['price']));
        $face_value = trim(htmlentities($_POST['face_value']));
        $proceeds = (floatval($price) * floatval($quantity)) / 100;
        $interest_rate = trim(htmlentities($_POST['interest_rate']));
        $maturity_date = trim(htmlentities($_POST['maturity_date']));
        $last_coupon_date = trim(htmlentities($_POST['last_coupon_date']));
        $next_coupon_date = trim(htmlentities($_POST['next_coupon_date']));
        $accrued_days = trim(htmlentities($_POST['accrued_days']));
        $accrued_interest_amount= trim(htmlentities($_POST['accrued_interest_amount']));        
        $other_fee = trim(htmlentities($_POST['other_fee']));
        $capital_gain_tax = trim(htmlentities($_POST['capital_gain_tax']));
        $interest_income_tax = trim(htmlentities($_POST['interest_income_tax']));
        $withholding_tax = trim(htmlentities($_POST['withholding_tax']));
        if(intval($buy_sell) == 1){
            $net_proceeds = $proceeds + floatval($accrued_interest_amount) + floatval($other_fee) - floatval($withholding_tax);
        }else{
            $net_proceeds = $proceeds + floatval($accrued_interest_amount) - floatval($other_fee) - floatval($withholding_tax);
        }
        //$net_proceeds = trim(htmlentities($_POST['net_proceeds']));
        $instruction_type = trim(htmlentities($_POST['instruction_type']));
        $seller_tax_id = trim(htmlentities($_POST['seller_tax_id']));
        $purpose_of_transaction = trim(htmlentities($_POST['purpose_of_transaction']));
        $remarks = trim(htmlentities($_POST['remarks']));
		
		$gotError = false;
		if($trade_id==''){
			$errorArr[0] = "Trade Id must be filled";
			$gotError = true;
		}
		/*if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}*/
        if($br_code==''){
			$errorArr[3] = "BR Code must be filled";
			$gotError = true;
		}
        
        if($fund_code==''){
			$errorArr[4] = "Fund Code must be filled";
			$gotError = true;
		}
        if($security_code==''){
			$errorArr[7] = "Security Code must be filled";
			$gotError = true;
		}
        if($security_name==''){
			$errorArr[8] = "Security Name must be filled";
			$gotError = true;
		}
        if($price==''){
			$errorArr[11] = "Price must be filled";
			$gotError = true;
		}
        if($face_value==''){
			$errorArr[12] = "Face Value must be filled";
			$gotError = true;
		}
        if($interest_rate==''){
			$errorArr[14] = "Interest Rate must be filled";
			$gotError = true;
		}
        if($accrued_days==''){
			$errorArr[18] = "Accrued Days must be filled";
			$gotError = true;
		}
        if($accrued_interest_amount==''){
			$errorArr[19] = "Accrued Interest Amount must be filled";
			$gotError = true;
		}
        if($seller_tax_id==''){
			$errorArr[26] = "Seller Tax Id must be filled";
			$gotError = true;
		}
		
        
		if (!$gotError){
			if($id == 0){
                $query = "INSERT INTO tbl_kr_si_fixed_income (
                        `type`,
                        trade_id,
                        trade_date,
                        settlement_date,
                        im_code,
                        br_code,
                        fund_code,
                        security_type,
                        security_code_type,
                        security_code,
                        security_name,
                        buy_sell,
                        ccy,
                        price,
                        face_value,
                        proceeds,
                        interest_rate,
                        maturity_date,
                        last_coupon_date,
                        next_coupon_date,
                        accrued_days,
                        accrued_interest_amount,
                        other_fee,
                        capital_gain_tax,
                        interest_income_tax,
                        withholding_tax,
                        net_proceeds,
                        instruction_type,
                        seller_tax_id,
                        purpose_of_transaction,
                        remarks,
                        created_date,
                        created_by,
                        last_updated_date,
                        last_updated_by,
                        is_deleted
                    )VALUES(
                        '$type',
                        '$trade_id',
                        '$trade_date',
                        '$settlement_date',
                        '$im_code',
                        '$br_code',
                        '$fund_code',
                        '$security_type',
                        '$security_code_type',
                        '$security_code',
                        '$security_name',
                        '$buy_sell',
                        '$ccy',
                        '$price',
                        '$face_value',
                        '$proceeds',
                        '$interest_rate',
                        '$maturity_date',
                        '$last_coupon_date',
                        '$next_coupon_date',
                        '$accrued_days',
                        '$accrued_interest_amount',
                        '$other_fee',
                        '$capital_gain_tax',
                        '$interest_income_tax',
                        '$withholding_tax',
                        '$net_proceeds',
                        '$instruction_type',
                        '$seller_tax_id',
                        '$purpose_of_transaction',
                        '$remarks',
                        now(),
                        '".$_SESSION['pk_id']."',
                        now(),
                        '".$_SESSION['pk_id']."',
                        '0')";
            }else{
                $query = "UPDATE tbl_kr_si_fixed_income SET 
                    trade_id = '$trade_id',
                    trade_date= '$trade_date',
                    settlement_date= '$settlement_date',
                    br_code= '$br_code',
                    fund_code= '$fund_code',
                    security_type= '$security_type',
                    security_code_type= '$security_code_type',
                    security_code= '$security_code',
                    security_name= '$security_name',
                    buy_sell= '$buy_sell',
                    ccy= '$ccy',
                    price= '$price',
                    face_value= '$face_value',
                    proceeds= '$proceeds',
                    interest_rate= '$interest_rate',
                    maturity_date= '$maturity_date',
                    last_coupon_date= '$last_coupon_date',
                    next_coupon_date= '$next_coupon_date',
                    accrued_days= '$accrued_days',
                    accrued_interest_amount= '$accrued_interest_amount',
                    other_fee= '$other_fee',
                    capital_gain_tax= '$capital_gain_tax',
                    interest_income_tax= '$interest_income_tax',
                    withholding_tax= '$withholding_tax',
                    net_proceeds= '$net_proceeds',
                    instruction_type= '$instruction_type',
                    seller_tax_id= '$seller_tax_id',
                    purpose_of_transaction= '$purpose_of_transaction',
                    remarks= '$remarks',
                    last_updated_date= now(),
                    last_updated_by= '".$_SESSION['pk_id']."'
                    WHERE si_fixed_income_id = '$id'
                ";
            }
			

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='si_fixed_income_unregistered.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = ($id == '0' ? "ADD" : "EDIT")." - SI FIXED INCOME UNREGISTERED";
$dataRows = array (
	    'TEXT' => array('Trade ID'
            ,'Trade Date'
            ,'Settlement Date'
            ,'BR Code'
            ,'Fund Code'
            ,'Security Type'
            ,'Security Code Type'
            ,'Security Code'
            ,'Security Name'
            ,'Buy/Sell'
            ,'CCY'
            ,'Price'
            ,'Face Value'
            ,'Proceeds'
            ,'Interest Rate'
            ,'Maturity Date'
            ,'Last Coupon Date'
            ,'Next Coupon Date'
            ,'Accrued Days'
            ,'Accrued Interest Amount'
            ,'Other Fee'
            ,'Capital Gain Tax'
            ,'Interest Income Tax'
            ,'Withholding Tax'
            ,'Net Proceeds'
            ,'Instruction Type'
            ,'Seller Tax Id'
            ,'Purpose of Transaction'
            ,'Remarks'
            ),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            "<input type=hidden id=inputId name=inputId value='$id'>
            <input type=text maxlength=20 size=20 id=trade_id name=trade_id value='$trade_id'>",
		    $data->datePicker('trade_date', $trade_date,''),
            $data->datePicker('settlement_date', $settlement_date,''),
            "<input type=text maxlength=20 size=20 id=br_code name=br_code value='$br_code'>",
            //"<input type=text maxlength=100 size=50 id=br_name name=br_name value='$br_name'>",
            //"<input type=text maxlength=20 size=20 id=counterparty_code name=counterparty_code value='$counterparty_code'>",
            //"<input type=text maxlength=100 size=50 id=counterparty_name name=counterparty_name value='$counterparty_name'>",
            //"<input type=text maxlength=11 size=15 id=place_of_settlement name=place_of_settlement value='$place_of_settlement'>",
            $data->cb_fundcode('fund_code', $fund_code,''),
            $data->cb_securitytypefi2('security_type', $security_type, ''),
            $data->cb_securitycodetype('security_code_type', $security_code_type, false,''),
            "<input type=text maxlength=35 size=50 id=security_code name=security_code value='$security_code'>",
            "<input type=text maxlength=100 size=50 id=security_name name=security_name value='$security_name'>",
            $data->cb_buysell('buy_sell', $buy_sell,'onchange="calc_net()"'),
            $data->cb_accountccy('ccy', $ccy,''),
            "<input type=number id=price name=price value='$price'  onkeyup='calc_proceeds()' step='0.01'>",
            "<input type=number id=face_value name=face_value value='$face_value' onkeyup='calc_proceeds()' step='0.01'>",
            "<input type=number id=proceeds name=proceeds value='$proceeds' readonly step='0.01'>",
            "<input type=number id=interest_rate name=interest_rate value='$interest_rate' step='0.01'>",
            $data->datePicker('maturity_date', $maturity_date,''),
            $data->datePicker('last_coupon_date', $last_coupon_date,''),
            $data->datePicker('next_coupon_date', $next_coupon_date,''),
            "<input type=number id=accrued_days name=accrued_days value='$accrued_days' step='1'>",
            "<input type=number id=accrued_interest_amount name=accrued_interest_amount value='$accrued_interest_amount' onkeyup='calc_net()' step='0.01'>",
            "<input type=number id=other_fee name=other_fee value='$other_fee' onkeyup='calc_net()' step='0.01'>",
            "<input type=number id=capital_gain_tax name=capital_gain_tax value='$capital_gain_tax' onkeyup='calc_net()' step='0.01'>",
            "<input type=number id=interest_income_tax name=interest_income_tax value='$interest_income_tax' onkeyup='calc_net()' step='0.01'>",
            "<input type=number id=withholding_tax name=withholding_tax value='$withholding_tax' onkeyup='calc_net()' step='0.01'>",
            "<input type=number id=net_proceeds name=net_proceeds value='$net_proceeds' readonly step='0.01'>",
            $data->cb_instructiontypefi('instruction_type', $instruction_type,''),
            "<input type=text id=seller_tax_id name=seller_tax_id value='$seller_tax_id' >",
            $data->cb_purposeoftransaction('purpose_of_transaction', $purpose_of_transaction,''),
            "<input type=text size=50 id=remarks name=remarks value='$remarks'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='si_fixed_income_unregistered.php';\">");

$javascript = "
    <script type='text/javascript'>
        function calc_proceeds(){
            var price = document.getElementById('price').value;
            var facevalue = document.getElementById('face_value').value;
            document.getElementById('proceeds').value = (parseFloat(price) * parseFloat(facevalue)) / 100;
            calc_net();
        }

        function calc_net(){
            var proceeds = document.getElementById('proceeds').value;
            var accrued = document.getElementById('accrued_interest_amount').value;
            var other = document.getElementById('other_fee').value;
            var withholding = document.getElementById('withholding_tax').value;
            
            var buy_sell = document.getElementById('buy_sell').value;
            if(buy_sell == 1){
                document.getElementById('net_proceeds').value = parseFloat(proceeds) + parseFloat(accrued) + parseFloat(other) - parseFloat(withholding);
            }else{
                document.getElementById('net_proceeds').value = parseFloat(proceeds) + parseFloat(accrued) - parseFloat(other) - parseFloat(withholding);;
            }
        }
    </script>
";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path','javascript',$javascript );
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>