<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transactionDate'];
$filename = $transaction_date."_kyc_personal.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT * FROM tbl_kr_cus_sup WHERE DATE_FORMAT(cus_tgl_subscribe,'%Y-%m-%d') = '$transaction_date'";

$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

//-------------STATIC FIELD-----------
$saCode = 'MU002';
//------------------------------------

/*$str = "Type|SA Code|SID|First Name|Middle Name|Last Name|Country of Nationality|ID No.|ID Expiration Date|NPWP No|NPWP Registration Date";
$str .= "|Country of Birth|Place of Birth|Date of Birth|Gender|Educational Background|Mothers Maiden Name|Religion|Occupation";
$str .= "|Income Level|Marital Status|Spouses Name|Investor Risk Profile|Investment Objective|Source of Fund|Asset Owner|KTP Address";
$str .= "|KTP City Code|KTP Postal Code|Correspondence Address|Correspondence City Code|Correspondence City Name|Correspondence Postal Code";
$str .= "|Country of Correspondence|Domicile Address|Domicile City Code|Domicile City Name|Domicile Postal Code";
$str .= "|Country of Domicile|Home Phone|Mobile Phone|Facsimile|Email|Statement Type|FATCA|TIN/Foreign TIN|TIN/Foreign TIN Issuance Country";
$str .= "|REDM Payment Bank BIC Code 1|REDM Payment Bank BI Member Code 1|REDM payment Bank Name 1|REDM Payment Bank Country 1";
$str .= "|REDM Payment Bank Branch 1|REDM Payment A/C CCY 1|REDM Payment A/C No. 1|REDM Payment A/C Name 1";
$str .= "|REDM Payment Bank BIC Code 2|REDM Payment Bank BI Member Code 2|REDM payment Bank Name 2|REDM Payment Bank Country 2";
$str .= "|REDM Payment Bank Branch 2|REDM Payment A/C CCY 2|REDM Payment A/C No. 2|REDM Payment A/C Name 2";
$str .= "|REDM Payment Bank BIC Code 3|REDM Payment Bank BI Member Code 3|REDM payment Bank Name 3|REDM Payment Bank Country 3";
$str .= "|REDM Payment Bank Branch 3|REDM Payment A/C CCY 3|REDM Payment A/C No. 3|REDM Payment A/C Name 3";
$str .= "|Client Code\r\n";
fwrite($output, $str);*/

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $str = "";
    $arr = array();
    $var = $rows[$i];
    
    $type = '1';
    if($var['cus_sid'] != null && $var['cus_sid'] != '')
        $type = '2';

    $idx = 0;
    $arr[$idx] = $type;                                               $idx++;     //0. type : 1-input, 2-amendment
    $arr[$idx] = $saCode;                                             $idx++;     //1. sa code
    $arr[$idx] = $type == '1' ? '' : $var['cus_sid'];                 $idx++;     //2. sid 
    $arr[$idx] = $var['cus_name'];                                    $idx++;     //3. customer first name
    $arr[$idx] = $var['middle_name'];                                 $idx++;     //4. customer middle name 
    $arr[$idx] = $var['last_name'];                                   $idx++;     //5. customer last name

    $countryOfNationality = 'ID';
    if($var['country_of_nationality'] != null && $var['country_of_nationality'] != '')
        $countryOfNationality = $var['country_of_nationality'];

    $arr[$idx] = $countryOfNationality;                               $idx++;     //6. country of nationality
    $arr[$idx] = $var['cus_no_identity'];                             $idx++;     //7. id no.

    $idDate = '';
    if($var['cus_berlaku_card'] != null && $var['cus_berlaku_card'] != '' && $var['cus_berlaku_card'] != '0000-00-00')
        $idDate = str_replace('-','',$var['cus_berlaku_card']);
    if($var['cus_status_identity'] == 'SEUMUR HIDUP'){
		if($idDate != '')
        $idDate = '2099'. substr($idDate,4);
		else
		$idDate = '2099'.substr(str_replace('-','',$var['cus_tgl_lahir']),4);
    }

    $arr[$idx] = $idDate;                                             $idx++;     //8. id expired date

    $npwp = $var['cus_no_npwp_1']."".$var['cus_no_npwp_2']."".$var['cus_no_npwp_3']."".$var['cus_no_npwp_4']."".$var['cus_no_npwp_5']."".$var['cus_no_npwp_6'];
    //if($npwp == '')
        //$npwp = '000000000000000';

    $arr[$idx] = $npwp;                                               $idx++;     //9. npwp no.

    $npwpRegisDate = '';
    if($var['npwp_regis_date'] != null && $var['npwp_regis_date'] != '' && $var['npwp_regis_date'] != '0000-00-00')
        $npwpRegisDate = str_replace('-','',$var['npwp_regis_date']);
    if($npwp == '000000000000000') $npwpRegisDate = '';

    $arr[$idx] = $npwpRegisDate;                                      $idx++;     //10. npwp registration date
    $arr[$idx] = $var['country_of_birth'];                            $idx++;     //12. country of birth
    $arr[$idx] = $var['cus_tempat_lahir'];                            $idx++;     //13. place of birth
    $arr[$idx] = str_replace('-','',$var['cus_tgl_lahir']);           $idx++;     //14. date of birth

    $gender = '';
    if($var['cus_sex'] == 'Laki-Laki' || $var['cus_sex'] == '1' || $var['cus_sex'] == 'Pria') $gender = '1';
    else $gender = '2';

    $arr[$idx] = $gender;                                             $idx++;     //15. gender

    $education = '';
    if($var['cus_pendidikan'] == 'SD' || $var['cus_pendidikan'] == '1') $education = '1';
    else if($var['cus_pendidikan'] == 'SMP' || $var['cus_pendidikan'] == '2') $education = '2';
    else if($var['cus_pendidikan'] == 'SMA/SMK' || $var['cus_pendidikan'] == '3') $education = '3';
    else if($var['cus_pendidikan'] == 'Diploma' || $var['cus_pendidikan'] == '4') $education = '4';
    else if($var['cus_pendidikan'] == 'Sarjana(S1)' || $var['cus_pendidikan'] == '5') $education = '5';
    else if($var['cus_pendidikan'] == 'Pasca Sarjana(S2)' || $var['cus_pendidikan'] == '6') $education = '6';
    else if($var['cus_pendidikan'] == 'Doktor(S3)' || $var['cus_pendidikan'] == '7') $education = '7';
    else if($var['cus_pendidikan'] == 'Lainnya' || $var['cus_pendidikan'] == '8') $education = '8';

    $arr[$idx] = $education;                                          $idx++;     //16. educational background
    $arr[$idx] = $var['cus_mother_name'];                             $idx++;     //17. mother maiden name

    $religion = '';
    if($var['cus_agama'] == 'Islam' || $var['cus_agama'] == '1') $religion = '1';
    else if($var['cus_agama'] == 'Protestan' || $var['cus_agama'] == '2') $religion = '2';
    else if($var['cus_agama'] == 'Katholik' || $var['cus_agama'] == '3') $religion = '3';
    else if($var['cus_agama'] == 'Hindu' || $var['cus_agama'] == '4') $religion = '4';
    else if($var['cus_agama'] == 'Budha' || $var['cus_agama'] == '5') $religion = '5';
    else if($var['cus_agama'] == 'Kong Hu Cu' || $var['cus_agama'] == '6') $religion = '6';
    else if($var['cus_agama'] == 'Lainnya' || $var['cus_agama'] == '7') $religion = '7';

    $arr[$idx] = $religion;                                           $idx++;     //18. religion

    $occupation = '';
    if($var['cus_pekerjaan'] == 'Pelajar/Mahasiswa' || $var['cus_pekerjaan'] == '1') $occupation = '1';
    else if($var['cus_pekerjaan'] == 'Ibu Rumah Tangga' || $var['cus_pekerjaan'] == '2') $occupation = '2';
    else if($var['cus_pekerjaan'] == 'Wiraswasta' || $var['cus_pekerjaan'] == '3') $occupation = '3';
    else if($var['cus_pekerjaan'] == 'PNS/BUMN' || $var['cus_pekerjaan'] == '4') $occupation = '4';
    else if($var['cus_pekerjaan'] == 'TNI/POLRI' || $var['cus_pekerjaan'] == '5') $occupation = '5';
    else if($var['cus_pekerjaan'] == 'Pensiun' || $var['cus_pekerjaan'] == '6') $occupation = '6';
    else if($var['cus_pekerjaan'] == 'Dosen/Pengajar/Guru' || $var['cus_pekerjaan'] == '7') $occupation = '7';
    else if($var['cus_pekerjaan'] == 'Pegawai Swasta' || $var['cus_pekerjaan'] == '8') $occupation = '8';
    else if($var['cus_pekerjaan'] == 'Lainnya' || $var['cus_pekerjaan'] == '' || $var['cus_pekerjaan'] == '9') $occupation = '9';

    $arr[$idx] = $occupation;                                         $idx++;     //19. occupation

    $incomelevel = '';
    if($var['cus_penghasilan'] == '< 10 Juta/Tahun' || $var['cus_penghasilan'] == '1') $incomelevel = '1';
    else if($var['cus_penghasilan'] == '10 - 50 Juta/Tahun' || $var['cus_penghasilan'] == '2') $incomelevel = '2';
    else if($var['cus_penghasilan'] == '50 - 100 Juta/Tahun' || $var['cus_penghasilan'] == '3') $incomelevel = '3';
    else if($var['cus_penghasilan'] == '100 - 500 Juta/Tahun' || $var['cus_penghasilan'] == '4') $incomelevel = '4';
    else if($var['cus_penghasilan'] == '500 Juta - 1 Milyar/Tahun' || $var['cus_penghasilan'] == '5') $incomelevel = '5';
    else if($var['cus_penghasilan'] == '> 1 Milyar/Tahun' || $var['cus_penghasilan'] == '6') $incomelevel = '6';

    $arr[$idx] = $incomelevel;                                        $idx++;     //20. income level

    $maritalstatus = '';
    if($var['cus_status_kawin'] == 'Lajang/Single' || $var['cus_status_kawin'] == '1') $maritalstatus = '1';
    else if($var['cus_status_kawin'] == 'Menikah/Merried' || $var['cus_status_kawin'] == '2') $maritalstatus = '2';
    else if($var['cus_status_kawin'] == 'Janda/Duda' || $var['cus_status_kawin'] == '3') $maritalstatus = '3';

    $arr[$idx] = $maritalstatus;                                      $idx++;     //21. marital status
    $arr[$idx] = $var['spouses_name'];                                $idx++;     //22. spouses name
    $arr[$idx] = $var['investors_risk_profile'];                      $idx++;     //23. investors_risk_profile

    $objective = '';
    if($var['cus_maksud_tujuan'] == 'Mendapatkan Kenaikan Harga' || $var['cus_maksud_tujuan'] == '1') $objective = '1';
    else if($var['cus_maksud_tujuan'] == 'Investasi' || $var['cus_maksud_tujuan'] == '2') $objective = '2';
    else if($var['cus_maksud_tujuan'] == 'Spekulasi' || $var['cus_maksud_tujuan'] == '3') $objective = '3';
    else if($var['cus_maksud_tujuan'] == 'Mendapatkan Penghasilan/Pendapatan' || $var['cus_maksud_tujuan'] == '4') $objective = '4';
    else if($var['cus_maksud_tujuan'] == 'Lainnya' || $var['cus_maksud_tujuan'] == '5') $objective = '5';

    $arr[$idx] = $objective;                                          $idx++;     //24. invesment objective

    $source = '';
    if($var['cus_sumber_dana'] == 'Pengahasilan' || $var['cus_sumber_dana'] == '1') $source = '1';
    else if($var['cus_sumber_dana'] == 'Keuntungan Bisnis' || $var['cus_sumber_dana'] == '2') $source = '2';
    else if($var['cus_sumber_dana'] == 'Bunga Simpanan' || $var['cus_sumber_dana'] == '3') $source = '3';
    else if($var['cus_sumber_dana'] == 'Warisan' || $var['cus_sumber_dana'] == '4') $source = '4';
    else if($var['cus_sumber_dana'] == 'Dari Orang Tua/Anak' || $var['cus_sumber_dana'] == '5') $source = '5';
    else if($var['cus_sumber_dana'] == 'Hibah' || $var['cus_sumber_dana'] == '6') $source = '6';
    else if($var['cus_sumber_dana'] == 'Dari Suami/Istri' || $var['cus_sumber_dana'] == '7') $source = '7';
    else if($var['cus_sumber_dana'] == 'Undian' || $var['cus_sumber_dana'] == '8') $source = '8';
    else if($var['cus_sumber_dana'] == 'Hasil Investasi' || $var['cus_sumber_dana'] == '9') $source = '9';
    else if($var['cus_sumber_dana'] == 'Lainnya' || $var['cus_sumber_dana'] == '10') $source = '10';

    $arr[$idx] = $source;                                             $idx++;     //25. source of fund

    $arr[$idx] = $var['asset_owner'];                                 $idx++;     //26. asset owner
    $arr[$idx] = $var['cus_address'];                                 $idx++;     //27. ktp address

    $ARIAcityCode = $data->ARIA_city_name($var['cus_city']);

    $arr[$idx] = $var['cus_city'];                                       $idx++;     //28. ktp city
    $arr[$idx] = $var['cus_kode_pos'];                                $idx++;     //29. ktp postal code

    if($var['correspondence_type'] == 0){
        //corespondence tempat tinggal
        $arr[$idx] = $var['cus_address_t'];                               $idx++;     //30. correspondence address ambil dr tmpt tinggal

        $ARIAcityCodeCorrespondence = $data->ARIA_city_name($var['cus_city_t']);

        $arr[$idx] = $var['cus_city_t'];                                  $idx++;     //31. correspondence city code
        $arr[$idx] = $ARIAcityCodeCorrespondence;                         $idx++;     //32. correspondence city name (optional)
        $arr[$idx] = $var['cus_kode_pos_t'];                              $idx++;     //33. correspondence postal code
        
        $countryDomicily = 'ID';
        if($var['country_of_domicily'] != null && $var['country_of_domicily'] != '')
            $countryDomicily = $var['country_of_domicily'];
        
        //$arr[$idx] = $var['country_of_correspondence'];                   $idx++;     //34. country of correspondence
        $arr[$idx] = $countryDomicily;                   $idx++;     //34. country of correspondence
    }else{  
        //corespondence tempat kerja
        $arr[$idx] = $var['cus_address_c'];                               $idx++;     //30. correspondence address ambil dr tmpt tinggal

        $ARIAcityCodeCorrespondence = $data->ARIA_city_name($var['cus_city_c']);

        $arr[$idx] = $var['cus_city_c'];                                  $idx++;     //31. correspondence city code
        $arr[$idx] = $ARIAcityCodeCorrespondence;                         $idx++;     //32. correspondence city name (optional)
        $arr[$idx] = $var['cus_kode_pos_c'];                              $idx++;     //33. correspondence postal code
        
        $countryDomicily = 'ID';
        if($var['country_of_correspondence'] != null && $var['country_of_correspondence'] != '')
            $countryDomicily = $var['country_of_correspondence'];
        
        //$arr[$idx] = $var['country_of_correspondence'];                   $idx++;     //34. country of correspondence
        $arr[$idx] = $countryDomicily;                   $idx++;     //34. country of correspondence
    }
    $arr[$idx] = $var['cus_address_t'];                               $idx++;     //35. domicili address

    $ARIAcityCodeDomicily = $data->ARIA_city_name($var['cus_city_t']);

    $arr[$idx] = $var['cus_city_t'];                               $idx++;     //36. domicili city code
    $arr[$idx] = $ARIAcityCodeDomicily;                                                  $idx++;     //37. domicili city name (optional)
    $arr[$idx] = $var['cus_kode_pos_t'];                              $idx++;     //38. domicili postal code

    $countryDomicily = 'ID';
    if($var['country_of_domicily'] != null && $var['country_of_domicily'] != '')
        $countryDomicily = $var['country_of_domicily'];

    $arr[$idx] = $countryDomicily;                                    $idx++;     //39. country of domicili
    $arr[$idx] = $var['cus_telp'];                                    $idx++;     //40. home phone
    $arr[$idx] = $var['cus_phone'];                                   $idx++;     //41. mobile phone

    $arr[$idx] = $var['cus_fax'];                                     $idx++;     //42. fax
    $arr[$idx] = $var['cus_email'];                                   $idx++;     //43. email 
    $arr[$idx] = $var['statement_type'];                              $idx++;     //44. statement type
    $arr[$idx] = $var['fatca'];                                       $idx++;     //45. fatca
    $arr[$idx] = $var['tin'];                                         $idx++;     //46. tin
    $arr[$idx] = $var['tin_issuance_cuntry'];                         $idx++;     //47. tin issuance country

    if($type == '1'){
        $arr[$idx] = $var['bank_bic_code1'];                         $idx++;     //48. redm payment bank bic code
        $arr[$idx] = $var['bank_bi_member_code1'];                   $idx++;     //49. redm payment bank bi member code
        $arr[$idx] = $var['cus_bank_name'];                          $idx++;     //50. redm payment bank name 
        $arr[$idx] = $var['bank_country1'];                          $idx++;     //51. redm payment bank country
        $arr[$idx] = $var['bank_branch1'];                           $idx++;     //52. redm payment bank branch
        $arr[$idx] = $var['bank_ccy1'];                              $idx++;     //53. redm payment acc currency
        $arr[$idx] = $var['cus_account_number'];                     $idx++;     //54. redm payment acc no
        $arr[$idx] = $var['cus_account_name'];                       $idx++;     //55. redm payment acc name

        $bankccy2 = '';
        if($var['bank_bi_member_code2'] != '' || $var['bank_bic_code2'] != '' || $var['bank_acc_no2'] !='')
            $bankccy2 = $var['bank_ccy2'];

        $arr[$idx] = $var['bank_bic_code2'];                         $idx++;     //56. redm payment bank bic code 2
        $arr[$idx] = $var['bank_bi_member_code2'];                   $idx++;     //57. redm payment bank bi member code 2
        $arr[$idx] = $var['cus_bank_name2'];                         $idx++;     //58. redm payment bank name  2
        $arr[$idx] = $var['bank_country2'];                          $idx++;     //59. redm payment bank country 2
        $arr[$idx] = $var['bank_branch2'];                           $idx++;     //60. redm payment bank branch 2
        $arr[$idx] = $bankccy2;                              $idx++;     //61. redm payment acc currency 2
        $arr[$idx] = $var['cus_account_number2'];                    $idx++;     //62. redm payment acc no 2
        $arr[$idx] = $var['cus_account_name2'];                      $idx++;     //63. redm payment acc name 2

        $bankccy3 = '';
        if($var['bank_bi_member_code3'] != '' || $var['bank_bic_code3'] != '' || $var['bank_acc_no3'] !='')
            $bankccy3 = $var['bank_ccy3'];

        $arr[$idx] = $var['bank_bic_code3'];                         $idx++;     //64. redm payment bank bic code 3
        $arr[$idx] = $var['bank_bi_member_code3'];                   $idx++;     //65. redm payment bank bi member code 3
        $arr[$idx] = $var['bank_name3'];                             $idx++;     //66. redm payment bank name 3
        $arr[$idx] = $var['bank_country3'];                          $idx++;     //67. redm payment bank country 3
        $arr[$idx] = $var['bank_branch3'];                           $idx++;     //68. redm payment bank branch 3
        $arr[$idx] = $bankccy3;                              $idx++;     //69. redm payment acc currency 3
        $arr[$idx] = $var['bank_acc_no3'];                           $idx++;     //70. redm payment acc no 3
        $arr[$idx] = $var['bank_acc_name3'];                         $idx++;     //71. redm payment acc name 3
        $arr[$idx] = $var['client_code'];                            $idx++;     //72. client code
    }else{
        $arr[$idx] = '';                                            $idx++;     //48. redm payment bank bic code
        $arr[$idx] = '';                                            $idx++;     //49. redm payment bank bi member code
        $arr[$idx] = '';                                            $idx++;     //50. redm payment bank name 
        $arr[$idx] = '';                                            $idx++;     //51. redm payment bank country
        $arr[$idx] = '';                                            $idx++;     //52. redm payment bank branch
        $arr[$idx] = '';                                            $idx++;     //53. redm payment acc currency
        $arr[$idx] = '';                                            $idx++;     //54. redm payment acc no
        $arr[$idx] = '';                                            $idx++;     //55. redm payment acc name

        $arr[$idx] = '';                                            $idx++;     //56. redm payment bank bic code 2
        $arr[$idx] = '';                                            $idx++;     //57. redm payment bank bi member code 2
        $arr[$idx] = '';                                            $idx++;     //58. redm payment bank name  2
        $arr[$idx] = '';                                            $idx++;     //59. redm payment bank country 2
        $arr[$idx] = '';                                            $idx++;     //60. redm payment bank branch 2
        $arr[$idx] = '';                                            $idx++;     //61. redm payment acc currency 2
        $arr[$idx] = '';                                            $idx++;     //62. redm payment acc no 2
        $arr[$idx] = '';                                            $idx++;     //63. redm payment acc name 2

        $arr[$idx] = '';                                            $idx++;     //64. redm payment bank bic code 3
        $arr[$idx] = '';                                            $idx++;     //65. redm payment bank bi member code 3
        $arr[$idx] = '';                                            $idx++;     //66. redm payment bank name 3
        $arr[$idx] = '';                                            $idx++;     //67. redm payment bank country 3
        $arr[$idx] = '';                                            $idx++;     //68. redm payment bank branch 3
        $arr[$idx] = '';                                            $idx++;     //69. redm payment acc currency 3
        $arr[$idx] = '';                                            $idx++;     //70. redm payment acc no 3
        $arr[$idx] = '';                                            $idx++;     //71. redm payment acc name 3
        $arr[$idx] = '';                                            $idx++;     //72. client code
    }

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= str_replace("
", "", $arr[$j]);
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
}

fclose($output);
?>
