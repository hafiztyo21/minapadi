<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('subsredm_report_agent.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='a.subsredm_order_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$allocation 	= $data->cb_allocation('txt_allocation',$_POST[txt_allocation]," ");
$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));

$rowo = $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$txt_allocation."'");
$allocation2 = $rowo['A'];

$linkAdd = 'subsredm_order_add.php';
$linkExport = 'subsredm_order_export_filter.php';

$btnView = '<input type="submit" name="btnView" value="View">';
// $btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'\'">';
// $btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'\',\'Export\',\'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=350,height=300 \')">';

$datagrid = array();

if ($_POST['btnView']=='View'){
  	$transaction_date =  trim(htmlentities($_POST['transactionDate']));
  	$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
  	// $txt_name 	= trim(htmlentities($_POST['cus_name']));


	if (empty($transaction_date))
		$transaction_date = date("Y-m-d");
	
	//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';

  	$sql = "SELECT 
	  			a.subsredm_order_id
                , a.transaction_date as TRANSACTIONDATE
                , a.transaction_type as TRANSACTIONTYPE
                , a.sa_code as SACODE
                , a.investor_ac_no as INVESTORACNO
				, a.fund_code as FUNDCODE
				, a.amount as AMOUNT
				, a.amount_unit as AMOUNTUNIT
				, a.amount_all_unit as AMOUNTALLUNIT
				, a.fee as FEE
                , a.fee_unit as FEEUNIT
                , a.fee_persen as FEEPERSEN
				, a.payment_date as PAYMENTDATE
				, a.transfer_type as TRANSFERTYPE
                , a.sa_reference_no as SAREFERENCENO
                , b.allocation as ALLOCATION
                , c.ifua_code AS CODE_CUSTOMER
              
			FROM tbl_kr_subsredm_order a  
			LEFT JOIN tbl_kr_allocation b ON a.fund_code = b.fund_code
			LEFT JOIN tbl_kr_cus_sup_ifua c ON a.investor_ac_no = c.ifua_code
			WHERE DATE_FORMAT(a.transaction_date, '%Y-%m-%d') = '$transaction_date' AND a.is_deleted=0 and b.pk_id = '".$txt_allocation."' 
			ORDER BY $order_by $sort_order";

			// print_r($sql);die;
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->dataSubsRedmOrder($sql, 'edit', 'subsredm_order_edit.php', 'delete', 'subsredm_order.php');
}

if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "UPDATE tbl_kr_subsredm_order SET is_deleted=1 WHERE subsredm_order_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='subsredm_order.php';</script>";
	}
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','txt_name',$data->cb_client_contract('cus_name',$_POST[cus_name]));
$tmpl->addVar('page','allocation',$allocation);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addVar('page','export',$btnExport);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>