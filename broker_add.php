<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('broker_add.html');
$tablename = 'tbl_kr_broker';

if ($_POST['btn_save']=='save'){
	$flag = true;
	$type = $_GET['type'];
	$id = $_GET['id'];
	$fk = $_GET['fk'];
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
 		$txt_code = trim(htmlentities($_POST['txt_code']));
		$txt_type_buy = trim(htmlentities($_POST['txt_type_buy']));
		$txt_securitas_id = trim(htmlentities($_POST['txt_securitas_id']));
		$txt_lembar = trim(htmlentities($_POST['txt_lembar']));
		$txt_lembar = str_replace(',','',$txt_lembar);
		$txt_harga = trim(htmlentities($_POST['txt_harga']));

		$txt_total = trim(htmlentities($_POST['txt_total']));
		$txt_komisi = trim(htmlentities($_POST['txt_komisi']));
		$txt_levy = trim(htmlentities($_POST['txt_levy']));
		$txt_kpei = trim(htmlentities($_POST['txt_kpei']));
        $txt_pajak = trim(htmlentities($_POST['txt_pajak']));

		$txt_pajak_jual = trim(htmlentities($_POST['txt_pajak_jual']));
		$txt_pajak_komisi = trim(htmlentities($_POST['txt_pajak_komisi']));
		$txt_nilai_beli = trim(htmlentities($_POST['txt_nilai_beli']));
		$txt_nilai_jual = trim(htmlentities($_POST['txt_nilai_jual']));
		$txt_average_cost = trim(htmlentities($_POST['txt_average_cost']));
	
		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_lembar==''){
			echo "<script>alert('Lembar is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
  		//----------------perhitungan-------------------------------
  		
	
		$nilai = $txt_lembar * $txt_harga;
		$total_komisi =( $nilai * 0.1427272727 ) / 100;
		$total_levy = ( $nilai * 0.033 ) / 100;
		$total_kpei = ($nilai * 0.01) / 100;
		$total_pajak = ($total_komisi * 10) / 100;
		$total_pajak_jual = ($nilai * 0.1) / 100;
		$total_pajak_komisi = ($total_komisi * 2) / 100;
		$total_nilai_beli = ( $nilai + $total_komisi + $total_levy + $total_kpei + $total_pajak ) - $total_pajak_komisi;
		$total_nilai_jual = ( $nilai - $total_komisi - $total_levy - $total_kpei - $total_pajak - $total_pajak_jual ) + $total_pajak_komisi;
		$total_average_cost = $total_nilai_beli / $txt_lembar;
		//-----------------------end--cek-lembar
		
		//warning---------------------
		/*$value_shares = $data->get_row("select sum(total) as jumlah from tbl_kr_mst_saham");
		$value_bonds = $data->get_row("select sum(market_value) as jumlah from tbl_kr_me_bonds");
		$value_pn = $data->get_row("select sum(price_val) as jumlah from tbl_kr_pn");
		$value = ($value_shares['jumlah'] + $value_bonds['jumlah']) + $value_pn['jumlah'];
		$stock = $data->get_row("select total from tbl_kr_mst_saham where code = '".$txt_code."'");
		$total_all = $stock['total'] + $total;
        if ($value>0){
        	$jumlah = $total_all * (100/$value);
			$warning = $data->get_rows("SELECT * FROM tbl_kr_warning");
			$valred = ($warning[0][batas]);
			$valyellow = ($warning[1][batas]);
			$valgreen = ($warning[2][batas]);
			if ($jumlah >= $valred){
				$status_war = 1;
			}elseif ($jumlah >= $valyellow){
				$status_war = 2;
			}elseif ($jumlah < $valyellow){
				$status_war = 3;
			}
		}else{
			$status_war = 1;
		}*/
		//end warning -------
		
  			$sql = "INSERT INTO tbl_kr_broker (
			code,
			type_buy,
			securitas_id,
			lembar,
			harga,
			total,
			komisi,
			levy,
			kpei,
			pajak,
			pajak_jual,
			pajak_komisi,
			nilai_beli,
			nilai_jual,
			average_cost,
			fk_id,
			date_created
			)VALUES(
			'$txt_code',
			'$txt_type_buy',
			'$txt_securitas_id',
			'$txt_lembar',
			'$txt_harga',
			'$txt_total',
			'$txt_komisi',
			'$txt_levy',
			'$txt_kpei',
			'$txt_pajak',
			'$txt_pajak_jual',
			'$txt_pajak_komisi',
			'$txt_nilai_beli',
			'$txt_nilai_jual',
			'$txt_average_cost',
			'$fk',
			 now())";
			//print($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='broker.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
	}
}

if ($_GET['add']==1){
$tittle = "ADD BROKER";
$id = $_GET['id'];
$fk = $_GET['fk'];
$dataRows = array (
	'TEXT' =>  array('Code','Type Buy','Securitas','Lembar','Harga','Total','Komisi','Levy','Kpei','Pajak','Pajak Jual','Pajak Komisi','Nilai Beli','Nilai Jual','Average Cost'),
  	'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':',':',':',':'),
	'FIELD' => array (
		$data->cb_saham_contract('txt_code',$_POST[txt_code]),
		$data->cb_saham_status('txt_type_buy',$_POST[txt_type_buy]),
		$data->cb_me_dealer_type('txt_securitas_id',$_POST[txt_securitas_id]," OnChange= \"getNew(this.value,'".$id."','".$fk."');\" ")."&nbsp;&nbsp;
		<span id='value'></span>",
		"<input style='text-align: left;' type=text size='20' name=txt_lembar value='".$_POST[txt_lembar]."' onKeyPress=\"return(currencyFormat(this,',',',',event))\">",
		"<input type=text size='20' name=txt_harga value='".$_POST[txt_harga]."'>",
		"<input type=text name=txt_total readonly value='".$nilai."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_komisi readonly value='".$total_komisi."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_levy readonly value='".$total_levy."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_kpei readonly value='".$total_kpei."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_pajak readonly value='".$total_pajak."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_pajak_jual readonly value='".$total_pajak_jual."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_pajak_komisi readonly value='".$total_pajak_komisi."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_nilai_beli readonly value='".$total_nilai_beli."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_nilai_jual readonly value='".$total_nilai_jual."' onKeyPress='return checkIt(event)'>",
		"<input type=text name=txt_cost_average readonly value='".$total_average_cost."' onKeyPress='return checkIt(event)'>",
		)
	);
	#$data->showsql($sql);
       $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='broker.php';\">");
}
if ($_GET['detail']==1){
	$rows = $data->selectQuery("select * from ".$tablename." where broker_id ='".$_GET['id']."'");
	$dataRows = array (
	'TEXT' =>  array('Code','Type Buy','Securitas','Lembar','Harga','Total','Komisi','Levy','Kpei','Pajak','Pajak Jual','Pajak Komisi','Nilai Beli','Nilai Jual','Average Cost'),
  	'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':',':',':',':'),
	'FIELD' => array (				
				"&nbsp;".$rows['code'],
				"&nbsp;".$rows['type_buy'],
				"&nbsp;".$rows['securitas_id'],
				"&nbsp;".$rows['lembar'],
				"&nbsp;".$rows['harga'],
				"&nbsp;".$rows['total'],
				"&nbsp;".$rows['komisi'],
				"&nbsp;".$rows['levy'],
				"&nbsp;".$rows['kpei'],
				"&nbsp;".$rows['pajak'],
				"&nbsp;".$rows['pajak_jual'],
				"&nbsp;".$rows['pajak_komisi'],
				"&nbsp;".$rows['nilai_beli'],
				"&nbsp;".$rows['nilai_jual'],
				"&nbsp;".$rows['average_cost']
				)
          			  );


	$tittle = "BROKER DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}

if ($_POST['btn_save_edit'])
{

	$country = explode(';',$_POST['txt_country']);
	$sql= "UPDATE ".$tablename." SET  cus_name = '".$_POST['txt_cus_name']."', cus_address = '".$_POST['txt_address']."'
			, cus_rt = '".$_POST['txt_rt']."',cus_rw = '".$_POST['txt_rw']."'
			, cus_city = '".$_POST['txt_city']."', cus_tgl_lahir = '".$_POST['txt_tgl_lahir']."'
			, cus_phone = '".$_POST['txt_phone']."', cus_fax = '".$_POST['txt_fax']."'
			, cus_email = '".$_POST['txt_email']."', cus_agama = '".$_POST['txt_agama']."'
			, cus_other = '".$_POST['txt_other']."', cus_actived = '".$_POST['txt_cus_active']."'
			, cus_no_npwp_1 = '".$_POST['txt_npwp1']."', cus_no_npwp_2 = '".$_POST['txt_npwp2']."'
			, cus_no_npwp_3 = '".$_POST['txt_npwp3']."', cus_no_npwp_4 = '".$_POST['txt_npwp4']."'
			, cus_no_npwp_5 = '".$_POST['txt_npwp5']."', cus_no_npwp_6 = '".$_POST['txt_npwp6']."'
			, cus_bank_name = '".$_POST['txt_name_bank']."', cus_account_name = '".$_POST['txt_account_name']."', cus_account_number = '".$_POST['txt_account_number']."'
			, cus_no_ktp = '".$_POST['txt_no_ktp']."', cus_expired_ktp = '".$_POST['txt_expired_ktp']."'
			WHERE cus_id = '".$_GET['id']."'";
   # $data->showsql($sql);
	if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('s01')."');opener.document.form1.submit();window.parent.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}
}

if ($_GET['edit'] == 1)
{   #$data->auth('09030102',$_SESSION['user_id']);
	$rows = $data->selectQuery("select * from ".$tablename." where cus_id ='".$_GET['id']."'");
#print_r($_POST);
if($_POST){
	$cus_name = $_POST['txt_cus_name'];
	$cus_address = $_POST['txt_address'];
	$cus_rt = $_POST['txt_rt'];
	$cus_rw = $_POST['txt_rw'];
	$cus_city = $_POST['txt_city'];
	$cus_tgl_lahir = $_POST['txt_tgl_lahir'];
	$cus_phone = $_POST['txt_phone'];
	$cus_fax = $_POST['txt_fax'];
	$cus_email = $_POST['txt_email'];
	$cus_agama = $_POST['txt_agama'];
	$cus_other = $_POST['txt_other'];
	$cus_actived = $_POST['txt_cus_active'];
	$cus_no_npwp_1 = $_POST['txt_npwp1'];
	$cus_no_npwp_2 = $_POST['txt_npwp2'];
	$cus_no_npwp_3 = $_POST['txt_npwp3'];
	$cus_no_npwp_4 = $_POST['txt_npwp4'];
	$cus_no_npwp_5 = $_POST['txt_npwp5'];
	$cus_no_npwp_6 = $_POST['txt_npwp6'];

	$name_bank = $_POST['txt_name_bank'];
	$account_name = $_POST['txt_account_name'];
	$account_number = $_POST['txt_account_number'];
	$cus_no_ktp = $_POST['txt_no_ktp'];
	$cus_expired_ktp = $_POST['txt_expired_ktp'];
	
}else{
	$cus_name = $rows['cus_name'];
	$cus_address = $rows['cus_address'];
	$cus_rt = $rows['cus_rt'];
	$cus_rw = $rows['cus_rw'];
	$cus_city = $rows['cus_city'];
	$cus_tgl_lahir = $rows['cus_tgl_lahir'];
	$cus_phone = $rows['cus_phone'];
	$cus_fax = $rows['cus_fax'];
	$cus_email = $rows['cus_email'];
	$cus_agama = $rows['cus_agama'];
	$cus_other = $rows['cus_other'];
	$cus_actived = $rows['cus_actived'];
	$cus_no_npwp_1 = $rows['cus_no_npwp_1'];
	$cus_no_npwp_2 = $rows['cus_no_npwp_2'];
	$cus_no_npwp_3 = $rows['cus_no_npwp_3'];
	$cus_no_npwp_4 = $rows['cus_no_npwp_4'];
	$cus_no_npwp_5 = $rows['cus_no_npwp_5'];
	$cus_no_npwp_6 = $rows['cus_no_npwp_6'];

	$name_bank = $rows['cus_bank_name'];
	$account_name = $rows['cus_account_name'];
	$account_number = $rows['cus_account_number'];
	$cus_no_ktp = $rows['cus_no_ktp'];
	$cus_expired_ktp = $_POST['txt_expired_ktp'];
}

	$dataRows = array (
				'TEXT' => array('CUSTOMER / SUPPLIER NAME','ADDRESS','RT','RW','CITY','TANGGAL LAHIR','PHONE','FAX','EMAIL','AGAMA',
				'BANK','ACCOUNT NAME','ACCOUNT NUMBER',
				'OTHER','CUSTOMER ACTIVED','NPWP','NO KTP','EXPIRED KTP'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array ("<input type=text name=txt_cus_name id=txt_cus_name value='".$cus_name."'>",
				"<textarea rows=3 cols=17 name=txt_address>".$cus_address."</textarea>",
				"<input type=text name=txt_rt id=txt_rt value='".$cus_rt."'>",
				"<input type=text name=txt_rw id=txt_rw value='".$cus_rw."'>",
				"<input type=text name=txt_city id=txt_city value='".$cus_city."'>",
				"<input type=text name=txt_tgl_lahir id=txt_tgl_lahir value='".$cus_tgl_lahir."'>",
				"<input type=text name=txt_phone id=txt_phone value='".$cus_phone."'>",
				"<input type=text name=txt_fax id=txt_fax value='".$cus_fax."'>",
				"<input type=text name=txt_email id=txt_email value='".$cus_email."'>",
				"<input type=text name=txt_agama id=txt_agama value='".$cus_agama."'>",

				"<input type=text name=txt_name_bank id=txt_name_bank value='".$name_bank."'>",
				"<input type=text name=txt_account_name id=txt_account_name value='".$account_name."'>",
				"<input type=text name=txt_account_number id=txt_account_number value='".$account_number."'>",

				"<textarea rows=3 cols=17 name=txt_other>".$cus_other."</textarea>",
				$data->cb_customer_status('txt_cus_active',$cus_actived),
				"<input type=text name=txt_npwp1 id=txt_npwp1 size='2'  onKeyPress='return checkIt(event)' maxlength='2' onKeyUp='valid_length1()' value='".$cus_no_npwp_1."'>.
				<input type=text name=txt_npwp2 id=txt_npwp2 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length2()' value='".$cus_no_npwp_2."'>.
				<input type=text name=txt_npwp3 id=txt_npwp3 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length3()' value='".$cus_no_npwp_3."'>.
				<input type=text name=txt_npwp4 id=txt_npwp4 size='2'  onKeyPress='return checkIt(event)' maxlength='1' onKeyUp='valid_length4()' value='".$cus_no_npwp_4."'>.
				<input type=text name=txt_npwp5 id=txt_npwp5 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length5()' value='".$cus_no_npwp_5."'>.
				<input type=text name=txt_npwp6 id=txt_npwp6 size='5'  onKeyPress='return checkIt(event)' maxlength='3' value='".$cus_no_npwp_6."'>",
				"<input type=text name=txt_no_ktp id=txt_no_ktp value='".$cus_no_ktp."'>",
				"<input type=text name=txt_expired_ktp id=txt_expired_ktp value='".$_POST['txt_expired_ktp']."'>")
          			  );

    $tittle = "CUSTOMER / CLIENT EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"opener.document.form1.submit();window.parent.close();\">"
					);
}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>