<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('manual.html');

$dataRows = array (
	'TEXT' =>  array('Kas pada Giro BII RD Keraton (Rp.)','Total Piutang (Rp.)','Aktiva Lain-lain (Rp.)','Total Kewajiban (Rp.)','Jumlah Up Beredar (Rp.)'),
  	'DOT'  => array (':',':',':',':',':'),
	'FIELD' => array (
		"<input type=text size='20' name=txt_kas value='".$_POST[txt_kas]."'>",
		"<input type=text size='20' name=txt_piutang value='".$_POST[txt_piutang]."'>",
		"<input type=text size='20' name=txt_aktiva value='".$_POST[txt_aktiva]."'>",
		"<input type=text size='20' name=txt_kewajiban value='".$_POST[txt_kewajiban]."'>",
		"<input type=text size='20' name=txt_up value='".$_POST[txt_up]."'>",
	)
		);
$button = array ('SUBMIT' => "<input type=submit name=btn_save value=OK>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='manual.php';\">");


if ($_POST['btn_save']=='OK'){
$txt_kas = number_format(trim(htmlentities($_POST['txt_kas'])),2);
$txt_piutang = number_format(trim(htmlentities($_POST['txt_piutang'])),2);
$txt_aktiva = number_format(trim(htmlentities($_POST['txt_aktiva'])),2);
$txt_kewajiban = number_format(trim(htmlentities($_POST['txt_kewajiban'])),2);
$txt_up = number_format(trim(htmlentities($_POST['txt_up'])),2);
if($txt_kas=='0.00'){
			echo "<script>alert('Kas is Empty!');</script>";
}elseif($txt_piutang=='0.00'){
			echo "<script>alert('Piutang is Empty!');</script>";
}
elseif($txt_aktiva=='0.00'){
			echo "<script>alert('Aktiva is Empty!');</script>";
}
elseif($txt_kewajiban=='0.00'){
			echo "<script>alert('Kewajiban is Empty!');</script>";
}
elseif($txt_up=='0.00'){
			echo "<script>alert('Jumlah Up is Empty!');</script>";
}else{
echo "<script>window.location='report_print.php?kas=$txt_kas&piutang=$txt_piutang&aktiva=$txt_aktiva&kewajiban=$txt_kewajiban&up=$txt_up';</script>";
}}
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>