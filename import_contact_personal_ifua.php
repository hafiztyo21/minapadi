<?php
session_start();
include 'Classes/excel_reader2.php';
//include 'Classes/PHPExcel/Writer/Excel2007.php';
//include 'Classes/PHPExcel/IOFactory.php';
//include "excel_reader2.php";
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$datadb = new globalFunction();
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('import_contact_personal_ifua.html');
$tablename = 'tbl_kr_cus_sup';
$tablename_detail = 'tbl_kr_cus_sup_ifua';


if($_POST[Submit]) {

    $myfile = fopen($_FILES['uploadedfile']['tmp_name'], "r") or die("Unable to open file!");
    // Output one line until end-of-file
    $suksesUpdate = 0;
    $suksesInsertIfua = 0;
    $gagalUpdate = 0;
    $gagalInsertIfua = 0;

    $errorResult = "";

    while(!feof($myfile)) {
        //echo fgets($myfile) . "<br>";
        $line = fgets($myfile);        
        if(strpos($line, 'SA CODE') !== false){

        }else{
            $param = explode("|",$line);
            if(count($param) == 1) continue;   
            $sid = $param[2];
            $ifua = $param[3];
            $ifuaName = $param[4];
            $clientCode = $param[5];
            $idNo = $param[12];

            $cek_data=$datadb->get_value("select count(cus_id) from tbl_kr_cus_sup where cus_no_identity='".$idNo."'");
            if ($cek_data ==0){
                $errorResult .= "Data Id no '$idNo' tidak di temukan dalam Database<br/>";
                $gagalInsertIfua++;
            }else{
                $query = "SELECT cus_id, client_code,bank_bic_code1, bank_bi_member_code1, cus_account_number, cus_bank_name, cus_account_name, bank_country1, bank_branch1, bank_ccy1  FROM tbl_kr_cus_sup WHERE cus_no_identity='".$idNo."'";
                $row = $datadb->get_row($query);
                
                if($row['client_code'] == ''){
                    $query = "UPDATE tbl_kr_cus_sup SET client_code='".$client_code."' WHERE cus_no_identity='".$idNo."'";  
                    if ($datadb->inpQueryReturnBool($query))
                    {	//echo "<script>alert('SUKSES');</script>";	
                        //$suksesUpdate++;
                    }
                    else
                    {	//echo "<script>alert('GAGAL');</script>";
                        //$gagalUpdate++;
                    }
                }
                if($row['cus_sid'] == ''){
                    $query = "UPDATE tbl_kr_cus_sup SET cus_sid='".$sid."' WHERE cus_no_identity='".$idNo."'";  
                    if ($datadb->inpQueryReturnBool($query))
                    {	//echo "<script>alert('SUKSES');</script>";	
                        //$suksesUpdate++;
                    }
                    else
                    {	//echo "<script>alert('GAGAL');</script>";
                        //$gagalUpdate++;
                    }
                }

                $bicCode = $row['bank_bic_code1'];
                $biMemberCode = $row['bank_bi_member_code1'];
                $accNumber = $row['cus_account_number'];
                $bankName = $row['cus_bank_name'];
                $accName = $row['cus_account_name'];
                $bankCountry = $row['bank_country1'];
                $bankBranch = $row['bank_branch1'];
                $bankCcy = $row['bank_ccy1'];
                
                $query = "SELECT count(ifua_code) FROM tbl_kr_cus_sup_ifua WHERE is_deleted=0 and ifua_code = '$ifua'";
                $ifuadata = $datadb->get_value($query);
                if($ifuadata == 0){
                    $saved = false;
                    $query = "INSERT INTO tbl_kr_cus_sup_ifua (cus_id, ifua_code, ifua_name, created_date, created_by, last_updated_date, last_updated_by, is_deleted)
                    VALUES ('".$row['cus_id']."','".$ifua."','".$ifuaName."',now(),'".$_SESSION['pk_id']."', now(), '".$_SESSION['pk_id']."',0)";  
                    if ($datadb->inpQueryReturnBool($query))
                    {	//echo "<script>alert('SUKSES');</script>";	
                        $suksesInsertIfua++;
                        $saved = true;
                    }
                    else
                    {	//echo "<script>alert('GAGAL');</script>";
                        $errorResult .= "Ifua '$ifua' gagal disimpan ke dalam Database (".$datadb->queryError().")<br/>";
                        $gagalInsertIfua++;
                    }
                    if($saved){
                        $lastid = $datadb->getLastId();

                        $query = "INSERT INTO tbl_kr_cus_sup_ifua_redm (ifua_id, sequential_code, bic_code, bi_member_code, bank_name, bank_country, bank_branch, ac_ccy, ac_name, ac_no, created_date, created_by, last_updated_date, last_updated_by, is_deleted)
                        VALUES ($lastid, '1', '$bicCode', '$biMemberCode', '$bankName', '$bankCountry', '$bankBranch', '$bankCcy', '$accName', '$accNumber', now(), '".$_SESSION['pk_id']."', now(), '".$_SESSION['pk_id']."', 0)";
                        if ($datadb->inpQueryReturnBool($query))
                        {		
                        }
                        else
                        {	
                            $errorResult .= "Ifua '$ifua' gagal disimpan Redemp Payment A/C ke dalam Database (".$datadb->queryError().")<br/>";
                        }
                    }
                    
                }else{
                    $errorResult .= "Ifua '$ifua' sudah ada dalam Database<br/>";

                }
                
            }

        }
        
        

    }
    fclose($myfile);
        
    // tampilan status sukses dan gagal
    echo "<h3>Proses import data selesai.</h3>";
    echo "<p>Jumlah data yang sukses diimport : ".$suksesInsertIfua."<br>";
    echo "Jumlah data yang gagal diimport : ".$gagalInsertIfua."</p>";
    if($errorResult != "")
	    echo "Error Log : <br/>".$errorResult;
	
}



$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->addVar('page', 'tittle',$tittle);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>