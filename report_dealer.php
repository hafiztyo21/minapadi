<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('report_dealer.html');

############################
$date=date("d-M-y",strtotime($_POST['txt_tanggal']));
//print_r($date);
$tanggal=$data->datePicker('txt_tanggal', $_POST[txt_tanggal],'');
$txt_tanggal = trim(htmlentities($_POST['txt_tanggal']));
$tanggal_to=$data->datePicker('txt_tanggal_to', $_POST[txt_tanggal_to],'');
$txt_tanggal_to = trim(htmlentities($_POST['txt_tanggal_to']));

$allocation	= $data->cb_allocation('txt_allocation',$_POST[txt_allocation]," ");
$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
$rowo = $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$txt_allocation."'");
$allocation2 = $rowo['A'];
 #######################
$tombol= "<input type=submit name=btn_view value=View>";

if($_GET['cek']=='1'){
	$txt_tanggal = trim(htmlentities($_POST['txt_tanggal']));
	$txt_tanggal_to = trim(htmlentities($_POST['txt_tanggal_to']));
	$txt_allocation = trim(htmlentities($_POST['txt_allocation']));
	/*
	
	$sql = "
			SELECT * FROM 
				(
					SELECT CODE AS kd_jual, SUM(lembar) AS lembar_jual, SUM(harga) AS harga_jual, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.securitas_type AS securitas_jual
					FROM tbl_kr_dealer_tmp 
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
					WHERE DATE(create_dt)='$txt_tanggal'
					AND tbl_kr_dealer_tmp.allocation = '$txt_allocation'
					AND type_buy='Sell'
					GROUP BY securitas_jual, kd_jual
					ORDER BY create_dt DESC
				) a,
				(
					SELECT CODE AS kd_beli, SUM(lembar) AS lembar_beli, SUM(harga) AS harga_beli, DATE(create_dt) AS tgl_beli, tbl_kr_securitas.securitas_type AS securitas_beli
					FROM tbl_kr_dealer_tmp 
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
					WHERE DATE(create_dt)='$txt_tanggal'
					AND tbl_kr_dealer_tmp.allocation = '$txt_allocation'
					AND type_buy='Buy'
					GROUP BY securitas_beli, kd_beli
					ORDER BY create_dt DESC
				) b
			WHERE kd_jual=kd_beli AND securitas_jual=securitas_beli
			GROUP BY tgl_jual, tgl_beli, securitas_jual, securitas_beli, kd_jual, kd_beli
			ORDER BY tgl_jual DESC, tgl_beli DESC
	";
	*/

	$sql1 = "
				SELECT SUM(market_value) AS TTL_NIL_OB FROM tbl_kr_me_bonds 
				WHERE 1 AND allocation ='".$txt_allocation."' AND trade_date BETWEEN '".$txt_tanggal."' AND '".$txt_tanggal_to."'  
				GROUP BY allocation
	";

	$sql_1 = "
				SELECT a.*, (a.NILAI_OB/b.TTL_NIL_OB*100) AS P_OB, b.*
				FROM(
					SELECT a.seller AS NAMA_OB, SUM(market_value) AS NILAI_OB
					FROM tbl_kr_me_bonds a
					WHERE 1 AND allocation ='".$txt_allocation."' AND trade_date BETWEEN '".$txt_tanggal."' AND '".$txt_tanggal_to."'  
					GROUP BY allocation,seller
				) a,
				(
					SELECT SUM(market_value) AS TTL_NIL_OB FROM tbl_kr_me_bonds 
					WHERE 1 AND allocation ='".$txt_allocation."' AND trade_date BETWEEN '".$txt_tanggal."' AND '".$txt_tanggal_to."' 
					GROUP BY allocation
				) b
	";

	$sql_2 = "
				SELECT a.*, (a.NILAI_SHM/b.TTL_NIL_SHM*100) AS P_SHM, b.*
				FROM (
					SELECT SUM(harga*lembar) AS NILAI_SHM, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.description AS NAMA_SHM FROM tbl_kr_dealer 
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
					WHERE DATE(create_dt) BETWEEN '".$txt_tanggal."' AND '".$txt_tanggal_to."' AND tbl_kr_dealer.allocation = '".$txt_allocation."' AND type_buy='Sell' GROUP BY allocation, NAMA_SHM 
				) a,
				(
					SELECT SUM(harga*lembar) AS TTL_NIL_SHM, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.description AS NAMA_SHM1 FROM tbl_kr_dealer
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
					WHERE DATE(create_dt) BETWEEN '".$txt_tanggal."' AND '".$txt_tanggal_to."' AND tbl_kr_dealer.allocation = '".$txt_allocation."' AND type_buy='Sell' GROUP BY allocation
				) b
	";
#echo $sql_2;
/*
	$sql_3 = "
				SELECT a.*, (a.NILAI_SHM/b.TTL_NIL_SHM*100) AS P_SHM, b.*
				FROM (
					SELECT SUM(harga*lembar) AS NILAI_SHM, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.description AS NAMA_SHM FROM tbl_kr_dealer 
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
					WHERE DATE(create_dt)='2016-02-05' AND tbl_kr_dealer.allocation = '3' AND type_buy='Sell' GROUP BY tgl_jual, NAMA_SHM 
				) a,
				(
					SELECT SUM(harga*lembar) AS TTL_NIL_SHM, DATE(create_dt) AS tgl_jual FROM tbl_kr_dealer 
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
					WHERE DATE(create_dt)='2016-02-05' AND tbl_kr_dealer.allocation = '3' AND type_buy='Sell' GROUP BY tgl_jual
				) b
				UNION 
				SELECT a.*, (a.NILAI_OB/b.TTL_NIL_OB*100) AS P_OB, b.*
				FROM(
					SELECT a.seller AS NAMA_OB, SUM(market_value) AS NILAI_OB
					FROM tbl_kr_me_bonds a
					WHERE 1 AND allocation ='".$txt_allocation."' AND trade_date ='".$txt_tanggal."'  
					GROUP BY trade_date,allocation,seller
				) a,
				(
					SELECT SUM(market_value) AS TTL_NIL_OB FROM tbl_kr_me_bonds 
					WHERE 1 AND allocation ='".$txt_allocation."' AND trade_date ='".$txt_tanggal."'  
					GROUP BY trade_date,allocation
				) b
				WHERE 
	";
*/
	$qry1=mysql_query($sql1);
	$row1=mysql_fetch_assoc($qry1);
	$ttl_nil_ob=number_format($row1[TTL_NIL_OB],0);
	$ttl_p_ob="100%";
	
	$qry2=mysql_query($sql_2);
	$row2=mysql_fetch_assoc($qry2);
	$ttl_nil_shm=number_format($row2[TTL_NIL_SHM],0);
	$ttl_p_shm="100%";
/*
	$qry3a=mysql_query($sql_1);
	$numrow3a=mysql_num_rows($qry3a);
	if($numrow3a<1){
		$sql_3="					
				SELECT a.*, (a.NILAI_SHM/b.TTL_NIL_SHM*100) AS P_SHM, b.*
				FROM (
					SELECT SUM(harga*lembar) AS NILAI_SHM, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.description AS NAMA_SHM FROM tbl_kr_dealer 
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
					WHERE DATE(create_dt)='".$txt_tanggal."' AND tbl_kr_dealer.allocation = '".$txt_allocation."' AND type_buy='Sell' GROUP BY tgl_jual, NAMA_SHM 
				) a,
				(
					SELECT SUM(harga*lembar) AS TTL_NIL_SHM, DATE(create_dt) AS tgl_jual FROM tbl_kr_dealer 
					LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
					WHERE DATE(create_dt)='".$txt_tanggal."' AND tbl_kr_dealer.allocation = '".$txt_allocation."' AND type_buy='Sell' GROUP BY tgl_jual
				) b
		";
		#echo $sql_3;
		$qry3b=mysql_query($sql_3);
		while($row3b=mysql_fetch_assoc($qry3b)){
				$nama_all=$row3b['NAMA_SHM'];			
				#$nama_all = $nama_sec1;
				$nilai_all = $row3b['NILAI_SHM'];
		}

	} else {
		while($row3a=mysql_fetch_assoc($qry3a)){
			$nama_sec=$row3a['NAMA_OB'];
			$nama_sec=str_replace("PT ", "", $nama_sec);
			if($nama_sec!=''){
				$sql_3="					
						SELECT a.*, (a.NILAI_SHM/b.TTL_NIL_SHM*100) AS P_SHM, b.*
						FROM (
							SELECT SUM(harga*lembar) AS NILAI_SHM, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.description AS NAMA_SHM FROM tbl_kr_dealer 
							LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
							WHERE DATE(create_dt)='".$txt_tanggal."' AND tbl_kr_dealer.allocation = '".$txt_allocation."' AND type_buy='Sell' GROUP BY tgl_jual, NAMA_SHM 
						) a,
						(
							SELECT SUM(harga*lembar) AS TTL_NIL_SHM, DATE(create_dt) AS tgl_jual FROM tbl_kr_dealer 
							LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer.securitas_id = tbl_kr_securitas.securitas_id 
							WHERE DATE(create_dt)='".$txt_tanggal."' AND tbl_kr_dealer.allocation = '".$txt_allocation."' AND type_buy='Sell' GROUP BY tgl_jual
						) b
				";
				#echo $sql_3;
				$qry3b=mysql_query($sql_3);
				while($row3b=mysql_fetch_assoc($qry3b)){
					$nama_sec1=$row3b['NAMA_SHM'];
					if($nama_sec==$nama_sec1){
						$nama_all = $nama_sec1;
						$nilai_all = $row3a['NILAI_OB']+$row3b['NILAI_SHM'];
					}
					else{					
						$nama_all = $nama_sec1;
						$nilai_all = $row3b['NILAI_SHM'];
					}
				}
			}
		}
	}
	*/
	$ttl_nil_alls=$row1[TTL_NIL_OB]+$row2[TTL_NIL_SHM];
	$ttl_p_all="100%";
	$tgl_terbilang=date('d F Y',strtotime($txt_tanggal_to));
	#$qry3=mysql_query($sql_3);

	$searchCB = $data->searchDG($arrFields,'');
	$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$DG=$data->dataGridDlr1($sql_1,'pk_id',$data->ResultsPerPage,$pg,'');
	$DG1=$data->dataGridDlr2($sql_2,'pk_id',$data->ResultsPerPage,$pg,'');
	$DG2=$data->dataGridDlr3($sql_1,'pk_id',$txt_tanggal,$txt_tanggal_to,$txt_allocation,$ttl_nil_alls,$data->ResultsPerPage,$pg,'');
	$ttl_nil_all=number_format($ttl_nil_alls,0);
}
else{	
	if($data->auth_boolean(1717,$_SESSION['pk_id'])){
		return header('location:report_dealer.php?cek=1');
	}
}


#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);
/*
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);
*/
$tmpl->addVar('page','tanggal',$tanggal);
$tmpl->addVar('page','tanggalto',$tanggal_to);
$tmpl->addVar('page','tgl',$txt_tanggal);
$tmpl->addVar('page','tglto',$txt_tanggal_to);
$tmpl->addVar('page','tgl_terbilang',$tgl_terbilang);
$tmpl->addVar('page','alloc',$txt_allocation);
$tmpl->addVar('page','allocation',$allocation);
$tmpl->addVar('page','tombol',$tombol);
$tmpl->addVar('page','TTL_NIL_OB',$ttl_nil_ob);
$tmpl->addVar('page','TTL_P_OB',$ttl_p_ob);
$tmpl->addVar('page','TTL_NIL_SHM',$ttl_nil_shm);
$tmpl->addVar('page','TTL_P_SHM',$ttl_p_shm);
#$tmpl->addVar('page','NAMA_ALL',$nama_all);
#$tmpl->addVar('page','NILAI_ALL',$nilai_all);
#$tmpl->addVar('page','P_ALL',$p_all);
$tmpl->addVar('page','TTL_NIL_ALL',$ttl_nil_all);
$tmpl->addVar('page','TTL_P_ALL',$ttl_p_all);
$tmpl->addVar('page','tombol',$tombol);
$tmpl->addVar('page','NODATA',$NODATA);
$tmpl->addRows('loopData',$DG);
$tmpl->addRows('loopData1',$DG1);
$tmpl->addRows('loopData2',$DG2);
$tmpl->displayParsedTemplate('page');
?>