<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'si.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new si;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('si_fixed_income_between_funds.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_si_fixed_income.si_fixed_income_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$linkAdd = 'si_fixed_income_between_funds_form.php';
$linkExport = 'si_fixed_income_between_funds_export.php';
$tablename = 'tbl_kr_si_fixed_income';

$btnView = '<input type="submit" name="btnView" value="View">';
$btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'\'">';
$btnExport = '<input type="button" name="btnExport" value="Export" onclick="alert(\'Please choose transaction date first\');">';

$datagrid = array();

if ($_POST['btnView']=='View'){
  	$transaction_date =  trim(htmlentities($_POST['transactionDate']));
	if (empty($transaction_date))
		$transaction_date = date("Y-m-d");

	$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';
	
  	$sql = "SELECT 
	  			si_fixed_income_id
				, trade_id as TRADEID
				, trade_date as TRADEDATE
				, settlement_date as SETTLEMENTDATE
				, fund_code as FUNDCODE
				, fund_code_buyer as FUNDCODEBUYER
			FROM $tablename
			WHERE DATE_FORMAT($tablename.trade_date, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0 AND `type`='3'
			ORDER BY $order_by $sort_order";
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->siData2($sql, 'edit', 'si_fixed_income_between_funds_form.php', 'delete', 'si_fixed_income_between_funds.php', 'Tax', 'si_fixed_income_tax.php', 'si_fixed_income_id');
}

if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "UPDATE $tablename SET is_deleted=1 WHERE si_fixed_income_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='si_fixed_income_between_funds.php';</script>";
	}
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addVar('page','export',$btnExport);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>