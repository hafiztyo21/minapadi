<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('time_deposit_add.html');

$action_type = '1';
$im_code = 'IM002';
$fund_code = '';
$placement_bank_code = '';
$branch_code = '';

$placement_bank_security_acc_no = '';
$placement_bank_cash_acc_no = '';
$ccy = '';
$principle = '0';
$interest_rate = '0';

$placement_date = date('Y-m-d');
$maturity_date = date('Y-m-d');
$interest_frequency = '1';
$interest_type = '1';
$sharia_deposit = '1';

$withdrawal_date = date('Y-m-d');
$adjusted_interest_rate = '0';
$withdrawal_principle = '0';
$withdrawal_interest = '0';
$total_withdrawal_amount = '0';

$rollover_type = '1';
$new_principle_amount = '0';
$new_interest_rate = '0';
$new_maturity_date = date('Y-m-d');
$statutory_type = '1';

$contact_person = '';
$telephone_no = '';
$fax_no = '';
$reference_no = '';
$parent_reference_no = '';

$description = '';

$errorArr = array('','','',''
    ,'','','','',''
    ,'','','','',''
    ,'','','','',''
    ,'','','','',''
    ,'','','','',''
    ,'');
$otherError='';

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
 		
        //$imCode = trim(htmlentities($_POST['imCode']));
        $action_type = trim(htmlentities($_POST['action_type']));
        //$im_code = 'IM002';
        $fund_code = trim(htmlentities($_POST['fund_code']));
        $placement_bank_code = trim(htmlentities($_POST['placement_bank_code']));
        $branch_code = trim(htmlentities($_POST['branch_code']));

        $placement_bank_security_acc_no = trim(htmlentities($_POST['placement_bank_security_acc_no']));
        $placement_bank_cash_acc_no = trim(htmlentities($_POST['placement_bank_cash_acc_no']));
        $ccy = trim(htmlentities($_POST['ccy']));
        $principle = trim(htmlentities($_POST['principle']));
        $interest_rate = trim(htmlentities($_POST['interest_rate']));

        $placement_date = trim(htmlentities($_POST['placement_date']));
        $maturity_date = trim(htmlentities($_POST['maturity_date']));
        $interest_frequency = trim(htmlentities($_POST['interest_frequency']));
        $interest_type = trim(htmlentities($_POST['interest_type']));
        $sharia_deposit = trim(htmlentities($_POST['sharia_deposit']));

        $withdrawal_date = trim(htmlentities($_POST['withdrawal_date']));
        $adjusted_interest_date = trim(htmlentities($_POST['adjusted_interest_date']));
        $withdrawal_principle = trim(htmlentities($_POST['withdrawal_principle']));
        $withdrawal_interest = trim(htmlentities($_POST['withdrawal_interest']));
        $total_withdrawal_amount = trim(htmlentities($_POST['total_withdrawal_amount']));

        $rollover_type = trim(htmlentities($_POST['rollover_type']));
        $new_principle_amount = trim(htmlentities($_POST['new_principle_amount']));
        $new_interest_rate = trim(htmlentities($_POST['new_interest_rate']));
        $new_maturity_date = trim(htmlentities($_POST['new_maturity_date']));
        $statutory_type = trim(htmlentities($_POST['statutory_type']));

        $contact_person = trim(htmlentities($_POST['contact_person']));
        $telephone_no = trim(htmlentities($_POST['telephone_no']));
        $fax_no = trim(htmlentities($_POST['fax_no']));
        $reference_no = trim(htmlentities($_POST['reference_no']));
        $parent_reference_no = trim(htmlentities($_POST['parent_reference_no']));

        $description = trim(htmlentities($_POST['description']));
		
		$gotError = false;
		/*if($fundCode==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}*/
        
		if (!$gotError){
			
			$query = "INSERT INTO tbl_kr_time_deposit (
					action_type,
					im_code,
					fund_code,
					placement_bank_code,
                    branch_code,
                    placement_bank_security_acc_no,
                    placement_bank_cash_acc_no,
                    ccy,
                    principle,
                    interest_rate,
                    placement_date,
                    maturity_date,
                    interest_frequency,
                    interest_type,
                    sharia_deposit,
                    withdrawal_date,
                    adjusted_interest_rate,
                    withdrawal_principle,
                    withdrawal_interest,
                    total_withdrawal_amount,
                    rollover_type,
                    new_principle_amount,
                    new_interest_rate,
                    new_maturity_date,
                    statutory_type,
                    contact_person,
                    telephone_no,
                    fax_no,
                    reference_no,
                    parent_reference_no,
                    description,

					created_date,
					created_by,
					last_updated_date,
					last_updated_by,
					is_deleted
				)VALUES(
                    '$action_type',
                    '$im_code',
                    '$fund_code',
                    '$placement_bank_code',
                    '$branch_code',
                    '$placement_bank_security_acc_no',
                    '$placement_bank_cash_acc_no',
                    '$ccy',
                    '$principle',
                    '$interest_rate',
                    '$placement_date',
                    '$maturity_date',
                    '$interest_frequency',
                    '$interest_type',
                    '$sharia_deposit',
                    '$withdrawal_date',
                    '$adjusted_interest_rate',
                    '$withdrawal_principle',
                    '$withdrawal_interest',
                    '$total_withdrawal_amount',
                    '$rollover_type',
                    '$new_principle_amount',
                    '$new_interest_rate',
                    '$new_maturity_date',
                    '$statutory_type',
                    '$contact_person',
                    '$telephone_no',
                    '$fax_no',
                    '$reference_no',
                    '$parent_reference_no',
                    '$description',
                    now(),
                    '".$_SESSION['pk_id']."',
                    now(),
                    '".$_SESSION['pk_id']."',
                    '0')";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='time_deposit.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = "ADD - TIME DEPOSIT";
$dataRows = array (
	    'TEXT' => array(
            'Action Type','Fund Code','Placement Bank Code', 'Branch Code', 'Placement Bank Security Acc. No'
            , 'placement Bank Cash Acc. No', 'Currency', 'Principle', 'Interest Rate (%)', 'Placement Date'
            , 'Maturity Date', 'Interest Frequency', 'Interest Type', 'Sharia Deposit', 'Withdrawal Date'
            , 'Adjusted Interest Rate (%)'
            , 'Withdrawal Principle'
            , 'Withdrawal Interest'
            , 'Total Withdrawal Amount'
            , 'Rollover Type'
            , 'New Principle Amount'
            , 'New Interest Rate (%)'
            , 'New Maturity Date'
            , 'Statutory Type'
            , 'Contact Person'
            , 'Telephone No'
            , 'Fax No'
            , 'Reference No'
            , 'Parent Reference No'
            , 'Description'),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            $data->cb_actiontype('action_type',$action_type),
            "<input type=text size='50' maxlength=16 name=fund_code value='$fund_code'>",
		    "<input type=text size='50' name=placement_bank_code value='$placement_bank_code'>",
            "<input type=text size='50' name=branch_code value='$branch_code'>",
            "<input type=text size='50' name=branch_code value='$placement_bank_security_acc_no'>",
            "<input type=text size='50' name=branch_code value='$placement_bank_cash_acc_no'>",
            "<input type=text size='50' name=ccy value='$ccy'>",
            "<input type=number name=principle value='$principle'>",
            "<input type=number name=interest_rate value='$interest_rate'>",
            $data->datePicker('placement_date', $placement_date,''),
            $data->datePicker('maturity_date', $maturity_date,''),
            $data->cb_interestfrequency('interest_frequency',$interest_frequency),
            $data->cb_interesttype('interest_type',$interest_type),
            $data->cb_shariadeposit('sharia_deposit',$sharia_deposit),
            $data->datePicker('withdrawal_date', $withdrawal_date,''),
            "<input type=number name=adjusted_interest_rate value='$adjusted_interest_rate'>",
            "<input type=number name=withdrawal_principle value='$withdrawal_principle'>",
            "<input type=number name=withdrawal_interest value='$withdrawal_interest'>",
            "<input type=number name=total_withdrawal_amount value='$total_withdrawal_amount'>",
            $data->cb_rollovertype('rollover_type',$rollover_type),
            "<input type=number name=new_principle_amount value='$new_principle_amount'>",
            "<input type=number name=new_interest_rate value='$new_interest_rate'>",
            $data->datePicker('new_maturity_date', $new_maturity_date,''),
            $data->cb_statutorytype('statutory_type',$statutory_type),
            "<input type=text size='50' name=contact_person value='$contact_person'>",
            "<input type=text size='50' name=telephone_no value='$telephone_no'>",
            "<input type=text size='50' name=fax_no value='$fax_no'>",
            "<input type=text size='50' name=reference_no value='$reference_no'>",
            "<input type=text size='50' name=parent_reference_no value='$parent_reference_no'>",
            "<input type=text size='50' name=description value='$description'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='time_deposit_add.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>