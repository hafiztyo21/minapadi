<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$data2 = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('me_dealer.html');


####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_me.session'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$combo 			= $data->cb_me_type('txt_status',$_POST[txt_status]," ");
$allocation 	= $data->cb_allocation('txt_allocation',$_POST[txt_allocation]," ");
$tombol			= "<input type=submit name=btn_view value=View>";
$tanggal		=$data->datePicker('txt_tanggal', $_POST[txt_tanggal],'');
//$tanggal		=$data->datePicker('txt_tanggal', '2017-08-31','');
$txt_status 	= trim(htmlentities($_POST['txt_status']));
$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
$nama 			= $_SESSION['username'];
$id 			= $_SESSION['pk_id'];
$link 			= "me_dealer_open.php";

$rowo = $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$txt_allocation."'");
$allocation2 = $rowo['A'];



if (($txt_status==1) or (empty($txt_status))){
	$txt_tanggal = trim(htmlentities($_POST['txt_tanggal']));
	$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
	if (empty($txt_tanggal)){
		$txt_tanggal = date("Y-m-d");
	}
	$sql  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '1'
	order by $order_by $sort_order";
	$sql1  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '2'
	order by $order_by $sort_order";
//	PRINT_r ($sql1);
	$sql2  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '3'
	order by $order_by $sort_order";

	$sql3  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '4'
	order by $order_by $sort_order";
	//echo $sql3;

	$sql4  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '5'
	order by $order_by $sort_order";
	
	$sql5  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '6'
	order by $order_by $sort_order";
	$sql6  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '7'
	order by $order_by $sort_order";
	$sql7  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '8'
	order by $order_by $sort_order";
	$sql8  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '9'
	order by $order_by $sort_order";

	$sql9  = "SELECT TBL_KR_ME.*, tbl_kr_price.price_type,  format((lembar_pending/100),2) as LMBR, fullname,
			 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar_hist/100),2) as LMBR_HIST,
			 format(((lembar_hist/100) - (lembar_pending/100)),2) as LMBR_PENDING FROM TBL_KR_ME
	LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
	LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.me_id
	where dealer_id = '".$id."' AND LEFT(create_dt,10) = '".$txt_tanggal."'
	AND allocation =  '10'
	order by $order_by $sort_order";

	$tmpl->setAttribute("shares","visibility","show");
}if ($txt_status==2){
	$txt_tanggal = trim(htmlentities($_POST['txt_tanggal']));
	if (empty($txt_tanggal)){
		$txt_tanggal = date("Y-m-d");
	}

	$sql  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '1'  ";
	$sql1  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '2'  ";
	$sql2  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '3'  ";
	$sql3  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '4'  ";
	$sql4  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '5'  ";
	$sql5  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '6'  ";
	$sql6  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '7'  ";
	$sql7  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '8'  ";
	$sql8  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '9'  ";

  	$sql9  = "select  a.*, b.fullname, format(a.market_price,0) as mrkt_prc, format(a.market_value,0) as mrkt_val
		from tbl_kr_me_bonds_tmp a
		LEFT JOIN tbl_kr_user b ON b.pk_id = a.me_id
  		where a.dealer_id = '".$id."' AND LEFT(a.create_dt,10) = '".$txt_tanggal."' AND a.allocation =  '10'  ";


	$judul="ORDER BOOK - BONDS";
	$link = "me_bonds_add.php";
	$addLink = "<a href='me_bonds_add.php?add=1')>Add Transaction</a>";
	$DG= $data->dataGridDealer($sql,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG1= $data->dataGridDealer($sql1,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG2= $data->dataGridDealer($sql2,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG3= $data->dataGridDealer($sql3,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG4= $data->dataGridDealer($sql4,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG5= $data->dataGridDealer($sql5,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG6= $data->dataGridDealer($sql6,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG7= $data->dataGridDealer($sql7,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG8= $data->dataGridDealer($sql8,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$DG9= $data->dataGridDealer($sql9,'obligasi_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	$tmpl->addRows('loopdata2',$DG);
		$tmpl->addRows('loopdata2a',$DG1);
			$tmpl->addRows('loopdata2b',$DG2);
			$tmpl->addRows('loopdata2c',$DG3);
			$tmpl->addRows('loopdata2d',$DG4);
			$tmpl->addRows('loopdata2e',$DG5);
			$tmpl->addRows('loopdata2f',$DG6);
			$tmpl->addRows('loopdata2g',$DG7);
			$tmpl->addRows('loopdata2h',$DG8);
			$tmpl->addRows('loopdata2i',$DG9);
	$tmpl->setAttribute("bonds","visibility","show");
}
$DG= $data->dataGridDealer($sql,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG1= $data->dataGridDealer($sql1,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG2= $data->dataGridDealer($sql2,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG3= $data->dataGridDealer($sql3,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG4= $data->dataGridDealer($sql4,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG5= $data->dataGridDealer($sql5,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG6= $data->dataGridDealer($sql6,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG7= $data->dataGridDealer($sql7,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG8= $data->dataGridDealer($sql8,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);
$DG9= $data->dataGridDealer($sql9,'me_id',$data->ResultsPerPage,$pg,'open',$link,'menu',$link,'delete',$link);

$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('page','nama',$nama);
$tmpl->addVar('page','date',$today);
$tmpl->addVar('page','combo',$combo);
$tmpl->addVar('page','tombol',$tombol);

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('path',$path);
$tmpl->addRows('loopData',$DG);
$tmpl->addRows('loopDataa1',$DG1);
$tmpl->addRows('loopDataa2',$DG2);
$tmpl->addRows('loopDataa3',$DG3);
$tmpl->addRows('loopDataa4',$DG4);
$tmpl->addRows('loopDataa5',$DG5);
$tmpl->addRows('loopDataa6',$DG6);
$tmpl->addRows('loopDataa7',$DG7);
$tmpl->addRows('loopDataa8',$DG8);
$tmpl->addRows('loopDataa9',$DG9);
$tmpl->addVar('page','combo',$combo);
$tmpl->addVar('page','txt_allocation',$allocation2);
$tmpl->addVar('page','tombol',$tombol);
$tmpl->addVar('page','tanggal',$tanggal);
$tmpl->addVar('page','allocation',$allocation);
$tmpl->addVar('page','tgl',$txt_tanggal);
$tmpl->addVar('shares','tgl',$txt_tanggal);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->displayParsedTemplate('page');
?>