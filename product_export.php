<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = date('Y-m-d');
$filename = $transaction_date."_fund_info.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

$add_date = $_GET['transactionDate'];


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT * FROM tbl_kr_allocation WHERE is_deleted=0 and add_date = '$add_date' ";
$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

//-------------STATIC FIELD-----------
$saCode = 'MU002';
//------------------------------------

$str = "Fund Code|Fund Name|Fund Short Name|IM Code|IM Name|Fund Type|Fund Sub Type|Fund CCY|Sharia Compliance|OJK Effective Statement Letter No.|OJK Effective Statement Letter Date";
$str .= "|Launching Date|Offering Period(Begin)|Offering Period(End)|Maturity Year|Liquidation Date|Fiscal Year|Initial NAV per Unit|Distribution Income Frequency|Distribution Income Frequency(Other)|Valuation Frequency|Valuation Frequency(Other)";
$str .= "|Number of Decimals in Funds NAV|Number of Decimals in Funds Unit|Rounding Method for Unit|ISIN Code|CB Code|Fund Operational A/C Name|Fund Operational A/C No.|Fund Subscription A/C Name|Fund Subscription A/C No.|Minimum NAV(AUM) Value (in IDR)|Minimum Initial Subscription Amount";
$str .= "|Minimum Subsequent Subscription Amount|Minimum Redemption Amount|Maximum Total Redemption per Day|Maximum Redemption Payment Date|Expectedemption Payment Date|Max Equity|Max Fixed Income|Max Money|Min Equity|Min Fixed Income|Min Money";
$str .= "|C-BEST Cash A/C No|Availability for switching|SID|NPWP No.|NPWP Registration Date|CFI Category|CFI Group|CFI 1st Attribute|CFI 2nd Attribute|CFI 3rd Attribute|CFI 4th Attribute";
$str .= "|Correspondence Address Line 1|Correspondence Address Line 2|Correspondence Address Line 3|Correspondence Postal Code|Correspondence City|Correspondence City(Other)|Correspondence State/Province|Correspondence State/Province(Other)|Correspondence Country|Billing Address Line 1|Billing Address Line 2";
$str .= "|Billing Address Line 3|Billing Postal Code|Billing City|Billing City(Other)|Billing State/Province|Billing State/Province(Other)|NPWP Address Line 1|NPWP Address Line 2|NPWP Address Line 3|NPWP Postal Code|NPWP City";
$str .= "|NPWP City(Other)|NPWP State/Province|NPWP State/Province(Other)|NPWP Country\r\n";

fwrite($output, $str);

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $str = "";
    $arr = array();
    $var = $rows[$i];
    
    $idx = 0;
    $arr[$idx] = $var['fund_code'];                                   $idx++;     //0. fund code
    $arr[$idx] = $var['fund_name'];                                   $idx++;     //1. fund name
    $arr[$idx] = $var['fund_shortname'];                              $idx++;     //2. fund short name 
    $arr[$idx] = $var['im_code'];                                    $idx++;     //3. customer first name
    $arr[$idx] = $var['im_name'];                                 $idx++;     //4. customer middle name 
    $arr[$idx] = $var['fund_type'];                                   $idx++;     //5. customer last name

    $arr[$idx] = $var['fund_sub_type'];                               $idx++;     //6. country of nationality
    $arr[$idx] = $var['fund_ccy'];                             $idx++;     //7. id no.

    $arr[$idx] = $var['sharia_compliance'];                                             $idx++;     //8. id expired date
    $arr[$idx] = $var['ojk_effective_statement_letter_no'];                                             $idx++;     //8. id expired date

    $ojkDate = '';
    if($var['ojk_effective_statement_letter_date'] != null && $var['ojk_effective_statement_letter_date'] != '' && $var['ojk_effective_statement_letter_date'] != '0000-00-00')
        $ojkDate = str_replace('-','',$var['ojk_effective_statement_letter_date']);

    $arr[$idx] = $ojkDate;                                               $idx++;     //9. npwp no.

    $launchingDate = '';
    if($var['launching_date'] != null && $var['launching_date'] != '' && $var['launching_date'] != '0000-00-00')
        $launchingDate = str_replace('-','',$var['launching_date']);

    $arr[$idx] = $launchingDate;                                      $idx++;     //10. npwp registration date

    $offeringPeriodBegin = '';
    if($var['offering_period_begin'] != null && $var['offering_period_begin'] != '' && $var['offering_period_begin'] != '0000-00-00')
        $offeringPeriodBegin = str_replace('-','',$var['offering_period_begin']);

    $arr[$idx] = $offeringPeriodBegin;                            $idx++;     //12. country of birth

    $offeringPeriodEnd = '';
    if($var['offering_period_end'] != null && $var['offering_period_end'] != '' && $var['offering_period_end'] != '0000-00-00')
        $offeringPeriodEnd = str_replace('-','',$var['offering_period_end']);

    $arr[$idx] = $offeringPeriodEnd;                                    $idx++;     //13. place of birth

    $arr[$idx] = $var['maturity_year'];                                         $idx++;     //14. date of birth

    $liquidationDate = '';
    if($var['liquidation_date'] != null && $var['liquidation_date'] != '' && $var['liquidation_date'] != '0000-00-00')
        $liquidationDate = str_replace('-','',$var['liquidation_date']);

    $arr[$idx] = $liquidationDate;                                         $idx++;     //14. date of birth

    $arr[$idx] = $var['fiscal_year'];                                             $idx++;     //15. gender
    $arr[$idx] = $var['initial_nav_per_unit'];                                             $idx++;     //15. gender

    $arr[$idx] = $var['distribution_income_frequency'];                                          $idx++;     //16. educational background
    $arr[$idx] = $var['distribution_income_frequency_other'];                             $idx++;     //17. mother maiden name

    $arr[$idx] = $var['valuation_frequency'];                                           $idx++;     //18. religion
    $arr[$idx] = $var['valuation_frequency_other'];                                         $idx++;     //19. occupation
    $arr[$idx] = $var['number_of_deicmals_in_funds_nav'];                                        $idx++;     //20. income level

    $arr[$idx] = $var['number_of_deicmals_in_funds_unit'];                                   $idx++;     //21. marital status
    $arr[$idx] = $var['rounding_method_for_unit'];                                $idx++;     //22. spouses name
    $arr[$idx] = $var['isin_code'];                      $idx++;     //23. investors_risk_profile

    $arr[$idx] = $var['cb_code'];                                             $idx++;     //24. invesment objective

    $arr[$idx] = $var['fund_operational_ac_name'];                                             $idx++;     //25. source of fund

    $arr[$idx] = $var['fund_operational_ac_no'];                                 $idx++;     //26. asset owner
    $arr[$idx] = $var['fund_subscription_ac_name'];                                 $idx++;     //27. ktp address

    $arr[$idx] = $var['fund_subscription_ac_no'];                                       $idx++;     //28. ktp city
    $arr[$idx] = $var['minimum_nav_value'];                                $idx++;     //29. ktp postal code

    $arr[$idx] = $var['minimum_initial_subscription_amount'];                               $idx++;     //30. correspondence address

    $arr[$idx] = $var['minimum_subsequent_subscription_amount'];                         $idx++;     //31. correspondence city code
    $arr[$idx] = $var['minimum_redemption_amount'];                                                  $idx++;     //32. correspondence city name (optional)
    $arr[$idx] = $var['maximum_total_redemption_per_day'];                              $idx++;     //33. correspondence postal code
    $arr[$idx] = $var['maximum_redemption_payment_date'];                   $idx++;     //34. country of correspondence
    $arr[$idx] = $var['expected_redemption_payment_date'];                               $idx++;     //35. domicili address

    $arr[$idx] = $var['max_equity'];                               $idx++;     //36. domicili city code
    $arr[$idx] = $var['max_fixed_income'];                                                  $idx++;     //37. domicili city name (optional)
    $arr[$idx] = $var['max_money'];                              $idx++;     //38. domicili postal code

    $arr[$idx] = $var['min_equity'];                                    $idx++;     //39. country of domicili
    $arr[$idx] = $var['min_fixed_income'];                                    $idx++;     //40. home phone
    $arr[$idx] = $var['min_money'];                                   $idx++;     //41. mobile phone

    $arr[$idx] = $var['cbest_securities_ac_no'];                                     $idx++;     //42. fax
    $arr[$idx] = $var['cbest_cash_ac_no'];                                   $idx++;     //43. email 
    $arr[$idx] = $var['availability_for_switching'];                              $idx++;     //44. statement type
    $arr[$idx] = $var['sid'];                                       $idx++;     //45. fatca
    $arr[$idx] = $var['npwp_no'];                                         $idx++;     //46. tin
    $arr[$idx] = $var['npwp_registration_date'];                         $idx++;     //47. tin issuance country

    
        $arr[$idx] = $var['cfi_category'];                         $idx++;     //48. redm payment bank bic code
        $arr[$idx] = $var['cfi_group'];                   $idx++;     //49. redm payment bank bi member code
        $arr[$idx] = $var['cfi_1_attribute'];                          $idx++;     //50. redm payment bank name 
        $arr[$idx] = $var['cfi_2_attribute'];                          $idx++;     //51. redm payment bank country
        $arr[$idx] = $var['cfi_3_attribute'];                           $idx++;     //52. redm payment bank branch
        $arr[$idx] = $var['cfi_4_attribute'];                              $idx++;     //53. redm payment acc currency
        $arr[$idx] = $var['correspondence_address1'];                     $idx++;     //54. redm payment acc no
        $arr[$idx] = $var['correspondence_address2'];                       $idx++;     //55. redm payment acc name

        $arr[$idx] = $var['correspondence_address3'];                         $idx++;     //56. redm payment bank bic code 2
        $arr[$idx] = $var['correspondence_postal_code'];                   $idx++;     //57. redm payment bank bi member code 2
        $arr[$idx] = $var['correspondence_city'];                         $idx++;     //58. redm payment bank name  2
        $arr[$idx] = $var['correspondence_city_other'];                          $idx++;     //59. redm payment bank country 2
        $arr[$idx] = $var['correspondence_state'];                           $idx++;     //60. redm payment bank branch 2
        $arr[$idx] = $var['correspondence_state_other'];                              $idx++;     //61. redm payment acc currency 2
        $arr[$idx] = $var['correspondence_country'];                    $idx++;     //62. redm payment acc no 2
        $arr[$idx] = $var['billing_address1'];                      $idx++;     //63. redm payment acc name 2

        $arr[$idx] = $var['billing_address2'];                         $idx++;     //64. redm payment bank bic code 3
        $arr[$idx] = $var['billing_address3'];                   $idx++;     //65. redm payment bank bi member code 3
        $arr[$idx] = $var['billing_postal_code'];                             $idx++;     //66. redm payment bank name 3
        $arr[$idx] = $var['billing_city'];                          $idx++;     //67. redm payment bank country 3
        $arr[$idx] = $var['billing_city_other'];                           $idx++;     //68. redm payment bank branch 3
        $arr[$idx] = $var['billing_state'];                              $idx++;     //69. redm payment acc currency 3
        $arr[$idx] = $var['billing_state_other'];                           $idx++;     //70. redm payment acc no 3
        $arr[$idx] = $var['billing_country'];                         $idx++;     //71. redm payment acc name 3
        $arr[$idx] = $var['npwp_address1'];                      $idx++;     //63. redm payment acc name 2

        $arr[$idx] = $var['npwp_address2'];                      $idx++;     //63. redm payment acc name 2
        $arr[$idx] = $var['npwp_address3'];                      $idx++;     //63. redm payment acc name 2
        $arr[$idx] = $var['npwp_postal_code'];                             $idx++;     //66. redm payment bank name 3
        $arr[$idx] = $var['npwp_city'];                          $idx++;     //67. redm payment bank country 3
        $arr[$idx] = $var['npwp_city_other'];                           $idx++;     //68. redm payment bank branch 3
        $arr[$idx] = $var['npwp_state'];                              $idx++;     //69. redm payment acc currency 3
        $arr[$idx] = $var['npwp_state_other'];                           $idx++;     //70. redm payment acc no 3
        $arr[$idx] = $var['npwp_country'];                         $idx++;

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
}

fclose($output);

?>
