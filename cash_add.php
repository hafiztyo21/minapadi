<?php
set_time_limit(0);
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('cash_add.html');
$tablename = 'tbl_kr_cash';


if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
		$txt_tgl = trim(htmlentities($_POST['txt_tgl']));
 		$txt_kas = trim(htmlentities($_POST['txt_kas']));
		$txt_total_piutang = trim(htmlentities($_POST['txt_total_piutang']));
        $txt_aktiva_lain = trim(htmlentities($_POST['txt_aktiva_lain']));
        $txt_total_kewajiban = trim(htmlentities($_POST['txt_total_kewajiban']));
        $txt_jumlah_up = trim(htmlentities($_POST['txt_jumlah_up']));
		$txt_nab = trim(htmlentities($_POST['txt_nab']));
		if($txt_kas==''){
			echo "<script>alert('Kas Giro is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_piutang==''){
			echo "<script>alert('Total Piutang is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
        if($txt_aktiva_lain==''){
			echo "<script>alert('Aktiva lain is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_kewajiban==''){
			echo "<script>alert('Total kewajiban is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_jumlah_up==''){
			echo "<script>alert('Jumlah up is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		/*$row_seleksi = $data->get_value("select kas_giro from tbl_kr_cash where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		print($row_seleksi);
		if($row_seleksi==1)
		{
			echo "<script>alert('Data already exists!');</script>";
			throw new Exception($data->err_report('s02'));
		}
*/		
		$sql_cek=$data->get_row("SELECT pk_id FROM tbl_kr_cash WHERE allocation='$txt_allocation' AND create_dt='$txt_tgl'");
		if ($sql_cek!=null){
			throw new Exception('Data Sudah pernah diinput');
		}

		$sql = "INSERT INTO tbl_kr_cash (
			kas_giro,
			total_piutang,
			aktiva_lain,
			total_kewajiban,
			jumlah_up,
			nab,
			allocation,
			create_dt
		)VALUES(
			'$txt_kas',
			'$txt_total_piutang',
			'$txt_aktiva_lain',
			'$txt_total_kewajiban',
			'$txt_jumlah_up',
			'$txt_nab',
			'$txt_allocation',
			'$txt_tgl'
			)";		
			
		$sql_simulasi = "INSERT INTO tbl_kr_simulasi_cash (
			kas_giro,
			total_piutang,
			aktiva_lain,
			total_kewajiban,
			jumlah_up,
			nab,
			allocation,
			create_dt
		)VALUES(
			'$txt_kas',
			'$txt_total_piutang',
			'$txt_aktiva_lain',
			'$txt_total_kewajiban',
			'$txt_jumlah_up',
			'$txt_nab',
			'$txt_allocation',
			'$txt_tgl'
			)";
			//print_r($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql_simulasi)){
			throw new Exception($data->err_report('s02'));
		}

        
		/*---------KALKULASI BALANCE IFUA--------*/
		
		$rowA = $data->get_row("select sum(price_val) as prc_val from tbl_kr_report_pn 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$a = $rowA['prc_val'];

		$rowB = $data->get_row("select 
					sum(market_value) as market
					from tbl_kr_report_bonds 
					where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$b = $rowB['market'];

		$rowC = $data->get_row("select sum(total) as TTL from tbl_kr_report_saham 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$c = $rowC['TTL'];

		$rowD = $data->get_row("select sum(face_value) as FACE from tbl_kr_report_deposit 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$d = $rowD['FACE'];

		$total_activa = $a + $b + $c +$d + floatval($txt_kas) + floatval($txt_total_piutang) + floatval($txt_aktiva_lain);
		$total_activa_bersih = $total_activa - floatval($txt_total_kewajiban);
		$nab_per_saham = $total_activa_bersih / floatval($txt_jumlah_up);
        $nab_per_saham = round($nab_per_saham, 4);

		$currentDate = $txt_tgl;

		$rowAllocation = $data->get_row("SELECT * FROM tbl_kr_allocation WHERE pk_id = '".$txt_allocation."'");
		$fundcodeAllocation = $rowAllocation['fund_code'];
        
		// insert ifua balance hari ini untuk customer individu
		/*
		$query = "SELECT ifua_code, cus_sid, fund_code, fund_name, unit_balance, amount_balance, last_change_date  FROM tbl_kr_cus_ifua_balance WHERE trade_date = (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') AND fund_code = '$fundcodeAllocation' order by ifua_code";
		*/		
		$rowlastdate = $data->get_row("select max(trade_date) AS lastdate from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate'");
		$lastdate=$rowlastdate['lastdate'];
		
		$rowExist = $data->get_rows("SELECT ifua_code FROM tbl_kr_cus_ifua_balance WHERE fund_code = '$fundcodeAllocation' AND trade_date = '$currentDate'");
		/*
		$query= "
			SELECT A.ifua_code, A.cus_sid, A.fund_code, A.fund_name, A.unit_balance, A.amount_balance, A.last_change_date , B.amt
			FROM (
				SELECT ifua_code, cus_sid, fund_code, fund_name, unit_balance, amount_balance, last_change_date  
				FROM tbl_kr_cus_ifua_balance 
				WHERE trade_date = '$lastdate' 
				AND fund_code = '$fundcodeAllocation' 
			) A left join ( 
				select X.investor_ac_no, sum(X.amt) amt from (
					 SELECT investor_ac_no, if(1=transaction_type, amount ,if(amount>0,-1*amount, if('Y'=amount_all_unit,0,amount_unit*'$nab_per_saham'))) amt FROM tbl_kr_subsredm_order 
 					WHERE fund_code = '$fundcodeAllocation' AND transaction_date = '$lastdate'
 				) X
 				group by X.investor_ac_no
 			) B on A.ifua_code = B.investor_ac_no";
 		*/
 		$query= "
			SELECT A.ifua_code, A.cus_sid, A.fund_code, A.fund_name, A.unit_balance, A.amount_balance, A.last_change_date , B.amount, B.amount_unit, B.amount_all_unit, B.transaction_type
			FROM (
				SELECT ifua_code, cus_sid, fund_code, fund_name, unit_balance, amount_balance, last_change_date  
				FROM tbl_kr_cus_ifua_balance 
				WHERE trade_date = '$lastdate' 
				AND fund_code = '$fundcodeAllocation' 
			) A left join ( 
				select X.investor_ac_no, X.amount, X.amount_unit, X.amount_all_unit, X.transaction_type from (
					 SELECT investor_ac_no, amount, amount_unit, amount_all_unit, transaction_type FROM tbl_kr_subsredm_order 
 					WHERE fund_code = '$fundcodeAllocation' AND transaction_date = '$lastdate'
 				) X
 				group by X.investor_ac_no
 			) B on A.ifua_code = B.investor_ac_no";
 		#echo $query."<br><br>";
		$rows = $data->get_rows($query);
		foreach($rows as $row){
			$ifuacode = $row['ifua_code'];
			$cus_sid = $row['cus_sid'];
            $fund_code = $row['fund_code'];
            $fund_name = $row['fund_name'];
			$unit_balance = 0;
			$amount_balance = 0;
			#$amount_balance = $row['amt'];

			$lastchange = '';

			if($row != null){
				$unit_balance = $row['unit_balance'];
				$amount_balance_old = $row['amount_balance'];
				$amount_balance = floatval($unit_balance) * $nab_per_saham;
				$lastchange = $row['last_change_date'];
			}

            if(intval($row['transaction_type']) == 1){ 
                //subscription
                $amount_balance += floatval($row['amount']);
            }else { 
                //redeem
                if($rowSubs['amount'] > 0){ //redem pakai amount
                    $amount_balance -= floatval($row['amount']);
                }else{  //redem pakai unit
                    if($row['amount_all_unit'] == 'Y'){ //semua unit
                        $amount_balance = 0;
                    }else{
                        $amount_balance -= (floatval($row['amount_unit']) * $nab_per_saham);    
                    }
                    
                }
            }
			/*
			$rowBalance = $data->get_row("SELECT * FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fundcodeAllocation' AND trade_date = (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') ORDER BY trade_date DESC LIMIT 1");
			if($rowBalance != null){
				$unit_balance = $rowBalance['unit_balance'];
				$amount_balance_old = $rowBalance['amount_balance'];
				$amount_balance = floatval($unit_balance) * $nab_per_saham;
				$lastchange = $rowBalance['last_change_date'];
			}
            
            //subscription & redeem
            $query = "SELECT amount, amount_unit, amount_all_unit, transaction_type FROM tbl_kr_subsredm_order WHERE investor_ac_no = '".$ifuacode."' AND fund_code = '".$fund_code."' AND transaction_date = '$currentDate' ORDER BY subsredm_order_id ASC";
            $rowsSubs = $data->get_rows($query);
            foreach($rowsSubs as $rowSubs){
                if(intval($rowSubs['transaction_type']) == 1){ 
                    //subscription
                    $amount_balance += floatval($rowSubs['amount']);
                }else { 
                    //redeem
                    if($rowSubs['amount'] > 0){ //redem pakai amount
                        $amount_balance -= floatval($rowSubs['amount']);
                    }else{  //redem pakai unit
                        if($rowSubs['amount_all_unit'] == 'Y'){ //semua unit
                            $amount_balance = 0;
                        }else{
                            $amount_balance -= (floatval($rowSubs['amount_unit']) * $nab_per_saham);    
                        }
                        
                    }
                }
            }
            */
			$lastchange = str_replace("-","",$txt_tgl);
			if($lastchange == '')
				$lastchange = date('Ymd');
				
            //convert balance to unit
            if($nab_per_saham > 0)
            $unit_balance = $amount_balance / $nab_per_saham;
            else $unit_balance = 0;
            
            /*
			$rowExist = $data->get_row("SELECT balance_id FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fundcodeAllocation'  AND trade_date = '$currentDate' LIMIT 1");
			if($rowExist == null){
			*/	
			$flag='1';
			foreach($rowExist as $rowEx){
				$ifuacodeEx=$rowEx[ifua_code];
				if($ifuacode==$rowEx[ifua_code]){
					$flag='2';
					break;
				}
			}
			if($flag == '1'){
				//insert query
				$query = "INSERT INTO tbl_kr_cus_ifua_balance (ifua_code,cus_sid, unit_balance, amount_balance, trade_date, fund_code, fund_name, last_change_date, nav) VALUES 
				('$ifuacode', '$cus_sid', '$unit_balance', '$amount_balance', '$currentDate', '$fund_code', '$fund_name', '$lastchange', '$nab_per_saham')";
 				#echo $query."<br><br>";				
			}else{
				//update query
				$query = "UPDATE tbl_kr_cus_ifua_balance SET unit_balance = '$unit_balance', amount_balance = '$amount_balance',last_change_date = '$lastchange', nav='$nab_per_saham' WHERE ifua_code = '$ifuacode' AND fund_code = '$fundcodeAllocation' AND trade_date='$currentDate'";
 				#echo $query."<br><br>";
			}
			$data->inpQueryReturnBool($query);

		}

		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');/*window.location='cash_repo.php';*/</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}


if ($_GET['add']==1){
	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Tanggal Cash Di input',
						'Kas pada Giro BII RD Keraton (Rp.)',
						'Total Piutang (Rp.)',
						'Aktiva Lain-lain (Rp.)',
						'Total Kewajiban (Rp.)',
						'Jumlah Up Beredar (Unit)',
						'Nab per Saham/ Unit Penyertaan Hari sebelumnya'),
		'DOT'  => array (':',':',':',':',':',':',':'),
		'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
		$data->datePicker('txt_tgl', $rows[txt_tgl],''),
		"<input type=text name=txt_kas value='".$_POST[txt_kas]."'>",
		"<input type=text name=txt_total_piutang value='".$_POST[txt_total_piutang]."'>",
		"<input type=text name=txt_aktiva_lain value='".$_POST[txt_aktiva_lain]."'>",
		"<input type=text name=txt_total_kewajiban value='".$_POST[txt_total_kewajiban]."'>",
		"<input type=text name=txt_jumlah_up value='".$_POST[txt_jumlah_up]."'>",
		"<input type=text name=txt_nab value='".$_POST[txt_nab]."'>"
		
	)
		);
	$tittle = "CASH ADD";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='cash_repo.php';\">"
	);
}


if ($_POST['btn_save_edit']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_tgl = trim(htmlentities($_POST['txt_tgl']));
		$txt_kas = trim(htmlentities($_POST['txt_kas']));
		$txt_total_piutang = trim(htmlentities($_POST['txt_total_piutang']));
        $txt_aktiva_lain = trim(htmlentities($_POST['txt_aktiva_lain']));
        $txt_total_kewajiban = trim(htmlentities($_POST['txt_total_kewajiban']));
        $txt_jumlah_up = trim(htmlentities($_POST['txt_jumlah_up']));
		$txt_nab = trim(htmlentities($_POST['txt_nab']));
		
		if($txt_kas==''){
			echo "<script>alert('Kas Giro is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_piutang==''){
			echo "<script>alert('Total Piutang is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
        if($txt_aktiva_lain==''){
			echo "<script>alert('Aktiva lain is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_kewajiban==''){
			echo "<script>alert('Total kewajiban is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_jumlah_up==''){
			echo "<script>alert('Jumlah up is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$sql = "UPDATE tbl_kr_cash SET
			kas_giro = '".$txt_kas."',
			total_piutang = '".$txt_total_piutang."',
			aktiva_lain = '".$txt_aktiva_lain."',
			total_kewajiban = '".$txt_total_kewajiban."',
			jumlah_up = '".$txt_jumlah_up."',
			nab = '".$txt_nab."'
			 WHERE pk_id = '".$_GET[id]."'";
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
        
        /*---------KALKULASI BALANCE IFUA--------*/
		$cashRow = $data->get_row("select * from tbl_kr_cash where pk_id = '".$_GET[id]."'");
		$txt_allocation = $cashRow['allocation'];
		$txt_tgl = $cashRow['create_dt'];
		
		$rowA = $data->get_row("select sum(price_val) as prc_val from tbl_kr_report_pn 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$a = $rowA['prc_val'];

		$rowB = $data->get_row("select 
					sum(market_value) as market
					from tbl_kr_report_bonds 
					where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$b = $rowB['market'];

		$rowC = $data->get_row("select sum(total) as TTL from tbl_kr_report_saham 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$c = $rowC['TTL'];

		$rowD = $data->get_row("select sum(face_value) as FACE from tbl_kr_report_deposit 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$d = $rowD['FACE'];

		$total_activa = $a + $b + $c +$d + floatval($txt_kas) + floatval($txt_total_piutang) + floatval($txt_aktiva_lain);
		$total_activa_bersih = $total_activa - floatval($txt_total_kewajiban);
		$nab_per_saham = $total_activa_bersih / floatval($txt_jumlah_up);
        
		$nab_per_saham = round($nab_per_saham, 4);

		$currentDate = $txt_tgl;

		

		$rowAllocation = $data->get_row("SELECT * FROM tbl_kr_allocation WHERE pk_id = '".$txt_allocation."'");
		$fundcodeAllocation = $rowAllocation['fund_code'];
        
		// insert ifua balance hari ini untuk customer individu
		/*
		$query = "SELECT ifua_code, cus_sid, fund_code, fund_name, unit_balance, amount_balance, last_change_date  FROM tbl_kr_cus_ifua_balance WHERE trade_date = (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') AND fund_code = '$fundcodeAllocation' order by ifua_code";
		*/
		$rowlastdate = $data->get_row("select max(trade_date) AS lastdate from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate'");
		$lastdate=$rowlastdate['lastdate'];

		$rowExist = $data->get_rows("SELECT ifua_code FROM tbl_kr_cus_ifua_balance WHERE fund_code = '$fundcodeAllocation' AND trade_date = '$currentDate'");

		/*
		$query= "
			SELECT A.ifua_code, A.cus_sid, A.fund_code, A.fund_name, A.unit_balance, A.amount_balance, A.last_change_date , B.amt
			FROM (
				SELECT ifua_code, cus_sid, fund_code, fund_name, unit_balance, amount_balance, last_change_date  
				FROM tbl_kr_cus_ifua_balance 
				WHERE trade_date = '$lastdate' 
				AND fund_code = '$fundcodeAllocation' 
			) A left join ( 
				select X.investor_ac_no, sum(X.amt) amt from (
					 SELECT investor_ac_no, if(1=transaction_type, amount ,if(amount>0,-1*amount, if('Y'=amount_all_unit,0,amount_unit*'$nab_per_saham'))) amt FROM tbl_kr_subsredm_order 
 					WHERE fund_code = '$fundcodeAllocation' AND transaction_date = '$lastdate'
 				) X
 				group by X.investor_ac_no
 			) B on A.ifua_code = B.investor_ac_no";
 		*/
 		$query= "
			SELECT A.ifua_code, A.cus_sid, A.fund_code, A.fund_name, A.unit_balance, A.amount_balance, A.last_change_date , B.amount, B.amount_unit, B.amount_all_unit, B.transaction_type
			FROM (
				SELECT ifua_code, cus_sid, fund_code, fund_name, unit_balance, amount_balance, last_change_date  
				FROM tbl_kr_cus_ifua_balance 
				WHERE trade_date = '$lastdate' 
				AND fund_code = '$fundcodeAllocation' 
			) A left join ( 
				select X.investor_ac_no, X.amount, X.amount_unit, X.amount_all_unit, X.transaction_type from (
					 SELECT investor_ac_no, amount, amount_unit, amount_all_unit, transaction_type FROM tbl_kr_subsredm_order 
 					WHERE fund_code = '$fundcodeAllocation' AND transaction_date = '$lastdate'
 				) X
 				group by X.investor_ac_no
 			) B on A.ifua_code = B.investor_ac_no";
 		#echo $query."<br><br>";
		$rows = $data->get_rows($query);
		foreach($rows as $row){
			$ifuacode = $row['ifua_code'];
			$cus_sid = $row['cus_sid'];
            $fund_code = $row['fund_code'];
            $fund_name = $row['fund_name'];
			$unit_balance = 0;
			$amount_balance = 0;
			#$amount_balance = $row['amt'];

			$lastchange = '';

			if($row != null){
				$unit_balance = $row['unit_balance'];
				$amount_balance_old = $row['amount_balance'];
				$amount_balance = floatval($unit_balance) * $nab_per_saham;
				$lastchange = $row['last_change_date'];
			}

            if(intval($row['transaction_type']) == 1){ 
                //subscription
                $amount_balance += floatval($row['amount']);
            }else { 
                //redeem
                if($rowSubs['amount'] > 0){ //redem pakai amount
                    $amount_balance -= floatval($row['amount']);
                }else{  //redem pakai unit
                    if($row['amount_all_unit'] == 'Y'){ //semua unit
                        $amount_balance = 0;
                    }else{
                        $amount_balance -= (floatval($row['amount_unit']) * $nab_per_saham);    
                    }
                    
                }
            }
            /*
			$rowBalance = $data->get_row("SELECT * FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fundcodeAllocation' AND trade_date =  (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') ORDER BY trade_date DESC LIMIT 1");
			if($rowBalance != null){
				$unit_balance = $rowBalance['unit_balance'];
				$amount_balance_old = $rowBalance['amount_balance'];
				$amount_balance = floatval($unit_balance) * $nab_per_saham;
				$lastchange = $rowBalance['last_change_date'];
			}
            
            //subscription & redeem
            $query = "SELECT amount, amount_unit, amount_all_unit, transaction_type FROM tbl_kr_subsredm_order WHERE investor_ac_no = '".$ifuacode."' AND fund_code = '".$fund_code."' AND transaction_date = '$currentDate' ORDER BY subsredm_order_id ASC";
            $rowsSubs = $data->get_rows($query);
            foreach($rowsSubs as $rowSubs){
                if($rowSubs['transaction_type'] == 1){ 
                    //subscription
                    $amount_balance += floatval($rowSubs['amount']);
                }else { 
                    //redeem
                    if($rowSubs['amount'] > 0){ //redem pakai amount
                        $amount_balance -= floatval($rowSubs['amount']);
                    }else{  //redem pakai unit
                        if($rowSubs['amount_all_unit'] == 'Y'){ //semua unit
                            $amount_balance = 0;
                        }else{
                            $amount_balance -= (floatval($rowSubs['amount_unit']) * $nab_per_saham);    
                        }
                        
                    }
                }
            }
            */
			$lastchange = str_replace("-","",$txt_tgl);
			if($lastchange == '')
				$lastchange = date('Ymd');
				
            //convert balance to unit
            if($nab_per_saham > 0)
            $unit_balance = $amount_balance / $nab_per_saham;
            else $unit_balance = 0;
            
            /*
			$rowExist = $data->get_row("SELECT balance_id FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fundcodeAllocation' AND trade_date = '$currentDate' LIMIT 1");
			*/
			$flag='1';
			foreach($rowExist as $rowEx){
				$ifuacodeEx=$rowEx[ifua_code];
				if($ifuacode==$rowEx[ifua_code]){
					$flag='2';
					break;
				}
			}
			if($flag == '1'){

				//insert query
				$query = "INSERT INTO tbl_kr_cus_ifua_balance (ifua_code,cus_sid, unit_balance, amount_balance, trade_date, fund_code, fund_name, last_change_date, nav) VALUES 
				('$ifuacode', '$cus_sid', '$unit_balance', '$amount_balance', '$currentDate', '$fund_code', '$fund_name', '$lastchange', '$nab_per_saham')";
 		#echo $query."<br><br>";
			
			}else{

				//update query
				$query = "UPDATE tbl_kr_cus_ifua_balance SET unit_balance = '$unit_balance', amount_balance = '$amount_balance',last_change_date = '$lastchange', nav='$nab_per_saham' WHERE ifua_code = '$ifuacode' AND fund_code = '$fundcodeAllocation' AND trade_date='$currentDate'";
 		#echo $query."<br><br>";
			}
			$data->inpQueryReturnBool($query);

		}
        
        
		echo "<script>alert('".$data->err_report('s01')."');window.location='cash_repo.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['edit']==1){
	$rows = $data->get_row("select * from tbl_kr_cash where pk_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];
	$tittle = "CASH EDIT ".$allocation2."";
	$dataRows = array (
			'TEXT' =>  array(
						'Allocation',
						'Kas pada Giro BII RD Keraton (Rp.)',
						'Total Piutang (Rp.)',
						'Aktiva Lain-lain (Rp.)',
						'Total Kewajiban (Rp.)',
						'Jumlah Up Beredar (Rp.)',
						'NAB Per Saham / Unit Penyertaan Hari Sebelumnya '),
		'DOT'  => array (':',':',':',':',':',':'),
	'FIELD' => array (
	"&nbsp;&nbsp;".$allocation2."",
	"<input type=text name=txt_kas value='".$rows[kas_giro]."'>",
	"<input type=text name=txt_total_piutang value='".$rows[total_piutang]."'>",
	"<input type=text name=txt_aktiva_lain value='".$rows[aktiva_lain]."'>",
	"<input type=text name=txt_total_kewajiban value='".$rows[total_kewajiban]."'>",
	"<input type=text name=txt_jumlah_up value='".$rows[jumlah_up]."'>",
		"<input type=text name=txt_nab value='".$rows[nab]."'>"
			)
	);
    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='cash_repo.php';\">"
	);
}



$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>