<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('investor_fund_unit_account_open_add.html');

$sa_code = 'SA002';
$sid = '';
$acc_name = '';
$client_code = '';
$invs_fund_unit_acc_no = '';

$errorArr = array('','','','');
$otherError='';

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
 		
        $sid = trim(htmlentities($_POST['sid']));
        $acc_name = trim(htmlentities($_POST['acc_name']));
        $client_code = trim(htmlentities($_POST['client_code']));
        $invs_fund_unit_acc_no = trim(htmlentities($_POST['invs_fund_unit_acc_no']));
        
		$gotError = false;
		/*if($fundCode==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}*/
		
        
		if (!$gotError){
			
			$query = "INSERT INTO tbl_kr_investor_fund_unit_acc_open (
					sa_code,
					sid,
					acc_name,
					client_code,
                    invs_fund_unit_acc_no,
					created_date,
					created_by,
					last_updated_date,
					last_updated_by,
					is_deleted
				)VALUES(
                    '$sa_code',
                    '$sid',
                    '$acc_name',
                    '$client_code',
                    '$invs_fund_unit_acc_no',
                    now(),
                    '".$_SESSION['pk_id']."',
                    now(),
                    '".$_SESSION['pk_id']."',
                    '0')";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='investor_fund_unit_account_open.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = "ADD - INVESTOR FUND UNIT ACCOUNT OPEN";
$dataRows = array (
	    'TEXT' => array(
            'SID','Investor Fund Unit Acc Name','Client Code', 'Investor Fund Unit Acc No'),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            "<input type=text size='50' maxlength=16 name=sid value='$sid'>",
		    "<input type=text size='50' name=acc_name value='$acc_name'>",
            "<input type=text size='50' name=client_code value='$client_code'>",
            "<input type=text size='50' name=invs_fund_unit_acc_no value='$invs_fund_unit_acc_no'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='investor_fund_unit_account_open.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>