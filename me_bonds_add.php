<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('me_bonds_add.html');
$tablename = 'tbl_kr_me_bonds';
#############################################################################
if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$me_id = $_SESSION['pk_id'];
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
 		$txt_code = trim(htmlentities($_POST['txt_code']));
 		$txt_name = trim(htmlentities($_POST['txt_name']));
		$txt_rating = trim(htmlentities($_POST['txt_rating']));
		$txt_face = trim(htmlentities($_POST['txt_face']));
		$txt_rate = trim(htmlentities($_POST['txt_rate']));
		$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_indikator_maks = trim(htmlentities($_POST['txt_indikator_maks']));
		$txt_harga_minus = trim(htmlentities($_POST['txt_harga_minus']));
		$txt_market_price = trim(htmlentities($_POST['txt_market_price']));
		$txt_prev_coupon = trim(htmlentities($_POST['txt_prev_coupon']));
		$txt_next_coupon = trim(htmlentities($_POST['txt_prev_coupon']));
		$txt_seller = trim(htmlentities($_POST['txt_seller']));
		$txt_settle = trim(htmlentities($_POST['txt_settle']));
		$txt_assign = trim(htmlentities($_POST['txt_assign']));

		$txt_trade = trim(htmlentities($_POST['txt_trade']));
		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_name ==''){
			echo "<script>alert('Bonds name is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

        if($txt_face==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_rate ==''){
			echo "<script>alert('Int Rate is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_aktiv ==''){
			echo "<script>alert('% Terhadap Total Aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_indikator_maks ==''){
			echo "<script>alert('Indikator maks is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga_minus ==''){
			echo "<script>alert('Harga Per H-1 is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

		if($txt_prev_coupon ==''){
			echo "<script>alert('Prev coupon is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

			if($txt_next_coupon ==''){
			echo "<script>alert('Next coupon is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

		if($txt_seller ==''){
			echo "<script>alert('Seller  is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

		/*if($txt_market_price==''){
			echo "<script>alert('Market price is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}*/
		$a = $txt_market_price/100;
		$txt_market_value = $txt_face * $a ;
		//print_r($txt_market_value);


		$sql1 = "INSERT INTO tbl_kr_me_bonds_tmp (
			code,
			name_bonds,
			rating,
			maturity,
			last_coupon,
			next_coupon,
			seller,
			settlement_date,
			trade_date,
			face_value,
			int_rate,
			total_aktiv,
			indikator_maks,
			harga_minus_one,
			market_price,
			market_value,
			dealer_id,
			me_id,
			allocation,
			create_dt
		)VALUES(
			'$txt_code',
			'$txt_name',
			'$txt_rating',
			'$txt_maturity',
			'$txt_prev_coupon',
			'$txt_next_coupon',
			'$txt_seller',
			'$txt_settle',
			'$txt_trade',
			'$txt_face',
			'$txt_rate',
			'$txt_total_aktiv',
			'$txt_indikator_maks',
			'$txt_harga_minus',
			'$txt_market_price',
			'$txt_market_value',
			'$txt_assign',
			'$me_id',
			'$txt_allocation',
			now()
			)";
		//	print_r($sql1);
		if (!$data->inpQueryReturnBool($sql1)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='me_repo.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_POST['btn_hitung']=='calculate'){
	$face = $_POST[txt_face];
	$price = $_POST[txt_market_price]/100;
	$last =		$_POST['txt_last'];
	$settle =		$_POST['txt_settle'];
	$next   =		$_POST['txt_next'];

	$hasil = $face*$price;

	$accrud_days =$data->Selisih_contract($last,$settle);
	$holding_days =$data->Selisih_contract($settle,$next);
	//print($hasil);
	//print($accrud_days);
	//print($holding_days);
}

if ($_GET['add']==1){
$tittle = "ADD TRANSACTION";
$dataRows = array (
	'TEXT' =>  array('Allocation',
					'Code',
					'Name of Bonds',
					'Rating',
					'Seller',
					'Trade Date',
					'Maturity Date',
					'Settlement Date',
					'Last Coupon Date',
					'Next Coupon Date',


					'Quantity (Rp.)',
					'Int Rate',
					' % terhadap total aktiv',
					'Indikator Maks',
					'Harga Per (H-1)',
					'Market Price (%)',
					'Assign To'
			),
  	'DOT'  => array (':',':',':',':',':',':',':',':',':'),
	'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
		"<input type=text size='20' name=txt_code value='".$_POST[txt_code]."'>",
		"<input type=text size='75'  name=txt_name value='".$_POST[txt_name]."'>",
		"<input type=text size='10'  name=txt_rating value='".$_POST[txt_rating]."'>",
		"<input type=text size='30' name=txt_seller value='".$_POST[txt_seller]."'>",
		$data->datePicker('txt_trade',''),
		$data->datePicker('txt_settle',''),
		$data->datePicker('txt_maturity',''),
		$data->datePicker('txt_prev_coupon',''),
		$data->datePicker('txt_next_coupon',''),

		"<input type=text size='30' name=txt_face value='".$_POST[txt_face]."'>",
		"<input type=text size='10'  name=txt_rate value='".$_POST[txt_rate]."'>",
		"<input type=text size='10'  name=txt_total_aktiv value='".$_POST[txt_total_aktiv]."'>",
		"<input type=text size='10'  name=txt_indikator_maks value='".$_POST[txt_indikator_maks]."'>",
		"<input type=text size='10'  name=txt_harga_minus value='".$_POST[txt_harga_minus]."'>",
		"<input type=text size='10' name=txt_market_price value='".$_POST[txt_market_price]."'>",
		$data->cb_user('txt_assign',$_POST[txt_assign])
		)
	);
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='me_repo.php';\">");
}
##################################################################################################
if ($_GET['detail']==1){

$rows = $data->get_row("select a.*, b.fullname from tbl_kr_me_bonds_tmp a
						LEFT JOIN tbl_kr_user b On a.dealer_id = b.pk_id
						where a.obligasi_id = '".$_GET[id]."'");
$tittle = "VIEW TRANSACTION";
$dataRows = array (
	'TEXT' =>  array('Code','Name of Bonds','Rating','Maturity Date','Coupon Rate %',
			'Last Coupon Payment Date','Next Coupon Payment Date','Seller','Settlement Date','Trade Date',
			'Int Rate','Total Aktiva','Indikator Maks','H-1','Face Value (Rp)','Market Price','Market Value (Rp.)','Assign To'),
  	'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
	'FIELD' => array (
		"&nbsp;".$rows[code]."",
		"&nbsp;".$rows[name_bonds]."",
        "&nbsp;".$rows[rating]."",
        "&nbsp;".$rows[maturity]."",
        "&nbsp;".$rows[coupon_rate]."",
		"&nbsp;".$rows[last_coupon]."",
		"&nbsp;".$rows[next_coupon]."",
		"&nbsp;".$rows[seller]."",
		"&nbsp;".$rows[settlement_date ]."",
		"&nbsp;".$rows[trade_date]."",
		"&nbsp;".$rows[int_rate]."",
		"&nbsp;".$rows[total_aktiv]."",
		"&nbsp;".$rows[indikator_maks]."",
		"&nbsp;".$rows[harga_minus_one]."",
		"&nbsp;".$rows[face_value]."",
		"&nbsp;".$rows[market_price]."%",
		"&nbsp;".$rows[market_value]."",
		"&nbsp;".$rows[fullname].""

		)
	);
    $button = array ('SUBMIT' => "",
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"window.location='me_repo.php';\">");
}

if ($_GET['approve']==1){

$rows = $data->get_row("select a.*, b.fullname from tbl_kr_me_bonds_tmp a
						LEFT JOIN tbl_kr_user b On a.dealer_id = b.pk_id
						where a.obligasi_id = '".$_GET[id]."'");
$tittle = "VIEW TRANSACTION";
$dataRows = array (
	'TEXT' =>  array('Code','Name of Bonds','Rating','Maturity Date','Coupon Rate %',
			'Last Coupon Payment Date','Next Coupon Payment Date','Seller','Settlement Date','Trade Date',
			'Int Rate','Total Aktiva','Indikator Maks','H-1','Face Value (Rp)','Market Price','Market Value (Rp.)','Assign To'),
  	'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
	'FIELD' => array (
		"&nbsp;".$rows[code]."",
		"&nbsp;".$rows[name_bonds]."",
        "&nbsp;".$rows[rating]."",
        "&nbsp;".$rows[maturity]."",
        "&nbsp;".$rows[coupon_rate]."",
		"&nbsp;".$rows[last_coupon]."",
		"&nbsp;".$rows[next_coupon]."",
		"&nbsp;".$rows[seller]."",
		"&nbsp;".$rows[settlement_date ]."",
		"&nbsp;".$rows[trade_date]."",
		"&nbsp;".$rows[int_rate]."",
		"&nbsp;".$rows[total_aktiv]."",
		"&nbsp;".$rows[indikator_maks]."",
		"&nbsp;".$rows[harga_minus_one]."",
		"&nbsp;".$rows[face_value]."",
		"&nbsp;".$rows[market_price]."%",
		"&nbsp;".$rows[market_value]."",
		"&nbsp;".$rows[fullname].""

		)
	);
	if($rows[approve] != 0){		$app = "";
	}else{
		$app = "<input type=submit name=approve value=approve>";
	}
    $button = array ('SUBMIT' => "$app",
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"window.location='settlement.php';\">");
}

if($_POST[approve]){	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$rows = $data->get_row("select a.*, b.fullname from tbl_kr_me_bonds_tmp a
						LEFT JOIN tbl_kr_user b On a.dealer_id = b.pk_id
						where a.obligasi_id = '".$_GET[id]."'");

         $sql1 = "INSERT INTO tbl_kr_me_bonds (
			code,
			name_bonds,
			rating,
			maturity,
			last_coupon,
			next_coupon,
			seller,
			settlement_date,
			trade_date,
			face_value,
			int_rate,
			total_aktiv,
			indikator_maks,
			harga_minus_one,
			market_price,
			market_value,
			dealer_id,
			me_id,
			approve,
			allocation,
			create_dt
		)values('".$rows[code]."','".$rows[name_bonds]."','".$rows[rating]."','".$rows[maturity]."','".$rows[last_coupon]."','".$rows[next_coupon]."','".$rows[seller]."',
		'".$rows[settlement_date]."','".$rows[trade_date]."','".$rows[face_value]."','".$rows[int_rate]."','".$rows[total_aktiv]."','".$rows[indikator_maks]."','".$rows[harga_minus_one]."',
		'".$rows[market_price]."','".$rows[market_value]."','".$rows[dealer_id]."','".$rows[me_id]."','".$_SESSION[pk_id]."','".$rows[allocation]."','".$rows[create_dt]."')";

		$sql2 = "update tbl_kr_me_bonds_tmp set approve = '".$_SESSION[pk_id]."' where obligasi_id ='".$_GET[id]."'";
		if (!$data->inpQueryReturnBool($sql1)){
			throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('Approved');window.location='settlement.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}
############################################################################


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);


$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>