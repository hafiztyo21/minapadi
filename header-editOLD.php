<?php
ob_start();
session_start();
#print_r($_SESSION);
include_once 'global.inc.php';
//require_once 'include/class/global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'setting.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
$data = new setting;
#$data->auth_boolean('10',$_SESSION['pk_id']);

?>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head>

<META HTTP-EQUIV="Expires" CONTENT="Fri, Jan 01 1900 00:00:00 GMT">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META http-equiv="Content-Type" content="text/html; charset=iszo-8859-1">
<link rel="stylesheet" type="text/css" href="include/css/normal.css" />

<title>Minapadi Aset Manajemen</title>
<script language="javascript">
var oTimer,odate
function drawtime(){
	arr_month=Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	odate=new Date();
	if(odate.getHours()<10){
		c_hour="0"+odate.getHours()
	}else{
		c_hour=odate.getHours();
	}
	if(odate.getMinutes()<10){
		c_minute="0"+odate.getMinutes()
	}else{
		c_minute=odate.getMinutes();
	}
	if(odate.getSeconds()<10){
		c_sec="0"+odate.getSeconds();
	}else{
		c_sec=odate.getSeconds();
	}
	//span_time.innerHTML=arr_month[odate.getMonth()]+" "+odate.getDate()+", "+odate.getYear()+" "+c_hour+":"+c_minute+":"+c_sec;
	//span_time.innerHTML=c_hour+":"+c_minute+":"+c_sec;
}
function fLoad(){
	drawtime()//span_time.innerHTML='xx';
	oTimer=setInterval(drawtime,1000);
}


</script>
<!--ZNX Add-->
<script type="text/javascript" src="menu_dock/js/jquery.js"></script>
<script type="text/javascript" src="menu_dock/js/interface.js"></script>
<link href="menu_dock/style.css" rel="stylesheet" type="text/css" />
<!--End ZNX Add-->
</head>
<body onLoad="fLoad();" topmargin="0px" rightmargin="0px" leftmargin="0px" bottommargin="0px">

<table width="100%" height="100"  border="0" cellpadding="0" cellspacing="0" >
  <tr>
    <td align="right" background="image/backhead.JPG">
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td rowspan="2" valign="bottom" >
				<img src="image/logohead.jpg">
			</td>
			<td width="70%" height="20" align="right" valign="top" colspan="2"></td>
		  </tr>
		  <tr>
			<td height="80" width="100%" align="left" valign="top" >
			
				<div class="dock" id="dock">
					<div class="dock-container">
						<a class="dock-item" href="welcome.php" target="mainFrame"><span>Home</span><img src="menu_dock/images/home.png" alt="home" /></a>
						<?php if($data->auth_boolean(30,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="distributed_income_all.php" target="mainFrame"><span>Distributed Income</span><img src="menu_dock/images/profile.png" alt="Distributed Income" /></a>
						<?php }  ?>
						<?php  if($data->auth_boolean(40,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="subsredm_all.php" target="mainFrame"><span>Subs/Redm</span><img src="menu_dock/images/profile.png" alt="Subs/Redm" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(50,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="liquidation.php" target="mainFrame"><span>Liquidation</span><img src="menu_dock/images/profile.png" alt="Liquidation" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(10,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="me_repo.php" target="mainFrame"><span>MI</span><img src="menu_dock/images/profile.png" alt="MI" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(11,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="me_dealer.php" target="mainFrame"><span>Dealer</span><img src="menu_dock/images/profile.png" alt="Dealer" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(12,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="settlement.php" target="mainFrame"><span>Settlement</span><img src="menu_dock/images/profile.png" alt="Settlement" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(20,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="settlement_periode.php" target="mainFrame"><span>History</span><img src="menu_dock/images/profile.png" alt="Settlement" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(80,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="si_all.php" target="mainFrame"><span>Settlement Instruction</span><img src="menu_dock/images/profile.png" alt="Settlement Instruction" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(13,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="saham_all.php" target="mainFrame"><span>Shares</span><img src="menu_dock/images/shares.png" alt="Shares" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(60,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="at_equity.php" target="mainFrame"><span>Equity</span><img src="menu_dock/images/bonds.png" alt="Equity" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(14,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="obligasi.php" target="mainFrame"><span>Bonds</span><img src="menu_dock/images/bonds.png" alt="Bonds" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(14,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="deposit.php" target="mainFrame"><span>Deposito</span><img src="menu_dock/images/bonds.png" alt="Deposit" /></a>
						<!--<a class="dock-item" href="time_deposit.php" target="mainFrame"><span>Deposito</span><img src="menu_dock/images/bonds.png" alt="Deposit" /></a>-->
						<?php } ?>
						<?php if($data->auth_boolean(15,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="pn_repo.php" target="mainFrame"><span>PN</span><img src="menu_dock/images/PN.png" alt="PN" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(22,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="broker.php" target="mainFrame"><span>Broker</span><img src="menu_dock/images/PN.png" alt="PN" /></a>
						<?php }  ?>
						<?php if($data->auth_boolean(16,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="deposit.php" target="mainFrame"><span>TD</span><img src="menu_dock/images/deposit.png" alt="TD" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(17,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="print_all.php" target="mainFrame"><span>Report</span><img src="menu_dock/images/report.png" alt="Report" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(26,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="action_all.php" target="mainFrame"><span>Action</span><img src="menu_dock/images/report.png" alt="Report" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(21,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="simulasi_all.php" target="mainFrame"><span>Simulasi Report</span><img src="menu_dock/images/report.png" alt="Report" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(18,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="stock.php" target="mainFrame"><span>Stock</span><img src="menu_dock/images/data.png" alt="Stock" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(19,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="admin_all.php" target="mainFrame"><span>Admin</span><img src="menu_dock/images/admin.png" alt="Admin" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(23,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="contact_all.php" target="mainFrame"><span>Contact/KYC</span><img src="menu_dock/images/contact.png" alt="Admin" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(70,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="master_all.php" target="mainFrame"><span>Master</span><img src="menu_dock/images/shares.png" alt="Admin" /></a>
						<?php } ?>
						<?php if($data->auth_boolean(25,$_SESSION['pk_id'])){ ?>
						<a class="dock-item" href="setting_all.php" target="mainFrame"><span>Setting</span><img src="menu_dock/images/setting.png" alt="Warning" /></a>
						<?php } ?>
						<a class="dock-item" href="logout.php"><span>Logout</span><img src="menu_dock/images/logout.png" alt="logout" /></a>
					</div>
				</div>
			
			  <script type="text/javascript">

					$(document).ready(
						function()
						{
							$('#dock').Fisheye(
								{
									maxWidth: 70,
									items: 'a',
									itemsText: 'span',
									container: '.dock-container',
									itemWidth: 65,
									proximity: 100,
									halign : 'center'
								}
							)
						}
					);

				</script>


			</td>
		</tr>
		</table>
	</td>

  </tr>
</table>

<table width="100%" height="8px" cellpadding="0" cellspacing="0" >
	<tr>
		<td align="center" bgcolor="#EBEEF5">
			<span id="span_time" style="display:none; "></span>
				<font color="#9999cc"><b> Welcome,&nbsp;<?php echo $_SESSION['full_name']; ?> - &nbsp;<?php  print_r($_SESSION['username']." [&nbsp;".$_SESSION['level_name']."&nbsp;]");?> &nbsp;&nbsp; </b></font>
		</td>
	</tr>
</table>

</body>
</html>
