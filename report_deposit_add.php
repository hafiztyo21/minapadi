<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('deposit_add.html');
$tablename = 'tbl_kr_report_deposit';

if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$deposit_id = $_SESSION['pk_id'];
			$txt_tanggal = trim(htmlentities($_POST['txt_tanggal']));
		$txt_allocation = trim(htmlentities($_POST['txt_allocation']));
 		$txt_bank = trim(htmlentities($_POST['txt_bank']));
		$txt_placement = trim(htmlentities($_POST['txt_placement']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
		$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_indikator = trim(htmlentities($_POST['txt_indikator']));
		$txt_face = trim(htmlentities($_POST['txt_face']));
		
	if($txt_bank==''){
			echo "<script>alert('Bank name is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_placement==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_maturity==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

	if($txt_total_aktiv==''){
			echo "<script>alert('Total Aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_indikator==''){
			echo "<script>alert('Indikator Maks is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_face==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	


		$sql = "INSERT INTO tbl_kr_report_deposit (
			name_bank,
			placement,
			maturity,
			total_aktiv,
			indikator_maks,
			face_value,
			allocation,
			create_dt
		)VALUES(
			'$txt_bank',
			'$_POST[txt_placement]',
			'$txt_maturity',
			'$txt_total_aktiv',
			'$txt_indikator',
			'$txt_face',
			'$txt_allocation',
			'".$txt_tanggal."'
			)";
	
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}

		/*---------KALKULASI BALANCE IFUA--------*/
		$cashRow = $data->get_row("select * from tbl_kr_cash where allocation = '".$txt_allocation."' AND create_dt='".$txt_tanggal."'");
		$txt_allocation = $cashRow['allocation'];
		$txt_tgl = $cashRow['create_dt'];
		$txt_kas = $cashRow['kas_giro'];
		$txt_total_piutang = $cashRow['total_piutang'];
		$txt_aktiva_lain = $cashRow['aktiva_lain'];
		$txt_total_kewajiban = $cashRow['total_kewajiban'];
		$txt_jumlah_up = $cashRow['jumlah_up'];
		
		$rowA = $data->get_row("select sum(price_val) as prc_val from tbl_kr_report_pn 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$a = $rowA['prc_val'];

		$rowB = $data->get_row("select 
					sum(market_value) as market
					from tbl_kr_report_bonds 
					where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$b = $rowB['market'];

		$rowC = $data->get_row("select sum(total) as TTL from tbl_kr_report_saham 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$c = $rowC['TTL'];

		$rowD = $data->get_row("select sum(face_value) as FACE from tbl_kr_report_deposit 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$d = $rowD['FACE'];

		$total_activa = $a + $b + $c +$d + floatval($txt_kas) + floatval($txt_total_piutang) + floatval($txt_aktiva_lain);
		$total_activa_bersih = $total_activa - floatval($txt_total_kewajiban);
		$nab_per_saham = $total_activa_bersih / floatval($txt_jumlah_up);
        
		$nab_per_saham = round($nab_per_saham, 4);

		$currentDate = $txt_tgl;

		$rowAllocation = $data->get_row("SELECT * FROM tbl_kr_allocation WHERE pk_id = '".$txt_allocation."'");
		$fundcodeAllocation = $rowAllocation['fund_code'];
        
		// insert ifua balance hari ini untuk customer individu
		$query = "SELECT ifua_code, cus_sid, fund_code, fund_name  FROM tbl_kr_cus_ifua_balance WHERE trade_date = (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') AND fund_code = '$fundcodeAllocation' order by ifua_code";
		$rows = $data->get_rows($query);
		foreach($rows as $row){
			$ifuacode = $row['ifua_code'];
			$cus_sid = $row['cus_sid'];
            $fund_code = $row['fund_code'];
            $fund_name = $row['fund_name'];
			$unit_balance = 0;
			$amount_balance = 0;

			$lastchange = '';

			$rowBalance = $data->get_row("SELECT * FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fund_code' AND trade_date =  (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') ORDER BY trade_date DESC LIMIT 1");
			if($rowBalance != null){
				$unit_balance = $rowBalance['unit_balance'];
				$amount_balance_old = $rowBalance['amount_balance'];
				$amount_balance = floatval($unit_balance) * $nab_per_saham;
				$lastchange = $rowBalance['last_change_date'];
			}
            
            //subscription & redeem
            $query = "SELECT amount, amount_unit, amount_all_unit, transaction_type FROM tbl_kr_subsredm_order WHERE investor_ac_no = '".$ifuacode."' AND fund_code = '".$fund_code."' AND transaction_date = '$currentDate' ORDER BY subsredm_order_id ASC";
            $rowsSubs = $data->get_rows($query);
            foreach($rowsSubs as $rowSubs){
                if(intval($rowSubs['transaction_type']) == 1){ 
                    //subscription
                    $amount_balance += floatval($rowSubs['amount']);
                }else { 
                    //redeem
                    if($rowSubs['amount'] > 0){ //redem pakai amount
                        $amount_balance -= floatval($rowSubs['amount']);
                    }else{  //redem pakai unit
                        if($rowSubs['amount_all_unit'] == 'Y'){ //semua unit
                            $amount_balance = 0;
                        }else{
                            $amount_balance -= (floatval($rowSubs['amount_unit']) * $nab_per_saham);    
                        }
                        
                    }
                }
				$lastchange = str_replace("-","",$txt_tgl);
            }
			if($lastchange == '')
				$lastchange = date('Ymd');
				
            //convert balance to unit
            if($nab_per_saham > 0)
            $unit_balance = $amount_balance / $nab_per_saham;
            else $unit_balance = 0;
            

			$rowExist = $data->get_row("SELECT balance_id FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fund_code' AND trade_date = '$currentDate' LIMIT 1");
			if($rowExist == null){

				//insert query
				$query = "INSERT INTO tbl_kr_cus_ifua_balance (ifua_code,cus_sid, unit_balance, amount_balance, trade_date, fund_code, fund_name, last_change_date, nav) VALUES 
				('$ifuacode', '$cus_sid', '$unit_balance', '$amount_balance', '$currentDate', '$fund_code', '$fund_name', '$lastchange', '$nab_per_saham')";
			
			}else{

				//update query
				$query = "UPDATE tbl_kr_cus_ifua_balance SET unit_balance = '$unit_balance', amount_balance = '$amount_balance',last_change_date = '$lastchange', nav='$nab_per_saham' WHERE ifua_code = '$ifuacode' AND fund_code = '$fund_code' AND trade_date='$currentDate'";
			}
			$data->inpQueryReturnBool($query);

		}

		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='report_deposit_edit.php?cek=1';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}


if ($_GET['add']==1){
	$dataRows = array (
		'TEXT' =>  array(
						'Tanggal',
						'Allocation',
						'Name of Bank',
						'Placement Date',
						'Maturity Date',
						'Rate',
						'Indikator Maks',
						'Quantity'
						),
		'DOT'  => array (':',':',':',':',':',':',':'),
		'FIELD' => array (
			$data->datePicker('txt_tanggal', $_POST[txt_tanggal],''),
			$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
			"<input type=text name=txt_bank value='".$_POST[txt_bank]."' >",
			$data->datePicker('txt_placement', $_POST[txt_placement],''),
			$data->datePicker('txt_maturity', $_POST[txt_maturity],''),
			"<input type=text name=txt_total_aktiv value='".$_POST[txt_total_aktiv]."' >",
			"<input type=text name=txt_indikator value='".$_POST[txt_indikator]."' >",
			"<input type=text name=txt_face value='".$_POST[txt_face]."' >"
			)
		);
	$tittle = "ADD TRANSACTION";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='report_deposit_edit.php?cek=1';\">"
	);
}

if ($_GET['edit']==1){
	$rows=$data->get_row("select * from tbl_kr_report_deposit where pk_id = '".$_GET[id]."'");
	$dataRows = array (
		'TEXT' =>  array(
						'Name of Bank',
						'Placement Date',
						'Maturity Date',
						'Total Aktiv',
						'Indikator Maks',
						'Quantity'),
		'DOT'  => array (':',':',':',':',':',':'),
		'FIELD' => array (
			"<input type=text size=75 name=txt_bank value='".$rows[name_bank]."' >",
			$data->datePicker('txt_placement', $rows[placement],''),
			$data->datePicker('txt_maturity', $rows[maturity],''),
				"<input type=text name=txt_total_aktiv value='".$rows[total_aktiv]."' >",
				"<input type=text name=txt_int value='".$rows[indikator_maks]."' >",
			"<input type=text size=20 name=txt_quantity value='".$rows[face_value]."' >"
			)
		);
	$tittle = "EDIT TRANSACTION";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='report_deposit_edit.php?cek=1';\">"
	);
}

if ($_POST['btn_save_edit']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
 		$txt_bank = trim(htmlentities($_POST['txt_bank']));
		$txt_placement = trim(htmlentities($_POST['txt_placement']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
	$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
	$txt_int = trim(htmlentities($_POST['txt_int']));
		$txt_quantity = trim(htmlentities($_POST['txt_quantity']));
		if($txt_bank==''){
			echo "<script>alert('Name Bank is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_placement=''){
			echo "<script>alert('Placement Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_maturity==''){
			echo "<script>alert('Maturity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_total_aktiv==''){
			echo "<script>alert('Total aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_int==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_quantity==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

		$sql = "update tbl_kr_report_deposit set
			name_bank = '".$txt_bank."',
			face_value = '".$txt_quantity."',
			placement = '".$_POST[txt_placement]."',
			maturity = '".$txt_maturity."',
			total_aktiv= '".$txt_total_aktiv."',
			indikator_maks ='".$txt_int."'
			where pk_id = '".$_GET[id]."'";
			#print_r($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}

		$bondsRow = $data->get_row("select * from tbl_kr_report_deposit where pk_id = '".$_GET[id]."'");
		$txt_allocation = $bondsRow['allocation'];
		$txt_tanggal = $bondsRow['create_dt'];

		/*---------KALKULASI BALANCE IFUA--------*/
		$cashRow = $data->get_row("select * from tbl_kr_cash where allocation = '".$txt_allocation."' AND create_dt='".$txt_tanggal."'");
		$txt_allocation = $cashRow['allocation'];
		$txt_tgl = $cashRow['create_dt'];
		$txt_kas = $cashRow['kas_giro'];
		$txt_total_piutang = $cashRow['total_piutang'];
		$txt_aktiva_lain = $cashRow['aktiva_lain'];
		$txt_total_kewajiban = $cashRow['total_kewajiban'];
		$txt_jumlah_up = $cashRow['jumlah_up'];
		
		$rowA = $data->get_row("select sum(price_val) as prc_val from tbl_kr_report_pn 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$a = $rowA['prc_val'];

		$rowB = $data->get_row("select 
					sum(market_value) as market
					from tbl_kr_report_bonds 
					where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$b = $rowB['market'];

		$rowC = $data->get_row("select sum(total) as TTL from tbl_kr_report_saham 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$c = $rowC['TTL'];

		$rowD = $data->get_row("select sum(face_value) as FACE from tbl_kr_report_deposit 
							where create_dt='".$txt_tgl."' and allocation='".$txt_allocation."'");
		$d = $rowD['FACE'];

		$total_activa = $a + $b + $c +$d + floatval($txt_kas) + floatval($txt_total_piutang) + floatval($txt_aktiva_lain);
		$total_activa_bersih = $total_activa - floatval($txt_total_kewajiban);
		$nab_per_saham = $total_activa_bersih / floatval($txt_jumlah_up);
        
		$nab_per_saham = round($nab_per_saham, 4);

		$currentDate = $txt_tgl;

		$rowAllocation = $data->get_row("SELECT * FROM tbl_kr_allocation WHERE pk_id = '".$txt_allocation."'");
		$fundcodeAllocation = $rowAllocation['fund_code'];
        
		// insert ifua balance hari ini untuk customer individu
		$query = "SELECT ifua_code, cus_sid, fund_code, fund_name  FROM tbl_kr_cus_ifua_balance WHERE trade_date = (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') AND fund_code = '$fundcodeAllocation' order by ifua_code";
		$rows = $data->get_rows($query);
		foreach($rows as $row){
			$ifuacode = $row['ifua_code'];
			$cus_sid = $row['cus_sid'];
            $fund_code = $row['fund_code'];
            $fund_name = $row['fund_name'];
			$unit_balance = 0;
			$amount_balance = 0;

			$lastchange = '';

			$rowBalance = $data->get_row("SELECT * FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fund_code' AND trade_date =  (select max(trade_date) from tbl_kr_cus_ifua_balance WHERE trade_date < '$currentDate') ORDER BY trade_date DESC LIMIT 1");
			if($rowBalance != null){
				$unit_balance = $rowBalance['unit_balance'];
				$amount_balance_old = $rowBalance['amount_balance'];
				$amount_balance = floatval($unit_balance) * $nab_per_saham;
				$lastchange = $rowBalance['last_change_date'];
			}
            
            //subscription & redeem
            $query = "SELECT amount, amount_unit, amount_all_unit, transaction_type FROM tbl_kr_subsredm_order WHERE investor_ac_no = '".$ifuacode."' AND fund_code = '".$fund_code."' AND transaction_date = '$currentDate' ORDER BY subsredm_order_id ASC";
            $rowsSubs = $data->get_rows($query);
            foreach($rowsSubs as $rowSubs){
                if($rowSubs['transaction_type'] == 1){ 
                    //subscription
                    $amount_balance += floatval($rowSubs['amount']);
                }else { 
                    //redeem
                    if($rowSubs['amount'] > 0){ //redem pakai amount
                        $amount_balance -= floatval($rowSubs['amount']);
                    }else{  //redem pakai unit
                        if($rowSubs['amount_all_unit'] == 'Y'){ //semua unit
                            $amount_balance = 0;
                        }else{
                            $amount_balance -= (floatval($rowSubs['amount_unit']) * $nab_per_saham);    
                        }
                        
                    }
                }
				$lastchange = str_replace("-","",$txt_tgl);
            }
			if($lastchange == '')
				$lastchange = date('Ymd');
				
            //convert balance to unit
            if($nab_per_saham > 0)
            $unit_balance = $amount_balance / $nab_per_saham;
            else $unit_balance = 0;
            

			$rowExist = $data->get_row("SELECT balance_id FROM tbl_kr_cus_ifua_balance WHERE ifua_code = '$ifuacode' AND fund_code = '$fund_code' AND trade_date = '$currentDate' LIMIT 1");
			if($rowExist == null){

				//insert query
				$query = "INSERT INTO tbl_kr_cus_ifua_balance (ifua_code,cus_sid, unit_balance, amount_balance, trade_date, fund_code, fund_name, last_change_date, nav) VALUES 
				('$ifuacode', '$cus_sid', '$unit_balance', '$amount_balance', '$currentDate', '$fund_code', '$fund_name', '$lastchange', '$nab_per_saham')";
			
			}else{

				//update query
				$query = "UPDATE tbl_kr_cus_ifua_balance SET unit_balance = '$unit_balance', amount_balance = '$amount_balance',last_change_date = '$lastchange', nav='$nab_per_saham' WHERE ifua_code = '$ifuacode' AND fund_code = '$fund_code' AND trade_date='$currentDate'";
			}
			$data->inpQueryReturnBool($query);

		}

		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='report_deposit_edit.php?cek=1';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['detail']=='1'){
	$rows = $data->get_row("select * from tbl_kr_report_deposit where pk_id = '".$_GET[id]."'");
	$dataRows = array (
		'TEXT' =>  array(
						'Name of Bank',
						'Placement Date',
						'Maturity Date',
						'Total Aktiv',
						'Indikator Maks',
						'Quantity'
						),
		'DOT'  => array (':',':',':',':',':'),
		'FIELD' => array (
			'&nbsp;'.$rows[name_bank],
			'&nbsp;'.$rows[placement],
			'&nbsp;'.$rows[maturity],
			'&nbsp;'.$rows[total_aktiv],
			'&nbsp;'.$rows[indikator_maks],
			'&nbsp;'.$rows[face_value]
			)
		);
	$tittle = "TIME DEPOSIT DETAIL";
    $button = array ('SUBMIT' => "",
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"window.location='report_deposit_edit.php?cek=1';\">"
	);
}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>