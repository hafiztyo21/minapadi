<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'asset_transaction.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new assettransaction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('at_equity_edit.html');

$id = 0;
$tradeId = '';
$tradeDate = date('Y-m-d');
$settlementDate = date('Y-m-d');
$brCode = '';
$fundCode = '';

$securityCode = '';
$tradeType = '';
$price = 0;
$qty = 0;
$tradeAmount = 0;

$commission = 0;
$salesTax = 0;
$levy = 0;
$vat = 0;
$otherCharges = 0;

$grossSettlementAmount = 0;
$whtOnCommission = 0;
$netSettlementAmount = 0;
$settlementType = '1';
$remarks = '';
$errorArr = array('','','','','','','','','','','','','','','','','','','');
$otherError='';

if($_GET['edit']==1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_equity WHERE equity_id = $id";
    $result = $data->get_row($query);
    $tradeId = $result['trade_id'];
    $tradeDate = $result['trade_date'];
    $settlementDate = $result['settlement_date'];
    $brCode = $result['br_code'];
    $fundCode = $result['fund_code'];

    $securityCode = $result['security_code'];
    $tradeType = $result['trade_type'];
    $price = $result['price'];
    $qty = $result['qty'];
    $tradeAmount = $result['trade_amount'];

    $commission = $result['commission'];
    $salesTax = $result['sales_tax'];
    $levy = $result['levy'];
    $vat = $result['vat'];
    $otherCharges = $result['other_charges'];

    $grossSettlementAmount = $result['gross_settlement_amount'];
    $whtOnCommission = $result['wht_on_commission'];
    $netSettlementAmount = $result['net_settlement_amount'];
    $settlementType = $result['settlement_type'];
    $remarks = $result['remarks'];
    
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
 		$tradeId = trim(htmlentities($_POST['inputTradeId']));
		$tradeDate = trim(htmlentities($_POST['inputTradeDate']));
		$settlementDate = trim(htmlentities($_POST['inputSettlementDate']));
		$brCode = trim(htmlentities($_POST['inputBrCode']));
		$fundCode = trim(htmlentities($_POST['inputFundCode']));

		$securityCode = trim(htmlentities($_POST['inputSecurityCode']));
		$tradeType = trim(htmlentities($_POST['inputTradeType']));
		$price = trim(htmlentities($_POST['inputPrice']));
		$qty = trim(htmlentities($_POST['inputQty']));
		//$tradeAmount = trim(htmlentities($_POST['inputTradeAmount']));
		$tradeAmount = floatval($price) * floatval($qty);

		$commission = trim(htmlentities($_POST['inputCommission']));
		$salesTax = trim(htmlentities($_POST['inputSalesTax']));
		$levy = trim(htmlentities($_POST['inputLevy']));
		$vat = trim(htmlentities($_POST['inputVat']));
		$otherCharges = trim(htmlentities($_POST['inputOtherCharges']));

		//$grossSettlementAmount = trim(htmlentities($_POST['inputGrossSettlementAmount']));
		$whtOnCommission = trim(htmlentities($_POST['inputWhtOnCommission']));
		//$netSettlementAmount = trim(htmlentities($_POST['inputNetSettlementAmount']));
		if($tradeType == 1){
			$grossSettlementAmount = $tradeAmount + floatval($commission) + floatval($levy) + floatval($salesTax) + floatval($vat) + floatval($otherCharges);
			$netSettlementAmount = $grossSettlementAmount - floatval($whtOnCommission);
		}else{
			$grossSettlementAmount = $tradeAmount - floatval($commission) - floatval($levy) - floatval($salesTax) - floatval($vat) - floatval($otherCharges);
			$netSettlementAmount = $grossSettlementAmount + floatval($whtOnCommission);
		}
		$settlementType = trim(htmlentities($_POST['inputSettlementType']));
		$remarks = trim(htmlentities($_POST['inputRemarks']));
        		
		$gotError = false;
		if($tradeId==''){
			$errorArr[0] = "Trade Id must be filled";
			$gotError = true;
		}
		if($brCode==''){
			$errorArr[3] = "BR Code must be filled";
			$gotError = true;
		}
		if($fundCode==''){
			$errorArr[4] = "Fund Code must be filled";
			$gotError = true;
		}
		if($securityCode==''){
			$errorArr[5] = "Security Code must be filled";
			$gotError = true;
		}
		if($price==''){
			$errorArr[7] = "Price must be filled";
			$gotError = true;
		}
		if($qty==''){
			$errorArr[8] = "Qty must be filled";
			$gotError = true;
		}
		if($commission==''){
			$errorArr[10] = "Commission must be filled";
			$gotError = true;
		}
		if($salesTax==''){
			$errorArr[11] = "Sales Tax must be filled";
			$gotError = true;
		}
		if($levy==''){
			$errorArr[12] = "Levy must be filled";
			$gotError = true;
		}
		if($vat==''){
			$errorArr[13] = "Vat must be filled";
			$gotError = true;
		}
		if($otherCharges == '') $otherCharges = 0;

		if($whtOnCommission==''){
			$errorArr[16] = "WHT on Commission must be filled";
			$gotError = true;
		}
		
        
		if (!$gotError){
			
			$query = "UPDATE tbl_kr_equity SET
                    trade_id = '$tradeId',
                    trade_date = '$tradeDate',
                    settlement_date = '$settlementDate',
                    br_code = '$brCode',
					fund_code = '$fundCode',
                    security_code = '$securityCode',
                    trade_type = '$tradeType',
                    price = '$price',
                    qty = '$qty',
                    trade_amount = '$tradeAmount',
                    commission = '$commission',
                    sales_tax = '$salesTax',
                    levy = '$levy',
                    vat = '$vat',
                    other_charges = '$otherCharges',
                    gross_settlement_amount = '$grossSettlementAmount',
                    wht_on_commission = '$whtOnCommission',
                    net_settlement_amount = '$netSettlementAmount',
                    settlement_type = '$settlementType',
                    remarks = '$remarks',

					last_updated_date = now(),
					last_updated_by = '".$_SESSION['pk_id']."'
				WHERE equity_id = '$id'";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Edit Success');window.location='at_equity.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}



$tittle = "EDIT - LIQUIDATION";
$dataRows = array (
	    'TEXT' => array(
                'Trade ID'
				,'Trade Date'
				,'Settlement Date'
				,'BR Code'
				,'Fund Code'

				,'Security Code'
				,'Trade Type'
				,'Price'
				,'Qty'
				,'Trade Amount'
				,'Commission'

				,'Sales Tax'
				,'Levy'
				,'VAT'
				,'Other Charges'
				,'Gross Settlement Amount'

				,'WHT on Commission'
				,'Net Settlement Amount'
				,'Settlement Type'
				,'Remarks'),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            "<input type=text size='50' maxlength=16 name=inputTradeId value='$tradeId'> <input type=hidden name=inputId value=$id>",
		    $data->datePicker('inputTradeDate', $tradeDate,''),
            $data->datePicker('inputSettlementDate', $settlementDate,''),
			"<input type=text size='50' maxlength=5 name=inputBrCode value='$brCode'>",
			"<input type=text size='50' maxlength=16 name=inputFundCode value='$fundCode'>",
			"<input type=text size='50' maxlength=16 name=inputSecurityCode value='$securityCode'>",
			$data->cb_tradetype('inputTradeType',$tradeType),
			"<input type=number step=0.01 name=inputPrice id=inputPrice onkeyup=calc_trade_amount() value='$price'>",
			"<input type=number step=1 name=inputQty id=inputQty onkeyup=calc_trade_amount() value='$qty'>",
			"<input type=number step=0.01 name=tradeAmount id=tradeAmount value='$tradeAmount' readonly>",
			"<input type=number step=0.01 name=inputCommission id=inputCommission onkeyup=calc_gross() value='$commission'>",
			"<input type=number step=0.01 name=inputSalesTax id=inputSalesTax onkeyup=calc_gross() value='$salesTax'>",
			"<input type=number step=0.01 name=inputLevy id=inputLevy onkeyup=calc_gross() value='$levy'>",
			"<input type=number step=0.01 name=inputVat id=inputVat onkeyup=calc_gross() value='$vat'>",
			"<input type=number step=0.01 name=inputOtherCharges id=inputOtherCharges onkeyup=calc_gross() value='$otherCharges'>",
			"<input type=number step=0.01 name=inputGrossSettlementAmount id=inputGrossSettlementAmount value='$grossSettlementAmount' readonly>",
			"<input type=number step=0.01 name=inputWhtOnCommission id=inputWhtOnCommission onkeyup=calc_nett() value='$whtOnCommission'>",
			"<input type=number step=0.01 name=inputNetSettlementAmount id=inputNetSettlementAmount value='$netSettlementAmount' readonly>",
			$data->cb_settlementtype('inputSettlementType',$settlementType),
			"<input type=text size=50 name=inputRemarks value='$remarks'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='at_equity.php';\">");
$javascript = "
    <script type='text/javascript'>
        function calc_trade_amount(){
            var price = document.getElementById('inputPrice').value;
            var quantity = document.getElementById('inputQty').value;
            document.getElementById('tradeAmount').value = parseFloat(price) * parseFloat(quantity);
            calc_gross();
        }

        function calc_gross(){
            var trade_amount = document.getElementById('tradeAmount').value;
            var commission = document.getElementById('inputCommission').value;
            var levy = document.getElementById('inputLevy').value;
            var sales_tax = document.getElementById('inputSalesTax').value;
            var vat = document.getElementById('inputVat').value;
            var other_charges = document.getElementById('inputOtherCharges').value;
            var buy_sell = document.getElementById('inputTradeType').value;
            if(buy_sell == 1){
                document.getElementById('inputGrossSettlementAmount').value = parseFloat(trade_amount) + parseFloat(commission) + parseFloat(levy) + parseFloat(sales_tax) + parseFloat(vat) + parseFloat(other_charges);
            }else{
                document.getElementById('inputGrossSettlementAmount').value = parseFloat(trade_amount) - parseFloat(commission) - parseFloat(levy) - parseFloat(sales_tax) - parseFloat(vat) - parseFloat(other_charges);
            }
            calc_nett();
        }

        function calc_nett(){
            var gross = document.getElementById('inputGrossSettlementAmount').value;
            var wht = document.getElementById('inputWhtOnCommission').value;
            var buy_sell = document.getElementById('inputTradeType').value;
            if(buy_sell == 1){
                document.getElementById('inputNetSettlementAmount').value = parseFloat(gross) - parseFloat(wht);
            }else{
                document.getElementById('inputNetSettlementAmount').value = parseFloat(gross) + parseFloat(wht);
            }
        }
    </script>
";
$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path','javascript',$javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');

?>