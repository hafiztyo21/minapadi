<?php
session_start();
#session_destroy();
#print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;

if($_GET['hist']==1){
	$rows 		= $data->get_rows("select * from tbl_kr_deposit_hist where pk_id = '".$_GET[id]."'");
}else{
	$rows 		= $data->get_rows("select * from tbl_kr_deposit where pk_id = '".$_GET[id]."'");
}

$A			=	$rows[0]['allocation'];
$rowo		= $data->get_row("select  tbl_kr_allocation.*,allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
$allocation = $rowo['A'];
$cd_client	= str_replace(".", "-", $rowo['cd_client']);
$tenor 		= $data->get_value("SELECT TIMESTAMPDIFF(MONTH,'".$rows[0]['placement']."','".$rows[0]['maturity']."')");
$row_bank	= $data->get_rows("select * from tbl_kr_placement_bank where bank_code = '".$rows[0]['placement_bank_code']."' AND branch_code = '".$rows[0]['branch_code']."' ");
if($tenor<1){
	$tenor=1;
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Deposit Roll Over Print</title>
</head>

<body onload="window.print();" style="font-size: 14px;">
<!-- <body>  -->

 <table width="100%" border="0" align="left" cellpadding="2">
	<tr>
		<td rowspan="4" width="4%">&nbsp;</td>
	</tr>
  	<tr>
		<td width="90%">
		<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td height="10" colspan="6" align="center" ></td>
  </tr>
  <tr>
		  	<td height="50" colspan="10" align="left" background="templates/image/bg_menu.gif">
			<patTemplate:tmpl name="tittles">
			</patTemplate:tmpl></tr>
  <tr>
		<td height="5px" colspan="6" align="center" ></td>
  </tr>

</table>
<br/>
		
  <table width="50%" border="0" align="left">
  	<tr>
		<td width="20%">To</td>
		<td width="5%">:</td>
		<td width="75%"><?=$rows2[0]['up']?>PT Bank Mandiri (Persero) Tbk</td>
	</tr>
  	<tr>
		<td width="20%">Attn</td>
		<td width="5%">:</td>
		<td width="75%"><?=$rows2[0]['up']?>Ibu Restu Binar Kusuma</td>
	</tr>
  	<tr>
		<td width="20%">Fax</td>
		<td width="5%">:</td>
		<td width="75%"><?=$rows2[0]['up']?>021-526 8201</td>
	</tr>
  	<tr>
		<td width="20%">CC</td>
		<td width="5%">:</td>
		<td width="75%"><?=$rows2[0]['up']?></td>
	</tr>
  </table>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <table width="90%"align="left">
	<tr>
	   <td>
	  	Please execute the following instruction - Perpanjangan Deposito (breakable):
	   </td>
	</tr>
  </table>
 <table width="90%" border="1" cellpadding="3" cellspacing="0"align="left">
  	<tr>
		<td width="25%"><strong>Beneficiary Bank</strong></td>
		<td width="33%"><?=$rows[0]['name_bank']?></td>
		<td align="center" colspan="2"><strong>Clearing Code</strong></td>		
	</tr>
  	<tr>
		<td>In Favor of</td>
		<td colspan="3" align="center"><strong>REKSADANA MINNA PADI <?php echo strtoupper($allocation);?></strong></td>		
	</tr>
  	<tr>
		<td>Amount IDR</td>
		<td>Rp <?=number_format($rows[0]['face_value'])?>,-</td>
		<td width="17%">Interest Rate</td>
		<td><?=number_format($rows[0]['new_interest_rate'],2)?> %</td>
	</tr>
  	<tr>
		<td>Value Date</td>
		<td><?=date('d F Y',strtotime($rows[0]['placement']))?></td>
		<td>Tenor</td>
		<td><?php echo $tenor;?> bulan</td>		
	</tr>
  	<tr>
		<td>Maturity</td>
		<td><?=date('d F Y',strtotime($rows[0]['new_maturity_date']))?></td>
		<td></td>
		<td></td>
	</tr>
  	<tr>
		<td><strong>Kredit To</strong></td>
		<td colspan="3" style="background: #666;"></td>	
	</tr>
  	<tr>
		<td>Bank Name</td>
		<td colspan="3"><?=$rows[0]['name_bank']?></td>		
	</tr>
  	<tr>
		<td>Kode RTGS</td>
		<td colspan="3"><?=$rows[0]['rtgs']?></td>		
	</tr>
  	<tr>
		<td>Acct Name</td>
		<td colspan="3"><?php echo $rows[0]['placement_bank_cash_name'];?></td>				
	</tr>
  	<tr>
		<td>Acct No</td>
		<td colspan="3"><?php echo $rows[0]['placement_bank_cash_no'];?></td>		
	</tr>
  	<tr>
		<td>Contact Person</td>
		<td colspan="3"><?php echo $rows[0]['contact_person'];?></td>				
	</tr>
  	<tr>
		<td>No Telp</td>
		<td colspan="3"><?php echo $rows[0]['telephone_no'];?></td>		
	</tr>
  	<tr>
		<td>No Fax</td>
		<td colspan="3"><?php echo $rows[0]['fax_no'];?></td>		
	</tr>
  </table>
	<table width="90%" align="left">
		<tr>
			<td style="font-size:20px">
				<input type="checkbox" style="transform: scale(3); margin-top: 30px"> &nbsp;
				New Placement
			</td>
			<td style="font-size:20px">
				<input type="checkbox" style="transform: scale(3); margin-top: 30px" checked=""> &nbsp;
				Roll Over
			</td>
			<td style="font-size:20px">
				<input type="checkbox" style="transform: scale(3); margin-top: 30px"> &nbsp;
				Liquidation
			</td>
		</tr>
	</table>

  <table width="90%" align="left" border="0" cellpadding="0" cellspacing="0">
  	<tr>
  		<td colspan="3">
  		<br>
  		<br>
  			Please debit the following account for the placement :
  		<br>
  		<br>
  		</td>
	</tr>
  	<tr>
		<td width="20%" align="left">Bank Name</td>
		<td width="3%" align="left">&nbsp;:&nbsp;</td>
		<td width="77%" align="left"><?=$rowo['bank']?> Cab Plaza Mandiri</td>
	</tr>
  	<tr>
		<td width="20%" align="left">Acct Name</td>
		<td width="3%" align="left">&nbsp;:&nbsp;</td>
		<td width="77%" align="left"><?=$rowo['description1']?></td>
	</tr>
  	<tr>
		<td width="20%" align="left">Acct No</td>
		<td width="3%" align="left">&nbsp;:&nbsp;</td>
		<td width="77%" align="left"><?=$cd_client?></td>
	</tr>
  </table>
  <table width="90%" align="left" border="0" cellpadding="0" cellspacing="1">
  	<tr>
  		<td colspan="3">
  		<br>
  			Upon Maturity, if there is no further information, please transfer the principal + interest to :
  		<br>
  		<br>
  		</td>
	</tr>	
  	<tr>
		<td width="20%" align="left">Bank Name</td>
		<td width="3%" align="left">&nbsp;:&nbsp;</td>
		<td width="77%" align="left"><?=$rowo['bank']?> Cab Plaza Mandiri</td>
	</tr>
  	<tr>
		<td width="20%" align="left">Acct Name</td>
		<td width="3%" align="left">&nbsp;:&nbsp;</td>
		<td width="77%" align="left"><?=$rowo['description1']?></td>
	</tr>
  	<tr>
		<td width="20%" align="left">Acct No</td>
		<td width="3%" align="left">&nbsp;:&nbsp;</td>
		<td width="77%" align="left"><?=$cd_client?></td>
	</tr>
  </table>
  <table width="90%" align="left" border="0" cellpadding="0" cellspacing="0">
  	<tr>
  		<td colspan="3">
  		<br>
  			Note : <strong>This placement is only temporary and adjustment will affect immediately to comply to V.D.10.</strong>
  		<br>
  		<br>
  		</td>
	</tr>	
  	<tr>
		<td width="25%" align="left">Sincerely yours,</td>
		<td width="25%" align="left">Acknowledge by,</td>
		<td width="25%" align="left"><?=$rows[0]['name_bank']?></td>
	</tr>
  	<tr>
		<td width="25%" align="left">Fund Manager</td>
		<td width="25%" align="left">PT Bank Mandiri (Persero),Tbk</td>
		<td width="25%" align="left">Cabang <?=$row_bank[0]['branch_name']?></td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  </table>
  <table width="90%" align="left">
  	<tr>
		<td width="20%" align="left" style="border-bottom:#000 solid 1px;"><strong>Fadli</strong></td>
		<td width="5%" align="left">&nbsp;</td>
		<td width="20%" align="left" style="border-bottom:#000 solid 1px;"></td>
		<td width="5%" align="left">&nbsp;</td>
		<td width="25%" align="left" style="border-bottom:#000 solid 1px;"></td>
	</tr>
  </table>
  <br/>
		
		
		</td>
	</tr>
	</table>	
  </div>

</body>
</html>