<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$transaction_type = $_GET['transaction_type'];
$filename = $transaction_date."_subs_redm_order.txt";

// output headers so that the file is downloaded rather than displayed
//header('Content-Type: text/csv; charset=utf-8');
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
try{


$output = fopen('php://output', 'w');

/*$myfile = fopen("newfile.txt", "w");
$txt = "Mickey Mouse\n";
fwrite($myfile, $txt);
$txt = "Minnie Mouse\n";
fwrite($myfile, $txt);
fclose($myfile);*/

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    DATE_FORMAT(transaction_date, '%Y%m%d') as transaction_date, 
    transaction_type, 
    sa_code, 
    investor_ac_no, 
    fund_code, 
    amount, 
    amount_unit, 
    amount_all_unit, 
    fee, 
    fee_unit, 
    fee_persen, 
    redm_payment_ac_sequential_code,
    redm_payment_bank_bic_code,
    redm_payment_bank_bi_member_code,
    redm_payment_ac_no,
    payment_date, 
    transfer_type, 
    sa_reference_no 
    FROM tbl_kr_subsredm_order 
    WHERE DATE_FORMAT(tbl_kr_subsredm_order.transaction_date, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0";

if($transaction_type != null && $transaction_type != ''){
    $query .= " AND tbl_kr_subsredm_order.transaction_type = '$transaction_type'";
}

$rows = $data->get_rows2($query);


fwrite($output, "\r\n");
// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    if($rows[$i]['transaction_type'] == '1'){
        $rows[$i]['amount_unit'] = '';
        $rows[$i]['amount_all_unit'] = '';

        $rows[$i]['payment_date'] = '';
        $rows[$i]['transfer_type'] = '';
        $rows[$i]['redm_payment_ac_sequential_code'] = '';
        $rows[$i]['redm_payment_bank_bic_code'] = '';
        $rows[$i]['redm_payment_bank_bi_member_code'] = '';
        $rows[$i]['redm_payment_ac_no'] = '';
    }else{
        //if(floatval($rows[$i]['amount']) == 0 && floatval($rows[$i]['amount_unit']) == 0 )
            //$rows[$i]['amount_all_unit'] = 'Y';

        $rows[$i]['payment_date'] = str_replace('-','', $rows[$i]['payment_date']);

        if($rows[$i]['redm_payment_ac_sequential_code'] == '' && $rows[$i]['redm_payment_bank_bic_code'] == ''
        && $rows[$i]['redm_payment_bank_bi_member_code'] == ''  && $rows[$i]['redm_payment_ac_no'] == ''){
            $rows[$i]['redm_payment_ac_sequential_code'] = '1';
        }
    }

    $rows[$i]['redm_payment_ac_sequential_code'] = '';

    if(floatval($rows[$i]['amount']) > 0){
        $rows[$i]['amount_unit'] = '';
        $rows[$i]['amount_all_unit'] = '';
    }else if(floatval($rows[$i]['amount_unit']) > 0){
        $rows[$i]['amount'] = '';
        //$rows[$i]['amount_all_unit'] = '';
    }

    if(floatval($rows[$i]['fee']) > 0){
        $rows[$i]['fee_unit'] = '';
        $rows[$i]['fee_persen'] = '';
    }else if(floatval($rows[$i]['fee_unit']) > 0){
        $rows[$i]['fee'] = '';
        $rows[$i]['fee_persen'] = '';
    }else{
        $rows[$i]['fee'] = '';
        $rows[$i]['fee_unit'] = '';
    }
    
    if($rows[$i]['fee'] == 0) $rows[$i]['fee'] = '';
    if($rows[$i]['fee_unit'] == 0) $rows[$i]['fee_unit'] = '';
    if($rows[$i]['fee_persen'] == 0) $rows[$i]['fee_persen'] = '';

    
    $str = $rows[$i]['transaction_date']."|".$rows[$i]['transaction_type']."|".$rows[$i]['sa_code']
        ."|".$rows[$i]['investor_ac_no']."|".$rows[$i]['fund_code']."|".$rows[$i]['amount']
        ."|".$rows[$i]['amount_unit']."|".$rows[$i]['amount_all_unit']."|".$rows[$i]['fee']
        ."|".$rows[$i]['fee_unit']."|".$rows[$i]['fee_persen']
        ."|".$rows[$i]['redm_payment_ac_sequential_code']."|".$rows[$i]['redm_payment_bank_bic_code']
        ."|".$rows[$i]['redm_payment_bank_bi_member_code']."|".$rows[$i]['redm_payment_ac_no']
        ."|".$rows[$i]['payment_date']."|".$rows[$i]['transfer_type']."|".$rows[$i]['sa_reference_no']."\r\n";

    fwrite($output, $str);
    //fputcsv($output, $rows[$i]);
}

fclose($output);
} catch(Exception $e){
    echo $e->getMessage();
}
?>
