<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('deposit_add.html');
$tablename = 'tbl_kr_deposit';

if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$deposit_id = $_SESSION['pk_id'];
		//$txt_deposit_id = trim(htmlentities($_POST['txt_deposit_id']));
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
 		$txt_bank = trim(htmlentities($_POST['txt_bank']));
		$txt_placement = trim(htmlentities($_POST['txt_placement']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
		//$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_indikator = trim(htmlentities($_POST['txt_indikator']));
		$txt_face = trim(htmlentities($_POST['txt_face']));
		$txt_total_aktiv = trim(htmlentities(str_replace(",","",$_POST['txt_int_rate'])));
		$txt_int_rate = trim(htmlentities(str_replace(",","",$_POST['txt_int_rate'])));
		$txt_deposit_id = trim(htmlentities($_POST['txt_deposit_id']));
		$txt_placement_bank_code = trim(htmlentities($_POST['txt_placement_bank_code']));
		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_action_type = trim(htmlentities($_POST['txt_action_type']));
		$txt_placement_bank_branch_code = trim(htmlentities($_POST['txt_placement_bank_branch_code']));
		$txt_placement_bank_cash_name = trim(htmlentities($_POST['txt_placement_bank_cash_name']));
		$txt_placement_bank_cash_no = trim(htmlentities($_POST['txt_placement_bank_cash_no']));
		$txt_ccy = trim(htmlentities($_POST['txt_ccy']));
		$txt_interest_frequency = trim(htmlentities($_POST['txt_interest_frequency']));
		$txt_interest_type = trim(htmlentities($_POST['txt_interest_type']));
		$txt_sharia_deposit = trim(htmlentities($_POST['txt_sharia_deposit']));
		$txt_withdrawal_date = trim(htmlentities($_POST['txt_withdrawal_date']));
		$txt_adjusted_interest_rate = trim(htmlentities(str_replace(",","",$_POST['txt_adjusted_interest_rate'])));
		$txt_withdrawal_principle = trim(htmlentities(str_replace(",","",$_POST['txt_withdrawal_principle'])));
		$txt_withdrawal_interest = trim(htmlentities(str_replace(",","",$_POST['txt_withdrawal_interest'])));
		$txt_total_withdrawal = trim(htmlentities(str_replace(",","",$_POST['txt_total_withdrawal'])));
		$txt_rollover_type = trim(htmlentities($_POST['txt_rollover_type']));
		$txt_new_principle_amount = trim(htmlentities(str_replace(",","",$_POST['txt_new_principle_amount'])));
		$txt_new_interest_rate = trim(htmlentities(str_replace(",","",$_POST['txt_new_interest_rate'])));
		$txt_new_maturity_date = trim(htmlentities($_POST['txt_new_maturity_date']));
		$txt_amount_to_be_transfered = trim(htmlentities(str_replace(",","",$_POST['txt_amount_to_be_transfered'])));
		$txt_statutory_type = trim(htmlentities($_POST['txt_statutory_type']));
		$txt_contact_person = trim(htmlentities($_POST['txt_contact_person']));
		$txt_telephone_no = trim(htmlentities($_POST['txt_telephone_no']));
		$txt_fax_no = trim(htmlentities($_POST['txt_fax_no']));
		$txt_reference_no = trim(htmlentities($_POST['txt_reference_no']));
		$txt_parent_reference_no = trim(htmlentities($_POST['txt_parent_reference_no']));
		$txt_description = trim(htmlentities($_POST['txt_description']));
		$txt_cancellation_reason = trim(htmlentities($_POST['txt_cancellation_reason']));
		/*
		$txt_rtgs = trim(htmlentities($_POST['txt_rtgs']));
		$txt_sandiBI = trim(htmlentities($_POST['txt_sandiBI']));
		$txt_sandikliring = trim(htmlentities($_POST['txt_sandikliring']));		
		*/
	if($txt_bank==''){
			echo "<script>alert('Bank name is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_placement==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_maturity==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
/*
	if($txt_total_aktiv==''){
			echo "<script>alert('Total Aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		*/
	if($txt_indikator==''){
			echo "<script>alert('Indikator Maks is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	if($txt_face==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

	if($txt_action_type==2||$txt_action_type==3){
		if($txt_deposit_id==''){
			echo "<script>alert('Deposit Transaction Reference Is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	}
		
	if($txt_action_type==1)
	{
		$sql1 = "INSERT INTO tbl_kr_deposit (
				name_bank,
				placement,
				maturity,
				total_aktiv,
				indikator_maks,
				face_value,
				int_rate,
				allocation,
				create_dt,
				transaction_status,
				action_type,
				im_code,
				placement_bank_code,
				branch_code,
				placement_bank_cash_name,
				placement_bank_cash_no,
				ccy,
				interest_frequency,
				interest_type,
				sharia_deposit,
				withdrawal_date,
				adjusted_interest_rate,
				withdrawal_principle,
				withdrawal_interest,
				total_withdrawal_amount,
				rollover_type,
				new_principle_amount,
				new_interest_rate,
				new_maturity_date,
				amount_to_be_transferred,
				statutory_type,
				contact_person,
				telephone_no,
				fax_no,
				reference_no,
				parent_reference_no,
				description,
				cancellation_reason
			)VALUES(
				'$txt_bank',
				'$_POST[txt_placement]',
				'$txt_maturity',
				'$txt_total_aktiv',
				'$txt_indikator',
				'$txt_face',
				'$txt_int_rate',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_action_type',
				'MU002',
				'$txt_placement_bank_code',
				'$txt_placement_bank_branch_code',
				'$txt_placement_bank_cash_name',
				'$txt_placement_bank_cash_no',
				'$txt_ccy',
				'$txt_interest_frequency',
				'$txt_interest_type',
				'$txt_sharia_deposit',
				'$txt_withdrawal_date',
				'$txt_adjusted_interest_rate',
				'$txt_withdrawal_principle',
				'$txt_withdrawal_interest',
				'$txt_total_withdrawal',
				'$txt_rollover_type',
				'$txt_new_principle_amount',
				'$txt_new_interest_rate',
				'$txt_new_maturity_date',
				'$txt_amount_to_be_transfered',
				'$txt_statutory_type',
				'$txt_contact_person',
				'$txt_telephone_no',
				'$txt_fax_no',
				'$txt_reference_no',
				'$txt_parent_reference_no',
				'$txt_description',
				'$txt_cancellation_reason'
			)";
		$exe_insert=mysql_query($sql1);
		
		$roww = $data->get_row("select pk_id  from tbl_kr_deposit where allocation = '".$txt_allocation."' and create_dt=DATE(NOW()) order by pk_id DESC LIMIT 1");
		$sql = "INSERT INTO tbl_kr_deposit_hist (
				name_bank,
				placement,
				maturity,
				total_aktiv,
				indikator_maks,
				face_value,
				int_rate,
				allocation,
				create_dt,
				transaction_status,
				action_type,
				im_code,
				placement_bank_code,
				branch_code,
				placement_bank_cash_name,
				placement_bank_cash_no,
				ccy,
				interest_frequency,
				interest_type,
				sharia_deposit,
				withdrawal_date,
				adjusted_interest_rate,
				withdrawal_principle,
				withdrawal_interest,
				total_withdrawal_amount,
				rollover_type,
				new_principle_amount,
				new_interest_rate,
				new_maturity_date,
				amount_to_be_transferred,
				statutory_type,
				contact_person,
				telephone_no,
				fax_no,
				reference_no,
				parent_reference_no,
				description,
				cancellation_reason,
				deposit_id
			)VALUES(
				'$txt_bank',
				'$_POST[txt_placement]',
				'$txt_maturity',
				'$txt_total_aktiv',
				'$txt_indikator',
				'$txt_face',
				'$txt_int_rate',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_action_type',
				'MU002',
				'$txt_placement_bank_code',
				'$txt_placement_bank_branch_code',
				'$txt_placement_bank_cash_name',
				'$txt_placement_bank_cash_no',
				'$txt_ccy',
				'$txt_interest_frequency',
				'$txt_interest_type',
				'$txt_sharia_deposit',
				'$txt_withdrawal_date',
				'$txt_adjusted_interest_rate',
				'$txt_withdrawal_principle',
				'$txt_withdrawal_interest',
				'$txt_total_withdrawal',
				'$txt_rollover_type',
				'$txt_new_principle_amount',
				'$txt_new_interest_rate',
				'$txt_new_maturity_date',
				'$txt_amount_to_be_transfered',
				'$txt_statutory_type',
				'$txt_contact_person',
				'$txt_telephone_no',
				'$txt_fax_no',
				'$txt_reference_no',
				'$txt_parent_reference_no',
				'$txt_description',
				'$txt_cancellation_reason',
				'$roww[pk_id]'
			)";
			if (!$data->inpQueryReturnBool($sql)){
				throw new Exception($data->err_report('s02'));
			}
			$rowz = $data->get_row("select tbl_kr_deposit_hist.* from tbl_kr_deposit_hist where allocation = '".$txt_allocation."' and create_dt=DATE(NOW()) order by pk_id DESC LIMIT 1");
			$link='deposit_print_newplacement.php?id='.$rowz[pk_id].'';
	}else if($txt_action_type==2){
		$rowx = $data->get_row("select tbl_kr_deposit.* from tbl_kr_deposit where pk_id='".$txt_deposit_id."' order by pk_id ASC");
		$sql_move = "INSERT INTO tbl_kr_deposit_hist (
				name_bank,
				placement,
				maturity,
				total_aktiv,
				indikator_maks,
				face_value,
				int_rate,
				allocation,
				create_dt,
				transaction_status,
				action_type,
				im_code,
				placement_bank_code,
				branch_code,
				placement_bank_cash_name,
				placement_bank_cash_no,
				ccy,
				interest_frequency,
				interest_type,
				sharia_deposit,
				withdrawal_date,
				adjusted_interest_rate,
				withdrawal_principle,
				withdrawal_interest,
				total_withdrawal_amount,
				rollover_type,
				new_principle_amount,
				new_interest_rate,
				new_maturity_date,
				amount_to_be_transferred,
				statutory_type,
				contact_person,
				telephone_no,
				fax_no,
				reference_no,
				parent_reference_no,
				description,
				cancellation_reason,
				deposit_id
			)VALUES(
				'$rowx[name_bank]',
				'$rowx[placement]',
				'$rowx[maturity]',
				'$rowx[total_aktiv]',
				'$rowx[indikator_maks]',
				'$rowx[face_value]',
				'$rowx[int_rate]',
				'$rowx[allocation]',
				'$rowx[create_dt]',
				'$rowx[transaction_status]',
				'$rowx[action_type]',
				'$rowx[im_code]',
				'$rowx[placement_bank_code]',
				'$rowx[branch_code]',
				'$rowx[placement_bank_cash_name]',
				'$rowx[placement_bank_cash_no]',
				'$rowx[ccy]',
				'$rowx[interest_frequency]',
				'$rowx[interest_type]',
				'$rowx[sharia_deposit]',
				'$rowx[withdrawal_date]',
				'$rowx[adjusted_interest_rate]',
				'$rowx[withdrawal_principle]',
				'$rowx[withdrawal_interest]',
				'$rowx[total_withdrawal_amount]',
				'$rowx[rollover_type]',
				'$rowx[new_principle_amount]',
				'$rowx[new_interest_rate]',
				'$rowx[new_maturity_date]',
				'$rowx[amount_to_be_transferred]',
				'$rowx[statutory_type]',
				'$rowx[contact_person]',
				'$rowx[telephone_no]',
				'$rowx[fax_no]',
				'$rowx[reference_no]',
				'$rowx[parent_reference_no]',
				'$rowx[description]',
				'$rowx[cancellation_reason]',
				'$txt_deposit_id'
			)";
		$exe_move=mysql_query($sql_move);
		$sql_del="DELETE FROM tbl_kr_deposit WHERE pk_id='".$rowx[pk_id]."' ";
		$exe_del=mysql_query($sql_del);

		$sql = "INSERT INTO tbl_kr_deposit_hist (
				name_bank,
				placement,
				maturity,
				total_aktiv,
				indikator_maks,
				face_value,
				int_rate,
				allocation,
				create_dt,
				transaction_status,
				action_type,
				im_code,
				placement_bank_code,
				branch_code,
				placement_bank_cash_name,
				placement_bank_cash_no,
				ccy,
				interest_frequency,
				interest_type,
				sharia_deposit,
				withdrawal_date,
				adjusted_interest_rate,
				withdrawal_principle,
				withdrawal_interest,
				total_withdrawal_amount,
				rollover_type,
				new_principle_amount,
				new_interest_rate,
				new_maturity_date,
				amount_to_be_transferred,
				statutory_type,
				contact_person,
				telephone_no,
				fax_no,
				reference_no,
				parent_reference_no,
				description,
				cancellation_reason,
				sandiBI,
				sandikliring
			)VALUES(
				'$txt_bank',
				'$_POST[txt_placement]',
				'$txt_maturity',
				'$txt_total_aktiv',
				'$txt_indikator',
				'$txt_face',
				'$txt_int_rate',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_action_type',
				'MU002',
				'$txt_placement_bank_code',
				'$txt_placement_bank_branch_code',
				'$txt_placement_bank_cash_name',
				'$txt_placement_bank_cash_no',
				'$txt_ccy',
				'$txt_interest_frequency',
				'$txt_interest_type',
				'$txt_sharia_deposit',
				'$txt_withdrawal_date',
				'$txt_adjusted_interest_rate',
				'$txt_withdrawal_principle',
				'$txt_withdrawal_interest',
				'$txt_total_withdrawal',
				'$txt_rollover_type',
				'$txt_new_principle_amount',
				'$txt_new_interest_rate',
				'$txt_new_maturity_date',
				'$txt_amount_to_be_transfered',
				'$txt_statutory_type',
				'$txt_contact_person',
				'$txt_telephone_no',
				'$txt_fax_no',
				'$txt_reference_no',
				'$txt_parent_reference_no',
				'$txt_description',
				'$txt_cancellation_reason',
				'',
				''
			)";
			if (!$data->inpQueryReturnBool($sql)){
				throw new Exception($data->err_report('s02'));
			}
			$rowz = $data->get_row("select tbl_kr_deposit_hist.* from tbl_kr_deposit_hist where allocation = '".$txt_allocation."' and create_dt=DATE(NOW()) order by pk_id DESC LIMIT 1");
			$link='deposit_print_liquidation.php?id='.$rowz[pk_id].'&hist=1';
	}else{
		$rowx = $data->get_row("select tbl_kr_deposit.* from tbl_kr_deposit where pk_id='".$txt_deposit_id."' order by pk_id ASC");
		$sql_move = "INSERT INTO tbl_kr_deposit_hist (
				name_bank,
				placement,
				maturity,
				total_aktiv,
				indikator_maks,
				face_value,
				int_rate,
				allocation,
				create_dt,
				transaction_status,
				action_type,
				im_code,
				placement_bank_code,
				branch_code,
				placement_bank_cash_name,
				placement_bank_cash_no,
				ccy,
				interest_frequency,
				interest_type,
				sharia_deposit,
				withdrawal_date,
				adjusted_interest_rate,
				withdrawal_principle,
				withdrawal_interest,
				total_withdrawal_amount,
				rollover_type,
				new_principle_amount,
				new_interest_rate,
				new_maturity_date,
				amount_to_be_transferred,
				statutory_type,
				contact_person,
				telephone_no,
				fax_no,
				reference_no,
				parent_reference_no,
				description,
				cancellation_reason,
				deposit_id
			)VALUES(
				'$rowx[name_bank]',
				'$rowx[placement]',
				'$rowx[maturity]',
				'$rowx[total_aktiv]',
				'$rowx[indikator_maks]',
				'$rowx[face_value]',
				'$rowx[int_rate]',
				'$rowx[allocation]',
				'$rowx[create_dt]',
				'$rowx[transaction_status]',
				'$rowx[action_type]',
				'$rowx[im_code]',
				'$rowx[placement_bank_code]',
				'$rowx[branch_code]',
				'$rowx[placement_bank_cash_name]',
				'$rowx[placement_bank_cash_no]',
				'$rowx[ccy]',
				'$rowx[interest_frequency]',
				'$rowx[interest_type]',
				'$rowx[sharia_deposit]',
				'$rowx[withdrawal_date]',
				'$rowx[adjusted_interest_rate]',
				'$rowx[withdrawal_principle]',
				'$rowx[withdrawal_interest]',
				'$rowx[total_withdrawal_amount]',
				'$rowx[rollover_type]',
				'$rowx[new_principle_amount]',
				'$rowx[new_interest_rate]',
				'$rowx[new_maturity_date]',
				'$rowx[amount_to_be_transferred]',
				'$rowx[statutory_type]',
				'$rowx[contact_person]',
				'$rowx[telephone_no]',
				'$rowx[fax_no]',
				'$rowx[reference_no]',
				'$rowx[parent_reference_no]',
				'$rowx[description]',
				'$rowx[cancellation_reason]',
				'$txt_deposit_id'
			)";
		$exe_move=mysql_query($sql_move);
		$sql_del="DELETE FROM tbl_kr_deposit WHERE pk_id='".$rowx[pk_id]."' ";
		$exe_del=mysql_query($sql_del);
		
		$sql_up = "INSERT INTO tbl_kr_deposit (
				name_bank,
				placement,
				maturity,
				total_aktiv,
				indikator_maks,
				face_value,
				int_rate,
				allocation,
				create_dt,
				transaction_status,
				action_type,
				im_code,
				placement_bank_code,
				branch_code,
				placement_bank_cash_name,
				placement_bank_cash_no,
				ccy,
				interest_frequency,
				interest_type,
				sharia_deposit,
				withdrawal_date,
				adjusted_interest_rate,
				withdrawal_principle,
				withdrawal_interest,
				total_withdrawal_amount,
				rollover_type,
				new_principle_amount,
				new_interest_rate,
				new_maturity_date,
				amount_to_be_transferred,
				statutory_type,
				contact_person,
				telephone_no,
				fax_no,
				reference_no,
				parent_reference_no,
				description,
				cancellation_reason,
				sandiBI,
				sandikliring
			)VALUES(
				'$txt_bank',
				'$txt_maturity',
				'$txt_new_maturity_date',
				'$txt_total_aktiv',
				'$txt_indikator',
				'$txt_face',
				'$txt_int_rate',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_action_type',
				'MU002',
				'$txt_placement_bank_code',
				'$txt_placement_bank_branch_code',
				'$txt_placement_bank_cash_name',
				'$txt_placement_bank_cash_no',
				'$txt_ccy',
				'$txt_interest_frequency',
				'$txt_interest_type',
				'$txt_sharia_deposit',
				'$txt_withdrawal_date',
				'$txt_adjusted_interest_rate',
				'$txt_withdrawal_principle',
				'$txt_withdrawal_interest',
				'$txt_total_withdrawal',
				'$txt_rollover_type',
				'$txt_new_principle_amount',
				'$txt_new_interest_rate',
				'$txt_new_maturity_date',
				'$txt_amount_to_be_transfered',
				'$txt_statutory_type',
				'$txt_contact_person',
				'$txt_telephone_no',
				'$txt_fax_no',
				'$txt_reference_no',
				'$txt_parent_reference_no',
				'$txt_description',
				'$txt_cancellation_reason',
				'$txt_sandiBI',
				'$txt_sandikliring'
			)";
		$exe_up=mysql_query($sql_up);

		$roww = $data->get_row("select pk_id  from tbl_kr_deposit where allocation = '".$txt_allocation."' and create_dt=DATE(NOW()) order by pk_id DESC LIMIT 1");
		$sql = "INSERT INTO tbl_kr_deposit_hist (
				name_bank,
				placement,
				maturity,
				total_aktiv,
				indikator_maks,
				face_value,
				int_rate,
				allocation,
				create_dt,
				transaction_status,
				action_type,
				im_code,
				placement_bank_code,
				branch_code,
				placement_bank_cash_name,
				placement_bank_cash_no,
				ccy,
				interest_frequency,
				interest_type,
				sharia_deposit,
				withdrawal_date,
				adjusted_interest_rate,
				withdrawal_principle,
				withdrawal_interest,
				total_withdrawal_amount,
				rollover_type,
				new_principle_amount,
				new_interest_rate,
				new_maturity_date,
				amount_to_be_transferred,
				statutory_type,
				contact_person,
				telephone_no,
				fax_no,
				reference_no,
				parent_reference_no,
				description,
				cancellation_reason,
				sandiBI,
				sandikliring,
				deposit_id
			)VALUES(
				'$txt_bank',
				'$rowx[placement]',
				'$rowx[maturity]',
				'$txt_total_aktiv',
				'$txt_indikator',
				'$txt_face',
				'$txt_int_rate',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_action_type',
				'MU002',
				'$txt_placement_bank_code',
				'$txt_placement_bank_branch_code',
				'$txt_placement_bank_cash_name',
				'$txt_placement_bank_cash_no',
				'$txt_ccy',
				'$txt_interest_frequency',
				'$txt_interest_type',
				'$txt_sharia_deposit',
				'$txt_withdrawal_date',
				'$txt_adjusted_interest_rate',
				'$txt_withdrawal_principle',
				'$txt_withdrawal_interest',
				'$txt_total_withdrawal',
				'$txt_rollover_type',
				'$txt_new_principle_amount',
				'$txt_new_interest_rate',
				'$txt_new_maturity_date',
				'$txt_amount_to_be_transfered',
				'$txt_statutory_type',
				'$txt_contact_person',
				'$txt_telephone_no',
				'$txt_fax_no',
				'$txt_reference_no',
				'$txt_parent_reference_no',
				'$txt_description',
				'$txt_cancellation_reason',
				'$txt_sandiBI',
				'$txt_sandikliring',
				'$roww[pk_id]'
			)";
			if (!$data->inpQueryReturnBool($sql)){
				throw new Exception($data->err_report('s02'));
			}
			$rowz = $data->get_row("select tbl_kr_deposit_hist.* from tbl_kr_deposit_hist where allocation = '".$txt_allocation."' and create_dt=DATE(NOW()) order by pk_id DESC LIMIT 1");
			$link='deposit_print_rollover.php?id='.$rowz[pk_id].'&hist=1';
	}
		//print_r($sql);
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='$link';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['add']==1){

	if($_POST['txt_deposit_id']!=''){
		$txt_allocation = trim(htmlentities($_POST['txt_allocation']));
		$txt_deposit_id = trim(htmlentities($_POST['txt_deposit_id']));
		$rows = $data->get_row("SELECT * FROM tbl_kr_deposit WHERE pk_id = '".$txt_deposit_id."' ");

		$txt_bank = $rows['name_bank'];
		$txt_placement = $rows['placement'];
		$txt_maturity = $rows['maturity'];
		$txt_int_rate = $rows['int_rate'];
		$txt_total_aktiv = $rows['total_aktiv'];
		$txt_indikator = $rows['indikator_maks'];
		$txt_face = $rows['face_value'];
		$txt_val = number_format($rows['face_value']);
		$txt_placement_bank_code = $_POST['txt_placement_bank_code'];
		$txt_transaction_status = $rows['transaction_status'];
		$txt_action_type = $_POST['txt_action_type'];
		$txt_placement_bank_branch_code = $rows['branch_code'];
		$txt_placement_bank_cash_name = $rows['placement_bank_cash_name'];
		$txt_placement_bank_cash_no = $rows['placement_bank_cash_no'];
		$txt_ccy = $rows['ccy'];
		$txt_interest_frequency = $rows['interest_frequency'];
		$txt_interest_type = $rows['interest_type'];
		$txt_sharia_deposit = $rows['sharia_deposit'];
		$txt_withdrawal_date = $rows['withdrawal_date'];
		$txt_adjusted_interest_rate = $rows['adjusted_interest_rate'];
		$txt_withdrawal_principle = $rows['withdrawal_principle'];
		$txt_withdrawal_interest = $rows['withdrawal_principle'];
		$txt_total_withdrawal = $rows['total_withdrawal_amount'];
		$txt_rollover_type = $rows['rollover_type'];
		$txt_new_principle_amount = $rows['new_principle_amount'];
		$txt_new_interest_rate = $rows['new_interest_rate'];
		$txt_new_maturity_date = $rows['new_maturity_date'];
		$txt_amount_to_be_transfered = $rows['amount_to_be_transferred'];
		$txt_statutory_type = $rows['statutory_type'];
		$txt_contact_person = $rows['contact_person'];
		$txt_telephone_no = $rows['telephone_no'];
		$txt_fax_no = $rows['fax_no'];
		//$txt_reference_no = $rows['reference_no'];
		$txt_reference_no = trim(htmlentities($_POST['txt_reference_no']));		
		$txt_parent_reference_no = $rows['reference_no'];
		$txt_description = $rows['description'];
		$txt_cancellation_reason = $rows['cancellation_reason'];
	}else{
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
		$txt_deposit_id = trim(htmlentities($_POST['txt_deposit_id']));
		$txt_bank = trim(htmlentities($_POST['txt_bank']));
		$txt_int_rate =	trim(htmlentities(str_replace(",","",$_POST['txt_int_rate'])));
		$txt_placement = trim(htmlentities($_POST['txt_placement']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
		$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_indikator = trim(htmlentities($_POST['txt_indikator']));
		$txt_face = trim(htmlentities($_POST['txt_face']));
		$txt_placement_bank_code = trim(htmlentities($_POST['txt_placement_bank_code']));
		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_action_type = trim(htmlentities($_POST['txt_action_type']));
		$txt_placement_bank_branch_code = trim(htmlentities($_POST['txt_placement_bank_branch_code']));
		$txt_placement_bank_cash_name = trim(htmlentities($_POST['txt_placement_bank_cash_name']));
		$txt_placement_bank_cash_no = trim(htmlentities($_POST['txt_placement_bank_cash_no']));
		$txt_ccy = trim(htmlentities($_POST['txt_ccy']));
		$txt_interest_frequency = trim(htmlentities($_POST['txt_interest_frequency']));
		$txt_interest_type = trim(htmlentities($_POST['txt_interest_type']));
		$txt_sharia_deposit = trim(htmlentities($_POST['txt_sharia_deposit']));
		$txt_withdrawal_date = trim(htmlentities($_POST['txt_withdrawal_date']));
		$txt_adjusted_interest_rate = trim(htmlentities(str_replace(",","",$_POST['txt_adjusted_interest_rate'])));
		$txt_withdrawal_principle = trim(htmlentities(str_replace(",","",$_POST['txt_withdrawal_principle'])));
		$txt_withdrawal_interest = trim(htmlentities(str_replace(",","",$_POST['txt_withdrawal_interest'])));
		$txt_total_withdrawal = trim(htmlentities(str_replace(",","",$_POST['txt_total_withdrawal'])));
		$txt_rollover_type = trim(htmlentities($_POST['txt_rollover_type']));
		$txt_new_principle_amount = trim(htmlentities(str_replace(",","",$_POST['txt_new_principle_amount'])));
		$txt_new_interest_rate = trim(htmlentities(str_replace(",","",$_POST['txt_new_interest_rate'])));
		$txt_new_maturity_date = trim(htmlentities($_POST['txt_new_maturity_date']));
		$txt_amount_to_be_transfered = trim(htmlentities(str_replace(",","",$_POST['txt_amount_to_be_transfered'])));
		$txt_statutory_type = trim(htmlentities($_POST['txt_statutory_type']));
		$txt_contact_person = trim(htmlentities($_POST['txt_contact_person']));
		$txt_telephone_no = trim(htmlentities($_POST['txt_telephone_no']));
		$txt_fax_no = trim(htmlentities($_POST['txt_fax_no']));
		$txt_reference_no = trim(htmlentities($_POST['txt_reference_no']));
		$txt_parent_reference_no = trim(htmlentities($_POST['txt_parent_reference_no']));
		$txt_description = trim(htmlentities($_POST['txt_description']));
		$txt_cancellation_reason = trim(htmlentities($_POST['txt_cancellation_reason']));
		/*
		$txt_rtgs = trim(htmlentities($_POST['txt_rtgs']));
		$txt_sandiBI = trim(htmlentities($_POST['txt_sandiBI']));
		$txt_sandikliring = trim(htmlentities($_POST['txt_sandikliring']));
		*/
	}
	/*
	if($txt_action_type==2){
		$rtgs='';
		$sandiBI='Sandi BI';
		$sandikliring='Sandi Kliring';
		$rtgs1='style="visibility:hidden"; disabled readonly';
		$sandiBI1='';
		$sandikliring1='';
	}else if($txt_action_type==3){
		$rtgs='Kode RTGS';
		$sandiBI='';
		$sandikliring='';
		$rtgs1='';
		$sandiBI1='style="visibility:hidden"; disabled readonly';
		$sandikliring1='style="visibility:hidden"; disabled readonly';
	}else{
		$rtgs='';
		$sandiBI='';
		$sandikliring='';
		$rtgs1='style="visibility:hidden"; disabled readonly';
		$sandiBI1='style="visibility:hidden"; disabled readonly';
		$sandikliring1='style="visibility:hidden"; disabled readonly';
	}
	*/
	if($txt_placement_bank_code!=''){
		$rowx = $data->get_row("SELECT DISTINCT bank_name FROM tbl_kr_placement_bank WHERE bank_code='".$txt_placement_bank_code."' ");
		$txt_bank=$rowx['bank_name'];
	}
	if($txt_placement_bank_branch_code!=''){
		$rowy = $data->get_row("SELECT * FROM tbl_kr_placement_bank WHERE bank_code='".$txt_placement_bank_code."' AND branch_code='".$txt_placement_bank_branch_code."' ");
		$txt_placement_bank_cash_name=$rowy['placement_bank_name'];
		$txt_placement_bank_cash_no=$rowy['placement_bank_no'];
		$txt_contact_person=$rowy['contact_person'];
		$txt_telephone_no=$rowy['telephone'];
		$txt_fax_no=$rowy['fax'];
		//echo "<script>alert('".$txt_placement_bank_branch_code."')</script>";
	}

	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Placement Bank Code',
						'Name of Bank',
						'Placement Date',
						'Maturity Date',
						/*'Rate',*/
						'Indikator Maks',
						'Interest Rate (%)',
						'Quantity',
						'&nbsp;',
						'Transaction Status',
						'Action Type',
						'Branch Code',
						'Placement Bank Cash A/C Name',
						'Placement Bank Cash A/C No',
						/*
						$rtgs,
						$sandiBI,
						$sandikliring,
						*/
						'CCY',
						'Interest Frequency',
						'Interest Type',
						'Sharia Deposit',
						'Withdrawal Date',
						'Adjusted Interest Rate (%)/Profit Sharing',
						'Withdrawal Principle',
						'Withdrawal Interest',
						'Total Withdrawal Amount',
						'Rollover Type',
						'New Principle Amount',
						'New Interest rate(%)/Profit Sharing',
						'New Maturity Date',
						'Amount to be transfered (withdrawn)',
						'Statutory Type',
						'Contact Person',
						'Telephone No',
						'Fax No',
						'Reference No',
						'Parent Reference No',
						'Descripton',
						'Cancellation Reason'
						),
		'DOT'  => array (':',':',':',':',':',':'),
		'FIELD' => array (
			$data->cb_allocation('txt_allocation',$txt_allocation," OnChange= \"document.form1.submit();\" "),
			$data->placement_bank('txt_placement_bank_code', $txt_placement_bank_code, " OnChange= \"document.form1.submit();\" "),	//placement bank code
			"<input type=text name=txt_bank id=txt_bank value='".$txt_bank."' >",
			$data->datePicker('txt_placement', $txt_placement,''),
			$data->datePicker('txt_maturity', $txt_maturity,''),
			"<input type=hidden name=txt_total_aktiv id=txt_total_aktiv value='".$txt_total_aktiv."' >
			<input type=text name=txt_indikator id=txt_indikator value='".$txt_indikator."' >",
			"<input type=text name=txt_int_rate id=txt_int_rate value='".$txt_int_rate."' >",
			"<input type=text name=txt_face id=txt_face value='".$txt_face."' >",
			"",
			"<select name='txt_transaction_status' id='txt_transaction_status' ><option value='NEWM' ".($txt_transaction_status=='NEWM' ? "selected" : "") .">New Trade</option><option value='CANC' ".($txt_transaction_status=='CANC' ? "selected" : "").">Cancel Trade</option></select>",
			"<select name='txt_action_type' id='txt_action_type' onchange='changeType();document.form1.submit();'>
				<option value='1' ".($txt_action_type=='1' ? "selected" : "") .">Deposit Placement</option>
				<option value='2' ".($txt_action_type=='2' ? "selected" : "") .">Deposit Withdrawal</option>
				<option value='3' ".($txt_action_type=='3' ? "selected" : "") .">Deposit Rollover</option>
			</select><br/>
			".$data->cb_deposit_new('txt_deposit_id',$txt_allocation,$txt_deposit_id," OnChange= \"document.form1.submit();\" ","style='visibility:hidden;' ").",&nbsp;&nbsp;
			<input type=text size='30'  name=txt_val id='txt_val' value='".$txt_val."' style='visibility:hidden;' readonly>
			",
			$data->placement_bank_branch('txt_placement_bank_branch_code', $txt_placement_bank_branch_code, "OnChange= \"document.form1.submit();\"", $txt_placement_bank_code),
			"<input type=text name=txt_placement_bank_cash_name id=txt_placement_bank_cash_name value='".$txt_placement_bank_cash_name."' size='50' >",
			"<input type=text name=txt_placement_bank_cash_no id=txt_placement_bank_cash_no value='".$txt_placement_bank_cash_no."' >",
			/*
			"<input type=text name=txt_rtgs id=txt_rtgs value='".$txt_rtgs."' ".$rtgs1.">",
			"<input type=text name=txt_sandiBI id=txt_sandiBI value='".$txt_sandiBI."' ".$sandiBI1.">",
			"<input type=text name=txt_sandikliring id=txt_sandikliring value='".$txt_sandikliring."' ".$sandikliring1.">",
			*/
			$data->cb_accountccy('txt_ccy',$txt_ccy), 
			"<select name='txt_interest_frequency' id='txt_interest_frequency'><option value='1'>On Due Date</option><option value='2'>Daily</option><option value='3'>Weekly</option><option value='4'>Monthly</option><option value='5'>Quarterly</option><option value='6'>Semiannually</option><option value='7'>Yearly</option></select>",
			"<select name='txt_interest_type' id='txt_interest_type' ><option value='1'>Fixed</option><option value='2'>Compound</option><option value='3'>Step/Floating</option></select>",
			"<select name='txt_sharia_deposit' id='txt_sharia_deposit' ><option value='Y'>Yes</option><option value='N'>No</option></select>",
			$data->datePicker('txt_withdrawal_date', $txt_withdrawal_date,'style="visibility:hidden;"'),
			"<input type=text name=txt_adjusted_interest_rate id=txt_adjusted_interest_rate value='".$txt_adjusted_interest_rate."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_withdrawal_principle id=txt_withdrawal_principle value='".$txt_withdrawal_principle."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_withdrawal_interest id=txt_withdrawal_interest value='".$txt_withdrawal_interest."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_total_withdrawal id=txt_total_withdrawal value='".$txt_total_withdrawal."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<select name='txt_rollover_type' id='txt_rollover_type' style='visibility:hidden' ><option value='1'>Principle Only</option><option value='2'>Principle + Interest</option></select>",
			"<input type=text name=txt_new_principle_amount id=txt_new_principle_amount value='".$txt_new_principle_amount."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_new_interest_rate id=txt_new_interest_rate value='".$txt_new_interest_rate."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			$data->datePicker('txt_new_maturity_date', $txt_new_maturity_date,'style="visibility:hidden;"'),
			"<input type=text name=txt_amount_to_be_transfered id=txt_amount_to_be_transfered value='".$txt_amount_to_be_transfered."' onkeyup='reformat(event, this)'>",
			"<select name='txt_statutory_type' id='txt_statutory_type' ><option value='Y'>Yes</option><option value='N'>No</option></select>",
			"<input type=text name=txt_contact_person id=txt_contact_person value='".$txt_contact_person."' >",
			"<input type=text name=txt_telephone_no id=txt_telephone_no value='".$txt_telephone_no."' >",
			"<input type=text name=txt_fax_no id=txt_fax_no value='".$txt_fax_no."' >",
			"<input type=text name=txt_reference_no id=txt_reference_no value='".$txt_reference_no."' >",
			"<input type=text name=txt_parent_reference_no id=txt_parent_reference_no value='".$txt_parent_reference_no."' >",
			"<input type=text size='50' name=txt_description id=txt_description value='".$txt_description."' >",
			"<input type=text size='50' name=txt_cancellation_reason id=txt_cancellation_reason value='".$txt_cancellation_reason."' >",
			)
		);
	$tittle = "ADD TRANSACTION";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='deposit.php';\">"
	);
}

if ($_GET['edit']==1){
	$rows=$data->get_row("select * from tbl_kr_deposit where pk_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];

	$txt_bank = empty($_POST['txt_bank']) ? $rows['name_bank'] : $_POST['txt_bank'];
	$txt_placement_bank_code = empty($_POST['txt_placement_bank_code']) ? $rows['placement_bank_code'] : $_POST['txt_placement_bank_code'];
	$txt_placement_bank_branch_code = empty($_POST['txt_placement_bank_branch_code']) ? $rows['branch_code'] : $_POST['txt_placement_bank_branch_code'];
	$txt_placement = empty($_POST['txt_placement']) ? $rows['placement'] : $_POST['txt_placement'];
	$txt_maturity = empty($_POST['txt_maturity']) ? $rows['maturity'] : $_POST['txt_maturity'];
	$txt_int_rate = empty($_POST['txt_int_rate']) ? $rows['int_rate'] : $_POST['txt_int_rate'];
	$txt_total_aktiv = empty($_POST['txt_total_aktiv']) ? $rows['total_aktiv'] : $_POST['txt_total_aktiv'];
	$txt_indikator = empty($_POST['txt_indikator']) ? $rows['indikator_maks'] : $_POST['txt_indikator'];
	$txt_face = empty($_POST['txt_face']) ? $rows['face_value'] : $_POST['txt_face'];
	$txt_val = empty($_POST['txt_val']) ? $rows['face_value'] : $_POST['txt_val'];
	$txt_transaction_status = empty($_POST['txt_transaction_status']) ? $rows['transaction_status'] : $_POST['txt_transaction_status'];
	$txt_action_type = empty($_POST['txt_action_type']) ? $rows['action_type'] : $_POST['txt_action_type'];
	$txt_placement_bank_cash_name = empty($_POST['txt_placement_bank_cash_name']) ? $rows['placement_bank_cash_name'] : $_POST['txt_placement_bank_cash_name'];
	$txt_placement_bank_cash_no = empty($_POST['txt_placement_bank_cash_no']) ? $rows['placement_bank_cash_no'] : $_POST['txt_placement_bank_cash_no'];
	$txt_ccy = empty($_POST['txt_ccy']) ? $rows['ccy'] : $_POST['txt_ccy'];
	$txt_interest_frequency = empty($_POST['txt_interest_frequency']) ? $rows['interest_frequency'] : $_POST['txt_interest_frequency'];
	$txt_interest_type = empty($_POST['txt_interest_type']) ? $rows['interest_type'] : $_POST['txt_interest_type'];
	$txt_sharia_deposit = empty($_POST['txt_sharia_deposit']) ? $rows['sharia_deposit'] : $_POST['txt_sharia_deposit'];
	$txt_withdrawal_date = empty($_POST['txt_withdrawal_date']) ? $rows['withdrawal_date'] : $_POST['txt_withdrawal_date'];
	$txt_adjusted_interest_rate = empty($_POST['txt_adjusted_interest_rate']) ? $rows['adjusted_interest_rate'] : $_POST['txt_adjusted_interest_rate'];
	$txt_withdrawal_principle = empty($_POST['txt_withdrawal_principle']) ? $rows['withdrawal_principle'] : $_POST['txt_withdrawal_principle'];
	$txt_withdrawal_interest = empty($_POST['txt_withdrawal_interest']) ? $rows['withdrawal_principle'] : $_POST['txt_withdrawal_interest'];
	$txt_total_withdrawal = empty($_POST['txt_total_withdrawal']) ? $rows['total_withdrawal_amount'] : $_POST['txt_total_withdrawal'];
	$txt_rollover_type = empty($_POST['txt_rollover_type']) ? $rows['rollover_type'] : $_POST['txt_rollover_type'];
	$txt_new_principle_amount = empty($_POST['txt_new_principle_amount']) ? $rows['new_principle_amount'] : $_POST['txt_new_principle_amount'];
	$txt_new_interest_rate = empty($_POST['txt_new_interest_rate']) ? $rows['new_interest_rate'] : $_POST['txt_new_interest_rate'];
	$txt_new_maturity_date = empty($_POST['txt_new_maturity_date']) ? $rows['new_maturity_date'] : $_POST['txt_new_maturity_date'];
	$txt_amount_to_be_transfered = empty($_POST['txt_amount_to_be_transfered']) ? $rows['amount_to_be_transferred'] : $_POST['txt_amount_to_be_transfered'];
	$txt_statutory_type = empty($_POST['txt_statutory_type']) ? $rows['statutory_type'] : $_POST['txt_statutory_type'];
	$txt_contact_person = empty($_POST['txt_contact_person']) ? $rows['contact_person'] : $_POST['txt_contact_person'];
	$txt_telephone_no = empty($_POST['txt_telephone_no']) ? $rows['telephone_no'] : $_POST['txt_telephone_no'];
	$txt_fax_no = empty($_POST['txt_fax_no']) ? $rows['fax_no'] : $_POST['txt_fax_no'];
	$txt_reference_no = empty($_POST['txt_reference_no']) ? $rows['reference_no'] : $_POST['txt_reference_no'];
	$txt_parent_reference_no = empty($_POST['txt_parent_reference_no']) ? $rows['parent_reference_no'] : $_POST['txt_parent_reference_no'];
	$txt_description = empty($_POST['txt_description']) ? $rows['description'] : $_POST['txt_description'];
	$txt_cancellation_reason = empty($_POST['txt_cancellation_reason']) ? $rows['cancellation_reason'] : $_POST['txt_cancellation_reason'];

	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Placement Bank Code',
						'Name of Bank',
						'Placement Date',
						'Maturity Date',
						'Rate',
						'Indikator Maks',
						'Interest Rate (%)',
						'Quantity',
						'&nbsp;',
						'Transaction Status',
						'Action Type',
						'Branch Code',
						'Placement Bank Cash A/C Name',
						'Placement Bank Cash A/C No',
						/*
						$rtgs,
						$sandiBI,
						$sandikliring,
						*/
						'CCY',
						'Interest Frequency',
						'Interest Type',
						'Sharia Deposit',
						'Withdrawal Date',
						'Adjusted Interest Rate (%)/Profit Sharing',
						'Withdrawal Principle',
						'Withdrawal Interest',
						'Total Withdrawal Amount',
						'Rollover Type',
						'New Principle Amount',
						'New Interest rate(%)/Profit Sharing',
						'New Maturity Date',
						'Amount to be transfered (withdrawn)',
						'Statutory Type',
						'Contact Person',
						'Telephone No',
						'Fax No',
						'Reference No',
						'Parent Reference No',
						'Descripton',
						'Cancellation Reason'
						),
		'DOT'  => array (':',':',':',':',':',':'),
		'FIELD' => array (
			"&nbsp;&nbsp;".$allocation2."",
			$data->placement_bank('txt_placement_bank_code', $txt_placement_bank_code, " OnChange= \"document.form1.submit();\" "),	//placement bank code
			"<input type=text name=txt_bank id=txt_bank value='".$txt_bank."' readonly>",
			$data->datePicker('txt_placement', $txt_placement,''),
			$data->datePicker('txt_maturity', $txt_maturity,''),
			"<input type=text name=txt_total_aktiv id=txt_total_aktiv value='".$txt_total_aktiv."' >",
			"<input type=text name=txt_indikator id=txt_indikator value='".$txt_indikator."' >",
			"<input type=text name=txt_int_rate id=txt_int_rate value='".$txt_int_rate."' >",
			"<input type=text name=txt_face id=txt_face value='".$txt_face."' >",
			"",
			"<select name='txt_transaction_status' id='txt_transaction_status' ><option value='NEWM' ".($txt_transaction_status=='NEWM' ? "selected" : "") .">New Trade</option><option value='CANC' ".($txt_transaction_status=='CANC' ? "selected" : "").">Cancel Trade</option></select>",
			"<select name='txt_action_type' id='txt_action_type' onchange='changeType();document.form1.submit();'>
				<option value='1' ".($txt_action_type=='1' ? "selected" : "") .">Deposit Placement</option>
				<option value='2' ".($txt_action_type=='2' ? "selected" : "") .">Deposit Withdrawal</option>
				<option value='3' ".($txt_action_type=='3' ? "selected" : "") .">Deposit Rollover</option>
			</select><br/>
			".$data->cb_deposit_new('txt_deposit_id',$txt_allocation,$txt_deposit_id," OnChange= \"document.form1.submit();\" ","style='visibility:hidden;' ").",&nbsp;&nbsp;
			<input type=text size='30'  name=txt_val id='txt_val' value='".$txt_val."' style='visibility:hidden;' readonly>
			",
			$data->placement_bank_branch('txt_placement_bank_branch_code', $txt_placement_bank_branch_code, "", $txt_placement_bank_code),
			"<input type=text name=txt_placement_bank_cash_name id=txt_placement_bank_cash_name value='".$txt_placement_bank_cash_name."' >",
			"<input type=text name=txt_placement_bank_cash_no id=txt_placement_bank_cash_no value='".$txt_placement_bank_cash_no."' >",
			/*
			"<input type=text name=txt_rtgs id=txt_rtgs value='".$txt_rtgs."' ".$rtgs1.">",
			"<input type=text name=txt_sandiBI id=txt_sandiBI value='".$txt_sandiBI."' ".$sandiBI1.">",
			"<input type=text name=txt_sandikliring id=txt_sandikliring value='".$txt_sandikliring."' ".$sandikliring1.">",
			*/
			$data->cb_accountccy('txt_ccy',$txt_ccy), 
			"<select name='txt_interest_frequency' id='txt_interest_frequency'>
				<option value='1' ".($txt_interest_frequency=='1' ? "selected" : "").">On Due Date</option>
				<option value='2' ".($txt_interest_frequency=='2' ? "selected" : "").">Daily</option>
				<option value='3' ".($txt_interest_frequency=='3' ? "selected" : "").">Weekly</option>
				<option value='4' ".($txt_interest_frequency=='4' ? "selected" : "").">Monthly</option>
				<option value='5' ".($txt_interest_frequency=='5' ? "selected" : "").">Quarterly</option>
				<option value='6' ".($txt_interest_frequency=='6' ? "selected" : "").">Semiannually</option>
				<option value='7' ".($txt_interest_frequency=='7' ? "selected" : "").">Yearly</option>
			</select>",
			"<select name='txt_interest_type' id='txt_interest_type' >
				<option value='1' ".($txt_interest_type=='1' ? "selected" : "").">Fixed</option>
				<option value='2' ".($txt_interest_type=='2' ? "selected" : "").">Compound</option>
				<option value='3' ".($txt_interest_type=='3' ? "selected" : "").">Step/Floating</option>
			</select>",
			"<select name='txt_sharia_deposit' id='txt_sharia_deposit' >
				<option value='Y' ".($txt_sharia_deposit=='Y' ? "selected" : "").">Yes</option>
				<option value='N' ".($txt_sharia_deposit=='N' ? "selected" : "").">No</option>
			</select>",
			$data->datePicker('txt_withdrawal_date', $txt_withdrawal_date,'style="visibility:hidden;"'),
			"<input type=text name=txt_adjusted_interest_rate id=txt_adjusted_interest_rate value='".$txt_adjusted_interest_rate."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_withdrawal_principle id=txt_withdrawal_principle value='".$txt_withdrawal_principle."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_withdrawal_interest id=txt_withdrawal_interest value='".$txt_withdrawal_interest."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_total_withdrawal id=txt_total_withdrawal value='".$txt_total_withdrawal."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<select name='txt_rollover_type' id='txt_rollover_type' style='visibility:hidden' ><option value='1'>Principle Only</option><option value='2'>Principle + Interest</option></select>",
			"<input type=text name=txt_new_principle_amount id=txt_new_principle_amount value='".$txt_new_principle_amount."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<input type=text name=txt_new_interest_rate id=txt_new_interest_rate value='".$txt_new_interest_rate."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			$data->datePicker('txt_new_maturity_date', $txt_new_maturity_date,'style="visibility:hidden;"'),
			"<input type=text name=txt_amount_to_be_transfered id=txt_amount_to_be_transfered value='".$txt_amount_to_be_transfered."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			"<select name='txt_statutory_type' id='txt_statutory_type' ><option value='Y'>Yes</option><option value='N'>No</option></select>",
			"<input type=text name=txt_contact_person id=txt_contact_person value='".$txt_contact_person."' >",
			"<input type=text name=txt_telephone_no id=txt_telephone_no value='".$txt_telephone_no."' >",
			"<input type=text name=txt_fax_no id=txt_fax_no value='".$txt_fax_no."' >",
			"<input type=text name=txt_reference_no id=txt_reference_no value='".$txt_reference_no."' >",
			"<input type=text name=txt_parent_reference_no id=txt_parent_reference_no value='".$txt_parent_reference_no."' >",
			"<input type=text size='50' name=txt_description id=txt_description value='".$txt_description."' >",
			"<input type=text size='50' name=txt_cancellation_reason id=txt_cancellation_reason value='".$txt_cancellation_reason."' >",
			)
		);
	$tittle = "EDIT TRANSACTION ".$allocation2."";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='deposit.php';\">"
	);
}

if ($_POST['btn_save_edit']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
 		$txt_bank = trim(htmlentities($_POST['txt_bank']));
		$txt_placement = trim(htmlentities($_POST['txt_placement']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
		//$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_indikator = trim(htmlentities($_POST['txt_indikator']));
		$txt_face = trim(htmlentities($_POST['txt_face']));
		$txt_total_aktiv = trim(htmlentities(str_replace(",","",$_POST['txt_int_rate'])));
		$txt_int_rate = trim(htmlentities(str_replace(",","",$_POST['txt_int_rate'])));
		$txt_deposit_id = trim(htmlentities($_POST['txt_deposit_id']));
		$txt_placement_bank_code = trim(htmlentities($_POST['txt_placement_bank_code']));
		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_action_type = trim(htmlentities($_POST['txt_action_type']));
		$txt_placement_bank_branch_code = trim(htmlentities($_POST['txt_placement_bank_branch_code']));
		$txt_placement_bank_cash_name = trim(htmlentities($_POST['txt_placement_bank_cash_name']));
		$txt_placement_bank_cash_no = trim(htmlentities($_POST['txt_placement_bank_cash_no']));
		$txt_ccy = trim(htmlentities($_POST['txt_ccy']));
		$txt_interest_frequency = trim(htmlentities($_POST['txt_interest_frequency']));
		$txt_interest_type = trim(htmlentities($_POST['txt_interest_type']));
		$txt_sharia_deposit = trim(htmlentities($_POST['txt_sharia_deposit']));
		$txt_withdrawal_date = trim(htmlentities($_POST['txt_withdrawal_date']));
		$txt_adjusted_interest_rate = trim(htmlentities(str_replace(",","",$_POST['txt_adjusted_interest_rate'])));
		$txt_withdrawal_principle = trim(htmlentities(str_replace(",","",$_POST['txt_withdrawal_principle'])));
		$txt_withdrawal_interest = trim(htmlentities(str_replace(",","",$_POST['txt_withdrawal_interest'])));
		$txt_total_withdrawal = trim(htmlentities(str_replace(",","",$_POST['txt_total_withdrawal'])));
		$txt_rollover_type = trim(htmlentities($_POST['txt_rollover_type']));
		$txt_new_principle_amount = trim(htmlentities(str_replace(",","",$_POST['txt_new_principle_amount'])));
		$txt_new_interest_rate = trim(htmlentities(str_replace(",","",$_POST['txt_new_interest_rate'])));
		$txt_new_maturity_date = trim(htmlentities($_POST['txt_new_maturity_date']));
		$txt_amount_to_be_transfered = trim(htmlentities(str_replace(",","",$_POST['txt_amount_to_be_transfered'])));
		$txt_statutory_type = trim(htmlentities($_POST['txt_statutory_type']));
		$txt_contact_person = trim(htmlentities($_POST['txt_contact_person']));
		$txt_telephone_no = trim(htmlentities($_POST['txt_telephone_no']));
		$txt_fax_no = trim(htmlentities($_POST['txt_fax_no']));
		$txt_reference_no = trim(htmlentities($_POST['txt_reference_no']));
		$txt_parent_reference_no = trim(htmlentities($_POST['txt_parent_reference_no']));
		$txt_description = trim(htmlentities($_POST['txt_description']));
		$txt_cancellation_reason = trim(htmlentities($_POST['txt_cancellation_reason']));
		/*
		$txt_rtgs = trim(htmlentities($_POST['txt_rtgs']));
		$txt_sandiBI = trim(htmlentities($_POST['txt_sandiBI']));
		$txt_sandikliring = trim(htmlentities($_POST['txt_sandikliring']));		
		*/

		if($txt_bank==''){
			echo "<script>alert('Name Bank is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_placement=''){
			echo "<script>alert('Placement Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_maturity==''){
			echo "<script>alert('Maturity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_total_aktiv==''){
			echo "<script>alert('Total aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
			if($txt_int_rate==''){
			echo "<script>alert('Interest is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_face==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}

		$sql = "update tbl_kr_deposit set
			name_bank = '".$txt_bank."',
			face_value = '".$txt_face."',
			placement = '".$_POST[txt_placement]."',
			maturity = '".$txt_maturity."',
			total_aktiv= '".$txt_total_aktiv."',
			indikator_maks ='".$txt_indikator."',
			int_rate = '".$txt_int_rate."',
			transaction_status = '".$txt_transaction_status."',
			action_type = '$txt_action_type',
			placement_bank_code = '$txt_placement_bank_code',
			branch_code = '$txt_placement_bank_branch_code',
			placement_bank_cash_name = '$txt_placement_bank_cash_name',
			placement_bank_cash_no = '$txt_placement_bank_cash_no',
			ccy = '$txt_ccy',
			interest_frequency = '$txt_interest_frequency',
			interest_type = '$txt_interest_type',
			sharia_deposit = '$txt_sharia_deposit',
			withdrawal_date = '$txt_withdrawal_date',
			adjusted_interest_rate = '$txt_adjusted_interest_rate',
			withdrawal_principle = '$txt_withdrawal_principle',
			withdrawal_interest = '$txt_withdrawal_interest',
			total_withdrawal_amount = '$txt_total_withdrawal',
			rollover_type = '$txt_rollover_type',
			new_principle_amount = '$txt_new_principle_amount',
			new_interest_rate = '$txt_new_interest_rate',
			new_maturity_date = '$txt_new_maturity_date',
			amount_to_be_transferred = '$txt_amount_to_be_transfered',
			statutory_type = '$txt_statutory_type',
			contact_person = '$txt_contact_person',
			telephone_no = '$txt_telephone_no',
			fax_no = '$txt_fax_no',
			reference_no = '$txt_reference_no',
			parent_reference_no = '$txt_parent_reference_no',
			description = '$txt_description',
			cancellation_reason = '$txt_cancellation_reason'
			where pk_id = '".$_GET[id]."'";
			#print_r($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}

		$sql1 = "update tbl_kr_deposit_hist set
			name_bank = '".$txt_bank."',
			face_value = '".$txt_face."',
			placement = '".$_POST[txt_placement]."',
			maturity = '".$txt_maturity."',
			total_aktiv= '".$txt_total_aktiv."',
			indikator_maks ='".$txt_indikator."',
			int_rate = '".$txt_int_rate."',
			deposit_id='$txt_deposit_id',
			transaction_status = '".$txt_transaction_status."',
			action_type = '$txt_action_type',
			placement_bank_code = '$txt_placement_bank_code',
			branch_code = '$txt_placement_bank_branch_code',
			placement_bank_cash_name = '$txt_placement_bank_cash_name',
			placement_bank_cash_no = '$txt_placement_bank_cash_no',
			ccy = '$txt_ccy',
			interest_frequency = '$txt_interest_frequency',
			interest_type = '$txt_interest_type',
			sharia_deposit = '$txt_sharia_deposit',
			withdrawal_date = '$txt_withdrawal_date',
			adjusted_interest_rate = '$txt_adjusted_interest_rate',
			withdrawal_principle = '$txt_withdrawal_principle',
			withdrawal_interest = '$txt_withdrawal_interest',
			total_withdrawal_amount = '$txt_total_withdrawal',
			rollover_type = '$txt_rollover_type',
			new_principle_amount = '$txt_new_principle_amount',
			new_interest_rate = '$txt_new_interest_rate',
			new_maturity_date = '$txt_new_maturity_date',
			amount_to_be_transferred = '$txt_amount_to_be_transfered',
			statutory_type = '$txt_statutory_type',
			contact_person = '$txt_contact_person',
			telephone_no = '$txt_telephone_no',
			fax_no = '$txt_fax_no',
			reference_no = '$txt_reference_no',
			parent_reference_no = '$txt_parent_reference_no',
			description = '$txt_description',
			cancellation_reason = '$txt_cancellation_reason'
			where deposit_id = '".$_GET[id]."'";
			#print_r($sql);
		if (!$data->inpQueryReturnBool($sql1)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='deposit_hist.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['detail']=='1'){
	$rows = $data->get_row("select * from tbl_kr_deposit where pk_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];

	$transaction_status = "";
	if($rows['transaction_status'] == 'NEWM') $transaction_status = "New Trade";
	else if($rows['transaction_status'] == 'CANC') $transaction_status = "Cancel Trade";

	$action_type = "";
	if($rows['action_type'] == 1) $action_type = "Deposit Placement";
	else if($rows['action_type'] == 2) $action_type = "Deposit Withdrawal";
	else if($rows['action_type'] == 3) $action_type = "Deposit Rollover";
	
	$ccy = "";
	if($rows['ccy'] == 'IDR') $ccy = "Indonesia Rupiah";
	else if($rows['ccy'] == 'USD') $ccy = "US Dollar";
	else if($rows['ccy'] == 'EUR') $ccy = "EU Euro";

	$interest_frequency = "";
	if($rows['interest_frequency'] == 1) $interest_frequency = "On Due Date";
	else if($rows['interest_frequency'] == 2) $interest_frequency = "Daily";
	else if($rows['interest_frequency'] == 3) $interest_frequency = "Weekly";
	else if($rows['interest_frequency'] == 4) $interest_frequency = "Monthly";
	else if($rows['interest_frequency'] == 5) $interest_frequency = "Quarterly";
	else if($rows['interest_frequency'] == 6) $interest_frequency = "Semiannually";
	else if($rows['interest_frequency'] == 7) $interest_frequency = "Yearly";

	$interest_type = "";
	if($rows['interest_type'] == 1) $interest_type = "Fixed";
	else if($rows['interest_type'] == 2) $interest_type = "Compound";
	else if($rows['interest_type'] == 3) $interest_type = "Step/Floating";

	$sharia_deposit = "";
	if($rows['sharia_deposit'] == 'Y') $sharia_deposit = "Yes";
	else if($rows['sharia_deposit'] == 'N') $sharia_deposit = "No";

	$rollover_type = "";
	if($rows['rollover_type'] == 1) $rollover_type = "Principle Only";
	else if($rows['rollover_type'] == 2) $rollover_type = "Principle + Interest";

	$statutory_type = "";
	if($rows['statutory_type'] == 'Y') $statutory_type = "Yes";
	else if($rows['statutory_type'] == 'N') $statutory_type = "No";

	$dataRows = array (
		'TEXT' =>  array(
						'allocation',
						'Placement Bank Code',
						'Name of Bank',
						'Placement Date',
						'Maturity Date',
						'Total Aktiv',
						'Indikator Maks',
						'Quantity',
						'&nbsp;',
						'Transaction Status',
						'Action Type',
						'Branch Code',
						'Placement Bank Cash A/C Name',
						'Placement Bank Cash A/C No',
						'CCY',
						'Interest Frequency',
						'Interest Type',
						'Sharia Deposit',
						'Withdrawal Date',
						'Adjusted Interest Rate (%)/Profit Sharing',
						'Withdrawal Principle',
						'Withdrawal Interest',
						'Total Withdrawal Amount',
						'Rollover Type',
						'New Principle Amount',
						'New Interest rate(%)/Profit Sharing',
						'New Maturity Date',
						'Amount to be transfered (withdrawn)',
						'Statutory Type',
						'Contact Person',
						'Telephone No',
						'Fax No',
						'Reference No',
						'Parent Reference No',
						'Descripton',
						'Cancellation Reason'
						),
		'DOT'  => array (':',':',':',':',':'),
		'FIELD' => array (
			'&nbsp;'.$allocation2,
			'&nbsp;'.$rows[placement_bank_code],
			'&nbsp;'.$rows[name_bank],
			'&nbsp;'.$rows[placement],
			'&nbsp;'.$rows[maturity],
			'&nbsp;'.$rows[total_aktiv],
			'&nbsp;'.$rows[indikator_maks],
			'&nbsp;'.$rows[face_value],
			'&nbsp;',
			'&nbsp;'.$rows[transaction_status],
			'&nbsp;'.$action_type,
			'&nbsp;'.$rows[branch_code],
			'&nbsp;'.$rows[placement_bank_cash_name],
			'&nbsp;'.$rows[placement_bank_cash_no],
			'&nbsp;'.$ccy,
			'&nbsp;'.$interest_frequency,
			'&nbsp;'.$interest_type,
			'&nbsp;'.$sharia_deposit,
			'&nbsp;'.$rows['withdrawal_date'],
			'&nbsp;'.$rows['adjusted_interest_rate'],
			'&nbsp;'.$rows['withdrawal_principle'],
			'&nbsp;'.$rows['withdrawal_interest'],
			'&nbsp;'.$rows['total_withdrawal_amount'],
			'&nbsp;'.$rollover_type,
			'&nbsp;'.$rows['new_principle_amount'],
			'&nbsp;'.$rows['new_interest_rate'],
			'&nbsp;'.$rows['new_maturity_date'],
			'&nbsp;'.$rows['amount_to_be_transferred'],
			'&nbsp;'.$statutory_type,
			'&nbsp;'.$rows['contact_person'],
			'&nbsp;'.$rows['telephone_no'],
			'&nbsp;'.$rows['fax_no'],
			'&nbsp;'.$rows['reference_no'],
			'&nbsp;'.$rows['parent_reference_no'],
			'&nbsp;'.$rows['description'],
			'&nbsp;'.$rows['cancellation_reason'],
			)
		);

	if($rows['action_type']==1){
		$linkPrint="deposit_print_newplacement.php?id=".$_GET[id];
	}elseif($rows['action_type']==2){
		$linkPrint="deposit_print_liquidation.php?id=".$_GET[id];
	}else{
		$linkPrint="deposit_print_rollover.php?id=".$_GET[id];
	}
	$tittle = "TIME DEPOSIT DETAIL ".$allocation2."";
    $button = array ('SUBMIT' => "<input type=button name=print value=print onclick=\"window.location='$linkPrint';\">",
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"window.location='deposit.php';\">"
	);
}

$javascript = "<script type='text/javascript'>
	function changeType(){
		var doc = document;

		var actionType = doc.getElementById('txt_action_type').value;
		if(actionType == 1){
			doc.getElementById('txt_withdrawal_date').readOnly = true;
			doc.getElementById('txt_adjusted_interest_rate').readOnly = true;
			doc.getElementById('txt_withdrawal_principle').readOnly = true;
			doc.getElementById('txt_withdrawal_interest').readOnly = true;
			doc.getElementById('txt_total_withdrawal').readOnly = true;

			doc.getElementById('txt_rollover_type').readOnly = true;
			doc.getElementById('txt_new_principle_amount').readOnly = true;
			doc.getElementById('txt_new_interest_rate').readOnly = true;
			doc.getElementById('txt_new_maturity_date').readOnly = true;

			doc.getElementById('txt_parent_reference_no').readOnly = true;
			doc.getElementById('txt_withdrawal_date').style.visibility = (doc.getElementById('txt_withdrawal_date').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_new_maturity_date').style.visibility = (doc.getElementById('txt_new_maturity_date').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_rollover_type').style.visibility = (doc.getElementById('txt_rollover_type').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_deposit_id').style.visibility = (doc.getElementById('txt_deposit_id').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_val').style.visibility = (doc.getElementById('txt_val').style.visibility==='visible')?'hidden':'hidden';
		}else if(actionType == 2){
			doc.getElementById('txt_withdrawal_date').readOnly = false;
			doc.getElementById('txt_adjusted_interest_rate').readOnly = false;
			doc.getElementById('txt_withdrawal_principle').readOnly = false;
			doc.getElementById('txt_withdrawal_interest').readOnly = false;
			doc.getElementById('txt_total_withdrawal').readOnly = false;

			doc.getElementById('txt_interest_frequency').disabled = true;
			doc.getElementById('txt_interest_frequency').value = '-';
			doc.getElementById('txt_interest_type').disabled = true;
			doc.getElementById('txt_interest_type').value = '-';

			doc.getElementById('txt_rollover_type').readOnly = true;
			doc.getElementById('txt_new_principle_amount').readOnly = true;
			doc.getElementById('txt_new_interest_rate').readOnly = true;
			doc.getElementById('txt_new_maturity_date').readOnly = true;

			doc.getElementById('txt_parent_reference_no').readOnly = false;
			doc.getElementById('txt_withdrawal_date').style.visibility = (doc.getElementById('txt_withdrawal_date').style.visibility==='hidden')?'visible':'visible';
			doc.getElementById('txt_new_maturity_date').style.visibility = (doc.getElementById('txt_new_maturity_date').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_rollover_type').style.visibility = (doc.getElementById('txt_rollover_type').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_deposit_id').style.visibility = (doc.getElementById('txt_deposit_id').style.visibility==='hidden')?'visible':'visible';
			doc.getElementById('txt_val').style.visibility = (doc.getElementById('txt_val').style.visibility==='hidden')?'visible':'visible';
		}else{
			doc.getElementById('txt_withdrawal_date').readOnly = true;
			doc.getElementById('txt_adjusted_interest_rate').readOnly = true;
			doc.getElementById('txt_withdrawal_principle').readOnly = true;
			doc.getElementById('txt_withdrawal_interest').readOnly = true;
			doc.getElementById('txt_total_withdrawal').readOnly = true;

			doc.getElementById('txt_rollover_type').readOnly = false;
			doc.getElementById('txt_new_principle_amount').readOnly = false;
			doc.getElementById('txt_new_interest_rate').readOnly = false;
			doc.getElementById('txt_new_maturity_date').readOnly = false;

			doc.getElementById('txt_interest_type').disabled = true;
			doc.getElementById('txt_interest_type').value = '-';
			
			doc.getElementById('txt_parent_reference_no').readOnly = false;
			doc.getElementById('txt_withdrawal_date').style.visibility = (doc.getElementById('txt_withdrawal_date').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_new_maturity_date').style.visibility = (doc.getElementById('txt_new_maturity_date').style.visibility==='hidden')?'visible':'visible';
			doc.getElementById('txt_rollover_type').style.visibility = (doc.getElementById('txt_rollover_type').style.visibility==='hidden')?'visible':'visible';
			doc.getElementById('txt_deposit_id').style.visibility = (doc.getElementById('txt_deposit_id').style.visibility==='hidden')?'visible':'visible';
			doc.getElementById('txt_val').style.visibility = (doc.getElementById('txt_val').style.visibility==='hidden')?'visible':'visible';
		}
	}
</script>";

if ($_GET['detail']=='1'){
	$javascriptBottom = "";
}else
	$javascriptBottom = "<script>changeType()</script>";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css',
	  'PATHCUSTOMJS' => $GLOBALS['JS'].'custom.js',
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('bottom','javascriptbottom',$javascriptBottom );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>