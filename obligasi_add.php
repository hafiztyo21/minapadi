<?php
session_start();
error_reporting(0);
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('obligasi_add.html');
$tablename = 'tbl_kr_me_bonds';

if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}

		$me_id = $_SESSION['pk_id'];
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
		$txt_code = trim(htmlentities($_POST['txt_code']));
		$txt_ct_obl = trim(htmlentities($_POST['txt_ct_obl']));
		$txt_name = trim(htmlentities($_POST['txt_name']));
		$txt_rating = trim(htmlentities($_POST['txt_rating']));
		$txt_face = trim(htmlentities(str_replace(",","",$_POST['txt_face'])));
		$txt_rate = trim(htmlentities($_POST['txt_rate']));
		$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_indikator_maks = trim(htmlentities($_POST['txt_indikator_maks']));
		$txt_coupon_rate = trim(htmlentities($_POST['txt_coupon_rate']));
		$txt_harga_minus = trim(htmlentities($_POST['txt_harga_minus']));
		$txt_market_price = trim(htmlentities($_POST['txt_market_price']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
		$txt_prev_coupon = trim(htmlentities($_POST['txt_prev_coupon']));
		$txt_next_coupon = trim(htmlentities($_POST['txt_next_coupon']));
		$txt_seller = trim(htmlentities($_POST['txt_seller']));
		$txt_settle = trim(htmlentities($_POST['txt_settle']));
		
		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
		$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
		$txt_buy_sell = trim(htmlentities($_POST['txt_buy_sell']));
		$txt_accrued_days = trim(htmlentities($_POST['txt_accrued_days']));
		$txt_accrued_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_accrued_interest_amount'])));
		$txt_other_fee = trim(htmlentities(str_replace(",","",$_POST['txt_other_fee'])));
		$txt_capital_gain_tax = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain_tax'])));
		$txt_interest_income_tax = trim(htmlentities(str_replace(",","",$_POST['txt_interest_income_tax'])));
		$txt_withholding_tax = trim(htmlentities(str_replace(",","",$_POST['txt_withholding_tax'])));
		$txt_net_proceeds = trim(htmlentities(str_replace(",","",$_POST['txt_net_proceeds'])));
		$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
		$txt_sellers_tax_id = trim(htmlentities($_POST['txt_sellers_tax_id']));
		$txt_purpose_of_transaction = trim(htmlentities($_POST['txt_purpose_of_transaction']));
		$txt_statutory_type = trim(htmlentities($_POST['txt_statutory_type']));
		$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
		$txt_cancellation_reason = trim(htmlentities($_POST['txt_cancellation_reason']));

		$txt_comp_type = trim(htmlentities($_POST['txt_comp_type']));
		$txt_acquisition_date = trim(htmlentities($_POST['txt_acquisition_date']));
		$txt_acquisition_price = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_price'])));
		$txt_acquisition_amount = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_amount'])));
		$txt_capital_gain = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain'])));
		$txt_days_of_holding_interest = trim(htmlentities(str_replace(",","",$_POST['txt_days_of_holding_interest'])));
		$txt_holding_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_holding_interest_amount'])));
		$txt_total_taxable_income = trim(htmlentities(str_replace(",","",$_POST['txt_total_taxable_income'])));
		$txt_tax_rate = trim(htmlentities(str_replace(",","",$_POST['txt_tax_rate'])));
		$txt_tax_amount = trim(htmlentities(str_replace(",","",$_POST['txt_tax_amount'])));	
		$txt_trade = trim(htmlentities($_POST['txt_trade']));
		$txt_sec = trim(htmlentities($_POST['txt_sec']));
		$txt_sec_code = trim(htmlentities($_POST['txt_sec_code']));

		$txt_custody = trim(htmlentities($_POST['txt_custody']));
		$txt_subreg = trim(htmlentities($_POST['txt_subreg']));
		$txt_client_code = trim(htmlentities($_POST['txt_client_code']));
		$txt_pic = trim(htmlentities($_POST['txt_pic']));
		$txt_telp = trim(htmlentities($_POST['txt_telp']));
		$txt_fax = trim(htmlentities($_POST['txt_fax']));

		$txt_ct_obl = trim(htmlentities($_POST['txt_ct_obl']));
		
		$arr_obl=array();
		$arr_val=array();

		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_name ==''){
			echo "<script>alert('Bonds name is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
        if($txt_face==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_rate ==''){
			echo "<script>alert('Harga Perolehan is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_aktiv ==''){
			echo "<script>alert('% Terhadap Total Aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		/*
		if($txt_indikator_maks ==''){
			echo "<script>alert('Indikator maks is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
		if($txt_harga_minus ==''){
			echo "<script>alert('Harga Per H-1 is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		*/
		if($txt_prev_coupon ==''){
			echo "<script>alert('Prev coupon is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	
		if($txt_next_coupon ==''){
			echo "<script>alert('Next coupon is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	
		if($txt_seller ==''){
			echo "<script>alert('Seller  is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
		/*if($txt_market_price==''){
			echo "<script>alert('Market price is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}*/
		$a = $txt_market_price/100;
		//$txt_market_value = $txt_face * $a ;
		$txt_market_value = $txt_face * $txt_rate/100;
		// print_r($txt_market_value);
				
		if($txt_buy_sell==2){

			if($txt_ct_obl==''){
				echo "<script>alert('None of One Bonds Was Selected!');</script>";
				throw new Exception($data->err_report('s02'));
			}
				#echo "sell";
			$rowxx= $data->get_rows2("select tbl_kr_me_bonds.*, format(market_value,0) as VAL from tbl_kr_me_bonds where code = '".$txt_code."' and allocation = '".$txt_allocation."' order by create_dt ASC");

			if($rowxx == null){
				#echo "null";
				echo "<script>alert('Belum ada pembelian obligasi '".$txt_code."' !');</script>";
				#throw new Exception($data->err_report('s02'));
			}
			else
			{
				#echo "isi";

				for($count_det=1;$count_det<=$txt_ct_obl;$count_det++){
					$txt_obl_id = '';
					$txt_val_obl = '';
					$txt_obl_id = trim(htmlentities($_POST['txt_obl_id'.$count_det]));
					$txt_val_obl = trim(htmlentities($_POST['value'.$count_det]));
					if($txt_obl_id!=''){
						$arr_obl[$count_det]=$txt_obl_id;
						$arr_val[$count_det]=$txt_val_obl;
						//echo "<script>alert('".$txt_obl_id."')</script>";
						$rowy = $data->get_row("select tbl_kr_me_bonds_hist.*, format(market_value,0) as VAL from tbl_kr_me_bonds_hist where fk_obligasi = '".$txt_obl_id."' ");
						$rowx = $data->get_row("select tbl_kr_me_bonds.*, format(market_value,0) as VAL from tbl_kr_me_bonds where obligasi_id='".$txt_obl_id."' order by create_dt ASC");
						//echo "<script>alert('".$rowx[obligasi_id]."')</script>";
						$value=$rowx['face_value'];
						$val_now=$value-$txt_val_obl;
						//echo "<script>alert('".$val_now."')</script>";
						//echo "<script>alert('masuk sini')</script>";
						$sql_move = "INSERT INTO tbl_kr_me_bonds_hist (
							fk_obligasi,
							code,
							name_bonds,
							rating,
							maturity,
							last_coupon,
							next_coupon,
							seller,
							settlement_date,
							trade_date,
							face_value,
							int_rate,
							total_aktiv,
							indikator_maks,
							coupon_rate,
							harga_minus_one,
							market_price,
							market_value,
							allocation,
							create_dt,
							transaction_status,
							ta_reference_id,
							ta_reference_no,
							buy_sell,
							accrued_days,
							accrued_interest_amount,
							other_fee,
							capital_gain_tax,
							interest_income_tax,
							withholding_tax,
							net_proceeds,
							settlement_type,
							sellers_tax_id,
							purpose_of_transaction,
							statutory_type,
							remarks,
							cancellation_reason,
							acquisition_date,
							acquisition_price,
							acquisition_amount,
							capital_gain,
							days_of_holding_interest,
							holding_interest_amount,
							total_taxable_income,
							tax_rate,
							tax_amount,
							im_code,
							br_code,
							securitas_id,
							company_type
						)VALUES(
							'".$rowx[obligasi_id]."',
							'".$rowx[code]."',
							'".$rowx[name_bonds]."',
							'".$rowx[rating]."',
							'".$rowx[maturity]."',
							'".$rowx[prev_coupon]."',
							'".$rowx[next_coupon]."',
							'".$rowx[seller]."',
							'".$rowx[settlement_date]."',
							'".$rowx[trade_date]."',
							'".$txt_val_obl."',
							'".$rowx[int_rate]."',
							'".$rowx[total_aktiv]."',
							'".$rowx[indikator_maks]."',
							'".$rowx[coupon_rate]."',
							'".$rowx[harga_minus_one]."',
							'".$rowx[market_price]."',
							'".$rowx[market_value]."',
							'".$rowx[allocation]."',
							'".$rowx[create_dt]."',
							'".$rowx[transaction_status]."',
							'".$rowx[ta_reference_id]."',
							'".$rowx[ta_reference_no]."',
							'1',
							'".$rowx[accrued_days]."',
							'".$rowx[accrued_interest_amount]."',
							'".$rowx[other_fee]."',
							'".$rowx[capital_gain_tax]."',
							'".$rowx[interest_income_tax]."',
							'".$rowx[withholding_tax]."',
							'".$rowx[net_proceeds]."',
							'".$rowx[settlement_type]."',
							'".$rowx[sellers_tax_id]."',
							'".$rowx[purpose_of_transaction]."',
							'".$rowx[statutory_type]."',
							'".$rowx[remarks]."',
							'".$rowx[cancellation_reason]."',
							'".$rowx[acquisition_date]."',
							'".$rowx[acquisition_price]."',
							'".$rowx[acquisition_amount]."',
							'".$rowx[capital_gain]."',
							'".$rowx[days_of_holding_interest]."',
							'".$rowx[holding_interest_amount]."',
							'".$rowx[total_taxable_income]."',
							'".$rowx[tax_rate]."',
							'".$rowx[tax_amount]."',
							'MU002',
							'".$rowx[br_code]."',
							'".$rowx[securitas_id]."',
							'".$rowx[company_type]."'
						)";
						//echo $sql_move."<br>";
						$exe_move=mysql_query($sql_move);
				
						if($value==$txt_val_obl){
							$sql_del="DELETE FROM tbl_kr_me_bonds WHERE obligasi_id='".$rowx[obligasi_id]."' ";
						}else{								
							$sql_del="UPDATE tbl_kr_me_bonds SET face_value ='".$val_now."' WHERE obligasi_id='".$rowx[obligasi_id]."' ";
						}
						//echo $sql_del."<br>";
						$exe_del=mysql_query($sql_del);
					}
				}

				if($count_det==1){
					$sql1 = "INSERT INTO tbl_kr_me_bonds_hist (
						code,
						name_bonds,
						rating,
						maturity,
						last_coupon,
						next_coupon,
						seller,
						settlement_date,
						trade_date,
						face_value,
						int_rate,
						total_aktiv,
						indikator_maks,
						coupon_rate,
						harga_minus_one,
						market_price,
						market_value,
						allocation,
						create_dt,
						transaction_status,
						ta_reference_id,
						ta_reference_no,
						buy_sell,
						accrued_days,
						accrued_interest_amount,
						other_fee,
						capital_gain_tax,
						interest_income_tax,
						withholding_tax,
						net_proceeds,
						settlement_type,
						sellers_tax_id,
						purpose_of_transaction,
						statutory_type,
						remarks,
						cancellation_reason,
						acquisition_date,
						acquisition_price,
						acquisition_amount,
						capital_gain,
						days_of_holding_interest,
						holding_interest_amount,
						total_taxable_income,
						tax_rate,
						tax_amount,
						im_code,
						br_code,
						securitas_id,
						custody,
						subreg,
						client_code,
						pic_name,
						pic_telp,
						pic_fax,
						company_type
					)VALUES(
						'$txt_code',
						'$txt_name',
						'$txt_rating',
						'$txt_maturity',
						'$txt_prev_coupon',
						'$txt_next_coupon',
						'$txt_seller',
						'$txt_settle',
						'$txt_trade',
						'$txt_face',
						'$txt_rate',
						'$txt_total_aktiv',
						'$txt_indikator_maks',
						'$txt_coupon_rate',
						'$txt_harga_minus',
						'$txt_market_price',
						'$txt_market_value',
						'$txt_allocation',
						now(),
						'$txt_transaction_status',
						'$txt_ta_reference_id',
						'$txt_ta_reference_no',
						'$txt_buy_sell',
						'$txt_accrued_days',
						'$txt_accrued_interest_amount',
						'$txt_other_fee',
						'$txt_capital_gain_tax',
						'$txt_interest_income_tax',
						'$txt_withholding_tax',
						'$txt_net_proceeds',
						'$txt_settlement_type',
						'$txt_sellers_tax_id',
						'$txt_purpose_of_transaction',
						'$txt_statutory_type',
						'$txt_remarks',
						'$txt_cancellation_reason',
						'$txt_acquisition_date',
						'$txt_acquisition_price',
						'$txt_acquisition_amount',
						'$txt_capital_gain',
						'$txt_days_of_holding_interest',
						'$txt_holding_interest_amount',
						'$txt_total_taxable_income',
						'$txt_tax_rate',
						'$txt_tax_amount',
						'MU002',
						'$txt_sec_code',
						'$txt_sec',
						'$txt_custody',
						'$txt_subreg',
						'$txt_client_code',
						'$txt_pic',
						'$txt_telp',
						'$txt_fax',
						'$txt_comp_type'
					)";

				}else{
					$sql1 = "INSERT INTO tbl_kr_me_bonds_hist (
						code,
						name_bonds,
						rating,
						maturity,
						last_coupon,
						next_coupon,
						seller,
						settlement_date,
						trade_date,
						face_value,
						int_rate,
						total_aktiv,
						indikator_maks,
						coupon_rate,
						harga_minus_one,
						market_price,
						market_value,
						allocation,
						create_dt,
						transaction_status,
						ta_reference_id,
						ta_reference_no,
						buy_sell,
						accrued_days,
						accrued_interest_amount,
						other_fee,
						capital_gain_tax,
						interest_income_tax,
						withholding_tax,
						net_proceeds,
						settlement_type,
						sellers_tax_id,
						purpose_of_transaction,
						statutory_type,
						remarks,
						cancellation_reason,
						acquisition_date,
						acquisition_price,
						acquisition_amount,
						capital_gain,
						days_of_holding_interest,
						holding_interest_amount,
						total_taxable_income,
						tax_rate,
						tax_amount,
						im_code,
						br_code,
						securitas_id,
						custody,
						subreg,
						client_code,
						pic_name,
						pic_telp,
						pic_fax,
						company_type,
						fk_obligasi
					)VALUES(
						'$txt_code',
						'$txt_name',
						'$txt_rating',
						'$txt_maturity',
						'$txt_prev_coupon',
						'$txt_next_coupon',
						'$txt_seller',
						'$txt_settle',
						'$txt_trade',
						'$txt_face',
						'$txt_rate',
						'$txt_total_aktiv',
						'$txt_indikator_maks',
						'$txt_coupon_rate',
						'$txt_harga_minus',
						'$txt_market_price',
						'$txt_market_value',
						'$txt_allocation',
						now(),
						'$txt_transaction_status',
						'$txt_ta_reference_id',
						'$txt_ta_reference_no',
						'$txt_buy_sell',
						'$txt_accrued_days',
						'$txt_accrued_interest_amount',
						'$txt_other_fee',
						'$txt_capital_gain_tax',
						'$txt_interest_income_tax',
						'$txt_withholding_tax',
						'$txt_net_proceeds',
						'$txt_settlement_type',
						'$txt_sellers_tax_id',
						'$txt_purpose_of_transaction',
						'$txt_statutory_type',
						'$txt_remarks',
						'$txt_cancellation_reason',
						'$txt_acquisition_date',
						'$txt_acquisition_price',
						'$txt_acquisition_amount',
						'$txt_capital_gain',
						'$txt_days_of_holding_interest',
						'$txt_holding_interest_amount',
						'$txt_total_taxable_income',
						'$txt_tax_rate',
						'$txt_tax_amount',
						'MU002',
						'$txt_sec_code',
						'$txt_sec',
						'$txt_custody',
						'$txt_subreg',
						'$txt_client_code',
						'$txt_pic',
						'$txt_telp',
						'$txt_fax',
						'$txt_comp_type',
						'0'
					)";
				}
				if (!$data->inpQueryReturnBool($sql1)){
					throw new Exception($data->err_report('s02'));
				}

				$rowe = $data->get_row("select tbl_kr_me_bonds_hist.*, format(market_value,0) as VAL from tbl_kr_me_bonds_hist where code = '".$txt_code."' and allocation = '".$txt_allocation."' and create_dt=DATE(NOW()) order by obligasi_id DESC LIMIT 1");
				$link='obligasi_sell_print.php?id='.$rowe[obligasi_id].'&hist=1';
			}
		}
		else
		{
			$sql1 = "INSERT INTO tbl_kr_me_bonds (
				code,
				name_bonds,
				rating,
				maturity,
				last_coupon,
				next_coupon,
				seller,
				settlement_date,
				trade_date,
				face_value,
				int_rate,
				total_aktiv,
				indikator_maks,
				coupon_rate,
				harga_minus_one,
				market_price,
				market_value,
				allocation,
				create_dt,
				transaction_status,
				ta_reference_id,
				ta_reference_no,
				buy_sell,
				accrued_days,
				accrued_interest_amount,
				other_fee,
				capital_gain_tax,
				interest_income_tax,
				withholding_tax,
				net_proceeds,
				settlement_type,
				sellers_tax_id,
				purpose_of_transaction,
				statutory_type,
				remarks,
				cancellation_reason,
				acquisition_date,
				acquisition_price,
				acquisition_amount,
				capital_gain,
				days_of_holding_interest,
				holding_interest_amount,
				total_taxable_income,
				tax_rate,
				tax_amount,
				im_code,
				br_code,
				securitas_id,
				company_type
			)VALUES(
				'$txt_code',
				'$txt_name',
				'$txt_rating',
				'$txt_maturity',
				'$txt_prev_coupon',
				'$txt_next_coupon',
				'$txt_seller',
				'$txt_settle',
				'$txt_trade',
				'$txt_face',
				'$txt_rate',
				'$txt_total_aktiv',
				'$txt_indikator_maks',
				'$txt_coupon_rate',
				'$txt_harga_minus',
				'$txt_market_price',
				'$txt_market_value',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_ta_reference_id',
				'$txt_ta_reference_no',
				'$txt_buy_sell',
				'$txt_accrued_days',
				'$txt_accrued_interest_amount',
				'$txt_other_fee',
				'$txt_capital_gain_tax',
				'$txt_interest_income_tax',
				'$txt_withholding_tax',
				'$txt_net_proceeds',
				'$txt_settlement_type',
				'$txt_sellers_tax_id',
				'$txt_purpose_of_transaction',
				'$txt_statutory_type',
				'$txt_remarks',
				'$txt_cancellation_reason',
				'$txt_acquisition_date',
				'$txt_acquisition_price',
				'$txt_acquisition_amount',
				'$txt_capital_gain',
				'$txt_days_of_holding_interest',
				'$txt_holding_interest_amount',
				'$txt_total_taxable_income',
				'$txt_tax_rate',
				'$txt_tax_amount',
				'MU002',
				'$txt_sec_code',
				'$txt_sec',
				'$txt_comp_type'
			)";

			if (!$data->inpQueryReturnBool($sql1)){
				throw new Exception($data->err_report('s02'));
			}
			$rowz = $data->get_row("select tbl_kr_me_bonds.* from tbl_kr_me_bonds where code = '".$txt_code."' and allocation = '".$txt_allocation."' and create_dt=DATE(NOW()) order by obligasi_id DESC LIMIT 1");
			#echo "select tbl_kr_me_bonds.* from tbl_kr_me_bonds where code = '".$txt_code."' and allocation = '".$txt_allocation."' and create_dt=DATE(NOW())";
			$sql2 = "INSERT INTO tbl_kr_me_bonds_hist (
				fk_obligasi,
				code,
				name_bonds,
				rating,
				maturity,
				last_coupon,
				next_coupon,
				seller,
				settlement_date,
				trade_date,
				face_value,
				int_rate,
				total_aktiv,
				indikator_maks,
				coupon_rate,
				harga_minus_one,
				market_price,
				market_value,
				allocation,
				create_dt,
				transaction_status,
				ta_reference_id,
				ta_reference_no,
				buy_sell,
				accrued_days,
				accrued_interest_amount,
				other_fee,
				capital_gain_tax,
				interest_income_tax,
				withholding_tax,
				net_proceeds,
				settlement_type,
				sellers_tax_id,
				purpose_of_transaction,
				statutory_type,
				remarks,
				cancellation_reason,
				acquisition_date,
				acquisition_price,
				acquisition_amount,
				capital_gain,
				days_of_holding_interest,
				holding_interest_amount,
				total_taxable_income,
				tax_rate,
				tax_amount,
				im_code,
				br_code,
				securitas_id,
				custody,
				subreg,
				client_code,
				pic_name,
				pic_telp,
				pic_fax,
				company_type
			)VALUES(
				'$rowz[obligasi_id]',
				'$txt_code',
				'$txt_name',
				'$txt_rating',
				'$txt_maturity',
				'$txt_prev_coupon',
				'$txt_next_coupon',
				'$txt_seller',
				'$txt_settle',
				'$txt_trade',
				'$txt_face',
				'$txt_rate',
				'$txt_total_aktiv',
				'$txt_indikator_maks',
				'$txt_coupon_rate',
				'$txt_harga_minus',
				'$txt_market_price',
				'$txt_market_value',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_ta_reference_id',
				'$txt_ta_reference_no',
				'$txt_buy_sell',
				'$txt_accrued_days',
				'$txt_accrued_interest_amount',
				'$txt_other_fee',
				'$txt_capital_gain_tax',
				'$txt_interest_income_tax',
				'$txt_withholding_tax',
				'$txt_net_proceeds',
				'$txt_settlement_type',
				'$txt_sellers_tax_id',
				'$txt_purpose_of_transaction',
				'$txt_statutory_type',
				'$txt_remarks',
				'$txt_cancellation_reason',
				'$txt_acquisition_date',
				'$txt_acquisition_price',
				'$txt_acquisition_amount',
				'$txt_capital_gain',
				'$txt_days_of_holding_interest',
				'$txt_holding_interest_amount',
				'$txt_total_taxable_income',
				'$txt_tax_rate',
				'$txt_tax_amount',
				'MU002',
				'$txt_sec_code',
				'$txt_sec',
				'$txt_custody',
				'$txt_subreg',
				'$txt_client_code',
				'$txt_pic',
				'$txt_telp',
				'$txt_fax',
				'$txt_comp_type'
			)";
			if (!$data->inpQueryReturnBool($sql2)){
				throw new Exception($data->err_report('s02'));
			}
			$link='obligasi_buy_print.php?id='.$rowz[obligasi_id].'';
		}
			//print_r($sql);
		#echo $sql1;
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='$link';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
	}
}

if ($_POST['btn_hitung']=='calculate'){
	$face = $_POST[txt_face];
	$price = $_POST[txt_market_price]/100;

	$hasil = $face*$price;
	//print($hasil);
}

if ($_GET['add']==1){

	$txt_allocation = trim(htmlentities($_POST['txt_allocation']));
	if($txt_allocation==''){
		$txt_allocation=1;
	}
	$txt_code = trim(htmlentities($_POST['txt_code']));
	$txt_name = trim(htmlentities($_POST['txt_name']));
	$txt_rating = trim(htmlentities($_POST['txt_rating']));
	$txt_face = trim(htmlentities(str_replace(",","",$_POST['txt_face'])));
	$txt_rate = trim(htmlentities($_POST['txt_rate']));
	$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
	$txt_indikator_maks = trim(htmlentities($_POST['txt_indikator_maks']));
	$txt_coupon_rate = trim(htmlentities($_POST['txt_coupon_rate']));
	$txt_harga_minus = trim(htmlentities($_POST['txt_harga_minus']));
	$txt_market_price = trim(htmlentities($_POST['txt_market_price']));
	$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
	$txt_prev_coupon = trim(htmlentities($_POST['txt_prev_coupon']));
	$txt_next_coupon = trim(htmlentities($_POST['txt_next_coupon']));
	$txt_seller = trim(htmlentities($_POST['txt_seller']));
	$txt_settle = trim(htmlentities($_POST['txt_settle']));
	
	$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
	$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
	$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
	$txt_buy_sell = trim(htmlentities($_POST['txt_buy_sell']));
	$txt_accrued_days = trim(htmlentities($_POST['txt_accrued_days']));
	$txt_accrued_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_accrued_interest_amount'])));
	$txt_other_fee = trim(htmlentities(str_replace(",","",$_POST['txt_other_fee'])));
	$txt_capital_gain_tax = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain_tax'])));
	$txt_interest_income_tax = trim(htmlentities(str_replace(",","",$_POST['txt_interest_income_tax'])));
	$txt_withholding_tax = trim(htmlentities(str_replace(",","",$_POST['txt_withholding_tax'])));
	$txt_net_proceeds = trim(htmlentities(str_replace(",","",$_POST['txt_net_proceeds'])));
	$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
	$txt_sellers_tax_id = trim(htmlentities($_POST['txt_sellers_tax_id']));
	$txt_purpose_of_transaction = trim(htmlentities($_POST['txt_purpose_of_transaction']));
	$txt_statutory_type = trim(htmlentities($_POST['txt_statutory_type']));
	$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
	$txt_cancellation_reason = trim(htmlentities($_POST['txt_cancellation_reason']));

	$txt_comp_type = trim(htmlentities($_POST['txt_comp_type']));
	$txt_acquisition_date = trim(htmlentities($_POST['txt_acquisition_date']));
	$txt_acquisition_price = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_price'])));
	$txt_acquisition_amount = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_amount'])));
	$txt_capital_gain = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain'])));
	$txt_days_of_holding_interest = trim(htmlentities(str_replace(",","",$_POST['txt_days_of_holding_interest'])));
	$txt_holding_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_holding_interest_amount'])));
	$txt_total_taxable_income = trim(htmlentities(str_replace(",","",$_POST['txt_total_taxable_income'])));
	$txt_tax_rate = trim(htmlentities(str_replace(",","",$_POST['txt_tax_rate'])));
	$txt_tax_amount = trim(htmlentities(str_replace(",","",$_POST['txt_tax_amount'])));	
	$txt_trade = trim(htmlentities($_POST['txt_trade']));
	$txt_sec = trim(htmlentities($_POST['txt_sec']));
	$txt_sec_code = trim(htmlentities($_POST['txt_sec_code']));

	$txt_custody = trim(htmlentities($_POST['txt_custody']));
	$txt_subreg = trim(htmlentities($_POST['txt_subreg']));
	$txt_client_code = trim(htmlentities($_POST['txt_client_code']));
	$txt_pic = trim(htmlentities($_POST['txt_pic']));
	$txt_telp = trim(htmlentities($_POST['txt_telp']));
	$txt_fax = trim(htmlentities($_POST['txt_fax']));

	#$txt_ct_obl = trim(htmlentities($_POST['txt_ct_obl']));

	$row_obl = $data->get_row("SELECT * FROM tbl_kr_mst_obligasi WHERE mst_obl_code = '".$txt_code."' ");
	$obl_name = $row_obl['mst_obl_name'];
	/*
		$row_det_obl = $data->get_rows("SELECT * FROM tbl_kr_me_bonds WHERE code = '".$txt_code."' order by obligasi_id ASC");
		$count_det = count($row_det_obl);
		$det_obl = array();
		for($x=0;$x<$count_det;$x++){
			$det_obl[$x]="
				<input type=checkbox size=1 name=det".$count_det." id=det".$count_det." value=".$row_det_obl['obligasi_id']." OnChange= \"document.form1.submit();\" ".$selected.">".$row_det_obl['code']." - ".$row_det_obl['name_bonds'];
		}
	*/

	$txt_ct_obl = trim(htmlentities($_POST['txt_ct_obl']));
	$arr_obl=array();
	$arr_val=array();
	for($i=1;$i<=$txt_ct_obl;$i++){
		$txt_obl_id ='';
		$txt_val_obl ='';
		$txt_obl_id = trim(htmlentities($_POST['txt_obl_id'.$i]));
		$txt_val_obl = trim(htmlentities($_POST['value'.$i]));
		if($txt_obl_id!=''){
			$arr_obl[$i]=$txt_obl_id;
			$arr_val[$i]=$txt_val_obl;
		}
	}

	if($_POST['txt_buy_sell']=='2'){
		$mst_obl=$data->cb_mst_obl_buy('txt_code',$_POST[txt_code], $_POST[txt_allocation], " OnChange= \"document.form1.submit();\" ");
		$det_obl=$data->cb_det_obl_buy('txt_obl_id', $_POST[txt_code], $_POST[txt_allocation], $arr_obl, $arr_val, " ");
	}else{			
		$mst_obl=$data->cb_mst_obl('txt_code',$_POST[txt_code], " OnChange= \"document.form1.submit();\" ");
	}

	$row_sec = $data->get_row("SELECT * FROM tbl_kr_securitas WHERE securitas_id = '".$txt_sec."' ");
	$sec_name = $row_sec['description'];
	$sec_code = $row_sec['securitas_code'];
	
	if($txt_buy_sell==2){
		$row_allo = $data->get_row("SELECT * FROM tbl_kr_allocation WHERE pk_id = '".$txt_allocation."' ");
		$txt_sellers_tax_id = $row_allo['npwp_no'];
	}
	
	if($txt_buy_sell==1){
		$hid = "style='visibility:hidden;'";
	}else{
		$hid="";
	}
	
	if($txt_comp_type=='1'){
		if($txt_settle!=$txt_prev_coupon){
			$month_intvl = $data->get_row("SELECT MONTH('".$txt_settle."')-MONTH('".$txt_prev_coupon."') AS month_intvl");
			#echo "SELECT MONTH('".$txt_settle."')-MONTH('".$txt_prev_coupon."') AS month_intvl";
			$month_intvl['month_intvl']=$month_intvl['month_intvl']+1;
			$starts = explode('-', $txt_prev_coupon);
			$ends = explode('-', $txt_settle);
			//echo $month_intvl['month_intvl'];
			if($starts[2]==30||$starts[2]==31){
				$month_intvl['month_intvl']=$month_intvl['month_intvl']-1;
				$starts[2]=0;
			}
			if(($starts[1]==2 && $starts[2]==28)||($starts[1]==2 && $starts[2]==29)){
				$month_intvl['month_intvl']=$month_intvl['month_intvl']-1;
				$starts[2]=0;
			}
			if($ends[2]>30){
				$ends[2]==30;
			}
			if(($ends[1]==2 && $ends[2]==28)||($ends[1]==2 && $ends[2]==29)){
				$ends[2]==30;
			}
				//echo $ends[2];

			if($month_intvl['month_intvl']>2){
				$intvl=$month_intvl['month_intvl']-2;
				$day_int=$intvl*30;
				$start=30-$starts[2];
				$acq_days=$day_int+$start+$ends[2];
				//echo $intvl.'-'.$month_intvl['month_intvl'].'<br>';
				//echo $day_int.'-'.$ends[2].'-'.$start;
				//echo "<script>alert('oke')</script>";
			}
			elseif($month_intvl['month_intvl']==2){
				$start=30-$starts[2];
				$acq_days=$start+$ends[2];
				//echo $month_intvl['month_intvl'].'-'.$ends[2].'-'.$starts[2];
			}
			else{
				$acq_days=$starts[2]-$ends[2];
			}
			//$row_acc = $data->get_row("SELECT DATEDIFF('".$txt_settle."','".$txt_prev_coupon."') AS acc_days");
			//$acq_days = $row_acc['acc_days'];
		}else{
			$acq_days=0;
		}
	}
	else{
		$row_acc = $data->get_row("SELECT DATEDIFF('".$txt_settle."','".$txt_prev_coupon."') AS acc_days");
		#echo "SELECT MONTH('".$txt_settle."')-MONTH('".$txt_prev_coupon."') AS month_intvl";
		$acq_days = $row_acc['acc_days'];	
	}

	if($txt_comp_type=='1'){
		if($txt_settle!=$txt_acquisition_date){
			$month_intvl = $data->get_row("SELECT MONTH('".$txt_settle."')-MONTH('".$txt_acquisition_date."') AS month_intvl");
			#echo "SELECT DATEDIFF('".$txt_trade."','".$txt_acquisition_date."') AS acc_days";
			$month_intvl['month_intvl']=$month_intvl['month_intvl']+1;
			$starts = explode('-', $txt_acquisition_date);
			$ends = explode('-', $txt_trade);
			if($starts[2]==30||$starts[2]==31){
				$month_intvl['month_intvl']=-1;
				$starts[2]==0;
			}
			if(($starts[1]==2 && $starts[2]==28)||($starts[1]==2 && $starts[2]==29)){
				$month_intvl['month_intvl']=-1;
				$starts[2]==0;
			}
			if($ends[2]>30){
				$ends[2]==30;
			}
			if(($ends[1]==2 && $ends[2]==28)||($ends[1]==2 && $ends[2]==29)){
				$ends[2]==30;
			}

			if($month_intvl['month_intvl']>2){
				$intvl=$month_intvl['month_intvl']-2;
				$day_int=$intvl*30;
				$start=30-$starts[2];
				$acc_days=$day_int+$start+$ends[2]+1;
				//echo $intvl.'-'.$month_intvl['month_intvl'].'<br>';
				//echo $day_int.'-'.$ends[2].'-'.$start;
				//echo "<script>alert('oke')</script>";
			}
			elseif($month_intvl['month_intvl']==2){
				$start=30-$starts[2];
				$acc_days=$start+$ends[2]+1;
			}
			else{
				$acc_days=$starts[2]-$ends[2]+1;
			}
		}else{
			$acc_days=0;
		}
	}
	else{	
		$row_acq = $data->get_row("SELECT DATEDIFF('".$txt_settle."','".$txt_acquisition_date."') AS acc_days");
		#echo "SELECT DATEDIFF('".$txt_trade."','".$txt_acquisition_date."') AS acc_days";
		$acc_days = $row_acq['acc_days']+1;
	}

	######################## NAB & AKTIVA #####################################################################
	$yesterday=Date('Y-m-d', strtotime("-1 days"));
	#echo $yesterday;
	$row_cash = $data->get_row("select * from tbl_kr_cash where allocation='".$txt_allocation."' order by create_dt DESC limit 1");
	$nab_now = $row_cash[nab_now];
	$nab = $row_cash[nab];
	$kewajiban = $row_cash[total_kewajiban];
	$kas = $row_cash[kas_giro];
	$piutang = $row_cash[total_piutang];
	$aktiva = $row_cash[aktiva_lain];
	$up = $row_cash[jumlah_up];

	$rowo = $data->get_row("select SUM(market_value) as QTY2,
					format(SUM(market_value),0) AS QTY, 
					sum(market_value) as market,
					format(SUM(face_value),0) AS VAL,
					SUM(face_value*(int_rate/100)) AS MARKET_PEROLEHAN
					from tbl_kr_report_bonds 
					where allocation='".$txt_allocation."' order by create_dt DESC limit 1 ");
	$qty = $rowo['VAL'];
	$value = $rowo['QTY'];
	$value1 = $rowo['QTY2'];
	$value1_perolehan = $rowo['MARKET_PEROLEHAN'];
	
	$b = $rowo['market'];

	$rowp = $data->get_row("select format(SUM(price_val),0) as quantity, sum(price_val) as prc_val from tbl_kr_report_pn 
							where allocation='".$txt_allocation."' order by create_dt DESC limit 1 ");
	$quantity = $rowp['quantity'];
	$a = $rowp['prc_val'];

	$rowb = $data->get_row("select format(SUM(face_value),0) as AKTIV, format(SUM(face_value),0) as VAL, sum(face_value) as FACE from tbl_kr_report_deposit where allocation='".$txt_allocation."' order by create_dt DESC limit 1 ");
	$qty3 = $rowb['VAL'];
	$total_aktiv = $rowb['AKTIV'];
	$d = $rowb['FACE'];

	$rows = $data->get_row("select format(SUM(total),0) as total2, format(SUM(lembar),0) as lembar2, sum(total) as TTL from tbl_kr_report_saham where allocation='".$txt_allocation."' order by create_dt DESC limit 1 ");
	$lembar = $rows['lembar2'];
	$total = $rows['total2'];
	$c = $rows['TTL'];
	
	//print ($a);
	// $total_aktiva = $a + $b + $c + $d+ $kas + $piutang + $aktiva;
	$total_aktiva = number_format($a + $b + $c + $d + $kas + $piutang + $aktiva);
	#$total_aktiv1 = number_format(($c/$total_aktiva)*100,2);
	$total_aktiva_bersih = number_format($total_aktiva + $kewajiban);
	$txt_market_value= number_format($txt_face * $txt_rate/100);
	//$txt_total_aktiv = $txt_market_value/$total_aktiva_bersih;
	//$txt_total_aktiv = number_format($txt_total_aktiv);
	$txt_total_aktiv = str_replace(",", "", round($txt_market_value/$total_aktiva_bersih,2));
	// echo $txt_total_aktiv;
	// echo "shared ".$total_aktiva. "<br>";
	// echo "total bersih ".$total_aktiva_bersih. "<br>";
	// echo "total bersih ".$total_market_value. "<br>";
	// echo $txt_total_aktiv. "<br>";


	$tittle = "ADD TRANSACTION";
	$dataRows = array (
	'TEXT' =>  array(
					'Allocation',
					'Buy/Sell',
					'Rating',
					'Seller',
					'Code',
					'Name of Bonds',
					'&nbsp;',
					'Trade Date',
					'Maturity Date',
					'Settlement Date',
					'Last Coupon Date',
					'Next Coupon Date',
					'Acquisition Date',
					'Company Type',
					'Quantity (Rp.)',
					'Harga Perolehan',
					'Market Price (%)',
					' % terhadap total aktiv',
					'Indikator Maks',
					'Coupon Rate',
					'Harga Per (H-1)',
					'Proceeds',
					'Transaction Status',
					'TA Reference ID',
					'TA Reference No',
					'Accrued Days',
					'Accrued Interest Amount',
					'Other Fee',
					'Capital Gain Tax',
					'Interest Income Tax',
					'Withholding Tax',
					'Net Proceeds',
					'Settlement Type',
					'Sellers Tax ID',
					'Purpose of Transaction',
					'Statutory Type',
					'Remarks',
					'Cancellation Reason',
					'&nbsp;',
					'Acquisition Price',
					'Acquisition Amount',
					'Capital Gain',
					'Days of Holding Interest',
					'Holding Interest Amount',
					'Total Taxable Income',
					'Tax Rate in %',
					'Tax Amount',

					'&nbsp;',
					'Custody',
					'Subreg',
					'Client Code',
					'PIC',
					'Telp',
					'Fax'

					),
  	'DOT'  => array (':',':',':',':',':',':',':',':',':'),
	'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," OnChange= \"document.form1.submit();\" "),
		"<select name='txt_buy_sell' id='txt_buy_sell' onchange='calcNetProceeds();document.form1.submit();'>
			<option value='1' ".($_POST['txt_buy_sell'] == '1'? "selected": "" ).">Buy</option>
			<option value='2' ".($_POST['txt_buy_sell'] == '2'? "selected": "" ).">Sell</option>
		</select>",
		"<input type=text size='10'  name=txt_rating value='".$_POST[txt_rating]."'>",
		"".$data->cb_securitas_participant('txt_sec',$_POST[txt_sec]," OnChange= \"document.form1.submit();\" ").",&nbsp;&nbsp;
		<input type=text size='30'  name=txt_seller id='txt_seller' value='".$sec_name."' readonly>
		<input type=hidden name=txt_sec_code id='txt_sec_code' value='".$sec_code."'>
		",
		$mst_obl,
		"<input type=text size='75'  name=txt_name value='".$obl_name."' readonly>",
		$det_obl[0]."<input type=text size='10' name=txt_ct_obl value='".$det_obl[1]."' readonly>",
		/*"<input type=text size='30' name=txt_seller value='".$_POST[txt_seller]."'>",*/
		$data->datePicker('txt_trade',$_POST[txt_trade]),
		$data->datePicker('txt_maturity',$_POST[txt_maturity]),
		$data->datePicker('txt_settle',$_POST[txt_settle]," onclick='calcAccrued();'"),
		$data->datePicker('txt_prev_coupon',$_POST[txt_prev_coupon]," onclick='calcAccrued();'"),
		$data->datePicker('txt_next_coupon',$_POST[txt_next_coupon]),
		$data->datePicker('txt_acquisition_date', $_POST['txt_acquisition_date']," onclick='calcAccrued();'"),
		
		"<select name='txt_comp_type' id='txt_comp_type' OnChange='calcNetProceeds();document.form1.submit();'>
			<option value='1' ".($_POST['txt_comp_type'] == '1'||$_POST['txt_comp_type'] == ''? "selected": "" ).">Corporate</option>
			<option value='2' ".($_POST['txt_comp_type'] == '2'? "selected": "" ).">Non-Corporate</option>
		</select>",
		"<input type=text size='30' name=txt_face id='txt_face' value='".$_POST[txt_face]."' onchange='calcNetProceeds();autofill();document.form1.submit();'>",
		"<input type=text size='30' name=txt_market_price id='txt_market_price' value='".$_POST[txt_market_price]."' onchange='calcNetProceeds();document.form1.submit();'>",
		"<input type=text size='30'  name=txt_rate id='txt_rate' value='".$_POST[txt_rate]."' onchange='calcNetProceeds();'>",
		"<input type=text size='30'  name=txt_total_aktiv value='".$txt_total_aktiv."'>",
		"<input type=text size='30'  name=txt_indikator_maks id='txt_indikator_maks' value='".$_POST[txt_indikator_maks]."'>",
		"<input type=text size='30'  name=txt_coupon_rate id='txt_coupon_rate' value='".$_POST[txt_coupon_rate]."'>",
		"<input type=text size='30'  name=txt_harga_minus value='".$_POST[txt_harga_minus]."'>",
		"<input type=text size='20' name=txt_market_value id='txt_market_value' value='".$_POST[txt_market_value]."' onchange='calcNetProceeds();' readonly>",
		"<select name='txt_transaction_status' id='txt_transaction_status' onchange='changeStatus();'><option value='NEWM' ".($_POST['txt_transaction_status'] == 'NEWM'? "selected": "" ).">New Trade</option><option value='CANC' ".($_POST['txt_transaction_status'] == 'CANC'? "selected": "" ).">Cancel Trade</option></select>",
		"<input type=text size='30' name=txt_ta_reference_id id='txt_ta_reference_id' readonly='readonly' value='".$_POST[txt_ta_reference_id]."' placeholder='will be assign by S-INVEST'>",
		"<input type=text size='30' name=txt_ta_reference_no value='".$_POST[txt_ta_reference_no]."' required>",
		"<input type=text size='10' name=txt_accrued_days id='txt_accrued_days' value='".$_POST[txt_accrued_days]."' required>",
		"<input type=text size='20' name=txt_accrued_interest_amount id='txt_accrued_interest_amount' value='".$_POST[txt_accrued_interest_amount]."' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_other_fee id='txt_other_fee' value='".$_POST[txt_other_fee]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_capital_gain_tax id='txt_capital_gain_tax' value='".$_POST[txt_capital_gain_tax]."' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_interest_income_tax id='txt_interest_income_tax' value='".$_POST[txt_interest_income_tax]."' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_withholding_tax id='txt_withholding_tax' value='".$_POST[txt_withholding_tax]."' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_net_proceeds id='txt_net_proceeds' value='".$_POST[txt_net_proceeds]."' readonly>",
		"<select name='txt_settlement_type' ><option value='1' ".($_POST['txt_settlement_type'] == '1'? "selected": "" ).">DVPBond</option><option value='2' ".($_POST['txt_settlement_type'] == '2'? "selected": "" ).">RVPBond</option><option value='3' ".($_POST['txt_settlement_type'] == '3'? "selected": "" ).">DFOPBond</option><option value='4' ".($_POST['txt_settlement_type'] == '4'? "selected": "" ).">RFOPBond</option></select>",
		"<input type=text size='20' name=txt_sellers_tax_id value='".$txt_sellers_tax_id."'>",
		"<select name='txt_purpose_of_transaction' ><option value='1' ".($_POST['txt_purpose_of_transaction'] == '1'? "selected": "" ).">HTM</option><option value='2' ".($_POST['txt_purpose_of_transaction'] == '2'? "selected": "" ).">AFS</option><option value='3' ".($_POST['txt_purpose_of_transaction'] == '3'? "selected": "" ).">FVTPL</option><option value='4' ".($_POST['txt_purpose_of_transaction'] == '4'? "selected": "" ).">LR</option></select>",
		"<select name='txt_statutory_type' ><option value='1' ".($_POST['txt_statutory_type'] == '1'? "selected": "" ).">Yes</option><option value='2' ".($_POST['txt_statutory_type'] == '2' || $_POST['txt_statutory_type'] == ''? "selected": "" ).">No</option></select>",
		"<input type=text size='30' name=txt_remarks value='".$_POST[txt_remarks]."'>",
		"<input type=text size='30' name=txt_cancellation_reason id='txt_cancellation_reason' readonly='readonly' value='".$_POST[txt_cancellation_reason]."'>",
		"<input type=button name=btn_save value='reset tax column' onclick='buysell();' ".$hid.">",
		"<input type=text size='20' name=txt_acquisition_price id='txt_acquisition_price' value='".$_POST[txt_acquisition_price]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();document.form1.submit();' ".$hid.">",
		"<input type=text size='20' name=txt_acquisition_amount id='txt_acquisition_amount' value='".$_POST[txt_acquisition_amount]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' readonly ".$hid.">",
		"<input type=text size='20' name=txt_capital_gain id='txt_capital_gain' value='".$_POST[txt_capital_gain]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();' ".$hid.">",
		"<input type=text size='20' name=txt_days_of_holding_interest id='txt_days_of_holding_interest' value='".$acq_days."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' ".$hid.">",
		"<input type=text size='20' name=txt_holding_interest_amount id='txt_holding_interest_amount' value='".$_POST[txt_holding_interest_amount]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();' ".$hid.">",
		"<input type=text size='20' name=txt_total_taxable_income id='txt_total_taxable_income' value='".$_POST[txt_total_taxable_income]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' readonly ".$hid.">",
		"<input type=text size='20' name=txt_tax_rate id='txt_tax_rate' value='".$_POST[txt_tax_rate]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' ".$hid." >",
		"<input type=text size='20' name=txt_tax_amount id='txt_tax_amount' value='".$_POST[txt_tax_amount]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' ".$hid." >",
		"",
		"<input type=text size='30' name=txt_custody value='".$_POST[txt_custody]."' >",
		"<input type=text size='30' name=txt_subreg value='".$_POST[txt_subreg]."' >",
		"<input type=text size='30' name=txt_client_code value='".$_POST[txt_client_code]."' >",
		"<input type=text size='30' name=txt_pic value='".$_POST[txt_pic]."' >",
		"<input type=text size='30' name=txt_telp value='".$_POST[txt_telp]."' >",
		"<input type=text size='30' name=txt_fax value='".$_POST[txt_fax]."' >"
		)
	);
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='obligasi.php';\">");
}


if ($_POST['btn_save_edit']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$rows 		= $data->get_row("select tbl_kr_me_bonds.*, format(market_value,0) as VAL from tbl_kr_me_bonds where obligasi_id = '".$_GET[id]."'");
		$A			=	$rows['allocation'];
		$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
		$allocation2 = $rowo['A'];

		$txt_code = trim(htmlentities($_POST['txt_code']));
		$txt_name = trim(htmlentities($_POST['txt_name']));
		$txt_rating = trim(htmlentities($_POST['txt_rating']));
		$txt_face = trim(htmlentities(str_replace(",","",$_POST['txt_face'])));
		$txt_rate = trim(htmlentities($_POST['txt_rate']));
		$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
		$txt_indikator_maks = trim(htmlentities($_POST['txt_indikator_maks']));
		$txt_coupon_rate = trim(htmlentities($_POST['txt_coupon_rate']));
		$txt_harga_minus = trim(htmlentities($_POST['txt_harga_minus']));
		$txt_market_price = trim(htmlentities($_POST['txt_market_price']));
		$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
		$txt_prev_coupon = trim(htmlentities($_POST['txt_prev_coupon']));
		$txt_next_coupon = trim(htmlentities($_POST['txt_next_coupon']));
		$txt_seller = trim(htmlentities($_POST['txt_seller']));
		$txt_settle = trim(htmlentities($_POST['txt_settle']));
		
		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
		$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
		$txt_buy_sell = trim(htmlentities($_POST['txt_buy_sell']));
		$txt_accrued_days = trim(htmlentities($_POST['txt_accrued_days']));
		$txt_accrued_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_accrued_interest_amount'])));
		$txt_other_fee = trim(htmlentities(str_replace(",","",$_POST['txt_other_fee'])));
		$txt_capital_gain_tax = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain_tax'])));
		$txt_interest_income_tax = trim(htmlentities(str_replace(",","",$_POST['txt_interest_income_tax'])));
		$txt_withholding_tax = trim(htmlentities(str_replace(",","",$_POST['txt_withholding_tax'])));
		$txt_net_proceeds = trim(htmlentities(str_replace(",","",$_POST['txt_net_proceeds'])));
		$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
		$txt_sellers_tax_id = trim(htmlentities($_POST['txt_sellers_tax_id']));
		$txt_purpose_of_transaction = trim(htmlentities($_POST['txt_purpose_of_transaction']));
		$txt_statutory_type = trim(htmlentities($_POST['txt_statutory_type']));
		$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
		$txt_cancellation_reason = trim(htmlentities($_POST['txt_cancellation_reason']));

		$txt_comp_type = trim(htmlentities($_POST['txt_comp_type']));
		$txt_acquisition_date = trim(htmlentities($_POST['txt_acquisition_date']));
		$txt_acquisition_price = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_price'])));
		$txt_acquisition_amount = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_amount'])));
		$txt_capital_gain = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain'])));
		$txt_days_of_holding_interest = trim(htmlentities(str_replace(",","",$_POST['txt_days_of_holding_interest'])));
		$txt_holding_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_holding_interest_amount'])));
		$txt_total_taxable_income = trim(htmlentities(str_replace(",","",$_POST['txt_total_taxable_income'])));
		$txt_tax_rate = trim(htmlentities(str_replace(",","",$_POST['txt_tax_rate'])));
		$txt_tax_amount = trim(htmlentities(str_replace(",","",$_POST['txt_tax_amount'])));	
		$txt_trade = trim(htmlentities($_POST['txt_trade']));
		$txt_sec = trim(htmlentities($_POST['txt_sec']));
		$txt_sec_code = trim(htmlentities($_POST['txt_sec_code']));

		$txt_custody = trim(htmlentities($_POST['txt_custody']));
		$txt_subreg = trim(htmlentities($_POST['txt_subreg']));
		$txt_client_code = trim(htmlentities($_POST['txt_client_code']));
		$txt_pic = trim(htmlentities($_POST['txt_pic']));
		$txt_telp = trim(htmlentities($_POST['txt_telp']));
		$txt_fax = trim(htmlentities($_POST['txt_fax']));

		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_name ==''){
			echo "<script>alert('Bonds name is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
        if($txt_face==''){
			echo "<script>alert('Quantity is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_rate ==''){
			echo "<script>alert('Harga Perolehan is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_aktiv ==''){
			echo "<script>alert('% Terhadap Total Aktiv is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		/*
		if($txt_indikator_maks ==''){
			echo "<script>alert('Indikator maks is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
		if($txt_harga_minus ==''){
			echo "<script>alert('Harga Per H-1 is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		*/
		if($txt_prev_coupon ==''){
			echo "<script>alert('Prev coupon is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	
			if($txt_next_coupon ==''){
			echo "<script>alert('Next coupon is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
	
		if($txt_seller ==''){
			echo "<script>alert('Seller  is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
		/*if($txt_market_price==''){
			echo "<script>alert('Market price is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}*/
		$a = $txt_market_price/100;
		//$txt_market_value = $txt_face * $a ;
		$txt_market_value = $txt_face * $txt_rate/100;
		//print_r($txt_market_value);
		#$txt_market_value = $txt_face * ($txt_market_price/100);

		$sql = "UPDATE tbl_kr_me_bonds SET
			code = '".$txt_code."',
			name_bonds = '".$txt_name."',
			rating = '".$txt_rating."',
			maturity='".$txt_maturity."',
			last_coupon ='".$txt_prev_coupon."',
			next_coupon ='".$txt_next_coupon."',
			seller ='".$txt_seller."',
			settlement_date ='".$txt_settle."',
			trade_date ='".$txt_trade."',
			face_value = '".$txt_face."',
			int_rate = '".$txt_rate."',
			total_aktiv ='".$txt_total_aktiv."',
			indikator_maks ='".$txt_indikator_maks."',
			coupon_rate='".$txt_coupon_rate."',
			harga_minus_one ='".$txt_harga_minus."',
			market_price = '".$txt_market_price."',
			market_value = '".$txt_market_value."',
			allocation ='".$A."',
			transaction_status = '".$txt_transaction_status."',
			ta_reference_id = '".$txt_ta_reference_id."',
			ta_reference_no = '".$txt_ta_reference_no."',
			buy_sell = '".$txt_buy_sell."',
			accrued_days = '".$txt_accrued_days."',
			accrued_interest_amount = '".$txt_accrued_interest_amount."',
			other_fee = '".$txt_other_fee."',
			capital_gain_tax = '".$txt_capital_gain_tax."',
			interest_income_tax = '".$txt_interest_income_tax."',
			withholding_tax = '".$txt_withholding_tax."',
			net_proceeds = '".$txt_net_proceeds."',
			settlement_type = '".$txt_settlement_type."',
			sellers_tax_id = '".$txt_sellers_tax_id."',
			purpose_of_transaction = '".$txt_purpose_of_transaction."',
			statutory_type = '".$txt_statutory_type."',
			remarks = '".$txt_remarks."',
			cancellation_reason = '".$txt_cancellation_reason."',
			acquisition_date = '".$txt_acquisition_date."',
			acquisition_price = '".$txt_acquisition_price."',
			acquisition_amount = '".$txt_acquisition_amount."',
			capital_gain = '".$txt_capital_gain."',
			days_of_holding_interest = '".$txt_days_of_holding_interest."',
			holding_interest_amount = '".$txt_holding_interest_amount."',
			total_taxable_income = '".$txt_total_taxable_income."',
			tax_rate = '".$txt_tax_rate."',
			tax_amount = '".$txt_tax_amount."',
			br_code = '".$txt_sec_code."',
			securitas_id = '".$txt_sec."',
			company_type = '".$txt_comp_type."'
			WHERE obligasi_id = '".$_GET[id]."'";
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}

		$sql1 = "UPDATE tbl_kr_me_bonds_hist SET
			code = '".$txt_code."',
			name_bonds = '".$txt_name."',
			rating = '".$txt_rating."',
			maturity='".$txt_maturity."',
			last_coupon ='".$txt_prev_coupon."',
			next_coupon ='".$txt_next_coupon."',
			seller ='".$txt_seller."',
			settlement_date ='".$txt_settle."',
			trade_date ='".$txt_trade."',
			face_value = '".$txt_face."',
			int_rate = '".$txt_rate."',
			total_aktiv ='".$txt_total_aktiv."',
			indikator_maks ='".$txt_indikator_maks."',
			coupon_rate ='".$txt_coupon_rate."',
			harga_minus_one ='".$txt_harga_minus."',
			market_price = '".$txt_market_price."',
			market_value = '".$txt_market_value."',
			allocation ='".$A."',
			transaction_status = '".$txt_transaction_status."',
			ta_reference_id = '".$txt_ta_reference_id."',
			ta_reference_no = '".$txt_ta_reference_no."',
			buy_sell = '".$txt_buy_sell."',
			accrued_days = '".$txt_accrued_days."',
			accrued_interest_amount = '".$txt_accrued_interest_amount."',
			other_fee = '".$txt_other_fee."',
			capital_gain_tax = '".$txt_capital_gain_tax."',
			interest_income_tax = '".$txt_interest_income_tax."',
			withholding_tax = '".$txt_withholding_tax."',
			net_proceeds = '".$txt_net_proceeds."',
			settlement_type = '".$txt_settlement_type."',
			sellers_tax_id = '".$txt_sellers_tax_id."',
			purpose_of_transaction = '".$txt_purpose_of_transaction."',
			statutory_type = '".$txt_statutory_type."',
			remarks = '".$txt_remarks."',
			cancellation_reason = '".$txt_cancellation_reason."',
			acquisition_date = '".$txt_acquisition_date."',
			acquisition_price = '".$txt_acquisition_price."',
			acquisition_amount = '".$txt_acquisition_amount."',
			capital_gain = '".$txt_capital_gain."',
			days_of_holding_interest = '".$txt_days_of_holding_interest."',
			holding_interest_amount = '".$txt_holding_interest_amount."',
			total_taxable_income = '".$txt_total_taxable_income."',
			tax_rate = '".$txt_tax_rate."',
			tax_amount = '".$txt_tax_amount."',

			br_code = '".$txt_sec_code."',
			securitas_id = '".$txt_sec."',
			custody = '".$txt_custody."',
			subreg = '".$txt_subreg."',
			client_code = '".$txt_client_code."',
			pic_name = '".$txt_pic."',
			pic_telp = '".$txt_telp."',
			pic_fax = '".$txt_fax."',
			company_type = '".$txt_comp_type."'
			WHERE fk_obligasi = '".$_GET[id]."'";
		if (!$data->inpQueryReturnBool($sql1)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='obligasi.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['edit']==1){
	$rows 		= $data->get_row("select tbl_kr_me_bonds.*, format(market_value,0) as VAL from tbl_kr_me_bonds where obligasi_id = '".$_GET[id]."'");
	//$rows 		= $data->get_row("select tbl_kr_me_bonds_hist.*, format(market_value,0) as VAL from tbl_kr_me_bonds_hist where fk_obligasi = '".$_GET[id]."'");
	$allo = $rows['allocation'];

	//$txt_allocation = trim(htmlentities($_POST['txt_allocation']));
	$txt_code = trim(htmlentities($_POST['txt_code']));
	$txt_name = trim(htmlentities($_POST['txt_name']));
	$txt_rating = trim(htmlentities($_POST['txt_rating']));
	$txt_face = trim(htmlentities(str_replace(",","",$_POST['txt_face'])));
	$txt_rate = trim(htmlentities($_POST['txt_rate']));
	$txt_total_aktiv = trim(htmlentities($_POST['txt_total_aktiv']));
	$txt_indikator_maks = trim(htmlentities($_POST['txt_indikator_maks']));
	$txt_coupon_rate = trim(htmlentities($_POST['txt_coupon_rate']));
	$txt_harga_minus = trim(htmlentities($_POST['txt_harga_minus']));
	$txt_market_price = trim(htmlentities($_POST['txt_market_price']));
	$txt_maturity = trim(htmlentities($_POST['txt_maturity']));
	$txt_prev_coupon = trim(htmlentities($_POST['txt_prev_coupon']));
	$txt_next_coupon = trim(htmlentities($_POST['txt_next_coupon']));
	$txt_seller = trim(htmlentities($_POST['txt_seller']));
	$txt_settle = trim(htmlentities($_POST['txt_settle']));
	
	$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
	$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
	$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
	$txt_buy_sell = trim(htmlentities($_POST['txt_buy_sell']));
	$txt_accrued_days = trim(htmlentities($_POST['txt_accrued_days']));
	$txt_accrued_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_accrued_interest_amount'])));
	$txt_other_fee = trim(htmlentities(str_replace(",","",$_POST['txt_other_fee'])));
	$txt_capital_gain_tax = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain_tax'])));
	$txt_interest_income_tax = trim(htmlentities(str_replace(",","",$_POST['txt_interest_income_tax'])));
	$txt_withholding_tax = trim(htmlentities(str_replace(",","",$_POST['txt_withholding_tax'])));
	$txt_net_proceeds = trim(htmlentities(str_replace(",","",$_POST['txt_net_proceeds'])));
	$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
	$txt_sellers_tax_id = trim(htmlentities($_POST['txt_sellers_tax_id']));
	$txt_purpose_of_transaction = trim(htmlentities($_POST['txt_purpose_of_transaction']));
	$txt_statutory_type = trim(htmlentities($_POST['txt_statutory_type']));
	$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
	$txt_cancellation_reason = trim(htmlentities($_POST['txt_cancellation_reason']));

	$txt_comp_type = trim(htmlentities($_POST['txt_comp_type']));
	$txt_acquisition_date = trim(htmlentities($_POST['txt_acquisition_date']));
	$txt_acquisition_price = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_price'])));
	$txt_acquisition_amount = trim(htmlentities(str_replace(",","",$_POST['txt_acquisition_amount'])));
	$txt_capital_gain = trim(htmlentities(str_replace(",","",$_POST['txt_capital_gain'])));
	$txt_days_of_holding_interest = trim(htmlentities(str_replace(",","",$_POST['txt_days_of_holding_interest'])));
	$txt_holding_interest_amount = trim(htmlentities(str_replace(",","",$_POST['txt_holding_interest_amount'])));
	$txt_total_taxable_income = trim(htmlentities(str_replace(",","",$_POST['txt_total_taxable_income'])));
	$txt_tax_rate = trim(htmlentities(str_replace(",","",$_POST['txt_tax_rate'])));
	$txt_tax_amount = trim(htmlentities(str_replace(",","",$_POST['txt_tax_amount'])));	
	$txt_trade = trim(htmlentities($_POST['txt_trade']));
	$txt_sec = trim(htmlentities($_POST['txt_sec']));
	$txt_sec_code = trim(htmlentities($_POST['txt_sec_code']));

	$txt_custody = trim(htmlentities($_POST['txt_custody']));
	$txt_subreg = trim(htmlentities($_POST['txt_subreg']));
	$txt_client_code = trim(htmlentities($_POST['txt_client_code']));
	$txt_pic = trim(htmlentities($_POST['txt_pic']));
	$txt_telp = trim(htmlentities($_POST['txt_telp']));
	$txt_fax = trim(htmlentities($_POST['txt_fax']));

	$row_obl = $data->get_row("SELECT * FROM tbl_kr_mst_obligasi WHERE mst_obl_code = '".$rows['code']."' ");
	$obl_name = $row_obl['mst_obl_name'];

	$row_sec = $data->get_row("SELECT * FROM tbl_kr_securitas WHERE securitas_id = '".$rows['securitas_id']."' ");
	$sec_name = $row_sec['description'];
	$sec_code = $row_sec['securitas_code'];
	$sec_id = $row_sec['securitas_id'];
	
	$row_allo = $data->get_row("SELECT * FROM tbl_kr_allocation WHERE pk_id = '".$allo."' ");
	$txt_sellers_tax_id = $row_allo['npwp_no'];
	$allocation2 = $row_allo['allocation'];
	
	if($txt_buy_sell==1 /*|| $rows['buy_sell']==1*/){
		$hid = "style='visibility:hidden;'";
	}else{
		$hid="";
	}
	
	if($txt_comp_type=='1'){
		if($txt_settle!=$txt_prev_coupon){
			$month_intvl = $data->get_row("SELECT MONTH('".$txt_settle."')-MONTH('".$txt_prev_coupon."') AS month_intvl");
			#echo "SELECT MONTH('".$txt_settle."')-MONTH('".$txt_prev_coupon."') AS month_intvl";
			$month_intvl['month_intvl']=$month_intvl['month_intvl']+1;
			$starts = explode('-', $txt_prev_coupon);
			$ends = explode('-', $txt_settle);
			//echo $month_intvl['month_intvl'];
			if($starts[2]==30||$starts[2]==31){
				$month_intvl['month_intvl']=$month_intvl['month_intvl']-1;
				$starts[2]=0;
			}
			if(($starts[1]==2 && $starts[2]==28)||($starts[1]==2 && $starts[2]==29)){
				$month_intvl['month_intvl']=$month_intvl['month_intvl']-1;
				$starts[2]=0;
			}
			if($ends[2]>30){
				$ends[2]==30;
			}
			if(($ends[1]==2 && $ends[2]==28)||($ends[1]==2 && $ends[2]==29)){
				$ends[2]==30;
			}
				//echo $ends[2];

			if($month_intvl['month_intvl']>2){
				$intvl=$month_intvl['month_intvl']-2;
				$day_int=$intvl*30;
				$start=30-$starts[2];
				$acq_days=$day_int+$start+$ends[2];
				//echo $intvl.'-'.$month_intvl['month_intvl'].'<br>';
				//echo $day_int.'-'.$ends[2].'-'.$start;
				//echo "<script>alert('oke')</script>";
			}
			elseif($month_intvl['month_intvl']==2){
				$start=30-$starts[2];
				$acq_days=$start+$ends[2];
				//echo $month_intvl['month_intvl'].'-'.$ends[2].'-'.$starts[2];
			}
			else{
				$acq_days=$starts[2]-$ends[2];
			}
			//$row_acc = $data->get_row("SELECT DATEDIFF('".$txt_settle."','".$txt_prev_coupon."') AS acc_days");
			//$acq_days = $row_acc['acc_days'];
		}else{
			$acq_days=0;
		}
	}
	else{
		$row_acc = $data->get_row("SELECT DATEDIFF('".$txt_settle."','".$txt_prev_coupon."') AS acc_days");
		#echo "SELECT MONTH('".$txt_settle."')-MONTH('".$txt_prev_coupon."') AS month_intvl";
		$acq_days = $row_acc['acc_days'];	
	}

	if($txt_comp_type=='1'){
		if($txt_settle!=$txt_acquisition_date){
			$month_intvl = $data->get_row("SELECT MONTH('".$txt_settle."')-MONTH('".$txt_acquisition_date."') AS month_intvl");
			#echo "SELECT DATEDIFF('".$txt_trade."','".$txt_acquisition_date."') AS acc_days";
			$month_intvl['month_intvl']=$month_intvl['month_intvl']+1;
			$starts = explode('-', $txt_acquisition_date);
			$ends = explode('-', $txt_trade);
			if($starts[2]==30||$starts[2]==31){
				$month_intvl['month_intvl']=-1;
				$starts[2]==0;
			}
			if(($starts[1]==2 && $starts[2]==28)||($starts[1]==2 && $starts[2]==29)){
				$month_intvl['month_intvl']=-1;
				$starts[2]==0;
			}
			if($ends[2]>30){
				$ends[2]==30;
			}
			if(($ends[1]==2 && $ends[2]==28)||($ends[1]==2 && $ends[2]==29)){
				$ends[2]==30;
			}

			if($month_intvl['month_intvl']>2){
				$intvl=$month_intvl['month_intvl']-2;
				$day_int=$intvl*30;
				$start=30-$starts[2];
				$acc_days=$day_int+$start+$ends[2]+1;
				//echo $intvl.'-'.$month_intvl['month_intvl'].'<br>';
				//echo $day_int.'-'.$ends[2].'-'.$start;
				//echo "<script>alert('oke')</script>";
			}
			elseif($month_intvl['month_intvl']==2){
				$start=30-$starts[2];
				$acc_days=$start+$ends[2]+1;
			}
			else{
				$acc_days=$starts[2]-$ends[2]+1;
			}
		}else{
			$acc_days=0;
		}
	}
	else{	
		$row_acq = $data->get_row("SELECT DATEDIFF('".$txt_settle."','".$txt_acquisition_date."') AS acc_days");
		#echo "SELECT DATEDIFF('".$txt_trade."','".$txt_acquisition_date."') AS acc_days";
		$acc_days = $row_acq['acc_days']+1;
	}

	######################## NAB & AKTIVA #####################################################################
	$yesterday=Date('Y-m-d', strtotime("-1 days"));
	#echo $yesterday;
	$row_cash = $data->get_row("select * from tbl_kr_cash where allocation='".$allo."' order by create_dt DESC limit 1");
	$nab_now = $row_cash[nab_now]/10000;
	$nab = $row_cash[nab]/10000;
	$kewajiban = $row_cash[total_kewajiban];
	$kas = $row_cash[kas_giro];
	$piutang = $row_cash[total_piutang];
	$aktiva = $row_cash[aktiva_lain];
	$up = $row_cash[jumlah_up];

	$rowo = $data->get_row("select SUM(market_value) as QTY2,
					format(SUM(market_value),0) AS QTY, 
					sum(market_value) as market,
					format(SUM(face_value),0) AS VAL,
					SUM(face_value*(int_rate/100)) AS MARKET_PEROLEHAN
					from tbl_kr_report_bonds 
					where allocation='".$allo."' order by create_dt DESC limit 1 ");
	$qty = $rowo['VAL'];
	$value = $rowo['QTY'];
	$value1 = $rowo['QTY2'];
	$value1_perolehan = $rowo['MARKET_PEROLEHAN'];
	
	$b = $rowo['market'];

	$rowp = $data->get_row("select format(SUM(price_val),0) as quantity, sum(price_val) as prc_val from tbl_kr_report_pn 
							where allocation='".$allo."' order by create_dt DESC limit 1 ");
	$quantity = $rowp['quantity'];
	$a = $rowp['prc_val'];

	$rowb = $data->get_row("select format(SUM(face_value),0) as AKTIV, format(SUM(face_value),0) as VAL, sum(face_value) as FACE from tbl_kr_report_deposit where allocation='".$allo."' order by create_dt DESC limit 1 ");
	$qty3 = $rowb['VAL'];
	$total_aktiv = $rowb['AKTIV'];
	$d = $rowb['FACE'];

	$rowx = $data->get_row("select format(SUM(total),0) as total2, format(SUM(lembar),0) as lembar2, sum(total) as TTL from tbl_kr_report_saham where allocation='".$allo."' order by create_dt DESC limit 1 ");
	$lembar = $rowx['lembar2'];
	$total = $rowx['total2'];
	$c = $rowx['TTL'];
	
	//print ($a);
	$total_aktiva = $a + $b + $c + $d+ $kas + $piutang + $aktiva;
	#$total_aktiv1 = number_format(($c/$total_aktiva)*100,2);
	$total_aktiva_bersih = $total_aktiva - $kewajiban;
	$txt_total_aktiv = $txt_market_value/$total_aktiva_bersih;
	$txt_market_value= number_format($txt_face * $txt_rate/100);
	#$txt_total_aktiv = number_format($txt_total_aktiv);
	$txt_total_aktiv = str_replace(",", "", round($txt_market_value/$total_aktiva_bersih,2));
	


	$tittle = "EDIT TRANSACTION &nbsp;".$allocation2."";
	$dataRows = array (
	'TEXT' =>  array(
					'Allocation',
					'Code',
					'Name of Bonds',
					'Rating',
					'Seller',
					'Trade Date',
					'Maturity Date',
					'Settlement Date',
					'Last Coupon Date',
					'Next Coupon Date',
					'Acquisition Date',
					'Company Type',
					'Quantity (Rp.)',
					'Harga Perolehan',
					'Market Price (%)',
					' % terhadap total aktiv',
					'Indikator Maks',
					'Coupon Rate',
					'Harga Per (H-1)',
					'Proceeds',
					'Transaction Status',
					'TA Reference ID',
					'TA Reference No',
					'Buy/Sell',
					'Accrued Days',
					'Accrued Interest Amount',
					'Other Fee',
					'Capital Gain Tax',
					'Interest Income Tax',
					'Withholding Tax',
					'Net Proceeds',
					'Settlement Type',
					'Sellers Tax ID',
					'Purpose of Transaction',
					'Statutory Type',
					'Remarks',
					'Cancellation Reason',
					'&nbsp;',
					'Acquisition Price',
					'Acquisition Amount',
					'Capital Gain',
					'Days of Holding Interest',
					'Holding Interest Amount',
					'Total Taxable Income',
					'Tax Rate in %',
					'Tax Amount',

					'&nbsp;',
					'Custody',
					'Subreg',
					'Client Code',
					'PIC',
					'Telp',
					'Fax'

					),
  	'DOT'  => array (':',':',':',':',':',':',':',':',':'),
	'FIELD' => array (
		"&nbsp;&nbsp;".$allocation2."",
		$data->cb_mst_obl('txt_code',$rows[code]," OnChange= \"document.form1.submit();\" "),
		"<input type=text size='75'  name=txt_name value='".$obl_name."' readonly>",
		"<input type=text size='10'  name=txt_rating value='".$rows[rating]."'>",
		"".$data->cb_securitas_participant('txt_sec',$sec_id," OnChange= \"document.form1.submit();\" ").",&nbsp;&nbsp;
		<input type=text size='30'  name=txt_seller id='txt_seller' value='".$sec_name."' readonly>
		<input type=text name=txt_sec_code id='txt_sec_code' value='".$sec_code."'>
		",
		$data->datePicker('txt_trade',$rows[trade_date]),
		$data->datePicker('txt_maturity',$rows[maturity]),
		$data->datePicker('txt_settle',$rows[settlement_date]," onclick='calcAccrued();'"),
		$data->datePicker('txt_prev_coupon',$rows[last_coupon]," onclick='calcAccrued();'"),
		$data->datePicker('txt_next_coupon',$rows[next_coupon]),
		$data->datePicker('txt_acquisition_date', $rows['acquisition_date']," onclick='calcAccrued();'"),
		
		"<select name='txt_comp_type' id='txt_comp_type' OnChange='calcNetProceeds();buysell();document.form1.submit();'>
			<option value='1' ".($rows['company_type'] == '1'? "selected": "" ).">Corporate</option>
			<option value='2' ".($rows['company_type'] == '2'? "selected": "" ).">Non-Corporate</option>
		</select>",
		"<input type=text size='30' name=txt_face id='txt_face' value='".$rows[face_value]."' onchange='calcNetProceeds();autofill();document.form1.submit();'>",
		
		/*"<input type=text size='30' name=txt_face id='txt_face' value='".number_format($rows[face_value])."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();autofill();'>",*/
		"<input type=text size='30' name=txt_market_price id='txt_market_price' value='".$rows[market_price]."' onchange='calcNetProceeds();'>",
		"<input type=text size='30'  name=txt_rate id='txt_rate' value='".$rows[int_rate]."' onchange='calcNetProceeds();'>",
		"<input type=text size='30'  name=txt_total_aktiv value='".$rows[total_aktiv]."'>",
		"<input type=text size='30'  name=txt_indikator_maks id='txt_indikator_maks' value='".$rows[indikator_maks]."'>",
		"<input type=text size='30'  name=txt_coupon_rate id='txt_coupon_rate' value='".$rows[coupon_rate]."'>",
		"<input type=text size='30'  name=txt_harga_minus value='".$rows[harga_minus_one]."'>",
		"<input type=text size='30' name=txt_market_value id='txt_market_value' value='".number_format($rows[market_value])."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();' readonly>",
		"<select name='txt_transaction_status' id='txt_transaction_status' onchange='changeStatus();'>
			<option value='NEWM' ".($rows['transaction_status'] == 'NEWM'? "selected": "" ).">New Trade</option>
			<option value='CANC' ".($rows['transaction_status'] == 'CANC'? "selected": "" ).">Cancel Trade</option>
		</select>",
		"<input type=text size='30' name=txt_ta_reference_id id='txt_ta_reference_id' readonly='readonly' value='".$rows[ta_reference_id]."' placeholder='will be assign by S-INVEST'>",
		"<input type=text size='30' name=txt_ta_reference_no value='".$rows[ta_reference_no]."' required>",
		"<select name='txt_buy_sell' id='txt_buy_sell' onchange='calcNetProceeds();'>
			<option value='1' ".($rows['buy_sell'] == '1'? "selected": "" ).">Buy</option>
			<option value='2' ".($rows['buy_sell'] == '2'? "selected": "" ).">Sell</option>
		</select>",
		"<input type=text size='10' name=txt_accrued_days value='".$rows[accrued_days]."' required>",
		"<input type=text size='20' name=txt_accrued_interest_amount id='txt_accrued_interest_amount' value='".$rows[accrued_interest_amount]."' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_other_fee id='txt_other_fee' value='".$rows[other_fee]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_capital_gain_tax id='txt_capital_gain_tax' value='".$rows[capital_gain_tax]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_interest_income_tax id='txt_interest_income_tax' value='".$rows[interest_income_tax]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_withholding_tax id='txt_withholding_tax' value='".$rows[withholding_tax]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_net_proceeds id='txt_net_proceeds' value='".$rows[net_proceeds]."' readonly>",
		"<select name='txt_settlement_type' >
			<option value='1' ".($rows['settlement_type'] == '1'? "selected": "" ).">DVPBond</option>
			<option value='2' ".($rows['settlement_type'] == '2'? "selected": "" ).">RVPBond</option>
			<option value='3' ".($rows['settlement_type'] == '3'? "selected": "" ).">DFOPBond</option>
			<option value='4' ".($rows['settlement_type'] == '4'? "selected": "" ).">RFOPBond</option>
		</select>",
		"<input type=text size='20' name=txt_sellers_tax_id value='".$rows[sellers_tax_id]."'>",
		"<select name='txt_purpose_of_transaction' >
			<option value='1' ".($rows['purpose_of_transaction'] == '1'? "selected": "" ).">HTM</option>
			<option value='2' ".($rows['purpose_of_transaction'] == '2'? "selected": "" ).">AFS</option>
			<option value='3' ".($rows['purpose_of_transaction'] == '3'? "selected": "" ).">FVTPL</option>
			<option value='4' ".($rows['purpose_of_transaction'] == '4'? "selected": "" ).">LR</option>
		</select>",
		"<select name='txt_statutory_type' >
			<option value='1' ".($rows['statutory_type'] == '1'? "selected": "" ).">Yes</option>
			<option value='2' ".($rows['statutory_type'] == '2' || $rows['statutory_type'] == ''? "selected": "" ).">No</option>
		</select>",
		"<input type=text size='30' name=txt_remarks value='".$rows[remarks]."'>",
		"<input type=text size='30' name=txt_cancellation_reason id='txt_cancellation_reason' readonly='readonly' value='".$rows[cancellation_reason]."'>",
		"",
		"<input type=text size='20' name=txt_acquisition_price id='txt_acquisition_price' value='".$rows[acquisition_price]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_acquisition_amount id='txt_acquisition_amount' value='".$rows[acquisition_amount]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' readonly>",
		"<input type=text size='20' name=txt_capital_gain id='txt_capital_gain' value='".$rows[capital_gain]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_days_of_holding_interest id='txt_days_of_holding_interest' value='".$rows[days_of_holding_interest]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
		"<input type=text size='20' name=txt_holding_interest_amount id='txt_holding_interest_amount' value='".$rows[holding_interest_amount]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calcNetProceeds();'>",
		"<input type=text size='20' name=txt_total_taxable_income id='txt_total_taxable_income' value='".$rows[total_taxable_income]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' readonly>",
		"<input type=text size='20' name=txt_tax_rate id='txt_tax_rate' value='".$rows[tax_rate]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' >",
		"<input type=text size='20' name=txt_tax_amount id='txt_tax_amount' value='".$rows[tax_amount]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' >",
		"",
		"<input type=text size='30' name=txt_custody value='".$rows[custody]."' >",
		"<input type=text size='30' name=txt_subreg value='".$rows[subreg]."' >",
		"<input type=text size='30' name=txt_client_code value='".$rows[client_code]."' >",
		"<input type=text size='30' name=txt_pic value='".$rows[pic_name]."' >",
		"<input type=text size='30' name=txt_telp value='".$rows[pic_telp]."' >",
		"<input type=text size='30' name=txt_fax value='".$rows[pic_fax]."' >",
		)
	);
    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='obligasi.php';\">"
	);
}
if ($_GET['detail']=='1'){
	$rows = $data->get_row("select tbl_kr_me_bonds.*, format(market_value,0) as VAL from tbl_kr_me_bonds where obligasi_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];
	
			if($rows[buy_sell]=='1'){
				$buysell='Buy';
				$link="<input type=button name=print value=print onclick=\"window.location='obligasi_buy_print.php?id=$_GET[id]';\">";
			} else {
				$buysell='Sell';
				$link="<input type=button name=print value=print onclick=\"window.location='obligasi_sell_print.php?id=$_GET[id]';\">";
			}

			if($rows[purpose_of_transaction]=='1'){
				$purpose_of_transaction='HTM';
			} elseif ($rows[purpose_of_transaction]=='2'){
				$purpose_of_transaction='AFS';				
			} elseif ($rows[purpose_of_transaction]=='3'){
				$purpose_of_transaction='FVTPL';				
			} else {
				$purpose_of_transaction='LR';
			}

			if($rows[settlement_type]=='1'){
				$settlement_type='DVPBond';
			} elseif ($rows[settlement_type]=='2'){
				$settlement_type='RVPBond';				
			} elseif ($rows[settlement_type]=='3'){
				$settlement_type='DFOPBond';				
			} else {
				$settlement_type='RFOPBond';
			}

			if($rows[statutory_type]=='1'){
				$statutory_type='Yes';
			} else {
				$statutory_type='No';
			}
			
	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Code',
						'Obligasi',
						'Rating',
						'Broker',
						'Trade Date',
						'Maturity Date',
						'Settlement Date',
						'Last Coupon Date',
						'Next Coupon Date',
					
						'Quantity (Rp.)',
						'Harga Perolehan (%)',
						'Terhadap Total Aktiv (%)',
						'Indikator Maks',
						'Coupon Rate',
						'Harga H-1',
						'Market Value (Rp.)',
						'&nbsp;',
						'Transaction Status',
						'TA Reference ID',
						'TA Reference No',
						'Buy/Sell',
						'Accrued Days',
						'Accrued Interest Amount',
						'Other Fee',
						'Capital Gain Tax',
						'Interest Income Tax',
						'Withholding Tax',
						'Net Proceeds',
						'Settlement Type',
						'Sellers Tax ID',
						'Purpose of Transaction',
						'Statutory Type',
						'Remarks',
						'Cancellation Reason',
						'&nbsp;',
						'Acquisition Date',
						'Acquisition Price',
						'Acquisition Amount',
						'Capital Gain',
						'Days of Holding Interest',
						'Holding Interest Amount',
						'Total Taxable Income',
						'Tax Rate in %',
						'Tax Amount'
						),
		'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':'),
		'FIELD' => array (
			'&nbsp;'.$allocation2,
			'&nbsp;'.$rows[code],
			'&nbsp;'.$rows[name_bonds],
			'&nbsp;'.$rows[rating],
			'&nbsp;'.$rows[seller],
			'&nbsp;'.$rows[trade_date],
			'&nbsp;'.$rows[maturity],
			'&nbsp;'.$rows[settlement_date],
			'&nbsp;'.$rows[last_coupon],
			'&nbsp;'.$rows[next_coupon],
			'&nbsp;'.number_format($rows[face_value],0),
			'&nbsp;'.$rows[int_rate],
			'&nbsp;'.$rows[total_aktiv],
			'&nbsp;'.$rows[indikator_maks],
			'&nbsp;'.$rows[coupon_rate],
			'&nbsp;'.$rows[harga_minus_one],
			'&nbsp;'.$rows[VAL],
			"",
			'&nbsp;'.$rows[transaction_status],
			'&nbsp;'.$rows[ta_reference_id],
			'&nbsp;'.$rows[ta_reference_no],
			'&nbsp;'.$buysell,
			'&nbsp;'.$rows[accrued_days],
			'&nbsp;'.number_format($rows[accrued_interest_amount],2),
			'&nbsp;'.number_format($rows[other_fee],2),
			'&nbsp;'.number_format($rows[capital_gain_tax],2),
			'&nbsp;'.number_format($rows[interest_income_tax],2),
			'&nbsp;'.number_format($rows[withholding_tax],2),
			'&nbsp;'.number_format($rows[net_proceeds],2),
			'&nbsp;'.$settlement_type,
			'&nbsp;'.$rows[sellers_tax_id],
			'&nbsp;'.$purpose_of_transaction,
			'&nbsp;'.$statutory_type,
			'&nbsp;'.$rows[remarks],
			'&nbsp;'.$rows[cancellation_reason],
			"",
			'&nbsp;'.$rows[acquisition_date],
			'&nbsp;'.$rows[acquisition_price],
			'&nbsp;'.$rows[acquisition_amount],
			'&nbsp;'.$rows[capital_gain],
			'&nbsp;'.$rows[days_of_holding_interest],
			'&nbsp;'.$rows[holding_interest_amount],
			'&nbsp;'.$rows[total_taxable_income],
			'&nbsp;'.$rows[tax_rate],
			'&nbsp;'.$rows[tax_amount],
			)
		);

	$tittle = "BONDS DETAIL ".$allocation2."";
    $button = array ('SUBMIT' => $link,
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"window.location='obligasi.php';\">"
	);
}

$javascript = "<script type='text/javascript'>
	
	function checkdet(a,b,c,d){
		var doc = document;
		var ttl_cek = isNaN(parseFloat(doc.getElementById('txt_face').value.toString().replace(',','')));
		if(ttl_cek==true){
			var total=parseFloat(0);
		}else{
			var total = parseFloat(doc.getElementById('txt_face').value);
		}
		if(a.checked==true){
			c.value=d.value;
			c.readOnly=false;
			total=parseFloat(total)+parseFloat(c.value);
			b.value=c.value;
		}else{
			b.value=c.value;
			total=parseFloat(total)-parseFloat(c.value);
			c.value=0;
			c.readOnly=true;
		}
		document.getElementById('txt_face').value=total;
	}

	function validval(a,b){
		if(parseFloat(a.value)>parseFloat(b.value)){
			alert('Nilai Qty Tidak Boleh Lebih Dari Nilai Qty yang Ada !');
			a.value=b.value;
		}
	}

	function sumFace(a,b,c,d){
		var doc = document;
		var total = parseFloat(doc.getElementById('txt_face').value.toString().replace(',',''));
		if(total==0){
			total=parseFloat(0);
		}
		//alert(total);
		if(a.checked==true){
			var selisih=parseFloat(c.value)-parseFloat(b.value);
			total=parseFloat(total)+parseFloat(selisih);
			b.value=c.value;
		}else{
			total=parseFloat(total)-parseFloat(selisih);
			c.value=0;
			c.readOnly=true;
		}
		document.getElementById('txt_face').value=total;
	}

	function buysell(){
		
		var doc = document;

		var buysell = doc.getElementById('txt_buy_sell').value;
		if(buysell==1){
			doc.getElementById('txt_acquisition_date').style.visibility = (doc.getElementById('txt_acquisition_date').style.visibility==='visible')?'hidden':'hidden';
			doc.getElementById('txt_acquisition_date').readOnly = true;
			doc.getElementById('txt_acquisition_price').readOnly = true;
			doc.getElementById('txt_acquisition_amount').disabled = true;
			doc.getElementById('txt_capital_gain').disabled = true;
			doc.getElementById('txt_days_of_holding_interest').disabled = true;
			doc.getElementById('txt_holding_interest_amount').disabled = true;
			doc.getElementById('txt_total_taxable_income').disabled = true;
			doc.getElementById('txt_tax_rate').disabled = true;
			doc.getElementById('txt_tax_amount').disabled = true;
			
			doc.getElementById('txt_acquisition_date').value = 0;
			doc.getElementById('txt_acquisition_price').value = 0;
			doc.getElementById('txt_acquisition_amount').value = 0;
			doc.getElementById('txt_capital_gain').value = 0;
			doc.getElementById('txt_days_of_holding_interest').value = 0;
			doc.getElementById('txt_holding_interest_amount').value = 0;
			doc.getElementById('txt_total_taxable_income').value = 0;
			doc.getElementById('txt_tax_rate').value = 0;
			doc.getElementById('txt_tax_amount').value = 0;
		}
	}
	
	function calcNetProceeds(){
		var doc = document;

		var buysell = doc.getElementById('txt_buy_sell').value;

		var qty = parseFloat(doc.getElementById('txt_face').value.toString().replace(',',''));
		var facevalue = parseFloat(doc.getElementById('txt_market_price').value.toString().replace(',',''));

		var accrued_interest_amount = parseFloat(doc.getElementById('txt_accrued_interest_amount').value.toString().replace(',',''));
		var other_fee = parseFloat(doc.getElementById('txt_other_fee').value.toString().replace(',',''));

		var capital_gain_tax = parseFloat(doc.getElementById('txt_capital_gain_tax').value.toString().replace(',',''));
		var interest_income_tax = parseFloat(doc.getElementById('txt_interest_income_tax').value.toString().replace(',',''));
		
		var withholding_tax = capital_gain_tax+interest_income_tax;
		var acquisition_price = parseFloat(doc.getElementById('txt_acquisition_price').value.toString().replace(',',''));
		doc.getElementById('txt_withholding_tax').value=reformat2(withholding_tax);

		var proceeds = qty * facevalue/100;
		var acquisition_amount = qty * acquisition_price/100;
		doc.getElementById('txt_acquisition_amount').value=reformat2(acquisition_amount.toFixed(2));

		
		//var capital_gain = proceeds-acquisition_amount;
		var capital_gain = ((facevalue-acquisition_price)/100)*qty;

		var company_type = document.getElementById('txt_comp_type').value;
		var day_holding = document.getElementById('txt_days_of_holding_interest').value;
		//var qty = parseFloat(doc.getElementById('txt_face').value.toString().replace(',',''));
		var coupon = parseFloat(doc.getElementById('txt_indikator_maks').value.toString().replace(',',''));
		if(company_type==2){
			var now = new Date();
			var start = new Date(now.getFullYear(), 0, 1);
			var end = new Date(now.getFullYear(), 11, 31);
			var diff = end - start;
			var oneDay = 1000 * 60 * 60 * 24;
			var day_year = Math.floor(diff / oneDay);

			holding=(day_holding/day_year)*(coupon/100)*qty;
		} else if(company_type==1){
			var day_year=360;
			holding=(day_holding/day_year)*(coupon/100)*qty;
		}
		var total_taxable_income = capital_gain+holding;
		
		doc.getElementById('txt_holding_interest_amount').value=reformat2(holding.toFixed(2));


		var net_proceeds = 0;
		if(buysell == 1){
			net_proceeds=proceeds+accrued_interest_amount+other_fee-withholding_tax;
			doc.getElementById('txt_net_proceeds').value=reformat2(net_proceeds);

		}else if(buysell == 2){
			net_proceeds=proceeds+accrued_interest_amount-other_fee-withholding_tax;
			doc.getElementById('txt_net_proceeds').value=reformat2(net_proceeds);
			//doc.getElementById('txt_acquisition_date').style.visibility = (doc.getElementById('txt_acquisition_date').style.visibility==='hidden')?'visible':'visible';
			
		}
		//doc.getElementById('txt_market_value').value=reformat2(proceeds);
		doc.getElementById('txt_market_value').value=reformat2( parseFloat(doc.getElementById('txt_face').value.toString().replace(',',''))* parseFloat(doc.getElementById('txt_rate').value.toString().replace(',',''))/100);
		doc.getElementById('txt_capital_gain').value=reformat2(capital_gain.toFixed(2));
		if(total_taxable_income<0){
			doc.getElementById('txt_tax_rate').value=0;
			doc.getElementById('txt_tax_amount').value=0;
		}else{
			doc.getElementById('txt_total_taxable_income').value=reformat2(total_taxable_income.toFixed(2));
		}
		calcAccrued()
	}

	function autofill(){
		if(document.getElementById('txt_market_price').value ==''){
			document.getElementById('txt_market_price').value = 0;
		}
		if(document.getElementById('txt_accrued_interest_amount').value ==''){
			document.getElementById('txt_accrued_interest_amount').value = 0;
		}
		if(document.getElementById('txt_other_fee').value ==''){
			document.getElementById('txt_other_fee').value = 0;
		}
		if(document.getElementById('txt_capital_gain_tax').value ==''){
			document.getElementById('txt_capital_gain_tax').value = 0;
		}
		if(document.getElementById('txt_interest_income_tax').value ==''){
			document.getElementById('txt_interest_income_tax').value = 0;
		}
		if(document.getElementById('txt_withholding_tax').value ==''){
			document.getElementById('txt_withholding_tax').value = 0;
		}
		if(document.getElementById('txt_acquisition_price').value ==''){
			document.getElementById('txt_acquisition_price').value = 0;
		}
		if(document.getElementById('txt_capital_gain').value ==''){
			document.getElementById('txt_capital_gain').value = 0;
		}
		if(document.getElementById('txt_holding_interest_amount').value ==''){
			document.getElementById('txt_holding_interest_amount').value = 0;
		}
		if(document.getElementById('txt_tax_rate').value ==''){
			document.getElementById('txt_tax_rate').value = 0;
		}
		if(document.getElementById('txt_tax_amount').value ==''){
			document.getElementById('txt_tax_amount').value = 0;
		}
		if(document.getElementById('txt_holding_interest_amount').value ==''){
			document.getElementById('txt_total_taxable_income').value = 0;
		}
		//calcAccrued();
	}
	function stringToDate(_date,_format,_delimiter)
	{
		var formatLowerCase=_format.toLowerCase();
		var formatItems=formatLowerCase.split(_delimiter);
		var dateItems=_date.split(_delimiter);
		var monthIndex=formatItems.indexOf('mm');
		var dayIndex=formatItems.indexOf('dd');
		var yearIndex=formatItems.indexOf('yyyy');
		var month=parseInt(dateItems[monthIndex]);
		month-=1;
		var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
		return formatedDate;
	}
	function calcAccrued() {
		/*
		console.log('calcAccrued');
		accdate = stringToDate(document.getElementById('txt_acquisition_date').value,'yyyy-mm-dd','-');
		lastdate = stringToDate(document.getElementById('txt_prev_coupon').value,'yyyy-mm-dd','-');
		settledate = stringToDate(document.getElementById('txt_settle').value,'yyyy-mm-dd','-');
		accruedday=0;
		if( accdate>=lastdate &&  accdate<=settledate ) {
			accruedday = ((settledate-accdate)-86400000)/86400000;
		} else {
			accruedday = ((settledate-lastdate)-86400000)/86400000;
		}
		console.log('accruedday : '+accruedday.toString());
		document.getElementById('txt_accrued_days').value=accruedday.toString();
		*/
		companytype= document.getElementById('txt_comp_type').value;
		accdate = stringToDate(document.getElementById('txt_acquisition_date').value,'yyyy-mm-dd','-');
		lastdate = stringToDate(document.getElementById('txt_prev_coupon').value,'yyyy-mm-dd','-');
		settledate = stringToDate(document.getElementById('txt_settle').value,'yyyy-mm-dd','-');
		accruedday=0;
		//if( accdate>=lastdate &&  accdate<=settledate ) {
		/*
			if ( companytype!=1 ) {
				accruedday = ((settledate-accdate)-86400000)/86400000;
			} else {
				month_interval = settledate.getMonth() - accdate.getMonth()+1;
				date_start = settledate.getDate();
				date_end = accdate.getDate();
				if(date_start==30 || date_start==31) {
					month_interval = month_interval-1;
					date_start=0;
				}
				if((settledate.getMonth()==2 && settledate.getDate()==28)||(settledate.getMonth()==2 && settledate.getDate()==29)) {
					month_interval=month_interval-1;
					date_start=0;
				}
				if(date_end>30){
					date_end==30;
				}
				if((accdate.getMonth()==2 && accdate.getDate()==28)||(accdate.getMonth() && accdate.getDate()==29)){
					date_end==30;
				}
				if(month_interval>2){
					var intvl=month_interval-2;
					var day_int=intvl*30;
					var datestart=30-date_start;
					accruedday=day_int+datestart+date_end;
				} else if(month_interval==2){
					var datestart=30-date_start;
					accruedday = datestart + date_end;
				} else{
					accruedday = date_start - date_end;
				}
			}
			*/
		// } else {
			if( companytype!=1 ) {
				accruedday = ((settledate-lastdate)-86400000)/86400000;
			} else  {
				month_interval = settledate.getMonth() - lastdate.getMonth()+1;
				date_start = settledate.getDate();
				date_end = lastdate.getDate();
				if(date_start==30 || date_start==31) {
					month_interval = month_interval-1;
					date_start=0;
				}
				if((settledate.getMonth()==1 && settledate.getDate()==28)||(settledate.getMonth()==1 && settledate.getDate()==29)) {
					month_interval=month_interval-1;
					date_start=0;
				}
				if(date_end>30){
					date_end==30;
				}
				if((lastdate.getMonth()==1 && lastdate.getDate()==28)||(lastdate.getMonth()==1 && lastdate.getDate()==29)){
					date_end==30;
				}
				if(month_interval>2){
					var intvl=month_interval-2;
					var day_int=intvl*30;
					var datestart=30-date_start;
					accruedday=day_int+datestart+date_end;
				} else if(month_interval==2){
					var datestart=30-date_start;
					accruedday = datestart + date_end;
				} else{
					accruedday = date_start - date_end;
				}
			}
			
		//}
		//console.log('accruedday : '+accruedday.toString());
		document.getElementById('txt_accrued_days').value=accruedday.toString();
	}
	function initscript() 
	{
		document.getElementById('form1').addEventListener('mouseover', function(){
		  calcAccrued();
		});
	}	
</script>";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css',
	  'PATHCUSTOMJS' => $GLOBALS['JS'].'custom.js',
      	);


$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>