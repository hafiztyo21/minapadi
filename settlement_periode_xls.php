<?php
session_start();
error_reporting(1);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';

//header("Content-type: application/vnd.ms-excel");
//header("Content-Disposition: attachment; filename=History_periode.xls");
//header("Pragma: no-cache");
//header("Expires: 0");
	
	$data = new contract;

	$txt_tanggal = trim(htmlentities($_GET['from']));
	$txt_tanggalto = trim(htmlentities($_GET['to']));
	$sahamCode = trim($_GET['code']);
	$txt_allocation 	= trim(htmlentities($_GET['all']));
	if($sahamCode=='')
	{
		$sql  = "SELECT date(create_dt) create_dt, code,type_buy,FORMAT(lembar,0) AS lembar,FORMAT(harga,2) AS harga,total  , tbl_kr_securitas.securitas_type 
		FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas
		ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
		WHERE date(create_dt) between '".$txt_tanggal."' and '".$txt_tanggalto."'
		AND tbl_kr_dealer_tmp.allocation = '".$txt_allocation."'
		ORDER BY create_dt";
	} else
	{
		$sql  = "SELECT date(create_dt) create_dt, code,type_buy,FORMAT(lembar,0) AS lembar,FORMAT(harga,2) AS harga,total, tbl_kr_securitas.securitas_type 
		FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas
		ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
		WHERE date(create_dt) between '".$txt_tanggal."' and '".$txt_tanggalto."'
		AND tbl_kr_dealer_tmp.code = '".$sahamCode."'
		AND tbl_kr_dealer_tmp.allocation = '".$txt_allocation."'
		ORDER BY create_dt";
	}
	$result = mysql_query($sql);
	//print_r( $rows);
	$rowo = $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$txt_allocation."'");
?>
<html>
	<head> 
		<title>Settlement Periode</title>
		<style type="text/css">
			table { page-break-inside:auto }
			tr    { page-break-inside:avoid; page-break-after:auto }
			thead { display:table-header-group }
		</style>
	</head>
	<body onload="window.print()">
	<?php
	echo  '<h3>Allocation : '.$rowo['A'].'</h3>';
	?>
		<table style="border: 1px solid black;border-spacing:0px;">
			<thead>
			<tr>
				<th style="border: 1px solid black;border-spacing:0px;width:40px;background-color:rgb(65, 151, 244);">No</th>
				<th style="border: 1px solid black;border-spacing:0px;width:95px;background-color:rgb(65, 151, 244);">Tanggal</th>
				<th style="border: 1px solid black;border-spacing:0px;width:40px;background-color:rgb(65, 151, 244);">Broker</th>
				<th style="border: 1px solid black;border-spacing:0px;width:80px;background-color:rgb(65, 151, 244);">Saham</th>
				<th style="border: 1px solid black;border-spacing:0px;width:70px;background-color:rgb(65, 151, 244);">Status</th>
				<th style="border: 1px solid black;border-spacing:0px;width:100px;background-color:rgb(65, 151, 244);">Jumlah</th>
				<th style="border: 1px solid black;border-spacing:0px;width:100px;background-color:rgb(65, 151, 244);">Harga</th>
				<th style="border: 1px solid black;border-spacing:0px;width:150px;background-color:rgb(65, 151, 244);">Nilai</th>
			</tr>
			</thead>
<?php

		$a=0;
		$total =0;
		while ($row = mysql_fetch_assoc($result))
		{	
			echo "<tr>";
			echo "<td style='border: 1px solid black;padding-left:5px;'>".($a+1)."</td>";			
			echo "<td style='border: 1px solid black;padding-left:5px;'>".$row['create_dt']."</td>";
			echo "<td style='border: 1px solid black;padding-left:5px;'>".$row['securitas_type']."</td>";
			echo "<td style='border: 1px solid black;padding-left:5px;'>".$row['code']."</td>";
			echo "<td style='border: 1px solid black;padding-left:5px;'>".$row['type_buy']."</td>";
			echo "<td style='border: 1px solid black;text-align:right;padding-right:5px;'>".$row['lembar']."</td>";
			echo "<td style='border: 1px solid black;text-align:right;padding-right:5px;'>".$row['harga']."</td>";
			echo "<td style='border: 1px solid black;text-align:right;padding-right:5px;'>".number_format($row['total'],2,",",".")."</td>";			
			echo "</tr>";			
			$total+=$row['total'];
			$a++;
		}
		echo "<tr>";
		echo "<td colspan='7'  style='border: 1px solid black;text-align:right;padding-right:5px;'><b>TOTAL</b></td>";
		echo "<td style='border: 1px solid black;text-align:right;padding-right:5px;'>".number_format($total,2,",",".")."</td>";
		echo "</tr>";
?>			
		</table>
	</body>
</html>