<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$filename = $transaction_date."_swtc_fee.txt";

// output headers so that the file is downloaded rather than displayed
//header('Content-Type: text/csv; charset=utf-8');
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
try{


$output = fopen('php://output', 'w');

/*$myfile = fopen("newfile.txt", "w");
$txt = "Mickey Mouse\n";
fwrite($myfile, $txt);
$txt = "Minnie Mouse\n";
fwrite($myfile, $txt);
fclose($myfile);*/

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    DATE_FORMAT(transaction_date, '%Y%m%d') as transaction_date, 
    transaction_type, 
    sa_code,
    reference_no,  
    investor_ac_no, 
    switch_out_fund_code, 
    amount, 
    amount_unit, 
    amount_all_unit,
    fee_charge_fund, 
    fee, 
    fee_unit, 
    fee_persen, 
    switch_in_fund_code,
    payment_date, 
    transfer_type, 
    sa_reference_no 
    FROM tbl_kr_swtc_fee 
    WHERE DATE_FORMAT(tbl_kr_swtc_fee.transaction_date, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0";

$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    
    $str = $rows[$i]['transaction_date']."|".$rows[$i]['transaction_type']."|".$rows[$i]['reference_no']."|".$rows[$i]['sa_code']
        ."|".$rows[$i]['investor_ac_no']."|".$rows[$i]['switch_out_fund_code']."|".$rows[$i]['amount']
        ."|".$rows[$i]['amount_unit']."|".$rows[$i]['amount_all_unit']."|".$rows[$i]['fee_charge_fund']."|".$rows[$i]['fee']
        ."|".$rows[$i]['fee_unit']."|".$rows[$i]['fee_persen']
        ."|".$rows[$i]['switch_in_fund_code']
        ."|".$rows[$i]['payment_date']."|".$rows[$i]['transfer_type']."|".$rows[$i]['sa_reference_no']."\r\n";

    fwrite($output, $str);
    //fputcsv($output, $rows[$i]);
}

fclose($output);
} catch(Exception $e){
    echo $e->getMessage();
}
?>
