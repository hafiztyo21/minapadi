<?php
session_start();
#session_destroy();
#print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;

if($_GET['hist']!=1){
	$rows 	= $data->get_rows("select * from tbl_kr_me_bonds where obligasi_id = '".$_GET[id]."'");
}else{
	$rows 	= $data->get_rows("select * from tbl_kr_me_bonds_hist where obligasi_id = '".$_GET[id]."'");	
}

$row_all	= $data->get_rows("select * from tbl_kr_allocation where pk_id = '".$rows[0][allocation]."'");

$row_sec = $data->get_row("SELECT * FROM tbl_kr_securitas WHERE securitas_id = '".$rows[0]['securitas_id']."' ");
$sec_name = $row_sec['description'];
$sec_code = $row_sec['securitas_code'];
$sec_id = $row_sec['securitas_id'];	

switch ($rows[0]['settlement_type']) {
	case '1':
		$settlement_type='DVP';
		break;
	
	case '2':
		$settlement_type='RVP';
		break;
	
	case '3':
		$settlement_type='DFOP';
		break;
	
	case '4':
		$settlement_type='RFOP';
		break;
	
	default:
		$settlement_type='';
		break;
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sold Bond Instruction Print </title>
</head>

<body onload="window.print();" style="font-size: 14px;">
<!-- <body>  -->

 <table width="100%" border="0" align="left" cellpadding="2">
	<tr>
		<td rowspan="4" width="4%">&nbsp;</td>
	</tr>
  	<tr>
		<td width="90%">
		<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td height="10" colspan="6" align="center" ></td>
  </tr>
  <tr>
		  	<td height="50" colspan="10" align="center" background="templates/image/bg_menu.gif">
			<patTemplate:tmpl name="tittles"> <strong>Instruksi Penjualan Obligasi</strong>
			</patTemplate:tmpl></td>
  <tr>
		<td height="5px" colspan="6" align="center" ></td>
  </tr>

</table>
<br/>
		
  <table width="90%" border="0" align="left" style="margin-bottom: 15px;">
  	<tr>
		<td width="24%">Date</td>
		<td width="2%">:</td>
		<td width="75%"><?=date('d F Y',strtotime($rows[0]['create_dt']))?></td>
	</tr>
  	<tr>
		<td width="24%">Nomor</td>
		<td width="2%">:</td>
		<td width="75%"><?=$rows[0]['ta_reference_no']?></td>
	</tr>
  	<tr>
		<td width="24%">To</td>
		<td width="2%">:</td>
		<td width="75%">Bank Mandiri Cab. Plaza Mandiri</td>
	</tr>
  	<tr>
		<td width="24%">Attn</td>
		<td width="2%">:</td>
		<td width="75%">Bpk Jati Satrio</td>
	</tr>
  	<tr>
		<td width="24%">Fax</td>
		<td width="2%">:</td>
		<td width="75%">(021) 526 8201</td>
	</tr>
  	<tr>
		<td width="24%">Re</td>
		<td width="2%">:</td>
		<td width="75%"><b><?=$rows[0]['name_bonds']?> <?=$rows[0]['code']?></b></td>
	</tr>
  </table>

  <table width="90%"align="left">
	<tr>
	   <td>
	  	We would like to confirm having <strong>SOLD BOND</strong> with details as follows :
	   </td>
	</tr>
  </table>

  <table width="90%" border="0" align="left" style="margin-bottom: 15px;">
  	<tr>
		<td width="24%">Securities name</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2"><b><?=$rows[0]['name_bonds']?></b></td>
	</tr>
  	<tr>
		<td width="24%">Buyer</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2">Reksa Dana Minna Padi <?=$row_all[0]['allocation']?></td>
	</tr>
  	<tr>
		<td width="24%">Seller</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2"><?=$rows[0]['seller']?></td>
	</tr>
  	<tr>
		<td width="24%">Maturity date</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2"><?=date('d F Y',strtotime($rows[0]['maturity']))?></td>
	</tr>
  	<tr>
		<td width="24%">Last Coupon date</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2"><?=date('d F Y',strtotime($rows[0]['last_coupon']))?></td>
	</tr>
  	<tr>
		<td width="24%">Quantity / Nominal</td>
		<td width="2%">:</td>
		<td width="22%" align="right"><?=number_format($rows[0]['face_value'],2)?></td>
		<td width="70%">&emsp;IDR</td>
	</tr>
  	<tr>
		<td width="24%">Current Coupon</td>
		<td width="2%">:</td>
		<td width="22%" align="right"><?=number_format($rows[0]['int_rate'],3)?>%</td>
		<td width="70%">&emsp;<i>p.a.</i></td>
	</tr>
  	<tr>
		<td width="24%">Price</td>
		<td width="2%">:</td>
		<td width="10%" align="right"><?=number_format($rows[0]['market_price'],3)?>%</td>
		<td width="70%">&emsp;</td>
	</tr>
  	<tr>
		<td width="24%">Amount</td>
		<td width="2%">:</td>
		<td width="22%" align="right"><?=number_format($rows[0]['market_value'],2)?></td>
		<td width="70%">&emsp;IDR</td>
	</tr>
  	<tr>
		<td width="24%">Accured Interest</td>
		<td width="2%">:</td>
		<td width="22%" align="right"><?=number_format($rows[0]['accured_interest_amount'],2)?></td>
		<td width="70%">&emsp;IDR</td>
	</tr>
  	<tr>
		<td width="24%">Cap. Gain tax (5%)</td>
		<td width="2%">:</td>
		<td width="22%" align="right"><?=number_format($rows[0]['capital_gain_tax'],2)?></td>
		<td width="70%">&emsp;IDR</td>
	</tr>
  	<tr>
		<td width="24%">Acc. Interest tax (15%)</td>
		<td width="2%">:</td>
		<td width="22%" align="right"><?=number_format($rows[0]['interest_income_tax'],2)?></td>
		<td width="70%">&emsp;IDR</td>
	</tr>
  	<tr>
		<td width="24%"><strong>Total</strong></td>
		<td width="2%">:</td>
		<td width="22%" align="right"><b><?=number_format(($rows[0]['market_value']+$rows[0]['accured_interest_amount']+$rows[0]['interest_income_tax']+$rows[0]['capital_gain_tax']),2)?></b></td>
		<td width="70%">&emsp;IDR</td>
	</tr>
  	<tr>
		<td width="24%">Trade date</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2"><?=date('d F Y',strtotime($rows[0]['trade_date']))?></td>
	</tr>
  	<tr>
		<td width="24%">Settlement date</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2"><?=date('d F Y',strtotime($rows[0]['settlement_date']))?></td>
	</tr>
  	<tr>
		<td width="24%">Acquisition date</td>
		<td width="2%">:</td>
		<td width="22%" colspan="2"><?=date('d F Y',strtotime($rows[0]['acquisition_date']))?></td>
	</tr>
  	<tr>
		<td width="24%">Acquisition Price</td>
		<td width="2%">:</td>
		<td width="10%" align="right"><?=number_format($rows[0]['acquisition_price'],3)?> %</td>
		<td width="70%">&emsp;</td>
	</tr>
  </table>

  <table width="90%" border="0" align="left" style="margin-bottom: 15px;">
  	<tr>
		<td width="24%">On settlement date</td>
		<td width="2%">:</td>
		<td width="75%"><?=date('d F Y',strtotime($rows[0]['settlement_date']))?></td>
	</tr>
  	<tr>
		<td width="24%">Kindly Receive</td>
		<td width="2%">:</td>
		<td width="75%"><b>IDR <?=number_format(($rows[0]['market_value']+$rows[0]['accured_interest_amount']+$rows[0]['interest_income_tax']+$rows[0]['capital_gain_tax']),2)?> From</b></td>
	</tr>
  </table>

  <table width="90%" border="0" align="left" style="margin-bottom: 15px;">
  	<tr>
		<td width="24%">Instruction Type</td>
		<td width="2%">:</td>
		<td width="75%"><?=$settlement_type?></td>
	</tr>
  	<tr>
		<td width="24%">Custody</td>
		<td width="2%">:</td>
		<td width="75%"><?=$rows[0]['custody']?></td>
	</tr>
  	<tr>
		<td width="24%">Sub Reg</td>
		<td width="2%">:</td>
		<td width="75%"><?=$rows[0]['subreg']?></td>
	</tr>
  	<tr>
		<td width="24%">IFO</td>
		<td width="2%">:</td>
		<td width="75%"><?=$rows[0]['seller']?>, Client Code : <?=$rows[0]['client_code']?></td>
	</tr>
  	<tr>
		<td width="24%">PIC</td>
		<td width="2%">:</td>
		<td width="75%"><?=$rows[0]['pic_name']?> Telp : <?=$rows[0]['pic_telp']?> Fax : <?=$rows[0]['pic_fax']?></td>
	</tr>
  </table>

  <table width="90%" align="left" border="0" cellpadding="0" cellspacing="0">
  	<tr>
		<td width="25%" align="left">Hormat kami,</td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  	<tr>
		<td width="30%" align="left">&nbsp;</td>
	</tr>
  </table>
  <table width="90%" align="left">
  	<tr>
		<td width="24%" align="left"><strong><u>DJAJADI</u></strong></td>
	</tr>
  </table>
  <br/>
		
		
		</td>
	</tr>
	</table>	
  </div>

</body>
</html>