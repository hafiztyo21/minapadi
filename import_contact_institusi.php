<?php
session_start();
include 'Classes/excel_reader2.php';
//include 'Classes/PHPExcel/Writer/Excel2007.php';
//include 'Classes/PHPExcel/IOFactory.php';
//include "excel_reader2.php";
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$datadb = new globalFunction();
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('import_contact_institusi.html');
$tablename = 'tbl_kr_cus_institusi';


if($_POST[Submit]) {
					$data = new Spreadsheet_Excel_Reader($_FILES['uploadedfile']['tmp_name']);
					 
					// membaca jumlah baris dari data excel
					$baris = $data->rowcount($sheet_index=0);
					 
					// nilai awal counter untuk jumlah data yang sukses dan yang gagal diimport
					$sukses = 0;
					$gagal = 0;
					 
					// import data excel mulai baris ke-2 (karena baris pertama adalah nama kolom)
					for ($i=2; $i<=$baris; $i++)
					{
						$sa_code = $data->val($i, 3);
						$sid = $data->val($i, 4);
						$company_name = $data->val($i, 5);
                        $country_of_domicile = $data->val($i, 6);
						$siup_no = $data->val($i, 7);
						
                        $date=$data->val($i, 8);
						$siup_exp_date = '0000-00-00';
						if($date != ''){
							$date_a= substr($date, 6);
							$date_b= substr($date, 3, 2);
							$date_c= substr($date, 0, 2);
							$siup_exp_date = "$date_a-$date_b-$date_c";
						}

						$skd_no=$data->val($i, 9);

						$date=$data->val($i, 10);
						$skd_exp_date = '0000-00-00';
						if($date != ''){
							$date_a= substr($date, 6);
							$date_b= substr($date, 3, 2);
							$date_c= substr($date, 0, 2);
							$skd_exp_date = "$date_a-$date_b-$date_c";
						}

						$npwp_no=$data->val($i, 11);
						
					  	$date=$data->val($i, 12);
						$npwp_regis_date = '0000-00-00';
						if($date != ''){
							$year= substr($date, 6);
							$month= substr($date, 3, 2);
							$day= substr($date, 0, 2);
							$npwp_regis_date = "$year-$month-$day";
						}

						$country_of_establishment = $data->val($i, 13);
						$place_of_establishment = $data->val($i, 14);

						$date=$data->val($i, 15);
						$date_of_establishment = '0000-00-00';
						if($date != ''){
							$year= substr($date, 6);
							$month= substr($date, 3, 2);
							$day= substr($date, 0, 2);
							$date_of_establishment = "$year-$month-$day";
						}

                        $articles_of_assocation_no = $data->val($i, 16);
						$company_type = $data->val($i, 17);
						$company_characteristic = $data->val($i, 18);
						$income_level = $data->val($i, 19);
						$investor_risk_profile = $data->val($i, 20);
						$invesment_objective = $data->val($i, 21);
						$source_of_fund = $data->val($i, 22);

						$asset_owner = $data->val($i, 23);
						$company_address = $data->val($i, 24);
						$company_city = $data->val($i, 25);
						$company_city_name=$data->val($i, 26);
						$company_postal_code=$data->val($i, 27);

						$country_of_company=$data->val($i, 28);
						$office_phone=$data->val($i, 29);
						$facsimile=$data->val($i, 30);
						$email=$data->val($i, 31);
						$statement_type=$data->val($i, 32);

						$person1_firstname=$data->val($i, 33);
						$person1_middlename = $data->val($i, 34);
						$person1_lastname=$data->val($i, 35);
						$person1_position = $data->val($i, 36);
						$person1_mobilephone=$data->val($i, 37);

						$person1_email=$data->val($i, 38);
						$person1_npwpno=$data->val($i, 39);
						$person1_ktpno=$data->val($i, 40);
						
                        $date=$data->val($i, 41);
						$person1_ktpdate = '0000-00-00';
						if($date != ''){
							$year= substr($date, 6);
							$month= substr($date, 3, 2);
							$day= substr($date, 0, 2);
							$person1_ktpdate = "$year-$month-$day";
						}

						$person1_passportno=$data->val($i, 42);

						$date=$data->val($i, 43);
						$person1_passportdate = '0000-00-00';
						if($date != ''){
							$year= substr($date, 6);
							$month= substr($date, 3, 2);
							$day= substr($date, 0, 2);
							$person1_passportdate = "$year-$month-$day";
						}

                        $person2_firstname=$data->val($i, 44);
						$person2_middlename = $data->val($i, 45);
						$person2_lastname=$data->val($i, 46);
						$person2_position = $data->val($i, 47);
						$person2_mobilephone=$data->val($i, 48);

						$person2_email=$data->val($i, 49);
						$person2_npwpno=$data->val($i, 50);
						$person2_ktpno=$data->val($i, 51);
						
                        $date=$data->val($i, 52);
						$person2_ktpdate = '0000-00-00';
						if($date != ''){
							$year= substr($date, 6);
							$month= substr($date, 3, 2);
							$day= substr($date, 0, 2);
							$person2_ktpdate = "$year-$month-$day";
						}

						$person2_passportno=$data->val($i, 53);

						$date=$data->val($i, 54);
						$person2_passportdate = '0000-00-00';
						if($date != ''){
							$year= substr($date, 6);
							$month= substr($date, 3, 2);
							$day= substr($date, 0, 2);
							$person2_passportdate = "$year-$month-$day";
						}

						$asset_1=$data->val($i, 55);
						$asset_2=$data->val($i, 56);
						$asset_3=$data->val($i, 57);

						$profit_1=$data->val($i, 58);
						$profit_2=$data->val($i, 59);
                        $profit_3=$data->val($i, 60);

                        $fatca=$data->val($i, 61);
						$tin=$data->val($i, 62);
						$tin_country=$data->val($i, 63);
						$giin=$data->val($i, 64);
						$substantial_name=$data->val($i, 65);
						$substantial_address=$data->val($i, 66);
						$substantial_tin = $data->val($i, 67);

                        //=============================
                        $bank_bic_code1= '';
                        $bank_bi_member_code1= '';
                        $bank_name1= '';
                        $bank_country1= '';
                        $bank_branch1= '';
                        $acc_ccy1= '';
                        $acc_no1= '';
                        $acc_name1= '';
                        $bank_bic_code2= '';
                        $bank_bi_member_code2= '';
                        $bank_name2= '';
                        $bank_country2= '';
                        $bank_branch2= '';
                        $acc_ccy2= '';
                        $acc_no2= '';
                        $acc_name2= '';
                        $bank_bic_code3= '';
                        $bank_bi_member_code3= '';
                        $bank_name3= '';
                        $bank_country3= '';
                        $bank_branch3= '';
                        $acc_ccy3= '';
                        $acc_no3 = '';
                        $acc_name3 = '';
                        $acc_type3 = '';
						
					//print($cus_no_identity);
					$cek_data=$datadb->get_value("select count(cus_id) from tbl_kr_cus_institusi where cus_no_npwp='".$npwp_no."'");
					if ($cek_data ==0){
					  // setelah data dibaca, sisipkan ke dalam tabel mhs
					  $query = 
					  "INSERT INTO $tablename(
						cus_code,
						cus_tgl_subscribe,
						cus_name,
						cus_tempat_pt,
						cus_tgl_pt,
						
						
						cus_alamat_pt,
						cus_kota_pt,
						cus_kode_pos_pt,
						cus_propinsi_pt,
		
						cus_telp_pt,
						cus_fax_pt,
						cus_email_pt,
						
						cus_no_npwp,
						cus_no_siup,
						cus_tgl_siup,						
                        cus_inves,
		
                        cus_nama_,
                        cus_jabatan,
                        cus_telp_hp,
                        cus_no_KTP,
                        
                        cus_nama_2,
                        cus_jabatan_2,
                        cus_telp_hp_2,
                        cus_no_KTP_2,
                        
                        cus_tipe,
                        cus_karakteristik,
                        cus_sumber_dana,
                        cus_penghasilan,
                        cus_no_skd,
                        cus_ins_sid,
                        country_of_domicily,
                        skd_expiration_date,
                        country_of_establisment,
                        articles_of_association_no,
                        investors_riks_profile,
                        asset_owner,
                        country_of_company,
                        statement_type,
                        authorized_person_middle_name1,
                        authorized_person_last_name1,
                        authorized_person_email1,
                        authorized_person_npwp1,
                        authorized_person_ktp_expiration_date1,
                        authorized_person_passport1,
                        authorized_person_passport_expiration_date1,
                        authorized_person_middle_name2,
                        authorized_person_last_name2,
                        authorized_person_email2,
                        authorized_person_npwp2,
                        authorized_person_ktp_expiration_date2,
                        authorized_person_passport2,
                        authorized_person_passport_expiration_date2,
                        asset_information_last_year,
                        asset_information_2_years_ago,
                        asset_information_3_years_ago,
                        profit_information_last_year,
                        profit_information_2_years_ago,
                        profit_information_3_years_ago,
                        fatca,
                        tin,
                        tin_issuance_country,
                        giin,
                        substantial_us_owner_name,
                        substantial_us_owner_address,
                        substantial_us_owner_tin,
                        bank_bic_code1,
                        bank_bi_member_code1,
                        bank_name1,
                        bank_country1,
                        bank_branch1,
                        acc_ccy1,
                        acc_no1,
                        acc_name1,
                        bank_bic_code2,
                        bank_bi_member_code2,
                        bank_name2,
                        bank_country2,
                        bank_branch2,
                        acc_ccy2,
                        acc_no2,
                        acc_name2,
                        bank_bic_code3,
                        bank_bi_member_code3,
                        bank_name3,
                        bank_country3,
                        bank_branch3,
                        acc_ccy3,
                        acc_no3,
                        acc_name3,
                        acc_type3,
                        npwp_registration_date,
						created_date
						) VALUES 
					  (
                        '',
						now(),
						'$company_name',
						'$place_of_establishment',
						'$date_of_establishment',
						
						'$company_address',
						'$company_city',
						'$company_postal_code',
						'$company_city_name',
		
						'$office_phone',
						'$facsimile',
						'$email',
						
						'$npwp_no',						
						'$siup_no',
						'$siup_exp_date',
                        '$invesment_objective',
						
                        '$person1_firstname',
                        '$person1_position',
                        '$person1_mobilephone',
                        '$person1_ktpno',
                        
                        '$person2_firstname',
                        '$person2_position',
                        '$person2_mobilephone',
                        '$person2_ktpno',
                        
                        '$company_type',
                        '$company_characteristic',

                        '$source_of_fund',
                        '$income_level',
                        '$skd_no',
                        '$sid',
                        '$country_of_domicile',
                        '$skd_exp_date',
                        '$country_of_establishment',
                        '$articles_of_assocation_no',
                        '$investor_risk_profile',
                        '$asset_owner',
                        '$country_of_company',

                        '$statement_type',

                        '$person1_middlename',
                        '$person1_lastname',
                        '$person1_email',
                        '$person1_npwpno',
                        '$person1_ktpdate',
                        '$person1_passportno',
                        '$person1_passportdate',

                        '$person2_middlename',
                        '$person2_lastname',
                        '$person2_email',
                        '$person2_npwpno',
                        '$person2_ktpdate',
                        '$person2_passportno,
                        '$person2_passportdate',
                        '$asset_1',
                        '$asset_2',
                        '$asset_3',
                        '$profit_1',
                        '$profit_2',
                        '$profit_3',

                        '$fatca',
                        '$tin',
                        '$tin_issuance_country',
                        '$giin',
                        '$substantial_name',
                        '$substantial_address',
                        '$substantial_tin',
                        '$bank_bic_code1',
                        '$bank_bi_member_code1',
                        '$bank_name1',
                        '$bank_country1',
                        '$bank_branch1',
                        '$acc_ccy1',
                        '$acc_no1',
                        '$acc_name1',
                        '$bank_bic_code2',
                        '$bank_bi_member_code2',
                        '$bank_name2',
                        '$bank_country2',
                        '$bank_branch2',
                        '$acc_ccy2',
                        '$acc_no2',
                        '$acc_name2',
                        '$bank_bic_code3',
                        '$bank_bi_member_code3',
                        '$bank_name3',
                        '$bank_country3',
                        '$bank_branch3',
                        '$acc_ccy3',
                        '$acc_no3',
                        '$acc_name3',
                        '$acc_type3',
                        '$npwp_regis_date',
                        now()
					  )";
					  }
					  else
					  {
					$query= "UPDATE ".$tablename." SET 
						cus_name = '$company_name',
						cus_tempat_pt = '$place_of_establishment',
						cus_tgl_pt = '$date_of_establishment',
						
						cus_alamat_pt='$company_address',
						cus_kota_pt = '$company_city',
						cus_kode_pos_pt = '$company_postal_code',
						cus_propinsi_pt = '$company_city_name',
		
						cus_telp_pt = '$office_phone',
						cus_fax_pt = '$facsimile',
						cus_email_pt = '$email',
						
						cus_no_siup = '$siup_no',
						cus_tgl_siup = '$siup_exp_date',						
                        cus_inves = '$invesment_objective',
		
                        cus_nama_ = '$person1_firstname',
                        cus_jabatan = '$person1_position',
                        cus_telp_hp = '$person1_mobilephone',
                        cus_no_KTP = '$person1_ktpno',
                        
                        cus_nama_2 = '$person2_firstname',
                        cus_jabatan_2 = '$person2_position',
                        cus_telp_hp_2 = '$person2_mobilephone',
                        cus_no_KTP_2 = '$person2_ktpno',
                        
                        cus_tipe ='$company_type',
                        cus_karakteristik = '$company_characteristic',
                        cus_sumber_dana = '$source_of_fund',
                        cus_penghasilan = '$income_level',
                        cus_no_skd = '$skd_no',
                        cus_ins_sid = '$sid',
                        country_of_domicily = '$country_of_domicile',
                        skd_expiration_date = '$skd_exp_date',
                        country_of_establisment = '$country_of_establishment',
                        articles_of_association_no = '$articles_of_assocation_no',
                        investors_riks_profile = '$investor_risk_profile',
                        asset_owner = '$asset_owner',
                        country_of_company = '$country_of_company',
                        statement_type = '$statement_type',
                        authorized_person_middle_name1 = '$person1_middlename',
                        authorized_person_last_name1 = '$person1_lastname',
                        authorized_person_email1 = '$person1_email',
                        authorized_person_npwp1 = '$person1_npwpno',
                        authorized_person_ktp_expiration_date1 = '$person1_ktpdate',
                        authorized_person_passport1 = '$person1_passportno',
                        authorized_person_passport_expiration_date1 = '$person1_passportdate',
                        authorized_person_middle_name2 = '$person2_middlename',
                        authorized_person_last_name2 = '$person2_lastname',
                        authorized_person_email2 = '$person2_email',
                        authorized_person_npwp2 = '$person2_npwpno',
                        authorized_person_ktp_expiration_date2 = '$person2_ktpdate',
                        authorized_person_passport2 = '$person2_passportno',
                        authorized_person_passport_expiration_date2 = '$person2_passportdate',
                        asset_information_last_year = '$asset_1',
                        asset_information_2_years_ago = '$asset_2',
                        asset_information_3_years_ago = '$asset_3',
                        profit_information_last_year = '$profit_1',
                        profit_information_2_years_ago = '$profit_2',
                        profit_information_3_years_ago = '$profit_3',
                        fatca = '$fatca',
                        tin = '$tin',
                        tin_issuance_country = '$tin_issuance_country',
                        giin = '$giin',
                        substantial_us_owner_name = '$substantial_name',
                        substantial_us_owner_address = '$substantial_address',
                        substantial_us_owner_tin = '$substantial_tin',
                        npwp_registration_date = '$npwp_regis_date'
						WHERE cus_no_npwp = '$npwp_no'";
					  
					  }
					//print($query);
                        
						if ($datadb->inpQueryReturnBool($query))
						{	//echo "<script>alert('SUKSES');</script>";	
						    $sukses++;
                        }
						else
						{	//echo "<script>alert('GAGAL');</script>";
                            //echo $datadb->queryError();
						    $gagal++;
                        }
			
					 
					  // jika proses insert data sukses, maka counter $sukses bertambah
					  // jika gagal, maka counter $gagal yang bertambah
					
				
					}
					 
					// tampilan status sukses dan gagal
					echo "<h3>Proses import data selesai.</h3>";
					echo "<p>Jumlah data yang sukses diimport : ".$sukses."<br>";
					echo "Jumlah data yang gagal diimport : ".$gagal."</p>";
	
	
}



$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->addVar('page', 'tittle',$tittle);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>