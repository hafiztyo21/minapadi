<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('subsredm_order_add.html');

$saCode = 'MU002';

$transactionDate = date('Y-m-d');  
$transactionType = 1;       
$investorAcNo = '';
$fundCode	= '';           
$amount = 0;
$amountUnit = 0;
$amountAllUnit = '';
$fee = 0;
$feeUnit = 0;               
$feePersen = 0;
$ifua_id = 0;
$ifua_redm = 0;
$redm_payment_ac_sequential_code = '';
$redm_payment_bank_bic_code = '';
$redm_payment_bank_bi_member_code = '';
$redm_payment_ac_no = '';
$paymentDate = date('Y-m-d');
$transferType = 1;
$saReferenceNo	= '';
$tipe = 0;

$errorArr = array('','','','','','','','','','','','','');
$otherError='';

if(count($_POST) > 0){
	$transactionDate = trim(htmlentities($_POST['transactionDate']));
	$transactionType = trim(htmlentities($_POST['transactionType']));
	//$saCode = trim(htmlentities($_POST['saCode']));
	$investorAcNo = trim(htmlentities($_POST['investorAcNo']));
	$fundCode	= trim(htmlentities($_POST['fundCode']));
	$amount	= trim(htmlentities($_POST['amount']));
	$amountUnit	= trim(htmlentities($_POST['amountUnit']));
	$amountAllUnit	= trim(htmlentities($_POST['amountAllUnit']));
	$fee	= trim(htmlentities($_POST['fee']));
	$feeUnit	= trim(htmlentities($_POST['feeUnit']));
	$feePersen	= trim(htmlentities($_POST['feePersen']));
	$ifua_id = trim(htmlentities($_POST['ifuaid']));
	$tipe = trim(htmlentities($_POST['tipe']));
	$ifua_redm = trim(htmlentities($_POST['ifua_redm']));
	$redm_payment_ac_sequential_code	= trim(htmlentities($_POST['redm_payment_ac_sequential_code']));
	$redm_payment_bank_bic_code	= trim(htmlentities($_POST['redm_payment_bank_bic_code']));
	$redm_payment_bank_bi_member_code	= trim(htmlentities($_POST['redm_payment_bank_bi_member_code']));
	$redm_payment_ac_no	= trim(htmlentities($_POST['redm_payment_ac_no']));
	$paymentDate = trim(htmlentities($_POST['paymentDate']));
	$transferType	= trim(htmlentities($_POST['transferType']));
	$saReferenceNo	= trim(htmlentities($_POST['saReferenceNo']));
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
 		$transactionDate = trim(htmlentities($_POST['transactionDate']));
		$transactionType = trim(htmlentities($_POST['transactionType']));
		//$saCode = trim(htmlentities($_POST['saCode']));
		$investorAcNo = trim(htmlentities($_POST['investorAcNo']));
		$fundCode	= trim(htmlentities($_POST['fundCode']));
        $amount	= trim(htmlentities($_POST['amount']));
        $amountUnit	= trim(htmlentities($_POST['amountUnit']));
        $amountAllUnit	= trim(htmlentities($_POST['amountAllUnit']));
        $fee	= trim(htmlentities($_POST['fee']));
        $feeUnit	= trim(htmlentities($_POST['feeUnit']));
        $feePersen	= trim(htmlentities($_POST['feePersen']));
		$redm_payment_ac_sequential_code	= trim(htmlentities($_POST['redm_payment_ac_sequential_code']));
		$redm_payment_bank_bic_code	= trim(htmlentities($_POST['redm_payment_bank_bic_code']));
		$redm_payment_bank_bi_member_code	= trim(htmlentities($_POST['redm_payment_bank_bi_member_code']));
		$redm_payment_ac_no	= trim(htmlentities($_POST['redm_payment_ac_no']));
        $paymentDate = trim(htmlentities($_POST['paymentDate']));
        $transferType	= trim(htmlentities($_POST['transferType']));
        $saReferenceNo	= trim(htmlentities($_POST['saReferenceNo']));

		$ifua_id = trim(htmlentities($_POST['ifuaid']));
		$tipe = trim(htmlentities($_POST['tipe']));

		$amountReplace = str_replace(',','', $amount);
		$amountUnitReplace = str_replace(',','', $amountUnit);
		$feeReplace = str_replace(',','', $fee);
		$feeUnitReplace = str_replace(',','', $feeUnit);
		$feePersenReplace = str_replace(',','', $feePersen);
		
		$gotError = false;
        //validation
		if($investorAcNo==''){
			$errorArr[2] = "Investor Fund Unit A/C No must be filled";
			$gotError = true;
		}
		if($fundCode==''){
			$errorArr[3] = "Fund Code must be filled";
			$gotError = true;
		}
		if($transactionType==1 && ($amount == '' || $amountReplace == 0)){
			$errorArr[4] = "On Subscription, Amount (nominal) must be filled";
			$gotError = true;
		}
		if($transactionType==2 && ($amount == '' || $amountReplace == 0) && ($amountUnit == '' || $amountUnitReplace == 0)){
			$errorArr[4] = "Amount(Nominal) or Amount(Unit) must be filled";
			$gotError = true;
		}
		if($transactionType==2 && ($amount != '' && $amountReplace != 0) && ($amountUnit != '' && $amountUnitReplace != 0)){
			$errorArr[4] = "Fill only one field, Amount(Nominal) or Amount(Unit)";
			$gotError = true;
		}

		if($transactionType==2 && ($feePersen != '' && $feePersen != 0 && $feePersenReplace > 100)){
			$errorArr[9] = "Invalid Value of Fee (Persen)";
			$gotError = true;
		}
		
		if (!$gotError){
			
			$query = "INSERT INTO tbl_kr_subsredm_order (
					transaction_date
					, transaction_type 
                    , sa_code 
                    , investor_ac_no 
                    , fund_code 
                    , amount 
                    , amount_unit 
                    , amount_all_unit 
                    , fee
                    , fee_unit
                    , fee_persen 
					, redm_payment_ac_sequential_code
					, redm_payment_bank_bic_code
					, redm_payment_bank_bi_member_code
					, redm_payment_ac_no
                    , payment_date 
                    , transfer_type 
                    , sa_reference_no 
					, created_time
					, created_by
					, last_updated_time
					, last_updated_by
					, is_deleted
					, `type`
				)VALUES('$transactionDate',
				'$transactionType',
				'$saCode',
				'$investorAcNo',
				'$fundCode',
				'$amountReplace',
                '$amountUnitReplace',
                '$amountAllUnit',
                '$feeReplace',
                '$feeUnitReplace',
                '$feePersenReplace',
				'$redm_payment_ac_sequential_code',
				'$redm_payment_bank_bic_code',
				'$redm_payment_bank_bi_member_code',
				'$redm_payment_ac_no',
				'$paymentDate',
                '$transferType',
				'$saReferenceNo',
				now(),
				'".$_SESSION['pk_id']."',
				now(),
				'".$_SESSION['pk_id']."',
				'0', '$tipe')";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='subsredm_order.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}
$readonly = $transactionType == 1 ? 'readonly' : '';
$hidden = $transactionType == 1 ? 'none' : 'inline';

$tittle = "ADD - SUBS / REDM ORDER";
$dataRows = array (
	    'TEXT' => array(
			'Transaction Date <span class="redstar">*</span>',
			'Transaction Type <span class="redstar">*</span>',
			//'SA Code',
			'Invs. Fund Unit A/C No. <span class="redstar">*</span>',
			'Fund Code <span class="redstar">*</span>',
			'Amount (Nominal)',
			'Amount (Unit)',
			'Amount (All Unit)',
			'Fee',
			'Fee (Unit)',
			'Fee (%)',
			'REDM Payment',
			'REDM Payment A/C Sequential Code',
			'REDM Payment BANK BIC Code',
			'REDM Payment BANK BI Member Code',
			'REDM Payment A/C No',
			'<span id="labelPaymentDate" style="display:'.$hidden.';">Payment Date</span>',
			'<span id="labelTransferType" style="display:'.$hidden.';">Transfer Type</span>',
			'SA Reference No.'
			),
  	    'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':'),
	    'FIELD' => array (
            $data->datePicker('transactionDate', $transactionDate,''),
            "<select name=transactionType onchange='changeType(this.value)'><option value=1 ". ($transactionType == 1 ? "selected=selected" : "") .">Subscription</option><option value=2 ".($transactionType == 2 ? "selected=selected" : "").">Redemption</option></select>",
            //"<input type=text size='50' maxlength=5 name=saCode value='$saCode'>",
            //"<input type=text size='50' maxlength=16 name=investorAcNo value='$investorAcNo'>",
			$data->cb_ifua_all('investorAcNo', $investorAcNo, 'onchange="changeIfua(this.options[this.selectedIndex].getAttribute(\'ifuaid\'), this.options[this.selectedIndex].getAttribute(\'tipe\'))"') . "<input type='hidden' name='ifuaid' id='ifuaid' value='$ifua_id'><input type='hidden' name='tipe' id='tipe' value='$tipe'>",
            //"<input type=text size='50' maxlength=16 name=fundCode value='$fundCode'>",
			$data->cb_fundcode('fundCode', $fundCode),
            "<input type=text size='50' name=amount id=amount value='$amount' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
            "<input type=text size='50' name=amountUnit id=amountUnit value='$amountUnit' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' $readonly>",
            "<input type=checkbox name=amountAllUnit id=amountAllUnit value='Y' ".($amountAllUnit == 'Y' ? "checked" : "")." $readonly>",
            "<input type=text size='50' name=fee value='$fee' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
            "<input type=text size='50'  name=feeUnit value='$feeUnit' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
            "<input type=text size='50'  name=feePersen value='$feePersen' onkeydown='validate(event, this)' onkeyup='reformat(event, this)'>",
			$tipe == 0 ? $data->cb_ifua_redm('ifua_redm', $ifua_redm, $ifua_id, 'onchange="changeIfuaRedm(this.options[this.selectedIndex])"') : $data->cb_institusi_ifua_redm('ifua_redm', $ifua_redm, $ifua_id, 'onchange="changeIfuaRedm(this.options[this.selectedIndex])"'),
			"<input type=text size='50' maxlength=2 id=redm_payment_ac_sequential_code name=redm_payment_ac_sequential_code value='$redm_payment_ac_sequential_code' $readonly>",
			"<input type=text size='50' maxlength=11 id=redm_payment_bank_bic_code name=redm_payment_bank_bic_code value='$redm_payment_bank_bic_code' $readonly>",
			"<input type=text size='50' maxlength=17 id=redm_payment_bank_bi_member_code name=redm_payment_bank_bi_member_code value='$redm_payment_bank_bi_member_code' $readonly>",
			"<input type=text size='50' maxlength=30 id=redm_payment_ac_no name=redm_payment_ac_no value='$redm_payment_ac_no' $readonly>",
		    $data->datePicker('paymentDate', $paymentDate, "style='display:".$hidden.";'"),
		    "<select name=transferType id=transferType style='display:".$hidden.";'><option value=1 ". ($transferType == 1 ? "selected=selected" : "") .">SKNBI</option><option value=2 ".($transactionType == 2 ? "selected=selected" : "").">RTGS</option><option value=3 ".($transactionType == 3 ? "selected=selected" : "").">N/A</option></select>",
		    "<input type=text size='50' maxlength=30 name=saReferenceNo value='$saReferenceNo'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='subsredm_order.php';\">");

$javascript = "<script type='text/javascript'>
	var transactionType = $transactionType;
	function changeType(type){
		if(type == 1){
			var doc = document;
			doc.getElementById('amountUnit').value = '0';
			doc.getElementById('amountUnit').readOnly = true;
			doc.getElementById('amountAllUnit').checked = false;
			doc.getElementById('amountAllUnit').readOnly = true;
			doc.getElementById('redm_payment_ac_sequential_code').value = '';
			doc.getElementById('redm_payment_ac_sequential_code').readOnly = true;
			doc.getElementById('redm_payment_bank_bic_code').value = '';
			doc.getElementById('redm_payment_bank_bic_code').readOnly = true;
			doc.getElementById('redm_payment_bank_bi_member_code').value = '';
			doc.getElementById('redm_payment_bank_bi_member_code').readOnly = true;
			doc.getElementById('redm_payment_ac_no').value = '';
			doc.getElementById('redm_payment_ac_no').readOnly = true;
			doc.getElementById('labelPaymentDate').style.display = 'none';
			doc.getElementById('labelTransferType').style.display = 'none';
			doc.getElementById('paymentDate').style.display = 'none';
			doc.getElementById('transferType').style.display = 'none';
			doc.getElementById('ifua_redm').value = '';
		}else{
			var doc = document;
			doc.getElementById('amountUnit').readOnly = false;
			doc.getElementById('amountAllUnit').readOnly = false;
			doc.getElementById('redm_payment_ac_sequential_code').readOnly = false;
			doc.getElementById('redm_payment_bank_bic_code').readOnly = false;
			doc.getElementById('redm_payment_bank_bi_member_code').readOnly = false;
			doc.getElementById('redm_payment_ac_no').readOnly = false;
			doc.getElementById('labelPaymentDate').style.display = 'inline';
			doc.getElementById('labelTransferType').style.display = 'inline';
			doc.getElementById('paymentDate').style.display = 'inline';
			doc.getElementById('transferType').style.display = 'inline';
		}
		transactionType = type;
	}

	function changeIfua(ifuaid, tipe){
		document.getElementById('ifuaid').value = ifuaid;
		document.getElementById('tipe').value = tipe;
		document.getElementById('form1').submit();
	}
	function changeIfuaRedm(option){
		if(transactionType == 2){
			var sequential = option.getAttribute('sequentialcode');
			var biccode = option.getAttribute('biccode');
			var bimembercode = option.getAttribute('bimembercode');
			var acno = option.getAttribute('acno');

			document.getElementById('redm_payment_ac_sequential_code').value = sequential;
			document.getElementById('redm_payment_bank_bic_code').value = biccode;
			document.getElementById('redm_payment_bank_bi_member_code').value = bimembercode;
			document.getElementById('redm_payment_ac_no').value = acno;
		}
	}
</script>";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHCUSTOMJS' => $GLOBALS['JS'].'custom.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>