<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('report_limit_share.html');

if($_GET['cek']=='1'){


	$sql = "SELECT b.code, c.stk_name, FORMAT(a.lembar,0) AS LMBRVALCUR,FORMAT(a.total,0) AS total, b.code,d.allocation, a.`create_dt`,
			FORMAT((b.share*b.percent_max),0) AS LMBRVALMAX,FORMAT((b.share*b.percent_warning),0) AS LIMITWARNING,
			FORMAT(((a.lembar/b.share)*100),2) AS LMBRPCTCUR,
			FORMAT((b.percent_max*100),2) AS LMBRPCTMAX
			FROM tbl_kr_mst_saham a
			LEFT JOIN tbl_kr_limit_share b ON a.code = b.code
			LEFT JOIN (
				SELECT stk_cd, stk_name FROM tbl_kr_stock_rate GROUP BY stk_cd
			) c ON a.code = c.stk_cd
			LEFT JOIN tbl_kr_allocation d ON a.`allocation` = d.`pk_id`
			WHERE  a.`create_dt` IN (SELECT MAX(create_dt) FROM tbl_kr_mst_saham) AND a.lembar >= (b.share*b.percent_warning)
			ORDER BY a.allocation,a.code ASC";

	// $sql = 'SELECT b.code, c.stk_name, d.allocation,

	// FORMAT((b.share*b.percent_max),0) AS LMBRVALMAX,
	// FORMAT((b.share*b.percent_warning),0) AS LIMITWARNING, 
	// FORMAT(a.lembar,0) AS LMBRVALCUR,
	// FORMAT((b.percent_max*100),2) AS LMBRPCTMAX,
	// FORMAT(a.total,0) AS total, 
	// a.create_dt, 
	// FORMAT(((a.lembar/b.share)*100),2) AS LMBRPCTCUR 
	// -- a.allocation

	// FROM ( SELECT DISTINCT aa.code, (aa.lembar + COALESCE(bb.lembar,0)) AS lembar,
	// 	aa.harga_close, aa.avg,(aa.total + COALESCE(bb.total,0)) AS total ,
	// 	aa.allocation, aa.create_dt
	// 	FROM tbl_kr_mst_saham aa 
	// 	LEFT JOIN tbl_kr_mst_saham bb ON CONCAT(aa.CODE, "-W")=bb.code AND aa.allocation=bb.allocation WHERE  RIGHT(aa.CODE, 2) != "-W" GROUP BY aa.code, aa.allocation
	// ) a 
	// LEFT JOIN tbl_kr_limit_share b ON a.code = b.CODE 
	// LEFT JOIN (
	//  			SELECT stk_cd, stk_name FROM tbl_kr_stock_rate GROUP BY stk_cd
	//  		) c ON a.code = c.stk_cd
	// LEFT JOIN tbl_kr_allocation d ON a.`allocation` = d.`pk_id` 		
	// WHERE a.create_dt IN (
	// 		SELECT MAX(create_dt) 
	// 		FROM tbl_kr_mst_saham) AND a.lembar >= (b.share*b.percent_warning)
 // 	ORDER BY a.allocation,a.code ASC';




	$DG=$data->dataGridLimit($sql,'pn_id',$data->ResultsPerPage,$pg,'');
}
else{	
	if($data->auth_boolean(1716,$_SESSION['pk_id'])){
		return header('location:report_limit_share.php?cek=1');
	}
}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);

$tmpl->addVar('page','NODATA',$NODATA);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>