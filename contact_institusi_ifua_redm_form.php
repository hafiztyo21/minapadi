<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('contact_institusi_ifua_redm_form.html');

$id = 0;
$ifua_id = '';  
$bic_code = '';       
$bi_member_code = '';
$bank_name = '';
$bank_country = '';
$bank_branch = '';
$acc_name = '';
$acc_ccy = '';
$acc_no	= '';           

$errorArr = array();
for($i=0;$i < 8;$i++)
	$errorArr[$i] = '';
$otherError='';

if($_GET['edit'] == 1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_cus_institusi_ifua_redm WHERE redm_id = $id";
    $result = $data->get_row($query);

    $ifua_id = $result['ifua_id'];     
    $bic_code = $result['bic_code'];
    $bi_member_code	= $result['bi_member_code'];
	$bank_name	= $result['bank_name'];
	$bank_country	= $result['bank_country'];
	$bank_branch	= $result['bank_branch'];
	$acc_name	= $result['ac_name'];
	$acc_ccy	= $result['ac_ccy'];    
    $acc_no = $result['ac_no'];
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
 		$ifua_id = trim(htmlentities($_POST['ifua_id']));
		$bic_code = trim(htmlentities($_POST['bic_code']));
		$bi_member_code	= trim(htmlentities($_POST['bi_member_code']));
		$bank_name	= trim(htmlentities($_POST['bank_name']));
		$bank_branch	= trim(htmlentities($_POST['bank_branch']));
		$bank_country	= trim(htmlentities($_POST['bank_country']));
		$acc_ccy	= trim(htmlentities($_POST['acc_ccy']));
		$acc_name	= trim(htmlentities($_POST['acc_name']));
        $acc_no	= trim(htmlentities($_POST['acc_no']));
        
		$gotError = false;
        //validation
		if($ifua_id=='' || $ifua_id=='0' || $ifua_id==0){
			$errorArr[0] = "Select Ifua";
			$gotError = true;
		}
		if($bic_code=='' && $bi_member_code == ''){
			$errorArr[1] = "BIC Code or BI Member Code must be filled";
			$gotError = true;
		}
		if($bank_name == ''){
			$errorArr[3] = "Bank Name must be filled";
			$gotError = true;
		}
		if($bank_country == ''){
			$errorArr[4] = "Bank Country must be filled";
			$gotError = true;
		}
		if($acc_ccy == ''){
			$errorArr[6] = "Account Currency must be filled";
			$gotError = true;
		}
		if($acc_name == ''){
			$errorArr[7] = "Account Name must be filled";
			$gotError = true;
		}
		if($acc_no == ''){
			$errorArr[8] = "Account Number must be filled";
			$gotError = true;
		}
        
		if (!$gotError){
			if($id == 0){

                $query = "SELECT ifnull(max(sequential_code),0) as counter FROM tbl_kr_cus_institusi_ifua_redm WHERE ifua_id = '$ifua_id' AND is_deleted=0";
                
                $row = $data->get_row($query);
                
				$sequential = intval($row['counter']) + 1;
                

                $query = "INSERT INTO tbl_kr_cus_institusi_ifua_redm (
                        ifua_id
                        , sequential_code 
                        , bic_code
                        , bi_member_code
						, bank_name
						, bank_country
						, bank_branch
						, ac_ccy
						, ac_name 
                        , ac_no 
                        , created_date
                        , created_by
                        , last_updated_date
                        , last_updated_by
                        , is_deleted
                    )VALUES('$ifua_id',
                    '$sequential',
                    '$bic_code',
                    '$bi_member_code',
					'$bank_name',
					'$bank_country',
					'$bank_branch',
					'$acc_ccy',
					'$acc_name',
                    '$acc_no',
                    now(),
                    '".$_SESSION['pk_id']."',
                    now(),
                    '".$_SESSION['pk_id']."',
                    '0')";
            }else{
                $query = "UPDATE tbl_kr_cus_institusi_ifua_redm SET
                        ifua_id = '$ifua_id',
                        bic_code = '$bic_code',
                        bi_member_code = '$bi_member_code',
                        ac_no = '$acc_no',
						ac_name = '$acc_name',
						ac_ccy = '$acc_ccy',
						bank_name = '$bank_name',
						bank_branch = '$bank_branch',
						bank_country = '$bank_country',
                        last_updated_date = now(),
                        last_updated_by = '".$_SESSION['pk_id']."'
                    WHERE redm_id = '$id'";
            }
			
			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='contact_institusi_ifua_redm.php?view=1';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}
$readonly = $transactionType == 1 ? 'readonly' : '';

$tittle = ($id == 0? "ADD" : "EDIT")." - CONTACT INSTITUSI REDM PAYMENT";
$dataRows = array (
	    'TEXT' => array(
			'IFUA <span class="redstar">*</span>',
			'BIC Code ',
			'BI Member Code ',
			'Bank Name <span class="redstar">*</span>',
			'Bank Country <span class="redstar">*</span>',
			'Bank Branch',
			'Account Currency <span class="redstar">*</span>',
			'Account Name <span class="redstar">*</span>',
			'Account Number <span class="redstar">*</span>'
			),
  	    'DOT'  => array (':',':',':',':'),
	    'FIELD' => array (
            $data->cb_ifua_ins_id('ifua_id', $ifua_id, '')."<input type='hidden' name='inputId' id='inputId' value='$id'>",
            "<input type=text size='50' name=bic_code id=bic_code value='$bic_code'>",
            //"<input type=text size='50' name=bi_member_code id=bi_member_code value='$bi_member_code' >",
			$data->cb_bimembercode('bi_member_code', $bi_member_code, 'onchange="changeBi1(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
			"<input type=text size='50' name=bank_name id=bank_name value='$bank_name'>",
			$data->cb_isocountry('bank_country', $bank_country,''),
			"<input type=text size='50' name=bank_branch id=bank_branch value='$bank_branch'>",
			$data->cb_accountccy('acc_ccy',$acc_ccy,''),
			"<input type=text size='50' name=acc_name id=acc_name value='$acc_name'>",
            "<input type=text size='50' name=acc_no id=acc_no value='$acc_no'>"
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='contact_institusi_ifua_redm.php?view=1';\">");

$javascript = "
<script type='text/javascript'>
	function changeBi1(name, account){
		document.getElementById('bank_name').value = name;
		
	}
</script>	
";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>