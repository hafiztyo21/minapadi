<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('si_equity_offshore_form.html');

$id = 0;
$type = '1';    //offshore
$trade_id = '';
$trade_date = date('Y-m-d');
$settlement_date = date('Y-m-d');
$im_code = 'MU002';
$br_code = '';
$br_name = '';
$counterparty_code = '';
$counterparty_name = '';
$place_of_settlement = '';
$fund_code = '';
$security_type = '1';
$security_code_type = '2';
$security_code = '';
$security_name = '';
$buy_sell = '1';
$ccy = '';
$price = 0;
$quantity = 0;
$trade_amount = 0;
$commission=0;
$sales_tax = 0;
$levy = 0;
$vat = 0;
$other_charges = 0;
$gross_settlement_amount =0;
$wht_on_commission=0;
$net_settlement_amount = 0;
$instruction_type = '1';
$remarks = '';

$errorArr = array();
for($i =0; $i<28;$i++){
    $errorArr[$i] = '';
}
$otherError='';

if($_GET['edit'] == 1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_si_equity WHERE si_equity_id='$id'";
    $result = $data->get_row($query);

    $trade_id = $result['trade_id'];
    $trade_date = $result['trade_date'];
    $settlement_date = $result['settlement_date'];
    $br_code = $result['br_code'];
    $br_name = $result['br_name'];
    $counterparty_code = $result['counterparty_code'];
    $counterparty_name = $result['counterparty_name'];
    $place_of_settlement = $result['place_of_settlement'];
    $fund_code = $result['fund_code'];
    $security_type = $result['security_type'];
    $security_code_type = $result['security_code_type'];
    $security_code = $result['security_code'];
    $security_name = $result['security_name'];
    $buy_sell = $result['buy_sell'];
    $ccy = $result['ccy'];
    $price = $result['price'];
    $quantity = $result['quantity'];
    $trade_amount = $result['trade_amount'];
    $commission= $result['commission'];
    $sales_tax = $result['sales_tax'];
    $levy = $result['levy'];
    $vat = $result['vat'];
    $other_charges = $result['other_charges'];
    $gross_settlement_amount = $result['gross_settlement_amount'];
    $wht_on_commission = $result['wht_on_commission'];
    $net_settlement_amount = $result['net_settlement_amount'];
    $instruction_type = $result['instruction_type'];
    $remarks = $result['remarks'];

}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
        $trade_id = trim(htmlentities($_POST['trade_id']));
        $trade_date = trim(htmlentities($_POST['trade_date']));
        $settlement_date = trim(htmlentities($_POST['settlement_date']));
        //$im_code = 'MU002';
        $br_code = trim(htmlentities($_POST['br_code']));
        $br_name = trim(htmlentities($_POST['br_name']));
        $counterparty_code = trim(htmlentities($_POST['counterparty_code']));
        $counterparty_name = trim(htmlentities($_POST['counterparty_name']));
        $place_of_settlement = trim(htmlentities($_POST['place_of_settlement']));
        $fund_code = trim(htmlentities($_POST['fund_code']));
        $security_type = trim(htmlentities($_POST['security_type']));
        $security_code_type = trim(htmlentities($_POST['security_code_type']));
        $security_code = trim(htmlentities($_POST['security_code']));
        $security_name = trim(htmlentities($_POST['security_name']));
        $buy_sell = trim(htmlentities($_POST['buy_sell']));
        $ccy = trim(htmlentities($_POST['ccy']));
        $price = trim(htmlentities($_POST['price']));
        $quantity = trim(htmlentities($_POST['quantity']));
        $trade_amount = floatval($price) * floatval($quantity);
        $commission = trim(htmlentities($_POST['commission']));
        $sales_tax = trim(htmlentities($_POST['sales_tax']));
        $levy = trim(htmlentities($_POST['levy']));
        $vat = trim(htmlentities($_POST['vat']));
        $other_charges = trim(htmlentities($_POST['other_charges']));
        if(intval($buy_sell) == 1){
            $gross_settlement_amount = $trade_amount + floatval($commission) + floatval($levy) + floatval($sales_tax) + floatval($vat) + floatval($other_charges);
        }else{
            $gross_settlement_amount = $trade_amount - floatval($commission) - floatval($levy) - floatval($sales_tax) - floatval($vat) - floatval($other_charges);
        }
        //$gross_settlement_amount = trim(htmlentities($_POST['gross_settlement_amount']));
        $wht_on_commission= trim(htmlentities($_POST['wht_on_commission']));
        if(intval($buy_sell) == 1){
            $net_settlement_amount = $gross_settlement_amount - floatval($wht_on_commission);
        }else{
            $net_settlement_amount = $gross_settlement_amount + floatval($wht_on_commission);
        }
        //$net_settlement_amount = trim(htmlentities($_POST['net_settlement_amount']));
        $instruction_type = trim(htmlentities($_POST['instruction_type']));
        $remarks = trim(htmlentities($_POST['remarks']));
		
		$gotError = false;
		if($trade_id==''){
			$errorArr[0] = "Trade Id must be filled";
			$gotError = true;
		}
		/*if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}*/
        if($br_code==''){
			$errorArr[3] = "BR Code must be filled";
			$gotError = true;
		}
        if($br_name==''){
			$errorArr[4] = "BR Name must be filled";
			$gotError = true;
		}
        if($place_of_settlement==''){
			$errorArr[7] = "Place of Settlement must be filled";
			$gotError = true;
		}
        if($fund_code==''){
			$errorArr[8] = "Fund Code must be filled";
			$gotError = true;
		}
        if($security_code==''){
			$errorArr[11] = "Security Code must be filled";
			$gotError = true;
		}
        if($security_name==''){
			$errorArr[12] = "Security Name must be filled";
			$gotError = true;
		}
        if($price==''){
			$errorArr[15] = "Price must be filled";
			$gotError = true;
		}
        if($quantity==''){
			$errorArr[16] = "Quantity must be filled";
			$gotError = true;
		}
		
        
		if (!$gotError){
			if($id == 0){
                $query = "INSERT INTO tbl_kr_si_equity (
                        `type`,
                        trade_id,
                        trade_date,
                        settlement_date,
                        im_code,
                        br_code,
                        br_name,
                        counterparty_code,
                        counterparty_name,
                        place_of_settlement,
                        fund_code,
                        security_type,
                        security_code_type,
                        security_code,
                        security_name,
                        buy_sell,
                        ccy,
                        price,
                        quantity,
                        trade_amount,
                        commission,
                        sales_tax,
                        levy,
                        vat,
                        other_charges,
                        gross_settlement_amount,
                        wht_on_commission,
                        net_settlement_amount,
                        instruction_type,
                        remarks,
                        created_date,
                        created_by,
                        last_updated_date,
                        last_updated_by,
                        is_deleted
                    )VALUES('$type',
                    '$trade_id',
                    '$trade_date',
                    '$settlement_date',
                    '$im_code',
                    '$br_code',
                    '$br_name',
                    '$counterparty_code',
                    '$counterparty_name',
                    '$place_of_settlement',
                    '$fund_code',
                    '$security_type',
                    '$security_code_type',
                    '$security_code',
                    '$security_name',
                    '$buy_sell',
                    '$ccy',
                    '$price',
                    '$quantity',
                    '$trade_amount',
                    '$commission',
                    '$sales_tax',
                    '$levy',
                    '$vat',
                    '$other_charges',
                    '$gross_settlement_amount',
                    '$wht_on_commission',
                    '$net_settlement_amount',
                    '$instruction_type',
                    '$remarks',
                    now(),
                    '".$_SESSION['pk_id']."',
                    now(),
                    '".$_SESSION['pk_id']."',
                    '0')";
            }else{
                $query = "UPDATE tbl_kr_si_equity SET 
                    trade_id = '$trade_id',
                    trade_date= '$trade_date',
                    settlement_date= '$settlement_date',
                    
                    br_code= '$br_code',
                    br_name= '$br_name',
                    counterparty_code= '$counterparty_code',
                    counterparty_name= '$counterparty_name',
                    place_of_settlement= '$place_of_settlement',
                    fund_code= '$fund_code',
                    security_type= '$security_type',
                    security_code_type= '$security_code_type',
                    security_code= '$security_code',
                    security_name= '$security_name',
                    buy_sell= '$buy_sell',
                    ccy= '$ccy',
                    price= '$price',
                    quantity= '$quantity',
                    trade_amount= '$trade_amount',
                    commission= '$commission',
                    sales_tax= '$sales_tax',
                    levy= '$levy',
                    vat= '$vat',
                    other_charges= '$other_charges',
                    gross_settlement_amount= '$gross_settlement_amount',
                    wht_on_commission= '$wht_on_commission',
                    net_settlement_amount= '$net_settlement_amount',
                    instruction_type= '$instruction_type',
                    remarks= '$remarks',
                    last_updated_date= now(),
                    last_updated_by= '".$_SESSION['pk_id']."'
                    WHERE si_equity_id = '$id'
                ";
            }
			

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='si_equity_offshore.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = ($id == '0' ? "ADD" : "EDIT")." - SI EQUITY OFFSHORE";
$dataRows = array (
	    'TEXT' => array('Trade ID'
            ,'Trade Date'
            ,'Settlement Date'
            ,'BR Code'
            ,'BR Name'
            ,'Counterparty Code'
            ,'Counterparty Name'
            ,'Place of Settlement'
            ,'Fund Code'
            ,'Security Type'
            ,'Security Code Type'
            ,'Security Code'
            ,'Security Name'
            ,'Buy/Sell'
            ,'CCY'
            ,'Price'
            ,'Qty'
            ,'Trade Amount'
            ,'Commission'
            ,'Sales Tax'
            ,'Levy'
            ,'VAT'
            ,'Other Charges'
            ,'Gross Settlement Amount'
            ,'WHT on Commission'
            ,'Net Settlement Amount'
            ,'Instruction Type'
            ,'Remarks'
            ),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            "<input type=hidden id=inputId name=inputId value='$id'>
            <input type=text maxlength=20 size=20 id=trade_id name=trade_id value='$trade_id'>",
		    $data->datePicker('trade_date', $trade_date,''),
            $data->datePicker('settlement_date', $settlement_date,''),
            "<input type=text maxlength=20 size=20 id=br_code name=br_code value='$br_code'>",
            "<input type=text maxlength=100 size=50 id=br_name name=br_name value='$br_name'>",
            "<input type=text maxlength=20 size=20 id=counterparty_code name=counterparty_code value='$counterparty_code'>",
            "<input type=text maxlength=100 size=50 id=counterparty_name name=counterparty_name value='$counterparty_name'>",
            "<input type=text maxlength=11 size=15 id=place_of_settlement name=place_of_settlement value='$place_of_settlement'>",
            $data->cb_fundcode('fund_code', $fund_code,''),
            $data->cb_securitytype('security_type', $security_type, true, ''),
            $data->cb_securitycodetype('security_code_type', $security_code_type, false,''),
            "<input type=text maxlength=35 size=50 id=security_code name=security_code value='$security_code'>",
            "<input type=text maxlength=100 size=50 id=security_name name=security_name value='$security_name'>",
            $data->cb_buysell('buy_sell', $buy_sell,'onchange="calc_gross()"'),
            $data->cb_accountccy('ccy', $ccy,''),
            "<input type=number id=price name=price value='$price'  onkeyup='calc_trade_amount()' step='0.01'>",
            "<input type=number id=quantity name=quantity value='$quantity' onkeyup='calc_trade_amount()' step='0.01'>",
            "<input type=number id=trade_amount name=trade_amount value='$trade_amount' readonly step='0.01'>",
            "<input type=number id=commission name=commission value='$commission' onkeyup='calc_trade_amount()' step='0.01'>",
            "<input type=number id=sales_tax name=sales_tax value='$sales_tax' onkeyup='calc_trade_amount()' step='0.01'>",
            "<input type=number id=levy name=levy value='$levy' onkeyup='calc_trade_amount()' step='0.01'>",
            "<input type=number id=vat name=vat value='$vat' onkeyup='calc_trade_amount()' step='0.01'>",
            "<input type=number id=other_charges name=other_charges value='$other_charges' onkeyup='calc_trade_amount()' step='0.01'>",
            "<input type=number id=gross_settlement_amount name=gross_settlement_amount value='$gross_settlement_amount' readonly step='0.01'>",
            "<input type=number id=wht_on_commission name=wht_on_commission value='$wht_on_commission' onkeyup='calc_nett()' step='0.01'>",
            "<input type=number id=net_settlement_amount name=net_settlement_amount value='$net_settlement_amount' readonly step='0.01'>",
            $data->cb_instructiontype('instruction_type', $instruction_type,''),
            "<input type=text size=50 id=remarks name=remarks value='$remarks'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='si_equity_offshore.php';\">");

$javascript = "
    <script type='text/javascript'>
        function calc_trade_amount(){
            var price = document.getElementById('price').value;
            var quantity = document.getElementById('quantity').value;
            document.getElementById('trade_amount').value = parseFloat(price) * parseFloat(quantity);
            calc_gross();
        }

        function calc_gross(){
            var trade_amount = document.getElementById('trade_amount').value;
            var commission = document.getElementById('commission').value;
            var levy = document.getElementById('levy').value;
            var sales_tax = document.getElementById('sales_tax').value;
            var vat = document.getElementById('vat').value;
            var other_charges = document.getElementById('other_charges').value;
            var buy_sell = document.getElementById('buy_sell').value;
            if(buy_sell == 1){
                document.getElementById('gross_settlement_amount').value = parseFloat(trade_amount) + parseFloat(commission) + parseFloat(levy) + parseFloat(sales_tax) + parseFloat(vat) + parseFloat(other_charges);
            }else{
                document.getElementById('gross_settlement_amount').value = parseFloat(trade_amount) - parseFloat(commission) - parseFloat(levy) - parseFloat(sales_tax) - parseFloat(vat) - parseFloat(other_charges);
            }
            calc_nett();
        }

        function calc_nett(){
            var gross = document.getElementById('gross_settlement_amount').value;
            var wht = document.getElementById('wht_on_commission').value;
            var buy_sell = document.getElementById('buy_sell').value;
            if(buy_sell == 1){
                document.getElementById('net_settlement_amount').value = parseFloat(gross) - parseFloat(wht);
            }else{
                document.getElementById('net_settlement_amount').value = parseFloat(gross) + parseFloat(wht);
            }
        }
    </script>
";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path','javascript',$javascript );
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>