<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$filename = $transaction_date."_si_fixed_income_offshore.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    trade_id,
    DATE_FORMAT(trade_date,'%Y%m%d') as trade_date,
    DATE_FORMAT(settlement_date,'%Y%m%d') as settlement_date,
    im_code,
    br_code,
    br_name,
    counterparty_code,
    counterparty_name,
    place_of_settlement,
    fund_code,
    security_type,
    security_code_type,
    security_code,
    security_name,
    buy_sell,
    ccy,
    price,
    face_value,
    proceeds,
    interest_rate,
    DATE_FORMAT(maturity_date,'%Y%m%d') as maturity_date,
    DATE_FORMAT(last_coupon_date,'%Y%m%d') as last_coupon_date,
    DATE_FORMAT(next_coupon_date,'%Y%m%d') as next_coupon_date,
    accrued_days,
    accrued_interest_amount,
    other_fee,
    net_proceeds,
    instruction_type,
    purpose_of_transaction,
    remarks
 
    FROM tbl_kr_si_fixed_income
    WHERE DATE_FORMAT(trade_date, '%Y-%m-%d') = '$transaction_date' 
    AND is_deleted=0 AND type = '1'";

$rows = $data->get_rows2($query);

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $arr = array();
    $var = $rows[$i];

    $idx = 0;
    $arr[$idx] = $var['trade_id'];                                               $idx++;     //0.
    $arr[$idx] = $var['trade_date'];                                                $idx++;
    $arr[$idx] = $var['settlement_date'];                                       $idx++;

    $arr[$idx] = $var['im_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['br_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['br_name'];                                               $idx++;     //0.
    $arr[$idx] = $var['counterparty_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['counterparty_name'];                                               $idx++;     //0.
    $arr[$idx] = $var['place_of_settlement'];                                               $idx++;     //0.
    $arr[$idx] = $var['fund_code'];                                             $idx++;     //0.
    $arr[$idx] = $var['security_type'];                                             $idx++;     //0.
    $arr[$idx] = $var['security_code_type'];                                             $idx++;     //0.
    $arr[$idx] = $var['security_code'];                                         $idx++;     //0.
    $arr[$idx] = $var['security_name'];                                         $idx++;     //0.
    $arr[$idx] = $var['buy_sell'];                                            $idx++;     //0.
    $arr[$idx] = $var['ccy'];                                            $idx++;     //0.
    $arr[$idx] = $var['price'];                                                 $idx++;     //0.
    $arr[$idx] = $var['face_value'];                                                   $idx++;     //0.
    $arr[$idx] = $var['proceeds'];                                          $idx++;     //0.
    $arr[$idx] = $var['interest_rate'];                                            $idx++;     //0.
    $arr[$idx] = $var['maturity_date'];                                             $idx++;     //0.
    $arr[$idx] = $var['last_coupon_date'];                                                  $idx++;     //0.
    $arr[$idx] = $var['next_coupon_date'];                                                   $idx++;     //0.
    $arr[$idx] = $var['accrued_days'];                                         $idx++;     //0.
    $arr[$idx] = $var['accrued_interest_amount'];                               $idx++;     //0.
    $arr[$idx] = $var['other_fee'];                                     $idx++;     //0.
    $arr[$idx] = $var['net_proceeds'];                                 $idx++;     //0.
    $arr[$idx] = $var['instruction_type'];                                       $idx++;     //0.
    $arr[$idx] = $var['purpose_of_transaction'];                                       $idx++;     //0.
    $arr[$idx] = $var['remarks'];                                               $idx++;     //0.

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
}

fclose($output);
?>
