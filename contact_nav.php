<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'user.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new user;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>

</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellspacing="0" cellpadding="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<?php if($data->auth_boolean(2311,$_SESSION['pk_id'])){  ?>
<li> <a href="contact.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Customer / Client Personal</a></li>
<?php } ?>
<?php if($data->auth_boolean(2312,$_SESSION['pk_id'])){  ?>
<li> <a href="contact_institusi.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Customer / Client Institusi</a></li>
<?php } ?>
<?php if($data->auth_boolean(2313,$_SESSION['pk_id'])){  ?>
<li> <a href="contact_ifua_redm.php?view=1" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Customer / Client Personal Redm Payment</a></li>
<?php } ?>
<?php if($data->auth_boolean(2314,$_SESSION['pk_id'])){  ?>
<li> <a href="contact_institusi_ifua_redm.php?view=1" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Customer / Client Institusi Redm Payment</a></li>
<?php } ?>
<?php if($data->auth_boolean(2315,$_SESSION['pk_id'])){  ?>
<li> <a href="contact_ifua_balance.php?view=1" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Customer / Client IFUA Balance</a></li>
<?php } ?>
<?php //if($data->auth_boolean(1031,$_SESSION['pk_id'])){  ?>
<!--<li> <a href="so.php" target="contentTabFrame" onClick="activeLink(this);">SO</a></li>-->
<?php //} ?>
</ul>
</td>
</tr>
 <tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</table>
</body>
</html>
