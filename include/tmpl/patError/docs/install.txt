/**
 * patError - simple and powerful error managemet system
 * 
 * $Id: install.txt 33 2004-09-25 08:57:01Z gerd $
 *
 * @access      public
 * @package     patError
 * @todo        more detailed information
 */
 
patError Installation
=====================
This is a very short document, that tries to help you setting up patError
for basic usage. Please read the readme.txt for further information.

INSTALLATION:
=============

Manual installation
-------------------
patError is a very small package that basically consits of two classes
seperated in two files in the include-directory:

    patError.php
    patErrorManager.php

Just copy these files to your include-directory. 

Install the PEAR package
------------------------
Download the PEAR package file from the project homepage and install it, by 
typing the command: 

 pear install <filename>
 
After installation, the patErrorManager can be included with:
  include_once 'pat/patErrorManager.php';
 
 
TESTING:
========
For testing patError, include "patErrorManager.php" and call the static
methods: Example:
<?PHP
    include_once 'include/patErrorManager.php';
    patErrorManager::raiseError( 'test:111', 'Test', 'Testing patErrorManager' );
?>

Notice, that patError.php will be included automatically! 

Have fun, gERD
Viel Spass am Ger�t, gERD

