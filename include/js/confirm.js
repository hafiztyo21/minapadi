function confirmDelete()
{
    return confirm("Are you sure you wish to delete this entry?");
}

function confirmCancel()
{
    return confirm("Are you sure you wish to cancel this entry?");
}

function confirmReturn()
{
    return confirm("Are you sure you wish to return this entry?");
}

function closePage()
{
	var browser=navigator.appName;
    if(browser.indexOf("Microsoft")>0){
		window.parent.close();
	}else{
		window.parent.close();
	}
}