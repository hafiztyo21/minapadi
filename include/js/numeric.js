//<input id="myInput" onKeyUp="addComma(this)" style="text-align:right">

    function addComma(inObj){
        outStr =inObj.value.replace(/,/g, "");
        leftStr = outStr
        rightStr = ""
        if (outStr.indexOf(".") > -1){
            leftStr = outStr.substr(0, outStr.indexOf("."))
            rightStr = outStr.substr(outStr.indexOf(".")+1)
        }
        if (leftStr.length > 3){
            numCommas = Math.floor(( leftStr.length - 1 ) / 3 );
            tempStr = ""
            for (x=0; x<numCommas; x++){
                tempStr =  "," + leftStr.substr(leftStr.length-3) + tempStr
                leftStr = leftStr.substr(0,leftStr.length-3)
            }
             leftStr += tempStr
        }
        outStr = outStr.indexOf(".") > -1 ? leftStr + "." + rightStr : leftStr
        inObj.value = outStr        
    }
	
	/*
 * �zay AKDORA
 * 05.01.2009
 */
 
 /*
 *  using by function ThausandSeperator!!
 */
 function removeCharacter(v, ch)
 {
  var tempValue = v+"";
  var becontinue = true;
  while(becontinue == true)
  {
   var point = tempValue.indexOf(ch);
   if( point >= 0 )
   {
    var myLen = tempValue.length;
    tempValue = tempValue.substr(0,point)+tempValue.substr(point+1,myLen);
    becontinue = true;
   }else{
    becontinue = false;
   }
  }
  return tempValue;
 }
 
 /*
 *  using by function ThausandSeperator!!
 */
 function characterControl(value)
 {
  var tempValue = "";
  var len = value.length;
  for(i=0; i<len; i++)
  {
   var chr = value.substr(i,1);
   if( (chr < '0' || chr > '9') && chr != '.' && chr != ',' )
   {
    chr = '';
   }
   
   tempValue = tempValue + chr;
  }
  return tempValue;
 }
 
 /*
 * Automaticly converts the value in the textbox in a currency format with
 * thousands seperator and decimal point
 *
 * @param value : the input text
 * @param digit : decimal number after comma
 */
 function ThausandSeperator(value, digit)
 {
  //var thausandSepCh = ".";
  //var decimalSepCh = ",";
  
  var thausandSepCh = ",";
  var decimalSepCh = ".";
  
  
  var tempValue = "";
  var realValue = value+"";
  var devValue = "";
  realValue = characterControl(realValue);
  var comma = realValue.indexOf(decimalSepCh);
  if(comma != -1 )
  {
   tempValue = realValue.substr(0,comma);
   devValue = realValue.substr(comma);
   devValue = removeCharacter(devValue,thausandSepCh);
   devValue = removeCharacter(devValue,decimalSepCh);
   devValue = decimalSepCh+devValue;
   if( devValue.length > 3)
   {
    devValue = devValue.substr(0,3);
   }
  }else{
   tempValue = realValue;
  }

 tempValue = removeCharacter(tempValue,thausandSepCh);
   
  var result = "";
  var len = tempValue.length;
  while (len > 3){
  result = thausandSepCh+tempValue.substr(len-3,3)+result;
  len -=3;
 }
 result = tempValue.substr(0,len)+result;
 return result+devValue;
 }
 
 /*
 * Automaticly converts the value in the textbox to upper
 * and it also converts the Turkish characters to the closest letter in the alphabet
 *
 * @param value : the input text
 * 
 */
 function allCharsToUpper(value)
 {
  var tempValue = "";
  var len = value.length;
  for(i=0; i<len; i++)
  {
   var chr = value.substr(i,1);
   chr = chr.toUpperCase();
   if( chr == 'I') {chr = 'I'; }
   else if( chr == 'G') {chr = 'G';}
   else if( chr == '�') {chr = 'O';}
   else if( chr == '�') {chr = 'U';}
   else if( chr == 'S') {chr = 'S';}
   else if( chr == '�') {chr = 'C';}
     
   tempValue = tempValue + chr;
  }
  return tempValue;
 }

/*
Usage of this funcitons:

 <TABLE border="1">
   <tr>    
    <td width="150px">1. Only Number</td>
    <td>
   <input type="text" name="xxxx" maxlength="20" size="40" onkeypress= 'javascript:if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;'/>
    </td>
  </tr>
  
  <tr>    
    <td width="150px">2. Curreny</td>
    <td>
   <input type="text" name="xxxx" maxlength="20" size="40" onkeyup= 'this.value = ThausandSeperator(this.value,2);' />
    </td>
  </tr>
  
  <tr>    
    <td width="150px">3. To Upper</td>
    <td>
   <input type="text" name="xxxx" maxlength="20" size="40" onkeyup= 'this.value = allCharsToUpper(this.value);'/>
    </td>
  </tr>
 </TABLE>
  */  
