var myBrowser;
var staticMenu;
var floaterHeight = -1;

function CreateStaticMenu(theObj, x, y)
{
	myBrowser = new xBrowser();
	//this floater height is the height of the floater
	//floaterHeight = 500;
	floaterHeight = 700;
	staticMenu = new xLayerFromObj(theObj);
	staticMenu.baseX = x;
	staticMenu.baseY = y;
	staticMenu.x = x;
	staticMenu.y = y;
	staticMenu.moveTo(x,y);
	staticMenu.show();
	setInterval("ani()", 20);
}

function ani()
{
	var b = staticMenu;
//	var targetX = myBrowser.getMinX() + b.baseX;
//	var targetY = myBrowser.getMinY() + b.baseY;
	var targetY; 
	if (floaterHeight == -1)
	{
		targetY = myBrowser.getMinY() + b.baseY;
	}
	else
	{
		if (myBrowser.getCanvasHeight() <= (floaterHeight + b.baseY))
		{
			if (myBrowser.getMinY() <= b.baseY)
			{
				targetY = myBrowser.getMinY() + b.baseY;
			}
			else
			{
				targetY = myBrowser.getMaxY() - floaterHeight;
			}
		}
		else
		{
			targetY = myBrowser.getMinY() + b.baseY;
		}
	}

//	var dx = (targetX - b.x)/8;
	var dy = (targetY - b.y)/8;
//	b.x += dx;
	b.y += dy;
//alert(' Target y ' + targetY);
	b.moveTo(b.x, b.y);
}

function MM_showHideLayers() { //v6.0
		var i,p,v,obj,args=MM_showHideLayers.arguments;
		for (i=0; i<(args.length-2); i+=3) 
			if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
			if (obj.style) { obj=obj.style; v=(v=='inline')?'inline':(v=='none')?'none':v; }
			obj.display=v; }
	}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}