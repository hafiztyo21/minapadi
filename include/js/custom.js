function validate(e, control){
    var keycode = e.which || e.keyCode;
    if((keycode < 48 || keycode > 57) && (keycode < 96 || keycode > 105) && keycode != 8 && keycode != 190 && keycode != 37 && keycode != 39 && keycode != 9){
        
        e.preventDefault();
        e.stopPropagation();
    }
    if(keycode == 190 && control.value.includes(".")){
        e.preventDefault();
        e.stopPropagation();
    }
}
function reformat(e, control){
    
    var value = control.value;
    value = value.replace(/,/g, '');

    var arr = value.split(".");
    var rslt = "";
    var counter = 0;
    for(var i=arr[0].length-1;i>=0;i--){
        rslt = arr[0].substring(i,i+1) + rslt;
        counter++;
        if(counter == 3){
            rslt =","+rslt;
            counter = 0;
        }
    }
    if(arr.length > 1)
        rslt = rslt + "."+arr[1];
    if(rslt.substring(0,1) == ",")
        rslt = rslt.substring(1);
    control.value = rslt;
}

function reformat2(amount){
    
    var value = amount;
    if(isNaN(value))
        return 0;
    value = value.toString().replace(/,/g, '');

    var arr = value.split(".");
    var rslt = "";
    var counter = 0;
    for(var i=arr[0].length-1;i>=0;i--){
        rslt = arr[0].substring(i,i+1) + rslt;
        counter++;
        if(counter == 3){
            rslt =","+rslt;
            counter = 0;
        }
    }
    if(arr.length > 1)
        rslt = rslt + "."+arr[1];
    if(rslt.substring(0,1) == ",")
        rslt = rslt.substring(1);
    return rslt;
}