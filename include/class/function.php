<?PHP

/**
* Function for input value
* Copyright by Harfiq Elnanda/Vyxell
* 			   08174124217
*              h_el_n@yahoo.com
* Version 1.0.0 Yogyakarta 2006-04-15
**/

//daftar variable lengkap
$bulan = array(1=> 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
$bulan_id = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
$bulan_short_id = array(1 => 'Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des');
$bulan = array(1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$bulan_short = array(1 => 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
$hari = array (0 => 'Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');

//format indo to mysql format
function date_for_mysql($ref) {
	$tanggal = explode("-",$ref);
	return $tanggal[2]."-".$tanggal[1]."-".$tanggal[0];
}

//format complate date
function date_for_ind($ref){
	global $bulan;
	$data = explode('-',$ref);
	return $data[2]." ".$bulan[abs($data[1])]." ".$data[0];
}
//for drop down selected
function buffered_select ($varname, $drop, $default = '', $header = ' class="inputflat"', $header_option = '')
{
    $_buffer = '';
    if ((!empty($varname)) && is_array($drop)) {
        $_buffer .= '<select name="' . $varname . '" id="' . $varname . '" ' . $header .' '.$header_option.'>';
		//print_r('select name="' . $varname . '" id="' . $varname . '" ' . $header .' '.$header_option. '>');
		$_buffer.='<option value=""></option>';
        foreach ($drop as $key => $value) {
            $_buffer .= '<option value="' . $key . '"' . ($key == $default?' SELECTED':'') . ' ' . (is_array($header_option) ? $header_option[$key] : $header_option) . '>' . $value . '</option>';
        } 
        $_buffer .= '</select>';
        return $_buffer;
    } else {
        if (!empty($varname)) {
            echo "Invalid variable name";
        }
		if (!is_array($drop)) {
		    echo "The second parameter should be <i>array</i> Key=>Value <br><pre>&lt;option value=&quot;KEY&quot;&gt;VALUE&lt;/option&gt;</pre>";
		}
        return false;
    } 
} 

 function NamaPaper($fn,$idcn) {
		$ext=strtolower(substr(strrchr($fn,"."), 1)); 
		$npaper="upload/".$idcn.".".$ext;
		return $npaper;
	}
function getNamefile($fn,$idcn){
		$ext=strtolower(substr(strrchr($fn,"."), 1)); 
		$npaper=$idcn.".".$ext;
		return $npaper;
}

//mengkonversi nilai array
/*
nilai array_1 merupakan nilai key array
nilai array_1 merupakan nilai value array
Digunakan untuk drop-down ints
*/
function variable_convert($array_1,$array_2,$long=0){
	$arr_data = array();
	$arr_data['000'] = '== Select One ==';
	for($i=$long;$i<(count($array_2)+$long);$i++){
		$arr_data[$array_1[$i]] = $array_2[$i];
	}
	return $arr_data;
}

//kembalikan nilai jika error
/*
nilai key dan value harus sama
*/
function back_value($arr){
	$data = array();
	foreach($arr AS $k=>$v){
		if(!empty($v)){
			$data[strtoupper($k)] = $v;
		}
	}
	return $data;
}

//for php >= 3.0.xx
function rand_id($long) {
   $string ="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdfghijklmnopqrstuvwxyz";
   $data ='';
   for($i=0; $i< $long; $i++) {
      $rand = str_shuffle($string);
      $char = mt_rand(0, strlen($rand));
      $data .= @$rand[$char];
   }
   return $data;
}

function keygen($long){
	$tempstring ="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	mt_srand((double)microtime()*1000000);
	for($length = 0; $length <$long; $length++) {
		$pass = rand(0,strlen($tempstring));
		$data .= $tempstring[$pass];
	}
	return $data;
}

/**
fungsi ini untuk modifikasi index sebuah array
1. setiap array mempunyai jumlah array yang sama
2. setiap array selalu dimulai dari 0 atau 1 .....
**/
function change_index($arr_1,$arr_2,$long=0){
	$data = array();
	for($i=$long;$i<(count($arr_1)+$long);$i++){
		$data[$arr_1[$i]] = $arr_2[$i];
	}
	return $data;
}

function dayid($ref = '@now', $nick = 0){
    global $bulan_id, $bulan_short_id;
	if ($nick) {
	    $bulan1 = $bulan_short_id;
	} else {
		$bulan1 = $bulan_id;
	}
	if ($ref == '@now' || !preg_match("/(\d{4})-(\d{1,2})-(\d{1,2})/", $ref, $_tgl) || $ref == '0000-00-00') {
        $_tgl1 = date('j');
		$_bln = $bulan1[date('n')];
		$_thn = date('Y');
    } else {
  		$_tgl[4] = mktime(0, 0, 0, $_tgl[2], $_tgl[3], $_tgl[1]);
        $_tgl1 = date('j', $_tgl[4]);
		$_bln = $bulan1[date('n', $_tgl[4])];
		$_thn = date('Y', $_tgl[4]);
	}
    return $_tgl1 . ' ' . $_bln . ' ' . $_thn;
}

?>