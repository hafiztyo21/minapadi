
<?php
require_once("Mysql_Execute.php");
require_once("function.php");
class assettransaction extends globalFunction {
	var $error 		= '';
	var $errTrig 	= '';
	var $data 		= '';
	var $info 		= '';
	var $menu 		= '';


	//$obj=new globalFunction();
	function parse_array($arr){
		$this->data = $arr;
	}




	function equityData($sql, $edit, $linkEdit, $delete, $linkDelete){
			$db = $this->conn; 
			$result = mysql_query($sql);
			
			if (!$result) {
				//echo "<br>SQL query An error occured.\n";
				echo mysql_error();
				exit;
			}
			//return (array)$result;
			$arr = array();
			$i =0;
			if (!empty($result)){
				while ($row = mysql_fetch_array($result)) {
					$arr[$i]=$row;
					$arr[$i]['SEQ'] = $i+1;
					
                    if($arr[$i]['TRADETYPE'] == '1'){
                        $arr[$i]['TRADETYPENAME'] = "Buy";
                    }else{
                        $arr[$i]['TRADETYPENAME'] = "Sell";
                    }

					//$arr[$i]['VIEW'] ="<img src='image/page_view.png' width='12' height='12'>&nbsp;<a href='".$linkView."?detail=1&id=".$row['dist_inc_im_id']."'>".$view."</a>";
					$arr[$i]['DELETE'] ="<img src='image/page_delete.png' width='12' height='12'>&nbsp;<a href='".$linkDelete."?del=1&id=".$row['equity_id']."'>".$delete."</a>";
					$arr[$i]['EDIT'] ="<img src='image/page_edit.png' width='12' height='12'>&nbsp;<a href='".$linkEdit."?edit=1&id=".$row['equity_id']."'>".$edit."</a>";
					$i++;
				}
			}
			return $arr;
		}
    function fixedIncomeData($sql, $edit, $linkEdit, $delete, $linkDelete){
			$db = $this->conn; 
			$result = mysql_query($sql);
			
			if (!$result) {
				//echo "<br>SQL query An error occured.\n";
				echo mysql_error();
				exit;
			}
			//return (array)$result;
			$arr = array();
			$i =0;
			if (!empty($result)){
				while ($row = mysql_fetch_array($result)) {
					$arr[$i]=$row;
					$arr[$i]['SEQ'] = $i+1;
					
                    if($arr[$i]['TRADETYPE'] == '1'){
                        $arr[$i]['TRADETYPENAME'] = "Buy";
                    }else{
                        $arr[$i]['TRADETYPENAME'] = "Sell";
                    }

                    if($arr[$i]['DATATYPE'] == '1'){
                        $arr[$i]['DATATYPENAME'] = "TR";
                    }else{
                        $arr[$i]['DATATYPENAME'] = "TX";
                    }

					//$arr[$i]['VIEW'] ="<img src='image/page_view.png' width='12' height='12'>&nbsp;<a href='".$linkView."?detail=1&id=".$row['dist_inc_im_id']."'>".$view."</a>";
					$arr[$i]['DELETE'] ="<img src='image/page_delete.png' width='12' height='12'>&nbsp;<a href='".$linkDelete."?del=1&id=".$row['fixed_income_id']."'>".$delete."</a>";
					$arr[$i]['EDIT'] ="<img src='image/page_edit.png' width='12' height='12'>&nbsp;<a href='".$linkEdit."?edit=1&id=".$row['fixed_income_id']."'>".$edit."</a>";
					$i++;
				}
			}
			return $arr;
		}

    function cb_tradetype($name, $value_selected = '',$event='')
    {
            $arr = array(
                    '1' => 'Buy',
                    '2' => 'Sell',

            );
            $data ="<select size=1 name='".$name."' id='".$name."' ".$event.">";
            foreach($arr as $key => $val)
            {
                $selected = ($value_selected == $key)? ' selected': '';
                $data .=  "<option value='".$key."' ".$selected.">".$val."</option>";
            }

            $data .="</select>";

            return $data;
    } 
    function cb_settlementtype($name, $value_selected = '',$event='')
    {
            $arr = array(
                    '1' => 'DVP',
                    '2' => 'RVP',
                    '3' => 'DFOP',
                    '4' => 'RFOP',
            );
            $data ="<select size=1 name='".$name."' id='".$name."' ".$event.">";
            foreach($arr as $key => $val)
            {
                $selected = ($value_selected == $key)? ' selected': '';
                $data .=  "<option value='".$key."' ".$selected.">".$val."</option>";
            }

            $data .="</select>";

            return $data;
    } 
    function cb_settlementtype2($name, $value_selected = '',$event='')
    {
            $arr = array(
                    '1' => 'DVPBond',
                    '2' => 'RVPBond',
                    '3' => 'DFOPBond',
                    '4' => 'RFOPBond',
            );
            $data ="<select size=1 name='".$name."' id='".$name."' ".$event.">";
            foreach($arr as $key => $val)
            {
                $selected = ($value_selected == $key)? ' selected': '';
                $data .=  "<option value='".$key."' ".$selected.">".$val."</option>";
            }

            $data .="</select>";

            return $data;
    } 
    function cb_purpose($name, $value_selected = '',$event='')
    {
            $arr = array(
                    '1' => 'Hold to Maturity',
                    '2' => 'Available for Sale',
                    '3' => 'Fair Value Through Profit & Loss',
                    '4' => 'Loan / Receivable',
            );
            $data ="<select size=1 name='".$name."' id='".$name."' ".$event.">";
            foreach($arr as $key => $val)
            {
                $selected = ($value_selected == $key)? ' selected': '';
                $data .=  "<option value='".$key."' ".$selected.">".$val."</option>";
            }

            $data .="</select>";

            return $data;
    } 
}
?>