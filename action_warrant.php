<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('action_warrant.html');



if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));		
		$txt_code = strtoupper(trim(htmlentities($_POST['txt_code']))); 		
		$txt_lembar = trim(htmlentities($_POST['txt_lembar']));
        //$total =0;		
		if($txt_allocation==''){
			echo "<script>alert('Allocation is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if(  substr(strtoupper(trim($txt_code)), -2)!="-W" )
		{
			echo "<script>alert('Invalid code! code should be end with -W');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$maxqty=0;
		$sqlcek = "SELECT ifnull(sum(lembar),0) FROM tbl_kr_mst_saham where code='$txt_code' and allocation='$txt_allocation'";
		$maxqty = $data->get_value($sqlcek );
		if( $txt_lembar>$maxqty || $maxqty<=0 || $maxqty==null)
		{
			echo "<script>alert('Invalid qty! max qty ".$maxqty."');</script>";
			throw new Exception($data->err_report('s02'));
		}
			
		$sql2 ="UPDATE tbl_kr_mst_saham SET lembar=lembar-$txt_lembar WHERE allocation='$txt_allocation' and code='$txt_code'";
		$sql1 ="UPDATE tbl_kr_mst_saham SET lembar=lembar+$txt_lembar WHERE allocation='$txt_allocation' and code=SUBSTRING_INDEX('$txt_code','-W',1)";
				
		
		if (!$data->inpQueryReturnBool($sql1)){
			throw new Exception($data->err_report('s01'));
		}
		
		if (!$data->inpQueryReturnBool($sql2)){
			throw new Exception($data->err_report('s02'));
		}
		
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."')</script>";
		unset($_POST[txt_code]);
		
	}catch (Exception $e1){
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err $err_msg');</script>";
	}
}

$dataRows = array (
		'TEXT' =>  array(
						'Allocation',						
						'Stock Code',
						'Qty',
						),
		'DOT'  => array (':',':'),
		'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
		"<input type=text size=20 maxlength='9' placeholder='ex: TLKM-W' style='text-transform:uppercase' name=txt_code value='".$_POST[txt_code]."' >",
		"<input type=number placeholder='Qty (shares)' name=txt_lembar value='".$_POST[txt_lembar]."' >"
			
	)
		);
	$tittle = "Warrant  Settlement";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>"
	);


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>