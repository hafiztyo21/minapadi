<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contact_ifua.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contact_ifua;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('contact_ifua_balance.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='TRADEDATE, IFUACODE'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='desc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$datepickerFrom = $data->datePicker('dateFrom', $_POST[dateFrom],'');
$datepickerTo = $data->datePicker('dateTo', $_POST[dateTo],'');
$searchInput = '<input type="text" name="search" id="search" value="'.$_POST['search'].'">';

//get allocation
  	$sql = "SELECT pk_id,allocation,description,bank,fund_code,npwp_no FROM tbl_kr_allocation";
	//$my_allocation = array();
$allocation_select = $data->get_allocation($sql,99);	

	

//print_r($my_allocation);
//$count_allocation = count($my_allocation);

//$linkAdd = 'contact_ifua_redm_form.php?add=1';
//$linkExport = 'swtc_order_export.php';

$btnView = '<input type="submit" name="btnView" value="View">';
//$btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'\'">';
//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="alert(\'Please choose transaction date first\');">';

$datagrid = array();
$pk_id = 99;

if ($_POST['btnView']=='View' || $_GET['view']=='1'){
    $search = "";
	$sql_search = "";
	$from = date('Y-m-d');
	$to = date('Y-m-d');
    if(isset($_POST['search']) || !EMPTY($_POST['search'])){
  	    $search =  trim(htmlentities($_POST['search']));
	$sql_search =" AND (X.ifua_code != '' OR X.ifua_code IS NOT NULL) AND (X.ifua_code like '%$search%' OR X.ifua_name like '%$search%' OR X.cus_sid like '%$search%')"; 
	}
	if (!empty($_POST['dateFrom']))
		$from = $_POST['dateFrom'];
	if (!empty($_POST['dateTo']))
		$to = $_POST['dateTo'];
	if (!empty($_POST['allocationss'])){
			$allocation = explode('||',$_POST['allocationss']);
			$fund_code = $allocation[1];
			$pk_id = $allocation[0];
			$allocationss = $pk_id;
			//print_r($allocation);
			$allocation_select = $data->get_allocation($sql,$pk_id);
		
		
		
	}
	else{
		
		
			$allocation = explode('||',$_POST['allocationss']);
			$fund_code = $allocation[1];
			$pk_id = "99";
			$allocationss = $pk_id;
			// print_r($allocation);
		
		
	}

	//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';

  	$sql = "SELECT 	X.*, 
		Z.balance, 
		Z.totalunit,
		Z.tradingdate, 
		Z.nav,
		$pk_id allocation,
		
		(
			SELECT count(XX.cus_sid) jlh FROM 
				(
					SELECT ifua_code FROM tbl_ifua_balance
					WHERE fund_code='".$fund_code."' AND  tradingdate>='".$from."' AND tradingdate<='".$to."'  
					GROUP BY ifua_code
				) YY LEFT JOIN
					(
					SELECT A.is_deleted,A.cus_ins_id n_id, A.ifua_code, A.ifua_name,'' tempat_lahir_cus,'' middle_name,'' last_name,B.cus_alamat_pt cus_alamat ,B.cus_code, B.cus_name, B.cus_pt TYPE, B.cus_struktur STATUS, B.cus_no_npwp ktp_siup, B.cus_ins_sid cus_sid , 0 isIndividu, B.cus_tgl_pt tgl, cus_bank_cab bank, cus_no_rek rek, cus_pemilik_rek rek_name,
					cus_direksi,cus_direksi_2,cus_direksi_3, cus_direksi_ktp,cus_direksi_ktp_2,cus_direksi_ktp_3, cus_direksi_tgl,cus_direksi_tgl_2,cus_direksi_tgl_3, cus_direksi_jbt, cus_no_npwp idd
					FROM  tbl_kr_cus_institusi_ifua  A LEFT JOIN tbl_kr_cus_institusi B
					ON A.cus_ins_id=B.cus_id and A.is_deleted='0' 


					UNION ALL
					SELECT A.is_deleted,A.cus_id n_id, A.ifua_code, A.ifua_name,B.cus_tempat_lahir tempat_lahir_cus,B.middle_name middle_name,B.last_name last_name,B.cus_address cus_alamat, B.cus_code, B.cus_name, B.Cus_national TYPE, B.cus_status_domisili STATUS, B.cus_no_identity ktp_siup, B.cus_sid cus_sid, 1 isIndividu, B.cus_tgl_lahir tgl,  cus_bank_name bank, cus_account_number rek, cus_account_name  rek_name,
					'' cus_direksi, '' cus_direksi_ktp,'' cus_direksi_ktp_2,'' cus_direksi_ktp_3,'' cus_direksi_2,'' cus_direksi_3, '' cus_direksi_tgl,''cus_direksi_tgl_2,''cus_direksi_tgl_3, '' cus_direksi_jbt, b.cus_no_identity idd
					FROM tbl_kr_cus_sup_ifua A LEFT JOIN tbl_kr_cus_sup B
					ON A.cus_id=B.cus_id and A.is_deleted='0'
					WHERE A.is_deleted='0' AND (A.ifua_code != '' OR A.ifua_code IS NOT NULL) AND (A.ifua_code like '%$search%' OR A.ifua_name like '%$search%' OR B.cus_sid like '%$search%') 
					) XX			
					ON YY.ifua_code=XX.ifua_code 
					LEFT JOIN (
					SELECT  ifua_code,  endingbalance *(SELECT nab_now/10000 FROM tbl_kr_cash WHERE allocation='".$pk_id."' AND create_dt='".$to."'  ) balance, endingbalance totalunit
					FROM tbl_ifua_balance 
					WHERE tradingdate='".$to."' AND fund_code='".$fund_code."'
					) Z ON YY.ifua_code=Z.ifua_code WHERE X.is_deleted = '0' $sql_search  AND XX.cus_sid = X.cus_sid GROUP BY XX.cus_sid					
					
		
		) JLH,
		(
			if(ifnull(Z.balance,0) < 1
				, if((SELECT count(investor_ac_no) FROM tbl_kr_subsredm_order where investor_ac_no = X.ifua_code AND fund_code = '".$fund_code."' AND transaction_date >= '".$from."' AND transaction_date <= '".$to."') > 0, 'SHOW','NOTSHOW' )
				,'SHOW') 
				) nul_balance			
		FROM 
(
SELECT ifua_code,tradingdate FROM tbl_ifua_balance
WHERE fund_code='".$fund_code."' AND  tradingdate>='".$from."' AND tradingdate<='".$to."'  
GROUP BY ifua_code
) Y LEFT JOIN
(
SELECT A.is_deleted,A.cus_ins_id n_id, A.ifua_code, A.ifua_name,'' tempat_lahir_cus,'' middle_name,'' last_name,B.cus_alamat_pt cus_alamat ,B.cus_code, B.cus_name, B.cus_pt TYPE, B.cus_struktur STATUS, B.cus_no_npwp ktp_siup, B.cus_ins_sid cus_sid , 0 isIndividu, B.cus_tgl_pt tgl, cus_bank_cab bank, cus_no_rek rek, cus_pemilik_rek rek_name,
cus_direksi,cus_direksi_2,cus_direksi_3, cus_direksi_ktp,cus_direksi_ktp_2,cus_direksi_ktp_3, cus_direksi_tgl,cus_direksi_tgl_2,cus_direksi_tgl_3, cus_direksi_jbt, cus_no_npwp idd
FROM  tbl_kr_cus_institusi_ifua  A LEFT JOIN tbl_kr_cus_institusi B
ON A.cus_ins_id=B.cus_id 
UNION ALL
SELECT A.is_deleted,A.cus_id n_id, A.ifua_code, A.ifua_name,B.cus_tempat_lahir tempat_lahir_cus,B.middle_name middle_name,B.last_name last_name,B.cus_address cus_alamat, B.cus_code, B.cus_name, B.Cus_national TYPE, B.cus_status_domisili STATUS, B.cus_no_identity ktp_siup, B.cus_sid cus_sid, 1 isIndividu, B.cus_tgl_lahir tgl,  cus_bank_name bank, cus_account_number rek, cus_account_name  rek_name,
'' cus_direksi, '' cus_direksi_ktp,'' cus_direksi_ktp_2,'' cus_direksi_ktp_3,'' cus_direksi_2,'' cus_direksi_3, '' cus_direksi_tgl,''cus_direksi_tgl_2,''cus_direksi_tgl_3, '' cus_direksi_jbt, b.cus_no_identity idd
FROM tbl_kr_cus_sup_ifua A LEFT JOIN tbl_kr_cus_sup B 
ON A.cus_id=B.cus_id 
WHERE  A.is_deleted='0' 
) X
ON Y.ifua_code=X.ifua_code 
LEFT JOIN (
SELECT  ifua_code,tradingdate,  endingbalance *(SELECT nab_now/10000 FROM tbl_kr_cash WHERE allocation='".$pk_id."' AND create_dt='".$to."'  ) balance,(SELECT nab_now/10000 FROM tbl_kr_cash WHERE allocation='".$pk_id."' AND create_dt='".$to."'  ) nav, endingbalance totalunit
FROM tbl_ifua_balance 
WHERE tradingdate='".$to."' AND fund_code='".$fund_code."'
) Z ON Y.ifua_code=Z.ifua_code AND X.is_deleted = '0' GROUP BY X.ifua_code HAVING nul_balance = 'SHOW' $sql_search ORDER BY JLH ASC, X.ifua_name";
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->ifuaBalanceData($sql, 'edit', '', 'delete', '', '');
	
}

$tmpl->addVar('page','search',$searchInput);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','datefrom',$datepickerFrom);
$tmpl->addVar('page','dateto',$datepickerTo);
$tmpl->addVar('page','allocationss',$allocation_select);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>