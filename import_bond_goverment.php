<?php
session_start();
include 'Classes/excel_reader2.php';
//include 'Classes/PHPExcel/Writer/Excel2007.php';
//include 'Classes/PHPExcel/IOFactory.php';
//include "excel_reader2.php";
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$datadb = new globalFunction();
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('import_contact_personal.html');
$tablename = 'tbl_kr_master_bond';


if($_POST[Submit]) {
						$data = new Spreadsheet_Excel_Reader($_FILES['uploadedfile']['tmp_name']);
					 
					// membaca jumlah baris dari data excel
					$baris = $data->rowcount($sheet_index=0);
					 
					// nilai awal counter untuk jumlah data yang sukses dan yang gagal diimport
					$sukses = 0;
					$gagal = 0;
					 
					// import data excel mulai baris ke-2 (karena baris pertama adalah nama kolom)
					for ($i=2; $i<=$baris; $i++)
					{
						$bond_code=$data->val($i, 1);
					  // membaca data nim (kolom ke-1)
					
					  $bond_type=$data->val($i, 2);
					    $isin_code= $data->val($i, 3);
					  // membaca data nama (kolom ke-2)
					  $bond_name=$data->val($i, 4);
					  $county_code=$data->val($i, 5);
					  $issuer_code=$data->val($i, 6);
					   //$wifg=$data->val($i, 7);
					  $bonds_issue_methode=$data->val($i, 7);
					  $abs_type=$data->val($i, 8);
					  $bond_seniority=$data->val($i, 9);
					 // $secondary_serial=$data->val($i, 9);
					 // $snsp=$data->val($i, 10);
					  
					
					
					$issue_price=$data->val($i, 11);
					
					$issue_date2=$data->val($i, 12);//date
					$issue_date_a= substr($issue_date2, 6);
					   $issue_date_b= substr($issue_date2, 3, 2);
					   $issue_date_c= substr($issue_date2, 0, 2);
					   $issue_date = "$issue_date_a-$issue_date_c-$issue_date_b";
				
					$maturity_date2=$data->val($i, 13);//date
					   $maturity_date_a= substr($maturity_date2, 6);
					   $maturity_date_b= substr($maturity_date2, 3, 2);
					   $maturity_date_c= substr($maturity_date2, 0, 2);
					   $maturity_date = "$maturity_date_a-$maturity_date_c-$maturity_date_b";
					//$otc_code=$data->val($i, 18);
					$listing_status=$data->val($i, 14);
					$listen_date2=$data->val($i, 15);//date
					   $listen_date_a= substr($listen_date2, 6);
					   $listen_date_b= substr($listen_date2, 3, 2);
					   $listen_date_c= substr($listen_date2, 0, 2);
					   $listen_date = "$listen_date_a-$listen_date_c-$listen_date_b";
					$issue_amount=$data->val($i, 16);
					
					//$custodion_code=$data->val($i, 22);
					//$payment_agen_code=$data->val($i, 23);
					$currency_code=$data->val($i, 17);
					//$interest_currency_code=$data->val($i, 27);
					$amonut_outstanding=$data->val($i, 18);
					//$payment_at_period=$data->val($i, 30);
					//$day_count_basis=$data->val($i, 31);
					$coupon_payment=$data->val($i, 19);
					
					$coupon_rate=$data->val($i, 20);
					$coupon_compounding=$data->val($i, 21);
					$coupon_term_month=$data->val($i, 22);
					$coupon_generation=$data->val($i, 23);
					//$fix_coupon_day=$data->val($i, 39);
	
					$calender_code=$data->val($i, 24);
				//	$instrument_type=$data->val($i, 57);
					$tenor=$data->val($i, 25);
					$orginated_amount=$data->val($i, 26);
					$bond_class_node=$data->val($i, 27);
					
					$issue_type="goverment";
				
					
					  // setelah data dibaca, sisipkan ke dalam tabel mhs
					  $query = 
				 "INSERT INTO $tablename(
				  bond_code ,
				  isin_code ,
				  bond_name ,
				  bond_type ,
				  county_code ,
				  issuer_code ,
				  wifg ,
				  bonds_issue_methode ,
				  secondary_serial ,
				  snsp ,
				  abs_type ,
				  bond_seniority ,
				  issue_price ,
				  issue_date  ,
				  maturity_date  ,
				  otc_code ,
				  listing_status ,
				  listen_date  ,
				  issue_amount  ,
				  custodion_code  ,
				  payment_agen_code  ,
				  currency_code  ,
				  interest_currency_code  ,
				  amonut_outstanding  ,
				  payment_at_period  ,
				  day_count_basis  ,
				  coupon_payment  ,
				  coupon_rate  ,
				  coupon_compounding  ,
				  coupon_term_month ,
				  coupon_generation ,
				  fix_coupon_day  ,
				  calender_code ,
				  instrument_type ,
				  tenor  ,
				  orginated_amount  ,
				  bond_class_node ,
				  issue_type 
				  ) VALUES 
					  ('$bond_code' ,
					  '$isin_code' ,
					  '$bond_name' ,
					  '$bond_type' ,
					  '$county_code' ,
					  '$issuer_code' ,
					  '$wifg' ,
					  '$bonds_issue_methode' ,
					  '$secondary_serial' ,
					  '$snsp' ,
					  '$abs_type' ,
					  '$bond_seniority' ,
					  '$issue_price' ,
					  '$issue_date'  ,
					  '$maturity_date'  ,
					  '$otc_code' ,
					  '$listing_status' ,
					  '$listen_date' ,
					  '$issue_amount'  ,
					  '$custodion_code'  ,
					  '$payment_agen_code'  ,
					  '$currency_code'  ,
					  '$interest_currency_code'  ,
					  '$amonut_outstanding'  ,
					  '$payment_at_period'  ,
					  '$day_count_basis'  ,
					  '$coupon_payment'  ,
					  '$coupon_rate'  ,
					  '$coupon_compounding'  ,
					  '$coupon_term_month' ,
					  '$coupon_generation' ,
					  '$fix_coupon_day'  ,
					  '$calender_code' ,
					  '$instrument_type' ,
					  '$tenor'  ,
					  '$orginated_amount'  ,
					  '$bond_class_node' ,
					  '$issue_type'  
					  
					  )";
				//	print($query);
						if ($datadb->inpQueryReturnBool($query))
						{	echo "<script>alert('SUKSES');</script>";	
						 $sukses++;}
						else
						{	echo "<script>alert('GAGAL');</script>";
						$gagal++;}
			
					 
					  // jika proses insert data sukses, maka counter $sukses bertambah
					  // jika gagal, maka counter $gagal yang bertambah
					
				
					}
					 
					// tampilan status sukses dan gagal
					echo "<h3>Proses import data selesai.</h3>";
					echo "<p>Jumlah data yang sukses diimport : ".$sukses."<br>";
					echo "Jumlah data yang gagal diimport : ".$gagal."</p>";
	
	
	
}



$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->addVar('page', 'tittle',$tittle);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>