<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('print.html');

if($_GET['cek']=='1'){
	$date=date("d-M-y",strtotime($_GET['tgl']));
	$txt_tanggal 		= $_GET['tgl'];
	$txt_allocation	 	= $_GET['allocation'];
	$rowo = $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$txt_allocation."'");
	$allocation2 = $rowo['A'];
	$rowc = $data->get_row("select * from tbl_kr_cash where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."'");
	$txt_kas = $rowc['kas_giro'];
	$txt_piutang =$rowc['total_piutang'];
	$txt_aktiva = $rowc['aktiva_lain'];
	$txt_kewajiban = $rowc['total_kewajiban'];
	$txt_up = $rowc['jumlah_up'];
	$txt_nab = $rowc['nab'];
	$waktu = date("Y-m-d");
	if (empty($rowc)){
		echo "<script>alert('Input manual cash tanggal ".$txt_tanggal." untuk ".$allocation2." belum dilakukan')</script>";
	}else{
	
	$_SESSION['sql']='';
	//$waktu='2011-07-14';
	
    $sql  = "select tbl_kr_report_bonds.*, format(face_value,0) as FACE, format(market_value,0) as VAL from tbl_kr_report_bonds
      where create_dt='".$txt_tanggal."'
	  and allocation='".$txt_allocation."'";

	$sql2 =  "select tbl_kr_report_pn.*,format(price_val,0) as val from tbl_kr_report_pn
	where create_dt='".$txt_tanggal."'
	and allocation='".$txt_allocation."'";

	$sql3  = "select tbl_kr_report_deposit.*, format(total_aktiv,0) as AKTIV, format(face_value,0) as FACE from tbl_kr_report_deposit
      where create_dt='".$txt_tanggal."'
		and allocation='".$txt_allocation."'
	  ORDER BY tbl_kr_report_deposit.name_bank ASC ";
/*
	$sql4  = "SELECT stk_name,tbl_kr_report_saham.code as CODE,format(tbl_kr_report_saham.harga_close,0) as HRG, stk_cd, tbl_kr_report_saham.total, tbl_kr_report_saham.allocation,format(harga_prev,0) as harga_prev, format(tbl_kr_report_saham.total,0) as TTL,
	format(tbl_kr_report_saham.avg,4) as AVG,
	(tbl_kr_report_saham.`harga_close`*tbl_kr_report_saham.`lembar`)-(tbl_kr_report_saham.`avg`*tbl_kr_report_saham.`lembar`) AS PL,
	tbl_kr_report_saham.lembar as LMBR FROM tbl_kr_report_saham
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_report_saham.create_dt = tbl_kr_stock_rate.stk_date
	and tbl_kr_stock_rate.stk_cd = tbl_kr_report_saham.code
	where create_dt='".$txt_tanggal."'
	and allocation='".$txt_allocation."'
	and total > '0'
	ORDER BY tbl_kr_report_saham.code ASC ";
*/
	// 	$sql4  = "SELECT if('".$txt_allocation."'=7,'20.00%','10.00%') as MAKSA,stk_name,tbl_kr_report_saham.code as CODE,tbl_kr_report_saham.harga_close as HRG, stk_cd, tbl_kr_report_saham.total, tbl_kr_report_saham.allocation,harga_prev, format(tbl_kr_report_saham.total,0) as TTL,
	// round(tbl_kr_report_saham.avg,4) as AVG,
	// (tbl_kr_report_saham.`harga_close`*tbl_kr_report_saham.`lembar`)-(tbl_kr_report_saham.`avg`*tbl_kr_report_saham.`lembar`) AS PL,
	// tbl_kr_report_saham.lembar as LMBR FROM tbl_kr_report_saham
	// LEFT JOIN tbl_kr_stock_rate ON tbl_kr_report_saham.create_dt = tbl_kr_stock_rate.stk_date
	// and tbl_kr_stock_rate.stk_cd = tbl_kr_report_saham.code
	// where create_dt='".$txt_tanggal."'
	// and allocation='".$txt_allocation."'
	// and total > '0'
	// ORDER BY tbl_kr_report_saham.code ASC ";

	 $sql4="SELECT if('".$txt_allocation."'=7,'20.00%','10.00%') as MAKSA, stk_name,tbl_kr_report_saham.code as CODE,
tbl_kr_report_saham.harga_close as HRG, stk_cd, tbl_kr_report_saham.total, bb.acm AS akumulasi,
tbl_kr_report_saham.allocation,harga_prev, 
format(tbl_kr_report_saham.total,0) as TTL, round(tbl_kr_report_saham.avg,4) as AVG,
 (tbl_kr_report_saham.`harga_close`*tbl_kr_report_saham.`lembar`)
 -(tbl_kr_report_saham.`avg`*tbl_kr_report_saham.`lembar`) AS PL,
  tbl_kr_report_saham.lembar as LMBR FROM 
  tbl_kr_report_saham 
  LEFT JOIN tbl_kr_stock_rate ON
   tbl_kr_report_saham.create_dt = tbl_kr_stock_rate.stk_date 
	and tbl_kr_stock_rate.stk_cd = tbl_kr_report_saham.CODE 	
	LEFT JOIN (SELECT REPLACE(CODE ,'-W', '') GROUPcode, CODE, SUM(total) AS acm
FROM tbl_kr_report_saham WHERE create_dt='".$txt_tanggal."' AND allocation='".$txt_allocation."' GROUP BY GROUPcode) bb ON REPLACE(tbl_kr_report_saham.CODE, '-W','')=bb.GROUPcode where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."' and total > '0'
ORDER BY tbl_kr_report_saham.code ASC ";

$rowo = $data->get_row("select SUM(market_value) as QTY2,
					format(SUM(market_value),0) AS QTY, 
					sum(market_value) as market,
					format(SUM(face_value),0) AS VAL,
					SUM(face_value*(int_rate/100)) AS MARKET_PEROLEHAN
					from tbl_kr_report_bonds 
					where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."'");
	$qty = $rowo['VAL'];
	$value = $rowo['QTY'];
	$value1 = $rowo['QTY2'];
	$value1_perolehan = $rowo['MARKET_PEROLEHAN'];
	$b = $rowo['market'];

	$rowp = $data->get_row("select format(SUM(price_val),0) as quantity, sum(price_val) as prc_val from tbl_kr_report_pn 
							where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."'");
	$quantity = $rowp['quantity'];
	$a = $rowp['prc_val'];

	$rowb = $data->get_row("select format(SUM(face_value),0) as AKTIV, format(SUM(face_value),0) as VAL, sum(face_value) as FACE from tbl_kr_report_deposit 
							where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."'");
	$qty3 = $rowb['VAL'];
	$total_aktiv = $rowb['AKTIV'];
	$d = $rowb['FACE'];

	$rows = $data->get_row("select format(SUM(total),0) as total2, format(SUM(lembar),0) as lembar2, sum(total) as TTL from tbl_kr_report_saham 
							where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."'");
	$lembar = $rows['lembar2'];
	$total = $rows['total2'];
	$c = $rows['TTL'];
	
	//print ($a);
	$total_aktiva = $a + $b + $c + $d+ $txt_kas + $txt_piutang + $txt_aktiva;
	
	$total_aktiv1 = number_format(($c/$total_aktiva)*100,2);
	$rowm = $data->get_row("SELECT count(stk_name) as max FROM tbl_kr_report_saham
	LEFT JOIN tbl_kr_stock_rate ON tbl_kr_report_saham.create_dt = tbl_kr_stock_rate.stk_date
	and tbl_kr_stock_rate.stk_cd = tbl_kr_report_saham.code
	where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."'");
	$ttlmax = $rowm['max'];
	$l=number_format($ttlmax*10,2);
	$total_aktiva_bersih = $total_aktiva - $txt_kewajiban;
	//mencari sum total aktiva %
	$sql5 = $data->get_rows("select total from tbl_kr_report_saham where create_dt='".$txt_tanggal."' and allocation='".$txt_allocation."'ORDER BY tbl_kr_report_saham.code ASC");
	$kt2=0;
	for ($i=0;$i<count($sql5);$i++){
	$kt= ($sql5[$i]['total']/$total_aktiva_bersih)*100;
	$kt2= $kt2 + $kt;
	
	$kt4=number_format($kt2,2);
	}
	$kt3=number_format($kt2,2);
	//end
	
	$nab_per_saham = number_format($total_aktiva_bersih/$txt_up,4);
	$g = $total_aktiva_bersih/$txt_up;
	$i = $txt_nab;
	$y = $i/10000;
	$f = ($g-$y)/(($y==0)?1:$y);
	$F =  number_format($f*100,3);
		$total_aktiva_bersih1 = number_format($total_aktiva_bersih,2);
	$nab = number_format($txt_nab/10000,4);
	$txt_kas1 = number_format($txt_kas,2);
//=============================================================================
	$isValidateOblig = false;
    $isValidateSaham = false;

	if ($txt_allocation=='1')
	{
		$tgl1 ="2006-04-19";
		$tglbulan ="19-Apr-2006";
		$kas_giro ="BII RD Keraton";
        $oblig = "75.00";
		$mana ="75.00";
		$pasaruang = "";
		$deposito = "";
        $headerOblig = "5% - 75%";
		$headerPasarUang = "";
		$headerDeposito = "";
        $headerMana = "5% - 75%";

		$isValidateOblig = true;
        $limitObligasi = array( 'batas atas'=> 75, 'batas bawah' => 5, 'selisih atas'=>3, 'selisih bawah'=>2.5);
        $isValidateSaham = true;
        $limitSaham = array( 'batas atas'=> 75, 'batas bawah' => 5, 'selisih atas'=>3, 'selisih bawah'=>2.5 );
	} else 	if ($txt_allocation=='2')
	{
		$tgl1 ="2012-04-11";
		$tglbulan ="11-Apr-2012";
		$kas_giro ="Bank Mandiri RD Keraton 2";
        $oblig = "79.00";
		$mana ="79.00";
		$pasaruang = "";
		$deposito = "";
        $headerOblig = "5% - 79%";
		$headerPasarUang = "";
		$headerDeposito = "";
        $headerMana = "5% - 79%";

		$isValidateOblig = true;
        $limitObligasi = array( 'batas atas'=> 79, 'batas bawah' => 5, 'selisih atas'=>4, 'selisih bawah'=>2.5);
        $isValidateSaham = true;
        $limitSaham = array( 'batas atas'=> 79, 'batas bawah' => 5, 'selisih atas'=>4, 'selisih bawah'=>2.5 );
	} else 	if ($txt_allocation=='3')
	{
		$tgl1 ="2012-04-11";
		$tglbulan ="11-Apr-2012";
		$kas_giro ="Bank Mandiri Property Plus";
        $oblig = "79.00";
		$mana ="79.00";
		$pasaruang = "";
		$deposito = "";
        $headerOblig = "5% - 79%";
		$headerPasarUang = "";
		$headerDeposito = "";
        $headerMana = "5% - 79%";

		$isValidateOblig = true;
        $limitObligasi = array( 'batas atas'=> 79, 'batas bawah' => 5, 'selisih atas'=>4, 'selisih bawah'=>2.5);
        $isValidateSaham = true;
        $limitSaham = array( 'batas atas'=> 79, 'batas bawah' => 5, 'selisih atas'=>4, 'selisih bawah'=>2.5 );
	} else if ($txt_allocation=='4')
	{
		$tgl1 ="2016-10-21";
		$tglbulan ="21-Okt-2016";
		$kas_giro ="Bank Mandiri Astana Saham";
        $oblig = "0.00";
		$mana ="80.00";
		$pasaruang = "20%";
		$deposito = "20%";
        $headerOblig = "";
		$headerPasarUang = "Maks 20%";
		$headerDeposito = "Maks 20%";
        $headerMana = "Minimum 80%";

		$isValidateOblig = false;
        $limitObligasi = array();
        $isValidateSaham = true;
        $limitSaham = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );
	} else 	if ($txt_allocation=='5')
	{
		$tgl1 ="2016-10-21";
		$tglbulan ="21-Okt-2016";
		$kas_giro ="Bank Mandiri Kahuripan Pendapatan Tetap";
        $oblig = "80.00";
		$mana ="0.00";
		$pasaruang = "20%";
		$deposito = "20%";
        $headerOblig = " Indikator Min (%)";
		$headerPasarUang = "Maks 20%";
		$headerDeposito = "Maks 20%";
        $headerMana = "";

		$isValidateOblig = true;
        $limitObligasi = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );
        $isValidateSaham = false;
        $limitSaham = array();
	} else if ($txt_allocation=='6')
	{
		$tgl1 ="2018-1-24";
		$tglbulan ="24-Jan-2018";
		$kas_giro ="Bank Mandiri Pringgondani Saham";
        $oblig = "80.00";
		$mana ="0.00";
		$pasaruang = "20%";
		$deposito = "20%";
        $headerOblig = " Indikator Min (%)";
		$headerPasarUang = "Maks 20%";
		$headerDeposito = "Maks 20%";
        $headerMana = "";

		$isValidateOblig = true;
        $limitObligasi = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );
        $isValidateSaham = false;
        $limitSaham = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );
	} else if ($txt_allocation=='7')
	{
		$tgl1 ="2018-4-18";
		$tglbulan ="18-Apr-2018";
		$kas_giro ="Bank Mandiri Indraprastha Saham Syariah";
        $oblig = "80.00";
		$mana ="0.00";
        $headerOblig = "Min";
        $headerMana = "Maks";
        
        $isValidateOblig = true;
        $limitObligasi = array( );
        $isValidateSaham = false;
        $limitSaham = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );
	} else if ($txt_allocation=='8')
	{
		$tgl1 ="2018-11-05";
		$tglbulan ="05-Nov-2018";
		$kas_giro ="Bank Mandiri Amanah Saham Syariah";
        $oblig = "80.00";
		$mana ="0.00";
        $headerOblig = "Min";
        $headerMana = "Maks";
        
        $isValidateOblig = true;
        $limitObligasi = array( );
        $isValidateSaham = false;
        $limitSaham = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );
	} else if ($txt_allocation=='9')
	{
		$tgl1 ="2019-01-02";
		$tglbulan ="02-Jan-2019";
		$kas_giro ="Bank Mandiri Hastinapura Saham";
        $oblig = "0.00";
		$mana ="80.00";
		$pasaruang = "20%";
		$deposito = "20%";
        $headerOblig = "";
		$headerPasarUang = "Maks 20%";
		$headerDeposito = "Maks 20%";
        $headerMana = "Minimum 80%";

		$isValidateOblig = false;
        $limitObligasi = array();
        $isValidateSaham = true;
        $limitSaham = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );

	}	else if($txt_allocation=='10'){

		$tgl1 ="2019-06-20";
		$tglbulan ="20-Jun-2019";
		$kas_giro ="Bank Mandiri Khazanah Pasar Uang Syariah";
        $oblig = "0.00";
		$mana ="80.00";
		$pasaruang = "20%";
		$deposito = "20%";
        $headerOblig = "";
		$headerPasarUang = "Maks 20%";
		$headerDeposito = "Maks 20%";
        $headerMana = "Minimum 80%";

		$isValidateOblig = false;
        $limitObligasi = array();
        $isValidateSaham = true;
        $limitSaham = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );


	}
	else {
		$tgl1 ="2018-4-17";
		$tglbulan ="17-Apr-2018";
		$kas_giro ="Bank Mandiri Pringgondani Saham";
        $oblig = "80.00";
		$mana ="0.00";
		$pasaruang = "20%";
		$deposito = "20%";
        $headerOblig = " Indikator Min (%)";
		$headerPasarUang = "Maks 20%";
		$headerDeposito = "Maks 20%";
        $headerMana = "";

		$isValidateOblig = true;
        $limitObligasi = array( 'batas atas'=> 100, 'batas bawah' => 80, 'selisih atas'=>0, 'selisih bawah'=>5 );
        $isValidateSaham = false;
        $limitSaham = array();
	}
	
	// Menghitung Jumlah hari
//	$tgl1 = "2006-04-19";  // 1 Oktober 2009
$tgl2 = $txt_tanggal;  // 10 Oktober 2009

// memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
// dari tanggal pertama

$pecah1 = explode("-", $tgl1);
$date1 = $pecah1[2];
$month1 = $pecah1[1];
$year1 = $pecah1[0];

// memecah tanggal untuk mendapatkan bagian tanggal, bulan dan tahun
// dari tanggal kedua

$pecah2 = explode("-", $tgl2);
$date2 = $pecah2[2];
$month2 = $pecah2[1];
$year2 =  $pecah2[0];

// menghitung JDN dari masing-masing tanggal

$jd1 = GregorianToJD($month1, $date1, $year1);
$jd2 = GregorianToJD($month2, $date2, $year2);

// hitung selisih hari kedua tanggal

$selisih = $jd2 - $jd1;

//echo "Selisih kedua tanggal adalah ".$selisih." hari";

//==================================================================================
	$ultah = number_format($selisih,2);

	$txt_piutang1 = number_format($txt_piutang,2);
	$txt_aktiva = number_format($txt_aktiva,2);
	$txt_kewajiban = number_format($txt_kewajiban,2);
	$txt_up = number_format($txt_up,2);
	$q=$txt_kas/$total_aktiva_bersih;
	$Q = number_format($q*100,2);

	$w=$txt_piutang/$total_aktiva_bersih;
	$W = number_format($w*100,2);
	$o =($g-1000)/1000;
	$O = number_format($o*100,3);
	if( $selisih==0 ) $selisih=1;
	$tumbuh = (365/$selisih)*$o;
	$DG= $data->dataGridSaham($sql,'obligasi_id',$data->ResultsPerPage,$pg,'','edit',$linkEdit,'','','','','','','',$total_aktiva_bersih,$total_aktiva);
	$DG2= $data->dataGridSaham($sql2,'pn_id',$data->ResultsPerPage,$pg,'');
	$DG3= $data->dataGridSaham($sql3,'pk_id',$data->ResultsPerPage,$pg,'');
	$DG4= $data->dataGridSaham($sql4,'code',$data->ResultsPerPage,$pg,'','','',$txt_allocation,'','','','','','',$total_aktiva_bersih);
	$total_aktiva1 = number_format($total_aktiva,2);
	$s1=number_format(($value1_perolehan/$total_aktiva_bersih)*100,2);
	$s2=number_format(($value1/$total_aktiva_bersih)*100,2);
	$s3=number_format(($value1_perolehan/$total_aktiva)*100,2);
	$s4=number_format(($value1/$total_aktiva)*100,2);			
	$v=number_format(($d/$total_aktiva_bersih)*100,2);

	$marketvalue = ($value1/$total_aktiva_bersih)*100;

		//$s = number_format($s*100,2);
	$tumbuhan = number_format($tumbuh*100,2);

	$colorOblig = "";
    if($isValidateOblig){
        
        if($marketvalue < $limitObligasi['batas bawah'] || $marketvalue > $limitObligasi['batas atas']){
            //merah
            $colorOblig = "style='background-color:#ff3300;'";
        }else if($marketvalue >=  ($limitObligasi['batas atas'] - $limitObligasi['selisih atas']) || $marketvalue <= ($limitObligasi['batas bawah'] - $limitObligasi['selisih bawah'])){
            //orange
            $colorOblig = "style='background-color:#ffcc00;'";
        }else{
            
        }
    }
    $colorSaham = "";
    if($isValidateSaham){
        if($kt2 < $limitSaham['batas bawah'] || $kt2 > $limitSaham['batas atas']){
            //merah
            $colorSaham = "style='background-color:#ff3300;'";
        }else if($kt2 >=  ($limitSaham['batas atas'] - $limitSaham['selisih atas']) || $kt2 <= ($limitSaham['batas bawah'] - $limitSaham['selisih bawah'])){
            //orange
            $colorSaham = "style='background-color:#ffcc00;'";
        }else{
            
        }
    }
		
}
}
$tmpl->addVar('page','kas_giro',$kas_giro);
$tmpl->addVar('page','tglbulan',$tglbulan);
$tmpl->addVar('page','tombol',$tombol);
$tmpl->addVar('page','tanggal',$tanggal);
$tmpl->addVar('page','tgl',$txt_tanggal);
$tmpl->addVar('page','date',$date);
$tmpl->addVar('page','pn',$quantity);
$tmpl->addVar('page','qty',$qty);
$tmpl->addVar('page','qty3',$qty3);
$tmpl->addVar('page','total aktiv',$total_aktiv);
$tmpl->addVar('page','total_aktiv1',$total_aktiv1);
$tmpl->addVar('page','value',$value);
$tmpl->addVar('page','total',$total);
$tmpl->addVar('page','lembar',$lembar);
$tmpl->addVar('page','waktu',$waktu);
$tmpl->addVar('page','kas',$txt_kas1);
$tmpl->addVar('page','piutang',$txt_piutang1);
$tmpl->addVar('page','aktiva',$txt_aktiva);
$tmpl->addVar('page','kewajiban',$txt_kewajiban);
$tmpl->addVar('page','up',$txt_up);
$tmpl->addVar('page','total_aktiva',$total_aktiva1);
$tmpl->addVar('page','total_aktiva_bersih',$total_aktiva_bersih1);
$tmpl->addVar('page','Q',$Q);
$tmpl->addVar('page','F',$F);
$tmpl->addVar('page','KT3',$kt3);
$tmpl->addVar('page','KT4',$kt4);
$tmpl->addVar('page','L',$l);
$tmpl->addVar('page','W',$W);
$tmpl->addVar('page','O',$O);
$tmpl->addVar('page','nab_per_saham',$nab_per_saham);
$tmpl->addVar('page','S2',$s2);
$tmpl->addVar('page','S4',$s4);
$tmpl->addVar('page','S1',$s1);
$tmpl->addVar('page','S3',$s3);
$tmpl->addVar('page','NAB',$nab);
$tmpl->addVar('page','Y',$y);
$tmpl->addVar('page','ULI',$ultah);
$tmpl->addVar('page','ADE',$tumbuhan);
$tmpl->addVar('page','OBLIG',$oblig);
$tmpl->addVar('page','M',$mana);
$tmpl->addVar('page','headeroblig',$headerOblig);
$tmpl->addVar('page','headermana',$headerMana);
$tmpl->addVar('page','headerpasaruang',$headerPasarUang);
$tmpl->addVar('page','headerdeposito',$headerDeposito);
$tmpl->addVar('page','COLOROBLIG',$colorOblig);
$tmpl->addVar('page','COLORSAHAM',$colorSaham);
$tmpl->addVar('page','pasauang',$pasaruang);
$tmpl->addVar('page','deposito',$deposito);
$tmpl->addVar('page','AHAY',$v);
$tmpl->addVar('page','allocation',$allocation2); 


$tmpl->addRows('loopData',$DG);
$tmpl->addRows('loopData2',$DG2);
$tmpl->addRows('loopData3',$DG3);
$tmpl->addRows('loopData4',$DG4);
$tmpl->displayParsedTemplate('page');

?>