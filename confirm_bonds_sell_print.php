<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tablename = 'tbl_kr_confirm_bonds';
$row = $data->get_row("select * from tbl_kr_confirm_bonds where fk_bonds ='".$_GET[id]."'");
//print($row[no_ref]);
$settlement_date=$data->indo($row[settlement_date]);
$trade_date=$data->indo($row[trade_date]);
$acc_date=$data->indo($row[acc_date]);
$maturity_date=$data->indo($row[maturity_date]);
$date_confirm =$data->indo($row[date_confirm]);
$last_coupon_date =$data->indo($row[last_coupon_date]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Confirmation Letter Bonds </title>
</head>

<body >
<!--onload="window.print();"--> 
<table width="70%" align="center">
<tr>
<td>REKSADANA KERATON</td>
</tr>
<tr>
<td>Equity Tower Lt.25 Suite 25 B</td>
</tr>
<tr>
<td>Jl. Jend. Sudirman Kav. 52 -53</td>
</tr>
<tr>
<td>Phone : (021) 2903 -5050, Fax : (021)2903-5055</td>

</tr>
</table>
<br>


<table width="50%" align="center">
<tr>
<td align="center">
INTRUKSI PENJUALAN OBLIGASI
</td>
</tr>
</table>
<br>
<table width="70%" align="center" border="0">
<tr>
<td width="10%">Ref. No.</td>
<td width="2%">:</td>
<td width="29%"><?=$row[no_ref]?></td>
<tr>
<tr>
<td>Date</td>
<td>:</td>
<td><?=$date_confirm?></td>
<tr>

<tr>
<td>Attn.</td>
<td>:</td>
<td><?=$row[attn]?></td>
<tr>
<tr>
<td>Re.</td>
<td>:</td>
<td><?=$row[re]?></td>
<tr>
</table>

<table width="70%" align="center">
<tr>
<td>We would Like to confirm having<b>  SOLD BOND </b>with detail as follows :
</td>
</tr>
</table>
<table width="70%" align="center" border="0">
<tr>
<td width="10%"> Securitities Name</td>
<td width="2%">: </td>
<td width="29%"><?=$row[name_bonds]?> </td>
</tr>
<tr>
<td>Buyer </td>
<td>: </td>
<td> <?=$row[buyer]?> </td>
</tr>
<tr>
<td> Seller</td>
<td> :</td>
<td> <?=$row[seller]?> </td>
</tr>
<tr>
<td> Maturity date</td>
<td>: </td>
<td><?=$maturity_date?>  </td>
</tr>
<tr>
<td> Last Coupon Date</td>
<td>: </td>
<td><?=$last_coupon_date?>  </td>
</tr>
<tr>
<td> Quantity/ Nominal</td>
<td>: </td>
<td>IDR &nbsp; <?=number_format($row[quantity],2)?></td>
</tr>
<tr>
<td> Current Coupon</td>
<td>: </td>
<td><?=$row[current_coupon]?>  </td>
</tr>
<tr>
<td> Price</td>
<td> :</td>
<td><?=number_format($row[price],3)?> %</td>
</tr>
<tr>
<td> Amount</td>
<td> :</td>
<td>IDR &nbsp;<?=number_format($row[amount],0)?></td> 
</tr>
<tr>
<td>Accrued Interest </td>
<td> :</td>
<td>IDR &nbsp; <?=number_format($row[accrued],0)?></td>
</tr>
<tr>
<td> Cap. gain tax (20%)</td>
<td>: </td>
<td>IDR &nbsp; <?=number_format($row[gain_tax],0)?></td>
</tr>
<tr>
<td> Acc. Interest tax (20%)</td>
<td> :</td>
<td>IDR &nbsp; <?=number_format($row[acc_tax],0)?> </td>
</tr>
<tr>
<td><b> Total</b></td>
<td> :</td>
<td> <b>IDR &nbsp; <?=number_format($row[total],0)?></b></td>
</tr>
<tr>
<td> Trade Date</td>
<td> :</td>
<td>  <?=$trade_date?></td>
</tr>
<tr>
<td> Settlment Date</td>
<td> :</td>
<td> <?=$settlement_date?> </td>
</tr>
<tr>
<td> Acqusition Date</td>
<td> :</td>
<td>  <?=$acc_date?></td>
</tr>
<tr>
<td> Acqusition Price</td>
<td> :</td>
<td>  <?=number_format($row[acc_price],3)?> %</td>
</tr>
</table>
<br>
<br>
<table width="70%" align="center" border="0">
<tr>
<td width="25%">On Settlement Date</td>
<td><?=$settlement_date?></td>
<td></td>
</tr>
<tr>
<td>Kindly transfer</td>
<td>IDR <?=number_format($row[total],0)?> on <?=$settlement_date?></td>

</tr>

<tr>
<td colspan="2"><?=$row[bank_name]?></td>
<td></td>

</tr>

<tr>
<td colspan="2">A/C <?=$row[account_number]?></td>


</tr>
<tr>
<td colspan="2">IFO <?=$row[ifo]?></td>


</tr>
</table>
<br>
<br>
<table width="70%" align="center">
<tr>
<td>Sincerely<td>
</tr>
<tr>
<td>Reksadana Keraton<td>
</tr>
</table>
<br>
<br>
<table width="70%" align="center">
<tr>
<td><?=$row[direktur_name]?></td>
<tr>
<tr>
<td>Direktur</td>
<tr>
</table>
</body>
</html>