<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('liquidation_add.html');

$fundCode = '';
$lastNavDate = date('Y-m-d');
$paymentDate = date('Y-m-d');
$errorArr = array('','','');
$otherError='';

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
 		$fundCode = trim(htmlentities($_POST['inputFundCode']));
		$lastNavDate = trim(htmlentities($_POST['inputLastNavDate']));
		$paymentDate = trim(htmlentities($_POST['inputPaymentDate']));
		
		$gotError = false;
		if($fundCode==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		
        
		if (!$gotError){
			
			$query = "INSERT INTO tbl_kr_liquidation (
					im_code,
					fund_code,
					last_nav_date,
					payment_date,
					created_date,
					created_by,
					last_updated_date,
					last_updated_by,
					is_deleted
				)VALUES(
					'MU002',
				'$fundCode',
				'$lastNavDate',
				'$paymentDate',
				now(),
				'".$_SESSION['pk_id']."',
				now(),
				'".$_SESSION['pk_id']."',
				'0')";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='liquidation.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = "ADD - LIQUIDATION";
$dataRows = array (
	    'TEXT' => array('Fund Code <span class="redstar">*</span>','Last NAV Date <span class="redstar">*</span>','Payment Date'),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            //"<input type=text size='50' maxlength=16 name=inputFundCode value='$fundCode'>",
			$data->cb_fundcode('inputFundCode', $fundCode, ''),
		    $data->datePicker('inputLastNavDate', $lastNavDate,''),
            $data->datePicker('inputPaymentDate', $paymentDate,''),
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='liquidation.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>