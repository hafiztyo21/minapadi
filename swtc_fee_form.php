<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('subsredm_order_add.html');

$saCode = 'MU002';

$id = 0;
$transaction_date = date('Y-m-d');  
$transaction_type = 3;
$reference_no = '';
$investor_ac_no = '';
$switch_out_fund_code	= '';           
$amount = 0;
$amount_unit = 0;
$amount_all_unit = '';
$fee_charge_fund = '1';
$fee = 0;
$feeUnit = 0;               
$feePersen = 0;
$switch_in_fund_code = '';
$payment_date = date('Y-m-d');
$transfer_type = 1;
$sa_reference_no = '';

$errorArr = array();
for($i=0;$i<14;$i++)
	$errorArr[$i] = '';
$otherError='';

if($_GET['edit'] == 1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_swtc_fee WHERE swtc_fee_id = $id";
    $result = $data->get_row($query);

    $transaction_date = $result['transaction_date'];
    $reference_no = $result['reference_no'];     
    $investor_ac_no = $result['investor_ac_no'];
    $switch_out_fund_code	= $result['switch_out_fund_code'];          
    $amount = $result['amount'];
    $amount_unit = $result['amount_unit'];
    $amount_all_unit = $result['amount_all_unit'];
    $fee_charge_fund = $result['fee_charge_fund'];
    $fee = $result['fee'];
    $fee_unit = $result['fee_unit'];              
    $fee_persen = $result['fee_persen'];
    $switch_in_fund_code = $result['switch_in_fund_code'];
    $payment_date = $result['payment_date'];
    $transfer_type = $result['transfer_type'];
    $sa_reference_no = $result['sa_reference_no'];
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
 		$transaction_date = trim(htmlentities($_POST['transaction_date']));
        $reference_no = trim(htmlentities($_POST['reference_no']));
		$investor_ac_no = trim(htmlentities($_POST['investor_ac_no']));
		$switch_out_fund_code	= trim(htmlentities($_POST['switch_out_fund_code']));
        $amount	= trim(htmlentities($_POST['amount']));
        $amount_unit	= trim(htmlentities($_POST['amount_unit']));
        $amount_all_unit	= trim(htmlentities($_POST['amount_all_unit']));
        $fee_charge_fund	= trim(htmlentities($_POST['fee_charge_fund']));
        $fee	= trim(htmlentities($_POST['fee']));
        $fee_unit	= trim(htmlentities($_POST['fee_unit']));
        $fee_persen	= trim(htmlentities($_POST['fee_persen']));
		$switch_in_fund_code	= trim(htmlentities($_POST['switch_in_fund_code']));
        $payment_date = trim(htmlentities($_POST['payment_date']));
        $transfer_type	= trim(htmlentities($_POST['transfer_type']));
        $sa_reference_no	= trim(htmlentities($_POST['sa_reference_no']));
		
		$gotError = false;
        //validation
        if($reference_no==''){
			$errorArr[0] = "Reference No must be filled";
			$gotError = true;
		}
		if($investor_ac_no==''){
			$errorArr[1] = "Investor Fund Unit A/C No must be filled";
			$gotError = true;
		}
		if($switch_out_fund_code==''){
			$errorArr[2] = "Switch out Fund Code must be filled";
			$gotError = true;
		}
		if(($amount == '' || $amount == 0) && ($amount_unit == '' || $amount_unit == 0)){
			$errorArr[3] = "Amount(nominal) or Amount(Unit) must be filled";
			$gotError = true;
		}
		if($amount != '' && $amount != 0) $amount_all_unit = '';

        if(($fee != '' && $fee != 0) && ($fee_unit != '' && $fee_unit != 0) && ($fee_persen != '' && $fee_persen != 0)){
			$errorArr[7] = "Only one of Fee can be filled";
			$gotError = true;
		}
        if($fee_persen > 100){
            $errorArr[9] = "Fee (%) can not more than 100%";
			$gotError = true;
        }
        if($switch_in_fund_code==''){
			$errorArr[10] = "Switch in Fund Code must be filled";
			$gotError = true;
		}

        
		if (!$gotError){
			if($id == 0){
                $query = "INSERT INTO tbl_kr_swtc_fee (
                        transaction_date
                        , transaction_type 
                        , sa_code 
                        , reference_no
                        , investor_ac_no 
                        , switch_out_fund_code 
                        , amount 
                        , amount_unit 
                        , amount_all_unit
                        , fee_charge_fund 
                        , fee
                        , fee_unit
                        , fee_persen 
                        , switch_in_fund_code
                        , payment_date 
                        , transfer_type 
                        , sa_reference_no 
                        , created_date
                        , created_by
                        , last_updated_date
                        , last_updated_by
                        , is_deleted
                    )VALUES('$transaction_date',
                    '$transaction_type',
                    'MU002',
                    '$reference_no',
                    '$investor_ac_no',
                    '$switch_out_fund_code',
                    '$amount',
                    '$amount_unit',
                    '$amount_all_unit',
                    '$fee_charge_fund',
                    '$fee',
                    '$fee_unit',
                    '$fee_persen',
                    '$switch_in_fund_code',
                    '$payment_date',
                    '$transfer_type',
                    '$sa_reference_no',
                    now(),
                    '".$_SESSION['pk_id']."',
                    now(),
                    '".$_SESSION['pk_id']."',
                    '0')";
            }else{
                $query = "UPDATE tbl_kr_swtc_fee SET
                        transaction_date = '$transaction_date',
                        reference_no = '$reference_no',
                        investor_ac_no = '$investor_ac_no',
                        switch_out_fund_code = '$switch_out_fund_code',
                        amount = '$amount',
                        amount_unit = '$amount_unit',
                        amount_all_unit = '$amount_all_unit',
                        fee_charge_fund = '$fee_charge_fund',
                        fee = '$fee',
                        fee_unit = '$feeUnit',
                        fee_persen = '$feePersen',
                        switch_in_fund_code = '$switch_in_fund_code',
                        payment_date = '$payment_date',
                        transfer_type = '$transfer_type',
                        sa_reference_no = '$sa_reference_no',
                        last_updated_time = now(),
                        last_updated_by = '".$_SESSION['pk_id']."'
                    WHERE swtc_fee_id = '$id'";
            }
			
			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='swtc_fee.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}
$readonly = $transactionType == 1 ? 'readonly' : '';

$tittle = ($id == 0? "ADD" : "EDIT")." - SWITCHING ORDER";
$dataRows = array (
	    'TEXT' => array(
            'Reference No <span class="redstar">*</span>',
			'Transaction Date <span class="redstar">*</span>',
			//'Transaction Type',
			//'SA Code',
			'Invs. Fund Unit A/C No.',
			'Switch Out Fund Code',
			'Amount (Nominal)',
			'Amount (Unit)',
			'Amount (All Unit)',
            'Switching Fee Charge Fund',
			'Fee',
			'Fee (Unit)',
			'Fee (%)',
			'Switch In Fund Code',
			'Payment Date',
			'Transfer Type',
			'SA Reference No.'
			),
  	    'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':'),
	    'FIELD' => array (
            "<input type=text size='20' maxlength=8 name=reference_no value='$reference_no'>",
            $data->datePicker('transaction_date', $transaction_date,'')."<input type=hidden name=inputId id=inputId value='$id'>",
            //"<select name=transactionType onchange='changeType(this.value)'><option value=1 ". ($transactionType == 1 ? "selected=selected" : "") .">Subscription</option><option value=2 ".($transactionType == 2 ? "selected=selected" : "").">Redemption</option></select>",
            //"<input type=text size='50' maxlength=5 name=saCode value='$saCode'>",
            //"<input type=text size='50' maxlength=16 name=investor_ac_no value='$investor_ac_no'>",
            $data->cb_ifua('investor_ac_no', $investor_ac_no, ''),
            //"<input type=text size='50' maxlength=16 name=fundCode value='$fundCode'>",
			$data->cb_fundcode('switch_out_fund_code', $switch_out_fund_code),
            "<input type=number size='50' step=0.01 name=amount id=amount value='$amount'>",
            "<input type=number size='50' step=0.0001 name=amount_unit id=amount_unit value='$amount_unit' >",
            "<input type=checkbox name=amount_all_unit id=amount_all_unit value='Y' ".($amount_all_unit == 'Y' ? "checked" : "")." >",
            $data->cb_feechargefund('fee_charge_fund', $fee_charge_fund, ''),
            "<input type=number size='50' step=0.01 name=fee value='$fee'>",
            "<input type=number size='50' step=0.0001 name=fee_unit value='$fee_unit'>",
            "<input type=number size='50' step=0.01 name=fee_persen value='$fee_persen'>",
			$data->cb_fundcode('switch_in_fund_code', $switch_in_fund_code),
		    $data->datePicker('payment_date', $payment_date,''),
		    "<select name=transfer_type id=transfer_type><option value=1 ". ($transfer_type == 1 ? "selected=selected" : "") .">SKNBI</option><option value=2 ".($transfer_type == 2 ? "selected=selected" : "").">RTGS</option><option value=3 ".($transfer_type == 3 ? "selected=selected" : "").">N/A</option></select>",
		    "<input type=text size='50' maxlength=30 name=sa_reference_no value='$sa_reference_no'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='swtc_fee.php';\">");

$javascript = "";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>