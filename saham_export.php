<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$allocation = $_GET['allocation'];
$filename = $transaction_date."_shares.txt";

// output headers so that the file is downloaded rather than displayed
//header('Content-Type: text/csv; charset=utf-8');
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
try{


$output = fopen('php://output', 'w');

/*$myfile = fopen("newfile.txt", "w");
$txt = "Mickey Mouse\n";
fwrite($myfile, $txt);
$txt = "Minnie Mouse\n";
fwrite($myfile, $txt);
fclose($myfile);*/

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
/*
$query = "SELECT 
        '$transaction_date' AS trans_date,
        A.allocation,
        A.transaction_status,
        A.ta_reference_id,
        A.ta_reference_no,
        DATE_FORMAT(A.create_dt, '%Y%m%d') as trade_date, 
        DATE_FORMAT(A.settlement_date, '%Y%m%d') as settlement_date, 
        A.im_code, 
        A.br_code, 
        B.fund_code, 
    /*  A.code as security_code, 
        A.code AS cd,
        DATE_FORMAT(A.create_dt, '%Y-%m-%d') as crt_dt, 
        A.status, 
        A.harga, 
        A.lembar, 
        A.harga*A.lembar as trade_amount, 
        A.commission, 
        sales_tax, 
        levy,
        vat,
        other_charges,
        wht_on_commission,
        settlement_type, 
        remarks, 
        cancellation_reason 
        FROM tbl_kr_saham_tmp A JOIN tbl_kr_allocation B ON A.allocation = B.pk_id
        WHERE DATE_FORMAT(A.create_dt, '%Y-%m-%d') = '$transaction_date' ";
    */
$query="SELECT 
        '$transaction_date' AS trans_date,
        a.allocation,
        DATE_FORMAT(a.create_dt, '%Y%m%d') as trade_date, 
        /*DATE_FORMAT(a.settlement_date, '%Y%m%d') as settlement_date,*/
        a.levy,
        a.pajak AS vat,
        a.kpei,
        a.total as trade_amount,
        a.harga,
        a.lembar,
        a.type_buy AS tp,
        a.pajak_jual AS sales_tax,
        a.pajak_komisi AS wht_on_commission,
        a.komisi AS commission,
        a.code AS security_code, 
       /*
        b.securitas_code AS security_code,
        */
        c.fund_code,
        c.im_code,
		b.securitas_code AS sec_cd
        FROM tbl_kr_dealer_tmp a 
        LEFT JOIN tbl_kr_securitas b ON a.securitas_id=b.securitas_id 
        LEFT JOIN tbl_kr_allocation c ON a.allocation = c.pk_id
        WHERE DATE_FORMAT(a.create_dt, '%Y-%m-%d') = '".$transaction_date."'";

if($allocation != null && $allocation != ''){
    $query .= " AND A.allocation = '$allocation'";
}

$rows = $data->get_rowsexport($query);


fwrite($output, "\r\n");
// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $rows[$i]['levy']=$rows[$i]['levy']+$rows[$i]['kpei'];
	//$rows[$i]['levy']=number_format(rows[$i]['levy']);
	
    if($rows[$i]['status'] == 'Buy'){
        $rows[$i]['sales_tax']=0;
        $rows[$i]['status']=1;
        $rows[$i]['gross_settlement_amount'] = $rows[$i]['trade_amount'] + round($rows[$i]['commission']) + round($rows[$i]['sales_tax'],0) + round($rows[$i]['levy'],2) + round($rows[$i]['vat']) + $rows[$i]['other_charges'];
        $rows[$i]['net_settlement_amount'] = $rows[$i]['gross_settlement_amount'] - round($rows[$i]['wht_on_commission']);

    }else{
        $rows[$i]['status']=2;
        $rows[$i]['gross_settlement_amount'] = $rows[$i]['trade_amount'] - round($rows[$i]['commission']) - round($rows[$i]['sales_tax'],0) - round($rows[$i]['levy'],2) - round($rows[$i]['vat']) - $rows[$i]['other_charges'];
        $rows[$i]['net_settlement_amount'] = $rows[$i]['gross_settlement_amount'] + round($rows[$i]['wht_on_commission']);
    }

    if($rows[$i]['transaction_status'] == 'NEWM'){
        $rows[$i]['ta_reference_id'] = '';
        $rows[$i]['cancellation_reason'] = '';
    }
	
    $rows[$i]['commission']= str_replace(",", "", round($rows[$i]['commission'])) ;
    $rows[$i]['sales_tax']=str_replace(",", "", number_format(round($rows[$i]['sales_tax'],0),0));
    $rows[$i]['levy']=str_replace(",", "", number_format(round($rows[$i]['levy'],2),2));
    $rows[$i]['vat']=str_replace(",", "", round($rows[$i]['vat']));
    $rows[$i]['wht_on_commission']=str_replace(",", "", round($rows[$i]['wht_on_commission']));
    $rows[$i]['net_settlement_amount']=str_replace(",", "", round($rows[$i]['net_settlement_amount'],2));
    $rows[$i]['gross_settlement_amount']=str_replace(",", "", number_format(round($rows[$i]['gross_settlement_amount'],2),2));
    $rows[$i]['other_charges']=str_replace(",", "", round($rows[$i]['other_charges']));

    
    $str = $rows[$i]['transaction_status']."|".$rows[$i]['ta_reference_id']."|".$rows[$i]['ta_reference_no']
        ."|".$rows[$i]['trade_date']."|".$rows[$i]['settlement_date']."|".$rows[$i]['im_code']
        ."|".$rows[$i]['br_code']."|".$rows[$i]['fund_code']."|".$rows[$i]['security_code']
        ."|".$rows[$i]['status']."|".$rows[$i]['harga']
        ."|".$rows[$i]['lembar']."|".$rows[$i]['trade_amount']
        ."|".$rows[$i]['commission']."|".$rows[$i]['sales_tax']
        ."|".$rows[$i]['levy']."|".$rows[$i]['vat']."|".$rows[$i]['other_charges']
        ."|".$rows[$i]['gross_settlement_amount']."|".$rows[$i]['wht_on_commission']."|".$rows[$i]['net_settlement_amount']
        ."|".$rows[$i]['settlement_type']."|".$rows[$i]['remarks']."|".$rows[$i]['cancellation_reason']
        ."\r\n";

    fwrite($output, $str);
    //fputcsv($output, $rows[$i]);
}

fclose($output);
} catch(Exception $e){
    echo $e->getMessage();
}
?>
