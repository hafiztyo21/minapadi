<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'si.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new si;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('si_equity_unregistered.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_si_equity.si_equity_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$linkAdd = 'si_equity_unregistered_form.php';
$linkExport = 'si_equity_unregistered_export.php';
$tablename = 'tbl_kr_si_equity';

$btnView = '<input type="submit" name="btnView" value="View">';
$btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'\'">';
$btnExport = '<input type="button" name="btnExport" value="Export" onclick="alert(\'Please choose transaction date first\');">';

$datagrid = array();

if ($_POST['btnView']=='View'){
  	$transaction_date =  trim(htmlentities($_POST['transactionDate']));
	if (empty($transaction_date))
		$transaction_date = date("Y-m-d");

	$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';
	
  	$sql = "SELECT 
	  			si_equity_id
				, trade_id as TRADEID
				, trade_date as TRADEDATE
				, settlement_date as SETTLEMENTDATE
				, im_code as IMCODE
				, br_code as BRCODE
				, fund_code as FUNDCODE
			FROM $tablename
			WHERE DATE_FORMAT($tablename.trade_date, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0 AND `type`='2'
			ORDER BY $order_by $sort_order";
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->siData($sql, 'edit', 'si_equity_unregistered_form.php', 'delete', 'si_equity_unregistered.php', 'si_equity_id');
}

if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "UPDATE $tablename SET is_deleted=1 WHERE si_equity_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='si_equity_unregistered.php';</script>";
	}
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addVar('page','export',$btnExport);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>