<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('print_settlement.html');

/*header("Content-type: application/vnd.msword");
header("Content-Disposition: attachment; filename=Rincian".$prev.".doc");
header("Pragma: no-cache");
header("Expires: 0");*/

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_dealer_tmp.pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='desc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$tanggal = $_GET['tgl'];
$cd = $_GET['code'];
$type = $_GET['type_buy'];
$nama = $_SESSION['username'];
$settlement_id = $_SESSION['pk_id'];
$_SESSION['sql']='';

$sql  = "SELECT tbl_kr_dealer_tmp.*,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type 
FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
WHERE LEFT(create_dt,10) = '".$tanggal."'
AND tbl_kr_dealer_tmp.code = '".$cd."'
ORDER BY $order_by $sort_order";

$sql2  = "SELECT tbl_kr_dealer_tmp.*,lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type 
FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
WHERE LEFT(create_dt,10) = '".$tanggal."'
AND tbl_kr_dealer_tmp.code = '".$cd."'
ORDER BY $order_by $sort_order";

//$share = "SELECT lembar FROM tbl_kr_mst_saham WHERE code='".$cd."'";
//print_r ($sql);
$DG= $data->dataGridDeal($sql,'pk_id',$data->ResultsPerPage);
$rowLembar = $data->get_rows($sql2);
$tottrans = 0;
for ($i=0;$i<count($DG);$i++) {
	if ($DG[$i][type_buy]=='Buy') {
		$DG[$i][pajak_jual] = '';
		$DG[$i][nilai_jual] = '';
		$DG[$i][pajak_beli] = 'Rp. '.$DG[$i][pajak_beli];
		$DG[$i][nilai_beli] = 'Rp. '.$DG[$i][nilai_beli];
		$tottrans += $rowLembar[$i][lembar];
	}else {
		$DG[$i][pajak_beli] = '';
		$DG[$i][nilai_beli] = '';
		$DG[$i][pajak_jual] = 'Rp. '.$DG[$i][pajak_jual];
		$DG[$i][nilai_jual] = 'Rp. '.$DG[$i][nilai_jual];
		$tottrans -= $rowLembar[$i][lembar];
	}
}

if ($tottrans < 0) {
	$tottrans = '';
}else {
	$tottrans = number_format($tottrans,0);
}
$rowNilai = $data->get_rows($sql2);
$totnilai = 0;
$totjual = 0;
for ($i=0;$i<count($DG);$i++) {
	if ($DG[$i][type_buy]=='Buy') {
		$DG[$i][pajak_jual] = '';
		$DG[$i][nilai_jual] = '';
		$DG[$i][pajak_beli] = 'Rp. '.$DG[$i][pajak_beli];
		$DG[$i][nilai_beli] = 'Rp. '.$DG[$i][nilai_beli];
		$totnilai += $rowNilai[$i][nilai_beli];
	}else {
		$DG[$i][pajak_beli] = '';
		$DG[$i][nilai_beli] = '';
		$DG[$i][pajak_jual] = 'Rp. '.$DG[$i][pajak_jual];
		$DG[$i][nilai_jual] = 'Rp. '.$DG[$i][nilai_jual];
		$totjual += $rowNilai[$i][nilai_jual];
	}
}

if ($totnilai < 0) {
	$totnilai = '';
}elseif ($totjual < 0){
	$totjual = '';
}else {
	$totnilai = 'Rp. '.number_format($totnilai,0);
	$totjual = 'Rp. '.number_format($totjual,0);
	
}

$tmpl->addVar('page','nama',$nama);
$tmpl->addVar('page','date',$tanggal);
$tmpl->addVar('page','code',$cd);
$tmpl->addVar('page','type_buy',$type);
$tmpl->addVar('page','tottrans',$tottrans);
$tmpl->addVar('page','totnilai',$totnilai);
$tmpl->addRows('loopData',$DG);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->displayParsedTemplate('page');
?>