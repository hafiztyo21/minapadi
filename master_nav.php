<?php
    session_start();
    #session_destroy();
    #print_r($_SESSION);
    require_once 'global.inc.php';
    require_once $GLOBALS['CLASS'].'global.class.php';
    require_once $GLOBALS['CLASS'].'xajax.inc.php';
    require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
    require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
    $data = new globalFunction;
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
    <title>activeLink</title>
    <script type="text/javascript">
        //Sets active link for ul.li.a
        function activeLink(objLink){ 
            var list = document.getElementById('videoList').getElementsByTagName('a');
            for (var i = list.length - 1; i >= 0; i--){
                list[i].className='nonActiveVid';
            };
            objLink.className= 'activeVid';
        }
    </script>
</head>
<body id="activelink" onLoad="">    
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <ul class="activeVid" id="videoList">
                    <?php if($data->auth_boolean(7010,$_SESSION['pk_id'])){ ?>
                    <li> <a href="product.php?view=1" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Fund Info</a></li>
                    <?php } ?>
                    <?php if($data->auth_boolean(7020,$_SESSION['pk_id'])){ ?>
                    <li> <a href="product_sa.php?view=1" target="contentTabFrame" onClick="activeLink(this);">Fund SA</a></li>
                    <?php } ?>
                    <?php if($data->auth_boolean(7030,$_SESSION['pk_id'])){ ?>
                    <li> <a href="profile_static_data.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">IM Profile Static Data</a></li>
                    <?php } ?>
                </ul>
            </td>
        </tr>
    </table>
</body>
</html>