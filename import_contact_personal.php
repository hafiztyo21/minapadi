<?php
session_start();
include 'Classes/excel_reader2.php';
//include 'Classes/PHPExcel/Writer/Excel2007.php';
//include 'Classes/PHPExcel/IOFactory.php';
//include "excel_reader2.php";
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$datadb = new globalFunction();
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('import_contact_personal.html');
$tablename = 'tbl_kr_cus_sup';


if($_POST[Submit]) {
						$data = new Spreadsheet_Excel_Reader($_FILES['uploadedfile']['tmp_name']);
					 
					// membaca jumlah baris dari data excel
					$baris = $data->rowcount($sheet_index=0);
					 
					// nilai awal counter untuk jumlah data yang sukses dan yang gagal diimport
					$sukses = 0;
					$gagal = 0;
					 
					// import data excel mulai baris ke-2 (karena baris pertama adalah nama kolom)
					for ($i=2; $i<=$baris; $i++)
					{

						$sa_code = $data->val($i,2);
						$cus_sid = $data->val($i, 3);
						$cus_name = $data->val($i, 4);
						$middle_name = $data->val($i, 5);
						$last_name = $data->val($i, 6);
						$country_of_nationality = $data->val($i,7);
						$cus_no_identity=$data->val($i, 8);
						

						$berlaku_card=$data->val($i, 9);
						$cus_berlaku_card = '0000-00-00';
						if($berlaku_card != ''){
							$berlaku_a= substr($berlaku_card, 6);
							$berlaku_b= substr($berlaku_card, 3, 2);
							$berlaku_c= substr($berlaku_card, 0, 2);
							$cus_berlaku_card = "$berlaku_a-$berlaku_c-$berlaku_b";
						}

						$npwp = $data->val($i, 10);
						if($npwp != ''){
							$cus_no_npwp_1=	substr($npwp, 0, 2);
							$cus_no_npwp_2= substr($npwp, 2, 3);
							$cus_no_npwp_3= substr($npwp, 5, 3);
							$cus_no_npwp_4= substr($npwp, 8, 1);
							$cus_no_npwp_5= substr($npwp, 9, 3);
							$cus_no_npwp_6= substr($npwp, 12);
						}else{
							$cus_no_npwp_1 = '';
							$cus_no_npwp_2 = '';
							$cus_no_npwp_3 = '';
							$cus_no_npwp_4 = '';
							$cus_no_npwp_5 = '';
							$cus_no_npwp_6 = '';
						}
						
						
					  	$npwp_date=$data->val($i, 11);
						$npwp_regis_date = '0000-00-00';
						if($npwp_date != ''){
							$year= substr($npwp_date, 6);
							$month= substr($npwp_date, 3, 2);
							$day= substr($npwp_date, 0, 2);
							$npwp_regis_date = "$year-$month-$day";
						}

						$country_of_birth = $data->val($i, 12);
						$cus_tempat_lahir = $data->val($i, 13);

						$tgl_lahir=$data->val($i, 14);
						$cus_tgl_lahir = '0000-00-00';
						if($tgl_lahir != ''){
							$year= substr($tgl_lahir, 6);
							$month= substr($tgl_lahir, 3, 2);
							$day= substr($tgl_lahir, 0, 2);
							$cus_tgl_lahir = "$year-$month-$day";
						}

						$cus_sex = $data->val($i, 15);
						$cus_pendidikan = $data->val($i, 16);
						$cus_mother_name = $data->val($i, 17);
						$cus_agama = $data->val($i, 18);
						$cus_pekerjaan = $data->val($i, 19);
						$cus_penghasilan = $data->val($i, 20);

						$cus_status_kawin = $data->val($i, 21);
						$spouses_name = $data->val($i, 22);
						$investors_risk_profile = $data->val($i, 23);
						$cus_maksud_tujuan=$data->val($i, 24);
						$cus_sumber_dana=$data->val($i, 25);

						$asset_owner=$data->val($i, 26);
						$cus_address=$data->val($i, 27);
						$cus_city=$data->val($i, 28);
						$cus_kode_pos=$data->val($i, 29);
						$cus_address_c=$data->val($i, 30);

						$cus_city_c=$data->val($i, 31);
						$correspondence_city_name = $data->val($i, 32);
						$cus_kode_pos_c=$data->val($i, 33);
						$country_of_correspondence = $data->val($i, 34);
						$cus_address_t=$data->val($i, 35);

						$cus_city_t=$data->val($i, 36);
						$domicile_city_name=$data->val($i, 37);
						$cus_kode_pos_t=$data->val($i, 38);
						$country_of_domicile = $data->val($i, 39);
						$cus_telp=$data->val($i, 40);
						
						$cus_phone=$data->val($i, 41);
						$cus_fax=$data->val($i, 42);
						$cus_email=$data->val($i, 43);
						$statement_type=$data->val($i, 44);
						$fatca=$data->val($i, 45);

						$tin=$data->val($i, 46);
						$tin_country=$data->val($i, 47);
						$bank_bic_code1=$data->val($i, 48);
						$bank_bi_member_code1=$data->val($i, 49);
						$cus_bank_name=$data->val($i, 50);

						$bank_country1 = $data->val($i, 51);
						$bank_branch1 = $data->val($i, 52);
						$bank_ccy1 = $data->val($i, 53);
						$cus_account_number = $data->val($i, 54);
						$cus_account_name = $data->val($i, 55);

						$bank_bic_code2=$data->val($i, 56);
						$bank_bi_member_code2=$data->val($i, 57);
						$cus_bank_name2=$data->val($i, 58);
						$bank_country2 = $data->val($i, 59);
						$bank_branch2 = $data->val($i, 60);

						$bank_ccy2 = $data->val($i, 61);
						$cus_account_number2 = $data->val($i, 62);
						$cus_account_name2 = $data->val($i, 63);
						$bank_bic_code3=$data->val($i, 64);
						$bank_bi_member_code3=$data->val($i, 65);

						$bank_name3=$data->val($i, 66);
						$bank_country3 = $data->val($i, 67);
						$bank_branch3 = $data->val($i, 68);
						$bank_ccy3 = $data->val($i, 69);
						$bank_acc_no3 = $data->val($i, 70);
						$bank_acc_name3 = $data->val($i, 71);
						
						$client_code=$data->val($i, 72); //client code

					  //--------------------------------------------------------

						
					  	$cus_tgl_subscribe = ''; //insert : now(); update : -
					  	$cus_card_identity='';
					  	$cus_status_identity='';	  
						$cus_national='';
						$cus_status_domisili='';
						$cus_jumlah_tanggungan='';
						$cus_propinsi='';
						$cus_propinsi_t='';
						$cus_status_rt='';
						$cus_menempati_t='';
						$cus_company_name='';
						$cus_bd_usaha='';
						$cus_jabatan='';
						$cus_telp_c='';
						$cus_propinsi_c='';
						$cus_fax_c='';
						$cus_email_c='';
						$cus_masa_usaha='';
						$cus_other='';
					 	$cus_actived='1';
					
					
					//print($cus_no_identity);
					$cek_data=$datadb->get_value("select count(cus_id) from tbl_kr_cus_sup where cus_no_identity='".$cus_no_identity."'");
					if ($cek_data ==0){
					  // setelah data dibaca, sisipkan ke dalam tabel mhs
					  $query = 
					  "INSERT INTO $tablename(
						cus_code,
						cus_tgl_subscribe,
						cus_name,
						cus_card_identity,
						cus_no_identity,
						
						cus_berlaku_card,
						cus_status_identity,
						cus_tempat_lahir,
						
						
						cus_tgl_lahir,
						cus_sex,
						cus_mother_name,
						cus_agama,
						cus_national,
		
						cus_status_domisili,
						cus_status_kawin,
						cus_jumlah_tanggungan,
						cus_address,
						cus_city,
						
						cus_kode_pos,
						cus_propinsi,
						cus_telp,
						cus_phone,
						cus_fax,
						
						
						
						cus_email,
						cus_address_t,
						cus_city_t,
						cus_kode_pos_t,
						cus_propinsi_t,
						
						cus_status_rt,
						cus_menempati_t,
						cus_pendidikan,
						cus_pekerjaan,
						cus_company_name,
						
						cus_bd_usaha,
						cus_jabatan,
						cus_address_c,
						cus_telp_c,
						cus_city_c,
						cus_kode_pos_c,
						
						cus_propinsi_c,
						cus_fax_c,
						cus_email_c,
						cus_masa_usaha,
						cus_other,
						
						cus_actived,
						cus_no_npwp_1,
						cus_no_npwp_2,
						cus_no_npwp_3,
						cus_no_npwp_4,
						
						cus_no_npwp_5,
						cus_no_npwp_6,
						cus_bank_name,
						cus_account_name,
						cus_account_number,
						
						cus_bank_name2,
						cus_account_name2,
						cus_account_number2,
						cus_sumber_dana,
						cus_maksud_tujuan,
						cus_penghasilan,
						cus_sid,
						middle_name
						, last_name
						, country_of_nationality
						, npwp_regis_date

						, country_of birth
						, spouses_name
						, investors_risk_profile
						, asset_owner
						, bank_bic_code1

						, bank_bi_member_code1
						, bank_country1
						, bank_branch1
						, bank_ccy1
						, bank_bic_code2

						, bank_bi_member_code2
						, bank_country2
						, bank_branch2
						, bank_ccy2
						, bank_bic_code3

						, bank_bi_member_code3
						, bank_name3
						, bank_country3
						, bank_branch3
						, bank_ccy3

						, bank_acc_no3
						, bank_acc_name3
						, statement_type
						, fatca
						, tin
						, tin_issuance_country
						, country_of_correspondence
						, country_of_domicily
						, client_code
						, created_date
						) VALUES 
					  ('',
					  now(),
					  '$cus_name',
					  '$cus_card_identity',
					  '$cus_no_identity',
					  
					  '$cus_berlaku_card',
					  '$cus_status_identity',
					  '$cus_tempat_lahir',
					  
					  '$cus_tgl_lahir',
					  '$cus_sex',
					  
					  '$cus_mother_name',
					  '$cus_agama',
					  '$cus_national',
					  
					  
					  '$cus_status_domisili',
					  '$cus_status_kawin',
					  '$cus_jumlah_tanggungan',
					  '$cus_address',
					  '$cus_city',
					  
					  
					  '$cus_kode_pos',
					  '$cus_propinsi',
					  '$cus_telp',
					  '$cus_phone',
					  '$cus_fax',
					  
					  '$cus_email',
					  '$cus_address_t',
					  '$cus_city_t',
					  '$cus_kode_pos_t',
					  '$cus_propinsi_t',
					  
					  '$cus_status_rt',
					  '$cus_menempati_t',
					  '$cus_pendidikan',
					  '$cus_pekerjaan',
					  '$cus_company_name',
					  
					  '$cus_bd_usaha',
					  '$cus_jabatan',
					  '$cus_address_c',
					  '$cus_telp_c',
					  '$cus_city_c',
					  '$cus_kode_pos_c',
					  
					   '$cus_propinsi_c',
					  '$cus_fax_c',
					  '$cus_email_c',
					  '$cus_masa_usaha',
					  '$cus_other',
					  
					  
					  
					   '$cus_actived',
					  '$cus_no_npwp_1',
					  '$cus_no_npwp_2',
					  '$cus_no_npwp_3',
					  '$cus_no_npwp_4',
					  
					   '$cus_no_npwp_5',
					  '$cus_no_npwp_6',
					  '$cus_bank_name',
					  '$cus_account_name',
					  '$cus_account_number',
					  
					   '$cus_bank_name2',
					  '$cus_account_name2',
					  '$cus_account_number2',
					  '$cus_sumber_dana',
					  '$cus_maksud_tujuan',
					  '$cus_penghasilan',
					  '$cus_sid'
					  , '".$middle_name."'
						, '".$last_name."'
						, '".$country_of_nationality."'
						, '".$npwp_regis_date."'

						, '".$country_of_birth."'
						, '".$spouses_name."'
						, '".$investors_risk_profile."'
						, '".$asset_owner."'
						, '".$bank_bic_code1."'

						, '".$bank_bi_member_code1."'
						, '".$bank_country1."'
						, '".$bank_branch1."'
						, '".$bank_ccy1."'
						, '".$bank_bic_code2."'

						, '".$bank_bi_member_code2."'
						, '".$bank_country2."'
						, '".$bank_branch2."'
						, '".$bank_ccy2."'
						, '".$bank_bic_code3."'

						, '".$bank_bi_member_code3."'
						, '".$bank_name3."'
						, '".$bank_country3."'
						, '".$bank_branch3."'
						, '".$bank_ccy3."'

						, '".$bank_acc_no3."'
						, '".$bank_acc_name3."'
						, '".$statement_type."'
						, '".$fatca."'
						, '".$tin."'
						, '".$tin_country."',
						, '".$country_of_correspondence."'
						, '".$country_of_domicile."'
					  	, '".$client_code."'
						, now()
					  )";
					  }
					  else
					  {
					$query= "UPDATE ".$tablename." SET 
					
					cus_name 	= '".$cus_name."',
					
					cus_no_identity='".$cus_no_identity."',
					cus_berlaku_card='".$cus_berlaku_card."',
					
					cus_tempat_lahir='".$cus_tempat_lahir."',
					
					
					cus_tgl_lahir=	'".$cus_tgl_lahir."',
					cus_sex='".$cus_sex."',
					cus_mother_name='".$cus_mother_name."',
					cus_agama=	'".$cus_agama."',
					
					
					
					cus_status_kawin='".$cus_status_kawin."',
					
					cus_address='".$cus_address."',
					cus_city='".$cus_city."',
					
					
					cus_kode_pos='".$cus_kode_pos."',
					
					cus_telp='".$cus_telp."',
					cus_phone='".$cus_phone."',
					cus_fax='".$cus_fax."',
					
					
					
					cus_email=	'".$cus_email."',
					cus_address_t='".$cus_address_t."',
					cus_city_t='".$cus_city_t."',
					cus_kode_pos_t='".$cus_kode_pos_t."',
					
					
					
					
					cus_pendidikan='".$cus_pendidikan."',
					cus_pekerjaan='".$cus_pekerjaan."',
					
					
					
					
					cus_address_c='".$cus_address_c."',
					
					cus_city_c='".$cus_city_c."',
					cus_kode_pos_c='".$cus_kode_pos_c."',
					
					cus_no_npwp_1='".$cus_no_npwp_1."',
					cus_no_npwp_2='".$cus_no_npwp_2."',
					cus_no_npwp_3='".$cus_no_npwp_3."',
					cus_no_npwp_4='".$cus_no_npwp_4."',
					
					cus_no_npwp_5='".$cus_no_npwp_5."',
					cus_no_npwp_6='".$cus_no_npwp_6."',
					cus_bank_name='".$cus_bank_name."',
					cus_account_name='".$cus_account_name."',
					cus_account_number='".$cus_account_number."',
					
					cus_bank_name2='".$cus_bank_name2."',
					cus_account_name2='".$cus_account_name2."',
					cus_account_number2='".$cus_account_number2."',
					
					
					cus_penghasilan='".$cus_penghasilan."',
					cus_sumber_dana='".$cus_sumber_dana."',
					cus_maksud_tujuan='".$cus_maksud_tujuan."',
					cus_sid='".$cus_sid."',

					middle_name = '".$middle_name."',
					last_name = '".$last_name."',
					country_of_nationality = '".$country_of_nationality."',
					npwp_regis_date = '".$npwp_regis_date."',
					country_of_birth = '".$country_of_birth."',
					spouses_name = '".$spouses_name."',
					investors_risk_profile = '".$investors_risk_profile."',
					asset_owner = '".$asset_owner."',
					bank_bic_code1 = '".$bank_bic_code1."',
					bank_bi_member_code1 = '".$bank_bi_member_code1."',
					bank_country1 = '".$bank_country1."',
					bank_branch1 = '".$bank_branch1."',
					bank_ccy1 = '".$bank_ccy1."',
					bank_bic_code2 = '".$bank_bic_code2."',
					bank_bi_member_code2 = '".$bank_bi_member_code2."',
					bank_country2 = '".$bank_country2."',
					bank_branch2 = '".$bank_branch2."',
					bank_ccy2 = '".$bank_ccy2."',
					bank_bic_code3 = '".$bank_bic_code3."',
					bank_bi_member_code3 = '".$bank_bi_member_code3."',
					bank_name3 = '".$bank_name3."',
					bank_country3 = '".$bank_country3."',
					bank_branch3 = '".$bank_branch3."',
					bank_ccy3 = '".$bank_ccy3."',
					bank_acc_no3 = '".$bank_acc_no3."',
					bank_acc_name3 = '".$bank_acc_name3."',
					statement_type = '".$statement_type."',
					fatca = '".$fatca."',
					tin = '".$tin."',
					tin_issuance_country = '".$tin_country."',
					country_of_correspondence = '".$country_of_correspondence."',
					country_of_domicily = '".$country_of_domicile."',
					client_code = '".$client_code."'
						WHERE cus_no_identity = '".$cus_no_identity."'";
					  
					  }
					//print($query);
						if ($datadb->inpQueryReturnBool($query))
						{	//echo "<script>alert('SUKSES');</script>";	
						 $sukses++;}
						else
						{	//echo "<script>alert('GAGAL');</script>";
						$gagal++;}
			
					 
					  // jika proses insert data sukses, maka counter $sukses bertambah
					  // jika gagal, maka counter $gagal yang bertambah
					
				
					}
					 
					// tampilan status sukses dan gagal
					echo "<h3>Proses import data selesai.</h3>";
					echo "<p>Jumlah data yang sukses diimport : ".$sukses."<br>";
					echo "Jumlah data yang gagal diimport : ".$gagal."</p>";
	
	
}



$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->addVar('page', 'tittle',$tittle);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>