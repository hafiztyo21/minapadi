<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('liquidation_edit.html');

$id = 0;
$fundCode = '';
$lastNavDate = date('Y-m-d');
$paymentDate = date('Y-m-d');
$errorArr = array('','','');
$otherError='';

if($_GET['edit']==1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_liquidation WHERE liquidation_id = $id";
    $result = $data->get_row($query);
    $fundCode = $result['fund_code'];
    $lastNavDate = $result['last_nav_date'];
    $paymentDate = $result['payment_date'];
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
 		$fundCode = trim(htmlentities($_POST['inputFundCode']));
		$lastNavDate = trim(htmlentities($_POST['inputLastNavDate']));
        $paymentDate = trim(htmlentities($_POST['inputPaymentDate']));
        		
		$gotError = false;
		if($fundCode==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}
		
        
		if (!$gotError){
			
			$query = "UPDATE tbl_kr_liquidation SET
					fund_code = '$fundCode',
					last_nav_date = '$lastNavDate',
					payment_date = '$paymentDate',
					last_updated_date = now(),
					last_updated_by = '".$_SESSION['pk_id']."'
				WHERE liquidation_id = '$id'";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Edit Success');window.location='liquidation.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}



$tittle = "EDIT - LIQUIDATION";
$dataRows = array (
	    'TEXT' => array('Fund Code <span class="redstar">*</span>','Last NAV Date <span class="redstar">*</span>','Payment Date'),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            //"<input type=text size='50' maxlength=16 name=inputFundCode value='$fundCode'> <input type=hidden name=inputId value=$id>",
			$data->cb_fundcode('inputFundCode', $fundCode,''),
		    $data->datePicker('inputLastNavDate', $lastNavDate,''),
            $data->datePicker('inputPaymentDate', $paymentDate,''),
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='liquidation.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');

?>