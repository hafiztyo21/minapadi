<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$filename = $transaction_date."_si_fixed_income_between_funds.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    si_fixed_income_id,
    trade_id,
    DATE_FORMAT(trade_date,'%Y%m%d') as trade_date,
    DATE_FORMAT(settlement_date,'%Y%m%d') as settlement_date,
    fund_code,
    fund_code_buyer,
    security_code,
    buy_sell,
    price,
    face_value,
    proceeds,
    DATE_FORMAT(last_coupon_date,'%Y%m%d') as last_coupon_date,
    DATE_FORMAT(next_coupon_date,'%Y%m%d') as next_coupon_date,
    accrued_days,
    accrued_interest_amount,
    other_fee,
    capital_gain_tax,
    interest_income_tax,
    withholding_tax,
    net_proceeds,
    settlement_type,
    seller_tax_id,
    purpose_of_transaction,
    statutory_type,
    remarks
 
    FROM tbl_kr_si_fixed_income
    WHERE DATE_FORMAT(trade_date, '%Y-%m-%d') = '$transaction_date' 
    AND is_deleted=0 AND type = '3'";

$rows = $data->get_rows2($query);

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $arr = array();
    $var = $rows[$i];

    $idx = 0;
    $arr[$idx] = '1';                                               $idx++;     //0.
    $arr[$idx] = $var['trade_id'];                                               $idx++;     //0.
    $arr[$idx] = $var['trade_date'];                                                $idx++;
    $arr[$idx] = $var['settlement_date'];                                       $idx++;
    $arr[$idx] = $var['fund_code'];                                             $idx++;     //0.
    $arr[$idx] = $var['fund_code_buyer'];                                             $idx++;     //0.
    $arr[$idx] = $var['security_code'];                                         $idx++;     //0.
    $arr[$idx] = $var['buy_sell'];                                            $idx++;     //0.
    $arr[$idx] = $var['price'];                                                 $idx++;     //0.
    $arr[$idx] = $var['face_value'];                                                   $idx++;     //0.
    $arr[$idx] = $var['proceeds'];                                          $idx++;     //0.
    $arr[$idx] = $var['last_coupon_date'];                                                  $idx++;     //0.
    $arr[$idx] = $var['next_coupon_date'];                                                   $idx++;     //0.
    $arr[$idx] = $var['accrued_days'];                                         $idx++;     //0.
    $arr[$idx] = $var['accrued_interest_amount'];                               $idx++;     //0.
    $arr[$idx] = $var['other_fee'];                                     $idx++;     //0.
    $arr[$idx] = $var['capital_gain_tax'];                                     $idx++;     //0.
    $arr[$idx] = $var['interest_income_tax'];                                     $idx++;     //0.
    $arr[$idx] = $var['withholding_tax'];                                     $idx++;     //0.
    $arr[$idx] = $var['net_proceeds'];                                 $idx++;     //0.
    $arr[$idx] = $var['settlement_type'];                                       $idx++;     //0.
    $arr[$idx] = $var['seller_tax_id'];                                       $idx++;     //0.
    $arr[$idx] = $var['purpose_of_transaction'];                                       $idx++;     //0.
    $arr[$idx] = $var['statutory_type'];                                       $idx++;     //0.
    $arr[$idx] = $var['remarks'];                                               $idx++;     //0.

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);

    //=======jika ada tax========
    $query = "SELECT 
        si_fixed_income_tax_id,
        face_value,
        DATE_FORMAT(acquisition_date,'%Y%m%d') as acquisition_date,
        acquisition_price_persen,
        capital_gain,
        days_of_holding_interest,
        holding_interest_amount,
        total_taxable_income,
        tax_rate_in_persen,
        tax_amount
     FROM tbl_kr_si_fixed_income_tax
     WHERE is_deleted =0 AND si_fixed_income_id = '".$var['si_fixed_income_id']."'
     ORDER BY si_fixed_income_tax_id ASC
     ";

     $rows2 = $data->get_rows2($query);
     for($j=0;$j<count($rows2);$j++){

        $arr2 = array();
        $var2 = $rows2[$j];

        $idx = 0;
        $arr2[$idx] = '2';                                               $idx++;     //0.
        $arr2[$idx] = $var['trade_id'];                                  $idx++;     //0.
        $arr2[$idx] = $var2['face_value'];                                  $idx++;     //0.
        $arr2[$idx] = $var2['acquisition_date'];                            $idx++;     //0.
        $arr2[$idx] = $var2['acquistion_price_persen'];                     $idx++;     //0.
        $arr2[$idx] = $var2['capital_gain'];                                $idx++;     //0.
        $arr2[$idx] = $var2['days_of_holding_interest'];                    $idx++;     //0.
        $arr2[$idx] = $var2['holding_interest_amount'];                     $idx++;     //0.
        $arr2[$idx] = $var2['total_taxable_income'];                        $idx++;     //0.
        $arr2[$idx] = $var2['tax_rate_in_persen'];                          $idx++;     //0.
        $arr2[$idx] = $var2['tax_amount'];                                  $idx++;     //0.

        $str = "";
        for($k=0; $k<count($arr2);$k++){
            if($k > 0) $str .="|";
            $str.= $arr2[$k];
        }
        $str.="\r\n";
        //fputcsv($output, $arr);
        fwrite($output, $str);
     }
     //====================================

}

fclose($output);
?>
