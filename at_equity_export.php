<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$filename = $transaction_date."_equity_trade_allocation.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    trade_id,
    trade_date,
    settlement_date,
    im_code,
    br_code,
    fund_code,
    security_code,
    trade_type,
    price,
    qty,
    trade_amount,
    commission,
    sales_tax,
    levy,
    vat,
    other_charges,
    gross_settlement_amount,
    wht_on_commission,
    net_settlement_amount,
    settlement_type,
    remarks
 
    FROM tbl_kr_equity 
    WHERE DATE_FORMAT(tbl_kr_equity.trade_date, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0";

$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $arr = array();
    $var = $rows[$i];

    $idx = 0;
    $arr[$idx] = $var['trade_id'];                                               $idx++;     //0.
    $date = '';
    if($var['trade_date'] != null && $var['trade_date'] != '' && $var['trade_date'] != '0000-00-00')
        $date = str_replace('-','',$var['trade_date']);

    $arr[$idx] = $date;
    
    $date = '';
    if($var['settlement_date'] != null && $var['settlement_date'] != '' && $var['settlement_date'] != '0000-00-00')
        $date = str_replace('-','',$var['settlement_date']);

    $arr[$idx] = $date;                                                      $idx++;

    $arr[$idx] = $var['im_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['br_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['fund_code'];                                             $idx++;     //0.
    $arr[$idx] = $var['security_code'];                                         $idx++;     //0.
    $arr[$idx] = $var['trade_type'];                                            $idx++;     //0.
    $arr[$idx] = $var['price'];                                                 $idx++;     //0.
    $arr[$idx] = $var['qty'];                                                   $idx++;     //0.
    $arr[$idx] = $var['trade_amount'];                                          $idx++;     //0.
    $arr[$idx] = $var['commission'];                                            $idx++;     //0.
    $arr[$idx] = $var['sales_tax'];                                             $idx++;     //0.
    $arr[$idx] = $var['levy'];                                                  $idx++;     //0.
    $arr[$idx] = $var['vat'];                                                   $idx++;     //0.
    $arr[$idx] = $var['other_charges'];                                         $idx++;     //0.
    $arr[$idx] = $var['gross_settlement_amount'];                               $idx++;     //0.
    $arr[$idx] = $var['wht_on_commission'];                                     $idx++;     //0.
    $arr[$idx] = $var['net_settlement_amount'];                                 $idx++;     //0.
    $arr[$idx] = $var['settlement_type'];                                       $idx++;     //0.
    $arr[$idx] = $var['remarks'];                                               $idx++;     //0.

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
}

fclose($output);
?>
