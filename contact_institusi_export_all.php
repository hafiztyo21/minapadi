<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
//$transaction_date = $_GET['transactionDate'];
$filename = "kyc_institusi_all.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT a.*,b.kota FROM tbl_kr_cus_institusi a LEFT JOIN tbl_kr_stock_kota b ON a.cus_kota_pt = b.kode_kota ";

$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

//-------------STATIC FIELD-----------
$saCode = 'MU002';
//------------------------------------

$str = "Type|SA Code|SID|Company Name|Country of Domicile|SIUP No|SIUP Expiration Date|SKD No|SKD Expiration Date|NPWP No|NPWP Registration Date";
$str .= "|Country of Establishment|Place of Establishment|Date of Establishment|Articles of Association No.";
$str .= "|Company Type|Company Characteristic|Income Level|Investor Risk Profile|Investment Objective|Source of Fund";
$str .= "|Asset Owner|Company Address|Company City Code|Company City Name|Company Postal Code|Country of Company|Office Phone|Facsimile|Email";
$str .= "|Statement Type|Authorized Person 1 - First Name|Authorized Person 1 - Middle Name|Authorized Person 1 - Last Name|Authorized Person 1 - Position";
$str .= "|Authorized Person 1 - Mobile Phone|Authorized Person 1 - Email|Authorized Person 1 - NPWP No|Authorized Person 1 - KTP No";
$str .= "|Authorized Person 1 - KTP Expiration Date|Authorized Person 1 - Passport No|Authorized Person 1 - Passport Expiration Date";
$str .= "|Authorized Person 2 - First Name|Authorized Person 2 - Middle Name|Authorized Person 2 - Last Name|Authorized Person 2 - Position";
$str .= "|Authorized Person 2 - Mobile Phone|Authorized Person 2 - Email|Authorized Person 2 - NPWP No|Authorized Person 2 - KTP No";
$str .= "|Authorized Person 2 - KTP Expiration Date|Authorized Person 2 - Passport No|Authorized Person 2 - Passport Expiration Date";
$str .= "|Asset Information for the past 3 Year - Last Year|Asset Information for the past 3 Year - 2 Years Ago|Asset Information for the past 3 Year - 3 Years Ago";
$str .= "|Profit Information for the past 3 Year - Last Year|Profit Information for the past 3 Year - 2 Years Ago|Profit Information for the past 3 Year - 3 Years Ago";
$str .= "|FATCA|TIN/Foreign TIN|TIN/Foreign TIN Issuance Country|GIIN|Substantial U.S Owner Name|Substantial U.S Owner Address|Substantial U.S Owner TIN";
$str .= "|REDM Payment Bank BIC Code 1|REDM Payment Bank BI Member Code 1|REDM payment Bank Name 1|REDM Payment Bank Country 1";
$str .= "|REDM Payment Bank Branch 1|REDM Payment A/C CCY 1|REDM Payment A/C No. 1|REDM Payment A/C Name 1";
$str .= "|REDM Payment Bank BIC Code 2|REDM Payment Bank BI Member Code 2|REDM payment Bank Name 2|REDM Payment Bank Country 2";
$str .= "|REDM Payment Bank Branch 2|REDM Payment A/C CCY 2|REDM Payment A/C No. 2|REDM Payment A/C Name 2";
$str .= "|REDM Payment Bank BIC Code 3|REDM Payment Bank BI Member Code 3|REDM payment Bank Name 3|REDM Payment Bank Country 3";
$str .= "|REDM Payment Bank Branch 3|REDM Payment A/C CCY 3|REDM Payment A/C No. 3|REDM Payment A/C Name 3";
$str .= "|Client Code\r\n";
//fwrite($output, $str);

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){
    $arr = array();
    $var = $rows[$i];
    
    $type = '1';
    if($var['cus_ins_sid'] != null && $var['cus_ins_sid'] != '')
        $type = '2';

    $idx = 0;
    $arr[$idx] = $type;                                               $idx++;     //0. type : 1-input, 2-amendment
    $arr[$idx] = $saCode;                                             $idx++;     //1. sa code
    $arr[$idx] = $type == '1' ? '' : $var['cus_ins_sid'];             $idx++;     //2. sid 
    $arr[$idx] = $var['cus_name'];                                    $idx++;     //3. company name
    $arr[$idx] = $var['country_of_domicily'];                         $idx++;     //4. country of domicily 
    $arr[$idx] = $var['cus_no_siup'];                                 $idx++;     //5. siup no.
    $arr[$idx] = str_replace('-','',$var['cus_tgl_siup']);            $idx++;     //6. siup expired date
    $arr[$idx] = $var['cus_no_skd'];                                  $idx++;     //7. skd no.

    $skdDate = '';
    if($var['skd_expiration_date'] != null && $var['skd_expiration_date'] != '' && $var['skd_expiration_date'] != '0000-00-00')
        $skdDate = str_replace('-','',$var['skd_expiration_date']);

    $arr[$idx] = $skdDate;                                            $idx++;     //8. skd expired date
    $arr[$idx] = $var['cus_no_npwp'];                                 $idx++;     //9. npwp no.
    $npwpDate = '';
    if($var['npwp_registration_date'] != null && $var['npwp_registration_date'] != '' && $var['npwp_registration_date'] != '0000-00-00')
        $npwpDate = str_replace('-','',$var['npwp_registration_date']);

    $arr[$idx] = $npwpDate;                                           $idx++;     //10. npwp registration date
    $arr[$idx] = $var['country_of_establisment'];                     $idx++;     //11. country of establisment
    $arr[$idx] = $var['cus_tempat_pt'];                               $idx++;     //12. place of establisment

    $estaDate = '';
    if($var['cus_tgl_pt'] != null && $var['cus_tgl_pt'] != '' && $var['cus_tgl_pt'] != '0000-00-00')
        $estaDate = str_replace('-','',$var['cus_tgl_pt']);

    $arr[$idx] = $estaDate;                                           $idx++;     //13. date of establisment
    $arr[$idx] = $var['articles_of_association_no'];                  $idx++;     //14. articles of association no

    $companyType= '';
    switch($var['cus_tipe']){
        case '1' : $companyType = 'Corporate'; break;
        case '2' : $companyType = 'Foundation'; break;
        case '3' : $companyType = 'Financial Institution'; break;
        case '4' : $companyType = 'Insurance'; break;
        case '5' : $companyType = 'Mutual Fund'; break;
        case '6' : $companyType = 'Pension Fund'; break;
        case '7' : $companyType = 'Securities Company'; break;
        case '8' : $companyType = 'Others'; break;
    }

    $arr[$idx] = $companyType;                                         $idx++;     //15. company type

    $characteristic = '';
    switch($var['cus_karakteristik']){
        case '1' : $characteristic = 'State Owned Company/Public'; break;
        case '2' : $characteristic = 'Private'; break;
        case '3' : $characteristic = 'Social'; break;
        case '4' : $characteristic = 'Joint Venture'; break;
        case '5' : $characteristic = 'PMA/Foreign Investment Company'; break;
        case '6' : $characteristic = 'Family Company'; break;
        case '7' : $characteristic = 'Afiliation'; break;
        case '8' : $characteristic = 'Others'; break;
    }

    $arr[$idx] = $characteristic;                                     $idx++;     //16. company characteristic

    $incomelevel = '';
    switch($var['cus_penghasilan']){
        case '1' : $incomelevel = '< 1 billion/Year'; break;
        case '2' : $incomelevel = '> 1– 5 billion/Year'; break;
        case '3' : $incomelevel = '> 5 – 10 billion/Year'; break;
        case '4' : $incomelevel = '> 10 – 50 billion/Year'; break;
        case '5' : $incomelevel = '> 50 billion/Year'; break;
    }

    $arr[$idx] = $incomelevel;                                        $idx++;     //17. income level

    $risk = "";
    switch($var['investors_riks_profile']){
        case '1' : $risk = 'Low'; break;
        case '2' : $risk = 'Low to Moderate'; break;
        case '3' : $risk = 'Moderate'; break;
        case '4' : $risk = 'Moderate to High'; break;
        case '5' : $risk = 'High'; break;
    }

    $arr[$idx] = $risk;                                               $idx++;     //18. investors risk profile

    $objective = '';
    switch($var['cus_inves']){
        case '1' : $objective = 'Gain from price margin'; break;
        case '2' : $objective = 'Investment'; break;
        case '3' : $objective = 'Speculation'; break;
        case '4' : $objective = 'Gain the revenue or income'; break;
        case '5' : $objective = 'Others'; break;
    }

    $arr[$idx] = $objective;                                         $idx++;     //19. invesment objective

    $source = '';
    switch($var['cus_sumber_dana']){
        case '1' : $source = 'Business Profit'; break;
        case '2' : $source = 'Pension Funds'; break;
        case '3' : $source = 'Saving Interest'; break;
        case '4' : $source = 'Investment Gain'; break;
        case '5' : $source = 'Others'; break;
    }

    $arr[$idx] = $source;                                             $idx++;     //20. source of fund

    $assetowner = '';
    switch($var['asset_owner']){
        case '1' : $assetowner = 'Myself'; break;
        case '2' : $assetowner = 'Representing Other Party'; break;
    }

    $arr[$idx] = $assetowner;                                 $idx++;     //21. asset owner
    $arr[$idx] = $var['cus_alamat_pt'];                               $idx++;     //22. company address
    $arr[$idx] = $var['cus_kota_pt'];                                 $idx++;     //23. company city code
    $arr[$idx] = $var['kota'];                                        $idx++;     //24. company city name
    $arr[$idx] = $var['cus_kode_pos_pt'];                             $idx++;     //25. company postal code

    $countryOfCompany = 'ID';
    if($var['country_of_company'] != null && $var['country_of_company'] != '')
        $countryOfCompany = $var['country_of_company'];

    $arr[$idx] = $countryOfCompany;                                   $idx++;     //26. country of company
    $arr[$idx] = $var['cus_telp_pt'];                                 $idx++;     //27. company telp
    $arr[$idx] = $var['cus_fax_pt'];                                  $idx++;     //28. company fax
    $arr[$idx] = $var['cus_email_pt'];                                $idx++;     //29. company email

    $statementType = '1';
    if($var['statement_type'] != null && $var['statement_type'] != '')
        $statementType = $var['statement_type'];

    switch($statementType){
        case '1': $statementType='Hard Copy';break;
        case '2': $statementType='e-Statement';break;
    }

    $arr[$idx] = $statementType;                                     $idx++;     //30. statement type

    $arr[$idx] = $var['cus_nama_'];                                  $idx++;     //31. authorized person first name 1
    $arr[$idx] = $var['authorized_person_middle_name1'];             $idx++;     //32. authorized person middle name 1
    $arr[$idx] = $var['authorized_person_last_name1'];               $idx++;     //33. authorized person last name 1
    $arr[$idx] = $var['cus_jabatan'];                                $idx++;     //34. authorized person position 1
    $arr[$idx] = $var['cus_telp_hp'];                                $idx++;     //35. authorized person telephone 1
    $arr[$idx] = $var['authorized_person_email1'];                   $idx++;     //36. authorized person email 1
    $arr[$idx] = $var['authorized_person_npwp1'];                    $idx++;     //37. authorized person npwp 1
    $arr[$idx] = $var['cus_no_KTP'];                                 $idx++;     //38. authorized person KTP no 1

    $authoKtpDate1 = '';
    if($var['authorized_person_ktp_expiration_date1'] != null && $var['authorized_person_ktp_expiration_date1'] != '' && $var['authorized_person_ktp_expiration_date1'] != '0000-00-00')
        $authoKtpDate1 = str_replace('-','',$var['authorized_person_ktp_expiration_date1']);
    if($var['cus_no_KTP'] == '')
        $authoKtpDate1 = '';

    $arr[$idx] = $authoKtpDate1;                                     $idx++;     //39. authorized person KTP expired date
    $arr[$idx] = $var['authorized_person_passport1'];                $idx++;     //40. authorized person passport no

    $authoPassportDate1 = '';
    if($var['authorized_person_passport_expiration_date1'] != null && $var['authorized_person_passport_expiration_date1'] != '' && $var['authorized_person_passport_expiration_date1'] != '0000-00-00')
        $authoPassportDate1 = str_replace('-','',$var['authorized_person_passport_expiration_date1']);
    if($var['authorized_person_passport1'] == '')
        $authoPassportDate1 = '';

    $arr[$idx] = $authoPassportDate1;                                $idx++;     //41. authorized person passport expored date

    $arr[$idx] = $var['cus_nama_2'];                                 $idx++;     //42. authorized person first name 2
    $arr[$idx] = $var['authorized_person_middle_name2'];             $idx++;     //43. authorized person middle name 2
    $arr[$idx] = $var['authorized_person_last_name2'];               $idx++;     //44. authorized person last name 2
    $arr[$idx] = $var['cus_jabatan_2'];                              $idx++;     //45. authorized person position 2
    $arr[$idx] = $var['cus_telp_hp_2'];                              $idx++;     //46. authorized person telephone 2
    $arr[$idx] = $var['authorized_person_email2'];                   $idx++;     //47. authorized person email 2
    $arr[$idx] = $var['authorized_person_npwp2'];                    $idx++;     //48. authorized person npwp 2
    $arr[$idx] = $var['cus_no_KTP_2'];                               $idx++;     //49. authorized person KTP no 2

    $authoKtpDate2 = '';
    if($var['authorized_person_ktp_expiration_date2'] != null && $var['authorized_person_ktp_expiration_date2'] != '' && $var['authorized_person_ktp_expiration_date2'] != '0000-00-00')
        $authoKtpDate2 = str_replace('-','',$var['authorized_person_ktp_expiration_date2']);
    if($var['cus_no_KTP_2'] == '')
        $authoKtpDate2 = '';

    $arr[$idx] = $authoKtpDate2;                                     $idx++;     //50. authorized person KTP expired date 2
    $arr[$idx] = $var['authorized_person_passport2'];                $idx++;     //51. authorized person passport no 2

    $authoPassportDate2 = '';
    if($var['authorized_person_passport_expiration_date2'] != null && $var['authorized_person_passport_expiration_date2'] != '' && $var['authorized_person_passport_expiration_date2'] != '0000-00-00')
        $authoPassportDate2 = str_replace('-','',$var['authorized_person_passport_expiration_date2']);
    if($var['authorized_person_passport2'] == '')
        $authoPassportDate2 = '';

    $arr[$idx] = $authoPassportDate2;   $idx++;     //52. authorized person passport expored date 2

    $asset1 = '';
    switch($var['asset_information_last_year']){
        case '1': $asset1='< 100 billion/Year';break;
        case '2': $asset1='> 100– 500 billion/Year';break;
        case '3': $asset1='> 500 – 1,000 billion/Year';break;
        case '4': $asset1='> 1,000 – 5,000 billion/Year';break;
        case '5': $asset1='> 5,000 billion/Year';break;
    }

    $arr[$idx] = $asset1;                                            $idx++;     //53. asset_information_last_year

    $asset2 = '';
    switch($var['asset_information_2_years_year']){
        case '1': $asset2='< 100 billion/Year';break;
        case '2': $asset2='> 100– 500 billion/Year';break;
        case '3': $asset2='> 500 – 1,000 billion/Year';break;
        case '4': $asset2='> 1,000 – 5,000 billion/Year';break;
        case '5': $asset2='> 5,000 billion/Year';break;
    }

    $arr[$idx] = $asset2;                                            $idx++;     //54. asset_information_2_years_year

    $asset3 = '';
    switch($var['asset_information_3_years_year']){
        case '1': $asset3='< 100 billion/Year';break;
        case '2': $asset3='> 100– 500 billion/Year';break;
        case '3': $asset3='> 500 – 1,000 billion/Year';break;
        case '4': $asset3='> 1,000 – 5,000 billion/Year';break;
        case '5': $asset3='> 5,000 billion/Year';break;
    }

    $arr[$idx] = $asset3;                                            $idx++;     //55. asset_information_3_years_year

    $profit1 = '';
    switch($var['profit_information_last_year']){
        case '1': $profit1='< 100 billion/Year';break;
        case '2': $profit1='> 100– 500 billion/Year';break;
        case '3': $profit1='> 500 – 1,000 billion/Year';break;
        case '4': $profit1='> 1,000 – 5,000 billion/Year';break;
        case '5': $profit1='> 5,000 billion/Year';break;
    }

    $arr[$idx] = $profit1;                                           $idx++;     //56. profit_information_last_year

    $profit2 = '';
    switch($var['profit_information_2_years_year']){
        case '1': $profit2='< 100 billion/Year';break;
        case '2': $profit2='> 100– 500 billion/Year';break;
        case '3': $profit2='> 500 – 1,000 billion/Year';break;
        case '4': $profit2='> 1,000 – 5,000 billion/Year';break;
        case '5': $profit2='> 5,000 billion/Year';break;
    }

    $arr[$idx] = $profit2;                                           $idx++;     //57. profit_information_2_years_year

    $profit3 = '';
    switch($var['profit_information_3_years_year']){
        case '1': $profit3='< 100 billion/Year';break;
        case '2': $profit3='> 100– 500 billion/Year';break;
        case '3': $profit3='> 500 – 1,000 billion/Year';break;
        case '4': $profit3='> 1,000 – 5,000 billion/Year';break;
        case '5': $profit3='> 5,000 billion/Year';break;
    }

    $arr[$idx] = $profit3;                                           $idx++;     //58. profit_information_3_years_year

    $fatca = '';
    switch($var['fatca']){
        case '1': $fatca='Registered Deemed-Compliant FFI';break;
        case '2': $fatca='Certified Deemed-Compliant FFI';break;
        case '3': $fatca='Owner-Documented FFI';break;
        case '4': $fatca='Owner-Documented FFI with Specified U.S. Owner';break;
        case '5': $fatca='Participating FFI';break;
        case '6': $fatca='Nonparticipating FFI';break;
        case '7': $fatca='Active NFFE';break;
        case '8': $fatca='Passive NFFE with Substantial U.S. Owner';break;
        case '9': $fatca='Passive NFFE without Substantial U.S. Owner';break;
        case '10': $fatca='Direct Reporting NFFE';break;
        case '11': $fatca='Exempt Beneficial Owner';break;
        case '12': $fatca='U.S. Financial Institution';break;
    }

    $arr[$idx] = $var['fatca'];                                      $idx++;     //59. fatca
    $arr[$idx] = $var['tin'];                                        $idx++;     //60. tin
    $arr[$idx] = $var['tin_issuance_country'];                       $idx++;     //61. tin_issuance_country
    $arr[$idx] = $var['giin'];                                       $idx++;     //62. giin
    $arr[$idx] = $var['substantial_us_owner_name'];                  $idx++;     //63. substantial_us_owner_name
    $arr[$idx] = $var['substantial_us_owner_address'];               $idx++;     //64. substantial_us_owner_address
    $arr[$idx] = $var['substantial_us_owner_tin'];                   $idx++;     //65. substantial_us_owner_tin

    if($type == '1'){
        $arr[$idx] = $var['bank_bic_code1'];                         $idx++;     //66. redm payment bank bic code
        $arr[$idx] = $var['bank_bi_member_code1'];                   $idx++;     //67. redm payment bank bi member code
        $arr[$idx] = $var['bank_name1'];                             $idx++;     //68. redm payment bank name 
        $arr[$idx] = $var['bank_country1'];                          $idx++;     //69. redm payment bank country
        $arr[$idx] = $var['bank_branch1'];                           $idx++;     //70. redm payment bank branch
        $arr[$idx] = $var['acc_ccy1'];                               $idx++;     //71. redm payment acc currency
        $arr[$idx] = $var['acc_no1'];                                $idx++;     //72. redm payment acc no
        $arr[$idx] = $var['acc_name1'];                              $idx++;     //73. redm payment acc name

        $arr[$idx] = $var['bank_bic_code2'];                         $idx++;     //74. redm payment bank bic code 2
        $arr[$idx] = $var['bank_bi_member_code2'];                   $idx++;     //75. redm payment bank bi member code 2
        $arr[$idx] = $var['bank_name2'];                             $idx++;     //76. redm payment bank name  2
        $arr[$idx] = $var['bank_country2'];                          $idx++;     //77. redm payment bank country 2
        $arr[$idx] = $var['bank_branch2'];                           $idx++;     //78. redm payment bank branch 2
        $arr[$idx] = $var['acc_ccy2'];                               $idx++;     //79. redm payment acc currency 2
        $arr[$idx] = $var['acc_no2'];                                $idx++;     //80. redm payment acc no 2
        $arr[$idx] = $var['acc_name2'];                              $idx++;     //81. redm payment acc name 2

        $arr[$idx] = $var['bank_bic_code3'];                         $idx++;     //82. redm payment bank bic code 3
        $arr[$idx] = $var['bank_bi_member_code3'];                   $idx++;     //83. redm payment bank bi member code 3
        $arr[$idx] = $var['bank_name3'];                             $idx++;     //84. redm payment bank name 3
        $arr[$idx] = $var['bank_country3'];                          $idx++;     //85. redm payment bank country 3
        $arr[$idx] = $var['bank_branch3'];                           $idx++;     //86. redm payment bank branch 3
        $arr[$idx] = $var['acc_ccy3'];                               $idx++;     //87. redm payment acc currency 3
        $arr[$idx] = $var['acc_no3'];                                $idx++;     //88. redm payment acc no 3
        $arr[$idx] = $var['acc_name3'];                              $idx++;     //89. redm payment acc name 3
        $arr[$idx] = $var['client_code'];                            $idx++;     //90. client code
    }else{
        $arr[$idx] = '';                                            $idx++;     //66. redm payment bank bic code
        $arr[$idx] = '';                                            $idx++;     //67. redm payment bank bi member code
        $arr[$idx] = '';                                            $idx++;     //68. redm payment bank name 
        $arr[$idx] = '';                                            $idx++;     //69. redm payment bank country
        $arr[$idx] = '';                                            $idx++;     //70. redm payment bank branch
        $arr[$idx] = '';                                            $idx++;     //71. redm payment acc currency
        $arr[$idx] = '';                                            $idx++;     //72. redm payment acc no
        $arr[$idx] = '';                                            $idx++;     //73. redm payment acc name

        $arr[$idx] = '';                                            $idx++;     //74. redm payment bank bic code 2
        $arr[$idx] = '';                                            $idx++;     //75. redm payment bank bi member code 2
        $arr[$idx] = '';                                            $idx++;     //76. redm payment bank name  2
        $arr[$idx] = '';                                            $idx++;     //77. redm payment bank country 2
        $arr[$idx] = '';                                            $idx++;     //78. redm payment bank branch 2
        $arr[$idx] = '';                                            $idx++;     //79. redm payment acc currency 2
        $arr[$idx] = '';                                            $idx++;     //80. redm payment acc no 2
        $arr[$idx] = '';                                            $idx++;     //81. redm payment acc name 2

        $arr[$idx] = '';                                            $idx++;     //82. redm payment bank bic code 3
        $arr[$idx] = '';                                            $idx++;     //83. redm payment bank bi member code 3
        $arr[$idx] = '';                                            $idx++;     //84. redm payment bank name 3
        $arr[$idx] = '';                                            $idx++;     //85. redm payment bank country 3
        $arr[$idx] = '';                                            $idx++;     //86. redm payment bank branch 3
        $arr[$idx] = '';                                            $idx++;     //87. redm payment acc currency 3
        $arr[$idx] = '';                                            $idx++;     //88. redm payment acc no 3
        $arr[$idx] = '';                                            $idx++;     //89. redm payment acc name 3
        $arr[$idx] = '';                                            $idx++;     //90. client code
    }

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
}

fclose($output);
?>
