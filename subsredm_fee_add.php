<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('subsredm_fee_add.html');

$transactionDate = date('Y-m-d');  
$transactionType = 1;    
$referenceNo = '';   
$saCode = 'MU002';       
$investorAcNo = '';
$fundCode	= '';           
$amount = 0;
$amountUnit = 0;
$amountAllUnit = '';
$fee = 0;
$feeUnit = 0;               
$feePersen = 0;
$redm_payment_ac_sequential_code = '';
$redm_payment_bank_bic_code = '';
$redm_payment_bank_bi_member_code = '';
$redm_payment_ac_no = '';
$paymentDate = date('Y-m-d');
$transferType = 1;
$saReferenceNo	= '';
$errorArr = array('','','','','','','','','','','','','','');
$otherError='';

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
 		$transactionDate = trim(htmlentities($_POST['transactionDate']));
		$transactionType = trim(htmlentities($_POST['transactionType']));
        $referenceNo = trim(htmlentities($_POST['referenceNo']));
		//$saCode = trim(htmlentities($_POST['saCode']));
		$investorAcNo = trim(htmlentities($_POST['investorAcNo']));
		$fundCode	= trim(htmlentities($_POST['fundCode']));
        $amount	= trim(htmlentities($_POST['amount']));
        $amountUnit	= trim(htmlentities($_POST['amountUnit']));
        $amountAllUnit	= trim(htmlentities($_POST['amountAllUnit']));
        $fee	= trim(htmlentities($_POST['fee']));
        $feeUnit	= trim(htmlentities($_POST['feeUnit']));
        $feePersen	= trim(htmlentities($_POST['feePersen']));
		$redm_payment_ac_sequential_code	= trim(htmlentities($_POST['redm_payment_ac_sequential_code']));
		$redm_payment_bank_bic_code	= trim(htmlentities($_POST['redm_payment_bank_bic_code']));
		$redm_payment_bank_bi_member_code	= trim(htmlentities($_POST['redm_payment_bank_bi_member_code']));
		$redm_payment_ac_no	= trim(htmlentities($_POST['redm_payment_ac_no']));
        $paymentDate = trim(htmlentities($_POST['paymentDate']));
        $transferType	= trim(htmlentities($_POST['transferType']));
        $saReferenceNo	= trim(htmlentities($_POST['saReferenceNo']));
		
		$gotError = false;
        //validation
		if($investorAcNo==''){
			$errorArr[3] = "Investor Fund Unit A/C No must be filled";
			$gotError = true;
		}
		if($fundCode==''){
			$errorArr[4] = "Fund Code must be filled";
			$gotError = true;
		}
		if($transactionType==1 && ($amount == '' || $amount == 0)){
			$errorArr[5] = "On Subscription, Amount (nominal) must be filled";
			$gotError = true;
		}
		if($transactionType==2 && ($amount == '' || $amount == 0) && ($amountUnit == '' || $amountUnit == 0)){
			$errorArr[5] = "Amount(Nominal) or Amount(Unit) must be filled";
			$gotError = true;
		}
		if($transactionType==2 && ($amount != '' && $amount != 0) && ($amountUnit != '' && $amountUnit != 0)){
			$errorArr[5] = "Fill only one field, Amount(Nominal) or Amount(Unit)";
			$gotError = true;
		}
		
        
		if (!$gotError){
			$query = "INSERT INTO tbl_kr_subsredm_fee (
					transaction_date
					, transaction_type
                    , reference_no 
                    , sa_code 
                    , investor_ac_no 
                    , fund_code 
                    , amount 
                    , amount_unit 
                    , amount_all_unit 
                    , fee
                    , fee_unit
                    , fee_persen 
					, redm_payment_ac_sequential_code
					, redm_payment_bank_bic_code
					, redm_payment_bank_bi_member_code
					, redm_payment_ac_no
                    , payment_date 
                    , transfer_type 
                    , sa_reference_no 
					, created_time
					, created_by
					, last_updated_time
					, last_updated_by
					, is_deleted
				)VALUES('$transactionDate',
					'$transactionType',
					'$referenceNo',
					'$saCode',
					'$investorAcNo',
					'$fundCode',
					'$amount',
					'$amountUnit',
					'$amountAllUnit',
					'$fee',
					'$feeUnit',
					'$feePersen',
					'$redm_payment_ac_sequential_code',
					'$redm_payment_bank_bic_code',
					'$redm_payment_bank_bi_member_code',
					'$redm_payment_ac_no',
					'$paymentDate',
					'$transferType',
					'$saReferenceNo',
					now(),
					'".$_SESSION['pk_id']."',
					now(),
					'".$_SESSION['pk_id']."',
					'0'
				)";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='subsredm_fee.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}
$readonly = $transactionType == 1 ? 'readonly' : '';
$tittle = "ADD - SUBS / REDM FEE";
$dataRows = array (
	    'TEXT' => array(
			'Transaction Date <span class="redstar">*</span>',
			'Transaction Type ',
			'Reference No. <span class="redstar">*</span>',
			'Invs. Fund Unit A/C No.',
			'Fund Code',
			'Amount (Nominal)',
			'Amount (Unit)',
			'Amount (All Unit)',
			'Fee',
			'Fee (Unit)',
			'Fee (%)',
			'REDM Payment A/C Sequential Code',
			'REDM Payment BANK BIC Code',
			'REDM Payment BANK BI Member Code',
			'REDM Payment A/C No',
			'Payment Date',
			'Transfer Type',
			'SA Reference No.'),
  	    'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':',':',':'),
	    'FIELD' => array (
            $data->datePicker('transactionDate', $transactionDate,''),
            "<select name=transactionType onchange='changeType(this.value)'><option value=1 ". ($transactionType == 1 ? "selected=selected" : "") .">Subscription</option><option value=2 ".($transactionType == 2 ? "selected=selected" : "").">Redemption</option></select>",
            "<input type=text size='50' maxlength=30 name=referenceNo value='$referenceNo'>",
            //"<input type=text size='50' maxlength=5 name=saCode value='$saCode'>",
            //"<input type=text size='50' maxlength=16 name=investorAcNo value='$investorAcNo'>",
			$data->cb_ifua('investorAcNo', $investorAcNo, ''),
            //"<input type=text size='50' maxlength=16 name=fundCode value='$fundCode'>",
			$data->cb_fundcode('fundCode', $fundCode),
            "<input type=number size='50' step=0.01 name=amount id=amount value='$amount'>",
            "<input type=number size='50' step=0.0001 name=amountUnit id=amountUnit value='$amountUnit' $readonly>",
            "<input type=checkbox name=amountAllUnit id=amountAllUnit value='Y' ".($amountAllUnit == 'Y' ? "checked" : "")." $readonly>",
            "<input type=number size='50' step=0.01 name=fee value='$fee'>",
            "<input type=number size='50' step=0.0001 name=feeUnit value='$feeUnit'>",
            "<input type=number size='50' step=0.01 name=feePersen value='$feePersen'>",
			"<input type=text size='50' maxlength=2 id=redm_payment_ac_sequential_code name=redm_payment_ac_sequential_code value='$redm_payment_ac_sequential_code' $readonly>",
			"<input type=text size='50' maxlength=11 id=redm_payment_bank_bic_code name=redm_payment_bank_bic_code value='$redm_payment_bank_bic_code' $readonly>",
			"<input type=text size='50' maxlength=17 id=redm_payment_bank_bi_member_code name=redm_payment_bank_bi_member_code value='$redm_payment_bank_bi_member_code' $readonly>",
			"<input type=text size='50' maxlength=30 id=redm_payment_ac_no name=redm_payment_ac_no value='$redm_payment_ac_no' $readonly>",
		    $data->datePicker('paymentDate', $paymentDate,''),
		    "<select name=transferType><option value=1 ". ($transferType == 1 ? "selected=selected" : "") .">SKNBI</option><option value=2 ".($transactionType == 2 ? "selected=selected" : "").">RTGS</option><option value=3 ".($transactionType == 3 ? "selected=selected" : "").">N/A</option></select>",
		    "<input type=text size='50' maxlength=30 name=saReferenceNo value='$saReferenceNo'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='subsredm_fee.php';\">");

$javascript = "<script type='text/javascript'>
	function changeType(type){
		if(type == 1){
			var doc = document;
			doc.getElementById('amountUnit').value = '0';
			doc.getElementById('amountUnit').readOnly = true;
			doc.getElementById('amountAllUnit').checked = false;
			doc.getElementById('amountAllUnit').readOnly = true;
			doc.getElementById('redm_payment_ac_sequential_code').value = '';
			doc.getElementById('redm_payment_ac_sequential_code').readOnly = true;
			doc.getElementById('redm_payment_bank_bic_code').value = '';
			doc.getElementById('redm_payment_bank_bic_code').readOnly = true;
			doc.getElementById('redm_payment_bank_bi_member_code').value = '';
			doc.getElementById('redm_payment_bank_bi_member_code').readOnly = true;
			doc.getElementById('redm_payment_ac_no').value = '';
			doc.getElementById('redm_payment_ac_no').readOnly = true;
		}else{
			var doc = document;
			doc.getElementById('amountUnit').readOnly = false;
			doc.getElementById('amountAllUnit').readOnly = false;
			doc.getElementById('redm_payment_ac_sequential_code').readOnly = false;
			doc.getElementById('redm_payment_bank_bic_code').readOnly = false;
			doc.getElementById('redm_payment_bank_bi_member_code').readOnly = false;
			doc.getElementById('redm_payment_ac_no').readOnly = false;
		}
	}
</script>";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>