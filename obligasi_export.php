<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$allocation = $_GET['allocation'];
$filename = $transaction_date."_bonds.txt";

// output headers so that the file is downloaded rather than displayed
//header('Content-Type: text/csv; charset=utf-8');
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
try{


$output = fopen('php://output', 'w');

/*$myfile = fopen("newfile.txt", "w");
$txt = "Mickey Mouse\n";
fwrite($myfile, $txt);
$txt = "Minnie Mouse\n";
fwrite($myfile, $txt);
fclose($myfile);*/

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    A.transaction_status,
    A.ta_reference_id,
    A.ta_reference_no,
    DATE_FORMAT(A.create_dt, '%Y%m%d') as trade_date, 
    DATE_FORMAT(A.settlement_date, '%Y%m%d') as settlement_date, 
    A.im_code, 
    A.br_code, 
    B.fund_code, 
    A.code as security_code, 
    A.buy_sell, 
    A.int_rate as price,
    A.face_value as face_value,
    (A.face_value * A.int_rate / 100) as proceeds, 
    DATE_FORMAT(A.last_coupon, '%Y%m%d') as last_coupon_date, 
    DATE_FORMAT(A.next_coupon, '%Y%m%d') as next_coupon_date, 
    A.accrued_days, 
    A.accrued_interest_amount, 
    A.other_fee, 
    A.capital_gain_tax,
    A.interest_income_tax,
    A.withholding_tax,
    A.settlement_type,
    A.sellers_tax_id,
    A.purpose_of_transaction,
    A.statutory_type,
    remarks, 
    cancellation_reason,
    DATE_FORMAT(A.acquisition_date, '%Y%m%d') as acquisition_date,
    A.acquisition_price,
    A.acquisition_amount,
    A.capital_gain,
    A.days_of_holding_interest,
    A.holding_interest_amount,
    A.total_taxable_income,
    A.tax_rate,
    A.tax_amount
    FROM tbl_kr_me_bonds_hist A JOIN tbl_kr_allocation B ON A.allocation = B.pk_id
    WHERE DATE_FORMAT(A.create_dt, '%Y-%m-%d') = '$transaction_date' ";

if($allocation != null && $allocation != ''){
    $query .= " AND A.allocation = '$allocation'";
}

    #echo $query;
$rows = $data->get_rows2($query);


fwrite($output, "Transaction Status|TA Reference ID|Data Type|TA Reference No.|Trade Date|Settlement Date|IM Code|BR Code|Fund Code|Security Code|Buy/Sell|Price|Face Value|Proceeds|Last Coupon Date|Next Coupon Date|Accrued Days|Accrued Interest Amount|Other Fee|Capital Gain Tax|Interest Income Tax|Withholding Tax|Net Proceeds|Settlement Type|Seller's Tax ID|Purpose of Transaction|Statutory Type|Remarks|Cancellation Reason\r\n");
//fwrite($output, "\r\n");
// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    if($rows[$i]['buy_sell'] == '1'){
        $rows[$i]['net_proceeds'] = $rows[$i]['proceeds'] + $rows[$i]['accrued_interest_amount'] + $rows[$i]['other_fee'] - $rows[$i]['withholding_tax'];
    }else{
        $rows[$i]['net_proceeds'] = $rows[$i]['proceeds'] + $rows[$i]['accrued_interest_amount'] - $rows[$i]['other_fee'] - $rows[$i]['withholding_tax'];
    }

    if($rows[$i]['transaction_status'] == 'NEWM'){
        $rows[$i]['ta_reference_id'] = '';
        $rows[$i]['cancellation_reason'] = '';
    }

    
    $str = $rows[$i]['transaction_status']
        ."|".$rows[$i]['ta_reference_id']
        ."|1"
        ."|".$rows[$i]['ta_reference_no']
        ."|".$rows[$i]['trade_date']
        ."|".$rows[$i]['settlement_date']
        ."|".$rows[$i]['im_code']
        ."|".$rows[$i]['br_code']
        ."|".$rows[$i]['fund_code']
        ."|".$rows[$i]['security_code']
        ."|".$rows[$i]['buy_sell']
        ."|".$rows[$i]['price']
        ."|".$rows[$i]['face_value']
        ."|".$rows[$i]['proceeds']
        ."|".$rows[$i]['last_coupon_date']
        ."|".$rows[$i]['next_coupon_date']
        ."|".$rows[$i]['accrued_days']
        ."|".$rows[$i]['accrued_interest_amount']
        ."|".$rows[$i]['other_fee']
        ."|".$rows[$i]['capital_gain_tax']
        ."|".$rows[$i]['interest_income_tax']
        ."|".$rows[$i]['withholding_tax']
        ."|".$rows[$i]['net_proceeds']
        ."|".$rows[$i]['settlement_type'];
		
    if($rows[$i]['buy_sell'] == '2'){
        $str.= "|".$rows[$i]['sellers_tax_id'];
	}else{
		$str.= "|";
	}
        $str .="|".$rows[$i]['purpose_of_transaction']
        ."|".$rows[$i]['statutory_type']
        ."|".$rows[$i]['remarks']
        ."|".$rows[$i]['cancellation_reason'];
    if($rows[$i]['buy_sell'] == '2'){
        $str .= "|2"
            ."|".$rows[$i]['ta_reference_no']
            ."|".$rows[$i]['face_value']
            ."|".$rows[$i]['acquisition_date']
            ."|".$rows[$i]['acquisition_price']
            ."|".$rows[$i]['acquisition_amount']
            ."|".$rows[$i]['capital_gain']
            ."|".$rows[$i]['days_of_holding_interest']
            ."|".$rows[$i]['holding_interest_amount']
            ."|".$rows[$i]['total_taxable_income']
            ."|".$rows[$i]['tax_rate']
            ."|".$rows[$i]['tax_amount']
            ."\r\n";
    }else{
        //$str .= "|||||||||||\r\n";
        $str .= "\r\n";
    }
    fwrite($output, $str);
    //fputcsv($output, $rows[$i]);
}

fclose($output);
} catch(Exception $e){
    echo $e->getMessage();
}
?>
