<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'asset_transaction.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new assettransaction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('at_fixed_income_add.html');

$dataType = '1';
$tradeId = '';
$tradeDate = date('Y-m-d');
$settlementDate = date('Y-m-d');
$brCode = '';
$fundCode = '';

$securityCode = '';
$tradeType = '';
$price = 0;
$faceValue = 0;
$proceeds = 0;

$lastCouponDate = date('Y-m-d');
$nextCouponDate = date('Y-m-d');
$accruedDays = 0;
$accruedInterestAmount = 0;
$otherFee = 0;

$capitalGainTax = 0;
$interestIncomeTax = 0;
$withholdingTax = 0;
$netProceeds = 0;
$settlementType = '1';
$taxId = '';
$purposeOfTransaction = '1';
$remarks = '';

$errorArr = array('','','','',''
    ,'','','','',''
    ,'','','','',''
    ,'','','','',''
    ,'');

$otherError='';

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        //$dataType = trim(htmlentities($_POST['inputDataType']));
        $dataType = 1; 
 		$tradeId = trim(htmlentities($_POST['inputTradeId']));
		$tradeDate = trim(htmlentities($_POST['inputTradeDate']));
		$settlementDate = trim(htmlentities($_POST['inputSettlementDate']));
		$brCode = trim(htmlentities($_POST['inputBrCode']));
		$fundCode = trim(htmlentities($_POST['inputFundCode']));

		$securityCode = trim(htmlentities($_POST['inputSecurityCode']));
		$tradeType = trim(htmlentities($_POST['inputTradeType']));
		$price = trim(htmlentities($_POST['inputPrice']));
		$faceValue = trim(htmlentities($_POST['inputFaceValue']));
		//$tradeAmount = trim(htmlentities($_POST['inputTradeAmount']));
		$proceeds = (floatval($price) * floatval($faceValue))/100;

		$lastCouponDate = trim(htmlentities($_POST['inputLastCouponDate']));
		$nextCouponDate = trim(htmlentities($_POST['inputNextCouponDate']));
		$accruedDays = trim(htmlentities($_POST['inputAccruedDays']));
		$accruedInterestAmount = trim(htmlentities($_POST['inputAccruedInterestAmount']));
		$otherFee = trim(htmlentities($_POST['inputOtherFee']));

		$capitalGainTax = trim(htmlentities($_POST['inputCapitalGainTax']));
		$interestIncomeTax = trim(htmlentities($_POST['inputInterestIncomeTax']));
		$withholdingTax = trim(htmlentities($_POST['inputWithholdingTax']));

        if(intval($tradeType) == 1){
            $netProceeds = floatval($proceeds) + floatval($accruedInterestAmount) + floatval($otherFee) - floatval($capitalGainTax) - floatval($interestIncomeTax);
        }else {
            $netProceeds = floatval($proceeds) + floatval($accruedInterestAmount) - floatval($otherFee) - floatval($capitalGainTax) - floatval($interestIncomeTax);
        }
        
		//$netProceeds = trim(htmlentities($_POST['inputNetProceeds']));
		$settlementType = trim(htmlentities($_POST['inputSettlementType']));

        $taxId = trim(htmlentities($_POST['inputTaxId']));
        $purposeOfTransaction = trim(htmlentities($_POST['inputPurposeOfTransaction']));
        $remarks = trim(htmlentities($_POST['inputRemarks']));
		
		$gotError = false;
		if($fundCode==''){
			$errorArr[5] = "Fund Code must be filled";
			$gotError = true;
		}
		if(strlen($fundCode)<16){
			$errorArr[5] = "Invalid Fund Code";
			$gotError = true;
		}
		
        
		if (!$gotError){
			
			$query = "INSERT INTO tbl_kr_fixed_income (
                    data_type,
					trade_id,
					trade_date,
					settlement_date,
					im_code,
					br_code,
					fund_code,
					security_code,
					trade_type,
					price,
					face_value,
					proceeds,
					last_coupon_date,
					next_coupon_date,
					accrued_days,
					accrued_interest_amount,
					other_fee,
					capital_gain_tax,
					interest_income_tax,
					withholding_tax,
                    net_proceeds,
					settlement_type,
                    tax_id,
                    purpose_of_transaction,
					remarks,
					created_date,
					created_by,
					last_updated_date,
					last_updated_by,
					is_deleted
				)VALUES(
                    '$dataType',
					'$tradeId',
					'$tradeDate',
					'$settlementDate',
					'MU002',
					'$brCode',
					'$fundCode',
					'$securityCode',
					'$tradeType',
					'$price',
					'$faceValue',
					'$proceeds',
					'$lastCouponDate',
					'$nextCouponDate',
					'$accruedDays',
					'$accruedInterestAmount',
					'$otherFee',
					'$capitalGainTax',
					'$interestIncomeTax',
					'$withholdingTax',
                    '$netProceeds',
					'$settlementType',
                    '$taxId',
                    '$purposeOfTransaction',
					'$remarks',
					now(),
					'".$_SESSION['pk_id']."',
					now(),
					'".$_SESSION['pk_id']."',
					'0')";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='at_equity.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = "ADD - FIXED INCOME";
$dataRows = array (
	    'TEXT' => array(
				'Trade ID'
				,'Trade Date'
				,'Settlement Date'
				,'BR Code'
				,'Fund Code'

				,'Security Code'
				,'Trade Type'
				,'Price'
				,'Face Value'
				,'Last Coupon Date'

				,'Next Coupon Date'
				,'Accrued Days'
				,'Accrued Interest Amount'
				,'Other Fee'
				,'Capital Gain Tax'

				,'Interest Income Tax'
				,'Withholding Tax'
				,'Settlement Type'
                ,'Tax ID'
                ,'Purpose of Transaction'
				,'Remarks'
			),
  	    'DOT'  => array (':',':',':',':',':'),
	    'FIELD' => array (
            "<input type=text size='50' maxlength=16 name=inputTradeId value='$tradeId'>",
		    $data->datePicker('inputTradeDate', $tradeDate,''),
            $data->datePicker('inputSettlementDate', $settlementDate,''),
			"<input type=text size='50' maxlength=5 name=inputBrCode value='$brCode'>",
			"<input type=text size='50' maxlength=16 name=inputFundCode value='$fundCode'>",
			"<input type=text size='50' maxlength=16 name=inputSecurityCode value='$securityCode'>",
			$data->cb_tradetype('inputTradeType',$tradeType),
			"<input type=number step=0.01 name=inputPrice value='$price'>",
			"<input type=number step=0.01 name=inputFaceValue value='$faceValue'>",
			$data->datePicker('inputLastCouponDate', $lastCouponDate,''),
			$data->datePicker('inputNextCouponDate', $nextCouponDate,''),
			"<input type=number step=1 name=inputAccruedDays value='$accruedDays'>",
			"<input type=number step=0.01 name=inputAccruedInterestAmount value='$accruedInterestAmount'>",
			"<input type=number step=0.01 name=inputOtherFee value='$otherFee'>",
			"<input type=number step=0.01 name=inputCapitalGainTax value='$capitalGainTax'>",
			"<input type=number step=0.01 name=inputInterestIncomeTax value='$interestIncomeTax'>",
			"<input type=number step=0.01 name=inputWithholdingTax value='$withholdingTax'>",
			$data->cb_settlementtype('inputSettlementType',$settlementType),
            "<input type=text size='50' maxlength=5 name=inputTaxId value='$taxId'>",
            $data->cb_purpose('inputPurposeOfTransaction',$purposeOfTransaction),
			"<input type=text size=50 name=inputRemarks value='$remarks'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='at_fixed_income.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>