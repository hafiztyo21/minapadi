<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('settlement_edit.html');

if ($_GET['detail']=='1'){
	$rows = $data->get_row("SELECT tbl_kr_dealer_tmp.*,FORMAT(lembar,0) AS lembar,FORMAT(harga,2) AS harga,FORMAT(total,2) as total,FORMAT(komisi,2) AS komisi,FORMAT(levy,2) AS levy,FORMAT(kpei,2) AS kpei,FORMAT(pajak,2) AS pajak,FORMAT(pajak_jual,2) AS pajak_jual,FORMAT(pajak_komisi,2) AS pajak_komisi,FORMAT(nilai_jual,2) AS nilai_jual,FORMAT(nilai_beli,2) AS nilai_beli,tbl_kr_securitas.securitas_type 
	FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
	ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
	WHERE pk_id = '".$_GET[id]."'");

	$dataRows = array (
		'TEXT' =>  array(
						'Saham',
						'Status',
						'Jumlah',
						'Harga',
						'Nilai',
						'Komisi',
						'Levy',
						'KPEI',
						'Pajak',
						'Pajak Jual',
						'Pajak Komisi',
						'Nilai Beli',
						'Nilai Jual'
						),
		'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
		'FIELD' => array (
			'&nbsp;'.$rows[code],
			'&nbsp;'.$rows[type_buy],
			'&nbsp;'.$rows[lembar],
			'&nbsp;'.$rows[harga],
			'&nbsp;'.$rows[total],
			'&nbsp;'.$rows[komisi],
			'&nbsp;'.$rows[levy],
			'&nbsp;'.$rows[kpei],
			'&nbsp;'.$rows[pajak],
			'&nbsp;'.$rows[pajak_jual],
			'&nbsp;'.$rows[pajak_komisi],
			'&nbsp;'.$rows[nilai_jual],
			'&nbsp;'.$rows[nilai_beli]
		
			)
		);
	$tittle = " DETAIL ";
    $button = array ('SUBMIT' => "",
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"window.location='settlement.php';\">"
	);
} else if ($_GET['edit']=='1'){
	$rows = $data->get_row("SELECT tbl_kr_dealer_tmp.*,FORMAT(lembar,0) AS lembar,FORMAT(harga,2) AS harga,FORMAT(total,2) as total,FORMAT(komisi,2) AS komisi,FORMAT(levy,2) AS levy,FORMAT(pajak,2) AS pajak,FORMAT(pajak_jual,2) AS pajak_jual,FORMAT(pajak_komisi,2) AS pajak_komisi,FORMAT(nilai_jual,2) AS nilai_jual,FORMAT(nilai_beli,2) AS nilai_beli,tbl_kr_securitas.securitas_type, allocation
	FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
	ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
	WHERE pk_id = '".$_GET[id]."'");

	$rows1 = $data->get_row("SELECT * FROM tbl_kr_saham WHERE code = '".$rows[code]."' AND allocation='".$rows[allocation]."' ORDER BY create_dt DESC");
	$rows2 = $data->get_row("SELECT * FROM tbl_kr_stock_rate WHERE stk_cd = '".$rows[code]."'");
	#echo "SELECT * FROM tbl_kr_saham LEFT JOIN tbl_kr_stock_rate ON tbl_kr_saham.code=tbl_kr_stock_rate.stk_cd WHERE code = '".$rows[code]."' AND allocation='".$rows[allocation]."' ORDER BY create_dt DESC";
	$dataRows = array (
		'TEXT' =>  array(
						'Saham',
						'Status',
						'Jumlah',
						'Harga',
						'Nilai',
						'Komisi',
						'Levy',
						'KPEI',
						'Pajak',
						'Pajak Jual',
						'Pajak Komisi',
						'Nilai Beli',
						'Nilai Jual',
						'',

						'Allocation',
						'Code',
						'Name of Shares',
						'Price (Rp.)',
						'Number of Shares',
						'Status',
						'Transaction Status',
						'TA Reference ID',
						'TA Reference No.',
						'Settlement Date',
						'Commision',
						'Sales Tax',
						'Levy',
						'VAT',
						'Other Charges',
						'Gross Settlement Amount',
						'WHT on Commision',
						'Net Settlement Amount',
						'Settlement Type',
						'Remarks',
						'Cancelation Reason'
						),
		'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
		'FIELD' => array (
			$rows[code],
			$rows[type_buy],
			$rows[lembar],
			$rows[harga],
			$rows[total],
			$rows[komisi],
			$rows[levy],
			"<input type=text name=txt_kpei id=txt_kpei  value='".$rows[kpei]."'>",
			$rows[pajak],
			$rows[pajak_jual],
			$rows[pajak_komisi],
			$rows[nilai_jual],
			$rows[nilai_beli],
			"",

			$data->cb_allocation('txt_allocation',$rows[allocation],""),
			"<input type=text size='50' name=txt_code id='txt_code' value='".$rows[code]."' readonly>",
			"<input type=text size='50' name=txt_stk_name id='txt_stk_name' value='".$rows2[stk_name]."' readonly>",
			"<input type=text size='50' name=txt_harga id='txt_harga' value='".$rows1[harga]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
			"<input type=text size='30' name=txt_lembar id='txt_lembar' value='".$rows1[lembar]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
			$data->cb_saham_status('txt_status',$rows[status], "onchange='calculate()'"),
			"<select name='txt_transaction_status' onchange='changeStatus()'><option value='NEWM'>New Trade</option><option value='CANC'>Cancel Trade</option></select>",
			"<input type=text size='50' name=txt_ta_reference_id id='txt_ta_reference_id' readonly='readonly' value='".$rows1[ta_reference_id]."' placeholder='will be assign by S-INVEST'>",
			"<input type=text size='50' name=txt_ta_reference_no value='".$rows1[ta_reference_no]."'>",
			$data->datePicker('txt_settlement_date', $rows1[settlement_date],''),
			"<input type=text size='25' name=txt_commision id='txt_commision' value='".$rows1[komisi]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
			"<input type=text size='25' name=txt_sales_tax id='txt_sales_tax' value='".$sales_tax."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()' readonly='readonly'>",
			"<input type=text size='25' name=txt_levy id='txt_levy' value='".$rows1[levy]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
			"<input type=text size='25' name=txt_vat id='txt_vat' value='".$rows1[vat]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
			"<input type=text size='25' name=txt_other_charges id='txt_other_charges' value='".$rows1[other_charges]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
			"<input type=text size='25' name=txt_gross_settlement_amount id='txt_gross_settlement_amount' value='".$rows1[gross_settlement_amount]."' readonly='readonly'>",
			"<input type=text size='25' name=txt_wht_on_commision id='txt_wht_on_commision' value='".$rows1[wht_on_commission]."' onkeydown='validate(event, this)' onkeyup='reformat(event, this)' onchange='calculate()'>",
			"<input type=text size='25' name=txt_net_settlement_amount id='txt_net_settlement_amount' value='".$rows1[net_settlement_amount]."' readonly='readonly'>",
			"<select name='txt_settlement_type'><option value='1'>DVP</option><option value='2'>RVP</option><option value='3'>DFOP</option><option value='4'>RFOP</option></select>",
			"<input type=text size='50' name=txt_remarks value='".$rows1[remarks]."'>",
			"<input type=text size='50' name=txt_cancelation_reason value='".$rows1[cancellation_reason]."'>",

			)
		);
	$tittle = " DETAIL ";
       $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"opener.document.form1.submit();window.parent.close();\">"
					);
					
					
}
if ($_POST['btn_save_edit'])
{


	$txt_saham_id = trim(htmlentities($_POST['txt_saham_id']));
	$txt_code = trim(htmlentities($_POST['txt_code']));
	$txt_lembar = trim(htmlentities(str_replace(",","",$_POST['txt_lembar'])));
	$txt_harga = trim(htmlentities(str_replace(",","",$_POST['txt_harga'])));
	$txt_status = trim(htmlentities($_POST['txt_status']));
	$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
	$total = ($txt_harga * $txt_lembar);

	$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
	$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
	$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
	$txt_settlement_date = trim(htmlentities($_POST['txt_settlement_date']));
	$txt_commision = trim(htmlentities(str_replace(",","",$_POST['txt_commision'])));
	$txt_sales_tax = trim(htmlentities(str_replace(",","",$_POST['txt_sales_tax'])));
	$txt_levy = trim(htmlentities(str_replace(",","",$_POST['txt_levy'])));
	$txt_vat = trim(htmlentities(str_replace(",","",$_POST['txt_vat'])));
	$txt_other_charges = trim(htmlentities(str_replace(",","",$_POST['txt_other_charges'])));
	$txt_wht_on_commision = trim(htmlentities(str_replace(",","",$_POST['txt_wht_on_commision'])));
	$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
	$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
	$txt_cancelation_reason = trim(htmlentities($_POST['txt_cancelation_reason']));

	//warning---------------------
	$value_shares = $data->get_row("select sum(total) as jumlah from tbl_kr_mst_saham");
	$value_bonds = $data->get_row("select sum(market_value) as jumlah from tbl_kr_me_bonds");
	$value_pn = $data->get_row("select sum(price_val) as jumlah from tbl_kr_pn");
	$value = $value_shares['jumlah'] + $value_bonds['jumlah'] + $value_pn['jumlah'];
    if ($value>0){
    	$jumlah = $total * (100/$value);
		$warning = $data->get_rows("SELECT * FROM tbl_kr_warning");
		$valred = ($warning[0][batas]);
		$valyellow = ($warning[1][batas]);
		$valgreen = ($warning[2][batas]);
		if ($jumlah >= $valred){
			$status_war = 1;
		}elseif ($jumlah >= $valyellow){
			$status_war = 2;
		}elseif ($jumlah >= $valgreen){
			$status_war = 3;
		}
	}else{
		$status_war = 1;
	}
	//end warning -------
    $rows_c = $data->get_row("select * from tbl_kr_mst_saham where code='".$txt_code."'");
	$query2= "select * from tbl_kr_saham where code='".$txt_code."'";
	$hasil = mysql_query($query2);
	while ($data2 = mysql_fetch_array($hasil))
	{
		$total_all = ($total_all + $data2[total]);
	}

	$sql= "UPDATE tbl_kr_dealer_tmp SET 
		kpei='".$_POST['txt_kpei']."'
			WHERE pk_id = '".$_GET['id']."'";

	if($txt_saham_id!='')
	{
		$sql_saham = "UPDATE tbl_kr_saham SET
			transaction_status='".$txt_transaction_status."',
			ta_reference_id='".$txt_ta_reference_id."',
			ta_reference_no='".$txt_ta_reference_no."',
			settlement_date='".$txt_settlement_date."',
			im_code='MU002',
			br_code='MU001',
			commission='".$txt_commision."',
			sales_tax='".$txt_sales_tax."',
			levy='".$txt_levy."',
			vat='".$txt_vat."',
			other_charges='".$txt_other_charges."',
			wht_on_commission='".$txt_wht_on_commision."',
			settlement_type='".$txt_settlement_type."',
			remarks='".$txt_remarks."',
			cancellation_reason='".$txt_cancelation_reason."'
			WHERE saham_id='".$txt_saham_id."'
		)";

		if ($txt_status=='Buy'){
			$total_all = $total_all + $total;
			$lembar = $rows_c[lembar] + $txt_lembar;
			$avg = $total_all/$lembar;
		}elseif ($txt_status=='Sell'){
			if ($txt_lembar > $rows_c[lembar]){
				echo "<script>alert('Lembar tidak mencukupi!');</script>";
				throw new Exception($data->err_report('s02'));
			}
			elseif ($txt_lembar <= $rows_c[lembar]){
				$total_all = $total_all - $total;
				$lembar = $rows_c[lembar] - $txt_lembar;
				$avg = $total_all/$lembar;
			}
		}
		$sql_saham1 = "UPDATE tbl_kr_mst_saham SET lembar = '".$lembar."',avg = '".$avg."',total = '".$total_all."'
		WHERE code = '".$txt_code."'";
	} else {		
		if (empty($rows_c[code])){
			$total_all = $total_all + $total;
			$sql_saham1 = "INSERT INTO tbl_kr_mst_saham (code,lembar,avg,total,allocation)VALUES('$txt_code','$txt_lembar','$txt_harga','$total_all','$txt_allocation')";
		} else {
			if ($txt_status=='Buy'){
				$total_all = $total_all + $total;
				$lembar = $rows_c[lembar] + $txt_lembar;
				$avg = $total_all/$lembar;
			}elseif ($txt_status=='Sell'){
				if ($txt_lembar > $rows_c[lembar]){
					echo "<script>alert('Lembar tidak mencukupi!');</script>";
					throw new Exception($data->err_report('s02'));
				}
				elseif ($txt_lembar <= $rows_c[lembar]){
					$total_all = $total_all - $total;
					$lembar = $rows_c[lembar] - $txt_lembar;
					$avg = $total_all/$lembar;
				}
			}
			$sql_saham1 = "UPDATE tbl_kr_mst_saham SET lembar = '".$lembar."',avg = '".$avg."',total = '".$total_all."'
			WHERE code = '".$txt_code."'";
		}
		$sql_saham = "INSERT INTO tbl_kr_saham (
			code,
			lembar,
			harga,
			total,
			status,
			allocation,
			create_dt,
			transaction_status,
			ta_reference_id,
			ta_reference_no,
			settlement_date,
			im_code,
			br_code,
			commission,
			sales_tax,
			levy,
			vat,
			other_charges,
			wht_on_commission,
			settlement_type,
			remarks,
			cancellation_reason
			)VALUES('$txt_code',
			'$txt_lembar',
			'$txt_harga',
			'$total',
			'$txt_status',
			'$txt_allocation',
			now(),
			'$txt_transaction_status',
			'$txt_ta_reference_id',
			'$txt_ta_reference_no',
			'$txt_settlement_date',
			'MU002',
			'MU001',
			'$txt_commision',
			'$txt_sales_tax',
			'$txt_levy',
			'$txt_vat',
			'$txt_other_charges',
			'$txt_wht_on_commision',
			'$txt_settlement_type',
			'$txt_remarks',
			'$txt_cancelation_reason'
			)";
	}
		//print($sql);	
   # $data->showsql($sql);
	if (!$data->inpQueryReturnBool($sql_saham))
	{	echo "<script>alert('".$data->err_report('gagal $sql_saham')."');</script>";	}
	if (!$data->inpQueryReturnBool($sql_saham1))
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}
	if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('s01')."');opener.document.form1.submit();window.parent.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}
}


$javascript = "<script type='text/javascript'>
	
	function calculate(){
		var type = document.getElementById('txt_status').value;

		var doc = document;

		var harga = parseFloat(doc.getElementById('txt_harga').value.toString().replace(',',''));
		var lembar = parseFloat(doc.getElementById('txt_lembar').value.toString().replace(',',''));

		var tradeAmount = harga * lembar;

		var commision = parseFloat(doc.getElementById('txt_commision').value.toString().replace(',',''));
		var salesTax = parseFloat(doc.getElementById('txt_sales_tax').value.toString().replace(',',''));
		var levy = parseFloat(doc.getElementById('txt_levy').value.toString().replace(',',''));
		var vat = parseFloat(doc.getElementById('txt_vat').value.toString().replace(',',''));
		var otherCharges = parseFloat(doc.getElementById('txt_other_charges').value.toString().replace(',',''));

		var whtOnCommision = parseFloat(doc.getElementById('txt_wht_on_commision').value.toString().replace(',',''));

		if(type == 'Buy'){

			salesTax = 0;

			doc.getElementById('txt_sales_tax').value = 0;
			doc.getElementById('txt_sales_tax').readOnly = true;

			var gross = tradeAmount + commision + levy + salesTax + vat + otherCharges;
			var net = gross - whtOnCommision;

		}else{
			doc.getElementById('txt_sales_tax').readOnly = false;

			var gross = tradeAmount - commision - levy - salesTax - vat - otherCharges;
			var net = gross + whtOnCommision;
		}
		doc.getElementById('txt_gross_settlement_amount').value = reformat2(gross);
		doc.getElementById('txt_net_settlement_amount').value = reformat2(net);
	}

	function changeStatus(){
		if(document.getElementById('txt_transaction_status').value == 'NEWN'){
			document.getElementById('txt_ta_reference_id').value = '';
			document.getElementById('txt_ta_reference_id').readOnly = true;
		}else{
			document.getElementById('txt_ta_reference_id').readOnly = false;
		}
	}
	
</script>";


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css',
	  'PATHCUSTOMJS' => $GLOBALS['JS'].'custom.js'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>