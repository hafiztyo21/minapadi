<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('print_fund.html');


####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_me.pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$tanggal = $_GET['tgl'];
$nama = $_SESSION['username'];
$id = $_SESSION['pk_id'];
$_SESSION['sql']='';
/*$sql  = "select  tbl_kr_me.*, tbl_kr_price.price_type, tbl_kr_stock_rate.stk_cd, format((lembar_hist/500),0) as LMBR,format(lembar_hist,0) as LEMBAR_1,
		 tbl_kr_user.user_name, left(create_dt,10) as tgl, FORMAT(persen,4) as PRSN,
		 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar/500),0) as LMBR2, format(lembar,0) as LEMBAR_2 from tbl_kr_me
		LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
		LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
		LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.dealer_id
  		where LEFT(tbl_kr_me.create_dt,10) = '".$tanggal."' AND me_id = '".$id."' order by $order_by $sort_order";
*/$sql  = "select  tbl_kr_me.*, tbl_kr_price.price_type, tbl_kr_stock_rate.stk_cd, format((lembar_hist/100),0) as LMBR,format(lembar_hist,0) as LEMBAR_1,
		 tbl_kr_user.user_name, left(create_dt,10) as tgl, FORMAT(persen,4) as PRSN,
		 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar/100),0) as LMBR2, format(lembar,0) as LEMBAR_2 from tbl_kr_me
		LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
		LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
		LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.dealer_id
  		where LEFT(tbl_kr_me.create_dt,10) = '".$tanggal."'  
		AND allocation =  '1'  
		order by $order_by $sort_order";
		//print ($sql);
	$sql1  = "select  tbl_kr_me.*, tbl_kr_price.price_type, tbl_kr_stock_rate.stk_cd, format((lembar_hist/100),0) as LMBR,format(lembar_hist,0) as LEMBAR_1,
		 tbl_kr_user.user_name, left(create_dt,10) as tgl, FORMAT(persen,4) as PRSN,
		 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar/100),0) as LMBR2, format(lembar,0) as LEMBAR_2 from tbl_kr_me
		LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
		LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
		LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.dealer_id
  		where LEFT(tbl_kr_me.create_dt,10) = '".$tanggal."'  
		AND allocation =  '2'  
		order by $order_by $sort_order";
	$sql2  = "select  tbl_kr_me.*, tbl_kr_price.price_type, tbl_kr_stock_rate.stk_cd, format((lembar_hist/100),0) as LMBR,format(lembar_hist,0) as LEMBAR_1,
		 tbl_kr_user.user_name, left(create_dt,10) as tgl, FORMAT(persen,4) as PRSN,
		 format(price_from,0) as PRC_FROM, format(price_to,0) as PRC_TO, format((lembar/100),0) as LMBR2, format(lembar,0) as LEMBAR_2 from tbl_kr_me
		LEFT JOIN tbl_kr_price ON tbl_kr_price.pk_id = tbl_kr_me.price_type
		LEFT JOIN tbl_kr_stock_rate ON tbl_kr_stock_rate.stk_id = tbl_kr_me.code
		LEFT JOIN tbl_kr_user ON tbl_kr_user.pk_id = tbl_kr_me.dealer_id
  		where LEFT(tbl_kr_me.create_dt,10) = '".$tanggal."'  
		AND allocation =  '3'  
		order by $order_by $sort_order";
//print_r ($sql);
$DG= $data->dataGridDealer($sql,'pk_id',$data->ResultsPerPage);
$DG1= $data->dataGridDealer($sql1,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
$DG2= $data->dataGridDealer($sql2,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$linkdel);
	
$tmpl->addVar('page','nama',$nama);
$tmpl->addVar('page','date',$tanggal);
$tmpl->addRows('loopData',$DG);
$tmpl->addRows('loopdata1',$DG1);
$tmpl->addRows('loopdata3',$DG2);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->displayParsedTemplate('page');
?>