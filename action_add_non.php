<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('action_nontrans_add.html');
$tablename = 'tbl_kr_action_nontrans';


if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
		$txt_tgl = trim(htmlentities($_POST['txt_tgl']));
		$txt_code = trim(htmlentities($_POST['txt_code']));
 		$txt_code_to = trim(htmlentities($_POST['txt_code_to']));
		$txt_lembar = trim(htmlentities($_POST['txt_lembar']));
		$txt_harga_prev = trim(htmlentities($_POST['txt_harga_prev']));
        $txt_harga_close = trim(htmlentities($_POST['txt_harga_close']));
        
		if($txt_allocation==''){
			echo "<script>alert('Allocation is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
        if($txt_code_to==''){
			echo "<script>alert('Code To is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga_prev==''){
			echo "<script>alert('Harga Prev is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga_close==''){
			echo "<script>alert('Harga Close is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$total = $txt_harga_close * $txt_lembar;
		$sql = "INSERT INTO tbl_kr_action_nontrans (
			code,
			code_to,
			lembar,
			harga_prev,
			harga_close,
			allocation,
			create_dt,
			create_dt2
		)VALUES(
			'$txt_code',
			'$txt_code_to',
			'$txt_lembar',	
			'$txt_harga_prev',
			'$txt_harga_close',
			'$txt_allocation',
			'$txt_tgl',
			now()
			)";
			#print_r($sql);
			
		$sql2 ="INSERT INTO tbl_kr_mst_saham(
			code,
			lembar,
			harga_prev,
			harga_close,
			allocation,
			total,
			create_dt
			)VALUES(
			'$txt_code',
			'$txt_lembar',
			'$txt_harga_prev',
			'$txt_harga_close',
			'$txt_allocation',
			'$total',
			'$txt_tgl'
			)";
			
		$sql3 ="INSERT INTO tbl_kr_report_saham(
			code,
			lembar,
			harga_prev,
			harga_close,
			allocation,
			total,
			create_dt
			)VALUES(
			'$txt_code',
			'$txt_lembar',
			'$txt_harga_prev',
			'$txt_harga_close',
			'$txt_allocation',
			'$total',
			'$txt_tgl'
			)";
			
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql3)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='action_repo_non.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}


if ($_GET['add']==1){
	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Date',
						'Code',
						
						'Code to',
						'Number of Shares',
						'Harga Prev',
						'Harga Close',
						),
		'DOT'  => array (':',':',':',':'),
		'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
		$data->datePicker('txt_tgl', $rows[txt_tgl],''),
		"<input type=text size=20 name=txt_code value='".$_POST[txt_code]."' >",
		$data->cb_saham_contract('txt_code_to',$_POST[txt_code_to]," OnChange= \"document.form1.submit();\" "),
		"<input type=text size=20 name=txt_lembar value='".$_POST[txt_lembar]."' >",
		"<input type=text size=20 name=txt_harga_prev value='".$_POST[txt_harga_prev]."' >",
		"<input type=text size=20 name=txt_harga_close value='".$_POST[txt_harga_close]."'>"
		
		
		
	)
		);
	$tittle = "ACTION ADD";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='cash_repo.php';\">"
	);
}


if ($_POST['btn_save_edit']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$rows = $data->get_row("select * from tbl_kr_action_nontrans where pk_id = '".$_GET[id]."'");
		$a			=	$rows['allocation'];
		$sql_tanggal = $data->get_row("select max(create_dt) as create_dt from tbl_kr_mst_saham");
		$tanggal = $sql_tanggal[create_dt];
		$txt_code = trim(htmlentities($_POST['txt_code']));
 		$txt_code_to = trim(htmlentities($_POST['txt_code_to']));
		$txt_lembar = trim(htmlentities($_POST['txt_lembar']));
		$txt_harga_prev = trim(htmlentities($_POST['txt_harga_prev']));
        $txt_harga_close = trim(htmlentities($_POST['txt_harga_close']));
        
		
		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
        if($txt_code_to==''){
			echo "<script>alert('Code To is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga_prev==''){
			echo "<script>alert('Harga Prev is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga_close==''){
			echo "<script>alert('Harga Close is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$sql = "UPDATE tbl_kr_action_nontrans SET
			code = '".$txt_code."',
			code_to = '".$txt_code_to."',
			lembar = '".$txt_lembar."',
			harga_prev = '".$txt_harga_prev."',
			harga_close = '".$txt_harga_close."'
			 WHERE pk_id = '".$_GET[id]."'";
		$sql_update ="UPDATE tbl_kr_report_saham set
			harga_close ='".$txt_harga_close."',
			harga_prev ='".$txt_harga_prev."'
			where code = '".$txt_code."' and allocation='".$a."' and create_dt ='".$tanggal."'
			
		";
	
		// print_r ($sql_update);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='action_repo_non.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['edit']==1){
	$rows = $data->get_row("select * from tbl_kr_action_nontrans where pk_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];
	$tittle = "ACTION EDIT ".$allocation2."";
	$dataRows = array (
			'TEXT' =>  array(
						'Allocation',
						'Code',
						'Code to',
						'Number of Shares',
						'Harga Prev',
						'Harga Close',
						),
			'DOT'  => array (':',':',':',':'),
			'FIELD' => array (
			"&nbsp;&nbsp;".$allocation2."",
			"<input type=text name=txt_code readonly=readonly value='".$rows[code]."'>",
			"<input type=text name=txt_code_to readonly=readonly value='".$rows[code_to]."'>",
			"<input type=text name=txt_lembar value='".$rows[lembar]."'>",
			"<input type=text name=txt_harga_prev value='".$rows[harga_prev]."'>",
			"<input type=text name=txt_harga_close value='".$rows[harga_close]."'>"

				
					)
	);
    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='action_repo_non.php';\">"
	);
}

if ($_POST['btn_app']=='save'){
	$flag = true;
	try {
		$rows = $data->get_row("select * from tbl_kr_action_nontrans where pk_id = '".$_GET[id]."'");
		$code 			= $rows[code];
		$code_to 		= $rows[code_to];
		$allocation		= $rows[allocation];
		$tanggal		= $rows[create_dt2];
		$lembar 		= $rows[lembar];
		$harga_close    = $rows[harga_close];
		
		$row_mst = $data->get_row("Select * from tbl_kr_mst_saham where allocation='".$allocation."' and code='".$code_to."'");
		$code_mst 		= $row_mst[code];
		$lembar_mst		= $row_mst[lembar];
		
		$row_max_date =$data -> get_row("Select max(create_dt) as d from tbl_kr_report_saham");
		$tanggal_repot = $row_max_date[d];
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_inc_lembar = trim(htmlentities($_POST['txt_inc_lembar']));
 		$txt_lembar_sisa = $rows[lembar] - $txt_inc_lembar;
		$lembar_total_mst = $lembar_mst + $txt_inc_lembar;
		
		$total = $txt_lembar_sisa * $harga_close;
		$total_mst= $lembar_total_mst * $harga_close;
		
		if($txt_inc_lembar==''){
			echo "<script>alert('Lembar include is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_lembar_sisa < 0 ){
			echo "<script>alert('Lembar tidak mencukupi !!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		
        $sql 	= "UPDATE tbl_kr_action_nontrans SET
				lembar = '".$txt_lembar_sisa."'
				WHERE pk_id = '".$_GET[id]."'";
		 
		$sql_mst = "UPDATE tbl_kr_mst_saham set 
					lembar='".$txt_lembar_sisa."',
					total ='".$total."'
					where allocation='".$allocation."' and code ='".$code."'";
					
		$sql_mst_codeto = "UPDATE tbl_kr_mst_saham set 
					lembar='".$lembar_total_mst."'
					where allocation='".$allocation."' and code ='".$code_to."'";
		
		$sql_repot = "UPDATE tbl_kr_report_saham set 
					lembar='".$txt_lembar_sisa."',
					total ='".$total."'
					where allocation='".$allocation."' and code ='".$code."'";
					
		$sql_repot_code_to = "UPDATE tbl_kr_report_saham set 
					lembar='".$lembar_total_mst."',
					total ='".$total_mst."'
					where allocation='".$allocation."' and code ='".$code_to."' and create_dt='".$tanggal_repot."'";	
		
		print($sql_repot_code_to);
		if (!$data->inpQueryReturnBool($sql)){
		throw new Exception($data->err_report('s02'));
	
		}
		if (!$data->inpQueryReturnBool($sql_mst)){
		throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql_mst_codeto)){
		throw new Exception($data->err_report('s02'));
		}
		if (!$data->inpQueryReturnBool($sql_repot)){
		throw new Exception($data->err_report('s02'));
		}
				if (!$data->inpQueryReturnBool($sql_repot_code_to)){
		throw new Exception($data->err_report('s02'));
		}

	if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='action_repo_non.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['app']==1){
	$rows = $data->get_row("select * from tbl_kr_action_nontrans where pk_id = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$A."'");
	$allocation2 = $rowo['A'];
	$tittle = "APPROVE EDIT ".$allocation2."";
	$dataRows = array (
			'TEXT' =>  array(
						'Allocation',
						'Code',
						'Code to',
						'Number Of Shares',
						'Number Of Shares Include',
						
						),
			'DOT'  => array (':',':',':',':'),
			'FIELD' => array (
			"&nbsp;&nbsp;".$allocation2."",
			"&nbsp;&nbsp;".$rows[code]."",
			"&nbsp;&nbsp;".$rows[code_to]."",
			"&nbsp;&nbsp;".$rows[lembar]."",
			"<input type=text name=txt_inc_lembar value='".$_POST[txt_inc_lembar]."'>"		
					)
	);
    $button = array ('SUBMIT' => "<input type=submit name=btn_app value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='action_repo_non.php';\">"
	);
}


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>