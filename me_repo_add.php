<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('me_repo_add.html');
$tablename = 'tbl_kr_me';

if ($_POST['btn_save']=='save'){
	$flag = true;
	$type = $_GET['type'];
	$nama = $_SESSION['pk_id'];
	$tgl = $_GET['tgl'];
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		
 		$txt_code = trim(htmlentities($_POST['txt_code']));
		$txt_lembar = trim(htmlentities(str_replace(",","",$_POST['txt_lembar'])));
		$txt_harga = trim(htmlentities(str_replace(",","",$_POST['txt_harga'])));
		$txt_status = trim(htmlentities($_POST['txt_status']));
		$txt_allocation = trim(htmlentities($_POST['txt_allocation']));
		$txt_price_type = trim(htmlentities($_POST['txt_price_type']));

		$txt_from = trim(htmlentities($_POST['txt_from']));
		$txt_from = str_replace(',','',$txt_from);
		$txt_to = trim(htmlentities($_POST['txt_to']));
		$txt_to = str_replace(',','',$txt_to);
        $txt_single = trim(htmlentities($_POST['txt_single']));
        // $txt_single = str_replace(',','',$txt_single);
        $txt_single = str_replace(',',',',$txt_single);

		$txt_session = trim(htmlentities($_POST['txt_session']));
		$txt_assign = trim(htmlentities($_POST['txt_assign']));
		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
		$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
		$txt_settlement_date = trim(htmlentities($_POST['txt_settlement_date']));
		$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
		$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
		$txt_cancelation_reason = trim(htmlentities($_POST['txt_cancelation_reason']));

		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_lembar==''){
			echo "<script>alert('Lembar is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_price_type==''){
			echo "<script>alert('Price type is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
        if($txt_allocation==''){
			echo "<script>alert('Allocation is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_assign==''){
			echo "<script>alert('Assign to is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
  		if ($txt_price_type=='2'){
  			if (empty($txt_from)){
  				echo "<script>alert('Price from is Empty!');</script>";
				throw new Exception($data->err_report('s02'));
  			}elseif (empty($txt_to)){
  				echo "<script>alert('Price to is Empty!');</script>";
				throw new Exception($data->err_report('s02'));
  			}elseif ($txt_to < $txt_from){
  				echo "<script>alert('Wrong price range!');</script>";
				throw new Exception($data->err_report('s02'));
  			}
  		}
  		if ($txt_price_type=='4'){
  			if (empty($txt_single)){
  				echo "<script>alert('Price is Empty!');</script>";
				throw new Exception($data->err_report('s02'));
  			}else{
  				$txt_from = $txt_single;
  			}
  		}
        //--------------validasi jumlah unit yg diinput----------
        $lembarV=$data->get_value("SELECT lembar FROM tbl_kr_mst_saham WHERE allocation='".$_POST[txt_allocation]."' AND code='".$_POST[txt_code]."'");
        $cek_buyV=$data->get_value("SELECT COUNT(pk_id) FROM tbl_kr_dealer WHERE code='".$_POST[txt_code]."' AND type_buy='Buy' AND DATE_FORMAT(create_dt,'%Y-%m-%d')=CURDATE()");
        if($cek_buyV > 0){
        $lembarV+=$data->get_value("SELECT SUM(lembar_hist) FROM `tbl_kr_me` WHERE allocation='".$_POST[txt_allocation]."' AND code='".$_POST[txt_code]."' AND Status='Buy' AND create_dt=CURDATE()");
        }
        $cek_sellV=$data->get_value("SELECT COUNT(pk_id) FROM tbl_kr_dealer WHERE code='".$_POST[txt_code]."' AND type_buy='Sell' AND DATE_FORMAT(create_dt,'%Y-%m-%d')=CURDATE()");
        if($cek_sellV > 0){
        $lembarV-=$data->get_value("SELECT SUM(lembar_hist) FROM `tbl_kr_me` WHERE allocation='".$_POST[txt_allocation]."' AND code='".$_POST[txt_code]."' AND Status='Sell' AND create_dt=CURDATE()");
        }
        if($_POST[txt_status]=='Sell' AND $txt_lembar*100 > $lembarV){
            echo "<script>alert('Not Enough Number of Shares !!!');</script>";
            throw new Exception($data->err_report('s02'));
        }
        //---------------------------------------------------------
        
        
  		//--------------------saham-cek lembar-tbl_kr_mst_saham--tbl_kr_me
		if ($type=='1'){
			$rows_c = $data->get_row("select * from tbl_kr_mst_saham where code='".$txt_code."' and allocation='".$txt_allocation."'");
			//print_r ($rows_c['lembar']);
			//print_r ("select * from tbl_kr_mst_saham where code='".$txt_code."' and allocation='".$txt_allocation."'");
			$rows_m = "select lembar_hist,status from tbl_kr_me where code='".$txt_code."' and '".$txt_allocation."' and create_dt=left(now(),10)";
			$hasil = mysql_query($rows_m);
			while ($data2 = mysql_fetch_array($hasil))
			{
				if ($data2['status']=='Buy'){
	            	$lembar_all = ($lembar_all + $data2['lembar_hist']);
				}else if ($data2['status']=='Sell'){
					$lembar_all = ($lembar_all - $data2['lembar_hist']);
				}
			}
			$lembar_txt = $txt_lembar * 100;
			$lembar = $rows_c['lembar'] + $lembar_all;
			//print ($lembar_all);
			//if ($txt_status=='Sell'){
			//	if ($lembar_txt > $lembar){
			//		echo "<script>alert('Lembar tidak mencukupi untuk dijual!');</script>";
			//		throw new Exception($data->err_report('s02'));
			//	}
		//	}
		}
		//-----------------------end--cek-lembar
		#$total = ($txt_harga * $txt_lembar);
		$total = $txt_from * $lembar_txt;
		if ($txt_price_type==3){
			$status_war = 0;
		}else{
			//warning---------------------
			$value_shares = $data->get_row("select sum(total) as jumlah from tbl_kr_mst_saham where allocation='".$txt_allocation."'");
			$value_bonds = $data->get_row("select sum(market_value) as jumlah from tbl_kr_me_bonds where allocation ='".$txt_allocation."'");
			$value_pn = $data->get_row("select sum(price_val) as jumlah from tbl_kr_pn where allocation='".$txt_allocation."'");
			$value = ($value_shares['jumlah'] + $value_bonds['jumlah']) + $value_pn['jumlah'];
			$stock = $data->get_row("select total from tbl_kr_mst_saham where code = '".$txt_code."' and allocation='".$txt_allocation."'");
			$total_all = $stock['total'] + $total;
			
	        if ($value>0){
	        	$jumlah = $total_all * (100/$value);
				$warning = $data->get_rows("SELECT * FROM tbl_kr_warning");
				$valred = ($warning[0][batas]);
				$valyellow = ($warning[1][batas]);
				$valgreen = ($warning[2][batas]);
				if ($jumlah >= $valred){
					$status_war = 1;
				}elseif ($jumlah >= $valyellow){
					$status_war = 2;
				}elseif ($jumlah < $valyellow){
					$status_war = 3;
				}
			}else{
				$status_war = 1;
			}
			//end warning -------
		}
		
  			$sql = "INSERT INTO tbl_kr_me (
			code,
			status,
			lembar_pending,
			lembar,
			lembar_hist,
			price_type,
			price_from,
			price_to,
			warning,
			persen,
			session,
			dealer_id,
			me_id,
			allocation,
			create_dt
			)VALUES(
			'$txt_code',
			'$txt_status',
			'$lembar_txt',
			'$lembar_txt',
			'$lembar_txt',
			'$txt_price_type',
			'$txt_from',
			'$txt_to',
			'$status_war',
			'$jumlah',
			'$txt_session',
			'$txt_assign',
			'$nama',
			'$txt_allocation',
			'$tgl')";
			//print($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}

		$query2= "select * from tbl_kr_saham where code='".$txt_code."'";
		$hasil = mysql_query($query2);
		/*
			$sql_saham1 = "INSERT INTO tbl_kr_saham_tmp (
				code,
				lembar,
				harga,
				total,
				status,
				allocation,
				create_dt,
				transaction_status,
				ta_reference_id,
				ta_reference_no,
				settlement_date,
				im_code,
				br_code,
				settlement_type,
				remarks,
				cancellation_reason
			)VALUES('$txt_code',
				'$lembar_txt',
				'$txt_from',
				'$total',
				'$txt_status',
				'$txt_allocation',
				now(),
				'$txt_transaction_status',
				'$txt_ta_reference_id',
				'$txt_ta_reference_no',
				'$txt_settlement_date',
				'MU002',
				'TP001',
				'$txt_settlement_type',
				'$txt_remarks',
				'$txt_cancelation_reason'
			)";
		*/
		
		$query3 = "select * from tbl_kr_me ORDER BY pk_id DESC LIMIT 1";
		$hasil3 = mysql_query($query3);
		$row_me = mysql_fetch_assoc($hasil3);
		$fk_me = $row_me['pk_id'];
			$sql_saham1 = "INSERT INTO tbl_kr_saham_tmp (
				code,
				lembar,
				harga,
				total,
				status,
				allocation,
				create_dt,
				fk_me,
				transaction_status,
				ta_reference_id,
				ta_reference_no,
				settlement_date,
				im_code,
				br_code,
				settlement_type,
				remarks,
				cancellation_reason
			)VALUES('$txt_code',
				'$lembar_txt',
				'$txt_from',
				'$total',
				'$txt_status',
				'$txt_allocation',
				now(),
				'$fk_me',
				'$txt_transaction_status',
				'$txt_ta_reference_id',
				'$txt_ta_reference_no',
				'$txt_settlement_date',
				'MU002',
				'',
				'$txt_settlement_type',
				'$txt_remarks',
				'$txt_cancelation_reason'
			)";
		//---------------------------------------------------------------------------------
		if (!$data->inpQueryReturnBool($sql_saham1)){
			throw new Exception($data->err_report('s02'));
		}
		/*
		if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}*/
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='me_repo.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
	}
}

if ($_GET['add']==1){
$tittle = "ADD TRANSACTION - SHARES";
 		$txt_code = trim(htmlentities($_POST['txt_code']));
		$txt_lembar = trim(htmlentities(str_replace(",","",$_POST['txt_lembar'])));
		$txt_harga = trim(htmlentities(str_replace(",","",$_POST['txt_harga'])));
		$txt_status = trim(htmlentities($_POST['txt_status']));
		$txt_allocation = trim(htmlentities($_POST['txt_allocation']));
		$txt_price_type = trim(htmlentities($_POST['txt_price_type']));

		$txt_from = trim(htmlentities($_POST['txt_from']));
		$txt_from = str_replace(',','',$txt_from);
		$txt_to = trim(htmlentities($_POST['txt_to']));
		$txt_to = str_replace(',','',$txt_to);
        $txt_single = trim(htmlentities($_POST['txt_single']));
        $txt_single = str_replace(',','',$txt_single);

		$txt_session = trim(htmlentities($_POST['txt_session']));
		$txt_assign = trim(htmlentities($_POST['txt_assign']));
		$txt_transaction_status = trim(htmlentities($_POST['txt_transaction_status']));
		$txt_ta_reference_id = trim(htmlentities($_POST['txt_ta_reference_id']));
		$txt_ta_reference_no = trim(htmlentities($_POST['txt_ta_reference_no']));
		$txt_settlement_date = trim(htmlentities($_POST['txt_settlement_date']));
		$txt_settlement_type = trim(htmlentities($_POST['txt_settlement_type']));
		$txt_remarks = trim(htmlentities($_POST['txt_remarks']));
		$txt_cancelation_reason = trim(htmlentities($_POST['txt_cancelation_reason']));
########################################### cek data ke tabel master saham ############################################################
// ambil value lembar dari master saham
$lembar=$data->get_value("SELECT lembar FROM tbl_kr_mst_saham WHERE allocation='".$_POST[txt_allocation]."' AND code='".$_POST[txt_code]."'");
$cek_buy=$data->get_value("SELECT COUNT(pk_id) FROM tbl_kr_dealer WHERE code='".$_POST[txt_code]."' AND type_buy='Buy' AND DATE_FORMAT(create_dt,'%Y-%m-%d')=CURDATE()");
if($cek_buy > 0){
$lembar+=$data->get_value("SELECT SUM(lembar_hist) FROM `tbl_kr_me` WHERE allocation='".$_POST[txt_allocation]."' AND code='".$_POST[txt_code]."' AND Status='Buy' AND create_dt=CURDATE()");
}
$cek_sell=$data->get_value("SELECT COUNT(pk_id) FROM tbl_kr_dealer WHERE code='".$_POST[txt_code]."' AND type_buy='Sell' AND DATE_FORMAT(create_dt,'%Y-%m-%d')=CURDATE()");
if($cek_sell > 0){
$lembar-=$data->get_value("SELECT SUM(lembar_hist) FROM `tbl_kr_me` WHERE allocation='".$_POST[txt_allocation]."' AND code='".$_POST[txt_code]."' AND Status='Sell' AND create_dt=CURDATE()");
}

#########################################################################################################################
$rows = $data->get_row("select * from tbl_kr_stock_rate where stk_cd='".$_POST[txt_code]."'");
/*$sql_C = "SELECT stk_date,stk_cd,stk_id FROM tbl_kr_stock_rate where stk_date = (select max(stk_date) from tbl_kr_stock_rate) ORDER BY stk_cd asc ";
  	   	#print_r($sql_C);
		$result = mysql_query($sql_C);
  		$saham_contract ="<select size=1 name=txt_code id=txt_code OnChange= \"document.form1.submit();\">";
		$saham_contract .= "<option value=''></option>";
		while ($row = mysql_fetch_array($result))
		{
		$cek_code=$data->get_value("SELECT code FROM tbl_kr_mst_saham WHERE code='".$row['stk_cd']."' AND allocation='".$_POST[txt_allocation]."'");
			if($cek_code<>""){
			$selected = ($_POST[txt_code] == $row['stk_cd'])? ' selected': '';
			$saham_contract .=  "<option value=".$row['stk_cd']." ".$selected.">".$row['stk_cd']."</option>";
			}
			
		}

	//set default value		
		$harga = $_POST['txt_harga'] == "" ? 0 : $_POST['txt_harga'];
		$lembar = $_POST['txt_lembar'] == "" ? 0 : $_POST['txt_lembar'];
		$commision = $_POST['txt_commision'] == "" ? 0 : $_POST['txt_commision'];
		$sales_tax = $_POST['txt_sales_tax'] == "" ? 0 : $_POST['txt_sales_tax'];
		$levy = $_POST['txt_levy'] == "" ? 0 : $_POST['txt_levy'];
		$vat = $_POST['txt_vat'] == "" ? 0 : $_POST['txt_vat'];
		$other_charges = $_POST['txt_other_charges'] == "" ? 0 : $_POST['txt_other_charges'];
		$gross_settlement_amount = $_POST['txt_gross_settlement_amount'] == "" ? 0 : $_POST['txt_gross_settlement_amount'];
		$wht_on_commision = $_POST['txt_wht_on_commision'] == "" ? 0 : $_POST['txt_wht_on_commision'];
		$net_settlement_amount = $_POST['txt_net_sttlement_amount'] == "" ? 0 : $_POST['txt_net_sttlement_amount'];
	//===========

          $saham_contract .="</select>";
*/
#########################################################################################################################

$dataRows = array (
	'TEXT' =>  array(
		'Allocation',
		'Code',
		'Name of Shares',
		'Status',
		'Number of Lots',
		'Price Type',
		'Session',
		'Assign To',
		'',
		'Transaction Status',
		'TA Reference ID',
		'TA Reference No.',
		'Settlement Date',
		'Settlement Type',
		'Remarks',
		'Cancelation Reason'
		),
  	'DOT'  => array (':',':',':',':',':',':',':'),
	'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," OnChange= \"document.form1.submit();\" ")." ",
		$data->cb_saham_contract('txt_code',$_POST[txt_code]," OnChange= \"document.form1.submit();\" "),
		"<input type=text size='50' readonly='readonly' name=txt_saham value='".$rows[stk_name]."'>",
		$data->cb_saham_status('txt_status',$_POST[txt_status],""),
		"<input style='text-align: left;' type=text size='20' name=txt_lembar value='".$_POST[txt_lembar]."' OnChange= \"document.form1.submit();\"> Number of Shares : ".$lembar,
		"".$data->cb_me_price_type('txt_price_type',''," OnChange= \"getNew(this.value);\" ")."&nbsp;&nbsp;
		<span id='value'></span>",
		$data->cb_session_me('txt_session',$_POST[txt_session])." ",
		$data->cb_user('txt_assign',$_POST[txt_assign]),
		"",
		"<select name='txt_transaction_status' id='txt_transaction_status' onchange='changeStatus();'><option value='NEWM'>New Trade</option><option value='CANC'>Cancel Trade</option></select>",
		"<input type=text size='50' name=txt_ta_reference_id id='txt_ta_reference_id' readonly='readonly' value='".$_POST[txt_ta_reference_id]."' placeholder='will be assign by S-INVEST'>",
		"<input type=text size='50' name=txt_ta_reference_no id='txt_ta_reference_no' value='".$_POST[txt_ta_reference_no]."'>",
		$data->datePicker('txt_settlement_date', $_POST[txt_settlement_date],''),
		"<select name='txt_settlement_type'><option value='1'>DVP</option><option value='2'>RVP</option><option value='3'>DFOP</option><option value='4'>RFOP</option></select>",
		"<input type=text size='50' name=txt_remarks value='".$_POST[txt_remarks]."'>",
		"<input type=text size='50' name=txt_cancelation_reason value='".$_POST[txt_cancelation_reason]."'>"
		)
	);
if(empty($_POST[txt_status])){
$_POST[txt_status]='Buy';
}
if(($_POST[txt_status]=='Buy') OR ($_POST[txt_status]=='Sell' AND $_POST[txt_lembar]<=$lembar) AND $lembar<>0){
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='me_repo.php';\">");
}else if ($lembar==''){
	$button = array ('SUBMIT' => "Number of Shares is EMPTY!!!",
					 'RESET'  => "");
}else{
	$button = array ('SUBMIT' => "Not Enough Number of Shares !!!",
					 'RESET'  => "");
}

}

$javascript = "<script type='text/javascript'>
	/*
	function calculate(){
		var type = document.getElementById('txt_status').value;

		var doc = document;

		var harga = parseFloat(doc.getElementById('txt_harga').value.toString().replace(',',''));
		var lembar = parseFloat(doc.getElementById('txt_lembar').value.toString().replace(',',''));

		var tradeAmount = harga * lembar;

		var commision = parseFloat(doc.getElementById('txt_commision').value.toString().replace(',',''));
		var salesTax = parseFloat(doc.getElementById('txt_sales_tax').value.toString().replace(',',''));
		var levy = parseFloat(doc.getElementById('txt_levy').value.toString().replace(',',''));
		var vat = parseFloat(doc.getElementById('txt_vat').value.toString().replace(',',''));
		var otherCharges = parseFloat(doc.getElementById('txt_other_charges').value.toString().replace(',',''));

		var whtOnCommision = parseFloat(doc.getElementById('txt_wht_on_commision').value.toString().replace(',',''));

		if(type == 'Buy'){

			salesTax = 0;

			doc.getElementById('txt_sales_tax').value = 0;
			doc.getElementById('txt_sales_tax').readOnly = true;

			var gross = tradeAmount + commision + levy + salesTax + vat + otherCharges;
			var net = gross - whtOnCommision;

		}else{
			doc.getElementById('txt_sales_tax').readOnly = false;

			var gross = tradeAmount - commision - levy - salesTax - vat - otherCharges;
			var net = gross + whtOnCommision;
		}
		doc.getElementById('txt_gross_settlement_amount').value = reformat2(gross);
		doc.getElementById('txt_net_settlement_amount').value = reformat2(net);
	}
*/
	function changeStatus(){
		if(document.getElementById('txt_transaction_status').value == 'NEWM'){
			document.getElementById('txt_ta_reference_id').value = '';
			document.getElementById('txt_ta_reference_id').readOnly = true;
		}else{
			document.getElementById('txt_ta_reference_id').readOnly = false;
		}
	}
	
</script>";

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css',
	  'PATHCUSTOMJS' => $GLOBALS['JS'].'custom.js'
      	);

$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>