<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$transaction_date = $_GET['transaction_date'];
$filename = $transaction_date."_distributed_income_data.txt";

// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/plain; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
//fputcsv($output, array('Column 1', 'Column 2', 'Column 3'));

// fetch the data
//mysql_connect('localhost', 'username', 'password');
//mysql_select_db('database');
$query = "SELECT 
    data_type, 
    im_code, 
    fund_code, 
    cum_date, 
    ex_date, 
    dist_inc_per_unit, 
    dist_inc_policy, 
    payment_date, 
    dist_inc_no 
 
    FROM tbl_kr_dist_inc_im 
    WHERE DATE_FORMAT(tbl_kr_dist_inc_im.created_time, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0";

$rows = $data->get_rows2($query);

fwrite($output, "\r\n");

// loop over the rows, outputting them
for($i=0;$i<count($rows);$i++){

    $arr = array();
    $var = $rows[$i];

    $idx = 0;
    $arr[$idx] = $var['data_type'];                                               $idx++;     //0.
    $arr[$idx] = $var['im_code'];                                               $idx++;     //0.
    $arr[$idx] = $var['fund_code'];                                               $idx++;     //0.

    $cumDate = '';
    if($var['cum_date'] != null && $var['cum_date'] != '' && $var['cum_date'] != '0000-00-00')
        $cumDate = str_replace('-','',$var['cum_date']);

    $arr[$idx] = $cumDate;                                                      $idx++;

    $exDate = '';
    if($var['ex_date'] != null && $var['ex_date'] != '' && $var['ex_date'] != '0000-00-00')
        $exDate = str_replace('-','',$var['ex_date']);

    $arr[$idx] = $exDate;                                                       $idx++;
    $arr[$idx] = $var['dist_inc_per_unit'];                                               $idx++;     //0.
    $arr[$idx] = $var['dist_inc_policy'];                                               $idx++;     //0.

    $date = '';
    if($var['payment_date'] != null && $var['payment_date'] != '' && $var['payment_date'] != '0000-00-00')
        $date = str_replace('-','',$var['payment_date']);

    $arr[$idx] = $date;
    $arr[$idx] = $var['dist_inc_no']; 

    $str = "";
    for($j=0; $j<count($arr);$j++){
        if($j > 0) $str .="|";
        $str.= $arr[$j];
    }
    $str.="\r\n";
    //fputcsv($output, $arr);
    fwrite($output, $str);
}

fclose($output);
?>
