<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('liquidation_add.html');

$id = '0';
$fund_code = '';
$sa_code = '';


$errorArr = array();
for($i=0;$i<2;$i++)
	$errorArr[$i] = '';
$otherError='';

if($_GET['edit']==1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_allocation_sa WHERE allocation_sa_id = $id";
    $result = $data->get_row($query);
	$fund_code = $result['fund_code'];
	$sa_code = $result['sa_code'];
}


if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
		$id = trim(htmlentities($_POST['inputId']));
		$fund_code = trim(htmlentities($_POST['fund_code']));
		$sa_code = trim(htmlentities($_POST['sa_code']));
				
		$gotError = false;
		if($fund_code==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		if($sa_code==''){
			$errorArr[1] = "SA Code must be filled";
			$gotError = true;
		}
		
        if($id == 0 && !$gotError){
            $query = "SELECT * FROM tbl_kr_allocation_sa WHERE sa_code='$sa_code' AND fund_code='$fund_code'";
            if($data->queryReturnExist($query)){
                $gotError = true;
                $otherError='The Data Already Exist';
            }
        }else{
            $query = "SELECT * FROM tbl_kr_allocation_sa WHERE sa_code='$sa_code' AND fund_code='$fund_code' AND allocation_sa_id != '$id'";
            if($data->queryReturnExist($query)){
                $gotError = true;
                $otherError='The Data Already Exist';
            }
        }
        

		if (!$gotError){
			
			if($id == 0){
				$query = "INSERT INTO tbl_kr_allocation_sa (
					fund_code,
					sa_code
                    
				)VALUES(
					'$fund_code',
					'$sa_code'
                    
                )";
			}else{
				$query = "UPDATE tbl_kr_allocation_sa SET
					fund_code='$fund_code',
					sa_code='$sa_code'
				WHERE allocation_sa_id = '$id'";
			}
			

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='product_sa.php?view=1';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = ($id == '0' ? "ADD" : "EDIT")." - FUND SA";
$dataRows = array (
	    'TEXT' => array(
				'Fund',
				'SA',
			),
	    'FIELD' => array (
			"<input type=hidden name=inputId id=inputId value='$id'>".
            $data->cb_fundcode('fund_code', $fund_code),
            $data->cb_sacode('sa_code', $sa_code)
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='product_sa.php?view=1';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>