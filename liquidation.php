<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'liquidation.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new liquidation;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('liquidation.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_liquidation.liquidation_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$linkAdd = 'liquidation_add.php';
$linkExport = 'liquidation_export.php';

$btnView = '<input type="submit" name="btnView" value="View">';
$btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'\'">';
$btnExport = '<input type="button" name="btnExport" value="Export" onclick="alert(\'Please pick transaction date first\')">';

$datagrid = array();

if ($_POST['btnView']=='View'){
  	$transaction_date =  trim(htmlentities($_POST['transactionDate']));
	if (empty($transaction_date))
		$transaction_date = date("Y-m-d");

    $btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';
	
  	$sql = "SELECT 
	  			liquidation_id
				, im_code as IMCODE
				, fund_code as FUNDCODE
				, last_nav_date as LASTNAVDATE
				, payment_date as PAYMENTDATE
			FROM tbl_kr_liquidation
			WHERE DATE_FORMAT(tbl_kr_liquidation.created_date, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0 
			ORDER BY $order_by $sort_order";
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->liquidationData($sql, 'edit', 'liquidation_edit.php', 'delete', 'liquidation.php');
}

if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "UPDATE tbl_kr_liquidation SET is_deleted=1 WHERE liquidation_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='liquidation.php';</script>";
	}
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addVar('page','export',$btnExport);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>