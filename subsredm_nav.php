<?php
    session_start();
    #session_destroy();
    #print_r($_SESSION);
    require_once 'global.inc.php';
    require_once $GLOBALS['CLASS'].'global.class.php';
    require_once $GLOBALS['CLASS'].'xajax.inc.php';
    require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
    require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
    $data = new globalFunction;
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
    <title>activeLink</title>
    <script type="text/javascript">
        //Sets active link for ul.li.a
        function activeLink(objLink){ 
            var list = document.getElementById('videoList').getElementsByTagName('a');
            for (var i = list.length - 1; i >= 0; i--){
                list[i].className='nonActiveVid';
            };
            objLink.className= 'activeVid';
        }
    </script>
</head>
<body id="activelink" onLoad="">    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <ul class="activeVid" id="videoList">
                    <?php if($data->auth_boolean(4010,$_SESSION['pk_id'])){ ?>
                    <li> <a href="subsredm_order.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Subs/Redm Order</a></li>
                    <?php } ?>
                    <?php if($data->auth_boolean(4020,$_SESSION['pk_id'])){ ?>
                    <li> <a href="subsredm_fee.php" target="contentTabFrame" onClick="activeLink(this);">Subs/Redm Fee</a></li>
                    <?php } ?>
                    <?php if($data->auth_boolean(4030,$_SESSION['pk_id'])){ ?>
                    <li> <a href="swtc_order.php" target="contentTabFrame" onClick="activeLink(this);">Swtc Order</a></li>
                    <?php } ?>
                    <?php if($data->auth_boolean(4040,$_SESSION['pk_id'])){ ?>
                    <li> <a href="swtc_fee.php" target="contentTabFrame" onClick="activeLink(this);">Swtc Fee</a></li>
                    <?php } ?>
                </ul>
            </td>
        </tr>
    </table>
</body>
</html>