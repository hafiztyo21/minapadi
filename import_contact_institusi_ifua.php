<?php
session_start();
include 'Classes/excel_reader2.php';
//include 'Classes/PHPExcel/Writer/Excel2007.php';
//include 'Classes/PHPExcel/IOFactory.php';
//include "excel_reader2.php";
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$datadb = new globalFunction();
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('import_contact_institusi_ifua.html');
$tablename = 'tbl_kr_cus_institusi';
$tablename_detail = 'tbl_kr_cus_institusi_ifua';


if($_POST[Submit]) {

    $myfile = fopen($_FILES['uploadedfile']['tmp_name'], "r") or die("Unable to open file!");
    // Output one line until end-of-file
    $suksesUpdate = 0;
    $suksesInsertIfua = 0;
    $gagalUpdate = 0;
    $gagalInsertIfua = 0;

    $errorResult = "";

    while(!feof($myfile)) {
        //echo fgets($myfile) . "<br>";
        $line = fgets($myfile);        
        if(strpos(strtolower($line), 'sa code') !== false){

        }else{
            $param = explode("|",$line);
            if(count($param) == 1) continue;

            $sid = $param[2];
            $ifua = $param[3];
            $ifuaName = $param[4];
            $clientCode = $param[5];
            $npwpNo = $param[13];

            $cek_data=$datadb->get_value("select count(cus_id) from tbl_kr_cus_institusi where cus_no_npwp='".$npwpNo."'");
            if ($cek_data ==0){
                $errorResult .= "NPWP '$npwpNo' tidak di temukan dalam Database<br/>";
                $gagalInsertIfua++;
            }else{
                $query = "SELECT cus_id, cus_ins_sid, client_code, bank_bic_code1, bank_bi_member_code1
                    , bank_name1, bank_country1, bank_branch1, acc_ccy1, acc_no1, acc_name1  
                    FROM tbl_kr_cus_institusi WHERE cus_no_npwp='".$npwpNo."'";
                $row = $datadb->get_row($query);
                
                if($row['client_code'] == ''){
                    $query = "UPDATE tbl_kr_cus_institusi SET client_code='".$client_code."' WHERE cus_no_npwp='".$npwpNo."'";  
                    if ($datadb->inpQueryReturnBool($query))
                    {	//echo "<script>alert('SUKSES');</script>";	
                        //$suksesUpdate++;
                    }
                    else
                    {	//echo "<script>alert('GAGAL');</script>";
                        //$gagalUpdate++;
                    }
                }
                
                if($row['cus_ins_id'] == ''){
                    $query = "UPDATE tbl_kr_cus_institusi SET cus_ins_sid='".$sid."' WHERE cus_no_npwp='".$npwpNo."'";  
                    if ($datadb->inpQueryReturnBool($query))
                    {	//echo "<script>alert('SUKSES');</script>";	
                        //$suksesUpdate++;
                    }
                    else
                    {	//echo "<script>alert('GAGAL');</script>";
                        //$gagalUpdate++;
                    }
                }

                $bicCode = $row['bank_bic_code1'];
                $biMemberCode = $row['bank_bi_member_code1'];
                $bankName = $row['bank_name1'];
                $bankCountry = $row['bank_country1'];
                $bankBranch = $row['bank_branch1'];
                $accCcy = $row['acc_ccy1'];
                $accNumber = $row['acc_no1'];
                $accName = $row['acc_name1'];
                
                $query = "SELECT count(ifua_code) FROM tbl_kr_cus_institusi_ifua WHERE is_deleted=0 and ifua_code = '$ifua'";
                $ifuadata = $datadb->get_value($query);
                if($ifuadata == 0){
                    $saved = false;
                    $query = "INSERT INTO tbl_kr_cus_institusi_ifua (cus_ins_id, ifua_code, ifua_name, created_date, created_by, last_updated_date, last_updated_by, is_deleted)
                    VALUES ('".$row['cus_id']."','".$ifua."','".$ifuaName."',now(),'".$_SESSION['pk_id']."', now(), '".$_SESSION['pk_id']."',0)";  
                    if ($datadb->inpQueryReturnBool($query))
                    {	//echo "<script>alert('SUKSES');</script>";	
                        $suksesInsertIfua++;
                        $saved = true;
                    }
                    else
                    {	//echo "<script>alert('GAGAL');</script>";
                        $errorResult .= "Ifua '$ifua' gagal disimpan ke dalam Database (".$datadb->queryError().")<br/>";
                        $gagalInsertIfua++;
                    }
                    if($saved){
                        $lastid = $datadb->getLastId();

                        $query = "INSERT INTO tbl_kr_cus_institusi_ifua_redm (ifua_id, sequential_code, bic_code, bi_member_code, bank_name, bank_country, bank_branch, ac_ccy, ac_name, ac_no, created_date, created_by, last_updated_date, last_updated_by, is_deleted)
                        VALUES ($lastid, '1', '$bicCode', '$biMemberCode', '$bankName', '$bankCountry', '$bankBranch', '$accCcy', '$accName', '$accNumber', now(), '".$_SESSION['pk_id']."', now(), '".$_SESSION['pk_id']."', 0)";
                        if ($datadb->inpQueryReturnBool($query))
                        {		
                        }
                        else
                        {	
                            $errorResult .= "Ifua '$ifua' gagal disimpan Redemp Payment A/C ke dalam Database (".$datadb->queryError().")<br/>";
                        }
                    }
                    
                }else{
                    $errorResult .= "Ifua '$ifua' sudah ada dalam Database<br/>";

                }
                
            }

        }
        
        

    }
    fclose($myfile);
        
    // tampilan status sukses dan gagal
    echo "<h3>Proses import data selesai.</h3>";
    echo "<p>Jumlah data yang sukses diimport : ".$suksesInsertIfua."<br>";
    echo "Jumlah data yang gagal diimport : ".$gagalInsertIfua."</p>";
    if($errorResult != "")
	    echo "Error Log : <br/>".$errorResult;
	
}



$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->addVar('page', 'tittle',$tittle);
$tmpl->addRows('loopData',$DG);
$tmpl->displayParsedTemplate('page');
?>