<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('me_edit_app.html');

if ($_POST['btn_save']=='save'){	$pk = $_GET['pk'];
	$id = $_GET['id'];
	$fk = $_GET['fk'];
	$tgl = $_GET['tgl'];
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
 		$txt_lembar = trim(htmlentities($_POST['txt_lembar']));
		$txt_harga = trim(htmlentities($_POST['txt_harga']));
		if($txt_lembar==''){
			echo "<script>alert('Number of Shares is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga==''){
			echo "<script>alert('Price is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$total = $txt_lembar * $txt_harga;
        //warning---------------------
		$value_shares = $data->get_row("select sum(total) as jumlah from tbl_kr_mst_saham");
		$value_bonds = $data->get_row("select sum(market_value) as jumlah from tbl_kr_me_bonds");
		$value_pn = $data->get_row("select sum(price_val) as jumlah from tbl_kr_pn");
		$value = $value_shares['jumlah'] + $value_bonds['jumlah'] + $value_pn['jumlah'];
		if ($value>0){
     		$jumlah = $total * (100/$value);
			$warning = $data->get_rows("SELECT * FROM tbl_kr_warning");
			$valred = ($warning[0][batas]);
			$valyellow = ($warning[1][batas]);
			$valgreen = ($warning[2][batas]);
		if ($jumlah >= $valred){
			$status_war = 1;
		}elseif ($jumlah >= $valyellow){
			$status_war = 2;
		}else{
			$status_war = 3;
		}
		}else{
			$status_war = 1;
		}
		//end warning -------
  		$sql = "update tbl_kr_dealer set
  				lembar = '".$txt_lembar."',
  				harga = '".$txt_harga."',
  				total = '".$total."',
  				persen = '".$jumlah."',
  				warning = '".$status_war."',
  				edit_id = 0 where pk_id ='".$pk."'";
		//print($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='detail_dealer.php?detail=1&id=".$id."&fk=".$fk."&tgl=".$tgl."';</script>";
	}catch (Exception $e1){
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
	}
}



if ($_GET['edit']==1){
$pk = $_GET['pk'];
$id = $_GET['id'];
$fk = $_GET['fk'];
$tgl = $_GET['tgl'];
$tittle = "EDIT TRANSACTION - SHARES";
$rows = $data->get_row("select tbl_kr_dealer.*, format(total,0) as TTL, stock_name, fullname from tbl_kr_dealer
						left join tbl_kr_stock on tbl_kr_stock.stock_id = tbl_kr_dealer.code
						left join tbl_kr_user on tbl_kr_user.pk_id = tbl_kr_dealer.dealer_id where tbl_kr_dealer.pk_id = '".$pk."'");
$dataRows = array (
	'TEXT' =>  array('Name of Shares','Status','Number of Shares','Price','Total','Dealer'),
  	'DOT'  => array (':',':',':',':',':',':'),
	'FIELD' => array (
			'&nbsp;'.$rows['stock_name'],
			'&nbsp;'.$rows['type_buy'],
			"<input type=text name=txt_lembar value='".$rows['lembar']."' >",
			"<input type=text name=txt_harga value='".$rows['harga']."' >",
			'&nbsp;Rp.&nbsp;'.$rows['TTL'],
			'&nbsp;'.$rows['fullname']

		)
	);
$button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					'RESET'  => "<input type=reset name=reset value=reset>
									<input type=button name=cancel value=cancel onclick=\"window.location='detail_dealer.php?detail=1&id=".$id."&fk=".$fk."&tgl=".$tgl."';\">");
}

$tmpl->addVars('row',$dataRows );
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>