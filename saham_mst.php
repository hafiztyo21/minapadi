<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('saham_mst.html');


####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_stock_rate.stk_cd'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
$tombol			= "<input type=submit name=btn_view value=View>";
$viewlink = 'saham_repo.php';
$link = 'saham_add.php';
$link2 = 'saham_sim.php';
$addLink = "<input type=button name=add value='Add Transaction' onclick=\"window.location='".$link."?add=1';\">";

$linkExport = 'saham_export_filter.php';
$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'\',\'Export\',\'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=350,height=300 \')">';

$back= "<a href='saham_mst.php'><< Back</a>";
$tmpl->addVar('page','back',$back);
if($data->auth_boolean(1310,$_SESSION['pk_id'])){
    $tmpl->addVar('page','add',$addLink);
    $tmpl->addVar('page','export',$btnExport);

}
$tmpl->addVar('page','Simulation Add',$addLink2);
$allocation 	= $data->cb_allocation('txt_allocation',$_POST[txt_allocation]," ");
$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
$rowo = $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$txt_allocation."'");
$allocation2 = $rowo['A'];

if ($_POST['btn_search'] )
{
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql  = "SELECT stk_name, FORMAT( avg, 2 ) AS average,total, FORMAT( total, 2 ) AS ttl, stk_cd, FORMAT( lembar, 0 ) AS lembar2, code
		FROM tbl_kr_mst_saham
		LEFT JOIN tbl_kr_stock_rate ON tbl_kr_mst_saham.code = tbl_kr_stock_rate.stk_cd
      where 1 and tbl_kr_stock_rate.stk_date=(select max(stk_date) from tbl_kr_stock_rate) AND upper($id_serach) like upper('%$q_serach%') order by $order_by $sort_order";
//print_r($sql);
	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else

{
	$_SESSION['sql']='';

	$sql  = "SELECT stk_name, FORMAT( avg, 2 ) AS average,total, FORMAT( total, 2 ) AS ttl, stk_cd, FORMAT( lembar, 0 ) AS lembar2, code, format(harga_close,0) as CLOSE, allocation
		FROM tbl_kr_mst_saham
		LEFT JOIN tbl_kr_stock_rate ON tbl_kr_mst_saham.code = tbl_kr_stock_rate.stk_cd
      			where 1 and tbl_kr_stock_rate.stk_date=(select max(stk_date) from tbl_kr_stock_rate) AND allocation ='".$txt_allocation."' order by $order_by $sort_order";
//print_r($sql);

}


if ($_POST['btn_view']=='View'){
  $id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];
  
  $sql = "SELECT stk_name, FORMAT( avg, 2 ) AS average,total, FORMAT( total, 2 ) AS ttl, stk_cd, FORMAT( lembar, 0 ) AS lembar2, code, allocation
		 FROM tbl_kr_mst_saham
		 LEFT JOIN tbl_kr_stock_rate ON tbl_kr_mst_saham.code = tbl_kr_stock_rate.stk_cd
         where 1 and tbl_kr_stock_rate.stk_date=(select max(stk_date) from tbl_kr_stock_rate) AND allocation ='".$txt_allocation."' AND upper($id_serach) like upper('%$q_serach%') order by $order_by $sort_order";

		 }
$arrFields = array(
		'tbl_kr_stock_rate.stk_cd'=>'CODE',
		'tbl_kr_stock_rate.stk_name'=>'NAMA'

);
//print_r($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridSaham($sql,'code',$data->ResultsPerPage,$pg,'view',$viewlink,'menu',$link,'edit',$link,'delete',$link, '', '', $txt_allocation);

  #print_r ($DG);
#$data->listData();
if ($txt_allocation != 0)
{
    //=======GET TOTAL AKTIVA BERSIH====
    $rowc = $data->get_row("select * from tbl_kr_cash where create_dt=(select max(stk_date) from tbl_kr_stock_rate) and allocation='".$txt_allocation."'");
    $txt_kas = $rowc['kas_giro'];
    $txt_piutang =$rowc['total_piutang'];
    $txt_aktiva = $rowc['aktiva_lain'];
    $txt_kewajiban = $rowc['total_kewajiban'];

    $rowp = $data->get_row("select format(SUM(price_val),0) as quantity, sum(price_val) as prc_val from tbl_kr_report_pn 
                            where create_dt=(select max(stk_date) from tbl_kr_stock_rate) and allocation='".$txt_allocation."'");
    $a = $rowp['prc_val'];

    $rowo = $data->get_row("select 
                    sum(market_value) as market
                    from tbl_kr_report_bonds 
                    where create_dt=(select max(stk_date) from tbl_kr_stock_rate) and allocation='".$txt_allocation."'");

    $b = $rowo['market'];

    $rowb = $data->get_row("select format(SUM(face_value),0) as AKTIV, format(SUM(face_value),0) as VAL, sum(face_value) as FACE from tbl_kr_report_deposit 
                            where create_dt=(select max(stk_date) from tbl_kr_stock_rate) and allocation='".$txt_allocation."'");
    $d = $rowb['FACE'];

    $rows = $data->get_row("select format(SUM(total),0) as total2, format(SUM(lembar),0) as lembar2, sum(total) as TTL from tbl_kr_report_saham 
                            where create_dt=(select max(stk_date) from tbl_kr_stock_rate) and allocation='".$txt_allocation."'");
    $c = $rows['TTL'];

    $total_aktiva = $a + $b + $c + $d+ $txt_kas + $txt_piutang + $txt_aktiva;
    $total_aktiva_bersih = $total_aktiva - $txt_kewajiban;
    //echo $total_aktiva_bersih;
    //===============================   
    
    for($i=0;$i<count($DG);$i++){
        $DG[$i]['WEIGHT'] = number_format($DG[$i]['total'] / $total_aktiva_bersih * 100,2);
        
    }
    
    $value_shares = $data->get_row("select sum(total) as jumlah from tbl_kr_mst_saham where allocation ='".$txt_allocation."'");
    $value_bonds = $data->get_row("select sum(market_value) as jumlah from tbl_kr_me_bonds where allocation ='".$txt_allocation."'");
    $value_pn = $data->get_row("select sum(price_val) as jumlah from tbl_kr_pn where allocation ='".$txt_allocation."'");
    $value = ($value_shares['jumlah'] + $value_bonds['jumlah']) + $value_pn['jumlah'];
    //print_r ($value);
    $weight = number_format($value_shares['jumlah'] * (100/$value),2);

    $total_all = number_format($value_shares['jumlah'],0);
    //-------warna
    if ($weight<=15 or $weight>=70){
        $warna ="<img src='image/red.png' width='12' height='12'>";
    }else{
        $warna ="<img src='image/green.png' width='12' height='12'>";
    }
}
//----------


$tmpl->addVar('page', 'judul',$allocation2); 
$tmpl->addVar('page','tombol',$tombol);
$tmpl->addVar('page', 'warna',$warna);
$tmpl->addVar('page', 'weight_all',$weight);
$tmpl->addVar('page', 'total_all',$total_all);
$tmpl->addVar('page', 'combo',$allocation);
$tmpl->addRows('loopData',$DG);
$tmpl->addVar('page', 'search',$searchCB);
$tmpl->displayParsedTemplate('page');
?>