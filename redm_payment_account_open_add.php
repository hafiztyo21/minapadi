<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('redm_payment_account_open_add.html');

$invs_acc_no = '';
$bank_bic_code = '';
$bank_bi_member_code = '';
$bank_name = '';
$bank_country = '';

$bank_branch = '';
$payment_acc_ccy = '';
$payment_acc_no = '';
$payment_acc_name = '';

$errorArr = array('','','','','','','','','');
$otherError='';

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
 		
        $invs_acc_no = trim(htmlentities($_POST['invs_acc_no']));
        $bank_bic_code = trim(htmlentities($_POST['bank_bic_code']));
        $bank_bi_member_code = trim(htmlentities($_POST['bank_bi_member_code']));
        $bank_name = trim(htmlentities($_POST['bank_name']));
        $bank_country = trim(htmlentities($_POST['bank_country']));

        $bank_branch = trim(htmlentities($_POST['bank_branch']));
        $payment_acc_ccy = trim(htmlentities($_POST['payment_acc_ccy']));
        $payment_acc_no = trim(htmlentities($_POST['payment_acc_no']));
        $payment_acc_name = trim(htmlentities($_POST['payment_acc_name']));
        
		$gotError = false;
		/*if($fundCode==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}*/
		
        
		if (!$gotError){
			
			$query = "INSERT INTO tbl_kr_redm_payment_acc_open (
					invs_acc_no,
					bank_bic_code,
					bank_bi_member_code,
					bank_name,
                    bank_country,
                    bank_branch,
                    payment_acc_ccy,
                    payment_acc_no,
                    payment_acc_name,
					created_date,
					created_by,
					last_updated_date,
					last_updated_by,
					is_deleted
				)VALUES(
                    '$invs_acc_no',
                    '$bank_bic_code',
                    '$bank_bi_member_code',
                    '$bank_name',
                    '$bank_country',
                    '$bank_branch',
                    '$payment_acc_ccy',
                    '$payment_acc_no',
                    '$payment_acc_name',
                    now(),
                    '".$_SESSION['pk_id']."',
                    now(),
                    '".$_SESSION['pk_id']."',
                    '0')";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='redm_payment_accoun_open.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = "ADD - REDM PAYMENT ACCOUNT OPEN";
$dataRows = array (
	    'TEXT' => array(
            'Investor Fund Unit Acc No','Bank BIC Code','Bank BI Member Code', 'Bank Name', 'Bank Country', 'Bank Branch', 'Bank Acc CCY', 'Bank Acc No', 'Bank Acc Name'),
  	    'DOT'  => array (':',':',':',':'),
	    'FIELD' => array (
            "<input type=text size='50' maxlength=16 name=invs_acc_no value='$invs_acc_no'>",
		    "<input type=text size='50' maxlength=11 name=bank_bic_code value='$bank_bic_code'>",
            "<input type=text size='50' maxlength=17 name=bank_bi_member_code value='$bank_bi_member_code'>",
            "<input type=text size='50' maxlength=100 name=bank_name value='$bank_name'>",
            $data->cb_isocountry('bank_country',$bank_country),
            "<input type=text size='50' maxlength=20 name=bank_branch value='$bank_branch'>",
            $data->cb_accountccy('bank_acc_ccy',$bank_acc_ccy),
            "<input type=text size='50' maxlength=30 name=payment_acc_no value='$payment_acc_no'>",
            "<input type=text size='50' maxlength=100 name=payment_acc_name value='$payment_acc_name'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='redm_payment_account_open.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>