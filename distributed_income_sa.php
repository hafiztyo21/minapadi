<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('distributed_income_sa.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_dist_inc_sa.dist_inc_sa_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$linkAdd = 'distributed_income_sa_add.php';
$linkExport = 'distributed_income_sa_export.php';

$btnView = '<input type="submit" name="btnView" value="View">';
$btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'\'">';
$btnExport = '<input type="button" name="btnExport" value="Export" onclick="alert(\'Please choose transaction date first\');">';
$datagrid = array();

if ($_POST['btnView']=='View'){
  	$transaction_date =  trim(htmlentities($_POST['transactionDate']));
	if (empty($transaction_date))
		$transaction_date = date("Y-m-d");

	$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';
	
  	$sql = "SELECT 
	  			dist_inc_sa_id
				, sa_code as SACODE
				, fund_code as FUNDCODE
				, investor_ac_no as INVESTORACNO
				, dist_inc_opt as `OPTION`
			FROM tbl_kr_dist_inc_sa
			WHERE DATE_FORMAT(tbl_kr_dist_inc_sa.created_time, '%Y-%m-%d') = '$transaction_date' AND is_deleted=0 
			ORDER BY $order_by $sort_order";
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->dataDistributedIncomeSA($sql, 'edit', 'distributed_income_sa_edit.php', 'delete', 'distributed_income_sa.php');
}
	/*$arrFields = array(
		'tbl_kr_stock_rate.stk_field'=>'CODE',
		'tbl_kr_stock_rate.stk_name'=>'NAMA'
	);*/

if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "UPDATE tbl_kr_dist_inc_sa SET is_deleted=1 WHERE dist_inc_sa_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='distributed_income_sa.php';</script>";
	}
}

$tmpl->addVar('page','datepicker',$datepicker);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addVar('page','export',$btnExport);
$tmpl->addRows('loopData', $datagrid);

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>