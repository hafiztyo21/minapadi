<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contact_ifua.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contact_ifua;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('contact_institusi_ifua_redm.html');
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='B.ifua_code, A.sequential_code'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

//$datepicker = $data->datePicker('transactionDate', $_POST[transactionDate],'');
$searchInput = '<input type="text" name="search" id="search" value="'.$_POST['search'].'">';
$linkAdd = 'contact_institusi_ifua_redm_form.php?add=1';
//$linkExport = 'swtc_order_export.php';

$btnView = '<input type="submit" name="btnView" value="View">';
$btnAdd = '<input type="button" name="btnAdd" value="Add Transaction" onclick="window.location=\''.$linkAdd.'\'">';
//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="alert(\'Please choose transaction date first\');">';

$datagrid = array();

if ($_POST['btnView']=='View' || $_GET['view']=='1'){
    $search = "";
    if(isset($_POST['search']))
  	    $search =  trim(htmlentities($_POST['search']));
	//if (empty($transaction_date))
		//$transaction_date = date("Y-m-d");
	
	//$btnExport = '<input type="button" name="btnExport" value="Export" onclick="window.open(\''.$linkExport.'?transaction_date='.$transaction_date.'\');">';

  	$sql = "SELECT 
	  			redm_id
                , A.ifua_id as IFUAID
                , B.ifua_code as IFUACODE
                , B.ifua_name as IFUANAME
                , A.sequential_code as SEQUENTIALCODE
				, A.bic_code as BICCODE
                , A.bi_member_code as BIMEMBERCODE
				, A.ac_no as ACNO
			FROM tbl_kr_cus_institusi_ifua_redm A LEFT JOIN tbl_kr_cus_institusi_ifua B ON A.ifua_id = B.ifua_id
			WHERE  A.is_deleted=0 AND (B.ifua_code like '%$search%' OR B.ifua_name like '%$search%' OR bic_code like '%$search%' OR bi_member_code like '%$search%' OR ac_no like '%$search%')
			ORDER BY $order_by $sort_order";
	//$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$datagrid= $data->ifuaRedmData($sql, 'edit', 'contact_institusi_ifua_redm_form.php', 'delete', 'contact_institusi_ifua_redm.php', 'redm_id');
}

if($_GET['del'] == '1'){
	$id=$_GET['id'];
	
	$query = "UPDATE tbl_kr_cus_institusi_ifua_redm SET is_deleted=1 WHERE redm_id=$id";
	if (!$data->inpQueryReturnBool($query)){
		echo "<script>alert('Error Delete : ".mysql_error()."');</script>";
	}else{
		echo "<script>alert('Delete Success'); window.location='contact_institusi_ifua_redm.php';</script>";
	}
}

$tmpl->addVar('page','search',$searchInput);
$tmpl->addVar('page','view',$btnView);
$tmpl->addVar('page','add',$btnAdd);
$tmpl->addVar('page','export',$btnExport);
$tmpl->addRows('loopData', $datagrid);

$path = array(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
);
$tmpl->addVars('path',$path);
$tmpl->displayParsedTemplate('page');
?>