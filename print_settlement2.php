<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'contract.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new contract;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('print_settlement2.html');

header("Content-type: application/vnd.msword");
header("Content-Disposition: attachment; filename=Instruksi_Jual_Beli_Saham ".$_GET['tgl']."".$prev.".doc");
header("Pragma: no-cache");
header("Expires: 0");

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_kr_dealer_tmp.pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='desc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);


###########################end of sorting##################################

$tanggal = $_GET['tgl'];
$cd = $_GET['code'];
$allocation = $_GET['all'];
$txt_hari = $_GET['day'];
$rowo = $data->get_row("select cd_client as D, description as C, up as UP, bank as BANK from tbl_kr_allocation where pk_id = '".$allocation."'");

$up 	= $rowo['UP'];
$bank	= $rowo['BANK'];
$c	= $rowo['C'];
$d	= $rowo['D'];
$type = $_GET['type_buy'];
$nama = $_SESSION['username'];
$settlement_id = $_SESSION['pk_id'];
$_SESSION['sql']='';
//print_r ($txt_hari);
if ($cd=='' && $txt_hari =='' ){
				$sql  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram,FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type,
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		

				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
			/*
				$sql  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram,FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type,
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		

				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
		*/
				
				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram, FORMAT( if(code like '%-W',total - komisi - pajak -levy - kpei-pajak_jual ,nilai_beli - pajak_komisi), 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
			
				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
				/*
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
			
				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,nilai_jual,nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
			*/
				$sql_jual = "
							SELECT CODE AS kd_jual, SUM(lembar) AS lembar_jual, if(code like '%-W',sum(total - komisi - pajak -levy - kpei-pajak_jual+pajak_komisi) ,SUM(nilai_jual) ) AS harga_jual, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.securitas_type AS securitas_jual
							FROM tbl_kr_dealer_tmp 
							LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
							WHERE DATE(create_dt)='$tanggal'
							AND tbl_kr_dealer_tmp.allocation = '$allocation'
							AND type_buy='Sell'
							GROUP BY securitas_jual, kd_jual
							ORDER BY create_dt DESC
				";
 /*
				$sql_jual = "
							SELECT CODE AS kd_jual, SUM(lembar) AS lembar_jual, SUM(nilai_jual) AS harga_jual, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.securitas_type AS securitas_jual
							FROM tbl_kr_dealer_tmp 
							LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
							WHERE DATE(create_dt)='$tanggal'
							AND tbl_kr_dealer_tmp.allocation = '$allocation'
							AND type_buy='Sell'
							GROUP BY securitas_jual, kd_jual
							ORDER BY create_dt DESC
				";
*/
				$sql_beli = "
							SELECT CODE AS kd_beli, SUM(lembar) AS lembar_beli, SUM(nilai_beli) AS harga_beli, DATE(create_dt) AS tgl_beli, tbl_kr_securitas.securitas_type AS securitas_beli
							FROM tbl_kr_dealer_tmp 
							LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
							WHERE DATE(create_dt)='$tanggal'
							AND tbl_kr_dealer_tmp.allocation = '$allocation'
							AND type_buy='Buy'
							GROUP BY securitas_beli, kd_beli
							ORDER BY create_dt DESC
				";

				}
				else{
					$sql  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total + komisi + pajak +levy + kpei,0) AS suram,FORMAT( nilai_jual - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type,
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
	
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";
			
				$sql2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,FORMAT(harga,0) AS harga,total, komisi,levy,kpei,pajak, pajak_jual,pajak_komisi,nilai_jual,nilai_beli,
				( total + komisi + pajak +levy + kpei) AS suram,
				tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				ORDER BY $order_by $sort_order";

				
				$sql3  = "SELECT sum(nilai_beli)as total, code,type_buy FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'BUY'
				GROUP BY code";

				//print($sql3);
				################################################################################################################################
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram, FORMAT( if(code like '%-W',total - komisi - pajak -levy - kpei-pajak_jual ,nilai_beli - pajak_komisi), 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
				/*
				$sqlsell  = "SELECT tbl_kr_dealer_tmp.*,LEFT( create_dt, 10 ) AS TGL,FORMAT( total - komisi - pajak -levy - kpei-pajak_jual,0) AS suram, FORMAT( nilai_beli - pajak_komisi, 0 ) AS suram2,FORMAT(lembar,0) AS lembar,FORMAT(harga,0) AS harga,FORMAT(total,0) as total,FORMAT(komisi,0) AS komisi,FORMAT(levy,0) AS levy,FORMAT(kpei,0) AS kpei,FORMAT(pajak,0) AS pajak,FORMAT(pajak_jual,0) AS pajak_jual,FORMAT(pajak_komisi,0) AS pajak_komisi,FORMAT(nilai_jual,0) AS nilai_jual,FORMAT(nilai_beli,0) AS nilai_beli,tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
				*/


				$sqlsell2  = "SELECT tbl_kr_dealer_tmp.*, allocation as AL, lembar,harga,total,komisi,levy,kpei, pajak, pajak_jual, pajak_komisi,nilai_jual,nilai_beli,
				( total - komisi - pajak -levy - kpei-pajak_jual) AS suram,
				tbl_kr_securitas.securitas_type, 
				tbl_kr_settle_date.fk_id,tbl_kr_settle_date.day,tbl_kr_settle_date.settle_date 
				FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
					AND tbl_kr_settle_date.day ='".$txt_hari."'
			
				AND allocation ='".$allocation."'
				AND type_buy = 'SELL'
				ORDER BY $order_by $sort_order";
				
				$sqlsell3  = "SELECT sum(nilai_jual) as total,code,type_buy FROM tbl_kr_dealer_tmp LEFT JOIN tbl_kr_securitas 
				ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
				LEFT JOIN tbl_kr_settle_date
				ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
				WHERE LEFT(create_dt,10) = '".$tanggal."'
				AND allocation ='".$allocation."'
				AND tbl_kr_settle_date.day ='".$txt_hari."'
				AND type_buy = 'SELL'
				GROUP BY code";
//print($sqlsell3);
				
				$sql_jual = "
							SELECT CODE AS kd_jual, SUM(lembar) AS lembar_jual, SUM(nilai_jual) AS harga_jual, DATE(create_dt) AS tgl_jual, tbl_kr_securitas.securitas_type AS securitas_jual
							FROM tbl_kr_dealer_tmp 
							LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
							LEFT JOIN tbl_kr_settle_date ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
							WHERE DATE(create_dt)='$tanggal'
							AND tbl_kr_dealer_tmp.allocation = '$allocation'
							AND type_buy='Sell'
							AND tbl_kr_settle_date.day ='".$txt_hari."'
							GROUP BY securitas_jual, kd_jual
							ORDER BY create_dt DESC
				";
				$sql_beli = "
							SELECT CODE AS kd_beli, SUM(lembar) AS lembar_beli, SUM(nilai_beli) AS harga_beli, DATE(create_dt) AS tgl_beli, tbl_kr_securitas.securitas_type AS securitas_beli
							FROM tbl_kr_dealer_tmp 
							LEFT JOIN tbl_kr_securitas ON tbl_kr_dealer_tmp.securitas_id = tbl_kr_securitas.securitas_id
							LEFT JOIN tbl_kr_settle_date ON tbl_kr_dealer_tmp.pk_id = tbl_kr_settle_date.fk_id
							WHERE DATE(create_dt)='$tanggal'
							AND tbl_kr_dealer_tmp.allocation = '$allocation'
							AND type_buy='Buy'
							AND tbl_kr_settle_date.day ='".$txt_hari."'
							GROUP BY securitas_beli, kd_beli
							ORDER BY create_dt DESC
				";
				}


//$share = "SELECT lembar FROM tbl_kr_mst_saham WHERE code='".$cd."'";
//print_r ($sql_jual);

$qry_beli=mysql_query($sql_beli);
$qry_jual=mysql_query($sql_jual);
$rst_beli=mysql_num_rows($qry_beli);
$rst_jual=mysql_num_rows($qry_jual);
$kd_jual='kd_jual';
$kd_beli='kd_beli';
$sc_jual='securitas_jual';
$sc_beli='securitas_beli';

if($rst_jual>=$rst_beli){
	$sqlx = "
			SELECT * FROM 
				(
					$sql_jual
				) a
				LEFT JOIN(
					$sql_beli
				) b ON a.$kd_jual=b.$kd_beli AND a.$sc_jual=b.$sc_beli
					WHERE a.$kd_jual=b.$kd_beli AND a.$sc_jual=b.$sc_beli
	";
} else {
	$sqlx = "
			SELECT * FROM 
				(
					$sql_beli
				) a
				LEFT JOIN(
					$sql_jual
				) b ON a.$kd_beli=b.$kd_jual AND a.$sc_beli=b.$sc_jual
					WHERE a.$kd_beli=b.$kd_jual AND a.$sc_beli=b.$sc_jual
	";
}
//print_r($sqlx);
//echo '<br>';
$DGSUM=$data->dataGridSettlementSum($sqlx,'pk_id',$data->ResultsPerPage,$pg,'');
$DG= $data->dataGridDeal($sql,'pk_id',$data->ResultsPerPage);
$DG2= $data->dataGridDeal($sqlsell,'pk_id',$data->ResultsPerPage);
//print_r($sqlsell);
$tmpl->addVar('page','SQL1',$sqlsell);
$rowLembar = $data->get_rows($sql2);
$rowLembar2 = $data->get_rows($sqlsell2);

$buy_l = 0;
$buy_t = 0;
for ($i=0;$i<count($rowLembar);$i++)
{

$buy_l = $buy_l + $rowLembar[$i][lembar];
$buy_t = $buy_t + $rowLembar[$i][nilai_beli];
$buy_levy += $rowLembar[$i][levy];
$buy_kpei += $rowLembar[$i][kpei];
$buy_pajak += $rowLembar[$i][pajak];
$buy_komisi += $rowLembar[$i][komisi];
$buy_pajak_komisi += $rowLembar[$i][pajak_komisi];
$buy_total += $rowLembar[$i][total];
$buy_pajak_jual += $rowLembar[$i][pajak_jual];
$buy_suram += $rowLembar[$i][suram];
}



$sell_l = 0;
$sell_t = 0;
for ($i=0;$i<count($rowLembar2);$i++)
{
$sell_l = $sell_l + $rowLembar2[$i][lembar];
$sell_t = $sell_t + $rowLembar2[$i][nilai_jual];
 $sell_total += $rowLembar2[$i][total];
 $sell_komisi += $rowLembar2[$i][komisi];
$sell_pajak += $rowLembar2[$i][pajak];
$sell_levy += $rowLembar2[$i][levy];
$sell_kpei += $rowLembar2[$i][kpei];
$sell_pajak_jual += $rowLembar2[$i][pajak_jual];
$sell_pajak_komisi += $rowLembar2[$i][pajak_komisi];
$sell_suram += $rowLembar2[$i][suram];


}

if($buy_t > $sell_t){
$total_suram==$buy_suram-$sell_suram;
$total_pajak_komisi=$buy_pajak_komisi-$sell_pajak_komisi;
$total_pajak_jual=$buy_pajak_jual-$sell_pajak_jual;
$total_kpei=$buy_kpei-$sell_kpei;
$total_levy=$buy_levy-$sell_levy;
$total_pajak=$buy_pajak-$sell_pajak;
$total_komisi=$buy_komisi-$sell_komisi;
$total_total=$buy_total-$sell_total;
$total_t=$buy_t-$sell_t;
$total_l=$buy_l-$sell_l;

}else{
$total_suram==$sell_suram-$buy_suram;
$total_pajak_komisi=$sell_pajak_komisi-$buy_pajak_komisi;
$total_pajak_jual=$sell_pajak_jual-$buy_pajak_jual;
$total_kpei=$sell_kpei-$buy_kpei;
$total_levy=$sell_levy-$buy_levy;
$total_pajak=$sell_pajak-$buy_pajak;
$total_komisi=$sell_komisi-$buy_komisi;
$total_total=$sell_total-$buy_total;
$total_t=$sell_t-$buy_t;
$total_l=$sell_l-$buy_l;
}



$total_t = number_format($total_t,0);
$total_l = number_format($total_l,0);

$buy_l = number_format($buy_l,0);
$buy_t = number_format($buy_t,0);

$sell_l = number_format($sell_l,0);
$sell_t = number_format($sell_t,0);


######################## NETT #######################################
$nett_buy = $data->get_rows($sql3);
$nett_sell = $data->get_rows($sqlsell3);
$detail.="<table width='100%' cellpadding='0' cellspacing='0' border='0' style='font-size:11px;'>";

for ($b=0;$b<count($nett_buy);$b++)
{
	
	for ($s=0;$s<count($nett_sell);$s++)
	{
	$DG3[$s] = $nett_sell[$s];
			if ($nett_buy[$b][code]== $nett_sell[$s][code])
			{
			$nett_total = $nett_buy[$b][total]-$nett_sell[$s][total];
				if ($nett_total < 0){
				$type = "SELL";
				$nett_total = $nett_total *-1;
				}
				else
				{
				$type ="BUY";
				$nett_total = $nett_total;
				}
				$nett_total = number_format($nett_total,0);
			$detail.="<tr>";
			$detail.="<td align='left' width=94%><b>NETTING ".$type." ".$nett_sell[$s][code]."</td>";
			$detail.="<td align='left'><b>".$nett_total."</b></td>";
			
			$detail.="</tr>";
			}
		
	}
	
}
$detail.= "</table>";


#$gbr=$_SERVER['DOCUMENT_ROOT'].dirname($_SERVER['PHP_SELF']).'/image/logoHead.jpg';
$gbr="http://10.5.1.74:88/keraton/image/logoHead.jpg";

for ($i=0;$i<count($rowLembar);$i++)
{

}

###################################################################
$tmpl->addVar('page','nama',$nama);
$tmpl->addVar('page','up',$up);
$tmpl->addVar('page','bank',$bank);
$tmpl->addVar('page','c',$c);
$tmpl->addVar('page','d',$d);
$tmpl->addVar('page','detail',$detail);
$tmpl->addVar('page','date',$tanggal);
$tmpl->addVar('page','code',$cd);
$tmpl->addVar('page','gbr',$gbr);

$tmpl->addVar('page','type_buy',$type);
$tmpl->addVar('page','tottrans',$buy_l);
$tmpl->addVar('page','totnilai',$buy_t);
$tmpl->addVar('page','totlevy_buy',number_format($buy_levy,0));
$tmpl->addVar('page','totkpei_buy',number_format($buy_kpei,0));
$tmpl->addVar('page','totpajak_buy',number_format($buy_pajak,0));
$tmpl->addVar('page','totpajak_komisi_buy',number_format($buy_pajak_komisi,0));
$tmpl->addVar('page','tottotal_buy',number_format($buy_total,0));
$tmpl->addVar('page','totkomisi_buy',number_format($buy_komisi,0));
$tmpl->addVar('page','totpajak_jual_buy',number_format($buy_pajak_jual,0));
$tmpl->addVar('page','totsuram_buy',number_format($buy_suram,0));
$tmpl->addVar('page','tottrans2',$sell_l);
$tmpl->addVar('page','totnilai2',$sell_t);
$tmpl->addVar('page','totsell_total',number_format($sell_total,0));
$tmpl->addVar('page','totsell_komisi',number_format($sell_komisi,0));
$tmpl->addVar('page','totsell_pajak',number_format($sell_pajak,0));
$tmpl->addVar('page','totsell_levy',number_format($sell_levy,0));
$tmpl->addVar('page','totsell_kpei',number_format($sell_kpei,0));
$tmpl->addVar('page','totsell_pajak_jual',number_format($sell_pajak_jual,0));
$tmpl->addVar('page','totsell_pajak_komisi',number_format($sell_pajak_komisi,0));
$tmpl->addVar('page','totsell_suram',number_format($sell_suram,0));
$tmpl->addVar('page','tottrans3',$total_l);
$tmpl->addVar('page','totnilai3',$total_t);
$tmpl->addVar('page','totsell_total3',number_format($total_total,0));
$tmpl->addVar('page','totsell_komisi3',number_format($total_komisi,0));
$tmpl->addVar('page','totsell_pajak3',number_format($total_pajak,0));
$tmpl->addVar('page','totsell_levy3',number_format($total_levy,0));
$tmpl->addVar('page','totsell_kpei3',number_format($total_kpei,0));
$tmpl->addVar('page','totsell_pajak_jual3',number_format($total_pajak_jual,0));
$tmpl->addVar('page','totsell_pajak_komisi3',number_format($total_pajak_komisi,0));
$tmpl->addVar('page','totsell_suram3',number_format($total_suram,0));
$tmpl->addRows('loopData',$DG);
$tmpl->addRows('loopData2',$DG2);
$tmpl->addRows('loopData3',$DGSUM);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->displayParsedTemplate('page');
?>