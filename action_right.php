<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('action_right_add.html');



if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));		
		$txt_code = strtoupper(trim(htmlentities($_POST['txt_code']))); 		
		$txt_lembar = trim(htmlentities($_POST['txt_lembar']));
		//$txt_harga_prev = trim(htmlentities($_POST['txt_harga_prev']));
        //$txt_harga_close = trim(htmlentities($_POST['txt_harga_close']));
        $total =0;		
		if($txt_allocation==''){
			echo "<script>alert('Allocation is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_code==''){
			echo "<script>alert('Code is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if(  substr(strtoupper(trim($txt_code)), -2)!="-R" )
		{
			echo "<script>alert('Invalid code! code should be end with -R');</script>";
			throw new Exception($data->err_report('s02'));
		}
		/*
		if($txt_harga_prev==''){
			echo "<script>alert('Harga Prev is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_harga_close==''){
			echo "<script>alert('Harga Close is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$total = $txt_harga_close * $txt_lembar;
		*/
		
		/*
		$sql = "INSERT INTO tbl_kr_action_nontrans (
			code,
			code_to,
			lembar,
			harga_prev,
			harga_close,
			allocation,
			create_dt,
			create_dt2
		)VALUES(
			'$txt_code',
			'$txt_code_to',
			'$txt_lembar',	
			'$txt_harga_prev',
			'$txt_harga_close',
			'$txt_allocation',
			'$txt_tgl',
			now()
			)";
			#print_r($sql);
		*/	
		$sql2 ="INSERT INTO tbl_kr_mst_saham(
			code,
			lembar,
			harga_prev,
			harga_close,
			allocation,
			total,
			create_dt
			)VALUES(
			'$txt_code',
			'$txt_lembar',
			0,
			0,
			'$txt_allocation',
			'$total',
			date(now())
			)";
		/*	
		$sql3 ="INSERT INTO tbl_kr_report_saham(
			code,
			lembar,
			harga_prev,
			harga_close,
			allocation,
			total,
			create_dt
			)VALUES(
			'$txt_code',
			'$txt_lembar',
			'$txt_harga_prev',
			'$txt_harga_close',
			'$txt_allocation',
			'$total',
			'$txt_tgl'
			)";
		*/	
		/*
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		*/
		if (!$data->inpQueryReturnBool($sql2)){
			throw new Exception($data->err_report('s02'));
		}
		/*
		if (!$data->inpQueryReturnBool($sql3)){
			throw new Exception($data->err_report('s02'));
		}
		*/
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."')</script>";
		unset($_POST[txt_code]);
		unset($_POST[txt_lembar]);
		unset($_POST[txt_harga_prev]);
		unset($_POST[txt_harga_close]);
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

$dataRows = array (
		'TEXT' =>  array(
						'Allocation',						
						'Stock Code',
						'Number of Shares',
						
						),
		'DOT'  => array (':',':',':'),
		'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
		"<input type=text size=20 maxlength='9' placeholder='ex: TLKM-R' style='text-transform:uppercase' name=txt_code value='".$_POST[txt_code]."' >",		
		"<input type=text size=20 maxlength='12' name=txt_lembar value='".$_POST[txt_lembar]."' >"
			
	)
		);
	$tittle = "RIGHT ISSUE";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='cash_repo.php';\">"
	);


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>