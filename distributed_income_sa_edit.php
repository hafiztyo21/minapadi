<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('distributed_income_sa_edit.html');

$id = 0;
$fundCode = '';
$investorAcNo = '';
$option	= '1';
$errorArr = array('','','');
$otherError='';

if($_GET['edit']==1){
    $id = $_GET['id'];
    $query = "SELECT * FROM tbl_kr_dist_inc_sa WHERE dist_inc_sa_id = $id";
    $result = $data->get_row($query);
    $fundCode = $result['fund_code'];
    $investorAcNo = $result['investor_ac_no'];
    $option = $result['dist_inc_opt'];
}

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
        $id = trim(htmlentities($_POST['inputId']));
 		$fundCode = trim(htmlentities($_POST['inputFundCode']));
		$investorAcNo = trim(htmlentities($_POST['inputInvestorAcNo']));
		$option = trim(htmlentities($_POST['inputOption']));
		
		$gotError = false;
		if($fundCode==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}
        if($investorAcNo==''){
			$errorArr[1] = "Investor Fund Unit A/C No must be filled";
			$gotError = true;
		}
		
        
		if (!$gotError){
			
			$query = "UPDATE tbl_kr_dist_inc_sa SET
					fund_code = '$fundCode',
					investor_ac_no = '$investorAcNo',
					dist_inc_opt = '$option',
					last_updated_time = now(),
					last_updated_by = '".$_SESSION['pk_id']."'
				WHERE dist_inc_sa_id = '$id'";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Edit Success');window.location='distributed_income_sa.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}



$tittle = "EDIT - DISTRIBUTED INCOME OPTION";
$dataRows = array (
	    'TEXT' => array('Fund Code <span class="redstar">*</span>','Investor Fund Unit A/C No <span class="redstar">*</span>','Distributed Income OPtion <span class="redstar">*</span>'),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            "<input type=hidden name=inputId value='$id'> ".$data->cb_fundcode('inputFundCode', $fundCode, ''),
		    //"<input type=text size='50' name=inputInvestorAcNo value='$investorAcNo'>",
			$data->cb_ifua('inputInvestorAcNo', $investorAcNo, ''),
		    "<select name=inputOption><option value=1 ". ($option == 1 ? "selected=selected" : "") .">Cash</option><option value=2 ".($option == 2 ? "selected=selected" : "").">Reinvesment</option>"
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='distributed_income_sa.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');

?>