<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('contact_add_institusi.html');
$tablename = 'tbl_kr_cus_institusi';
$tablenameDetail = 'tbl_kr_cus_institusi_detail';

if ($_POST['btn_save']){
	#print_r($_POST);
	$name = trim(htmlentities($_POST['txt_cus_name']));
	if ($name!=''){
		#$country = explode(';',$_POST['txt_country']);

		$countrydomisili = trim(htmlentities($_POST['country_of_domicily']));
		if($countrydomisili == ''){
			$errorMessage .= "COUNTRY OF DOMICILE must be filled\\n";
			$gotError = true;
		}
		$place = trim(htmlentities($_POST['txt_tempat_pt']));
		if($place == ''){
			$errorMessage .= "TEMPAT must be filled\\n";
			$gotError = true;
		}
		$address = trim(htmlentities($_POST['txt_alamat_pt']));
		if($address == ''){
			$errorMessage .= "ALAMAT LENGKAP must be filled\\n";
			$gotError = true;
		}
		$beneficalowner=trim(htmlentities($_POST['benefical_owner']));
		if($beneficalowner=='') {
			$errorMessage .= "Benefical Owner must be filled\\n";
				$gotError = true;
		}

		$npwp = trim(htmlentities($_POST['txt_no_npwp1'])).''.trim(htmlentities($_POST['txt_no_npwp2'])).''.trim(htmlentities($_POST['txt_no_npwp3'])).''.trim(htmlentities($_POST['txt_no_npwp4'])).''.trim(htmlentities($_POST['txt_no_npwp5'])).''.trim(htmlentities($_POST['txt_no_npwp6']));
		/*
		if($npwp == ''){
			$errorMessage .= "No. NPWP must be filled\\n";
			$gotError = true;
		}
		*/
		$sql = "INSERT INTO $tablename(
		cus_code,
		cus_name,
		cus_tempat_pt,
		cus_tgl_pt,
		cus_pt,
		cus_struktur,
		persen_struktur,
		
		cus_struktur_2,
		persen_struktur_2,
		cus_bidang_usaha,
		cus_alamat_pt,
		cus_kota_pt,
		cus_kode_pos_pt,
		cus_propinsi_pt,
		cus_telp_pt,
		cus_fax_pt,
		cus_email_pt,
		cus_status_domisili_pt,		
		cus_lama,
		cus_actived,
		
		cus_no_npwp,
		cus_no_tdp,
		cus_tgl_tdp,
		cus_tempat_tdp,
		cus_no_siup,
		cus_tgl_siup,
		cus_tempat_siup,
		cus_no_pma,
		cus_tgl_pma,
		cus_tempat_pma,
		
		cus_pemegang_saham,
		cus_pemegang_saham_ktp,
		cus_pemegang_saham_milik,
		
		cus_pemegang_saham_2,
		cus_pemegang_saham_ktp_2,
		cus_pemegang_saham_milik_2,
		
		cus_pemegang_saham_3,
		cus_pemegang_saham_ktp_3,
		cus_pemegang_saham_milik_3,
		
		cus_direksi,
		cus_direksi_ktp,
		cus_direksi_tgl,
		cus_direksi_jbt,
		
		cus_direksi_2,
		cus_direksi_ktp_2,
		cus_direksi_tgl_2,
		cus_direksi_jbt_2,
		
		cus_direksi_3,
		cus_direksi_ktp_3,
		cus_direksi_tgl_3,
		cus_direksi_jbt_3,
		
		cus_komisaris,
		cus_komisaris_ktp,
		cus_komisaris_tgl,
		cus_komisaris_jbt,
		
		cus_komisaris_2,
		cus_komisaris_ktp_2,
		cus_komisaris_tgl_2,
		cus_komisaris_jbt_2,
		
		cus_komisaris_3,
		cus_komisaris_ktp_3,
		cus_komisaris_tgl_3,
		cus_komisaris_jbt_3,
		
		cus_modal_saham,
		cus_total_aset,
		cus_tot_wajib,
		cus_laba_bersih,
		cus_pndp_opera,
		cus_pndp_non_opera,
		cus_inves,
		
		cus_bank_cab,
		cus_pemilik_rek,
		cus_jenis_rek,
		cus_no_rek,
		
		cus_bank_cab_2,
		cus_pemilik_rek_2,
		cus_jenis_rek_2,
		cus_no_rek_2,
		
		cus_nama_,
		cus_jabatan,
		cus_telp_hp,
		cus_no_KTP,
		
		cus_nama_2,
		cus_jabatan_2,
		cus_telp_hp_2,
		cus_no_KTP_2,
		
		
		cus_domisili,
		cus_tipe,
		cus_karakteristik,
		cus_sumber_dana,
		cus_penghasilan,
		cus_no_skd,
		cus_ins_sid,
		country_of_domicily,
		skd_expiration_date,
		country_of_establisment,
		articles_of_association_no,
		investors_riks_profile,
		asset_owner,
		country_of_company,
		statement_type,
		benefical_owner,
		authorized_person_middle_name1,
		authorized_person_last_name1,
		authorized_person_email1,
		authorized_person_npwp1,
		authorized_person_ktp_expiration_date1,
		authorized_person_passport1,
		authorized_person_passport_expiration_date1,
		authorized_person_middle_name2,
		authorized_person_last_name2,
		authorized_person_email2,
		authorized_person_npwp2,
		authorized_person_ktp_expiration_date2,
		authorized_person_passport2,
		authorized_person_passport_expiration_date2,
		asset_information_last_year,
		asset_information_2_years_ago,
		asset_information_3_years_ago,
		profit_information_last_year,
		profit_information_2_years_ago,
		profit_information_3_years_ago,
		fatca,
		tin,
		tin_issuance_country,
		giin,
		substantial_us_owner_name,
		substantial_us_owner_address,
		substantial_us_owner_tin,
		bank_bic_code1,
		bank_bi_member_code1,
		bank_name1,
		bank_country1,
		bank_branch1,
		acc_ccy1,
		acc_no1,
		acc_name1,
		bank_bic_code2,
		bank_bi_member_code2,
		bank_name2,
		bank_country2,
		bank_branch2,
		acc_ccy2,
		acc_no2,
		acc_name2,
		bank_bic_code3,
		bank_bi_member_code3,
		bank_name3,
		bank_country3,
		bank_branch3,
		acc_ccy3,
		acc_no3,
		acc_name3,
		acc_type3,
		npwp_registration_date,
		client_code,
		created_date
		)VALUES(
		'".$_POST['txt_cus_id']."',
		'".$_POST['txt_cus_name']."',
		'".$_POST['txt_tempat_pt']."',
		'".$_POST['txt_tgl_pt']."',
		'".$_POST['txt_cus_pt']."',
		'".$_POST['txt_struktur']."',
		'".$_POST['txt_persen_struktur']."',
		
		'".$_POST['txt_struktur_2']."',
		'".$_POST['txt_persen_struktur_2']."',
		'".$_POST['txt_bidang_usaha']."',
		'".$_POST['txt_alamat_pt']."',
		'".$_POST['txt_kota_pt']."',
		'".$_POST['txt_kode_pos_pt']."',
		'".$_POST['txt_propinsi_pt']."',
		'".$_POST['txt_telp_pt']."',
		'".$_POST['txt_fax_pt']."',		
		'".$_POST['txt_email_pt']."',
		'".$_POST['txt_status_domisili_pt']."',
		'".$_POST['txt_lama_pt']."',
		'".$_POST['txt_cus_actived']."',
		
		'".$npwp."',
		'".$_POST['txt_no_tdp']."',
		
		'".$_POST['txt_tgl_tdp']."',
		'".$_POST['txt_tempat_tdp']."',
		'".$_POST['txt_no_siup']."',
		'".$_POST['txt_tgl_siup']."',
		'".$_POST['txt_tempat_siup']."',
		
		'".$_POST['txt_no_pma']."',
		'".$_POST['txt_tgl_pma']."',	
		'".$_POST['txt_tempat_pma']."',	
		
		'".$_POST['txt_pemegang_saham']."',
		'".$_POST['txt_pemegang_saham_ktp']."',
		'".$_POST['txt_pemegang_saham_milik']."',
		
		'".$_POST['txt_pemegang_saham_2']."',
		'".$_POST['txt_pemegang_saham_ktp_2']."',
		'".$_POST['txt_pemegang_saham_milik_2']."',
		
		'".$_POST['txt_pemegang_saham_3']."',
		'".$_POST['txt_pemegang_saham_ktp_3']."',
		'".$_POST['txt_pemegang_saham_milik_3']."',
		
		'".$_POST['txt_direksi']."',
		'".$_POST['txt_direksi_ktp']."',
		'".$_POST['txt_direksi_tgl']."',
		'".$_POST['txt_direksi_jbt']."',
		
		'".$_POST['txt_direksi_2']."',
		'".$_POST['txt_direksi_ktp_2']."',
		'".$_POST['txt_direksi_tgl_2']."',
		'".$_POST['txt_direksi_jbt_2']."',
		
		'".$_POST['txt_direksi_3']."',
		'".$_POST['txt_direksi_ktp_3']."',
		'".$_POST['txt_direksi_tgl_3']."',
		'".$_POST['txt_direksi_jbt_3']."',
		
		'".$_POST['txt_komisaris']."',
		'".$_POST['txt_komisaris_ktp']."',
		'".$_POST['txt_komisaris_tgl']."',
		'".$_POST['txt_komisaris_jbt']."',
				
		'".$_POST['txt_komisaris_2']."',
		'".$_POST['txt_komisaris_ktp_2']."',
		'".$_POST['txt_komisaris_tgl_2']."',
		'".$_POST['txt_komisaris_jbt_2']."',
				
		'".$_POST['txt_komisaris_3']."',
		'".$_POST['txt_komisaris_ktp_3']."',
		'".$_POST['txt_komisaris_tgl_3']."',
		'".$_POST['txt_komisaris_jbt_3']."',
		
		
		'".$_POST['txt_modal_saham']."',
		'".$_POST['txt_total_aset']."',
		'".$_POST['txt_tot_wajib']."',
		'".$_POST['txt_laba_bersih']."',
		'".$_POST['txt_pndp_opera']."',
		'".$_POST['txt_pndp_non_opera']."',
		'".$_POST['txt_inves']."',
		
		'".$_POST['txt_bank_cab']."',
		'".$_POST['txt_pemilik_rek']."',	
		'".$_POST['txt_jenis_rek']."',
		'".$_POST['txt_no_rek']."',	
		
		'".$_POST['txt_bank_cab_2']."',
		'".$_POST['txt_pemilik_rek_2']."',	
		'".$_POST['txt_jenis_rek_2']."',
		'".$_POST['txt_no_rek_2']."',			
		
		'".$_POST['txt_nama_']."',
		'".$_POST['txt_jabatan']."',
		'".$_POST['txt_telp_hp']."',
		'".$_POST['txt_no_ktp']."',
		
		'".$_POST['txt_nama_2']."',
		'".$_POST['txt_jabatan_2']."',
		'".$_POST['txt_telp_hp_2']."',
		'".$_POST['txt_no_ktp_2']."',
	
				
		'".$_POST['txt_cus_domisili']."',
		'".$_POST['txt_cus_tipe']."',
		'".$_POST['txt_cus_karakteristik']."',
		'".$_POST['txt_cus_sumber_dana']."',
		'".$_POST['txt_cus_penghasilan']."',
		'".$_POST['cus_no_skd']."',
		'".$_POST['cus_ins_sid']."',
		'".$_POST['country_of_domicily']."',
		'".$_POST['skd_expiration_date']."',
		'".$_POST['country_of_establisment']."',
		'".$_POST['articles_of_association_no']."',
		'".$_POST['investors_riks_profile']."',
		'".$_POST['asset_owner']."',
		'".$_POST['country_of_company']."',
		'".$_POST['statement_type']."',
		'".$beneficalowner."',
		'".$_POST['authorized_person_middle_name1']."',
		'".$_POST['authorized_person_last_name1']."',
		'".$_POST['authorized_person_email1']."',
		'".$_POST['authorized_person_npwp1']."',
		'".$_POST['authorized_person_ktp_expiration_date1']."',
		'".$_POST['authorized_person_passport1']."',
		'".$_POST['authorized_person_passport_expiration_date1']."',
		'".$_POST['authorized_person_middle_name2']."',
		'".$_POST['authorized_person_last_name2']."',
		'".$_POST['authorized_person_email2']."',
		'".$_POST['authorized_person_npwp2']."',
		'".$_POST['authorized_person_ktp_expiration_date2']."',
		'".$_POST['authorized_person_passport2']."',
		'".$_POST['authorized_person_passport_expiration_date2']."',
		'".$_POST['asset_information_last_year']."',
		'".$_POST['asset_information_2_years_ago']."',
		'".$_POST['asset_information_3_years_ago']."',
		'".$_POST['profit_information_last_year']."',
		'".$_POST['profit_information_2_years_ago']."',
		'".$_POST['profit_information_3_years_ago']."',
		'".$_POST['fatca']."',
		'".$_POST['tin']."',
		'".$_POST['tin_issuance_country']."',
		'".$_POST['giin']."',
		'".$_POST['substantial_us_owner_name']."',
		'".$_POST['substantial_us_owner_address']."',
		'".$_POST['substantial_us_owner_tin']."',
		'".$_POST['bank_bic_code1']."',
		'".$_POST['bank_bi_member_code1']."',
		'".$_POST['txt_bank_cab']."',
		'".$_POST['bank_country1']."',
		'".$_POST['bank_branch1']."',
		'".$_POST['acc_ccy1']."',
		'".$_POST['txt_no_rek']."',
		'".$_POST['txt_pemilik_rek']."',
		'".$_POST['bank_bic_code2']."',
		'".$_POST['bank_bi_member_code2']."',
		'".$_POST['txt_bank_cab_2']."',
		'".$_POST['bank_country2']."',
		'".$_POST['bank_branch2']."',
		'".$_POST['acc_ccy2']."',
		'".$_POST['txt_no_rek_2']."',
		'".$_POST['txt_pemilik_rek_2']."',
		'".$_POST['bank_bic_code3']."',
		'".$_POST['bank_bi_member_code3']."',
		'".$_POST['bank_name3']."',
		'".$_POST['bank_country3']."',
		'".$_POST['bank_branch3']."',
		'".$_POST['acc_ccy3']."',
		'".$_POST['acc_no3']."',
		'".$_POST['acc_name3']."',
		'".$_POST['acc_type3']."',
		'".$_POST['npwp_registration_date']."',
		'".$_POST['client_code']."',
		now()
		)";
		//print_r($sql);
	}else{
		echo "<script>alert('Name is Empty!');</script>";
	};
   #$data->showsql($sql);
   #$id = $data->get_value("SELECT cus_code from tbl_kr_cus_institusi where pk_id ='".$_POST['txt_cus_id']."'");
   #print_r($id);
   if($gotError){
		   echo "<script>alert('".$errorMessage."Save Failed.');</script>";
	   }else{
			if ($data->inpQueryReturnBool($sql))
				{	echo "<script>alert('".$data->err_report('s01')."');opener.document.form1.submit();window.parent.close();</script>";	}
				else
				{	
					//echo $data->queryError();
					echo "<script>alert('".$data->err_report('s02')."');</script>";	}
	   }
	
}
#ADD BARU
if ($_GET['add'] == 1){
	#$data->auth('09030101',$_SESSION['user_id']);
	#$value_kode=$data->get_value("select max(kode_akses) from tbl_upx_akses");
	#$new_id = $value_kode+1;
	#print_r($_POST);
	$date = $_POST[txt_tgl_pt];
	$pecah1 = explode("-", $date);
	$date1 = $pecah1[2];
	$month = $pecah1[1];
	$year = $pecah1[0];
	$name = $_POST['txt_cus_name'];
	$pecah_name = substr($name, 0,1);
	$pecah_year = substr($year,2,2);
	$id = "$pecah_name$pecah_year$month ";
	//print($_POST[txt_tgl_berlaku]);
	
	$cek_data=$data->get_value("select count(cus_id) from tbl_kr_cus_sup where left(cus_code,5) ='".$id."'");
	$cek_data = $cek_data + 1;
	if ($cek_data < 10 ){
	
	$no_auto= "0$cek_data";
		//print($no_auto);
	}
	else
	{
	$no_auto=$cek_data;

	}
	
$id2 = "$pecah_name$pecah_year$month$no_auto";
//print($id2);
	$dataRows = array (
				'TEXT' => array(
					'ID',
					'NAMA INSTITUSI <span class="redstar">*</span>',
					'TEMPAT <span class="redstar">*</span>',
					'NEGARA',
					'TGL. PENDIRIAN <span class="redstar">*</span>',
					'JENIS INSTITUSI',
					'STRUKTUR KEPIMILIKAN SAHAM',
					'BIDANG USAHA',
					'ALAMAT LENGKAP <span class="redstar">*</span>',
					'',
					'No. TELP. PERUSAHAAN',
					'EMAIL PERUSAHAAN',
					'NEGARA PERUSAHAAN',
					'STATUS DOMISILI KANTOR',
					'LAMA MENEMPATI',
					'ACTIVE',
					'STATEMENT TYPE <span class="redstar">*</span>',
					'BENEFICAL OWNER <span class="redstar">*</span>'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				"<input type=text name=txt_cus_id id=txt_cus_id ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_cus_name id=txt_cus_name  value='".$_POST['txt_cus_name']."'><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_tempat_pt id=txt_tempat_pt value='".$_POST['txt_tempat_pt']."'><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				$data->cb_isocountry('country_of_establisment', $_POST[country_of_establisment] ,'')."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->datePicker('txt_tgl_pt', $_POST[txt_tgl_pt],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_institusi('txt_cus_pt',$_POST[txt_cus_pt])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_shm('txt_struktur',$_POST[txt_struktur])."<br>
				".$data->cb_shm('txt_struktur_2',$_POST[txt_struktur_2])."<td bgcolor='#DAECFF' width='200' class='mandatory'>
				Persen Kepemilikan Saham :
				<input type=text name=txt_persen_struktur id=txt_persen_struktur value='".$_POST['txt_persen_struktur']."'>%<br>
				Persen Kepemilikan Saham :
				<input type=text name=txt_persen_struktur_2 id=txt_persen_struktur_2 value='".$_POST['txt_persen_struktur_2']."'>%</td>",
				"<input type=text name=txt_bidang_usaha id=txt_bidang_usaha ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_alamat_pt id=txt_alamat_pt size=50 value='".$_POST['txt_alamat_pt']."'><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"KOTA"
				// <input type=text name=txt_kota_pt id=txt_kota_pt>&nbsp;&nbsp;
				 .$data->cb_kota('txt_kota_pt',$_POST[txt_kota_pt])."
				 </br>KODE POS
				 <input type=text name=txt_kode_pos_pt id=txt_kode_pos_pt>&nbsp;&nbsp;
				 </br>PROPINSI
				 <input type=text name=txt_propinsi_pt id=txt_propinsi_pt><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_telp_pt id=txt_telp_pt >&nbsp;&nbsp;NO.FAX<input type=text name=txt_fax_pt id=txt_fax_pt ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_email_pt id=txt_email_pt ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_isocountry('country_of_company',$_POST[country_of_company])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_domi('txt_status_domisili_pt',$_POST[txt_status_domisili_pt])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_lama_pt id=txt_lama_pt ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_customer_status('txt_cus_actived',$_POST[txt_cus_actived])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_statementtype('statement_type',$_POST[statement_type])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=benefical_owner id=benefical_owner value='".$_POST['benefical_owner']."''><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				));
				
			$dataRows2 = array (
				'TEXT' => array(
				'No. NPWP <span class="redstar">*</span>',
				'NPWP REGISTRATION DATE',
				'No. TDP',
				'TANGGAL KADARLUARSA TDP',
				'No. SIUPP/IJIN USAHA',
				'TANGGAL KADALUARSA SIUPP',
				'N. IJIN PMA',
				'TANGGAL KADALUARSA PMA',
				),

				'DOT'  => array (':',':',':',':',':',':',':'),
				'FIELD' => array (
				
				//"<input type=text name=txt_no_npwp id=txt_no_npwp value='".$_POST['txt_no_npwp']."'><td bgcolor='#DAECFF' width='200' class='mandatory'>",
				"<input type=text name=txt_no_npwp1 id=txt_no_npwp1 size='2' onKeyPress='return checkIt(event)' maxlength='2' onKeyUp='valid_length1()' value='".$_POST['txt_no_npwp1']."'>.
				<input type=text name=txt_no_npwp2 id=txt_no_npwp2 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length2()' value='".$_POST['txt_no_npwp2']."'>.
				<input type=text name=txt_no_npwp3 id=txt_no_npwp3 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length3()' value='".$_POST['txt_no_npwp3']."'>.
				<input type=text name=txt_no_npwp4 id=txt_no_npwp4 size='2'  onKeyPress='return checkIt(event)' maxlength='1' onKeyUp='valid_length4()' value='".$_POST['txt_no_npwp4']."'>.
				<input type=text name=txt_no_npwp5 id=txt_no_npwp5 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length5()' value='".$_POST['txt_no_npwp5']."'>.
				<input type=text name=txt_no_npwp6 id=txt_no_npwp6 size='5'  onKeyPress='return checkIt(event)' maxlength='3' value='".$_POST['txt_no_npwp6']."'>",
				"".$data->datePicker('npwp_registration_date', $_POST[npwp_registration_date],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_no_tdp id=txt_no_tdp value='".$_POST['txt_no_tdp']."'><td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				
				"".$data->datePicker('txt_tgl_tdp', $_POST[txt_tgl_tdp],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'>Diterbitkan di :<input type=text name=txt_tempat_tdp id=txt_tempat_tdp value='".$_POST['txt_tempat_tdp']."'</td>",
				
				"<input type=text name=txt_no_siup id=txt_no_siup value='".$_POST['txt_no_siup']."'><td bgcolor='#DAECFF' width='200' class='mandatory'>",
				
				"".$data->datePicker('txt_tgl_siup', $_POST[txt_tgl_siup],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'>Diterbitkan di :<input type=text name=txt_tempat_siup id=txt_tempat_siup value='".$_POST['txt_tempat_siup']."'</td>",
	
				
				"<input type=text name=txt_no_pma id=txt_no_pma value='".$_POST['txt_no_pma']."'><td bgcolor='#DAECFF' width='200' class='mandatory'>",
				"".$data->datePicker('txt_tgl_pma', $_POST[txt_tgl_pma],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'>Diterbitkan di :<input type=text name=txt_tempat_pma id=txt_tempat_pma value='".$_POST['txt_tempat_pma']."'</td>",
				),

          	 );		  
					  
			$dataRows3 = array (
				'TEXT' => array(
					'PEMEGANG SAHAM',
					'SUSUNAN DIREKSI/PENGURUS',
					'SUSUNAN KOMISARIS/PENGURUS',
				),
				'TEXT2' => array(
					'NO KTP',
					'NO KTP',
					'NO KTP',
				),
				'TEXT3' => array(
					'JUMLAH KEPEMILIKAN',
					'TANGGAL LAHIR',
					'TANGGAL LAHIR',
				),
				'TEXT4' => array(
					'',
					'JABATAN',
					'JABATAN',
				),
				'DOT'  => array (':',':',':'),
				'FIELD' => array (
					"1 :&nbsp;<input type=text name=txt_pemegang_saham id=txt_pemegang_saham value='".$_POST['txt_pemegang_saham']."'></br>
					2 :&nbsp;<input type=text name=txt_pemegang_saham_2 id=txt_pemegang_saham_2 value='".$_POST['txt_pemegang_saham_2']."'></br>
					3 :&nbsp;<input type=text name=txt_pemegang_saham_3 id=txt_pemegang_saham_3 value='".$_POST['txt_pemegang_saham_3']."'>",
					
					"1 :&nbsp;<input type=text name=txt_direksi id=txt_direksi value='".$_POST['txt_direksi']."'></br>
					2 :&nbsp;<input type=text name=txt_direksi_2 id=txt_direksi_2 value='".$_POST['txt_direksi_2']."'></br>
					3 :&nbsp;<input type=text name=txt_direksi_3 id=txt_direksi_3 value='".$_POST['txt_direksi_3']."'>",
					
					"1 :&nbsp;<input type=text name=txt_komisaris id=txt_komisaris value='".$_POST['txt_komisaris']."'></br>
					2 :&nbsp;<input type=text name=txt_komisaris_2 id=txt_komisaris_2 value='".$_POST['txt_komisaris_2']."'></br>
					3 :&nbsp;<input type=text name=txt_komisaris_3 id=txt_komisaris_3 value='".$_POST['txt_komisaris_3']."'>",
				),
				'FIELD2' => array (
					"<input type=text name=txt_pemegang_saham_ktp id=txt_pemegang_saham_ktp value='".$_POST['txt_pemegang_saham_ktp']."'></br>
					<input type=text name=txt_pemegang_saham_ktp_2 id=txt_pemegang_saham_ktp_2 value='".$_POST['txt_pemegang_saham_ktp_2']."'></br>
					<input type=text name=txt_pemegang_saham_ktp_3 id=txt_pemegang_saham_ktp_3 value='".$_POST['txt_pemegang_saham_ktp_3']."'>",
					
					"<input type=text name=txt_direksi_ktp id=txt_direksi_ktp value='".$_POST['txt_direksi_ktp']."'></br>
					<input type=text name=txt_direksi_ktp_2 id=txt_direksi_ktp_2 value='".$_POST['txt_direksi_ktp_2']."'></br>
					<input type=text name=txt_direksi_ktp_3 id=txt_direksi_ktp_3 value='".$_POST['txt_direksi_ktp_3']."'>",
					
					"<input type=text name=txt_komisaris_ktp id=txt_komisaris_ktp value='".$_POST['txt_komisaris_ktp']."'></br>
					<input type=text name=txt_komisaris_ktp_2 id=txt_komisaris_ktp_2 value='".$_POST['txt_komisaris_ktp_2']."'></br>
					<input type=text name=txt_komisaris_ktp_3 id=txt_komisaris_ktp_3 value='".$_POST['txt_komisaris_ktp_3']."'>",
				),
				'FIELD3' => array (
					"<input type=text name=txt_pemegang_saham_milik id=txt_pemegang_saham_milik value='".$_POST['txt_pemegang_saham_milik']."'></br>
					<input type=text name=txt_pemegang_saham_milik_2 id=txt_pemegang_saham_milik_2 value='".$_POST['txt_pemegang_saham_milik_2']."'></br>
					<input type=text name=txt_pemegang_saham_milik_3 id=txt_pemegang_saham_milik_3 value='".$_POST['txt_pemegang_saham_milik_3']."'>",
					
					"".$data->datePicker('txt_direksi_tgl', $_POST[txt_direksi_tgl],'')."
					".$data->datePicker('txt_direksi_tgl_2', $_POST[txt_direksi_tgl_2],'')."
					".$data->datePicker('txt_direksi_tgl_3', $_POST[txt_direksi_tgl_3],'')."",

					"".$data->datePicker('txt_komisaris_tgl', $_POST[txt_komisaris_tgl],'')."
					".$data->datePicker('txt_komisaris_tgl_2', $_POST[txt_komisaris_tgl_2],'')."
					".$data->datePicker('txt_komisaris_tgl_3', $_POST[txt_komisaris_tgl_3],'')."",

				),
				'FIELD4' => array (
					"",
					"<input type=text name=txt_direksi_jbt id=txt_direksi_jbt value='".$_POST['txt_direksi_jbt']."'></br>
					<input type=text name=txt_direksi_jbt_2 id=txt_direksi_jbt_2 value='".$_POST['txt_direksi_jbt_2']."'></br>
					<input type=text name=txt_direksi_jbt_3 id=txt_direksi_jbt_3 value='".$_POST['txt_direksi_jbt_3']."'>",
					
					"<input type=text name=txt_komisaris_jbt id=txt_komisaris_jbt value='".$_POST['txt_komisaris_jbt']."'></br>
					<input type=text name=txt_komisaris_jbt_2 id=txt_komisaris_jbt_2 value='".$_POST['txt_komisaris_jbt_2']."'></br>
					<input type=text name=txt_komisaris_jbt_3 id=txt_komisaris_jbt_3 value='".$_POST['txt_komisaris_jbt_3']."'>",
				)
          	 );	

		$dataRows4 = array
		(
			'TEXT' => array(
				'Modal Saham Disetor(dalam Rp.)',
				'Total Aset :',
				'Total Kewajiban :',
				'Laba Bersih :',
				'Pendapatan Operasional :',
				'Pendapatan Non-Operasional :',
				'Tujuan Investasi <span class="redstar">*</span>',
				'Asset Owner'
				),
			'DOT'  => array (':',':',':'),
			'FIELD' => array
			(
				$data->cb_one('txt_modal_saham',$_POST[txt_modal_saham]),
				$data->cb_one('txt_total_aset',$_POST[txt_total_aset]),
				$data->cb_one('txt_tot_wajib',$_POST[txt_tot_wajib]),
				$data->cb_two('txt_laba_bersih',$_POST[txt_laba_bersih]),
				$data->cb_two('txt_pndp_opera',$_POST[txt_pndp_opera]),
				$data->cb_two('txt_pndp_non_opera',$_POST[txt_pndp_non_opera]),
				$data->cb_tujuan('txt_inves',$_POST[txt_inves]),
				$data->cb_assetowner('asset_owner',$_POST[asset_owner]),
			),
		);
		$dataRows5 = array
		(
				'TEXT' => array(
				'NAMA BANK DAN CABANG',
				),
				'TEXT2' => array(
				'NAMA PEMILIK REKENING',
				),
				'TEXT3' => array(
				'JENIS REKENING',
				),
				'TEXT4' => array(
				'No. REKENING',
				),
				'DOT'  => array (':'),
				'FIELD' => array (
				"1 :&nbsp;<input type=text name=txt_bank_cab id=txt_bank_cab value='".$_POST['txt_bank_cab']."'></br>
				 2 :&nbsp;<input type=text name=txt_bank_cab_2 id=txt_bank_cab_2 value='".$_POST['txt_bank_cab_2']."'>",
				),
				'FIELD2' => array (
				"&nbsp;<input type=text name=txt_pemilik_rek id=txt_pemilik_rek value='".$_POST['txt_pemilik_rek']."'></br>
				 &nbsp;<input type=text name=txt_pemilik_rek_2 id=txt_pemilik_rek_2 value='".$_POST['txt_pemilik_rek_2']."'>",
				),
				'FIELD3' => array (
				"&nbsp;<input type=text name=txt_jenis_rek id=txt_jenis_rek value='".$_POST['txt_jenis_rek']."'></br>
				 &nbsp;<input type=text name=txt_jenis_rek_2 id=txt_jenis_rek_2 value='".$_POST['txt_jenis_rek_2']."'>",
				),
				'FIELD4' => array (
				"&nbsp;<input type=text name=txt_no_rek id=txt_no_rek value='".$_POST['txt_no_rek']."'></br>
				 &nbsp;<input type=text name=txt_no_rek_2 id=txt_no_rek_2 value='".$_POST['txt_no_rek_2']."'>",
				),
		);
		$dataRows6 = array
		(
				/*'TEXT' => array(
				'NAMA',
				),
				'TEXT2' => array(
				'JABATAN',
				),
				'TEXT3' => array(
				'No. TELEPON/HANDPHONE',
				),
				'TEXT4' => array(
				'EMAIL',
				),
				'TEXT5' => array(
				'No. KTP/SIM/PASPOR/KIMS',
				),
				'DOT'  => array (':'),
				'FIELD' => array (
				"1 :&nbsp;<input type=text name=txt_nama_ id=txt_nama_ value='".$_POST['txt_nama_']."'>
					<input type=text name=authorized_person_middle_name1 id=authorized_person_middle_name1 size=10 value='".$_POST['authorized_person_middle_name1']."'>
					<input type=text name=authorized_person_last_name1 id=authorized_person_last_name1 size=10 value='".$_POST['authorized_person_last_name1']."'></br>
				 2 :&nbsp;<input type=text name=txt_nama_2 id=txt_nama_2 value='".$_POST['txt_nama_2']."'>
				 <input type=text name=authorized_person_middle_name2 id=authorized_person_middle_name2 size=10 value='".$_POST['authorized_person_middle_name2']."'>
					<input type=text name=authorized_person_last_name2 id=authorized_person_last_name2 size=10 value='".$_POST['authorized_person_last_name2']."'>",
				),
				'FIELD2' => array (
				"&nbsp;<input type=text name=txt_jabatan id=txt_jabatan value='".$_POST['txt_jabatan']."'></br>
				 &nbsp;<input type=text name=txt_jabatan_2 id=txt_jabatan_2 value='".$_POST['txt_jabatan_2']."'>",
				),
				'FIELD3' => array (
				"&nbsp;<input type=text name=txt_telp_hp id=txt_telp_hp value='".$_POST['txt_telp_hp']."'></br>
				 &nbsp;<input type=text name=txt_telp_hp_2 id=txt_telp_hp_2 value='".$_POST['txt_telp_hp_2']."'>",
				),
				'FIELD4' => array (
				"&nbsp;<input type=text name=authorized_person_email1 id=authorized_person_email1 value='".$_POST['authorized_person_email1']."'></br>
				 &nbsp;<input type=text name=authorized_person_email2 id=authorized_person_email2 value='".$_POST['authorized_person_email2']."'>",
				),
				'FIELD5' => array (
				"&nbsp;<input type=text name=txt_no_ktp id=txt_no_ktp value='".$_POST['txt_no_ktp']."'></br>
				 &nbsp;<input type=text name=txt_no_ktp_2 id=txt_no_ktp_2 value='".$_POST['txt_no_ktp_2']."'>",
				),*/
				'TEXT' => array(
					'NAME',
					'JABATAN',
					'TELEPHONE',
					'EMAIL',
					'NPWP No.',
					'KTP No.',
					'KTP EXPIRATION DATE',
					'PASSPORT NO.',
					'PASSPORT EXPIRATION DATE'
				),
				'FIELD' => array(
					"<input type=text name=txt_nama_ id=txt_nama_ value='".$_POST['txt_nama_']."'>
					<input type=text name=authorized_person_middle_name1 id=authorized_person_middle_name1 value='".$_POST['authorized_person_middle_name1']."'>
					<input type=text name=authorized_person_last_name1 id=authorized_person_last_name1  value='".$_POST['authorized_person_last_name1']."'>",
					"<input type=text name=txt_jabatan id=txt_jabatan value='".$_POST['txt_jabatan']."'>",
					"<input type=text name=txt_telp_hp id=txt_telp_hp value='".$_POST['txt_telp_hp']."'>",
					"<input type=text name=authorized_person_email1 id=authorized_person_email1 value='".$_POST['authorized_person_email1']."'>",
					"<input type=text name=authorized_person_npwp1 id=authorized_person_npwp1 value='".$_POST['authorized_person_npwp1']."'>",
					"<input type=text name=txt_no_ktp id=txt_no_ktp value='".$_POST['txt_no_ktp']."'>",
					$data->datepicker('authorized_person_ktp_expiration_date1',$_POST['authorized_person_ktp_expiration_date1'], ''),
					"<input type=text name=authorized_person_passport1 id=authorized_person_passport1 value='".$_POST['authorized_person_passport1']."'>",
					$data->datepicker('authorized_person_passport_expiration_date1',$_POST['authorized_person_passport_expiration_date1'], '')
				),
		);
		$dataRows7 = array
		(
				'TEXT' => array(
					'NAME',
					'JABATAN',
					'TELEPHONE',
					'EMAIL',
					'NPWP No.',
					'KTP No.',
					'KTP EXPIRATION DATE',
					'PASSPORT NO.',
					'PASSPORT EXPIRATION DATE'
				),
				'FIELD' => array(
					"<input type=text name=txt_nama_2 id=txt_nama_2 value='".$_POST['txt_nama_2']."'>
				 <input type=text name=authorized_person_middle_name2 id=authorized_person_middle_name2 value='".$_POST['authorized_person_middle_name2']."'>
					<input type=text name=authorized_person_last_name2 id=authorized_person_last_name2 value='".$_POST['authorized_person_last_name2']."'>",
					"<input type=text name=txt_jabatan_2 id=txt_jabatan_2 value='".$_POST['txt_jabatan_2']."'>",
					"<input type=text name=txt_telp_hp_2 id=txt_telp_hp_2 value='".$_POST['txt_telp_hp_2']."'>",
					"<input type=text name=authorized_person_email2 id=authorized_person_email2 value='".$_POST['authorized_person_email2']."'>",
					"<input type=text name=authorized_person_npwp2 id=authorized_person_npwp2 value='".$_POST['authorized_person_npwp2']."'>",
					"<input type=text name=txt_no_ktp id=txt_no_ktp_2 value='".$_POST['txt_no_ktp_2']."'>",
					$data->datepicker('authorized_person_ktp_expiration_date2',$_POST['authorized_person_ktp_expiration_date2'], ''),
					"<input type=text name=authorized_person_passport2 id=authorized_person_passport2 value='".$_POST['authorized_person_passport2']."'>",
					$data->datepicker('authorized_person_passport_expiration_date2',$_POST['authorized_person_passport_expiration_date2'], '')
				),
		);

		$rowAssetInformation = array
		(
				'TEXT' => array(
					'LAST YEAR',
					'2 YEARS AGO',
					'3 YEARS AGO',
				),
				'FIELD' => array(
					$data->cb_assetinformation('asset_information_last_year',$_POST['asset_information_last_year'],''),
					$data->cb_assetinformation('asset_information_2_years_ago',$_POST['asset_information_2_years_ago'],''),
					$data->cb_assetinformation('asset_information_3_years_ago',$_POST['asset_information_3_years_ago'],''),
				),
		);

		$rowProfitInformation = array
		(
				'TEXT' => array(
					'LAST YEAR',
					'2 YEARS AGO',
					'3 YEARS AGO',
				),
				'FIELD' => array(
					$data->cb_profitinformation('profit_information_last_year',$_POST['asset_information_last_year'],''),
					$data->cb_profitinformation('profit_information_2_years_ago',$_POST['asset_information_2_years_ago'],''),
					$data->cb_profitinformation('profit_information_3_years_ago',$_POST['asset_information_3_years_ago'],''),
				),
		);

		$rowOther = array
		(
				'TEXT' => array(
					'FATCA',
					'TIN/ Foreign TIN',
					'TIN/ Foreign TIN Issuance Country',
					'Global Intermediary Identification Number',
					'Substantial U.S Owner Name',
					'Substantial U.S Owner Address',
					'Substantial U.S Owner TIN',
				),
				'FIELD' => array(
					$data->cb_fatcainstitusi('fatca',$_POST['fatca'],''),
					"<input type=text name=tin id=tin value='".$_POST['tin']."'>",
					$data->cb_isocountry('tin_issuance_country', $_POST['tin_issuance_country'],''),
					"<input type=text name=giin id=giin value='".$_POST['giin']."'>",
					"<input type=text name=substantial_us_owner_name id=substantial_us_owner_name value='".$_POST['substantial_us_owner_name']."'>",
					"<input type=text name=substantial_us_owner_address id=substantial_us_owner_address value='".$_POST['substantial_us_owner_address']."'>",
					"<input type=text name=substantial_us_owner_tin id=substantial_us_owner_tin value='".$_POST['substantial_us_owner_tin']."'>",
				),
		);

		$rowRedmPayment = array
		(
				'TEXT' => array(
					'BANK BIC CODE 1',
					'BANK BI MEMBER CODE 1',
					'BANK NAME 1 <span class="redstar">*</span>',
					'BANK COUNTRY 1 <span class="redstar">*</span>',
					'BANK BRANCH 1',
					'ACCOUNT TYPE 1',
					'ACCOUNT CCY 1 <span class="redstar">*</span>',
					'ACCOUNT NO 1 <span class="redstar">*</span>',
					'ACCOUNT NAME 1 <span class="redstar">*</span>',
					'',
					'BANK BIC CODE 2',
					'BANK BI MEMBER CODE 2',
					'BANK NAME 2 <span class="redstar">*</span>',
					'BANK COUNTRY 2 <span class="redstar">*</span>',
					'BANK BRANCH 2',
					'ACCOUNT TYPE 2',
					'ACCOUNT CCY 2 <span class="redstar">*</span>',
					'ACCOUNT NO 2 <span class="redstar">*</span>',
					'ACCOUNT NAME 2 <span class="redstar">*</span>',
					'',
					'BANK BIC CODE 3',
					'BANK BI MEMBER CODE 3',
					'BANK NAME 3 <span class="redstar">*</span>',
					'BANK COUNTRY 3 <span class="redstar">*</span>',
					'BANK BRANCH 3',
					'ACCOUNT TYPE 3',
					'ACCOUNT CCY 3 <span class="redstar">*</span>',
					'ACCOUNT NO 3 <span class="redstar">*</span>',
					'ACCOUNT NAME 3 <span class="redstar">*</span>',
				),
				'FIELD' => array(
					"<input type=text name=bank_bic_code1 id=bank_bic_code1 value='".$_POST['bank_bic_code1']."'>",
					//"<input type=text name=bank_bi_member_code1 id=bank_bi_member_code1 value='".$_POST['bank_bi_member_code1']."'>",
					$data->cb_bimembercode('bank_bi_member_code1', $_POST[bank_bi_member_code1], 'onchange="changeBi1(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_bank_cab id=txt_bank_cab value='".$_POST['txt_bank_cab']."'>",
					$data->cb_isocountry('bank_country1',$_POST['bank_country1'],''),
					"<input type=text name=bank_branch1 id=bank_branch1 value='".$_POST['bank_branch1']."'>",
					"<input type=text name=txt_jenis_rek id=txt_jenis_rek value='".$_POST['txt_jenis_rek']."'>",
					$data->cb_accountccy('acc_ccy1', $_POST['acc_ccy1'],''),
					"<input type=text name=txt_no_rek id=txt_no_rek value='".$_POST['txt_no_rek']."'>",
					"<input type=text name=txt_pemilik_rek id=txt_pemilik_rek value='".$_POST['txt_pemilik_rek']."'>",
					'',
					"<input type=text name=bank_bic_code2 id=bank_bic_code2 value='".$_POST['bank_bic_code2']."'>",
					//"<input type=text name=bank_bi_member_code2 id=bank_bi_member_code2 value='".$_POST['bank_bi_member_code2']."'>",
					$data->cb_bimembercode('bank_bi_member_code2', $_POST[bank_bi_member_code2], 'onchange="changeBi2(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_bank_cab_2 id=txt_bank_cab_2 value='".$_POST['txt_bank_cab_2']."'>",
					$data->cb_isocountry('bank_country2',$_POST['bank_country2'],''),
					"<input type=text name=bank_branch2 id=bank_branch2 value='".$_POST['bank_branch2']."'>",
					"<input type=text name=txt_jenis_rek_2 id=txt_jenis_rek_2 value='".$_POST['txt_jenis_rek_2']."'>",
					$data->cb_accountccy('acc_ccy2', $_POST['acc_ccy2'],''),
					"<input type=text name=txt_no_rek_2 id=txt_no_rek_2 value='".$_POST['txt_no_rek_2']."'>",
					"<input type=text name=txt_pemilik_rek_2 id=txt_pemilik_rek_2 value='".$_POST['txt_pemilik_rek_2']."'>",
					'',
					"<input type=text name=bank_bic_code3 id=bank_bic_code3 value='".$_POST['bank_bic_code3']."'>",
					//"<input type=text name=bank_bi_member_code3 id=bank_bi_member_code3 value='".$_POST['bank_bi_member_code3']."'>",
					$data->cb_bimembercode('bank_bi_member_code3', $_POST[bank_bi_member_code3], 'onchange="changeBi3(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=bank_name3 id=bank_name3 value='".$_POST['bank_name3']."'>",
					$data->cb_isocountry('bank_country3',$_POST['bank_country3'],''),
					"<input type=text name=bank_branch3 id=bank_branch3 value='".$_POST['bank_branch3']."'>",
					"<input type=text name=acc_type3 id=acc_type3 value='".$_POST['acc_type3']."'>",
					$data->cb_accountccy('acc_ccy3', $_POST['acc_ccy3'],''),
					"<input type=text name=acc_no3 id=acc_no3 value='".$_POST['acc_no3']."'>",
					"<input type=text name=acc_name3 id=acc_name3 value='".$_POST['acc_name3']."'>",
				),
		);

			$dataRowsAria = array (
				'TEXT' => array(
					'NOMOR SKD/AD/ART',
					'SKD/AD/ART Exp Date',
					'DOMISILI',
					'NEGARA DOMISILI <span class="redstar">*</span>',
					'TIPE <span class="redstar">*</span>',
					'KARAKTERISTIK <span class="redstar">*</span>',
					'SUMBER DANA <span class="redstar">*</span>',
					'PENGHASILAN <span class="redstar">*</span>',
					'ARTICLES OF ASSOCIATION No.',
					'INVESTORS RISK PROFILE',
					'NO SID',
					'CLIENT CODE'
				),
				'DOT'  => array (':',':',':',':'),
				'FIELD' => array (
					"<input type=text name=cus_no_skd id=cus_no_skd value='".$_POST['cus_no_skd']."'>",
					$data->datePicker('skd_expiration_date', $_POST[skd_expiration_date],''),
					$data->cb_domisili('txt_cus_domisili',$_POST[txt_cus_domisili]),
					$data->cb_isocountry('country_of_domicily',$_POST[country_of_domicily]),
					$data->cb_tipe('txt_cus_tipe',$_POST[txt_cus_tipe]),
					$data->cb_karakteristik('txt_cus_karakteristik',$_POST[txt_cus_karakteristik]),
					$data->cb_sumber_dana_inst('txt_cus_sumber_dana',$_POST[txt_cus_sumber_dana]),
					$data->cb_penghasilan_insti('txt_cus_penghasilan',$_POST[txt_cus_penghasilan]),
                    "<input type=text name=articles_of_association_no id=articles_of_association_no size=40 maxlength=40 value='".$_POST[articles_of_association_no]."'>",
					$data->cb_riskprofile('investors_risk_profile', $_POST[investor_risk_profile]),
					"<input type=text name=cus_ins_sid id=cus_ins_sid value='".$_POST['cus_ins_sid']."'>",
					"<input type=text name=client_code id=client_code maxlength=6 value='".$_POST['client_code']."'>"
				)
			);
    $tittle = "CUSTOMER / CLIENT ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"opener.document.form1.submit();window.parent.close();\">"
					);
}

if ($_GET['detail']==1){
	$rows = $data->selectQuery("select * from ".$tablename." where cus_id ='".$_GET['id']."'");
	#print_r($rows);
	#$cus_pt = $data->selectQuery("select * from tbl_kr_pt where cus_id ='".$_GET['id']."'");
	
	#print_r($rows);
	#echo"</br>";
	if($rows[cus_actived]==1){
		$act = "Active";
	}else{
		$act = "Non-Active";
	}
	if ($rows[cus_status_identity]==""){
		$masa_ktp = $rows[cus_berlaku_card];
	}
	else
	{
		$masa_ktp = $rows[cus_status_identity];
	}
	
	if ($rows[cus_penghasilan]==1)
	{
	$penghaislan = "< 25 Juta";
	}
	else if ($rows[cus_penghasilan]==2)
	{
		$penghaislan = "25 Juta-100 Juta";
	}
	else if ($rows[cus_penghasilan]==3)
	{
		$penghaislan = "100 Juta-250 Juta";
	}
	else if ($rows[cus_penghasilan]==4)
	{
		$penghaislan = "250 Juta-1 M";
	}
	else if ($rows[cus_penghasilan]==5)
	{
		$penghaislan = ">1 M";
	}
	
	if($rows[cus_pt] == 1)
	{
		$institusi = "BUMN";
	}else if($rows[cus_pt] == 2)
	{
		$institusi = "Perseroan";
	}else if($rows[cus_pt] == 3)
	{
		$institusi = "Swasta";
	}else if($rows[cus_pt] == 4)
	{
		$institusi = "Yayasan";
	}else if($rows[cus_pt] == 5)
	{
		$institusi = "Perseroan (tertutup)";
	}else if($rows[cus_pt] == 6)
	{
		$institusi = "perseroan(terbuka)";
	}else if($rows[cus_pt] == 7)
	{
		$institusi = "PMA";
	}else if($rows[cus_pt] == 8)
	{
		$institusi = "Kemitraan";
	}else if($rows[cus_pt] == 9)
	{
		$institusi = "Koperasi";
	}
	
	if($rows[cus_struktur] == 1)
	{
		$str = "Lokal";
	}
	else if($rows[cus_struktur] == 2)
	{
		$str = "Asing";
	}
	
	if($rows[cus_struktur_2] == 1)
	{
		$str2 = "Lokal";
	}
	else if($rows[cus_struktur_2] == 2)
	{
		$str2 = "Asing";
	}
	if($rows[cus_status_domisili_pt] == 1)
	{
		$domisili = "Milik Sendiri";
	}
	else if($rows[cus_status_domisili_pt] == 2)
	{
		$domisili = "Sewa";
	}
	else
	{
		$domisili = "Lainnya";
	}
	
	
	if($rows[cus_modal_saham] == 1)
	{
		$modal = "<500 juta";
	}
	else if($rows[cus_modal_saham] == 2)
	{
		$modal = "500 juta - 1 Milliar";
	}
	else if($rows[cus_modal_saham] == 3)
	{
		$modal = "1 Milliar - 10 Milliar";
	}
	else 
	{
		$modal = "> 10 Milliar";
	}
	
	
	if($rows[cus_total_aset] == 1)
	{
		$aset = "<500 juta";
	}
	else if($rows[cus_total_aset] == 2)
	{
			$aset = "500 juta - 1 Milliar";
	}
	else if($rows[cus_total_aset] == 3)
	{
		$aset = "1 Milliar - 100 Milliar";
	}
	else
	{
		$aset = "> 10 Milliar";
	}
	
	if($rows[cus_tot_wajib] == 1)
	{
		$total = "<500 juta";
	}
	else if($rows[cus_tot_wajib] == 2)
	{
		$total = "500 juta - 100 Milliar";
	}
	else if($rows[cus_tot_wajib] == 3)
	{
		$total = "1 Milliar - 100 Milliar";
	}
	else
	{
		$total = "> 10 Milliar";
	}
	
	if($rows[cus_laba_bersih] == 1)
	{
		$laba = "<250 juta";
	}
	else if($rows[cus_laba_bersih] == 2)
	{
		$laba = " 250 Juta - 500 Juta ";
	}
	else if($rows[cus_laba_bersih] == 3)
	{
		$laba = " 500 Juta - 1 Milliar ";
	}
	else
	{
		$laba = " > 1 Milliar ";
	}
	
	
	if($rows[cus_pndp_opera] == 1)
	{
		$opera = "<250 Juta";
	}
	else if($rows[cus_pndp_opera] ==2)
	{
		$opera = "250 Juta - 500 Juta";
	}
	else if($rows[cus_pndp_opera] == 3)
	{
		$opera = "500 Juta - 1 Milliar";
	}
	else
	{
		$opera = "> 1 Milliar";
	}
	
	if($rows[cus_pndp_non_opera] == 1)
	{
		$non_opera = "<250 Juta";
	}
	else if($rows[cus_pndp_non_opera] == 2)
	{
		$non_opera = "250 Juta - 500 Juta";
	}
	else if($rows[cus_pndp_non_opera] == 3)
	{
		$non_opera = "500 Juta - 1 Milliar";
	}
	else
	{
		$non_opera = "> 1 Milliar";
	}
	
	if($rows[cus_inves] == 1)
	{
		$inves = "Investasi Jangka Panjang";
	}
	else if($rows[cus_inves] == 2)
	{
		$inves = "Apresiasi Harga";
	}
	else if($rows[cus_inves] == 3)
	{
		$inves = "Jangka Pendek";
	}
	else if($rows[cus_inves] == 4)
	{
		$inves = "Sumber Pendapatan";
	}
	else if($rows[cus_inves] == 5)
	{
		$inves = "Spekulasi";
	}
	else
	{
		$inves = "Lainnya___";
	}
	if($rows[benefical_owner]>=0){
		$beneficalowner="Data Sudah ada";
	}
	else {
		$beneficalowner=$rows[benefical_owner];
	}
	
	$dataRows = array (
				'TEXT' => array(
				'ID',
				'NAMA INSTITUSI',
				'TEMPAT',
				'TGL. PENDIRIAN',
				'JENIS INSTITUSI',
				'STRUKTUR KEPIMILIKAN SAHAM',
				'',
				'BIDANG USAHA',
				'ALAMAT LENGKAP',
				'',
				'No. TELP. PERUSAHAAN',
				'EMAIL PERUSAHAAN',
				'STATUS DOMISILI KANTOR',
				'LAMA MENEMPATI',
				'ACTIVE'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				"$rows[cus_code]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_name]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_tempat_pt]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_tgl_pt]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$institusi"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_struktur]"."<td bgcolor='#DAECFF' width='200' class='mandatory'>
				Persen Kepemilikan Saham : $rows[persen_struktur]%</td>",
				"$rows[cus_struktur_2]"."<td bgcolor='#DAECFF' width='200' class='mandatory'>
				Persen Kepemilikan Saham : $rows[persen_struktur_2]%</td>",
				
				"$rows[cus_bidang_usaha]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_alamat_pt]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"KOTA 	  : $rows[cus_kota_pt].</br>
				 KODE POS : $rows[cus_kode_pos_pt].</br>
				 PROPINSI : $rows[cus_propinsi_pt]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_telp_pt]"."<td bgcolor='#DAECFF' width='200' class='mandatory'>No. Fax : $rows[cus_fax_pt]</td>",
				"$rows[cus_email_pt]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_status_domisili_pt]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_lama]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_actived]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>"
				
				)
          			  );
			$dataRows2 = array (
				'TEXT' => array(
				'No. NPWP',
				'No. TDP',
				'TANGGAL KADARLUARSA TDP',
				'No. SIUPP/IJIN USAHA',
				'TANGGAL KADALUARSA SIUPP',
				'N. IJIN PMA',
				'TANGGAL KADALUARSA PMA',
				),
				'DOT'  => array (':',':',':',':',':',':',':'),
				'FIELD' => array 
				(
				"$rows[cus_no_npwp]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_no_tdp]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_tgl_tdp]"."<td bgcolor='#DAECFF' width='200' class='mandatory'>Diterbitkan di : $rows[cus_tempat_tdp]</td>",
				"$rows[cus_no_siup]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_tgl_siup]"."<td bgcolor='#DAECFF' width='200' class='mandatory'>Diterbitkan di : $rows[cus_tempat_siup]</td>",
				"$rows[cus_no_pma]"."<td bgcolor='#DAECFF' width='200' class='mandatory'/>",
				"$rows[cus_tgl_pma]"."<td bgcolor='#DAECFF' width='200' class='mandatory'>Diterbitkan di : $rows[cus_tempat_pma]</td>",
				)
          			  );		  
					  
	
		
	
			$dataRows3 = array 
			(
					'TEXT' => array(
					'PEMEGANG SAHAM',
					'SUSUNAN DIREKSI/PENGURUS',
					'SUSUNAN KOMISARIS/PENGURUS',
					),
					'TEXT2' => array(
					'NO KTP',
					'NO KTP',
					'NO KTP',
					),
					'TEXT3' => array(
					'JUMLAH KEPEMILIKAN',
					'TANGGAL LAHIR',
					'TANGGAL LAHIR',
					),
					'TEXT4' => array(
					'',
					'JABATAN',
					'JABATAN',
					),
					'DOT'  => array (':',':',':'),
					'FIELD' => array (

					"$rows[cus_pemegang_saham]</br>
					 $rows[cus_pemegang_saham_2]</br>
					 $rows[cus_pemegang_saham_3]",
					
					
					"$rows[cus_direksi]</br>
					 $rows[cus_direksi_2]</br>
					 $rows[cus_direksi_3]",
					
					
					"$rows[cus_komisaris]</br>
					 $rows[cus_komisaris_2]</br>
					 $rows[cus_komisaris_3]",
		
					),
					'FIELD2' => array (
					"$rows[cus_pemegang_saham_ktp]</br>
					 $rows[cus_pemegang_saham_ktp_2]</br>
					 $rows[cus_pemegang_saham_ktp_3]",
					
					
					"$rows[cus_direksi_ktp]</br>
					 $rows[cus_direksi_ktp_2]</br>
					 $rows[cus_direksi_ktp_3]",
					
					
					"$rows[cus_komisaris_ktp]</br>
					 $rows[cus_komisaris_ktp_2]</br>
					 $rows[cus_komisaris_ktp_3]",
					 ),
					 
					'FIELD3' => array (	
					"$rows[cus_pemegang_saham_milik]</br>
					 $rows[cus_pemegang_saham_milik_2]</br>
					 $rows[cus_pemegang_saham_milik_3]",
					
					"$rows[cus_komisaris_tgl]</br>
					 $rows[cus_komisaris_tgl_2]</br>
					 $rows[cus_komisaris_tgl_3]",
							
					"$rows[cus_komisaris_tgl]</br>
					 $rows[cus_komisaris_tgl_2]</br>
					 $rows[cus_komisaris_tgl_3]",
					),				
					
					'FIELD4' => array (	
					"",
					"$rows[cus_direksi_jbt]</br>
					 $rows[cus_direksi_jbt_2]</br>
					 $rows[cus_direksi_jbt_3]",
					
					"$rows[cus_komisaris_jbt]</br>
					 $rows[cus_komisaris_jbt_2]</br>
					 $rows[cus_komisaris_jbt_3]",
					)
			);			

				$dataRows4 = array
				(
				'TEXT' => array(
						'Modal Saham Disetor(dalam Rp.)',
						'Total Aset :',
						'Total Kewajiban :',
						'Laba Bersih :',
						'Pendapatan Operasional :',
						'Pendapatan Non-Operasional :',
						'Tujuan Investasi'
						),
					'DOT'  => array (':',':',':'),
					'FIELD' => array
					(
						$modal,
						$aset,
						$total,
						$laba,
						$opera,
						$non_opera,
						$inves,
					),);
					
		$dataRows5 = array
		(
				'TEXT' => array(
				'NAMA BANK DAN CABANG',
				),
				'TEXT2' => array(
				'NAMA PEMILIK REKENING',
				),
				'TEXT3' => array(
				'JENIS REKENING',
				),
				'TEXT4' => array(
				'No. REKENING',
				),
				'DOT'  => array (':'),
				'FIELD' => array (
				"$rows[cus_bank_cab]</br>
				 $rows[cus_bank_cab_2]",
				),
				'FIELD2' => array (
				"$rows[cus_pemilik_rek]</br>
				 $rows[cus_pemilik_rek_2]",
				),
				'FIELD3' => array (
				"$rows[cus_jenis_rek]</br>
				 $rows[cus_jenis_rek_2]",
				),
				'FIELD4' => array (
				"$rows[cus_no_rek]</br>
				 $rows[cus_no_rek_2]",
				),
		);
		$dataRows6 = array
		(
				'TEXT' => array(
				'NAMA',
				),
				'TEXT2' => array(
				'JABATAN',
				),
				'TEXT3' => array(
				'No. TELEPON/HANDPHONE',
				),
				'TEXT4' => array(
				'No. KTP/SIM/PASPOR/KIMS',
				),
				'DOT'  => array (':'),
				'FIELD' => array (
				"$rows[cus_nama_]</br>
				 $rows[cus_nama_2]",
				),
				'FIELD2' => array (
				"$rows[cus_jabatan]</br>
				 $rows[cus_jabatan_2]",
				),
				'FIELD3' => array (
				"$rows[cus_telp_hp]</br>
				 $rows[cus_telp_hp_2]",
				),
				'FIELD4' => array (
				"$rows[cus_no_KTP]</br>
				 $rows[cus_no_KTP_2]",
				),
			);
	############################################################################		
	if($rows[cus_domisili] == 1)
	{
		$domisili = "Lokal";
	}
	else if($rows[cus_domisili] == 2)
	{
		$domisili = "Asing";
	}
	
	if($rows[cus_tipe] == 1)
	{
		$tipe = "Perusahaan/Corporation";
	}
	else if($rows[cus_tipe] == 2)
	{
		$tipe = "Yayasan/Foundation";
	}
	else if($rows[cus_tipe] == 3)
	{
		$tipe = "Lembaga Keuangan/Finacial Institution";
	}
	else if($rows[cus_tipe] == 4)
	{
		$tipe = "Asuransi/Insurance";
	}
	else if($rows[cus_tipe] == 5)
	{
		$tipe = "Reksa Dana/Mutual Fund";
	}
	else if($rows[cus_tipe] == 6)
	{
		$tipe = "Dana Pensiun";
	}
	else if($rows[cus_tipe] == 7)
	{
		$tipe = "Perusahaan Efek/Securities Company";
	}
	else if($rows[cus_tipe] == 8)
	{
		$tipe = "Lainnya";
	}
		
	if($rows[cus_karakteristik] == 1)
	{
		$karakteristik = "BUMN/Negeri";
	}
	else if($rows[cus_karakteristik] == 2)
	{
		$karakteristik = "Swasta";
	}
		else if($rows[cus_karakteristik] == 3)
	{
		$karakteristik = "Sosial";
	}
	else if($rows[cus_karakteristik] == 4)
	{
		$karakteristik = "Joint Venture";
	}
	else if($rows[cus_karakteristik] == 5)
	{
		$karakteristik = "PMA/Foreign Invesment Company";
	}	
	else if($rows[cus_karakteristik] == 6)
	{
		$karakteristik = "Perusahaan Keluarga";
	}
	else if($rows[cus_karakteristik] == 7)
	{
		$karakteristik = "Aliansi";
	}else if($rows[cus_karakteristik] == 8)
	{
		$karakteristik = "Lainnya";
	}
	
	if($rows[cus_penghasilan] == 1)
	{
		$penghasilan = "< 1 Milyar/Tahun";
	}
	else if($rows[cus_penghasilan] == 2)
	{
		$penghasilan = "> 1 Milyar - 5 Milyar/Tahun";
	}
	else if($rows[cus_penghasilan] == 3)
	{
		$penghasilan = "> 5 Milyar - 10 Milyar/Tahun";
	}	
	else if($rows[cus_penghasilan] == 4)
	{
		$penghasilan = "> 10 Milyar - 50 Milyar/Tahun";
	}	
	else if($rows[cus_penghasilan] == 5)
	{
		$penghasilan = "> 50 Milyar/Tahun";
	}	
	
	if($rows[cus_sumber_dana] == 1)
	{
		$sumber_dana = "Dana Usaha/Business Profit";
	}
	else if($rows[cus_sumber_dana] == 2)
	{
		$sumber_dana = "Dana Pensiun/Pension Fund";
	}
	else if($rows[cus_sumber_dana] == 3)
	{
		$sumber_dana = "Bunga Simpanan";
	}
	else if($rows[cus_sumber_dana] == 4)
	{
		$sumber_dana = "Dari Investasi";
	}			
	else if($rows[cus_sumber_dana] == 5)
	{
		$sumber_dana = "Lainnya";
	}		
	
	###############################################################	
	$dataRowsAria = array (
				'TEXT' => array(
				'NOMOR SKD/AD/ART',
				'DOMISILI',
				'TIPE',
				'KARAKTERISTIK',
				'SUMBER DANA',
				'PENGHASILAN',
				),
				'DOT'  => array (':',':',':',':'),
				'FIELD' => array (
				$row[cus_no_skd],
				$domisili,
				$tipe,
				$karakteristik,
				$sumber_dana,
				$penghasilan
			
				)
          			  );		  
	$tittle = "CUSTOMER / CLIENT DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}
#EDIT
if ($_POST['btn_save_edit'])
{
	$name = trim(htmlentities($_POST['txt_cus_name']));
	if($name == ''){
		$errorMessage .= "NAMA INSTITUSI must be filled\\n";
		$gotError = true;
	}
	$countrydomisili = trim(htmlentities($_POST['country_of_domicily']));
	if($countrydomisili == ''){
		$errorMessage .= "COUNTRY OF DOMICILE must be filled\\n";
		$gotError = true;
	}
	$place = trim(htmlentities($_POST['txt_tempat_pt']));
	if($place == ''){
		$errorMessage .= "TEMPAT must be filled\\n";
		$gotError = true;
	}
	$address = trim(htmlentities($_POST['txt_alamat_pt']));
	if($address == ''){
		$errorMessage .= "ALAMAT LENGKAP must be filled\\n";
		$gotError = true;
	}


	//$npwp = trim(htmlentities($_POST['txt_no_npwp1']));
	

	$npwp = strval($_POST['txt_no_npwp1']).''.strval($_POST['txt_no_npwp2']).''.strval($_POST['txt_no_npwp3']).''.strval($_POST['txt_no_npwp4']).''.strval($_POST['txt_no_npwp5']).''.strval($_POST['txt_no_npwp6']);
	
	
	if($npwp == ''){
		$errorMessage .= "No. NPWP must be filled\\n";
		$gotError = true;
	}

	$sql= "UPDATE ".$tablename." SET 
			cus_code = '".$_POST['txt_cus_id']."',
			cus_name 	= '".$_POST['txt_cus_name']."',
			cus_tempat_pt='".$_POST['txt_tempat_pt']."',
			cus_tgl_pt='".$_POST['txt_tgl_pt']."',
			cus_pt='".$_POST['txt_cus_pt']."',
			cus_struktur='".$_POST['txt_struktur']."',
			persen_struktur='".$_POST['txt_persen_struktur']."',

			cus_struktur_2='".$_POST['txt_struktur_2']."',
			persen_struktur_2='".$_POST['txt_persen_struktur_2']."',

			cus_bidang_usaha='".$_POST['txt_bidang_usaha']."',
			cus_alamat_pt='".$_POST['txt_alamat_pt']."',
			cus_kota_pt='".$_POST['txt_kota_pt']."',
			cus_kode_pos_pt=	'".$_POST['txt_kode_pos_pt']."',
			cus_propinsi_pt='".$_POST['txt_propinsi_pt']."',
			
			cus_telp_pt='".$_POST['txt_telp_pt']."',
			cus_fax_pt='".$_POST['txt_fax_pt']."',
			cus_email_pt='".$_POST['txt_email_pt']."',
			cus_status_domisili_pt='".$_POST['txt_status_domisili_pt']."',
			cus_lama='".$_POST['txt_lama_pt']."',	
			cus_actived='".$_POST['txt_cus_actived']."',
			
			
			
			cus_no_npwp='".$npwp."',
			cus_no_tdp='".$_POST['txt_no_tdp']."',
			cus_tgl_tdp='".$_POST['txt_tgl_tdp']."',
			cus_tempat_tdp='".$_POST['txt_tempat_tdp']."',
			
			cus_no_siup='".$_POST['txt_no_siup']."',
			cus_tgl_siup='".$_POST['txt_tgl_siup']."',
			cus_tempat_siup='".$_POST['txt_tempat_siup']."',
			cus_no_pma='".$_POST['txt_no_pma']."',
			cus_tgl_pma='".$_POST['txt_tgl_pma']."',
			cus_tempat_pma='".$_POST['txt_tempat_pma']."',
			
		
			cus_pemegang_saham='".$_POST['txt_pemegang_saham']."',
			cus_pemegang_saham_ktp='".$_POST['txt_pemegang_saham_ktp']."',
			cus_pemegang_saham_milik='".$_POST['txt_pemegang_saham_milik']."',
			
			cus_pemegang_saham_2='".$_POST['txt_pemegang_saham_2']."',
			cus_pemegang_saham_ktp_2='".$_POST['txt_pemegang_saham_ktp_2']."',
			cus_pemegang_saham_milik_2=	'".$_POST['txt_pemegang_saham_milik_2']."',
			
			cus_pemegang_saham_3='".$_POST['txt_pemegang_saham_3']."',
			cus_pemegang_saham_ktp_3='".$_POST['txt_pemegang_saham_ktp_3']."',
			cus_pemegang_saham_milik_3='".$_POST['txt_pemegang_saham_milik_3']."',
				
			cus_direksi='".$_POST['txt_direksi']."',
			cus_direksi_ktp='".$_POST['txt_direksi_ktp']."',
			cus_direksi_tgl='".$_POST['txt_direksi_tgl']."',
			cus_direksi_jbt='".$_POST['txt_direksi_jbt']."',	

			cus_direksi_2='".$_POST['txt_direksi_2']."',
			cus_direksi_ktp_2='".$_POST['txt_direksi_ktp_2']."',
			cus_direksi_tgl_2='".$_POST['txt_direksi_tgl_2']."',
			cus_direksi_jbt_2='".$_POST['txt_direksi_jbt_2']."',		

			cus_direksi_3='".$_POST['txt_direksi_3']."',
			cus_direksi_ktp_3='".$_POST['txt_direksi_ktp_3']."',
			cus_direksi_tgl_3='".$_POST['txt_direksi_tgl_3']."',
			cus_direksi_jbt_3='".$_POST['txt_direksi_jbt_3']."',
			
			cus_komisaris='".$_POST['txt_komisaris']."',
			cus_komisaris_ktp='".$_POST['txt_komisaris_ktp']."',
			cus_komisaris_tgl='".$_POST['txt_komisaris_tgl']."',
			cus_komisaris_jbt='".$_POST['txt_komisaris_jbt']."',
			
			cus_komisaris_2='".$_POST['txt_komisaris_2']."',
			cus_komisaris_ktp_2='".$_POST['txt_komisaris_ktp_2']."',
			cus_komisaris_tgl_2='".$_POST['txt_komisaris_tgl_2']."',
			cus_komisaris_jbt_2='".$_POST['txt_komisaris_jbt_2']."',	

			cus_komisaris_3='".$_POST['txt_komisaris_3']."',
			cus_komisaris_ktp_3='".$_POST['txt_komisaris_ktp_3']."',
			cus_komisaris_tgl_3='".$_POST['txt_komisaris_tgl_3']."',
			cus_komisaris_jbt_3='".$_POST['txt_komisaris_jbt_3']."',
			
			cus_modal_saham='".$_POST['txt_modal_saham']."',
			cus_total_aset='".$_POST['txt_total_aset']."',
			cus_tot_wajib='".$_POST['txt_tot_wajib']."',
			cus_laba_bersih='".$_POST['txt_laba_bersih']."',
			cus_pndp_opera='".$_POST['txt_pndp_opera']."',
			cus_pndp_non_opera='".$_POST['txt_pndp_non_opera']."',
			cus_inves='".$_POST['txt_inves']."',
			
			
			cus_bank_cab='".$_POST['txt_bank_cab']."',
			cus_pemilik_rek='".$_POST['txt_pemilik_rek']."',
			cus_jenis_rek='".$_POST['txt_jenis_rek']."',
			cus_no_rek='".$_POST['txt_no_rek']."',

			cus_bank_cab_2='".$_POST['txt_bank_cab_2']."',
			cus_pemilik_rek_2='".$_POST['txt_pemilik_rek_2']."',
			cus_jenis_rek_2='".$_POST['txt_jenis_rek_2']."',
			cus_no_rek_2='".$_POST['txt_no_rek_2']."',
			
			cus_nama_='".$_POST['txt_nama_']."',
			cus_jabatan='".$_POST['txt_jabatan']."',
			cus_telp_hp='".$_POST['txt_telp_hp']."',
			cus_no_KTP='".$_POST['txt_no_ktp']."',
			
			cus_nama_2='".$_POST['txt_nama_2']."',
			cus_jabatan_2='".$_POST['txt_jabatan_2']."',
			cus_telp_hp_2='".$_POST['txt_telp_hp_2']."',
			cus_no_KTP_2='".$_POST['txt_no_ktp_2']."',
			
			cus_domisili='".$_POST['txt_cus_domisili']."',
			cus_tipe='".$_POST['txt_cus_tipe']."',
			cus_karakteristik='".$_POST['txt_cus_karakteristik']."',
			cus_sumber_dana='".$_POST['txt_cus_sumber_dana']."',
			cus_penghasilan='".$_POST['txt_cus_penghasilan']."',
			cus_no_skd='".$_POST['cus_no_skd']."',
			
			country_of_domicily='".$_POST['country_of_domicily']."',
			skd_expiration_date='".$_POST['skd_expiration_date']."',
			country_of_establisment='".$_POST['country_of_establisment']."',
			articles_of_association_no='".$_POST['articles_of_association_no']."',
			investors_riks_profile='".$_POST['investors_risk_profile']."',
			asset_owner='".$_POST['asset_owner']."',
			country_of_company='".$_POST['country_of_company']."',
			statement_type='".$_POST['statement_type']."',
			benefical_owner='".$beneficalowner."',
			authorized_person_middle_name1='".$_POST['authorized_person_middle_name1']."',
			authorized_person_last_name1='".$_POST['authorized_person_last_name1']."',
			authorized_person_email1='".$_POST['authorized_person_email1']."',
			authorized_person_npwp1='".$_POST['authorized_person_npwp1']."',
			authorized_person_ktp_expiration_date1='".$_POST['authorized_person_ktp_expiration_date1']."',
			authorized_person_passport1='".$_POST['authorized_person_passport1']."',
			authorized_person_passport_expiration_date1='".$_POST['authorized_person_passport_expiration_date1']."',
			authorized_person_middle_name2='".$_POST['authorized_person_middle_name2']."',
			authorized_person_last_name2='".$_POST['authorized_person_last_name2']."',
			authorized_person_email2='".$_POST['authorized_person_email2']."',
			authorized_person_npwp2='".$_POST['authorized_person_npwp2']."',
			authorized_person_ktp_expiration_date2='".$_POST['authorized_person_ktp_expiration_date2']."',
			authorized_person_passport2='".$_POST['authorized_person_passport2']."',
			authorized_person_passport_expiration_date2='".$_POST['authorized_person_passport_expiration_date2']."',
			asset_information_last_year='".$_POST['asset_information_last_year']."',
			asset_information_2_years_ago='".$_POST['asset_information_2_years_ago']."',
			asset_information_3_years_ago='".$_POST['asset_information_3_years_ago']."',
			profit_information_last_year='".$_POST['profit_information_last_year']."',
			profit_information_2_years_ago='".$_POST['profit_information_2_years_ago']."',
			profit_information_3_years_ago='".$_POST['profit_information_3_years_ago']."',
			fatca='".$_POST['fatca']."',
			tin='".$_POST['tin']."',
			tin_issuance_country='".$_POST['tin_issuance_country']."',
			giin='".$_POST['giin']."',
			substantial_us_owner_name='".$_POST['substantial_us_owner_name']."',
			substantial_us_owner_address='".$_POST['substantial_us_owner_address']."',
			substantial_us_owner_tin='".$_POST['substantial_us_owner_tin']."',

			bank_bic_code1='".$_POST['bank_bic_code1']."',
			bank_bi_member_code1='".$_POST['bank_bi_member_code1']."',
			bank_name1='".$_POST['txt_bank_cab']."',
			bank_country1='".$_POST['bank_country1']."',
			bank_branch1='".$_POST['bank_branch1']."',
			acc_ccy1='".$_POST['acc_ccy1']."',
			acc_no1='".$_POST['txt_no_rek']."',
			acc_name1='".$_POST['txt_pemilik_rek']."',

			bank_bic_code2='".$_POST['bank_bic_code2']."',
			bank_bi_member_code2='".$_POST['bank_bi_member_code2']."',
			bank_name2='".$_POST['txt_bank_cab_2']."',
			bank_country2='".$_POST['bank_country2']."',
			bank_branch2='".$_POST['bank_branch2']."',
			acc_ccy2='".$_POST['acc_ccy2']."',
			acc_no2='".$_POST['txt_no_rek_2']."',
			acc_name2='".$_POST['txt_pemilik_rek_2']."',

			bank_bic_code3='".$_POST['bank_bic_code3']."',
			bank_bi_member_code3='".$_POST['bank_bi_member_code3']."',
			bank_name3='".$_POST['bank_name3']."',
			bank_country3='".$_POST['bank_country3']."',
			bank_branch3='".$_POST['bank_branch3']."',
			acc_ccy3='".$_POST['acc_ccy3']."',
			acc_no3='".$_POST['acc_no3']."',
			acc_name3='".$_POST['acc_name3']."',
			acc_type3='".$_POST['acc_type3']."',
			npwp_registration_date='".$_POST['npwp_registration_date']."',
			client_code = '".$_POST['client_code']."'
			
			WHERE cus_id = '".$_GET['id']."'";
		//print($sql);	
   # $data->showsql($sql);
   if($gotError){
		   echo "<script>alert('".$errorMessage."Save Failed.');</script>";
	   }else{
				if ($data->inpQueryReturnBool($sql))
				{	echo "<script>alert('".$data->err_report('s01')."');opener.document.form1.submit();window.parent.close();</script>";	}
				else
				{	
					echo $data->queryError();
					echo "<script>alert('".$data->err_report('s02')."');</script>";	}
	   }
}
#EDIT
if ($_GET['edit'] == 1)
{   #$data->auth('09030102',$_SESSION['user_id']);
	$rows = $data->selectQuery("select * from ".$tablename." where cus_id ='".$_GET['id']."'");
	echo $rows['cus_penghasilan'];
#print_r($_POST);
	$date = $_POST[txt_tgl_pt];
	$pecah1 = explode("-", $date);
	$date1 = $pecah1[2];
	$month = $pecah1[1];
	$year = $pecah1[0];
	$name = $_POST['txt_cus_name'];
	$pecah_name = substr($name, 0,1);
	$pecah_year = substr($year,2,2);
	$id = "$pecah_name$pecah_year$month ";
	
	//print($name);
	$npwp1 = substr($rows['cus_no_npwp'],0,2);
	$npwp2 = substr($rows['cus_no_npwp'],2,3);
	$npwp3 = substr($rows['cus_no_npwp'],5,3);
	$npwp4 = substr($rows['cus_no_npwp'],8,1);
	$npwp5 = substr($rows['cus_no_npwp'],9,3);
	$npwp6 = substr($rows['cus_no_npwp'],12,3);
	
	$cek_data=$data->get_value("select count(cus_id) from tbl_kr_cus_sup where left(cus_code,5) ='".$id."'");
	$cek_data = $cek_data + 1;
	if ($cek_data < 10 ){
	
	$no_auto= "0$cek_data";
		//print($no_auto);
	}
	else
	{
	$no_auto=$cek_data;

	}
	
	$id2 = "$pecah_name$pecah_year$month$no_auto";
	$dataRows = array (
				'TEXT' => array(
				'ID',
				'NAMA INSTITUSI <span class="redstar">*</span>',
				'TEMPAT <span class="redstar">*</span>',
				'NEGARA',
				'TGL. PENDIRIAN',
				'JENIS INSTITUSI',
				'STRUKTUR KEPIMILIKAN SAHAM',
				'BIDANG USAHA',
				'ALAMAT LENGKAP <span class="redstar">*</span>',
				'',
				'No. TELP. PERUSAHAAN',
				'EMAIL PERUSAHAAN',
				'NEGARA PERUSAHAAN',
				'STATUS DOMISILI KANTOR',
				'LAMA MENEMPATI',
				'ACTIVE',
				'STATEMENT TYPE <span class="redstar">*</span>',
				'BENEFICAL OWNER <span class="redstar">*</span>'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				"<input type=text name=txt_cus_id id=txt_cus_id value='".$rows[cus_code]."' ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_cus_name id=txt_cus_name  value='".$rows[cus_name]."''><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_tempat_pt id=txt_tempat_pt   value='".$rows[cus_tempat_pt]."' ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				$data->cb_isocountry('country_of_establisment', $rows[country_of_establisment] ,'')."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->datePicker('txt_tgl_pt', $rows[cus_tgl_pt],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_institusi('txt_cus_pt',$rows[cus_pt])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_shm('txt_struktur',$rows[cus_struktur])."<br>"
				.$data->cb_shm('txt_struktur_2',$rows[cus_struktur_2])."<td bgcolor='#DAECFF' width='200' class='mandatory'>
				
				Persen Kepemilikan Saham :
				<input type=text name=txt_persen_struktur id=txt_persen_struktur value='".$rows['persen_struktur']."'>%<br>
				Persen Kepemilikan Saham :
				<input type=text name=txt_persen_struktur_2 id=txt_persen_struktur_2 value='".$rows['persen_struktur_2']."'>%</td>",
				"<input type=text name=txt_bidang_usaha id=txt_bidang_usaha value='".$rows[cus_bidang_usaha]."'><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_alamat_pt id=txt_alamat_pt value='".$rows[cus_alamat_pt]."'><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"KOTA"
			//	 <input type=text name=txt_kota_pt id=txt_kota_pt value='".$rows[cus_kota_pt]."'>&nbsp;&nbsp;
					.$data->cb_kota('txt_kota_pt',$rows[cus_kota_pt])."
				</br>KODE POS
				 <input type=text name=txt_kode_pos_pt id=txt_kode_pos_pt value='".$rows[cus_kode_pos_pt]."'>&nbsp;&nbsp;
				 </br>PROPINSI
				 <input type=text name=txt_propinsi_pt id=txt_propinsi_pt value='".$rows[cus_propinsi_pt]."'><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_telp_pt id=txt_telp_pt value='".$rows[cus_telp_pt]."'>&nbsp;&nbsp;NO.FAX<input type=text name=txt_fax_pt id=txt_fax_pt value='".$rows[cus_fax_pt]."'><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_email_pt id=txt_email_pt value='".$rows[cus_email_pt]."' ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_isocountry('country_of_company',$rows[country_of_company])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_domi('txt_status_domisili_pt',$rows[cus_status_domisili_pt])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_lama_pt id=txt_lama_pt value='".$rows[cus_lama]."' ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_customer_status('txt_cus_actived',$rows[cus_actived])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->cb_statementtype('statement_type',$rows[statement_type])."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=benefical_owner id=benefical_owner value='".$beneficalowner."' ><td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
					));
				
			$dataRows2 = array (
				'TEXT' => array(
				'No. NPWP <span class="redstar">*</span>',
				'NPWP REGISTRATION DATE',
				'No. TDP',
				'TANGGAL KADARLUARSA TDP',
				'No. SIUPP/IJIN USAHA',
				'TANGGAL KADALUARSA SIUPP',
				'N. IJIN PMA',
				'TANGGAL KADALUARSA PMA',
				),

				'DOT'  => array (':',':',':',':',':',':',':'),
				'FIELD' => array (
				//"<input type=text name=txt_no_npwp id=txt_no_npwp value='".$rows[cus_no_npwp]."'>"."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_no_npwp1 id=txt_no_npwp1 size='2' onKeyPress='return checkIt(event)' maxlength='2' onKeyUp='valid_length1()' value='".$npwp1."'>.
				<input type=text name=txt_no_npwp2 id=txt_no_npwp2 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length2()' value='".$npwp2."'>.
				<input type=text name=txt_no_npwp3 id=txt_no_npwp3 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length3()' value='".$npwp3."'>.
				<input type=text name=txt_no_npwp4 id=txt_no_npwp4 size='2'  onKeyPress='return checkIt(event)' maxlength='1' onKeyUp='valid_length4()' value='".$npwp4."'>.
				<input type=text name=txt_no_npwp5 id=txt_no_npwp5 size='5'  onKeyPress='return checkIt(event)' maxlength='3' onKeyUp='valid_length5()' value='".$npwp5."'>.
				<input type=text name=txt_no_npwp6 id=txt_no_npwp6 size='5'  onKeyPress='return checkIt(event)' maxlength='3' value='".$npwp6."'>",
				"".$data->datePicker('npwp_registration_date', $rows[npwp_registration_date],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"<input type=text name=txt_no_tdp id=txt_no_tdp value='".$rows[cus_no_tdp]."'>"."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->datePicker('txt_tgl_tdp', $rows[cus_tgl_tdp],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'>
				Di Terbitkan di :
				<input type=text name=txt_tempat_tdp id=txt_tempat_tdp value='".$rows[cus_tempat_tdp]."'></td>",
				"<input type=text name=txt_no_siup id=txt_no_siup value='".$rows[cus_no_siup]."'>"."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				
				"".$data->datePicker('txt_tgl_siup', $rows[cus_tgl_siup],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'>
				Di Terbitkan di :
				<input type=text name=txt_tempat_siup id=txt_tempat_siup value='".$rows[cus_tempat_siup]."'></td>",
				
				"<input type=text name=txt_no_pma id=txt_no_pma value='".$rows[cus_no_pma]."'>"."<td bgcolor='#DAECFF' width='200' class='mandatory'></td>",
				"".$data->datePicker('txt_tgl_pma', $rows[cus_tgl_pma],'')."<td bgcolor='#DAECFF' width='200' class='mandatory'>
				Di Terbitkan di :
				<input type=text name=txt_tempat_pma id=txt_tempat_pma value='".$rows[cus_tempat_pma]."'></td>",
				),
          	 );		  
					  
			$dataRows3 = array (
				'TEXT' => array(
				'PEMEGANG SAHAM',
				'SUSUNAN DIREKSI/PENGURUS',
				'SUSUNAN KOMISARIS/PENGURUS',
				),
				'TEXT2' => array(
				'NO KTP',
				'NO KTP',
				'NO KTP',
				),
				'TEXT3' => array(
				'JUMLAH KEPEMILIKAN',
				'TANGGAL LAHIR',
				'TANGGAL LAHIR',
				),
				'TEXT4' => array(
				'',
				'JABATAN',
				'JABATAN',
				),
				'DOT'  => array (':',':',':'),
				'FIELD' => array (
				"1 :&nbsp;<input type=text name=txt_pemegang_saham id=txt_pemegang_saham value='".$rows['cus_pemegang_saham']."'></br>
				 2 :&nbsp;<input type=text name=txt_pemegang_saham_2 id=txt_pemegang_saham_2 value='".$rows['cus_pemegang_saham_2']."'></br>
				 3 :&nbsp;<input type=text name=txt_pemegang_saham_3 id=txt_pemegang_saham_3 value='".$rows['cus_pemegang_saham_3']."'>",
				 
				"1 :&nbsp;<input type=text name=txt_direksi id=txt_direksi value='".$rows['cus_direksi']."'></br>
				 2 :&nbsp;<input type=text name=txt_direksi_2 id=txt_direksi_2 value='".$rows['cus_direksi_2']."'></br>
				 3 :&nbsp;<input type=text name=txt_direksi_3 id=txt_direksi_3 value='".$rows['cus_direksi_3']."'>",
				 
    			"1 :&nbsp;<input type=text name=txt_komisaris id=txt_komisaris value='".$rows['cus_komisaris']."'></br>
				 2 :&nbsp;<input type=text name=txt_komisaris_2 id=txt_komisaris_2 value='".$rows['cus_komisaris_2']."'></br>
				 3 :&nbsp;<input type=text name=txt_komisaris_3 id=txt_komisaris_3 value='".$rows['cus_komisaris_3']."'>",
				),
				'FIELD2' => array (
				"<input type=text name=txt_pemegang_saham_ktp id=txt_pemegang_saham_ktp value='".$rows['cus_pemegang_saham_ktp']."'></br>
				 <input type=text name=txt_pemegang_saham_ktp_2 id=txt_pemegang_saham_ktp_2 value='".$rows['cus_pemegang_saham_ktp_2']."'></br>
				 <input type=text name=txt_pemegang_saham_ktp_3 id=txt_pemegang_saham_ktp_3 value='".$rows['cus_pemegang_saham_ktp_2']."'>",
				 
				"<input type=text name=txt_direksi_ktp id=txt_direksi_ktp value='".$rows['cus_direksi_ktp']."'></br>
				 <input type=text name=txt_direksi_ktp_2 id=txt_direksi_ktp_2 value='".$rows['cus_direksi_ktp_2']."'></br>
				 <input type=text name=txt_direksi_ktp_3 id=txt_direksi_ktp_3 value='".$rows['cus_direksi_ktp_3']."'>",
				 
    			"<input type=text name=txt_komisaris_ktp id=txt_komisaris_ktp value='".$rows['cus_komisaris_ktp']."'></br>
				 <input type=text name=txt_komisaris_ktp_2 id=txt_komisaris_ktp_2 value='".$rows['cus_komisaris_ktp_2']."'></br>
				 <input type=text name=txt_komisaris_ktp_3 id=txt_komisaris_ktp_3 value='".$rows['cus_komisaris_ktp_3']."'>",
				),
				'FIELD3' => array (
				"<input type=text name=txt_pemegang_saham_milik id=txt_pemegang_saham_milik value='".$rows['cus_pemegang_saham_milik']."'></br>
				 <input type=text name=txt_pemegang_saham_milik_2 id=txt_pemegang_saham_milik_2 value='".$rows['cus_pemegang_saham_milik_2']."'></br>
				 <input type=text name=txt_pemegang_saham_milik_3 id=txt_pemegang_saham_milik_3 value='".$rows['cus_pemegang_saham_milik_3']."'>",
				 
				 "".$data->datePicker('txt_direksi_tgl', $rows[cus_direksi_tgl],'')."
				 ".$data->datePicker('txt_direksi_tgl_2', $rows[cus_direksi_tgl_2],'')."
				 ".$data->datePicker('txt_direksi_tgl_3', $rows[cus_direksi_tgl_3],'')."",

				"".$data->datePicker('txt_komisaris_tgl', $rows[cus_komisaris_tgl],'')."
				 ".$data->datePicker('txt_komisaris_tgl_2', $rows[cus_komisaris_tgl_2],'')."
				 ".$data->datePicker('txt_komisaris_tgl_3', $rows[cus_komisaris_tgl_3],'')."",

				),
				'FIELD4' => array (
				"",
				"<input type=text name=txt_direksi_jbt id=txt_direksi_jbt value='".$rows['cus_direksi_jbt']."'></br>
				 <input type=text name=txt_direksi_jbt_2 id=txt_direksi_jbt_2 value='".$rows['cus_direksi_jbt_2']."'></br>
				 <input type=text name=txt_direksi_jbt_3 id=txt_direksi_jbt_3 value='".$rows['cus_direksi_jbt_3']."'>",
				 
    			"<input type=text name=txt_komisaris_jbt id=txt_komisaris_jbt value='".$rows['cus_komisaris_jbt']."'></br>
				 <input type=text name=txt_komisaris_jbt_2 id=txt_komisaris_jbt_2 value='".$rows['cus_komisaris_jbt_2']."'></br>
				 <input type=text name=txt_komisaris_jbt_3 id=txt_komisaris_jbt_3 value='".$rows['cus_komisaris_jbt_3']."'>",
				)
          	 );	

		$dataRows4 = array
		(
			'TEXT' => array(
				'Modal Saham Disetor(dalam Rp.)',
				'Total Aset :',
				'Total Kewajiban :',
				'Laba Bersih :',
				'Pendapatan Operasional :',
				'Pendapatan Non-Operasional :',
				'Tujuan Investasi <span class="redstar">*</span>'
				),
			'DOT'  => array (':',':',':'),
			'FIELD' => array
			(
				$data->cb_one('txt_modal_saham',$rows[cus_modal_saham]),
				$data->cb_one('txt_total_aset',$rows[cus_total_aset]),
				$data->cb_one('txt_tot_wajib',$rows[cus_tot_wajib]),
				$data->cb_two('txt_laba_bersih',$rows[cus_laba_bersih]),
				$data->cb_two('txt_pndp_opera',$rows[cus_pndp_opera]),
				$data->cb_two('txt_pndp_non_opera',$rows[cus_pndp_non_opera]),
				$data->cb_tujuan('txt_inves',$rows[cus_inves]),
			),
		);
		$dataRows5 = array
		(
				'TEXT' => array(
				'NAMA BANK DAN CABANG',
				),
				'TEXT2' => array(
				'NAMA PEMILIK REKENING',
				),
				'TEXT3' => array(
				'JENIS REKENING',
				),
				'TEXT4' => array(
				'No. REKENING',
				),
				'DOT'  => array (':'),
				'FIELD' => array (
				"1 :&nbsp;<input type=text name=txt_bank_cab id=txt_bank_cab value='".$rows['cus_bank_cab']."'></br>
				 2 :&nbsp;<input type=text name=txt_bank_cab_2 id=txt_bank_cab_2 value='".$rows['cus_bank_cab_2']."'>",
				),
				'FIELD2' => array (
				"&nbsp;<input type=text name=txt_pemilik_rek id=txt_pemilik_rek value='".$rows['cus_pemilik_rek']."'></br>
				 &nbsp;<input type=text name=txt_pemilik_rek_2 id=txt_pemilik_rek_2 value='".$rows['cus_pemilik_rek_2']."'>",
				),
				'FIELD3' => array (
				"&nbsp;<input type=text name=txt_jenis_rek id=txt_jenis_rek value='".$rows['cus_jenis_rek']."'></br>
				 &nbsp;<input type=text name=txt_jenis_rek_2 id=txt_jenis_rek_2 value='".$rows['cus_jenis_rek_2']."'>",
				),
				'FIELD4' => array (
				"&nbsp;<input type=text name=txt_no_rek id=txt_no_rek value='".$rows['cus_no_rek']."'></br>
				 &nbsp;<input type=text name=txt_no_rek_2 id=txt_no_rek_2 value='".$rows['cus_no_rek_2']."'>",
				),

		);
		$dataRows6 = array
		(
				/*'TEXT' => array(
				'NAMA',
				),
				'TEXT2' => array(
				'JABATAN',
				),
				'TEXT3' => array(
				'No. TELEPON/HANDPHONE',
				),
				'TEXT4' => array(
				'No. KTP/SIM/PASPOR/KIMS',
				),
				'DOT'  => array (':'),
				'FIELD' => array (
				"1 :&nbsp;<input type=text name=txt_nama_ id=txt_nama_ value='".$rows['cus_nama_']."'></br>
				 2 :&nbsp;<input type=text name=txt_nama_2 id=txt_nama_2 value='".$rows['cus_nama_2']."'>",
				),
				'FIELD2' => array (
				"&nbsp;<input type=text name=txt_jabatan id=txt_jabatan value='".$rows['cus_jabatan']."'></br>
				 &nbsp;<input type=text name=txt_jabatan_2 id=txt_jabatan_2 value='".$rows['cus_jabatan_2']."'>",
				),
				'FIELD3' => array (
				"&nbsp;<input type=text name=txt_telp_hp id=txt_telp_hp value='".$rows['cus_telp_hp']."'></br>
				 &nbsp;<input type=text name=txt_telp_hp_2 id=txt_telp_hp_2 value='".$rows['cus_telp_hp_2']."'>",
				),
				'FIELD4' => array (
				"&nbsp;<input type=text name=txt_no_ktp id=txt_no_ktp value='".$rows['cus_no_KTP']."'></br>
				 &nbsp;<input type=text name=txt_no_ktp_2 id=txt_no_ktp_2 value='".$rows['cus_no_KTP_2']."'>",
				),*/
				'TEXT' => array(
					'NAME',
					'JABATAN',
					'TELEPHONE',
					'EMAIL',
					'NPWP No.',
					'KTP No.',
					'KTP EXPIRATION DATE',
					'PASSPORT NO.',
					'PASSPORT EXPIRATION DATE'
				),
				'FIELD' => array(
					"<input type=text name=txt_nama_ id=txt_nama_ value='".$rows['cus_nama_']."'>
					<input type=text name=authorized_person_middle_name1 id=authorized_person_middle_name1 value='".$rows['authorized_person_middle_name1']."'>
					<input type=text name=authorized_person_last_name1 id=authorized_person_last_name1  value='".$rows['authorized_person_last_name1']."'>",
					"<input type=text name=txt_jabatan id=txt_jabatan value='".$rows['cus_jabatan']."'>",
					"<input type=text name=txt_telp_hp id=txt_telp_hp value='".$rows['cus_telp_hp']."'>",
					"<input type=text name=authorized_person_email1 id=authorized_person_email1 value='".$rows['authorized_person_email1']."'>",
					"<input type=text name=authorized_person_npwp1 id=authorized_person_npwp1 value='".$rows['authorized_person_npwp1']."'>",
					"<input type=text name=txt_no_ktp id=txt_no_ktp value='".$rows['cus_no_KTP']."'>",
					$data->datepicker('authorized_person_ktp_expiration_date1',$rows['authorized_person_ktp_expiration_date1'], ''),
					"<input type=text name=authorized_person_passport1 id=authorized_person_passport1 value='".$rows['authorized_person_passport1']."'>",
					$data->datepicker('authorized_person_passport_expiration_date1',$rows['authorized_person_passport_expiration_date1'], '')
				),
		);

		$dataRows7 = array
		(
				'TEXT' => array(
					'NAME',
					'JABATAN',
					'TELEPHONE',
					'EMAIL',
					'NPWP No.',
					'KTP No.',
					'KTP EXPIRATION DATE',
					'PASSPORT NO.',
					'PASSPORT EXPIRATION DATE'
				),
				'FIELD' => array(
					"<input type=text name=txt_nama_2 id=txt_nama_2 value='".$rows['cus_nama_2']."'>
				 <input type=text name=authorized_person_middle_name2 id=authorized_person_middle_name2 value='".$rows['authorized_person_middle_name2']."'>
					<input type=text name=authorized_person_last_name2 id=authorized_person_last_name2 value='".$rows['authorized_person_last_name2']."'>",
					"<input type=text name=txt_jabatan_2 id=txt_jabatan_2 value='".$rows['cus_jabatan_2']."'>",
					"<input type=text name=txt_telp_hp_2 id=txt_telp_hp_2 value='".$rows['cus_telp_hp_2']."'>",
					"<input type=text name=authorized_person_email2 id=authorized_person_email2 value='".$rows['authorized_person_email2']."'>",
					"<input type=text name=authorized_person_npwp2 id=authorized_person_npwp2 value='".$rows['authorized_person_npwp2']."'>",
					"<input type=text name=txt_no_ktp id=txt_no_ktp_2 value='".$rows['cus_no_KTP_2']."'>",
					$data->datepicker('authorized_person_ktp_expiration_date2',$rows['authorized_person_ktp_expiration_date2'], ''),
					"<input type=text name=authorized_person_passport2 id=authorized_person_passport2 value='".$rows['authorized_person_passport2']."'>",
					$data->datepicker('authorized_person_passport_expiration_date2',$rows['authorized_person_passport_expiration_date2'], '')
				),
		);

		$rowAssetInformation = array
		(
				'TEXT' => array(
					'LAST YEAR',
					'2 YEARS AGO',
					'3 YEARS AGO',
				),
				'FIELD' => array(
					$data->cb_assetinformation('asset_information_last_year',$rows['asset_information_last_year'],''),
					$data->cb_assetinformation('asset_information_2_years_ago',$rows['asset_information_2_years_ago'],''),
					$data->cb_assetinformation('asset_information_3_years_ago',$rows['asset_information_3_years_ago'],''),
				),
		);

		$rowProfitInformation = array
		(
				'TEXT' => array(
					'LAST YEAR',
					'2 YEARS AGO',
					'3 YEARS AGO',
				),
				'FIELD' => array(
					$data->cb_profitinformation('profit_information_last_year',$rows['asset_information_last_year'],''),
					$data->cb_profitinformation('profit_information_2_years_ago',$rows['asset_information_2_years_ago'],''),
					$data->cb_profitinformation('profit_information_3_years_ago',$rows['asset_information_3_years_ago'],''),
				),
		);

		$rowOther = array
		(
				'TEXT' => array(
					'FATCA',
					'TIN/ Foreign TIN',
					'TIN/ Foreign TIN Issuance Country',
					'Global Intermediary Identification Number',
					'Substantial U.S Owner Name',
					'Substantial U.S Owner Address',
					'Substantial U.S Owner TIN',
				),
				'FIELD' => array(
					$data->cb_fatcainstitusi('fatca',$rows['fatca'],''),
					"<input type=text name=tin id=tin value='".$rows['tin']."'>",
					$data->cb_isocountry('tin_issuance_country', $rows['tin_issuance_country'],''),
					"<input type=text name=giin id=giin value='".$rows['giin']."'>",
					"<input type=text name=substantial_us_owner_name id=substantial_us_owner_name value='".$rows['substantial_us_owner_name']."'>",
					"<input type=text name=substantial_us_owner_address id=substantial_us_owner_address value='".$rows['substantial_us_owner_address']."'>",
					"<input type=text name=substantial_us_owner_tin id=substantial_us_owner_tin value='".$rows['substantial_us_owner_tin']."'>",
				),
		);

		$rowRedmPayment = array
		(
				'TEXT' => array(
					'BANK BIC CODE 1',
					'BANK BI MEMBER CODE 1',
					'BANK NAME 1 <span class="redstar">*</span>',
					'BANK COUNTRY 1 <span class="redstar">*</span>',
					'BANK BRANCH 1',
					'ACCOUNT TYPE 1',
					'ACCOUNT CCY 1 <span class="redstar">*</span>',
					'ACCOUNT NO 1 <span class="redstar">*</span>',
					'ACCOUNT NAME 1 <span class="redstar">*</span>',
					'',
					'BANK BIC CODE 2',
					'BANK BI MEMBER CODE 2',
					'BANK NAME 2 <span class="redstar">*</span>',
					'BANK COUNTRY 2 <span class="redstar">*</span>',
					'BANK BRANCH 2',
					'ACCOUNT TYPE 2',
					'ACCOUNT CCY 2 <span class="redstar">*</span>',
					'ACCOUNT NO 2 <span class="redstar">*</span>',
					'ACCOUNT NAME 2 <span class="redstar">*</span>',
					'',
					'BANK BIC CODE 3',
					'BANK BI MEMBER CODE 3',
					'BANK NAME 3 <span class="redstar">*</span>',
					'BANK COUNTRY 3 <span class="redstar">*</span>',
					'BANK BRANCH 3',
					'ACCOUNT TYPE 3',
					'ACCOUNT CCY 3 <span class="redstar">*</span>',
					'ACCOUNT NO 3 <span class="redstar">*</span>',
					'ACCOUNT NAME 3 <span class="redstar">*</span>',
				),
				'FIELD' => array(
					"<input type=text name=bank_bic_code1 id=bank_bic_code1 value='".$rows['bank_bic_code1']."'>",
					//"<input type=text name=bank_bi_member_code1 id=bank_bi_member_code1 value='".$rows['bank_bi_member_code1']."'>",
					$data->cb_bimembercode('bank_bi_member_code1', $rows[bank_bi_member_code1], 'onchange="changeBi1(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_bank_cab id=txt_bank_cab value='".$rows['cus_bank_cab']."'>",
					$data->cb_isocountry('bank_country1',$rows['bank_country1'],''),
					"<input type=text name=bank_branch1 id=bank_branch1 value='".$rows['bank_branch1']."'>",
					"<input type=text name=txt_jenis_rek id=txt_jenis_rek value='".$rows['cus_jenis_rek']."'>",
					$data->cb_accountccy('acc_ccy1', $rows['acc_ccy1'],''),
					"<input type=text name=txt_no_rek id=txt_no_rek value='".$rows['cus_no_rek']."'>",
					"<input type=text name=txt_pemilik_rek id=txt_pemilik_rek value='".$rows['cus_pemilik_rek']."'>",
					'',
					"<input type=text name=bank_bic_code2 id=bank_bic_code2 value='".$rows['bank_bic_code2']."'>",
					//"<input type=text name=bank_bi_member_code2 id=bank_bi_member_code2 value='".$rows['bank_bi_member_code2']."'>",
					$data->cb_bimembercode('bank_bi_member_code2', $rows[bank_bi_member_code2], 'onchange="changeBi1(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=txt_bank_cab_2 id=txt_bank_cab_2 value='".$rows['cus_bank_cab_2']."'>",
					$data->cb_isocountry('bank_country2',$rows['bank_country2'],''),
					"<input type=text name=bank_branch2 id=bank_branch2 value='".$rows['bank_branch2']."'>",
					"<input type=text name=txt_jenis_rek_2 id=txt_jenis_rek_2 value='".$rows['cus_jenis_rek_2']."'>",
					$data->cb_accountccy('acc_ccy2', $rows['acc_ccy2'],''),
					"<input type=text name=txt_no_rek_2 id=txt_no_rek_2 value='".$rows['cus_no_rek_2']."'>",
					"<input type=text name=txt_pemilik_rek_2 id=txt_pemilik_rek_2 value='".$rows['cus_pemilik_rek_2']."'>",
					'',
					"<input type=text name=bank_bic_code3 id=bank_bic_code3 value='".$rows['bank_bic_code3']."'>",
					//"<input type=text name=bank_bi_member_code3 id=bank_bi_member_code3 value='".$rows['bank_bi_member_code3']."'>",
					$data->cb_bimembercode('bank_bi_member_code3', $rows[bank_bi_member_code3], 'onchange="changeBi1(this.options[this.selectedIndex].getAttribute(\'bankname\'), this.options[this.selectedIndex].getAttribute(\'account\') )"' ),
					"<input type=text name=bank_name3 id=bank_name3 value='".$rows['bank_name3']."'>",
					$data->cb_isocountry('bank_country3',$rows['bank_country3'],''),
					"<input type=text name=bank_branch3 id=bank_branch3 value='".$rows['bank_branch3']."'>",
					"<input type=text name=acc_type3 id=acc_type3 value='".$rows['acc_type3']."'>",
					$data->cb_accountccy('acc_ccy3', $rows['acc_ccy3'],''),
					"<input type=text name=acc_no3 id=acc_no3 value='".$rows['acc_no3']."'>",
					"<input type=text name=acc_name3 id=acc_name3 value='".$rows['acc_name3']."'>",
				),
		);

		
		################################################################
		$dataRowsAria = array (
				'TEXT' => array(
				'NOMOR SKD/AD/ART',
				'SKD/AD/ART Exp Date',
				'DOMISILI',
				'NEGARA DOMISILI <span class="redstar">*</span>',
				'TIPE <span class="redstar">*</span>',
				'KARAKTERISTIK <span class="redstar">*</span>',
				'SUMBER DANA <span class="redstar">*</span>',
				'PENGHASILAN <span class="redstar">*</span>',
				'ARTICLES OF ASSOCIATION No.',
				'INVESTORS RISK PROFILE',
				'CLIENT CODE'
				),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
					"<input type=text name=cus_no_skd id=cus_no_skd value='".$rows['cus_no_skd']."'>",
					$data->datePicker('skd_expiration_date', $rows[skd_expiration_date],''),
					$data->cb_domisili('txt_cus_domisili',$rows[cus_domisili]),
					$data->cb_isocountry('country_of_domicily',$rows[country_of_domicily]),
					$data->cb_tipe('txt_cus_tipe',$rows[cus_tipe]),
					$data->cb_karakteristik('txt_cus_karakteristik',$rows[cus_karakteristik]),
					$data->cb_sumber_dana_inst('txt_cus_sumber_dana',$rows[cus_sumber_dana]),
					$data->cb_penghasilan_insti('txt_cus_penghasilan',$rows[cus_penghasilan]),
                    "<input type=text name=articles_of_association_no id=articles_of_association_no size=40 maxlength=40 value='".$rows[articles_of_association_no]."'>",
					$data->cb_riskprofile('investors_risk_profile', $rows[investors_riks_profile]),
					"<input type=text name=client_code id=client_code maxlength=6 value='".$rows['client_code']."'>"
				)
			);
				
		#############################################################
    $tittle = "CUSTOMER / CLIENT EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"opener.document.form1.submit();window.parent.close();\">"
					);
}
$javascript = "<script type='text/javascript'>
	function changeBi1(name, account){
		document.getElementById('txt_bank_cab').value = name;
		
	}
	function changeBi2(name, account){
		document.getElementById('txt_bank_cab_2').value = name;
		
	}
	function changeBi3(name, account){
		document.getElementById('bank_name3').value = name;
		
	}
</script>";
	

	$tmpl->addRows('loopDataPayment',$DG);
	
	if($_GET['del']==1){
		$id = trim($_GET['id_del']);
		$sql = "Delete from tbl_kr_cus_reksadetail where pk_id='".$id."'";
		$data->inpQueryReturnBool($sql);
		
		//print($sql);
	
		if ($data->inpQueryReturnBool($sql)){
			echo "<script>alert('".$data->err_report('d01')."');window.location='contact_add_institusi.php?detail=1&id=".$_GET[id_con]."'</script>";
		}else{
			echo "<script>alert('".$data->err_report('d02')."');window.location='contact_add_institusi.php?detail=1&id=".$_GET[id_con]."'</script>";
		}
	}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
#$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));		
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('row2',$dataRows2 );
$tmpl->addVars('row3',$dataRows3 );
$tmpl->addVars('row4',$dataRows4 );
$tmpl->addVars('row5',$dataRows5 );
$tmpl->addVars('row6',$dataRows6 );
if($dataRows7 != null)
	$tmpl->addVars('row7',$dataRows7 );
if($rowAssetInformation != null)
$tmpl->addVars('rowAssetInformation',$rowAssetInformation );
if($rowProfitInformation != null)
$tmpl->addVars('rowProfitInformation',$rowProfitInformation );
if($rowOther != null)
$tmpl->addVars('rowOther',$rowOther );
if($rowRedmPayment != null)
$tmpl->addVars('rowRedmPayment',$rowRedmPayment );
$tmpl->addVars('rowaria',$dataRowsAria);
$tmpl->addVars('path',$path);
$tmpl->addVar('path', 'javascript', $javascript);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>