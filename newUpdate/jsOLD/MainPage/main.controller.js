app.controller('mainCtrl', ['$scope', '$state', function ($scope, $state) {
    $scope.coba = "hahahah";
    $scope.myData = [
        {
            firstName: "Cox",
            lastName: "Carney",
            company: "Enormo",
            employed: true
        },
        {
            firstName: "Lorraine",
            lastName: "Wise",
            company: "Comveyer",
            employed: false
        },
        {
            firstName: "Nancy",
            lastName: "Waters",
            company: "Fuelton",
            employed: false
        }
    ];

    $scope.new = function () {
        $state.go('product-form');
    }
}]); 