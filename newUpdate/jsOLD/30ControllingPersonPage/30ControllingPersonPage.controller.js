app.controller('30ControllingPersonPageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

  $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 $scope.id = [];
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ReportingFI', field: 'DocRefID_AccountReport' }, 
            { name: 'PrecedingTitle', field: 'PrecedingTitle' },
			{ name: 'Title', field: 'Title' },
			{ name: 'FirstName', field: 'FirstName' },
			{ name: 'FirstNameType', field: 'FirstNameType' },
			{ name: 'MiddleName', field: 'MiddleName' },
			{ name: 'MiddleNameType', field: 'MiddleNameType' },
			{ name: 'NamePrefixType', field: 'NamePrefixType' },
			{ name: 'LastName', field: 'LastName' },
			{ name: 'LastNameType', field: 'LastNameType' },
			{ name: 'GenerationIdentifier', field: 'GenerationIdentifier' },
			{ name: 'Suffix', field: 'Suffix' },
			{ name: 'GeneralSuffix', field: 'GeneralSuffix' },
			{ name: 'FirstName', field: 'FirstName' },
			{ name: 'NameType_Name_Individual', field: 'NameType_Name_Individual' },
            {
                name: 'Action', field: 'npwp_no',
                cellTemplate: '<button type="button" ng-click="grid.appScope.edit(row.entity)" class="btn btn-primary btn-sm" "> Edit </button> <button type="button" ng-click="grid.appScope.hapus(row.entity.id)" class="btn btn-primary btn-sm" "> Hapus </button>'
            }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
		if($scope.data.IsIndividual == 'true' || $scope.data.IsIndividual == '' ){
			return false;
		}
			$http({
            method: "POST",
            url: webservicesUrl + "/getAccRepName-Individual.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				$scope.data.idTempAccRep_Name_Individual = [];
				$scope.data.DocRefID_AccountReport_Name_Individual = [];
				$scope.data.PrecedingTitle = [];
				$scope.data.Title = [];
				$scope.data.FirstName = [];
				$scope.data.FirstNameType = [];
				$scope.data.MiddleName = [];
				$scope.data.MiddleNameType = [];
				$scope.data.NamePrefix = [];
				$scope.data.NamePrefixType = [];
				$scope.data.LastName = [];
				$scope.data.LastNameType = [];
				$scope.data.GenerationIdentifier = [];
				$scope.data.Suffix = [];
				$scope.data.GeneralSuffix = [];
				$scope.data.NameType_Name_Individual = [];			
				for(var i=0, length=data.records.length;i<length;i++){	
					$scope.data.idTempAccRep_Name_Individual.push(data.records[i].id);
					$scope.data.DocRefID_AccountReport_Name_Individual.push(data.records[i].DocRefID_AccountReport);
					$scope.data.PrecedingTitle.push('');
					$scope.data.Title.push('');
					$scope.data.FirstName.push(data.records[i].FirstName);
					$scope.data.FirstNameType.push('');
					$scope.data.MiddleName.push('');
					$scope.data.MiddleNameType.push('');
					$scope.data.NamePrefix.push('');
					$scope.data.NamePrefixType.push('');
					$scope.data.LastName.push('');
					$scope.data.LastNameType.push('');
					$scope.data.GenerationIdentifier.push('');
					$scope.data.Suffix.push('');
					$scope.data.GeneralSuffix.push('');
					$scope.data.NameType_Name_Individual.push('DJP202');			
				}
				//console.log($scope.data.DocRefID_AccountReport_TIN_individual);
					$scope.gridOptions.data = data.records;
					console.log($scope.gridOptions.data);
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
		//console.log($scope.data.Timestamp);
		//console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	$scope.hapus = function(Item){
		var data = { 'crud' : '3','id': Item, 'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/hapusAccRepTIN-Individual.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);

						$scope.data.DocRefID_AccountReport_TIN_individual =[];
		$scope.data.TIN = [];
		$scope.data.TINType_TIN_individual = [];
		$scope.data.IssuedBy_TIN_individual = [];	
		$scope.data.idTempAccRep = [];
				for(var i=0, length=data.records.length;i<length;i++){
					$scope.data.idTempAccRep.push(data.records[i].id);
					$scope.data.DocRefID_AccountReport_TIN_individual.push(data.records[i].DocRefID_AccountReport_TIN_individual);
					$scope.data.TIN.push(data.records[i].TIN);
					$scope.data.TINType_TIN_individual.push(data.records[i].TINType_TIN_individual);
					$scope.data.IssuedBy_TIN_individual.push(data.records[i].IssuedBy_TIN_individual);
				}				
				$scope.getListData();

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
		
		
		
	}	
	
    $scope.add = function () {
        $state.go('AccRepTIN-IndividualForm', { data: { crud: '1' } });
    }	
	
    $scope.edit = function (Item) {
		//alert(Item.id);
		//return false;
        $state.go('AccRepIN-OrganizationForm', { data: {crud: '2', id: Item.id, TIN:Item.TIN, DocRefID_AccountReport_TIN_individual:Item.DocRefID_AccountReport_TIN_individual, IssuedBy_TIN_individual : Item.IssuedBy_TIN_individual, TINType_TIN_individual :Item.TINType_TIN_individual  } });
		
	
		}		
	$scope.init = function(){
		$scope.getListData();
	}
	$scope.init();
	 
}]); 
