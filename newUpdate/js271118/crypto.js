/*
    need dependency aes.js, aes-ctr.js, json.js 
*/

var CRYPTO = new function () {
    this.privateKey = "{([<.?*+-#!,>])}";
    this.keyLength = 16;

    this.setPrivateKey = function (newKey) {
        this.privateKey = newKey;
    };

    this.generateKey = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < this.keyLength; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };
    this.encrypt = function (data) {
        var publicKey = this.generateKey();
        var enc = Aes.Ctr.encrypt(JSON.stringify(data), publicKey + this.privateKey, 256);
        var jsonData = publicKey + enc;
        return jsonData;
    };
    this.decrypt = function (respone) {

        var publicKey = respone.substring(0, 16);
        var data = respone.substring(16);
        var dec = Aes.Ctr.decrypt(data, publicKey + this.privateKey, 256);
        return JSON.parse(dec);
    };
}

function getQrCode(param) {
    var dt = new Date();
    var date = (parseInt(dt.getDate()) + 100).toString().substr(1, 2);
    var month = (parseInt(dt.getMonth()) + 101).toString().substr(1, 2);
    var year = dt.getFullYear();
    var str = param + "|" + year + month + date;
    var result = CRYPTO.encrypt(str);
    return result;
}