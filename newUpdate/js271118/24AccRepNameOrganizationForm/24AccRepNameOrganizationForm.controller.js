app.controller('24AccRepNameOrganizationFormCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

 
$scope.dataLocal ={
	DocRefID_AccountReport : '',
	crud : '1',
	Name : '',
	NameType : 'DJP207',
	id : '',
} 
 $scope.id = [];
  $scope.save = function(){
	  console.log($scope.dataLocal);
	  var data = { 'dataLocal' : $scope.dataLocal,'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/updateAccRepName-Organization.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);

				$scope.data.idTempAccRep_Name_Organization = [];
			$scope.data.DocRefID_AccountReport_Name_Organization = [];
		$scope.data.Name_Name_Organization = [];
		$scope.data.nameType_Name_Organization = []; 
				
				$scope.data.NameType_Name_Individual = [];			
				for(var i=0, length=data.records.length;i<length;i++){	
					$scope.data.idTempAccRep_Name_Organization.push(data.records[i].id);
					$scope.data.DocRefID_AccountReport_Name_Organization.push(data.records[i].DocRefID_AccountReport);
					$scope.data.Name_Name_Organization.push(data.records[i].Name);
					$scope.data.nameType_Name_Organization.push('DJP207'); 				
				}
				$state.go('AccRepNameOrganizationPage');

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
	  
	  
	  
  } 

  $scope.init = function(){ 
  console.log($stateParams.data);
	    if ($stateParams.data != null) {
			console.log($stateParams.data);
            //$scope.dataLocal = $scope.data;
			//$scope.dataLocal = JSON.parse(JSON.stringify($scope.data));
			if($stateParams.data.id != null){
				//alert('123');
	$scope.dataLocal.DocRefID_AccountReport = $stateParams.data.DocRefID_AccountReport;
	$scope.dataLocal.Name = $stateParams.data.Name;
	$scope.dataLocal.NameType = $stateParams.data.NameType;
	$scope.dataLocal.crud = $stateParams.data.crud;
	$scope.dataLocal.id = $stateParams.id
	
			}	
        } else {
            $state.go('homepage');
        }
	  
  }
  $scope.init();
	 
}]); 
