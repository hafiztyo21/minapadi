app.controller('20AccountReportPageCtrl', ['$state', '$scope', '$http', '$timeout', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants,$stateParams) {

    $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 
	 
	 
	 
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ReportingFI', field: 'DocRefID_ReportingFI' },
            { name: 'DocTypeIndic_AccountReport', field: 'DocTypeIndic_AccountReport' },
			{ name: 'DocRefID_AccountReport', field: 'DocRefID_AccountReport' },
			{ name: 'CorrMessageRefID_AccountReport', field: 'CorrMessageRefID_AccountReport' },
			
			{ name: 'CorrDocRefID_AccountReport', field: 'CorrDocRefID_AccountReport' },
			{ name: 'AccountNumber', field: 'AccountNumber' },
			{ name: 'AccountNumberType', field: 'AccountNumberType' },
			{ name: 'UndocumentedAccount', field: 'UndocumentedAccount' },
			{ name: 'ClosedAccount', field: 'ClosedAccount' },
			{ name: 'DormantAccount', field: 'DormantAccount' },
			{ name: 'IsIndividual', field: 'IsIndividual' },
			{ name: 'AcctHolderType', field: 'AcctHolderType' },
			{ name: 'ResCountryCode', field: 'ResCountryCode' },
			{ name: 'Nationality', field: 'Nationality' },
			{ name: 'CurrencyCodeAccountBalance', field: 'CurrencyCodeAccountBalance' },
			{ name: 'AccountBalance', field: 'AccountBalance' },
			{ name: 'BirthInfo_BirthDate', field: 'BirthInfo_BirthDate' },
			{ name: 'BirthInfo_City', field: 'BirthInfo_City' },
			{ name: 'BirthInfo_CountryCode', field: 'BirthInfo_CountryCode' },
            {
                name: 'Action', field: 'npwp_no',
                cellTemplate: '<button type="button" ng-click="grid.appScope.edit(row.entity)" class="btn btn-primary btn-sm" "> Edit </button>'
            }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
	$scope.localData[0] = $scope.data;	

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
		console.log($scope.data.Timestamp);
		console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	
    $scope.edit = function (data) {
        $state.go('AccountReportForm', { data: { npwp_no: data.npwp_no } });
    }	
	
	//$scope.init = function(){
	//	$scope.getListData();
	//}
	//$scope.init();
	
	 
}]); 
