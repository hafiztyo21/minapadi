app.controller('33CtrlgPersonNamePageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {
   $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 
	 /*
		DocRefID_ControllingPerson_cpn : '',
		PrecedingTitle_cpn : '',
		Title_cpn : '',
		FirstName_cpn : '',
		FirstNameType_cpn : '',
		MiddleName_cpn : '',
		MiddleNameType_cpn :'',
		NamePrefix_cpn : '',
		NamePrefixType_cpn : '',
		LastName_cpn : '',
		LastNameType_cpn : '',
		GenerationIdentifier_cpn :'',
		Suffix_cpn : '',
		GeneralSuffix_cpn : '',
		NameType_cpn :'',
	 
	 */
	 
	 
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ControllingPerson', field: 'DocRefID_ControllingPerson_cpn' },
            { name: 'PrecedingTitle', field: 'PrecedingTitle_cpN' },
			{ name: 'Title', field: 'Title_cpn' },
			{ name: 'FirstName', field: 'FirstName_cpn' },
			{ name: 'FirstNameType', field: 'FirstNameType_cpn' },
			{ name: 'MiddleName', field: 'MiddleName_cpn' },
			{ name: 'MiddleNameType', field: 'MiddleNameType_cpn' },
			{ name: 'NamePrefix', field: 'NamePrefix_cpn' },
			{ name: 'NamePrefixType', field: 'NamePrefixType_cpn' },
			{ name: 'LastName', field: 'LastName_cpn' },
			{ name: 'LastNameType', field: 'LastNameType_cpn' },
			{ name: 'GenerationIdentifier', field: 'GenerationIdentifier_cpn' },
			{ name: 'Suffix', field: 'Suffix_cpn' },
			{ name: 'GeneralSuffix', field: 'GeneralSuffix_cpn' },
			{ name: 'NameType_Name_Individual', field: 'NameType_Name_Individual_cpn' },
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
	$scope.localData[0] = $scope.data;	

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	$scope.gridOptions.data[0].DocRefID_ControllingPerson_cp = $scope.data.DocRefID_ControllingPerson_cp[0];
	console.log($scope.data.DocRefID_ControllingPerson_cp)
	
	}
	
	$scope.refresh = function(){
		if($scope.data.IsIndividual == 'true' ||  $scope.data.IsIndividual == '' ){
			return false;
			
			
		}		
		
		console.log($scope.data);
		$scope.getListData();
		
		
		//console.log($scope.data.Timestamp);
		console.log($scope.gridOptions.data);
		console.log($scope.data);	
	}	
	$scope.init = function(){
		if($scope.data.IsIndividual == 'true' ||  $scope.data.IsIndividual == '' ){
			return false;
			
			
		}		
		//alert('123');
		
		$scope.getListData();
	}
	$scope.init();
	 
}]); 
