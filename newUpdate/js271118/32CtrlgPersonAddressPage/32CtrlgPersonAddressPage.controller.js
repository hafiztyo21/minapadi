app.controller('32CtrlgPersonAddressPageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {
   $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 
	 /*
		DocRefID_ControllingPerson_cpa : '',
		legalAddressType_cpa : '',
		CountryCode_cpa : '',
		AddressFree_cpa : '',
		Street_cpa : '',
		BuildingIdentifier_cpa : '',
		SuiteIdentifier_cpa : '',
		FloorIdentifier_cpa : '',
		DistrictName_cpa :'',
		POB_cpa : '',
		PostCode_cpa : '',
		City_cpa : '',
		CountrySubentity_cpa : '',
	 
	 */
	 
	 
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ControllingPerson', field: 'DocRefID_ControllingPerson_cpa' },
            { name: 'legalAddressType', field: 'legalAddressType_cpa'},
			{ name: 'CountryCode', field: 'CountryCode_cpa'},
			{ name: 'AddressFree', field: 'AddressFree_cpa' },
			{ name: 'Street', field: 'Street_cpa ' },
			{ name: 'BuildingIdentifier', field: 'BuildingIdentifier_cpa' },
			{ name: 'SuiteIdentifier', field: 'SuiteIdentifier_cpa' },
			{ name: 'FloorIdentifier', field: 'FloorIdentifier_cpa' },
			{ name: 'DistrictName', field: 'DistrictName_cpa' },
			{ name: 'City', field: 'City_cpa' },
			{ name: 'PostCode', field: 'PostCode_cpa' },
			{ name: 'CountrySubentity', field: 'CountrySubentity_cpa ' }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
	$scope.localData[0] = $scope.data;	

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	$scope.gridOptions.data[0].DocRefID_ControllingPerson_cp = $scope.data.DocRefID_ControllingPerson_cp[0];
	console.log($scope.data.DocRefID_ControllingPerson_cp)
	
	}
	
	$scope.refresh = function(){
		if($scope.data.IsIndividual == 'true' ||  $scope.data.IsIndividual == '' ){
			return false;
			
			
		}		
		
		console.log($scope.data);
		$scope.getListData();
		
		
		//console.log($scope.data.Timestamp);
		console.log($scope.gridOptions.data);
		console.log($scope.data);	
	}	
	$scope.init = function(){
		if($scope.data.IsIndividual == 'true' ||  $scope.data.IsIndividual == '' ){
			return false;
			
			
		}		
		//alert('123');
		$scope.data.DocRefID_ControllingPerson_cpa = $scope.data.DocRefID_ControllingPerson_cpt;
		$scope.data.legalAddressType_cpa = 'DPJ303';
		$scope.data.CountryCode_cpa = 'ID';
		$scope.getListData();
	}
	$scope.init();
	 
}]); 
