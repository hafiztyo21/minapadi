app.controller('21AccRepTIN-IndividualFormCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

$scope.dataLocal ={
	crud : '1',
	id : '',
	DocRefID_AccountReport : '',
	TIN : '',
	IssuedBy : 'ID',
	TINType : '0',
} 
 $scope.id = [];
  $scope.save = function(){
	  var data = { 'dataLocal' : $scope.dataLocal,'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/updateAccRepTIN-Individual.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);

						$scope.data.DocRefID_AccountReport_TIN_individual =[];
		$scope.data.TIN = [];
		$scope.data.TINType_TIN_individual = [];
		$scope.data.IssuedBy_TIN_individual = [];	
		$scope.data.idTempAccRep = [];
				for(var i=0, length=data.records.length;i<length;i++){
					$scope.data.idTempAccRep.push(data.records[i].id);
					$scope.data.DocRefID_AccountReport_TIN_individual.push(data.records[i].DocRefID_AccountReport_TIN_individual);
					$scope.data.TIN.push(data.records[i].TIN);
					$scope.data.TINType_TIN_individual.push(data.records[i].TINType_TIN_individual);
					$scope.data.IssuedBy_TIN_individual.push(data.records[i].IssuedBy_TIN_individual);
				}				
				$state.go('AccRepTIN-IndividualPage');

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
	  
	  
	  
  } 

  $scope.init = function(){ 
	    if ($stateParams.data != null) {
			console.log
            //$scope.dataLocal = $scope.data;
			//$scope.dataLocal = JSON.parse(JSON.stringify($scope.data));
			if($stateParams.data.id != null){
				//alert('123');
				$scope.dataLocal.crud = $stateParams.data.crud;
				$scope.dataLocal.id = $stateParams.data.id;
				$scope.dataLocal.DocRefID_AccountReport = $stateParams.data.DocRefID_AccountReport_TIN_individual;
				$scope.dataLocal.TIN = $stateParams.data.TIN;
				$scope.dataLocal.IssuedBy = $stateParams.data.IssuedBy_TIN_individual;
				$scope.dataLocal.TINType = $stateParams.data.TINType_TIN_individual;
			}	
        } else {
            $state.go('homepage');
        }
	  
  }
  $scope.init();
	  
	
	 
}]); 
