app.controller('13RepF1AddressPageCtrl', ['$state', '$scope', '$http', '$timeout', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants,$stateParams) {

    $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 
	 
	 
	 
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ReportingFI', field: 'DocRefID_ReportingFI' },
            { name: 'legalAddressType', field: 'legalAddressType' },
			{ name: 'CountryCode', field: 'CountryCode' },
			{ name: 'AddressFree', field: 'AddressFree' },
			{ name: 'Street', field: 'Street' },
			{ name: 'BuildingIdentifier', field: 'BuildingIdentifier' },
			{ name: 'SuiteIdentifier', field: 'SuiteIdentifier' },
			{ name: 'FloorIdentifier', field: 'FloorIdentifier' },
			{ name: 'DistrictName', field: 'DistrictName' },
			{ name: 'POB', field: 'POB' },
			{ name: 'PostCode', field: 'PostCode' },
			{ name: 'City', field: 'City' },
			{ name: 'CountrySubentity', field: 'CountrySubentity' }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
	$scope.localData[0] = $scope.data;	

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
		console.log($scope.data.Timestamp);
		console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	
    $scope.edit = function (data) {
        $state.go('RepF1INForm', { data: { npwp_no: data.npwp_no } });
    }	
	
	//$scope.init = function(){
	//	$scope.getListData();
	//}
	//$scope.init();
	
	 
}]); 
