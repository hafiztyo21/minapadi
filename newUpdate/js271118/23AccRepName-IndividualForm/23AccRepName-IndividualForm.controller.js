app.controller('23AccRepName-IndividualFormCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

$scope.dataLocal ={
	DocRefID_AccountReport : '',
	crud : '1',
	FirstName : '',
	NameType_Name_Individual : 'DJP202',
	id : '',
} 
 $scope.id = [];
  $scope.save = function(){
	  console.log($scope.dataLocal);
	  var data = { 'dataLocal' : $scope.dataLocal,'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/updateAccRepName-Individual.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);

		$scope.data.idTempAccRep_Name_Individual = [];
		$scope.data.DocRefID_AccountReport_Name_Individual = [];
		$scope.data.NameType_Name_Individual = [];
		$scope.data.FirstName = [];		
				for(var i=0, length=data.records.length;i<length;i++){	
					$scope.data.idTempAccRep_Name_Individual.push(data.records[i].id);
					$scope.data.DocRefID_AccountReport_Name_Individual.push(data.records[i].DocRefID_AccountReport);
					$scope.data.FirstName.push(data.records[i].FirstName);
					$scope.data.NameType_Name_Individual.push('DJP202');			
				}		
				$state.go('AccRepName-IndividualPage');

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
	  
	  
	  
  } 

  $scope.init = function(){ 
  console.log($stateParams.data);
	    if ($stateParams.data != null) {
			console.log($stateParams.data);
            //$scope.dataLocal = $scope.data;
			//$scope.dataLocal = JSON.parse(JSON.stringify($scope.data));
			if($stateParams.data.id != null){
				//alert('123');
	$scope.dataLocal.DocRefID_AccountReport = $stateParams.data.DocRefID_AccountReport;
	$scope.dataLocal.FirstName = $stateParams.data.FirstName;
	$scope.dataLocal.LastName = $stateParams.data.LastName;
	$scope.dataLocal.id = $stateParams.data.id;
	$scope.data.NameType_Name = $stateParams.data.NameType_Name_Individual;
	$scope.dataLocal.crud = $stateParams.data.crud;
	
			}	
        } else {
            $state.go('homepage');
        }
	  
  }
  $scope.init();
	  
	
	 
}]); 
