app.controller('000MessageSpecPageCtrl', ['$state', '$scope', '$http', '$timeout', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants,$stateParams) {

    $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'SendingCompanyIN', field: 'npwp_no' },
            { name: 'TransmittingCountry', field: 'TransmittingCountry' },
			{ name: 'ReceivingCountry', field: 'ReceivingCountry' },
			{ name: 'MessageType', field: 'MessageType' },
			{ name: 'Warning', field: 'Warning' },
			{ name: 'Contact', field: 'Contact' },
			{ name: 'MessageRefId', field: 'MessageRefId' },
			{ name: 'MessageTypeIndic', field: 'MessageTypeIndic' },
			{ name: 'CorrMessageRefId', field: 'CorrMessageRefId' },
			{ name: 'ReportingPeriod', field: 'ReportingPeriod' },
			{ name: 'TimeStamp', field: 'Timestamp' },
            {
                name: 'Action', field: 'npwp_no',
                cellTemplate: '<button type="button" ng-click="grid.appScope.edit(row.entity)" class="btn btn-primary btn-sm" "> Edit </button>'
            }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
	$scope.localData[0] = $scope.data;	

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
	//	console.log($scope.data.Timestamp);
	//	console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	
    $scope.edit = function (data) {
        $state.go('MessageSpecForm', { data: { npwp_no: data.npwp_no } });
    }	
	
	$scope.init = function(){
		$scope.getListData();
	}
	$scope.init();
	
	 
}]); 
