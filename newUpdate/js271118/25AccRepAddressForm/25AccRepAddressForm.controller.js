app.controller('25AccRepAddressFormCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {
 
$scope.dataLocal ={
	DocRefID_AccountReport : '',
	legalAddressType : '',
	CountryCode : 'ID',
	AddressFree : '',
	crud : '1',
	id : '',
} 
 $scope.id = [];
  $scope.save = function(){
	  console.log($scope.dataLocal);
	  var data = { 'dataLocal' : $scope.dataLocal,'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/updateAccRepAddress.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				console.log(data);

		$scope.data.DocRefID_AccountReport_AccRepAddress =[];
		$scope.data.legalAddressType_AccRepAddress =[];
		$scope.data.CountryCode_AccRepAddress =[];
		$scope.data.AddressFree_AccRepAddress =[];
		$scope.data.idTemp_AccRepAddress = [];
				
				$scope.data.NameType_Name_Individual = [];			
				for(var i=0, length=data.records.length;i<length;i++){	
					
		$scope.data.DocRefID_AccountReport_AccRepAddress.push(data.records[i].DocRefID_AccountReport);
		$scope.data.legalAddressType_AccRepAddress.push(data.records[i].legalAddressType);
		$scope.data.CountryCode_AccRepAddress.push('ID');
		$scope.data.AddressFree_AccRepAddress.push(data.records[i].AddressFree);
		$scope.data.idTemp_AccRepAddress.push(data.records[i].id);
					
				}
			//console.log($scope.data.DocRefID_AccountReport_AccRepAddress);
			$state.go('AccRepAddressPage');

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
	  
	  
	  
  } 

  $scope.init = function(){ 
  console.log($stateParams.data);
	    if ($stateParams.data != null) {
			console.log($stateParams.data);
            //$scope.dataLocal = $scope.data;
			//$scope.dataLocal = JSON.parse(JSON.stringify($scope.data));
			if($stateParams.data.id != null){
				//alert('123');
	$scope.dataLocal.DocRefID_AccountReport = $stateParams.data.DocRefID_AccountReport;
	$scope.dataLocal.legalAddressType = $stateParams.data.legalAddressType;
	$scope.data.CountryCode = $stateParams.data.CountryCode;
	$scope.dataLocal.AddressFree = $stateParams.data.AddressFree;
	$scope.dataLocal.crud = $stateParams.data.crud;
	$scope.dataLocal.id = $stateParams.data.id;
	
			}	
        } else {
            $state.go('homepage');
        }
	  
  }
  $scope.init();
	 
	 
}]); 
