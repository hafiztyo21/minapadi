<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");
	session_start();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));

$data = $param_POST->data;
	
$TINType_TIN_individual 				= array();
$DocRefID_AccountReport_TIN_individual 	= array();
$TIN									= array();
	
$DocRefID_AccountReport_TIN_individual = $data->DocRefID_AccountReport_TIN_individual;
$TIN = $data->TIN;
$TINType_TIN_individual = $data->TINType_TIN_individual;
$DocTypeIndic_AccountReport  = $data->DocTypeIndic_AccountReport ;



//print_r($DocRefID_AccountReport_TIN_individual);
//print_r(count($DocRefID_AccountReport_TIN_individual));
$outp='';

if($DocTypeIndic_AccountReport != ''){
	if(count($data->DocRefID_AccountReport_TIN_individual) > 0 ){
		$ii = count($data->DocRefID_AccountReport_TIN_individual);
		for($i=0;$i<$ii;$i++){
			$IssuedBy_TIN_individual[$i] = 'ID'; 
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"DocRefID_AccountReport_TIN_individual":"'.$DocRefID_AccountReport_TIN_individual[$i].'",';
			$outp .= '"id":"'.$i.'",';
			$outp .= '"TIN":"'.$TIN[$i].'",';
			$outp .= '"TINType_TIN_individual":"'.$TINType_TIN_individual[$i].'",';
			$outp .= '"IssuedBy_TIN_individual":"'.$IssuedBy_TIN_individual[$i].'"}'; 				
		}
		
		
		
	}
	else{

		$DocRefID_AccountReport_TIN_individual[0] = $DocTypeIndic_AccountReport;
		$TINType_TIN_individual[0] = '';
		$TIN[0] = '';
		$IssuedBy_TIN_individual[0] = 'ID'; 
		$id = 0;
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"DocRefID_AccountReport_TIN_individual":"'.$DocRefID_AccountReport_TIN_individual[0].'",';
			$outp .= '"id":"'.$id.'",';
			$outp .= '"TIN":"'.$TIN[0].'",';
			$outp .= '"TINType_TIN_individual":"'.$TINType_TIN_individual[0].'",';
			$outp .= '"IssuedBy_TIN_individual":"'.$IssuedBy_TIN_individual[0].'"}'; 	
	}

}

/*
if($DocRefID_AccountReport != ''){
	$TINType_TIN_individual 				= array();
	$DocRefID_AccountReport_TIN_individual 	= array();
	$TIN									= array();
	
	$TINType_TIN_individual[0] = $DocRefID_AccountReport;
	$TINType_TIN_individual[0] = '';
	$TIN[0] = '';
	}

*/
//print_r($data);
			//print_r($outp);
	$result = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	print_r($result);

?>
