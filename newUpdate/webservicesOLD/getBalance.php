<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");
	session_start();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));


	/*-- DECRYPT --*/

	//$data = (empty($_POST['data']))?trim($param_POST->data):trim($_POST['data']);
	//$Public_Key = substr($data,0,16);
	//$dec_data = CryptoAES::decrypt(substr($data,16), $Public_Key.$Private_Key, 256);
	//$data = json_decode($dec_data);
	$data = $param_POST->data;
	$fund_code = $data->fund_code;
	$from = $data->date_from;
	$to = $data->date_to;
	$to2 = $to;
	$pk_id = $data->id;
	$identitas = $data->identitas;
	//print_r($data);
	
	if($data->typeNasabah == 'true'){
			$sql = "SELECT X.*, Z.balance, Z.totalunit FROM 
(
SELECT ifua_code FROM tbl_ifua_balance
WHERE fund_code='".$fund_code."' AND  tradingdate>='".$from."' AND tradingdate<='".$to."'  AND IFNULL(endingbalance ,0)>0
GROUP BY ifua_code
) Y LEFT JOIN
(
SELECT A.cus_ins_id n_id, A.ifua_code, A.ifua_name, B.cus_code, B.cus_name, B.cus_pt TYPE, B.cus_struktur STATUS, B.CUS_NO_KTP ktp, B.cus_ins_sid sid , 0 isIndividu, B.cus_tgl_pt tgl,'' tempatlahir, cus_bank_cab bank, cus_no_rek rek, cus_pemilik_rek rek_name,
cus_direksi, cus_direksi_ktp, cus_direksi_tgl, cus_direksi_jbt
FROM  tbl_kr_cus_institusi_ifua  A LEFT JOIN tbl_kr_cus_institusi B
ON A.cus_ins_id=B.cus_id
UNION ALL
SELECT A.cus_id n_id, A.ifua_code, A.ifua_name, B.cus_code, B.cus_name, B.Cus_national TYPE, B.cus_status_domisili STATUS, b.cus_no_identity ktp, B.cus_sid sid, 1 isIndividu, B.cus_tgl_lahir tgl,B.cus_tempat_lahir tempatlahir,  cus_bank_name bank, cus_account_number rek, cus_account_name  rek_name,
'' cus_direksi, '' cus_direksi_ktp, '' cus_direksi_tgl, '' cus_direksi_jbt
FROM tbl_kr_cus_sup_ifua A LEFT JOIN tbl_kr_cus_sup B
ON A.cus_id=B.cus_id
WHERE A.is_deleted=0
) X
ON Y.ifua_code=X.ifua_code 
LEFT JOIN (
SELECT  ifua_code,  endingbalance *(SELECT nab_now/10000 FROM tbl_kr_cash WHERE allocation='".$pk_id."' AND create_dt='".$to."'  ) balance, endingbalance totalunit
FROM tbl_ifua_balance 
WHERE tradingdate='".$to."' AND fund_code='".$fund_code."'
) Z ON Y.ifua_code=Z.ifua_code WHERE X.ktp = '".$identitas."'
						";
			$stmnt = $conn->query($sql);
		
					
		//print_r($conn->query($sql));
	}
	
	elseif($data->typeNasabah == 'false'){
		
			$sql = "SELECT X.*, Z.balance, Z.totalunit FROM 
(
SELECT ifua_code FROM tbl_ifua_balance
WHERE fund_code='".$fund_code."' AND  tradingdate>='".$from."' AND tradingdate<='".$to."'  AND IFNULL(endingbalance ,0)>0
GROUP BY ifua_code
) Y LEFT JOIN
(
SELECT A.cus_ins_id n_id, A.ifua_code, A.ifua_name, B.cus_code, B.cus_name, B.cus_pt TYPE, B.cus_struktur STATUS, B.CUS_NO_KTP , B.cus_ins_sid sid , 0 isIndividu, B.cus_tgl_pt tgl,'' tempatlahir, cus_bank_cab bank, cus_no_rek rek, cus_pemilik_rek rek_name,
cus_direksi, cus_direksi_ktp, cus_direksi_tgl, cus_direksi_jbt,cus_no_npwp
FROM  tbl_kr_cus_institusi_ifua  A LEFT JOIN tbl_kr_cus_institusi B
ON A.cus_ins_id=B.cus_id
UNION ALL
SELECT A.cus_id n_id, A.ifua_code, A.ifua_name, B.cus_code, B.cus_name, B.Cus_national TYPE, B.cus_status_domisili STATUS, b.cus_no_identity ktp, B.cus_sid sid, 1 isIndividu, B.cus_tgl_lahir tgl,B.cus_tempat_lahir tempatlahir,  cus_bank_name bank, cus_account_number rek, cus_account_name  rek_name,
'' cus_direksi, '' cus_direksi_ktp, '' cus_direksi_tgl, '' cus_direksi_jbt,'' cus_no_npwp
FROM tbl_kr_cus_sup_ifua A LEFT JOIN tbl_kr_cus_sup B
ON A.cus_id=B.cus_id
WHERE A.is_deleted=0
) X
ON Y.ifua_code=X.ifua_code 
LEFT JOIN (
SELECT  ifua_code,  endingbalance *(SELECT nab_now/10000 FROM tbl_kr_cash WHERE allocation='".$pk_id."' AND create_dt='".$to."'  ) balance, endingbalance totalunit
FROM tbl_ifua_balance 
WHERE tradingdate='".$to."' AND fund_code='".$fund_code."'
) Z ON Y.ifua_code=Z.ifua_code WHERE X.cus_no_npwp = '".$identitas."'
						";
			$stmnt = $conn->query($sql);		
		
		
		
	}	
	
	
	$outp = "";

	
	if(!mysqli_error($conn)){
	while($row=mysqli_fetch_array($stmnt)){
		//echo $row['allocation'];
			if ($outp != "") {$outp .= ",";}
		
				
				
			$outp .= '{"AccountBalance":"'.$row['balance'].'",';
			$outp .= '"BirthInfo_BirthDate":"'.$row['tgl'].'",'; //
			$outp .= '"cus_name":"'.$row['cus_name'].'",';
			$outp .= '"BirthInfo_City":"'.$row['tempatlahir'].'"}'; 				
		
	};		
	//print_r($outp);
	$result = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	//print_r($result);
	}
	else{
		$result = '{ "status":"no", "message":"'.mysqli_error($conn).'", "records":[]}';
		
	}
	print_r($result);	
	//die(mysqli_error($db));

	//$Public_Key = getRandomKey(16);
	//$chiper = CryptoAES::encrypt($result, $Public_Key.$Private_Key, 256);
	//echo('{"data":'.json_encode($Public_Key.$chiper).'}');
	$conn = null;	
	
	
?>
