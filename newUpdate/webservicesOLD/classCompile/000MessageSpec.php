<?php


class MessageSpace
{
	public function genDate($date){
		
		$arrayDate = explode(" ",$date);
		$date = $arrayDate[1]."-".$arrayDate[2]."-".$arrayDate[3];
		$date = date('Y-m-d', strtotime($date));		
		
		
		return $date;
	}
	
	
	public function generateDate($from,$to){
		$myDate = array();
		$from 	= $this->genDate($from);
		$to 	= $this->genDate($to);
		array_push($myDate,$from);
		array_push($myDate,$to);
		return $myDate ;
	}
	
	
	public function generateNPWP($data,$conn){
		$myNpwpList = array();
		$sql = "SELECT npwp_no,fund_name,billing_address1,fund_code FROM tbl_kr_allocation WHERE pk_id = '".$data."'";
		$stmnt = $conn->query($sql);
		//print_r($stmnt);
		if(!mysqli_error($conn)){
		while($row=mysqli_fetch_array($stmnt)){
			
				$npwp = $row['npwp_no'];
				$fundName = $row['fund_name'];
				$bil_add = explode(',',$row['billing_address1']);
				$fund_code = $row['fund_code'];
		};		
		}
		else{
			$datas = 'ERROR :'.mysqli_error($conn).'"';
			//print_r($result);	
			//return false;
		}		
			
			array_push($myNpwpList,$npwp);
			array_push($myNpwpList,$fundName);
			array_push($myNpwpList,$bil_add[1]);
			array_push($myNpwpList,$fund_code);
			return $myNpwpList;
	}

	public function generateMessageRefId($id,$dateto,$npwp,$conn){
		//get ID Number peralocation
		$sql = "SELECT count(v_allocationcode) n_code FROM tbl_kr_eoi WHERE v_allocationcode = '".$id."'";
		$stmnt = $conn->query($sql);
		//print_r($stmnt);
		if(!mysqli_error($conn)){
		while($row=mysqli_fetch_array($stmnt)){
				$n_code = $row['n_code'];
				if($n_code > 0 ){
					$n_code = $n_code + 1;
				}
				else{
					$n_code = 1;
				}				
			}	
		}
		else{
			$datas = 'ERROR :'.mysqli_error($conn).'"';
			//print_r($result);	
			return $datas;
			//return false;
			
		}

		//generate no doc
		if(($n_code > 0 && $n_code < 10 ))
			{
				$gen_n_code = '00'.$n_code;
			}
		elseif($n_code > 9 && $n_code <  100 ){
			$gen_n_code = '0'.$n_code;
		}
		elseif($n_code >100 && $n_code < 1000 ){
			$gen_n_code = $n_code;
		}
		else{
			$gen_n_code = 'OUTOFDOC';
		}
		
		//get Years
		$year = explode('-',$dateto);
		$year = $year[0];
		$datas = "ID".$year."_".$npwp."_".$gen_n_code;
		
		return $datas;
		
		
	}
	
	
	public function generateTimeStamp($date){
		$dateto = date('Y-m-d', strtotime($date));
		$time = 'T'.date('h:m:s');
		$dateto = $dateto.$time;
		return $dateto;
		
		
		
	}

}



?>