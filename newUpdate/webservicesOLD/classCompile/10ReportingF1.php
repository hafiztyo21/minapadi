<?php


class ReportingF1
{	
	public function generateDocRefID_ReportingFI($data){
		$proses = explode('_',$data);
		$n_code = intval($proses[2]);
		
		if(($n_code > 0 && $n_code < 10 ))
			{
				$gen_n_code = '0000'.$n_code;
			}
		elseif($n_code > 9 && $n_code <  100 ){
			$gen_n_code = '000'.$n_code;
		}
		elseif($n_code >100 && $n_code < 1000 ){
			$gen_n_code = '00'.$n_code;
		}
		elseif($n_code >999 && $n_code < 100000 ){
			$gen_n_code = $n_code;
		}		
		else{
			$gen_n_code = 'OUTOFDOC';
		}
		$DocRefID_ReportingFI = $data.'_CI_F1_'.$gen_n_code; 
	
		return $DocRefID_ReportingFI ;
	}
	
	

}



?>