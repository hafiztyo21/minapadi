<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");
	session_start();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));

$data = $param_POST->data;


//print_r($data);
$dataGlobal = $data->dataGlobal;

$id = $data->id;

$count = count($dataGlobal->DocRefID_AccountReport_TIN_individual);

$iBaru = $count - 1;

$TINType_TIN_individual 				= array();
$DocRefID_AccountReport_TIN_individual 	= array();
$TIN									= array();
$IssuedBy_TIN_individual				= array();
$j = 0;
$outp = '';
for($i=0 ; $i<$count; $i++){
	
	if($i != $id){
		
		$DocRefID_AccountReport_TIN_individual[intval($j)] = $dataGlobal->DocRefID_AccountReport_TIN_individual[intval($i)];
		$TIN[intval($j)] = $dataGlobal->TIN[intval($i)];
		$TINType_TIN_individual[intval($j)] = $dataGlobal->TINType_TIN_individual[intval($i)];
		$IssuedBy_TIN_individual[intval($j)] = 'ID';		

			if ($outp != "") {$outp .= ",";}
			$outp .= '{"DocRefID_AccountReport_TIN_individual":"'.$DocRefID_AccountReport_TIN_individual[intval($j)].'",';
			$outp .= '"id":"'.$j.'",';
			$outp .= '"TIN":"'.$TIN[intval($j)].'",';
			$outp .= '"TINType_TIN_individual":"'.$TINType_TIN_individual[intval($j)].'",';
			$outp .= '"IssuedBy_TIN_individual":"'.$IssuedBy_TIN_individual[intval($j)].'"}'; 		
		$j++;
		
	}
	else{
		$x= '';
		
	}	
}


	
	$result = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	print_r($result);
?>
