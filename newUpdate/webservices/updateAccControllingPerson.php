<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");
	session_start();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));

$data = $param_POST->data;


		/*
	$scope.data.DocRefID_AccountReport_cp = [];
$scope.data.DocRefID_ControllingPerson_cp = [];
$scope.data.ResCountryCode_cp = [];
$scope.data.Nationality_cp = [];
$scope.data.BirthInfo_BirthDate_cp = [];
$scope.data.BirthInfo_City_cp = [];
$scope.data.BirthInfo_CountryCode_cp = [];
$scope.data.CtrlPersonType_cp = [];
$scope.data.idTemp_cp = [];
																																										
		*/

$dataLocal = $data->dataLocal;
$dataGlobal = $data->dataGlobal;
//print_r($dataLocal);

$DocRefID_AccountReport = array();
$DocRefID_ControllingPerson= array();
$ResCountryCode = array();
$Nationality = array();
$BirthInfo_BirthDate = array();

$BirthInfo_City = array();
$BirthInfo_CountryCode = array();
$CtrlPersonType = array();

/*
$scope.data.DocRefID_AccountReport_cp = [];
$scope.data.DocRefID_ControllingPerson_cp = [];
$scope.data.ResCountryCode_cp = [];
$scope.data.Nationality_cp = [];
$scope.data.BirthInfo_BirthDate_cp = [];
$scope.data.BirthInfo_City_cp = [];
$scope.data.BirthInfo_CountryCode_cp = [];
$scope.data.CtrlPersonType_cp = [];
*/

$outp = '';
$DocRefID_AccountReport = $dataGlobal->DocRefID_AccountReport_cp;
$DocRefID_ControllingPerson= $dataGlobal->DocRefID_ControllingPerson_cp;
$ResCountryCode= $dataGlobal->ResCountryCode_cp;
$Nationality =$dataGlobal->Nationality_cp;
$BirthInfo_BirthDate= $dataGlobal->BirthInfo_BirthDate_cp;
$BirthInfo_City =$dataGlobal->BirthInfo_City_cp;
$BirthInfo_CountryCode = $dataGlobal->BirthInfo_CountryCode_cp;
$CtrlPersonType = $dataGlobal->CtrlPersonType_cp;

$i = count($DocRefID_AccountReport);
if($dataLocal->crud == '1' ){
	

$DocRefID_AccountReport[$i] = $dataLocal->DocRefID_AccountReport;
$DocRefID_ControllingPerson[$i]= $dataLocal->DocRefID_ControllingPerson;
$ResCountryCode[$i]= $dataLocal->ResCountryCode;
$Nationality[$i] =$dataLocal->Nationality;
$BirthInfo_BirthDate[$i]= $dataLocal->BirthInfo_BirthDate;
$BirthInfo_City[$i] =$dataLocal->BirthInfo_City;
$BirthInfo_CountryCode[$i] = $dataLocal->BirthInfo_CountryCode;
$CtrlPersonType[$i] = $dataLocal->CtrlPersonType;





//$dataGlobal->Name_Name_Organization[$i] = $dataLocal->Name;
//print_r($DocRefID_AccountReport);

$ii = $i + 1;
//echo $ii;
		$outp = '';
		for($i=0;$i<$ii;$i++){


			if ($outp != "") {$outp .= ",";}
			$outp .= '{"DocRefID_AccountReport":"'.$DocRefID_AccountReport[$i].'",';			
			$outp .= '"id":"'.$i.'",';
			$outp .= '"DocRefID_ControllingPerson":"'.$DocRefID_ControllingPerson[$i].'",';
			$outp .= '"ResCountryCode":"'.$ResCountryCode[$i].'",';
			$outp .= '"Nationality":"'.$Nationality[$i].'",';
			$outp .= '"BirthInfo_BirthDate":"'.$BirthInfo_BirthDate[$i].'",';
			$outp .= '"BirthInfo_City":"'.$BirthInfo_City[$i].'",';
			$outp .= '"BirthInfo_CountryCode":"'.$BirthInfo_CountryCode[$i].'",';
			$outp .= '"CtrlPersonType":"'.$CtrlPersonType[$i].'"}'; 			
		}		
}
elseif($dataLocal->crud == '2'){
$x = $dataLocal->id;
$DocRefID_AccountReport[$x] = $dataLocal->DocRefID_AccountReport;
$DocRefID_ControllingPerson[$x]= $dataLocal->DocRefID_ControllingPerson;
$ResCountryCode[$x]= $dataLocal->ResCountryCode;
$Nationality[$x] =$dataLocal->Nationality; 
$BirthInfo_BirthDate[$x]= $dataLocal->BirthInfo_BirthDate;
$BirthInfo_City[$x] =$dataLocal->BirthInfo_City;
$BirthInfo_CountryCode[$x] = $dataLocal->BirthInfo_CountryCode;
$CtrlPersonType[$x] = $dataLocal->CtrlPersonType;
		$outp = '';
		for($ie=0;$ie<$i;$ie++){
					//	echo $dataGlobal->FirstName[$i].'-';
		
			//echo $dataGlobal->TINType_TIN_individual[$ie];
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"DocRefID_AccountReport":"'.$DocRefID_AccountReport[$ie].'",';			
			$outp .= '"id":"'.$ie.'",';
			$outp .= '"DocRefID_ControllingPerson":"'.$DocRefID_ControllingPerson[$ie].'",';
			$outp .= '"ResCountryCode":"'.$ResCountryCode[$ie].'",';
			$outp .= '"Nationality":"'.$Nationality[$ie].'",';
			$outp .= '"BirthInfo_BirthDate":"'.$BirthInfo_BirthDate[$ie].'",';
			$outp .= '"BirthInfo_City":"'.$BirthInfo_City[$ie].'",';
			$outp .= '"BirthInfo_CountryCode":"'.$BirthInfo_CountryCode[$ie].'",';
			$outp .= '"CtrlPersonType":"'.$CtrlPersonType[$ie].'"}'; 					
		}	

	
}



	$result = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	print_r($result);

?>
