<?php
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
set_time_limit(0);
ini_set('memory_limit','1050M');
		$param_POST = json_decode(file_get_contents("php://input"));
$data = $param_POST->data;
//print_r($data->AddressFree_AccRepAddress);
//print_r($data);

require_once 'plugins/PHPExcel-1.8.1/Classes/PHPExcel.php';
require_once 'plugins/PHPExcel-1.8.1/Classes/PHPExcel/Shared/ZipStreamWrapper.php';
$filename = 'coba123.xlsx';


$objPHPExcel = new PHPExcel();

/*--000MessageSpec--*/
    $sheet = $objPHPExcel->getActiveSheet();

    //Start adding next sheets

      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(0); //Setting index when creating

      //Write cells
      $objWorkSheet->setCellValue('A1', 'NPWPLembagaKeuanganPengirim')
      ->setCellValue('B1', 'NPWPLembagaKeuanganPelapor')
      ->setCellValue('C1', 'IdentitasUnik')
      ->setCellValue('D1', 'JenisData')
      ->setCellValue('E1', 'IdentitasUnikKoreksi')
      ->setCellValue('F1', 'JenisLembagaKeuangan')
      ->setCellValue('G1', 'NomorCIF')
      ->setCellValue('H1', 'NomorRekening')
      ->setCellValue('I1', 'NomorCIF+NomorRekening')
      ->setCellValue('J1', 'StsRekening')
      ->setCellValue('K1', 'JnsPemegangRekening')
      ->setCellValue('L1', 'MataUang')
      ->setCellValue('M1', 'SaldoatauNilai')
      ->setCellValue('N1', 'Deviden')
      ->setCellValue('O1', 'Bunga')
      ->setCellValue('P1', 'PhBruto')
      ->setCellValue('Q1', 'PhLainnya')
      ->setCellValue('R1', 'NamaPemegangRek')
      ->setCellValue('S1', 'NamaLainPemegangRek')
      ->setCellValue('T1', 'NPWPPemegangRek')
      ->setCellValue('U1', 'NIKPemegangRek')
      ->setCellValue('V1', 'SIMPemegangRek')
      ->setCellValue('W1', 'PasporPemegangRek')
      ->setCellValue('X1', 'SIUPPemegangRek')
      ->setCellValue('Y1', 'SITUPemegangRek')
      ->setCellValue('Z1', 'AktaPemegangRek')
      ->setCellValue('AA1', 'KewarganegaraanPemegangRek')
      ->setCellValue('AB1', 'TempatLahirPemegangRek')
      ->setCellValue('AC1', 'TglLahirPemegangRek')
      ->setCellValue('AD1', 'AlamatDOMPemegangRek')
      ->setCellValue('AE1', 'AlamatUsahaPemegangRek')
      ->setCellValue('AF1', 'AlamatKorespondensiPemegangRek');
			
   $objWorkSheet->getStyle('A1:AF1')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 	

$objWorkSheet->getStyle('N2:N5000')->getNumberFormat()->setFormatCode('#,##0.00');

$objWorkSheet->getStyle('O2:O5000')->getNumberFormat()->setFormatCode('#,##0.00');
            	
$objWorkSheet->getStyle('P2:P5000')->getNumberFormat()->setFormatCode('#,##0.00');
           
$objWorkSheet->getStyle('Q2:Q5000')->getNumberFormat()->setFormatCode('#,##0.00');	

$objWorkSheet->getStyle('M2:Q5000')->getNumberFormat()->setFormatCode('#,##0.00');


$objWorkSheet->getStyle('Q2:Q5000')->getNumberFormat()->setFormatCode('#,##0.00');

	/* $objWorkSheet->getStyle('A2:B10000')
            ->getNumberFormat()
            ->setFormatCode(
                PHPExcel_Style_NumberFormat::FORMAT_NUMBER
            );	*/
            $objWorkSheet->getStyle('M2:B10000')
            ->getNumberFormat()
            ->setFormatCode(
                PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
            );	
/*
$objPHPExcel->getSheet(0)->getStyle('A2:K3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(0)->getStyle('A2:K3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/
	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('G')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('H')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('I')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('J')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('K')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('L')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('M')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('N')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('O')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('P')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('Q')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('R')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('S')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('T')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('U')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('V')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('W')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('X')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('Y')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('Z')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('AA')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('AB')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('AC')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('AD')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('AE')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('AF')
    ->setAutoSize(true);
                    $brs=2;
                    $x=count($data->AccountNumber);
                   // echo $x;
                    $datetos = $data->date_tos;
                    $nourut=0;
					$totalbalance = 0;
                    $cif_entitas=array();
                    //print_r($data->cus_name);
				//	$objPHPExcel->getActiveSheet()->setCellValueExplicit('A1', '1234567890', PHPExcel_Cell_DataType::TYPE_STRING);
                    for($i=0; $i<$x; $i++) {
                        $namacus= utf8_decode(urldecode($data->ifua_name[$i]));
						$find_= array("/",",",".","&");
						$replace_= array(" ");
						$barunamacus= str_replace($find_,$replace_,$namacus);
                      //  echo $data->cus_name[$i];
                      //  ECHO $i;
                        $nopwp_=$data->npwp_no;
                        $idd=$data->idd[$i];
                        $kodeunik = explode('T', $datetos);
                        $nounik = explode('-', $kodeunik[0]);
                        $variabelunik= sprintf("1%06s", $nourut);
						$variabelunik1= sprintf("%07s", $nourut);
                        $nourut++;
                        $CIF_= explode('_', $data->AccountNumber[$i]);
                        $CIF= $CIF_[0];
                        $NO_REK=utf8_decode(urldecode($CIF_[1]));
						$find_2= array("/",",",".","&", " ");
						$replace_2= array("");
						$rangerek = str_replace($find_2,$replace_2,$NO_REK);
                        $NO_CIF= $CIF.$NO_REK;
						$find_1= array("/",",",".","&", " ");
						$replace_1= array("");
						$rangecif = str_replace($find_1,$replace_1,$NO_CIF);
                        $matauang= "IDR";
                        $nopwP= $data->npwp_no;
                        
                        $combinevar=$nounik[0].$nopwP.$variabelunik;
						$combinevar1=$nounik[0].$nopwP.$variabelunik1;
						
                   $aktif="01";
                   $tanggallhr= utf8_decode(urldecode($data->BirthInfo_BirthDate[$i])); 
                 //  ($data->BirthInfo_BirthDate[$i]);
                $tglformat= strtotime($tanggallhr);
                   $tgllhr = date('d-m-Y', $tglformat);
                   $jenis_data= $data->IsIndividual[$i];
                   if($jenis_data=='FALSE') {
                    $tgllhr = '';
                   } else{
                    $tgllhr = date('d-m-Y', $tglformat);
                   }
                   $tmptlhr = utf8_decode(urldecode($data->BirthInfo_City[$i]));
                   
									$a= $data->IsIndividual[$i];
									//$b= $data->IsIndividual;
									$indi= '';
									//$mynpwp_X = $data->mynpwp[$i];
									$mynpwp =$data->mynpwp[$i];
											
									
									
                if($a=='TRUE') {
						
						$indi= 'INDIVIDUAL';
						$mynpwp_X = $nopwp_;
									 
								}else{
									$indi='ENTITAS';
									$mynpwp_X = $data->mynpwp[$i];
								}
                
                //  if($nopwP==''){
                   // $nopwP = isset($nopwP) ? $nopwP : '000000000000000';
                 // }
                 // if($a) {

								//		$a = isset($a) ? $a: $data->cus_no_npwp;
										

							//		}  

                 // $saldo=$data->AccountBalance[$i];
                  $deviden=0.00;
                  $bunga=0.00;
                  $phBruto=0.00;
                  $phlainnya=0.00;
				  
                  $addressPem= utf8_decode(urldecode($data->AddressFree_AccRepAddress[$i]));
				  $find_3= array("/",",",".","&","-");
				$replace_3= array(" ");
				  $rangealamat = str_replace($find_3,$replace_3,$addressPem);
                  $alamatusaha='';
				$alamatkores='';
									//$namapem= $data->FirstName[$i];
									//$yy=0;
									//if(!ISSET($data->FirstName[$i])){
										
                                    //$namespace=	utf8_decode(urldecode($data->Name_Name_Organization[$yy]));
                                    //$yy++;
                                   // }
                                   // else{
                                     //   $namespace=$data->FirstName[$i].' '.$data->MiddleNameType[$i].' '.$data->LastName[$i];
                                    //}
                                   // $namacus= utf8_decode(urldecode($data->cus_name));
                                    //echo $namacus;
                                    $accountbalance_ = (double)$data->AccountBalance[$i];

									
									$totalbalance += $accountbalance_;
 			$objWorkSheet->setCellValue("A$brs",$nopwp_);
			//$objWorkSheet->setCellValue("B$brs",$nopwp_);
			$objWorkSheet->setCellValue("B$brs",$nopwp_);
							//		 $objWorkSheet->getStyle('B2:B10000')
							//	->getNumberFormat()
							//	->setFormatCode(
							//		PHPExcel_Style_NumberFormat::FORMAT_NUMBER
//            );
				$djp=$data->DocTypeIndic1;
				//$djp1=$data->DocTypeIndic1;
				
				if($djp=='DJP1')
				 {
					$noidentitasunik=$combinevar1;
					$koreksi=null;
				 }
				 if($djp=='DJP2') {
				 $noidentitasunik=$combinevar;
				  $koreksi=$combinevar1;
				  }
				  $unikkode = utf8_decode(urldecode($data->IsIndividual[$i]));
                       if($unikkode !='TRUE'){
                           array_push($cif_entitas,$noidentitasunik);
//$cif_entitas[$i];
                       }
				 
				
                 $objWorkSheet->setCellValue("C$brs", "$noidentitasunik");
                // $indentitasunik='';
 				$objWorkSheet->setCellValue("D$brs", "$data->DocTypeIndic1");
 				$objWorkSheet->setCellValue("E$brs", "$koreksi");
 				$objWorkSheet->setCellValue("F$brs", "IE");
 				$objWorkSheet->setCellValue("G$brs", "$CIF");
 				//$objWorkSheet->setCellValue("H$brs", "$NO_REK");	
				$objWorkSheet->setCellValueExplicit("H$brs",$rangerek, PHPExcel_Cell_DataType::TYPE_STRING);				
 				$objWorkSheet->setCellValue("I$brs","$rangecif");			
                 $objWorkSheet->setCellValue("J$brs", $aktif);
                 $objWorkSheet->setCellValue("K$brs", "$indi");	
                 $objWorkSheet->setCellValue("L$brs", "$matauang");
				 //setCellValueExplicit('A1', '1234567890', PHPExcel_Cell_DataType::TYPE_STRING);
                 //$objWorkSheet->setCellValueExplicit("M$brs",$accountbalance_, PHPExcel_Cell_DataType::TYPE_STRING);
				 $objWorkSheet->setCellValue("M$brs",$accountbalance_);
                 $objWorkSheet->setCellValue("N$brs", $deviden);
                 $objWorkSheet->setCellValue("O$brs", $bunga);
                 $objWorkSheet->setCellValue("P$brs", $phBruto);
                 $objWorkSheet->setCellValue("Q$brs", $phlainnya);
                 $objWorkSheet->setCellValue("R$brs","$barunamacus");
                $objWorkSheet->setCellValue("S$brs", "");
                // $objWorkSheet->setCellValue("T$brs", $idd);
				 $objWorkSheet->setCellValue("T$brs", $mynpwp);
                $objWorkSheet->setCellValue("U$brs", "");
               $objWorkSheet->setCellValue("V$brs", "");
                 $objWorkSheet->setCellValue("W$brs","");
                 $objWorkSheet->setCellValue("X$brs", "");
                 $objWorkSheet->setCellValue("Y$brs", "");
                 $objWorkSheet->setCellValue("Z$brs", "");
                 $objWorkSheet->setCellValue("AA$brs", "ID");
                $objWorkSheet->setCellValue("AB$brs", "$tmptlhr");
                 $objWorkSheet->setCellValue("AC$brs", "$tgllhr");
                 $objWorkSheet->setCellValue("AD$brs","$rangealamat");
                 $objWorkSheet->setCellValue("AE$brs", "");
                 $objWorkSheet->setCellValue("AF$brs", "");
                $brs++;
            }
            
            $objPHPExcel->getActiveSheet()->getStyle("B1")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $objWorkSheet->setTitle("DataRekening");
                
      // Rename sheet
     
	  
/*--000MessageSpec--*/
$objWorkSheet = $objPHPExcel->createSheet(1); //Setting index when creating

//Write cells
$objWorkSheet->setCellValue('A1', 'IdentitasUnik')
  ->setCellValue('B1', 'Nama_CP')
  ->setCellValue('C1', 'KodeNegara_CP')
  ->setCellValue('D1', 'TempatLahir_CP')
  ->setCellValue('E1', 'TglLahir_CP')
  ->setCellValue('F1', 'NPWPTIN_CP')
  ->setCellValue('G1', 'NIK_CP')
  ->setCellValue('H1', 'SIM_CP')
  ->setCellValue('I1', 'PASSPORT_CP')
  ->setCellValue('J1', 'AlamatDomisili_CP')
  ->setCellValue('K1', 'AlamatKorespodensi_CP');
  
$objWorkSheet->getStyle('A1:K1')
  ->getAlignment()
  ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
  
/*
$objPHPExcel->getSheet(1)->getStyle('A2:F3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(1)->getStyle('A2:F3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

$objWorkSheet
->getColumnDimension('A')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('B')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('C')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('D')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('E')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('F')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('G')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('H')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('I')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('J')
->setAutoSize(true);
$objWorkSheet
->getColumnDimension('K')
->setAutoSize(true);

/*
DocRefID_ReportingFI : '',
ResCountryCode : 'ID',
DocTypeIndic1  : '',
CorrDocRefID : '',
*/
          $brs=2;
          $x=count($data->DocRefID_ControllingPerson_cpn);
         // echo $x;
          $is_entitas=array();

          $datetos_ = $data->date_tos;
          $nourut_=0;
                for($i=0; $i<$x; $i++) {
                    if($tmptlhr==''){
                        $tmptlhr = isset($tmptlhr) ? $tmptlhr : '';
                      }
            $kodeunik_ = explode('T', $datetos_);
            $nounik_ = explode('-', $kodeunik_[0]);
            $variabelunik_= sprintf("%07s", $nourut_);
            $nourut_++;
            $person_cpa=$data->DocRefID_ControllingPerson_cpa;
            $Firstname_cpn=utf8_decode(urldecode($data->FirstName_cpn[$i]));
            $CodeCountry=$data->CountryCode_cpa[$i];
            $AddressCpt=$data->AddressFree_cpa[$i];
			 $find_4= array("/",",",".","&");
			 $replace_4= array(" ");
			 $rangealamatcpt = str_replace($find_4,$replace_4,$AddressCpt);
            $TinCpt= $data->TIN_cpt[$i];
            $middlenamecpn=utf8_decode(urldecode($data->MiddleName_cpn[$i]));
            $lastname_cpn=utf8_decode(urldecode($data->LastName_cpn[$i]));
            $combinevar_=$nounik_[0].$nopwP.$variabelunik_;
           // $jenisdata =  count($data->IsIndividual[$i]);
           $tmptlhr_cpn=$data->BirthInfo_City_cp[$i];
            $combinename=$Firstname_cpn." ".$middlenamecpn." ".$lastname_cpn;
			$find= array("/",",",".","&");
			$replace= array(" ");
			$pemisahchar= str_replace($find,$replace,$combinename);
			
           //if($jenisdata[$i] == "FALSE" ){
            //array_push($is_entitas,$jenisdata[$i]);
            //}
			
			$tglcpn= utf8_decode(urldecode($data->BirthInfo_BirthDate_cp[$i])); 
			
			//$= $data->BirthInfo_BirthDate_cp[$i];
			$tgl_format= strtotime($tglcpn);
            $tl_lhrcpn = date('d-m-Y',$tgl_format);
           // $=$tgl_format;
             $objWorkSheet->setCellValue("A$brs",  "$cif_entitas[$i]");
             $objWorkSheet->setCellValue("B$brs", "$pemisahchar");
             $objWorkSheet->setCellValue("C$brs", "$CodeCountry");
             $objWorkSheet->setCellValue("D$brs", "$tmptlhr_cpn");
              $objWorkSheet->setCellValue("E$brs", "$tl_lhrcpn");
              $objWorkSheet->setCellValue("F$brs", "");
              $objWorkSheet->setCellValue("G$brs", "$TinCpt");
              $objWorkSheet->setCellValue("H$brs", "");			
              $objWorkSheet->setCellValue("I$brs", "");			
             $objWorkSheet->setCellValue("J$brs", "$rangealamatcpt");
             $objWorkSheet->setCellValue("K$brs", "");	
            $brs++;
        }


// Rename sheet
$objWorkSheet->setTitle("PengendalianEntitas");
$objWorkSheet = $objPHPExcel->createSheet(2); //Setting index when creating


//
//$objWorkSheet = $objPHPExcel->createSheet(2); //Setting index when creating
/*

		IN : '',
		IssuedBy : 'ID',
		INType : 'ID06',

*/
      //Write cells
  $objWorkSheet->setCellValue('A1', 'JumlahDataRekening')
            ->setCellValue('B1', 'JumlahDataPengendalianEntitas')
            ->setCellValue('C1', 'JumlahNilaiSaldo');
            $objWorkSheet->getStyle('A1:C1')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 		
            $objWorkSheet->getStyle('C2:B10000')
            ->getNumberFormat()
            ->setFormatCode(
                PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
            );	
			
/*
$objPHPExcel->getSheet(2)->getStyle('A2:D3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(2)->getStyle('A2:D3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	
$objWorkSheet->getStyle('C2:Q5000')->getNumberFormat()->setFormatCode('#,##0.00');

                    $brs=2;   
                $docrefid = count($data->DocRefID_ControllingPerson_cpa);
                $accountrek = count($data->AccountNumber);
				
                //$rpIDR = count($data->AccountBalance[$i]);
                $objWorkSheet->setCellValue("A$brs", "$accountrek");
                $objWorkSheet->setCellValue("B$brs", "$docrefid");
				 $objWorkSheet->setCellValue("C$brs", "$totalbalance");
				////	$objPHPExcel->getActiveSheet()->setCellValueExplicit('A1', '1234567890', PHPExcel_Cell_DataType::TYPE_STRING);
            //    $objWorkSheet->setCellValueExplicit("C$brs", $totalbalance, PHPExcel_Cell_DataType::TYPE_STRING);
	
	

      // Rename sheet
      $objWorkSheet->setTitle("DataDomestikInduk"); 
 
//33 CtrlgPerson Name--//
 /*

 */
 
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=$filename");
header('Cache-Control: max-age=0');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');

//Replace php://output with $filename and add code below after that(before exit;)
//readfile($filename);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save($filename);
//echo json_encode(readfile($file));
readfile($filename);
exit;	

?>