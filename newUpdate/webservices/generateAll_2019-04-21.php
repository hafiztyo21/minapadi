<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");
	session_start();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));


	/*-- DECRYPT --*/

	//$data = (empty($_POST['data']))?trim($param_POST->data):trim($_POST['data']);
	//$Public_Key = substr($data,0,16);
	//$dec_data = CryptoAES::decrypt(substr($data,16), $Public_Key.$Private_Key, 256);
	//$data = json_decode($dec_data);

	//$data = (empty($_POST['data']))?trim($param_POST->data):trim($_POST['data']);
	//$Public_Key = substr($data,0,16);
	//$dec_data = CryptoAES::decrypt(substr($data,16), $Public_Key.$Private_Key, 256);
	//$data = json_decode($dec_data);
	$data = $param_POST->data;
	$fund_code = $data->fund_code;
	$from = $data->date_from;
	$to = $data->date_to;
	$to2 = $to;
	$pk_id = $data->pk_id;
	
	$proses = explode('_FI_',$data->DocRefID_ReportingFI);
	//print_r($data);
	

			$sql = "SELECT 	X.*, 
			Z.balance, 
			Z.totalunit,
			(
				SELECT count(XX.cus_sid) jlh FROM 
					(
						SELECT ifua_code FROM tbl_ifua_balance
						WHERE fund_code='".$fund_code."' AND  tradingdate>='".$from."' AND tradingdate<='".$to."'  AND IFNULL(endingbalance ,0)>0
						GROUP BY ifua_code
					) YY LEFT JOIN
						(
						SELECT A.is_deleted,A.cus_ins_id n_id, A.ifua_code, A.ifua_name,'' tempat_lahir_cus,'' middle_name,'' last_name,B.cus_alamat_pt cus_alamat ,B.cus_code, B.cus_name, B.cus_pt TYPE, B.cus_struktur STATUS, B.cus_no_npwp ktp_siup, B.cus_ins_sid cus_sid , 0 isIndividu, B.cus_tgl_pt tgl, cus_bank_cab bank, cus_no_rek rek, cus_pemilik_rek rek_name,
						cus_direksi,cus_direksi_2,cus_direksi_3, cus_direksi_ktp,cus_direksi_ktp_2,cus_direksi_ktp_3, cus_direksi_tgl,cus_direksi_tgl_2,cus_direksi_tgl_3, cus_direksi_jbt, cus_no_npwp idd
						FROM  tbl_kr_cus_institusi_ifua  A LEFT JOIN tbl_kr_cus_institusi B
						ON A.cus_ins_id=B.cus_id and A.is_deleted='0'
	
	
						UNION ALL
						SELECT A.is_deleted,A.cus_id n_id, A.ifua_code, A.ifua_name,B.cus_tempat_lahir tempat_lahir_cus,B.middle_name middle_name,B.last_name last_name,B.cus_address cus_alamat, B.cus_code, B.cus_name, B.Cus_national TYPE, B.cus_status_domisili STATUS, B.cus_no_identity ktp_siup, B.cus_sid cus_sid, 1 isIndividu, B.cus_tgl_lahir tgl,  cus_bank_name bank, cus_account_number rek, cus_account_name  rek_name,
						'' cus_direksi, '' cus_direksi_ktp,'' cus_direksi_ktp_2,'' cus_direksi_ktp_3,'' cus_direksi_2,'' cus_direksi_3, '' cus_direksi_tgl,''cus_direksi_tgl_2,''cus_direksi_tgl_3, '' cus_direksi_jbt, b.cus_no_identity idd
						FROM tbl_kr_cus_sup_ifua A LEFT JOIN tbl_kr_cus_sup B
						ON A.cus_id=B.cus_id and A.is_deleted='0'
						WHERE A.is_deleted='0'
						) XX			
						ON YY.ifua_code=XX.ifua_code 
						LEFT JOIN (
						SELECT  ifua_code,  endingbalance *(SELECT nab_now/10000 FROM tbl_kr_cash WHERE allocation='".$pk_id."' AND create_dt='".$to."'  ) balance, endingbalance totalunit
						FROM tbl_ifua_balance 
						WHERE tradingdate='".$to."' AND fund_code='".$fund_code."'
						) Z ON YY.ifua_code=Z.ifua_code WHERE Z.balance > 0 AND X.is_deleted = '0' AND XX.cus_sid = X.cus_sid GROUP BY XX.cus_sid					
						
			
			) JLH
			FROM 
	(
	SELECT ifua_code FROM tbl_ifua_balance
	WHERE fund_code='".$fund_code."' AND  tradingdate>='".$from."' AND tradingdate<='".$to."'  AND IFNULL(endingbalance ,0)>0
	GROUP BY ifua_code
	) Y LEFT JOIN
	(
	SELECT A.is_deleted,A.cus_ins_id n_id, A.ifua_code, A.ifua_name,'' tempat_lahir_cus,'' middle_name,'' last_name,B.cus_alamat_pt cus_alamat ,B.cus_code, B.cus_name, B.cus_pt TYPE, B.cus_struktur STATUS, B.cus_no_npwp ktp_siup, B.cus_ins_sid cus_sid , 0 isIndividu, B.cus_tgl_pt tgl, cus_bank_cab bank, cus_no_rek rek, cus_pemilik_rek rek_name,
	cus_direksi,cus_direksi_2,cus_direksi_3, cus_direksi_ktp,cus_direksi_ktp_2,cus_direksi_ktp_3, cus_direksi_tgl,cus_direksi_tgl_2,cus_direksi_tgl_3, cus_direksi_jbt, cus_no_npwp idd
	FROM  tbl_kr_cus_institusi_ifua  A LEFT JOIN tbl_kr_cus_institusi B
	ON A.cus_ins_id=B.cus_id and A.is_deleted='0'
	
	
	UNION ALL
	SELECT A.is_deleted,A.cus_id n_id, A.ifua_code, A.ifua_name,B.cus_tempat_lahir tempat_lahir_cus,B.middle_name middle_name,B.last_name last_name,B.cus_address cus_alamat, B.cus_code, B.cus_name, B.Cus_national TYPE, B.cus_status_domisili STATUS, B.cus_no_identity ktp_siup, B.cus_sid cus_sid, 1 isIndividu, B.cus_tgl_lahir tgl,  cus_bank_name bank, cus_account_number rek, cus_account_name  rek_name,
	'' cus_direksi, '' cus_direksi_ktp,'' cus_direksi_ktp_2,'' cus_direksi_ktp_3,'' cus_direksi_2,'' cus_direksi_3, '' cus_direksi_tgl,''cus_direksi_tgl_2,''cus_direksi_tgl_3, '' cus_direksi_jbt, b.cus_no_identity idd
	FROM tbl_kr_cus_sup_ifua A LEFT JOIN tbl_kr_cus_sup B
	ON A.cus_id=B.cus_id and A.is_deleted='0'
	WHERE A.is_deleted='0'
	) X
	ON Y.ifua_code=X.ifua_code 
	LEFT JOIN (
	SELECT  ifua_code,  endingbalance *(SELECT nab_now/10000 FROM tbl_kr_cash WHERE allocation='".$pk_id."' AND create_dt='".$to."'  ) balance, endingbalance totalunit
	FROM tbl_ifua_balance 
	WHERE tradingdate='".$to."' AND fund_code='".$fund_code."'
	) Z ON Y.ifua_code=Z.ifua_code WHERE Z.balance > 0 AND X.is_deleted = '0' GROUP BY X.ifua_code ORDER BY JLH ASC, X.ifua_name";
			$stmnt = $conn->query($sql);
		
					
		//print_r($conn->query($sql));

	
	
	$outp = "";	
	if(!mysqli_error($conn)){
		$idtrx = 0;
		$n_code = 1;
	while($row=mysqli_fetch_array($stmnt)){
	
	
	//get accountNumberReport
		if(($n_code > 0 && $n_code < 10 ))
			{
				$gen_n_code = '00000'.$n_code;
			}
		elseif($n_code > 9 && $n_code <  100 ){
			$gen_n_code = '0000'.$n_code;
		}
		elseif($n_code >99 && $n_code < 1000 ){
			$gen_n_code = '000'.$n_code;
		}
		elseif($n_code >999 && $n_code < 10000 ){
			$gen_n_code = '00'.$n_code;
		}
		elseif($n_code >9999 && $n_code < 100000 ){
			$gen_n_code = '0'.$n_code;
		}		
		else{
			$gen_n_code = $n_code;
		}	
	$DocRefID_AccountReport = $proses[0].'_AR_'.$gen_n_code;
	//get AccountNumberReport
	
	
	//get First Name
	/*
			$explode_cus_name = explode(' ',$row['cus_name']);
			$count = intval(count($explode_cus_name));
			$first_name = $explode_cus_name[0];
			if($first_name == ''){
				$first_name = "THE";
				
			}
			
		if(!ISSET($row['middle_name']) || EMPTY($row['middle_name']) ){
			//$count = count($explode_cus_name);
			if($count > 1){
			$row['middle_name'] = $explode_cus_name[1];
			}
			//get last name 
			if($count > 2){
				$row['middle_name'] =  end($explode_cus_name);
				
			}
			
			
		}	
	IF(( (!ISSET($row['middle_name'])) || (EMPTY($row['middle_name'])) ) && ( (ISSET($row['last_name'])) || (EMPTY($row['last_name'])) )   ){
			$row['middle_name'] = $row['last_name'];
			$row['last_name'] = '';
			//echo $row['cus_name'];
		}
		
	//	if($count == '4' ){
	//		echo $row['cus_name'];
			
	//	}
		
			$count = 0;
	*/
	//get First Name
	
		$explode_ifua = explode(' ',$row['ifua_name']);
	$countI = count($explode_ifua);
	
	//echo $countI;
//	if($row['cus_name'] == 'DRS. JOENG'){
//		$first_name = $row['cus_name'];
	
		
		
//	}
	

									if($countI >= 4){
										

										//			$row['middle_name'] = $row['last_name'];
			//$row['last_name'] = '';
										
							$first_name = $explode_ifua[0]." ".$explode_ifua[1];
							$row['middle_name'] = $explode_ifua[2];
							$row['last_name'] = $explode_ifua[3];
									
						}
						elseif($countI == '3'){
							
							$first_name = $explode_ifua[0];
							$row['middle_name'] = $explode_ifua[1];
							$row['last_name'] = $explode_ifua[2];	
						}							
						elseif($countI == '2'){
							$first_name = $explode_ifua[0];
							$row['middle_name'] = '';
							$row['last_name'] = $explode_ifua[1];			
						}	
						else{
							$first_name = $explode_ifua[0];
							$row['middle_name'] = '';
							$row['last_name'] = '';								
						
						}

	
	
	//$count = $row['ifua_name'];

	
	//get jumlah controllong person
		if($row['cus_direksi_3'] != "" ){
			$jcp = 3;
			
		}
		else{
			if($row['cus_direksi_2'] != "" ){
				$jcp = 2;
			}
			else{
					if($row['cus_direksi'] != "" ){
						$jcp = 1;			
					}
					else{
						$jcp = 0;
					}					
			}
			
		}
	//get jumlah controllong person
	
	
	//get first name direksi 1
			$explode_cus_name = "";
			$first_name_direksi = "";
			$explode_cus_name = explode(' ',$row['cus_direksi']);
			$count = count($explode_cus_name);
			if(ISSET($row['cus_direksi']) || !EMPTY($row['cus_direksi'])){
									//echo "<br/> DIR:".$row['cus_direksi'].'|'.$count;
			
				
				
			}

									if($count >= 4){
										

										
										
							$cus_direksi_first = $explode_cus_name[0]." ".$explode_cus_name[1];
							$cus_direksi_middle = $explode_cus_name[2];
							$cus_direksi_last = $explode_cus_name[3];
									
						}
						elseif($count == '3'){
							
							$cus_direksi_first = $explode_cus_name[0];
							$cus_direksi_middle = $explode_cus_name[1];
							$cus_direksi_last = $explode_cus_name[2];	
						}							
						elseif($count == '2'){
							$cus_direksi_first = $explode_cus_name[0];
							$cus_direksi_middle = '';
							$cus_direksi_last = $explode_cus_name[1];									
							//echo $row['cus_direksi'];
						}	
						else{
							$cus_direksi_first = $explode_cus_name[0];
							$cus_direksi_middle = '';
							$cus_direksi_last = '';								
						
						}
						
			/*
			if($count > 0){
				
				if(strpos($row['cus_direksi'],".") > 0 ) 
					{
						//echo $row['cus_direksi']."<br/>";
						if($count > 4){
							$cus_direksi_first = $explode_cus_name[0]." ".$explode_cus_name[1];
							$cus_direksi_middle = $explode_cus_name[2];
							$cus_direksi_last = $explode_cus_name[3];
						}
						elseif($count == '3'){
							$cus_direksi_first = $explode_cus_name[0]." ".$explode_cus_name[1];
							$cus_direksi_middle = $explode_cus_name[2];
							$cus_direksi_last = '';	
						}							
						elseif($count == '2'){
							$cus_direksi_first = $explode_cus_name[0]." ".$explode_cus_name[1];
							$cus_direksi_middle = '';
							$cus_direksi_last = '';									
						}	
						else{
							$cus_direksi_first = $explode_cus_name[0];
							$cus_direksi_middle = '';
							$cus_direksi_last = '';								
						}
						
						
						/*
						print_r($explode_cus_name);
						if(!ISSET($explode_cus_name[1])){
							$first_name_direksi= $row['cus_direksi'];
						}
						else{
							
							$first_name_direksi = $explode_cus_name[1];
							
						}
								
						
				}
				else{
					
						if($count == '3'){
							$cus_direksi_first = $explode_cus_name[0]." ".$explode_cus_name[1];
							$cus_direksi_middle = $explode_cus_name[2];
							$cus_direksi_last = '';	
						}							
						elseif($count == '2'){
							$cus_direksi_first = $explode_cus_name[0];
							$cus_direksi_middle = $explode_cus_name[1];
							$cus_direksi_last = '';									
						}	
						else{
							$cus_direksi_first = $explode_cus_name[0];
							$cus_direksi_middle = '';
							$cus_direksi_last = '';								
							
						}
					
				}	
			}
			else{
				$first_name_direksi = "";
			}
			*/
	
	//get first name direksi 1
	$count = 0;
	//get first name direksi 2
			$explode_cus_name = "";
			$first_name_direksi_2 = "";
			$explode_cus_name = explode(' ',$row['cus_direksi_2']);
			$count = count($explode_cus_name);
			if($count > 0){
				if(!preg_match('/./i', $explode_cus_name[0])) 
					{
						if(!ISSET($explode_cus_name[1])){
							$first_name_direksi_2= $row['cus_direksi_2'];
						}
						else{
							
							$first_name_direksi_2 = $explode_cus_name[1];
							
						}
				}
				else{
					
					$first_name_direksi_2= $explode_cus_name[0];
				}	
			}
			else{
				$first_name_direksi_2 = "";
			}
	
	//get first name direksi 2	
	$count = 0;

	//get first name direksi 3
			$explode_cus_name = "";
			$first_name_direksi_3 = "";
			$explode_cus_name = explode(' ',$row['cus_direksi_3']);
			$count = count($explode_cus_name);
			if($count > 0){
				if(!preg_match('/./i', $explode_cus_name[0])) 
					{
						if(!ISSET($explode_cus_name[1])){
							$first_name_direksi_3= $row['cus_direksi_3'];
						}
						else{
							
							$first_name_direksi_3 = $explode_cus_name[1];
							
						}
				}
				else{
					
					$first_name_direksi_3= $explode_cus_name[0];
				}	
			}
			else{
				$first_name_direksi_3 = "";
			}
	
	//get first name direksi 3	

$count = 0;
	
	//get Account_number
		
	//	$accountNumber = $row['cus_sid']."_".$row['rek'];
		if(!ISSET($row['cus_sid'])  || EMPTY($row['cus_sid']) ){
			if(!ISSET($row['cus_sid'])  || EMPTY($row['cus_sid'])){
				$accountNumber = "NANUM_NANUM";
				
			}
			else{
					$accountNumber = "NANUM_".$row['rek'];
			}
		
		}
		else{
			$accountNumber = $row['cus_sid']."_".$row['rek'];
		}
			
		
		//echo $row['allocation']; tempat_lahir_cus
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"cus_code":"'.urlencode($row['cus_code']).'",';
			$outp .= '"idtrx":"'.urlencode($idtrx).'",';
			$outp .= '"cus_name":"'.urlencode($row['cus_name']).'",';
			$outp .= '"midle_name":"'.urlencode($row['middle_name']).'",';
			$outp .= '"last_name":"'.urlencode($row['last_name']).'",';
			$outp .= '"identitas":"'.urlencode($row['idd']).'",';
			$outp .= '"isIndividu":"'.urlencode($row['isIndividu']).'",';
			$outp .= '"tgl_lahirCus_berdiriIns":"'.urlencode($row['tgl']).'",';
			$outp .= '"tempat_lahir_cus":"'.urlencode($row['tempat_lahir_cus']).'",';
			$outp .= '"sid":"'.urlencode($row['cus_sid']).'",';
			$outp .= '"rek_nasabah":"'.urlencode($row['rek']).'",';
			$outp .= '"accountNumber":"'.urlencode($accountNumber).'",';
			$outp .= '"idAccountReport":"'.urlencode($DocRefID_AccountReport).'",';
			$outp .= '"cus_ktp_siup":"'.urlencode($row['ktp_siup']).'",';
			$outp .= '"first_name":"'.urlencode($first_name).'",';
			$outp .= '"cus_direksi":"'.urlencode($row['cus_direksi']).'",';
			$outp .= '"ifua_name":"'.urlencode($row['ifua_name']).'",';
			$outp .= '"cus_direksi_first":"'.urlencode($cus_direksi_first).'",';
			$outp .= '"cus_direksi_middle":"'.urlencode($cus_direksi_middle).'",';
			$outp .= '"cus_direksi_last":"'.urlencode($cus_direksi_last).'",';
			$outp .= '"cus_direksi_2":"'.urlencode($row['cus_direksi_2']).'",';
			$outp .= '"cus_direksi_3":"'.urlencode($row['cus_direksi_3']).'",';
			$outp .= '"cus_alamat":"'.urlencode($row['cus_alamat']).'",';
			$outp .= '"cus_direksi_ktp":"'.urlencode($row['cus_direksi_ktp']).'",';
			$outp .= '"cus_direksi_ktp_2":"'.urlencode($row['cus_direksi_ktp_2']).'",';
			$outp .= '"cus_direksi_ktp_3":"'.urlencode($row['cus_direksi_ktp_3']).'",';
			$outp .= '"cus_direksi_tgl":"'.urlencode($row['cus_direksi_tgl']).'",';
			$outp .= '"cus_direksi_tgl_2":"'.urlencode($row['cus_direksi_tgl_2']).'",';
			$outp .= '"cus_direksi_tgl_3":"'.urlencode($row['cus_direksi_tgl_3']).'",';			
			$outp .= '"cus_alamat":"'.urlencode($row['cus_alamat']).'",';			
			$outp .= '"jcp":"'.urlencode($jcp).'",';
			$outp .= '"balance":"'.urlencode($row['balance']).'",';
			
			
			
			$outp .= '"cus_sid":"'.$row['cus_sid'].'"}'; 
		
	$idtrx++;
	$n_code++;
	};		
	//print_r($outp);
	$result = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	//print_r($result);
	}
	else{
		$result = '{ "status":"no", "message":"'.mysqli_error($conn).'", "records":[]}';
		
	}
	print_r($result);	
	//die(mysqli_error($db));

	//$Public_Key = getRandomKey(16);
	//$chiper = CryptoAES::encrypt($result, $Public_Key.$Private_Key, 256);
	//echo('{"data":'.json_encode($Public_Key.$chiper).'}');
	$conn = null;	
	
		
	
	
	
	
	
	
?>
