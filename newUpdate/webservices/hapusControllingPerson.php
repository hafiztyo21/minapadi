<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");
	session_start();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));

$data = $param_POST->data;


//print_r($data);
$dataGlobal = $data->dataGlobal;
$id = $data->id;

$DocRefID_AccountReport 		= array();
$DocRefID_ControllingPerson		= array();
$Nationality 					= array();
$BirthInfo_BirthDate 			= array();
$BirthInfo_City 				= array();
$BirthInfo_CountryCode 			= array();
$CtrlPersonType 				= array();
$ResCountryCode					= array(); 
/*
		DocRefID_AccountReport_cp : '',
		DocRefID_ControllingPerson_cp : '',
		ResCountryCode_cp : '',
		Nationality_cp : '',
		BirthInfo_BirthDate_cp : '',
		BirthInfo_City_cp : '',
		BirthInfo_CountryCode_cp : '',
		CtrlPersonType_cp : '',
*/


$tempDocRefID_AccountReport 		= array();
$tempDocRefID_ControllingPerson		= array();
$tempNationality 					= array();
$tempBirthInfo_BirthDate 			= array();
$tempBirthInfo_City 				= array();
$tempBirthInfo_CountryCode 			= array();
$tempCtrlPersonType 				= array();
$tempResCountryCode					= array();

$tempDocRefID_AccountReport 		= $data->dataGlobal->DocRefID_AccountReport_cp;
$tempDocRefID_ControllingPerson		= $data->dataGlobal->DocRefID_ControllingPerson_cp;
$tempNationality 					= $data->dataGlobal->Nationality_cp;
$tempBirthInfo_BirthDate 			= $data->dataGlobal->BirthInfo_BirthDate_cp;
$tempBirthInfo_City 				= $data->dataGlobal->BirthInfo_City_cp;
$tempBirthInfo_CountryCode 			= $data->dataGlobal->BirthInfo_CountryCode_cp;
$tempCtrlPersonType 				= $data->dataGlobal->CtrlPersonType_cp;
$tempResCountryCode						= $data->dataGlobal->ResCountryCode_cp ;


$count = count($tempDocRefID_AccountReport);

$iBaru = $count - 1;
$j = 0;
$outp = '';
for($i=0 ; $i<$count; $i++){
	
	if($i != $id){
$DocRefID_AccountReport[$j] 		= $tempDocRefID_AccountReport[$i];
$DocRefID_ControllingPerson[$j]		= $tempDocRefID_ControllingPerson[$i];
$Nationality[$j] 					= $tempNationality[$i];
$BirthInfo_BirthDate[$j] 			= $tempBirthInfo_BirthDate[$i];
$BirthInfo_City[$j] 				= $tempBirthInfo_City[$i];
$BirthInfo_CountryCode[$j] 			= $tempBirthInfo_CountryCode[$i];
$CtrlPersonType[$j] 				= $tempCtrlPersonType[$i];
$ResCountryCode[$j]					= $tempResCountryCode[$i];
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"DocRefID_AccountReport":"'.$DocRefID_AccountReport[$j].'",';			
			$outp .= '"id":"'.$j.'",';
			$outp .= '"DocRefID_ControllingPerson":"'.$DocRefID_ControllingPerson[$j].'",';
			$outp .= '"ResCountryCode":"'.$ResCountryCode[$j].'",';
			$outp .= '"Nationality":"'.$Nationality[$j].'",';
			$outp .= '"BirthInfo_BirthDate":"'.$BirthInfo_BirthDate[$j].'",';
			$outp .= '"BirthInfo_City":"'.$BirthInfo_City[$j].'",';
			$outp .= '"BirthInfo_CountryCode":"'.$BirthInfo_CountryCode[$j].'",';
			$outp .= '"CtrlPersonType":"'.$CtrlPersonType[$j].'"}'; 	
		$j++;
	}
	else{
		$x= '';
	}	
}
	$result = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	print_r($result);
?>
