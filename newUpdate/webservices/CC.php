<?php


33 CtrlgPerson Name
    $sheet = $objPHPExcel->getActiveSheet();

    //Start adding next sheets

      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(15); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'PrecedingTitle')
            ->setCellValue('C2', 'Title')
			->setCellValue('D2', 'FirstName')
			->setCellValue('E2', 'FirstNameType')
			->setCellValue('F2', 'MiddleName')
			->setCellValue('G2', 'MiddleNameType')
			->setCellValue('H2', 'NamePrefix')
			->setCellValue('I2', 'NamePrefixType')
			->setCellValue('J2', 'LastName')
			->setCellValue('K2', 'LastNameType')
			->setCellValue('L2', 'GenerationIdentifier')
			->setCellValue('M2', 'Suffix')
			->setCellValue('N2', 'GeneralSuffix')
            ->setCellValue('O2', 'NameType');
			
   $objWorkSheet->getStyle('A2:O2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			

$objPHPExcel->getSheet(0)->getStyle('A2:O3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(0)->getStyle('A2:O3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');


	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('G')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('H')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('I')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('J')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('K')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('L')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('M')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('N')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('O')
    ->setAutoSize(true);

/*
DocRefID_ControllingPerson_cpn : '',
		PrecedingTitle_cpn : '',
		Title_cpn : '',
		FirstName_cpn : '',
		FirstNameType_cpn : '',
		MiddleName_cpn : '',
		MiddleNameType_cpn :'',
		NamePrefix_cpn : '',
		NamePrefixType_cpn : '',
		LastName_cpn : '',
		LastNameType_cpn : '',
		GenerationIdentifier_cpn :'',
		Suffix_cpn : '',
		GeneralSuffix_cpn : '',
		NameType_cpn :'',

*/
					$brs=3;
	$val_1 = $data->DocRefID_ControllingPerson_cpn;
	$val_2 = $data->NameType_cpn;
	$val_3 = $data->FirstName_cpn;
                $objWorkSheet->setCellValue("A$brs", "$val_1");
				$objWorkSheet->setCellValue("B$brs", " ");
				$objWorkSheet->setCellValue("C$brs", " ");
				$objWorkSheet->setCellValue("D$brs", "$val_3");
				$objWorkSheet->setCellValue("E$brs", " ");
				$objWorkSheet->setCellValue("F$brs", " ");
				$objWorkSheet->setCellValue("G$brs", " ");
				$objWorkSheet->setCellValue("H$brs", " ");
				$objWorkSheet->setCellValue("I$brs", " ");
				$objWorkSheet->setCellValue("J$brs", " ");
				$objWorkSheet->setCellValue("K$brs", " ");
				$objWorkSheet->setCellValue("L$brs", " ");
				$objWorkSheet->setCellValue("M$brs", " ");
				$objWorkSheet->setCellValue("N$brs", " ");
				$objWorkSheet->setCellValue("O$brs", "DPJ202");

      // Rename sheet
      $objWorkSheet->setTitle("33 CtrlgPerson Name");

	  
/*--33 CtrlgPerson Name--*/


?>