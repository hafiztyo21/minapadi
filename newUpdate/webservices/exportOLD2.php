<?php
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");

		$param_POST = json_decode(file_get_contents("php://input"));

$data = $param_POST->data;
//print_r($data);


require_once 'plugins/PHPExcel-1.8.1/Classes/PHPExcel.php';
require_once 'plugins/PHPExcel-1.8.1/Classes/PHPExcel/Shared/ZipStreamWrapper.php';
$filename = 'coba123.xlsx';


$objPHPExcel = new PHPExcel();

/*--000MessageSpec--*/
    $sheet = $objPHPExcel->getActiveSheet();

    //Start adding next sheets

      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(0); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'SendingCompanyIN')
            ->setCellValue('B2', 'TransmittingCountry')
            ->setCellValue('C2', 'ReceivingCountry')
            ->setCellValue('D2', 'MessageType')
			->setCellValue('E2', 'Warning')
            ->setCellValue('F2', 'Contact')
            ->setCellValue('G2', 'MessageRefId')
            ->setCellValue('H2', 'MessageTypeIndic')
            ->setCellValue('I2', 'CorrMessageRefId')
			->setCellValue('J2', 'ReportingPeriod')
            ->setCellValue('K2', 'Timestamp');
			
   $objWorkSheet->getStyle('A2:K2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(0)->getStyle('A2:K3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(0)->getStyle('A2:K3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('G')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('H')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('I')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('J')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('K')
    ->setAutoSize(true);


					$brs=3;
                $objWorkSheet->setCellValue("A$brs", " $data->npwp_no");
                $objWorkSheet->setCellValue("B$brs", "$data->TransmittingCountry");
                $objWorkSheet->setCellValue("C$brs", "$data->ReceivingCountry");
				$objWorkSheet->setCellValue("D$brs", "$data->MessageType");
				$objWorkSheet->setCellValue("E$brs", "$data->Warning");
				$objWorkSheet->setCellValue("F$brs", "$data->Contact");
				$objWorkSheet->setCellValue("G$brs", "$data->MessageRefId");
				$objWorkSheet->setCellValue("H$brs", "$data->MessageTypeIndic");			
				$objWorkSheet->setCellValue("I$brs", "$data->CorrMessageRefId");			
				$objWorkSheet->setCellValue("J$brs", "$data->ReportingPeriod");			
				$objWorkSheet->setCellValue("K$brs", "$data->Timestamp ");	
	

      // Rename sheet
      $objWorkSheet->setTitle("000MessageSpec");

	  
/*--000MessageSpec--*/



















//--10REPORTINGF1--//

      $objWorkSheet = $objPHPExcel->createSheet(1); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'MessageRefId')
            ->setCellValue('B2', 'DocRefID_ReportingFI')
            ->setCellValue('C2', 'ResCountryCode')
            ->setCellValue('D2', 'DocTypeIndicl')
			->setCellValue('E2', 'CorrMessageRefID')
            ->setCellValue('F2', 'CorrDocRefID_ReportingFI');
			
   $objWorkSheet->getStyle('A2:F3')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(1)->getStyle('A2:F3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(1)->getStyle('A2:F3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);

/*
		DocRefID_ReportingFI : '',
		ResCountryCode : 'ID',
		DocTypeIndic1  : '',
		CorrDocRefID : '',

*/
					$brs=3;
                $objWorkSheet->setCellValue("A$brs", " $data->MessageRefId");
                $objWorkSheet->setCellValue("B$brs", "$data->DocRefID_ReportingFI");
                $objWorkSheet->setCellValue("C$brs", "$data->ResCountryCode");
				$objWorkSheet->setCellValue("D$brs", "$data->DocTypeIndic1");
				$objWorkSheet->setCellValue("E$brs", "$data->CorrMessageRefId ");
				$objWorkSheet->setCellValue("F$brs", "$data->CorrDocRefID");

	

      // Rename sheet
      $objWorkSheet->setTitle("10 Reporting F1");
	  
 //--10REPORTINGF1--//
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  //--11REP F1 IN--//
 
      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(2); //Setting index when creating
/*

		IN : '',
		IssuedBy : 'ID',
		INType : 'ID06',

*/
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_ReportingFI')
            ->setCellValue('B2', 'IN')
            ->setCellValue('C2', 'IssuedBy')
            ->setCellValue('D2', 'INType');
			
   $objWorkSheet->getStyle('A2:d2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(2)->getStyle('A2:D3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(2)->getStyle('A2:D3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);



					$brs=3;
                $objWorkSheet->setCellValue("A$brs", "$data->DocRefID_ReportingFI");
                $objWorkSheet->setCellValue("B$brs", " $data->IN");
                $objWorkSheet->setCellValue("C$brs", "$data->IssuedBy");
				$objWorkSheet->setCellValue("D$brs", "$data->INType");

	

      // Rename sheet
      $objWorkSheet->setTitle("11 REP F1 IN"); 
  
  
  
  //--11REP F1 IN--//
  
  
  
  
  
//--12 REP F1 NAME--//
      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(3); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_ReportingFI')
            ->setCellValue('B2', 'Name')
            ->setCellValue('C2', 'NameType');
			
   $objWorkSheet->getStyle('A2:C2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(3)->getStyle('A2:C3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(3)->getStyle('A2:C3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);




				$brs=3;
                $objWorkSheet->setCellValue("A$brs", " $data->DocRefID_ReportingFI");
                $objWorkSheet->setCellValue("B$brs", "$data->Name");
                $objWorkSheet->setCellValue("C$brs", "$data->nameType");

	

      // Rename sheet
      $objWorkSheet->setTitle("12 REP F1 NAME"); 
 
//--12 REP F1 NAME--//
 
 
//--13 REP F1 ADDRESS--//

     $objWorkSheet = $objPHPExcel->createSheet(4); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_ReportingFI')
            ->setCellValue('B2', 'legalAddressType')
            ->setCellValue('C2', 'CountryCode')
            ->setCellValue('D2', 'AddressFree')
			->setCellValue('E2', 'Street')
            ->setCellValue('F2', 'BuildingIdentifier')
            ->setCellValue('G2', 'SuiteIdentifier')
            ->setCellValue('H2', 'FloorIdentifier')
            ->setCellValue('I2', 'DistrictName')
			->setCellValue('J2', 'POB')
            ->setCellValue('K2', 'PostCode')
			->setCellValue('L2', 'City')
			->setCellValue('M2', 'CountrySubentity');
			
   $objWorkSheet->getStyle('A2:M2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(4)->getStyle('A2:M3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(4)->getStyle('A2:M3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('G')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('H')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('I')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('J')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('K')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('L')
    ->setAutoSize(true);	
	$objWorkSheet
    ->getColumnDimension('M')
    ->setAutoSize(true);
					$brs=3;
                $objWorkSheet->setCellValue("A$brs", " $data->DocRefID_ReportingFI");
                $objWorkSheet->setCellValue("B$brs", "$data->legalAddressType");
                $objWorkSheet->setCellValue("C$brs", "$data->CountryCode");
				$objWorkSheet->setCellValue("D$brs", "$data->AddressFree");
				$objWorkSheet->setCellValue("E$brs", "$data->Street");
				$objWorkSheet->setCellValue("F$brs", "$data->BuildingIdentifier");
				$objWorkSheet->setCellValue("G$brs", "$data->SuiteIdentifier");
				$objWorkSheet->setCellValue("H$brs", "$data->FloorIdentifier");			
				$objWorkSheet->setCellValue("I$brs", "$data->DistrictName");			
				$objWorkSheet->setCellValue("J$brs", "$data->POB");			
				$objWorkSheet->setCellValue("K$brs",  "$data->PostCode");	
				$objWorkSheet->setCellValue("L$brs", "$data->City");	
				$objWorkSheet->setCellValue("M$brs", " ");	
	

      // Rename sheet
      $objWorkSheet->setTitle("13 REP F1 Address");


//--13 REP F1 ADDRESS--/ 


















//--20 Account Report --//
      $objWorkSheet = $objPHPExcel->createSheet(5); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_ReportingFI')
            ->setCellValue('B2', 'DocTypeIndic_AccountReport')
            ->setCellValue('C2', 'DocRefID_AccountReport')
            ->setCellValue('D2', 'CorrMessageRefID_AccountReport')
			->setCellValue('E2', 'CorrDocRefID_AccountReport')
            ->setCellValue('F2', 'AccountNumber')
            ->setCellValue('G2', 'AccountNumberType')
            ->setCellValue('H2', 'UndocumentedAccount')
            ->setCellValue('I2', 'ClosedAccount')
			->setCellValue('J2', 'DormantAccount')
            ->setCellValue('K2', 'IsIndividual')
			->setCellValue('L2', 'AcctHolderType')
			->setCellValue('M2', 'ResCountryCode')
			->setCellValue('N2', 'Nationality')
			->setCellValue('O2', 'CurrencyCodeAccountBalance')
			->setCellValue('P2', 'AccountBalance')
			->setCellValue('Q2', 'BirthInfo_BirthDate')
			->setCellValue('R2', 'BirthInfo_City')
			->setCellValue('S2', 'BirthInfo_CountryCode');
			
   $objWorkSheet->getStyle('A2:K2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(5)->getStyle('A2:S3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle('A2:S2')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('G')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('H')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('I')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('J')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('K')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('L')
    ->setAutoSize(true);	
	$objWorkSheet
    ->getColumnDimension('M')
    ->setAutoSize(true);
	
	$objWorkSheet
    ->getColumnDimension('N')
    ->setAutoSize(true);

	$objWorkSheet
    ->getColumnDimension('O')
    ->setAutoSize(true);

	$objWorkSheet
    ->getColumnDimension('P')
    ->setAutoSize(true);

	$objWorkSheet
    ->getColumnDimension('Q')
    ->setAutoSize(true);

	$objWorkSheet
    ->getColumnDimension('R')
    ->setAutoSize(true);

	$objWorkSheet
    ->getColumnDimension('S')
    ->setAutoSize(true);	

					$brs=3;
                $objWorkSheet->setCellValue("A$brs", " $data->DocRefID_ReportingFI");
                $objWorkSheet->setCellValue("B$brs", "$data->DocTypeIndic_AccountReport");
                $objWorkSheet->setCellValue("C$brs", "$data->DocRefID_AccountReport");
				$objWorkSheet->setCellValue("D$brs", "$data->CorrMessageRefID_AccountReport");
				$objWorkSheet->setCellValue("E$brs", "$data->CorrDocRefID_AccountReport");
				$objWorkSheet->setCellValue("F$brs", "$data->AccountNumber");
				$objWorkSheet->setCellValue("G$brs", "$data->AccountNumberType");
				$objWorkSheet->setCellValue("H$brs", "$data->UndocumentedAccount");			
				$objWorkSheet->setCellValue("I$brs", "$data->ClosedAccount");			
				$objWorkSheet->setCellValue("J$brs", "$data->DormantAccount");			
				$objWorkSheet->setCellValue("K$brs", "$data->IsIndividual");	
				$objWorkSheet->setCellValue("L$brs", "$data->AcctHolderType");	
				$objWorkSheet->setCellValue("M$brs", "$data->ResCountryCode");	
				$objWorkSheet->setCellValue("N$brs", "$data->Nationality");	
				$objWorkSheet->setCellValue("O$brs", "$data->CurrencyCodeAccountBalance");	
				$objWorkSheet->setCellValue("P$brs", "$data->AccountBalance");	
				$objWorkSheet->setCellValue("Q$brs", "$data->BirthInfo_BirthDate");	
				$objWorkSheet->setCellValue("R$brs", "$data->BirthInfo_City");	
				$objWorkSheet->setCellValue("S$brs", "$data->BirthInfo_CountryCode");	
	

      // Rename sheet
      $objWorkSheet->setTitle("20 Account Report");



//--20 Account Report --//




//--21 Acc Rep-Individual--//
      $objWorkSheet = $objPHPExcel->createSheet(6); //Setting index when creating

	  
 


$x = count($data->idTempAccRep);
$brs = 3;
$colom = 2+$x;
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'TIN')
            ->setCellValue('C2', 'IssuedBy')
            ->setCellValue('D2', 'TINType');
			
   $objWorkSheet->getStyle('A2:D2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);


for($i=0;$i<$x; $i++){
	$val_1 = $data->DocRefID_AccountReport_TIN_individual[$i];
	$val_2 = $data->TIN[$i];
	$val_3 = $data->TINType_TIN_individual[$i];
	$val_4 = $data->IssuedBy_TIN_individual[$i];
                $objWorkSheet->setCellValue("A$brs", "$val_1");
				$objWorkSheet->setCellValue("B$brs", " $val_2");
				$objWorkSheet->setCellValue("C$brs", "$val_3");
				$objWorkSheet->setCellValue("D$brs", "$val_4");
$brs++;
				}

      // Rename sheet
      $objWorkSheet->setTitle("21 Acc Rep TIN-Individual");
				

//21 Acc Rep-Individual--//













//--22 Acc Rep-Organization--//
      $objWorkSheet = $objPHPExcel->createSheet(7); //Setting index when creating

	  
 


$x = count($data->idTempAccRep_IN_organization);
$brs = 3;
$colom = 2+$x;
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'IN')
            ->setCellValue('C2', 'IssuedBy')
            ->setCellValue('D2', 'INType');
			
   $objWorkSheet->getStyle('A2:D2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);

/*
		$scope.data.DocRefID_AccountReport_IN_organization =[];
		$scope.data.IN_IN_organization = [];
		$scope.data.INType_IN_organization = [];
		$scope.data.IssuedBy_IN_organization = [];	


*/
for($i=0;$i<$x; $i++){
	$val_1 = $data->DocRefID_AccountReport_IN_organization[$i];
	$val_2 = $data->TIN[$i];
	$val_3 = $data->INType_IN_organization[$i];
	$val_4 = $data->IssuedBy_IN_organization[$i];
                $objWorkSheet->setCellValue("A$brs", "$val_1");
				$objWorkSheet->setCellValue("B$brs", " $val_2");
				$objWorkSheet->setCellValue("C$brs", "$val_4");
				$objWorkSheet->setCellValue("D$brs", "$val_3");
$brs++;
				}

			
		

      // Rename sheet
      $objWorkSheet->setTitle("22 Acc Rep IN-Organization");


//22 Acc Rep-Organization--//








//23 acc Rep Name-Individual--//
	 // skipp 7
      $objWorkSheet = $objPHPExcel->createSheet(8); //Setting index when creating

	  
$x = count($data->idTempAccRep_Name_Individual);
$brs = 3;
$colom = $brs+$x;
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'PrecedingTitle')
            ->setCellValue('C2', 'Title')
			->setCellValue('D2', 'FirstName')
			->setCellValue('E2', 'FirstNameType')
			->setCellValue('F2', 'MiddleName')
			->setCellValue('G2', 'MiddleNameType')
			->setCellValue('H2', 'NamePrefix')
			->setCellValue('I2', 'NamePrefixType')
			->setCellValue('J2', 'LastName')
			->setCellValue('K2', 'LastNameType')
			->setCellValue('L2', 'GenerationIdentifier')
			->setCellValue('M2', 'Suffix')
			->setCellValue('N2', 'GeneralSuffix')
            ->setCellValue('O2', 'NameType');
			
   $objWorkSheet->getStyle('A2:O2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(5)->getStyle("A2:O$colom")->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle("A2:O$colom")->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('G')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('H')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('I')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('J')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('K')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('L')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('M')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('N')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('O')
    ->setAutoSize(true);


	
	
	

/*
		$scope.data.idTempAccRep_Name_Individual = [];
		$scope.data.DocRefID_AccountReport_Name_Individual = [];
		$scope.data.NameType_Name_Individual = [];
		$scope.data.FirstName = [];
		
		
		  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'PrecedingTitle')
            ->setCellValue('C2', 'Title')
			->setCellValue('D2', 'FirstName')
			->setCellValue('E2', 'FirstNameType')
			->setCellValue('F2', 'MiddleName')
			->setCellValue('G2', 'MiddleNameType')
			->setCellValue('H2', 'NamePrefix')
			->setCellValue('I2', 'NamePrefixType')
			->setCellValue('J2', 'LastName')
			->setCellValue('K2', 'LastNameType')
			->setCellValue('L2', 'GenerationIdentifier')
			->setCellValue('M2', 'Suffix')
			->setCellValue('N2', 'GeneralSuffix')
            ->setCellValue('O2', 'NameType');

*/

for($i=0;$i<$x; $i++){
	$val_1 = $data->DocRefID_AccountReport_Name_Individual[$i];
	$val_2 = $data->NameType_Name_Individual[$i];
	$val_3 = $data->FirstName[$i];
                $objWorkSheet->setCellValue("A$brs", "$val_1");
				$objWorkSheet->setCellValue("B$brs", " ");
				$objWorkSheet->setCellValue("C$brs", " ");
				$objWorkSheet->setCellValue("D$brs", "$val_3");
				$objWorkSheet->setCellValue("E$brs", " ");
				$objWorkSheet->setCellValue("F$brs", " ");
				$objWorkSheet->setCellValue("G$brs", " ");
				$objWorkSheet->setCellValue("H$brs", " ");
				$objWorkSheet->setCellValue("I$brs", " ");
				$objWorkSheet->setCellValue("J$brs", " ");
				$objWorkSheet->setCellValue("K$brs", " ");
				$objWorkSheet->setCellValue("L$brs", " ");
				$objWorkSheet->setCellValue("M$brs", " ");
				$objWorkSheet->setCellValue("N$brs", " ");
				$objWorkSheet->setCellValue("O$brs", "$val_2");
$brs++;
				}
				
		

      // Rename sheet
      $objWorkSheet->setTitle("23 Acc RepName-Individual");



//23 acc rep Name-Individual--//
















//24 acc Rep Name-Organization--//
	 // skipp 7
      $objWorkSheet = $objPHPExcel->createSheet(9); //Setting index when creating

	  
$x = count($data->DocRefID_AccountReport_Name_Organization);
$brs = 3;
$colom = $brs+$x;
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'Name')
            ->setCellValue('C2', 'NameType');
   $objWorkSheet->getStyle('A2:C2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(5)->getStyle("A2:C$colom")->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle("A2:C$colom")->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);


	
	
	

/*
		$scope.data.DocRefID_AccountReport_Name_Organization = [];
		$scope.data.Name_Name_Organization = [];
		$scope.data.nameType_Name_Organization = [];
		$scope.data.idTemp_Name_organization = [];


*/

for($i=0;$i<$x; $i++){
	$val_1 = $data->DocRefID_AccountReport_Name_Organization[$i];
	$val_2 = $data->Name_Name_Organization[$i];
	$val_3 = $data->nameType_Name_Organization[$i];
                $objWorkSheet->setCellValue("A$brs", "$val_1");
				$objWorkSheet->setCellValue("B$brs", "$val_2");
				$objWorkSheet->setCellValue("C$brs", "$val_3");
$brs++;
				}
      // Rename sheet
      $objWorkSheet->setTitle("24 Acc RepName-Organization");



//24 acc rep Name-Organization--//











//25 acc rep address--//
	 // skipp 7
      $objWorkSheet = $objPHPExcel->createSheet(10); //Setting index when creating

	  
$x = count($data->DocRefID_AccountReport_AccRepAddress);
$brs = 3;
$colom = $brs+$x;
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'legalAddressType')
			->setCellValue('C2', 'CountryCode')
			->setCellValue('D2', 'AddressFree')
			->setCellValue('E2', 'Street')
			->setCellValue('F2', 'BuildingIdentifier')
			->setCellValue('G2', 'SuiteIdentifier')
			->setCellValue('H2', 'FloorIdentifier')
			->setCellValue('I2', 'DistrictName')
			->setCellValue('J2', 'POB')
			->setCellValue('K2', 'PostCode')
			->setCellValue('L2', 'City')
			->setCellValue('M2', 'CountrySubentity');
   $objWorkSheet->getStyle('A2:M2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(5)->getStyle("A2:M$colom")->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle("A2:M$colom")->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);


	
	
	

/*
		$scope.data.DocRefID_AccountReport_AccRepAddress =[];
		$scope.data.legalAddressType_AccRepAddress =[];
		$scope.data.CountryCode_AccRepAddress =[];
		$scope.data.AddressFree_AccRepAddress =[];
		$scope.data.idTemp_AccRepAddress = [];
		


*/
//print_r($data->AddressFree_AccRepAddress);
for($i=0;$i<$x; $i++){
	$val_1 = $data->DocRefID_AccountReport_AccRepAddress[$i];
	$val_2 = $data->legalAddressType_AccRepAddress[$i];
	$val_3 = $data->CountryCode_AccRepAddress[$i];
	$val_4 = $data->AddressFree_AccRepAddress[$i];
                $objWorkSheet->setCellValue("A$brs", "$val_1");
				$objWorkSheet->setCellValue("B$brs", "$val_2");
				$objWorkSheet->setCellValue("C$brs", "$val_3");
				$objWorkSheet->setCellValue("D$brs", "$val_4");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				$objWorkSheet->setCellValue("D$brs", " ");
				
				
$brs++;
				}
      // Rename sheet
      $objWorkSheet->setTitle("25 Acc Rep Address");



//25 acc rep address--//



 
 
 
 
 
 
//26 acc rep payment--//
// skipp 7
      $objWorkSheet = $objPHPExcel->createSheet(11); //Setting index when creating

	  
$x = count($data->DocRefID_AccountReport_AccRepPayment);
$brs = 3;
$colom = $brs+$x;
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'PaymentType')
			->setCellValue('C2', 'CurrencyCodePayment')
			->setCellValue('D2', 'PaymentAmnt');
   $objWorkSheet->getStyle('A2:D2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			

$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->getColor()->setRGB('000000');


	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);

	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	
	
	





for($i=0;$i<$x; $i++){
	$val_1 = $data->DocRefID_AccountReport_AccRepPayment[$i];
	$val_2 = $data->PaymentType[$i];
	$val_3 = $data->CurrencyCodePayment[$i];
	$val_4 = $data->PaymentAmnt[$i];
    $objWorkSheet->setCellValue("A$brs", "$val_1");
	$objWorkSheet->setCellValue("B$brs", "$val_2");
	$objWorkSheet->setCellValue("C$brs", "$val_3");
	$objWorkSheet->setCellValue("D$brs", " $val_4");
$brs++;
				}
      // Rename sheet
      $objWorkSheet->setTitle("26 Acc Rep Payment");



//26 acc rep payment--// 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
//30 Controllong person--//
// skipp 7
      $objWorkSheet = $objPHPExcel->createSheet(12); //Setting index when creating

	  
$x = count($data->DocRefID_AccountReport_cp);
$brs = 3;
$colom = $brs+$x;
      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'DocRefID_ControllingPerson')
			->setCellValue('C2', 'ResCountryCode')
			->setCellValue('D2', 'Nationality')
			->setCellValue('E2', 'BirthInfo_BirthDate')
			->setCellValue('F2', 'BirthInfo_City')
			->setCellValue('G2', 'BirthInfo_CountryCode')
			->setCellValue('H2', 'CtrlPersonType');
			
			
   $objWorkSheet->getStyle('A2:D2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			

$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(5)->getStyle("A2:D$colom")->getBorders()
->getAllBorders()->getColor()->setRGB('000000');


	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);

	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	
	
	

/*
		$scope.data.DocRefID_AccountReport_cp = [];
		$scope.data.DocRefID_ControllingPerson_cp = [];
		$scope.data.ResCountryCode_cp = [];
		$scope.data.Nationality_cp = [];
		$scope.data.BirthInfo_BirthDate_cp = [];
		$scope.data.BirthInfo_City_cp = [];
		$scope.data.BirthInfo_CountryCode_cp = [];
		$scope.data.CtrlPersonType_cp = [];
		$scope.data.idTemp_cp = [];	


*/



for($i=0;$i<$x; $i++){
	$val_1 = $data->DocRefID_AccountReport_cp[$i];
	$val_2 = $data->DocRefID_ControllingPerson_cp[$i];
	$val_3 = $data->ResCountryCode_cp[$i];
	$val_4 = $data->Nationality_cp[$i];
	$val_5 = $data->BirthInfo_BirthDate_cp[$i];
	$val_6 = $data->BirthInfo_City_cp[$i];
	$val_7 = $data->BirthInfo_CountryCode_cp [$i];
	$val_8 = $data->CtrlPersonType_cp[$i];
	

    $objWorkSheet->setCellValue("A$brs", "$val_1");
	$objWorkSheet->setCellValue("B$brs", "$val_2");
	$objWorkSheet->setCellValue("C$brs", "$val_3");
	$objWorkSheet->setCellValue("D$brs", " $val_4");
	
	$objWorkSheet->setCellValue("E$brs", "$val_5");
	$objWorkSheet->setCellValue("F$brs", "$val_6");
	$objWorkSheet->setCellValue("G$brs", "$val_7");
	$objWorkSheet->setCellValue("H$brs", "$val_8");
$brs++;
				}
      // Rename sheet
      $objWorkSheet->setTitle("30 Controlling Person");



//30 Controlling person--//  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

/*--31 CtrlgPerson TIN--*/
    $sheet = $objPHPExcel->getActiveSheet();

    //Start adding next sheets

      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(13); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_ControllingPerson')
            ->setCellValue('B2', 'TIN')
            ->setCellValue('C2', 'IssuedBy')
            ->setCellValue('D2', 'TINType');
			
   $objWorkSheet->getStyle('A2:D2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(0)->getStyle('A2:D3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(0)->getStyle('A2:D3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);

/*
		DocRefID_ControllingPerson_cpt : '',
		TIN_cpt : '',
		IssuedBy_cpt : 'ID',
		TINType_cpt : 'ID02',


*/
					$brs=3;
                $objWorkSheet->setCellValue("A$brs", " $data->DocRefID_ControllingPerson_cpt");
                $objWorkSheet->setCellValue("B$brs", "$data->TIN_cpt");
                $objWorkSheet->setCellValue("C$brs", "$data->IssuedBy_cpt");
				$objWorkSheet->setCellValue("D$brs", "$data->TINType_cpt");
	

      // Rename sheet
      $objWorkSheet->setTitle("31 CtrlgPerson TIN");

	  
/*--31 CtrlgPerson TIN--*/
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/*--32 CtrlgPerson Address--*/
    $sheet = $objPHPExcel->getActiveSheet();

    //Start adding next sheets

      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(14); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_ControllingPerson')
            ->setCellValue('B2', 'legalAddressType')
            ->setCellValue('C2', 'CountryCode')
            ->setCellValue('D2', 'AddressFree')
            ->setCellValue('E2', 'Street')
            ->setCellValue('F2', 'BuildingIdentifier')
            ->setCellValue('G2', 'SuiteIdentifier')
            ->setCellValue('H2', 'FloorIdentifier')
            ->setCellValue('I2', 'DistrictName')
            ->setCellValue('J2', 'POB')
            ->setCellValue('K2', 'PostCode')
            ->setCellValue('L2', 'City')
            ->setCellValue('M2', 'CountrySubentity');
			
   $objWorkSheet->getStyle('A2:D2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(0)->getStyle('A2:M3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(0)->getStyle('A2:M3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);

/*
		DocRefID_ControllingPerson_cpa : '',
		legalAddressType_cpa : '',
		CountryCode_cpa : '',
		AddressFree_cpa : '',
		Street_cpa : '',
		BuildingIdentifier_cpa : '',
		SuiteIdentifier_cpa : '',
		FloorIdentifier_cpa : '',
		DistrictName_cpa :'',
		POB_cpa : '',
		PostCode_cpa : '',
		City_cpa : '',
		CountrySubentity_cpa : '',

*/
					$brs=3;
                $objWorkSheet->setCellValue("A$brs", " $data->DocRefID_ControllingPerson_cpa");
                $objWorkSheet->setCellValue("B$brs", "$data->legalAddressType_cpa");
                $objWorkSheet->setCellValue("C$brs", "ID");
				$objWorkSheet->setCellValue("D$brs", "$data->AddressFree_cpa");
				
				$objWorkSheet->setCellValue("E$brs", "$data->Street_cpa");
				$objWorkSheet->setCellValue("F$brs", "$data->BuildingIdentifier_cpa");
				$objWorkSheet->setCellValue("G$brs", "$data->SuiteIdentifier_cpa");
				$objWorkSheet->setCellValue("H$brs", "$data->FloorIdentifier_cpa");
				$objWorkSheet->setCellValue("I$brs", "$data->DistrictName_cpa");
				$objWorkSheet->setCellValue("J$brs", "$data->POB_cpa");
				$objWorkSheet->setCellValue("K$brs", "$data->PostCode_cpa");
				$objWorkSheet->setCellValue("L$brs", "$data->City_cpa");
				$objWorkSheet->setCellValue("M$brs", "$data->CountrySubentity_cpa");
	

      // Rename sheet
      $objWorkSheet->setTitle("32 CtrlgPerson Address");

	  
/*--32 CtrlgPerson Address--*/ 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
/*-- 33 CtrlgPerson Name --*/
    $sheet = $objPHPExcel->getActiveSheet();

    //Start adding next sheets

      // Add new sheet
      $objWorkSheet = $objPHPExcel->createSheet(15); //Setting index when creating

      //Write cells
  $objWorkSheet->setCellValue('A2', 'DocRefID_AccountReport')
            ->setCellValue('B2', 'PrecedingTitle')
            ->setCellValue('C2', 'Title')
			->setCellValue('D2', 'FirstName')
			->setCellValue('E2', 'FirstNameType')
			->setCellValue('F2', 'MiddleName')
			->setCellValue('G2', 'MiddleNameType')
			->setCellValue('H2', 'NamePrefix')
			->setCellValue('I2', 'NamePrefixType')
			->setCellValue('J2', 'LastName')
			->setCellValue('K2', 'LastNameType')
			->setCellValue('L2', 'GenerationIdentifier')
			->setCellValue('M2', 'Suffix')
			->setCellValue('N2', 'GeneralSuffix')
            ->setCellValue('O2', 'NameType');
			
   $objWorkSheet->getStyle('A2:O2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 			
			
/*
$objPHPExcel->getSheet(0)->getStyle('A2:O3')->getBorders()
->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getSheet(0)->getStyle('A2:O3')->getBorders()
->getAllBorders()->getColor()->setRGB('000000');
*/

	$objWorkSheet
    ->getColumnDimension('A')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('B')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('C')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('D')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('E')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('F')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('G')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('H')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('I')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('J')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('K')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('L')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('M')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('N')
    ->setAutoSize(true);
	$objWorkSheet
    ->getColumnDimension('O')
    ->setAutoSize(true);

/*
DocRefID_ControllingPerson_cpn : '',
		PrecedingTitle_cpn : '',
		Title_cpn : '',
		FirstName_cpn : '',
		FirstNameType_cpn : '',
		MiddleName_cpn : '',
		MiddleNameType_cpn :'',
		NamePrefix_cpn : '',
		NamePrefixType_cpn : '',
		LastName_cpn : '',
		LastNameType_cpn : '',
		GenerationIdentifier_cpn :'',
		Suffix_cpn : '',
		GeneralSuffix_cpn : '',
		NameType_cpn :'',

*/
					$brs=3;
	$val_1 = $data->DocRefID_ControllingPerson_cpn;
	$val_2 = $data->NameType_cpn;
	$val_3 = $data->FirstName_cpn;
                $objWorkSheet->setCellValue("A$brs", "$val_1");
				$objWorkSheet->setCellValue("B$brs", " ");
				$objWorkSheet->setCellValue("C$brs", " ");
				$objWorkSheet->setCellValue("D$brs", "$val_3");
				$objWorkSheet->setCellValue("E$brs", " ");
				$objWorkSheet->setCellValue("F$brs", " ");
				$objWorkSheet->setCellValue("G$brs", " ");
				$objWorkSheet->setCellValue("H$brs", " ");
				$objWorkSheet->setCellValue("I$brs", " ");
				$objWorkSheet->setCellValue("J$brs", " ");
				$objWorkSheet->setCellValue("K$brs", " ");
				$objWorkSheet->setCellValue("L$brs", " ");
				$objWorkSheet->setCellValue("M$brs", " ");
				$objWorkSheet->setCellValue("N$brs", " ");
				$objWorkSheet->setCellValue("O$brs", " ");

      // Rename sheet
      $objWorkSheet->setTitle("33 CtrlgPerson Name");

	  
/*--33 CtrlgPerson Name--*/
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=$filename");
header('Cache-Control: max-age=0');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');

//Replace php://output with $filename and add code below after that(before exit;)
//readfile($filename);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save($filename);
//echo json_encode(readfile($file));
readfile($filename);
exit;	

?>