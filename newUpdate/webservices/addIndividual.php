<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");

	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));
		$data = $param_POST->data;
		


if(ISSET($data)){
	 // Load librari PHPExcel nya
    require_once 'plugins/PHPExcel-1.8.1/Classes/PHPExcel.php';
	 $excelreader = new PHPExcel_Reader_Excel2007();
	$loadexcel = $excelreader->load('upload/add_NEW2.xlsx');
	$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
          
	$numrow = 0;
    $kosong = 0;
	$x= 0;
    foreach($sheet as $row){ 
	
	if($x == '0'){
		
		$cc = '';//20180909
	}
	else{
		
		//MENCARI TGL SUBSCRIBE DARI EXCELL ROW B
		$tahun = substr($row['B'],0,4);
		$bulan = substr($row['B'],4,2);
		$hari = substr($row['B'],6,2);
		$tgl_subs = $tahun."-".$bulan."-".$hari;
		$cus_name = $row['G']." ".$row['H']." ".$row['I'];
		//GET EXPIRED KTP
		$tahun_expired = substr($row['M'],0,4);
		$bulan_expired = substr($row['M'],4,2);
		$hari_expired = substr($row['M'],6,2);
		$expired_id = $tahun_expired."-".$bulan_expired."-".$hari_expired;
		//GET TGL LAHIR
		$tahun_lahir = substr($row['R'],0,4);
		$bulan_lahir = substr($row['R'],4,2);
		$hari_lahir = substr($row['R'],6,2);
		$tanggal_lahir = $tahun_lahir."-".$bulan_lahir."-".$hari_lahir;
		//GET JENIS KELAMIN
		$params_gender = $row['S'];

		if($params_gender == "Female"){
			$genders = "Perempuan";
		}
		else{
			$genders = "Laki-Laki";
		}
		//GET NATIONAL(KEWARGANEGARAAN)
		$c_o_n = $row['J'];
		if($c_o_n == "ID"){
			$cus_national = "WNI";
		}		
		else{
			$cus_national = "WNA";
			
		}
		//GET STATUS DOMISILI
		if($cus_national == "WNI"){
			$cus_status_domisili = "Dalam Negeri";
			
		}
		else{
			$cus_status_domisili = 'Luar Negeri';
			
		}
		
		//GET NPWP
		if(!ISSET($row["N"]) || EMPTY($row["N"]) ){
			$cus_npwp_1 = '';
			$cus_npwp_2 = '';
			$cus_npwp_3 = '';
			$cus_npwp_4 = '';
			$cus_npwp_5 = '';
			$cus_npwp_6 = '';
			
		}
		else{
			$cus_npwp_1 = substr($row['N'],0,2);
			$cus_npwp_2 = substr($row['N'],3,3);
			$cus_npwp_3 = substr($row['N'],5,3);
			$cus_npwp_4 = substr($row['N'],8,1);
			$cus_npwp_5 = substr($row['N'],9,3);
			$cus_npwp_6 = substr($row['N'],12,3);
			
			
			
		}
		//INSERT TO subs_redm
			$sql = " INSERT INTO tbl_kr_cus_sup (
						cus_code
						, cus_tgl_subscribe
						, cus_name
						, cus_card_identity
						, cus_no_identity
						, cus_status_identity
						, cus_berlaku_card
						, cus_tempat_lahir
						, cus_tgl_lahir
						, cus_sex
						, cus_mother_name
						, cus_agama
						, cus_national
						, cus_status_domisili
						, cus_status_kawin
						, cus_jumlah_tanggungan
						, cus_address
						, cus_city
						, cus_kode_pos
						, cus_propinsi
						, cus_telp
						, cus_phone
						, cus_fax
						, cus_email
						, cus_address_t
						, cus_city_t
						, cus_kode_pos_t
						, cus_propinsi_t
						, cus_status_rt
						, cus_menempati_t
						, cus_pendidikan
						, cus_pekerjaan
						, cus_company_name
						, cus_bd_usaha
						, cus_jabatan
						, cus_address_c
						, cus_telp_c
						, cus_phone_c
						, cus_city_c
						, cus_kode_pos_c
						, cus_propinsi_c
						, cus_fax_c
						, cus_email_c
						, cus_masa_usaha
						, cus_other 
						, cus_actived
						, cus_no_npwp_1
						, cus_no_npwp_2
						, cus_no_npwp_3
						, cus_no_npwp_4
						, cus_no_npwp_5
						, cus_no_npwp_6
						, cus_bank_name
						, cus_account_name
						, cus_account_number
						, cus_bank_name2
						, cus_account_name2
						, cus_account_number2
						, cus_sumber_dana
						, cus_maksud_tujuan
						, cus_penghasilan
						, cus_sid
						, middle_name
						, last_name
						, country_of_nationality
						, npwp_regis_date
						, country_of_birth
						, spouses_name
						, investors_risk_profile
						, asset_owner
						, bank_bic_code1
						, bank_bi_member_code1
						, bank_country1
						, bank_branch1
						, bank_ccy1
						, bank_bic_code2
						, bank_bi_member_code2
						, bank_country2
						, bank_branch2
						, bank_ccy2
						, bank_bic_code3
						, bank_bi_member_code3
						, bank_name3
						, bank_country3
						, bank_branch3
						, bank_ccy3
						, bank_acc_no3
						, bank_acc_name3
						, statement_type
						, fatca
						, tin
						, tin_issuance_country
						, country_of_correspondence
						, country_of_domicily
						, client_code
						, created_date
						, correspondence_type
						) VALUES
					( 
						' ',
						'$tgl_subs',
						'$cus_name',
						'$row[K]',
						'$row[L]',
						' ',
						'$expired_id',
						'$row[Q]',
						'$tanggal_lahir',
						'$genders',
						'$row[U]',
						'$row[V]',
						'$cus_national',
						'$cus_status_domisili',
						'$row[Y]',
						'-',
						'$row[AM]',
						'$row[AO]',
						'$row[AP]',
						'-',
						'$row[AR]',
						'$row[AS]',
						'$row[AT]',
						'$row[AU]',
						'$row[AH]',
						'$row[AJ]',
						'$row[AP]',
						'-',
						'$row[AD]',
						' ',
						'$row[T]',
						'$row[W]',
						'-',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						'1',
						'$cus_npwp_1',
						'$cus_npwp_2',
						'$cus_npwp_3',
						'$cus_npwp_4',
						'$cus_npwp_5',
						'$cus_npwp_6',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						'$row[AC]',
						'$row[AB]',
						'$row[X]',
						'$row[E]',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						' ',
						'$tgl_subs',
						' '
					
					)
						";
			$stmnt = $conn->query($sql);		
	if(!mysqli_error($conn)){
echo "BERHASIL<BR/>";

//insert to table tbl_kr_cus_sup
$ifua_code = $row["AZ"];

if($ifua_code == '0'){
	$type = 'Continue';
	
}
else{

				$sql_GET_IFUA = "SELECT cus_id,cus_sid from tbl_kr_cus_sup where cus_sid = '$row[E]' limit 1 ";
				$stmnt_GET_IFUA = $conn->query($sql_GET_IFUA);							
						if(!mysqli_error($conn)){
							while($row_GET_IFUA=mysqli_fetch_array($stmnt_GET_IFUA)){
								$cus_id = $row_GET_IFUA['cus_id'];
								//INSERT TO TABLE tbl_kr_cus_sup_ifua
			$sql = "INSERT INTO tbl_kr_cus_sup_ifua (cus_id, ifua_code, ifua_name, created_date, created_by, last_updated_date, last_updated_by, is_deleted) VALUES
					( 
						'$cus_id'
						,'$row[AZ]'
						,'$cus_name'
						,'$tgl_subs'
						,'1'
						,'$tgl_subs'
						,'1'
						,'0'
					)
						";
								$stmnt = $conn->query($sql);		
						if(!mysqli_error($conn)){
					echo "BERHASIL<BR/>";
							
						
						}
						else{

						echo "ERROR PADA SID:".$row["E"]."<br/>";
						echo "KESALAHAN PADA:".mysqli_error($conn)."<BR/> MENCARI LAST NAV";
						DIE();	
						
						}

													
							}
						
						
						}	
						else{
							echo "POSISI IFUA CODE:".$ifua_code."<br/>";
							echo "KESALAHAN PADA:".mysqli_error($conn)."<BR/> MENCARI LAST NAV";
							DIE();
						}	
	
	
	
	
}

		
	
	}
	else{
		echo "KESALAHAN PADA:".mysqli_error($conn);
		DIE();
	}	


            

	}
$x++;	
		  }
	

}

?>
