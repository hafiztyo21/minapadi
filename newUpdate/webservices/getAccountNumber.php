<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");
	session_start();
	
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
include_once 'config.php';

		$param_POST = json_decode(file_get_contents("php://input"));


	/*-- DECRYPT --*/

	//$data = (empty($_POST['data']))?trim($param_POST->data):trim($_POST['data']);
	//$Public_Key = substr($data,0,16);
	//$dec_data = CryptoAES::decrypt(substr($data,16), $Public_Key.$Private_Key, 256);
	//$data = json_decode($dec_data);
	$data = $param_POST->data;
	$fund_code = $data->fund_code;

	//print_r($data);
	
	if($data->typeNasabah == 'true'){
			$sql = "SELECT 
						A.cus_code
						,A.cus_name
						,A.cus_no_identity
						,A.cus_tgl_lahir
						,A.cus_bank_name
						,A.cus_account_name
						,A.cus_account_number
						,A.cus_bank_name2
						,A.cus_account_number2
						,A.cus_sid
						FROM tbl_kr_cus_sup A 
							LEFT JOIN (SELECT fund_code
												,cus_sid FROM tbl_kr_cus_ifua_balance WHERE fund_code = '".$fund_code."' AND trade_date >= '".$data->date_from."' and trade_date <= '".$data->date_to."' ) B
							ON 	A.cus_sid = B.cus_sid
							
						WHERE A.cus_no_identity = '".$data->identitas."'
						GROUP BY A.cus_sid	
						";
			$stmnt = $conn->query($sql);
		
					
		//print_r($conn->query($sql));
	}
	
	elseif($data->typeNasabah == 'false'){
		
			$sql = "SELECT 
						A.cus_code
						,A.cus_name
						,A.cus_no_npwp cus_no_identity
						,A.cus_tgl_pt cus_tgl_lahir
						
						,A.cus_bank_cab cus_bank_name
						,A.cus_pemilik_rek cus_account_name
						,A.cus_no_rek cus_account_number
						
						
						,A.cus_bank_cab_2 cus_bank_name2
						,A.cus_no_rek_2 cus_account_number2
						,A.cus_ins_sid cus_sid
						FROM tbl_kr_cus_institusi A 
							LEFT JOIN (SELECT fund_code
										,cus_sid FROM tbl_kr_cus_ifua_balance WHERE fund_code = '".$fund_code."' AND trade_date >= '".$data->date_from."' and trade_date <= '".$data->date_to."' ) B
							ON 	A.cus_ins_sid = B.cus_sid
						
						WHERE A.cus_no_npwp = '".$data->identitas."'						
						GROUP BY cus_ins_sid	
						";
			$stmnt = $conn->query($sql);		
		
		
		
	}	
	$outp = "";
	if(!mysqli_error($conn)){
	while($row=mysqli_fetch_array($stmnt)){
		if(!EMPTY($row['cus_sid']))
			{
				if(!EMPTY($row['cus_bank_name2']))
					{
						$sccNumber = $row['cus_sid'].'_'.$row['cus_account_number'].' | '.$row['cus_sid'].'_'.$row['cus_account_number2'];
				}
				else{
						$sccNumber = $row['cus_sid'].'_'.$row['cus_account_number'];
					
				}	
				
			}
		else{
				if(!EMPTY($row['cus_bank_name']))
					{		
						if(!EMPTY($row['cus_bank_name2']))
							{
								$sccNumber = 'NANUM_'.$row['cus_account_number'].' | '.$row['cus_sid'].'_'.$row['cus_account_number2'];
						}				
						else{
								$sccNumber = 'NANUM_'.$row['cus_account_number'];
							
						}
						
					}
				else{
						$sccNumber = 'NANUM_NANUM';
				}
				
			
			
			}	
		
		//echo $row['allocation'];
			if ($outp != "") {$outp .= ",";}
			$outp .= '{"sccNumber":"'.$sccNumber.'"}'; 
	
		
	};		
	//print_r($outp);
	$result = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	//print_r($result);
	}
	else{
		$result = '{ "status":"no", "message":"'.mysqli_error($conn).'", "records":[]}';
		
	}
	print_r($result);	
	//die(mysqli_error($db));

	//$Public_Key = getRandomKey(16);
	//$chiper = CryptoAES::encrypt($result, $Public_Key.$Private_Key, 256);
	//echo('{"data":'.json_encode($Public_Key.$chiper).'}');
	$conn = null;	
	
	
?>
