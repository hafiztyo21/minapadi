<?php
	session_start();
	$msg='';
	if( $_SESSION['emoney_valid']==true && (time()-$_SESSION['emoney_timeout'])<=900 )
	{
		header('Refresh: 0; URL = home.php');
		exit;
	} else 
	{
		unset($_SESSION["emoney_valid"]);
		unset($_SESSION["emoney_timeout"]);
		unset($_SESSION["emoney_username"]);
		unset($_SESSION["emoney_type"]);
		unset($_SESSION["emoney_merchant"]);
		unset($_SESSION["emoney_access"]);
	}
	if (!empty($_POST['username']) && !empty($_POST['password'])) 
	{	
		require_once('webservices/config.php');
		try {
			$conn = new PDO("mysql:host=localhost;dbname=$MySQL_DB;charset=utf8", $MySQL_USER, $MySQL_PASSWORD);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $conn->prepare("select v_user, v_logintype, v_merchantcode, n_menuaccess FROM ms_login WHERE v_active='Y' and v_user=? and v_password=?");
			$stmt->bindValue(1, trim(strtoupper($_POST['username'])), PDO::PARAM_STR);
			$stmt->bindValue(2, sha1($_POST['password']), PDO::PARAM_STR);
			$stmt->execute();
			$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$outp = "";
			foreach($res as $row)
			{
				$_SESSION['emoney_valid'] = true;
				$_SESSION['emoney_timeout'] = time();
				$_SESSION['emoney_username'] = strtoupper($_POST['username']);
				$_SESSION["emoney_type"] = $row["v_logintype"];
				$_SESSION["emoney_merchant"] = $row["v_merchantcode"];
				$_SESSION["emoney_access"] = $row["n_menuaccess"];
			}
			if(isset($stmt)) $stmt=null;
			if( isset($_SESSION["emoney_username"]) )
			{
				$conn = null;
				header('Refresh: 0; URL = home.php');
				exit;
			} else
			{
				$msg = 'Wrong username or password ';
			}
		}
		catch(PDOException $e) {
			$msg = $e->getMessage();
			$stmt= null;
		}
		$conn = null;
	}
	//require_once('dictionary.php'); 
?>

<!DOCTYPE html>
<html>
<head>
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<meta charset="UTF-8">

<title>Control Panel</title>
<style>
body {    
    background: #000000;
    font-family: Montserrat;
	
}

.login-position {
	position: fixed;
	top: 50%;
	left: 50%;
	margin-top: -132px;
	margin-left: -180px;
}
.login-block {
    width: 320px;
    padding: 20px;
    background: #fff;
    border-radius: 5px;    
    margin: 0 auto;	
}

.login-block h1 {
    text-align: center;
    color: #EC008C;
    font-size: 18px;
    text-transform: uppercase;
    margin-top: 0;
    margin-bottom: 20px;
}

.login-block img {	
    text-align: center;   
    margin-top: auto;
	margin-bottom: 20px;
}


.login-block input {
    width: 100%;
    height: 42px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #ccc;
    margin-bottom: 20px;
    font-size: 14px;
    font-family: Montserrat;
    padding: 0 20px 0 50px;
    outline: none;
}

.login-block input#username {
    background: #fff url('./images/user.png') 20px top no-repeat;
    background-size: 16px 80px;
}

.login-block input#username:focus {
    background: #fff url('./images/user.png') 20px bottom no-repeat;
    background-size: 16px 80px;
}

.login-block input#password {
    background: #fff url('./images/password.png') 20px top no-repeat;
    background-size: 16px 80px;
}

.login-block input#password:focus {
    background: #fff url('./images/password.png') 20px bottom no-repeat;
    background-size: 16px 80px;
}

.login-block input:active, .login-block input:focus {
    border: 1px solid #000000;
}

.login-block button {
    width: 100%;
    height: 40px;
    background: #000000;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #000000;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 14px;
    font-family: Montserrat;
    outline: none;
    cursor: pointer;
}

.login-block button:hover {
    background: #000000;
}

</style>
</head>

<body>

<div class="login-position">
	<div class="login-block">
		<form action ="#" method = "post">
			<input type="text" value="" placeholder="Username" id="username" name="username"/>
			<input type="password" value="" placeholder="Password" id="password" name="password"/>
			<button>Login</button>
			<center><h5> <?php echo $msg; ?></h5></center>
		</form>
	</div>
</div>
</body>

</html>
