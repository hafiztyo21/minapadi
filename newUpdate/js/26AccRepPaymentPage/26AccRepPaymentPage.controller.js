app.controller('26AccRepPaymentPageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

  $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 $scope.id = [];
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_AccountReport', field: 'DocRefID_AccountReport' }, 
			{ name: 'PaymentType', field: 'PaymentType' }, 
			{ name: 'CurrencyCodePayment', field: 'CurrencyCodePayment' }, 
			{ name: 'PaymentAmnt', field: 'PaymentAmnt' }, 

            {
                name: 'Action', field: 'npwp_no',
                cellTemplate: '<button type="button" ng-click="grid.appScope.edit(row.entity)" class="btn btn-primary btn-sm" "> Edit </button> <button type="button" ng-click="grid.appScope.hapus(row.entity.id)" class="btn btn-primary btn-sm" "> Hapus </button>'
            }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
		//if($scope.data.IsIndividual == 'true' || $scope.data.IsIndividual == '' ){
		//	return false;
		//}
			$http({
            method: "POST",
            url: webservicesUrl + "/getAccRepPayment.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {

	 		$scope.data.DocRefID_AccountReport_AccRepPayment =[];
		$scope.data.PaymentType =[];
		$scope.data.CurrencyCodePayment =[];
		$scope.data.PaymentAmnt =[];	
		$scope.data.idTemp_AccRepPayment = [];	

				for(var i=0, length=data.records.length;i<length;i++){	
		$scope.data.DocRefID_AccountReport_AccRepPayment.push(data.records[i].DocRefID_AccountReport );
		$scope.data.PaymentType.push(data.records[i].PaymentType);
		$scope.data.CurrencyCodePayment.push(data.records[i].CurrencyCodePayment);
		$scope.data.PaymentAmnt.push(data.records[i].PaymentAmnt);	
		$scope.data.idTemp_AccRepPayment.push(data.records[i].id);	
				}
				//console.log($scope.data.DocRefID_AccountReport_TIN_individual);
					$scope.gridOptions.data = data.records;
					console.log($scope.gridOptions.data);
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
		//console.log($scope.data.Timestamp);
		//console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	$scope.hapus = function(Item){
		var data = { 'crud' : '3','id': Item, 'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/hapusAccRepPayment.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);

	 		$scope.data.DocRefID_AccountReport_AccRepPayment =[];
		$scope.data.PaymentType =[];
		$scope.data.CurrencyCodePayment =[];
		$scope.data.PaymentAmnt =[];	
		$scope.data.idTemp_AccRepPayment = [];	

				for(var i=0, length=data.records.length;i<length;i++){	
		$scope.data.DocRefID_AccountReport_AccRepPayment.push(data.records[i].DocRefID_AccountReport );
		$scope.data.PaymentType.push(data.records[i].PaymentType);
		$scope.data.CurrencyCodePayment.push(data.records[i].CurrencyCodePayment);
		$scope.data.PaymentAmnt.push(data.records[i].PaymentAmnt);	
		$scope.data.idTemp_AccRepPayment.push(data.records[i].id);	
				}				
				$scope.getListData();

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
		
		
		
	}	
	
    $scope.add = function () {
        $state.go('AccRepPaymentForm', { data: { crud: '1' } });
    }	
	
    $scope.edit = function (Item) {
		//alert(Item.id);
		//return false;
		
		
		/*
	DocRefID_AccountReport : '',
	 	DocRefID_AccountReport : '';
		PaymentType :'',
		CurrencyCodePayment :'',
		PaymentAmnt : '',		
		
		*/
		
        $state.go('AccRepPaymentForm', { data: {crud: '2', id: Item.id, DocRefID_AccountReport: Item.DocRefID_AccountReport, PaymentType:Item.PaymentType, CurrencyCodePayment:Item.CurrencyCodePayment, PaymentAmnt : Item.PaymentAmnt  } });
		
	
		}		
	$scope.init = function(){
		$scope.getListData();
	}
	$scope.init();
	 
}]); 
