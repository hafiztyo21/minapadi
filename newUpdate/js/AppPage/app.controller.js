app.controller('appCtrl', ['$scope', '$state','$http','$timeout','uiGridConstants' ,'$rootScope', function ($scope, $state,$http,$timeout,uiGridConstants, $rootScope) {

    /*-- GLOBAL VARIABLE --*/
    $scope.language = languages[uselang];
    $scope.globallang = $scope.language['global'];
    $scope.menulang = $scope.language['menu'];
    $scope.pagelang = null;
    /*---------------------*/
	$scope.data = {
		date_froms : '',
		date_tos : '',
		date_from : '',
		date_to:'',
		pk_id : '0',
		allocation : '',
		description :'',
		bank :'',
		fund_code :'',
		npwp_no :'',
		fund_code : '',
		TransmittingCountry : 'ID',
		ReceivingCountry : 'ID',
		MessageType : 'DJP',
		Warning : 'Laporan periode setahun',
		Contact :'herawati_02129035050-sylviana_02129035050-ervina_02129035050',
		MessageRefId : '',
		MessageTypeIndic : 'DJP701',
		CorrMessageRefId : '',
		ReportingPeriod : '',
		Timestamp : '',
		//10 Reporting F1
		DocRefID_ReportingFI : '',
		ResCountryCode : 'ID',
		DocTypeIndic1  : 'DJP1',
		CorrDocRefID : '',
		//11 RepF1IN
		IN : '',
		IssuedBy : 'ID',
		INType : 'ID06',
	
		//12 REP F1 NAME
		Name : '',
		nameType : 'DJP207',
		//13 REP F1 ADRESS
		legalAddressType : 'DJP303',
		CountryCode : 'ID',
		AddressFree : '',
		Street : '',
		BuildingIdentifier : '',
		SuiteIdentifier : '',
		FloorIdentifier : '',
		DistrictName :'',
		POB : '',
		PostCode : '',
		City : '',
		CountrySubentity : '',
		//20 Account Report
		idTempAccRepAccRep : '',
		cus_name : '',
		DocTypeIndic_AccountReport : '',
		DocRefID_AccountReport : '',
		CorrMessageRefID_AccountReport : '',
		CorrDocRefID_AccountReport : '',
		AccountNumber : '',
		AccountNumberType : 'DJP602',
		UndocumentedAccount : 'true',
		ClosedAccount : 'false',
		DormantAccount : 'false',
		IsIndividual : '',
		AcctHolderType : 'DJP101',
		ResCountryCode : 'ID',
		Nationality : 'ID',
		CurrencyCodeAccountBalance : 'IDR',
		AccountBalance : '',
		BirthInfo_BirthDate : '',
		BirthInfo_City : '',
		BirthInfo_CountryCode : '',
		BirthInfo_CountryCode : '',
		//21AccRep TIN - individual
		idTempAccRep : '',
		DocRefID_AccountReport_TIN_individual : '',
		TIN : '',
		TINType_TIN_individual : '',
		IssuedBy_TIN_individual : '',
		
		//22AccRep IN- organization
		DocRefID_AccountReport_IN_organization : '',
		IN_IN_organization : '',
		INType_IN_organization : '',
		IssuedBy_IN_organization : '',
		idTempAccRep_IN_organization : '',
		
		//23 
		DocRefID_AccountReport_Name_Individual : '',
		PrecedingTitle : '',
		Title : '',
		FirstName : '',
		FirstNameType : '',
		MiddleName : '',
		MiddleNameType : '',
		NamePrefix : '',
		NamePrefixType : '',
		LastName : '',
		LastNameType : '',
		GenerationIdentifier : '',
		Suffix : '',
		GeneralSuffix : '',
		NameType_Name_Individual : 'DJP202',
		idTempAccRep_Name_Individual : '',
		
		//24
		DocRefID_AccountReport_Name_Organization : '',
		Name_Name_Organization : '',
		nameType_Name_Organization : '',
		idTemp_Name_organization : '',
		//25
		DocRefID_AccountReport_AccRepAddress : '',
		legalAddressType_AccRepAddress : '',
		CountryCode_AccRepAddress : '',
		AddressFree_AccRepAddress : '',
		Street_AccRepAddress : '',
		BuildingIdentifier_AccRepAddress : '',
		SuiteIdentifier_AccRepAddress : '',
		FloorIdentifier_AccRepAddress : '',
		DistrictName_AccRepAddress :'',
		POB_AccRepAddress : '',
		PostCode_AccRepAddress : '',
		City_AccRepAddress : '',
		CountrySubentity_AccRepAddress : '',
		//26
		DocRefID_AccountReport_AccRepPayment : '',
		PaymentType : '',
		CurrencyCodePayment : '',
		PaymentAmnt : '',
		//30
		DocRefID_AccountReport_cp : '',
		DocRefID_ControllingPerson_cp : '',
		ResCountryCode_cp : '',
		Nationality_cp : '',
		BirthInfo_BirthDate_cp : '',
		BirthInfo_City_cp : '',
		BirthInfo_CountryCode_cp : '',
		CtrlPersonType_cp : '',
		//31
		DocRefID_ControllingPerson_cpt : '',
		TIN_cpt : '',
		IssuedBy_cpt : 'ID',
		TINType_cpt : 'ID02',
		//32
		DocRefID_ControllingPerson_cpa : '',
		legalAddressType_cpa : '',
		CountryCode_cpa : '',
		AddressFree_cpa : '',
		Street_cpa : '',
		BuildingIdentifier_cpa : '',
		SuiteIdentifier_cpa : '',
		FloorIdentifier_cpa : '',
		DistrictName_cpa :'',
		POB_cpa : '',
		PostCode_cpa : '',
		City_cpa : '',
		CountrySubentity_cpa : '',
		//33
		DocRefID_ControllingPerson_cpn : '',
		PrecedingTitle_cpn : '',
		Title_cpn : '',
		FirstName_cpn : '',
		FirstNameType_cpn : '',
		MiddleName_cpn : '',
		MiddleNameType_cpn :'',
		NamePrefix_cpn : '',
		NamePrefixType_cpn : '',
		LastName_cpn : '',
		LastNameType_cpn : '',
		GenerationIdentifier_cpn :'',
		Suffix_cpn : '',
		GeneralSuffix_cpn : '',
		NameType_cpn :'',
		ifua_name:[],
		idTemp_cpn : [],
		idTemp_cpa : [],
		idTemp_cp : [],
		idd: [],
		mynpwp: [],
	//digunakan dalam page :
	//1. 000MessageSpecForm
	//2. 10ReportingF1Form
	//3. 20AccountReport
	visible : '0',
	//digunakan dalam page 
	//1. 10ReportingF1Form	
	visible2 : '0',
		}

		//20
		
				$scope.data.idTempAccRepAccRep = [];
				$scope.data.idd = [];
				$scope.data.ifua_name = [];
				$scope.data.cus_name = [];
				$scope.data.DocTypeIndic_AccountReport = [];
				$scope.data.DocRefID_AccountReport = [];
				$scope.data.CorrMessageRefID_AccountReport = [];
				$scope.data.CorrDocRefID_AccountReport = [];
				$scope.data.AccountNumber = [];
				$scope.data.AccountNumberType = [];
				$scope.data.UndocumentedAccount = [];
				$scope.data.ClosedAccount = [];
				$scope.data.DormantAccount = [];
				$scope.data.IsIndividual = [];
				$scope.data.AcctHolderType = [];
				$scope.data.ResCountryCode = [];
				$scope.data.Nationality = [];
				$scope.data.CurrencyCodeAccountBalance = [];
				$scope.data.AccountBalance = [];
				$scope.data.BirthInfo_BirthDate = [];
				$scope.data.BirthInfo_City = [];
				$scope.data.BirthInfo_CountryCode = [];
				$scope.data.BirthInfo_CountryCode = [];
				$scope.data.mynpwp = [];
		
		
		
		//21
		
		$scope.data.DocRefID_AccountReport_TIN_individual =[];
		$scope.data.TIN = [];
		$scope.data.TINType_TIN_individual = [];
		$scope.data.IssuedBy_TIN_individual = [];	
		$scope.data.idTempAccRep = [];
		
		//22
		$scope.data.DocRefID_AccountReport_IN_organization =[];
		$scope.data.IN_IN_organization = [];
		$scope.data.INType_IN_organization = [];
		$scope.data.IssuedBy_IN_organization = [];	
		$scope.data.idTempAccRep_IN_organization = [];
		
	//23
		$scope.data.idTempAccRep_Name_Individual = [];
		$scope.data.DocRefID_AccountReport_Name_Individual = [];
		$scope.data.NameType_Name_Individual = [];
		$scope.data.FirstName = [];
	//	$scope.data.PrecedingTitle = [];
	//	$scope.data.Title = [];
	//	$scope.data.FirstNameType = [];
	//	$scope.data.MiddleName = [];
	//	$scope.data.MiddleNameType = [];
	//	$scope.data.NamePrefix = [];
	//	$scope.data.NamePrefixType = [];
	//	$scope.data.LastName = [];
	//	$scope.data.LastNameType = [];
	//	$scope.data.GenerationIdentifier = [];
	//	$scope.data.Suffix = [];
	//	$scope.data.GeneralSuffix = [];
			
	
	
	
	//24
		$scope.data.DocRefID_AccountReport_Name_Organization = [];
		$scope.data.Name_Name_Organization = [];
		$scope.data.nameType_Name_Organization = [];
		$scope.data.idTemp_Name_organization = [];
	
	
	//25
			//25
		$scope.data.DocRefID_AccountReport_AccRepAddress =[];
		$scope.data.legalAddressType_AccRepAddress =[];
		$scope.data.CountryCode_AccRepAddress =[];
		$scope.data.AddressFree_AccRepAddress =[];
		$scope.data.idTemp_AccRepAddress = [];
		

	
	//26
		$scope.data.DocRefID_AccountReport_AccRepPayment =[];
		$scope.data.PaymentType =[];
		$scope.data.CurrencyCodePayment =[];
		$scope.data.PaymentAmnt =[];
		$scope.data.idTemp_AccRepPayment = [];		




	//30
		$scope.data.DocRefID_AccountReport_cp = [];
		$scope.data.DocRefID_ControllingPerson_cp = [];
		$scope.data.ResCountryCode_cp = [];
		$scope.data.Nationality_cp = [];
		$scope.data.BirthInfo_BirthDate_cp = [];
		$scope.data.BirthInfo_City_cp = [];
		$scope.data.BirthInfo_CountryCode_cp = [];
		$scope.data.CtrlPersonType_cp = [];
		$scope.data.idTemp_cp = [];	
		
	
	
	
		//31

		$scope.data.DocRefID_ControllingPerson_cpt = [];	
		$scope.data.TIN_cpt = [];	
		$scope.data.IssuedBy_cpt = [];
		$scope.data.TINType_cpt = [];
		$scope.data.idTemp_cpt = [];
		//32
		$scope.data.DocRefID_ControllingPerson_cpa = [];	
		$scope.data.legalAddressType_cpa = [];	
		$scope.data.CountryCode_cpa = [];	
		$scope.data.AddressFree_cpa = [];	
		$scope.data.idTemp_cpa = [];

		//33
		$scope.data.DocRefID_ControllingPerson_cpn = [];
		$scope.data.FirstName_cpn = [];		
		$scope.data.LastName_cpn = [];
		$scope.data.LastNameType_cpn = [];
		$scope.data.NameType_cpn = [];			
		$scope.data.idTemp_cpn = [];	
		$scope.data.MiddleName_cpn = [];
		$scope.data.LastName_cpn= [];
	
	
	
	/* MENU SHOW */
	$scope.menu = 0;
	$scope.loading = 0;
	$scope.x = 0; 
	$scope.y = 0;
	$scope.tempId_Id = 0;
	$scope.j_cp = 1;
	
	/*-- DATE -- */
	
	    $scope.dateOptions = {
        //dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(),
        //minDate: new Date(),
        startingDay: 1
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.filter = {
        fromdate : new Date(),
        todate: new Date(),
        accountno: '0'
    }
	
	/*----------------------------*/
	
	
	/*-- GET ALLOCATION --*/
	$scope.allocationList = [];
	$scope.getAllocationList = function(){
        $http({
            method: "POST",
            url: webservicesUrl + "/getAllocation.php",
            data: { 'data': '' },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
			
//			console.log(response.data);
            $scope.gridIsLoading = false;
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
//			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
                $scope.allocationList = data.records;
				console.log($scope.allocationList);
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	
	}	
	
	/*-- GENERATE NASABAH -- */
	$scope.GenerateAll = function(){
        $http({
            method: "POST",
            url: webservicesUrl + "/generateAll.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            $scope.gridIsLoading = false;
            //var data = CRYPTO.decrypt(response.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				$scope.j_cp = 1;
               // $scope.allocationList = data.records;
				/*20

				}
				
				20
				
				*/
				$scope.data.idTempAccRepAccRep = [];
				$scope.data.cus_name = [];
				$scope.data.DocTypeIndic_AccountReport = [];
				$scope.data.DocRefID_AccountReport = [];
				$scope.data.CorrMessageRefID_AccountReport = [];
				$scope.data.idd = [];
				$scope.data.mynpwp = [];
				$scope.data.ifua_name = [];
				$scope.data.CorrDocRefID_AccountReport = [];
				$scope.data.AccountNumber = [];
				$scope.data.AccountNumberType = [];
				$scope.data.UndocumentedAccount = [];
				$scope.data.ClosedAccount = [];
				$scope.data.DormantAccount = [];
				$scope.data.IsIndividual = [];
				$scope.data.AcctHolderType = [];
				$scope.data.ResCountryCode = [];
				$scope.data.Nationality = [];
				$scope.data.CurrencyCodeAccountBalance = [];
				$scope.data.AccountBalance = [];
				$scope.data.BirthInfo_BirthDate = [];
				$scope.data.BirthInfo_City = [];
				$scope.data.BirthInfo_CountryCode = [];
				$scope.data.BirthInfo_CountryCode = [];
				
				
				
					/*--21--*/
					$scope.data.DocRefID_AccountReport_TIN_individual =[];
					$scope.data.TIN = [];
					$scope.data.TINType_TIN_individual = [];
					$scope.data.IssuedBy_TIN_individual = [];	
					$scope.data.idTempAccRep = [];
					
					/*--21--*/
					
					

					/*--22--*/
					$scope.data.DocRefID_AccountReport_IN_organization =[];
					$scope.data.IN_IN_organization = [];
					$scope.data.INType_IN_organization = [];
					$scope.data.IssuedBy_IN_organization = [];	
					$scope.data.idTempAccRep_IN_organization = [];
						
					/*--22--*/	

					
					
					
					/*--23--*/
					$scope.data.idTempAccRep_Name_Individual = [];
					$scope.data.DocRefID_AccountReport_Name_Individual = [];
					$scope.data.NameType_Name_Individual = [];
					$scope.data.FirstName = [];
					$scope.data.MiddleNameType = [];
					$scope.data.LastName = [];
					/*--23--*/


					
					

					/*--24--*/	
					$scope.data.DocRefID_AccountReport_Name_Organization = [];
					$scope.data.Name_Name_Organization = [];
					$scope.data.nameType_Name_Organization = [];
					$scope.data.idTemp_Name_organization = [];
						
					/*--24--*/					
				
				
				
				
					/*--25--*/
					$scope.data.DocRefID_AccountReport_AccRepAddress =[];
					$scope.data.legalAddressType_AccRepAddress =[];
					$scope.data.CountryCode_AccRepAddress =[];
					$scope.data.AddressFree_AccRepAddress =[];
					$scope.data.idTemp_AccRepAddress = [];					
					/*--25--*/

					/*--26--*/
					$scope.data.DocRefID_AccountReport_AccRepPayment =[];
					$scope.data.PaymentType =[];
					$scope.data.CurrencyCodePayment =[];
					$scope.data.PaymentAmnt =[];
					$scope.data.idTemp_AccRepPayment = [];	
					/*--26--*/
									
					/*--30--*/	
						$scope.data.DocRefID_AccountReport_cp = [];
						$scope.data.DocRefID_ControllingPerson_cp = [];
						$scope.data.ResCountryCode_cp = [];
						$scope.data.Nationality_cp = [];
						$scope.data.BirthInfo_BirthDate_cp = [];
						$scope.data.BirthInfo_City_cp = [];
						$scope.data.BirthInfo_CountryCode_cp = [];
						$scope.data.CtrlPersonType_cp = [];
						$scope.data.idTemp_cp = [];	
					/*--30--*/			


					/*--31--*/
						$scope.data.DocRefID_ControllingPerson_cpt = [];	
						$scope.data.TIN_cpt = [];	
						$scope.data.IssuedBy_cpt = [];
						$scope.data.TINType_cpt = [];										
					/*--31--*/
					
					
					
					
					/*--32--*/
						$scope.data.DocRefID_ControllingPerson_cpa = [];	
						$scope.data.legalAddressType_cpa = [];	
						$scope.data.CountryCode_cpa = [];	
						$scope.data.AddressFree_cpa = [];	
						$scope.data.Street_cpa = [];	
						$scope.data.BuildingIdentifier_cpa = [];	
						$scope.data.SuiteIdentifier_cpa = [];	
						$scope.data.FloorIdentifier_cpa = [];	
						$scope.data.DistrictName_cpa =[];
						$scope.data.POB_cpa = [];	
						$scope.data.PostCode_cpa = [];	
						$scope.data.City_cpa = [];	
						$scope.data.CountrySubentity_cpa = [];	
						$scope.data.idTemp_cpa = [];					
					
					
					/*--32--*/
					
					
					/*--33--*/
						$scope.data.DocRefID_ControllingPerson_cpn = [];	
						$scope.data.PrecedingTitle_cpn = [];	
						$scope.data.Title_cpn = [];	
						$scope.data.FirstName_cpn = [];	
						$scope.data.FirstNameType_cpn = [];	
						$scope.data.MiddleName_cpn = [];	
						$scope.data.MiddleNameType_cpn =[];
						$scope.data.NamePrefix_cpn = [];	
						$scope.data.NamePrefixType_cpn = [];	
						$scope.data.LastName_cpn = [];
						$scope.data.LastNameType_cpn = [];
						$scope.data.GenerationIdentifier_cpn = [];
						$scope.data.Suffix_cpn = [];
						$scope.data.GeneralSuffix_cpn = [];
						$scope.data.NameType_cpn = [];			
						$scope.data.idTemp_cpn = [];		
	
		$scope.data.MiddleName_cpn = [];
		$scope.data.LastName_cpn= [];						
					/*--33--*/
				for(var i=0, length=data.records.length;i<length;i++){
					$scope.data.cus_name.push(data.records[i].cus_name);
					$scope.data.mynpwp.push(data.records[i].mynpwp);
					$scope.data.idTempAccRepAccRep.push(data.records[i].idtrx);
					//$scope.data.cus_name.push(data.records[i].cus_name);
					$scope.data.DocTypeIndic_AccountReport.push('DJP1');
					$scope.data.DocRefID_AccountReport.push(data.records[i].idAccountReport);
					$scope.data.CorrMessageRefID_AccountReport.push('');
					$scope.data.CorrDocRefID_AccountReport.push('');
					$scope.data.AccountNumber.push(data.records[i].accountNumber);
					$scope.data.AccountNumberType.push('DJP602');
					$scope.data.UndocumentedAccount.push('FALSE');
					$scope.data.ClosedAccount.push('FALSE');
					$scope.data.DormantAccount.push('FALSE');
					$scope.data.ResCountryCode.push("ID");
					$scope.data.idd.push(data.records[i].identitas);
					$scope.data.ifua_name.push(data.records[i].ifua_name);
					$scope.data.CurrencyCodeAccountBalance.push("IDR");
					$scope.data.AccountBalance.push(data.records[i].balance);
					$scope.data.BirthInfo_City.push(data.records[i].tempat_lahir_cus);
					
					
					
					/*--25--*/
					$scope.data.DocRefID_AccountReport_AccRepAddress.push(data.records[i].idAccountReport);
					$scope.data.CountryCode_AccRepAddress.push("ID");
					$scope.data.AddressFree_AccRepAddress.push(data.records[i].cus_alamat);
					$scope.data.idTemp_AccRepAddress.push(data.records[i].idtrx);					
					/*--25--*/					
					
					/*--26--*/
					$scope.data.DocRefID_AccountReport_AccRepPayment.push(data.records[i].idAccountReport);
					$scope.data.PaymentType.push("DJP503");
					$scope.data.CurrencyCodePayment.push("IDR");
					$scope.data.PaymentAmnt.push("0.00");
					$scope.data.idTemp_AccRepPayment.push(data.records[i].idtrx);
					/*--26--*/
					
					
					if(data.records[i].isIndividu == '1'){
						$scope.data.IsIndividual.push("TRUE");
						$scope.data.AcctHolderType.push(' ');
						$scope.data.BirthInfo_BirthDate.push(data.records[i].tgl_lahirCus_berdiriIns);
						$scope.data.BirthInfo_CountryCode.push("ID");
						$scope.data.Nationality.push("ID");
						
						//--21--//
						$scope.data.DocRefID_AccountReport_TIN_individual.push(data.records[i].idAccountReport);
						$scope.data.TIN.push(data.records[i].cus_ktp_siup);
						$scope.data.TINType_TIN_individual.push("ID02");
						$scope.data.IssuedBy_TIN_individual.push("ID");	
						$scope.data.idTempAccRep.push($scope.x);						
						
						//--21--//
						
					/*--23--*/
						$scope.data.idTempAccRep_Name_Individual.push($scope.x);
						$scope.data.DocRefID_AccountReport_Name_Individual.push(data.records[i].idAccountReport);
						$scope.data.NameType_Name_Individual.push("DJP202");
						$scope.data.FirstName.push(data.records[i].first_name);
						$scope.data.MiddleNameType.push(data.records[i].midle_name);
						$scope.data.LastName.push(data.records[i].last_name);
					/*--23--*/						
						
						
						
					/*--25--*/
						$scope.data.legalAddressType_AccRepAddress.push("DJP301");
					/*--25--*/					
						
					}
					else{
						$scope.data.IsIndividual.push("FALSE");
						$scope.data.AcctHolderType.push("DJP101");
						$scope.data.BirthInfo_BirthDate.push(" ");
						$scope.data.BirthInfo_CountryCode.push(" ");
						$scope.data.Nationality.push(" ");
						//--22--//
						$scope.data.IssuedBy_IN_organization.push("ID");
						$scope.data.DocRefID_AccountReport_IN_organization.push(data.records[i].idAccountReport);
						$scope.data.IN_IN_organization.push(data.records[i].cus_ktp_siup);
						if(data.records[i].cus_ktp_siup != ""){
								$scope.data.INType_IN_organization.push("ID06");
							}
						else{
								$scope.data.INType_IN_organization.push("ID06");
							}
						$scope.data.idTempAccRep_IN_organization.push($scope.y);
							//--22--//		


					/*--24--*/	
					$scope.data.DocRefID_AccountReport_Name_Organization.push(data.records[i].idAccountReport);
					$scope.data.Name_Name_Organization.push(data.records[i].cus_name);
					$scope.data.nameType_Name_Organization.push("DJP207");
					$scope.data.idTemp_Name_organization.push($scope.y);
						
					/*--24--*/
						

					/*--25--*/
						$scope.data.legalAddressType_AccRepAddress.push("DJP303");
					/*--25--*/						
						
						
					/*--30--*/	

					//	$scope.data.idTemp_cp = [];	
					$scope.jcp = Number(data.records[i].jcp);
					$scope.jcp = Number(1);
					if($scope.jcp > 0){
						/*--30--*/
						$scope.tempId_cp = [];
						$scope.tempCusdireksi = [];
						$scope.tempCusdireksi.push(data.records[i].cus_direksi);
						$scope.tempCusdireksi.push(data.records[i].cus_direksi_2);
						$scope.tempCusdireksi.push(data.records[i].cus_direksi_3);
						$scope.tempBirthInfo_BirthDate_cp = [];
						$scope.tempBirthInfo_BirthDate_cp.push(data.records[i].cus_direksi_tgl);
						$scope.tempBirthInfo_BirthDate_cp.push(data.records[i].cus_direksi_tgl_2);
						$scope.tempBirthInfo_BirthDate_cp.push(data.records[i].cus_direksi_tgl_3);
						/*--30--*/
						
						
						
						/*--31--*/
					/*

						
					*/						
						$scope.tempCusdireksiktp =[];
						$scope.tempCusdireksiktp.push(data.records[i].cus_direksi_ktp);
						$scope.tempCusdireksiktp.push(data.records[i].cus_direksi_ktp_2);
						$scope.tempCusdireksiktp.push(data.records[i].cus_direksi_ktp_3);
						/*--31--*/
						
						
						
					/*--32--*/

					/*--32--*/
					
					
					/*--33--*/
	
					$scope.tempFirstName_cpn = [];
					$scope.tempFirstName_cpn.push(data.records[i].cus_direksi_first);
					$scope.tempFirstName_cpn.push(data.records[i].cus_direksi_first_2);
					$scope.tempFirstName_cpn.push(data.records[i].cus_direksi_first_3);
	
		$scope.data.MiddleName_cpn.push(data.records[i].cus_direksi_middle);
		$scope.data.LastName_cpn.push(data.records[i].cus_direksi_last);					
					/*--33--*/						
						
						
						
						
						
						
						
						
						
						
						
						for(var j=0; j<$scope.jcp;j++){
							
							if($scope.j_cp > 0 && $scope.j_cp < 10){
							$scope.generateCP = data.records[i].idAccountReport+"_CP_00"+ $scope.j_cp;
							}
						else if($scope.j_cp > 9 && $scope.j_cp < 100){
							
							$scope.generateCP = data.records[i].idAccountReport+"_CP_0"+$scope.j_cp;
						}
						else{
							$scope.generateCP = data.records[i].idAccountReport+"_CP_"+$scope.j_cp;
							
							
						}
							/*--30--*/
							$scope.data.BirthInfo_BirthDate_cp.push($scope.tempBirthInfo_BirthDate_cp[j]);
							$scope.data.DocRefID_ControllingPerson_cp.push($scope.generateCP);
							$scope.data.DocRefID_AccountReport_cp.push(data.records[i].idAccountReport);
							$scope.data.ResCountryCode_cp.push("ID");
							$scope.data.Nationality_cp.push("ID");
							$scope.data.BirthInfo_City_cp.push("");
							$scope.data.BirthInfo_CountryCode_cp.push("");
							$scope.data.CtrlPersonType_cp.push("DJP801");
							$scope.data.idTemp_cp.push($scope.tempId_Id);
							/*--30--*/
							
							
							
							/*--31--*/
							$scope.data.DocRefID_ControllingPerson_cpt.push($scope.generateCP);
							$scope.data.TIN_cpt.push($scope.tempCusdireksiktp[j]);	
							$scope.data.IssuedBy_cpt.push("ID");
							$scope.data.TINType_cpt.push("ID02");			
							$scope.data.idTemp_cpt.push($scope.tempId_Id);
							/*--31--*/
							
							
							/*--32--*/
							$scope.data.DocRefID_ControllingPerson_cpa.push($scope.generateCP);	
							$scope.data.legalAddressType_cpa.push("DJP303");	
							$scope.data.CountryCode_cpa.push("ID");	
							//$scope.data.AddressFree_cpa.push(data.records[i].cus_alamat);	
							$scope.data.AddressFree_cpa.push('Undocumented');
							$scope.data.idTemp_cpa.push($scope.tempId_Id);		
							
							
							/*--32--*/
							
							/*--33--*/
							$scope.data.DocRefID_ControllingPerson_cpn.push($scope.generateCP);
							$scope.data.FirstName_cpn.push($scope.tempFirstName_cpn[j]);
							$scope.data.NameType_cpn.push("DJP202");			
							$scope.data.idTemp_cpn.push($scope.tempId_Id);															
							/*--33--*/
							
							
							
							
							$scope.tempId_Id++;
							$scope.j_cp++;
						}
					}
					
						
						
						}
		
				$scope.x = $scope.x + 1;
				$scope.y = $scope.y + 1;
				}
				
				console.log($scope.data);
				//console.log($scope.allocationList);
				$scope.menu = 1;
				$scope.loading  = 0;
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			
		
		
		
		
	}
	
	
	
	
	
	
	
	/*---------------------------*/
	
/*--COMPILE--*/
$scope.compile = function(){
		if($scope.data.pk_id == '0'){
			alert('Allocation Harus diisi');
			return false;
		}
		if($scope.data.date_tos == ''){
			alert('Period Tanggal Akhir Harus diisi');
			return false;
		}		
		if($scope.data.date_froms == ''){
			alert('Period Tanggal Awal Harus diisi');
			return false;
		}			
		$scope.loading = 1;
		$scope.menu = 0;
		$state.go('homepage');
	    var data = { 'datefrom' : $scope.data.date_froms+'00:00:00','dateto'  : $scope.data.date_tos+'23:59:00' , 'id': $scope.data.pk_id };
		console.log($scope.data.date_to);
        var jsonData = CRYPTO.encrypt(data);
	//	console.log(jsonData);
   
        $http({
            method: "POST",
            url: webservicesUrl + "/getMasterCompile.php",
            data: { 'data':data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            $scope.gridIsLoading = false;
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
					data.records = $scope.urlDecode(data.records);
					$scope.data.npwp_no = data.records[0].npwp_no;
					$scope.data.date_from = data.records[0].dateFrom;
					$scope.data.date_to = data.records[0].dateTo;
					$scope.data.MessageRefId = data.records[0].MessageRefId;
					$scope.data.Timestamp =data.records[0].timeStamp;
					$scope.data.ReportingPeriod =data.records[0].ReportingPeriod;
					$scope.data.fund_code = data.records[0].fund_code;
					//10ReportingF1
					$scope.data.DocRefID_ReportingFI = data.records[0].DocRefID_ReportingFI;
					
					//11RepF1IN
					//if($scope.data.IN == ''){
						$scope.data.IN = $scope.data.npwp_no;
					//}
					//12RepF1Name
					$scope.data.Name =data.records[0].Name;
					//13RepF1Address
					$scope.data.AddressFree =data.records[0].AddressFree;			
					//20 Account Report 
					$scope.data.DocTypeIndic_AccountReport = data.records[0].DocTypeIndic_AccountReport;
					console.log($scope.data);
					
					
					
					
				$scope.GenerateAll();
				//$scope.getAllocationList();
					
					
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	

	
}	
	
	
//$--EXPORT --//

$scope.export = function(){
		var explode = $scope.data.ReportingPeriod.split("-");
	console.log(explode);
	var title = $scope.data.npwp_no+'-ID-'+explode[0]+'-0.xlsx';
            $http({
                 url: webservicesUrl + "/export.php",
				data: { 'data':$scope.data },
                method: 'POST',
                responseType: 'arraybuffer',
                headers: {
                    'Content-type': 'application/json',
                    'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                }
            }).then(function mySuccess(response) {
                var blob = new Blob([response.data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
				console.log(response);
                saveAs(blob, title);
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });
		
        }		
		
		

		/*
	    var data = { 'datefrom' : $scope.data.date_from+'00:00:00','dateto'  : $scope.data.date_to+'23:59:00' , 'id': $scope.data.pk_id };
		console.log($scope.data.date_to);
        var jsonData = CRYPTO.encrypt(data);
	//	console.log(jsonData);
   
        $http({
            method: "POST",
            url: webservicesUrl + "/export.php",
            data: { 'data':$scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            $scope.gridIsLoading = false;
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
					alert('data berhasil di export');
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	

	/*
}	
	
	
	
    /*-- GLOBAL FUNCTION --*/
    $scope.changePage = function (page) {
	if( page=="logout") {
		window.location.href="logout.php";
	}
        $state.go(page)
    }

    $scope.convertJsDateToString = function (date) {
        var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        var month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
        var year = date.getFullYear();

        return year + '-' + month + '-' + day;
    }

    $scope.urlDecode = function(data){
        for(var i=0, length=data.length;i<length;i++) {
            for ( var temp in data[i] ) {
                data[i][temp] = decodeURIComponent(data[i][temp]);
            }
        }
        return data;
    }
    /*---------------------*/
	

    /*---GLOBAL EVENT--*/
    /*
    EVENT WHEN CHANGE STATE, FIRED WHEN LOADING CONTENT
    */
    $rootScope.$on('$viewContentLoading',
        function (event, viewConfig) {
            // Access to all the view config properties.
            // and one special property 'targetView'
            // viewConfig.targetView 
            if ($state.current.name != '') {
                if ($scope.language[$state.current.name] != undefined)
                    $scope.pagelang = $scope.language[$state.current.name];
                else
                    $scope.pagelang = null;
            }
        });
    /*-----------------*/
	
	
	

	$scope.init = function(){
		$scope.getAllocationList();
	}
$scope.init();	
	
	
	
}]); 



	$('.class').click(function(){
		//$('.class').removeClass('activeVid');
		//$(this).addClass('activeVid');
		alert('123');
		
		
	});
	
	
	
