app.controller('23AccRepName-IndividualPageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

  $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 $scope.id = [];
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ReportingFI', field: 'DocRefID_AccountReport',width:200 }, 
            { name: 'PrecedingTitle', field: 'PrecedingTitle',width:200 },
			{ name: 'Title', field: 'Title',width:200 },
			{ name: 'FirstName', field: 'FirstName',width:200 },
			{ name: 'FirstNameType', field: 'FirstNameType',width:200 },
			{ name: 'MiddleName', field: 'MiddleName',width:200 },
			{ name: 'MiddleNameType', field: 'MiddleNameType',width:200 },
			{ name: 'NamePrefixType', field: 'NamePrefixType',width:200 },
			{ name: 'LastName', field: 'LastName',width:200 },
			{ name: 'LastNameType', field: 'LastNameType',width:200 },
			{ name: 'GenerationIdentifier', field: 'GenerationIdentifier',width:200 },
			{ name: 'Suffix', field: 'Suffix',width:200 },
			{ name: 'GeneralSuffix', field: 'GeneralSuffix',width:200 },
			{ name: 'FirstName', field: 'FirstName',width:200 },
			{ name: 'NameType_Name_Individual', field: 'NameType_Name_Individual',width:200 },
       //     {
        //        name: 'Action', field: 'npwp_no',width:200,
         //       cellTemplate: '<button type="button" ng-click="grid.appScope.edit(row.entity)" class="btn btn-primary btn-sm" "> Edit </button> <button type="button" ng-click="grid.appScope.hapus(row.entity.id)" class="btn btn-primary btn-sm" "> Hapus </button>'
          //  }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
		if($scope.data.IsIndividual == 'false' || $scope.data.IsIndividual == '' ){
			return false;
		}
			$http({
            method: "POST",
            url: webservicesUrl + "/getAccRepName-Individual.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
		$scope.data.idTempAccRep_Name_Individual = [];
		$scope.data.DocRefID_AccountReport_Name_Individual = [];
		$scope.data.NameType_Name_Individual = [];
		$scope.data.FirstName = [];		
				for(var i=0, length=data.records.length;i<length;i++){	
					$scope.data.idTempAccRep_Name_Individual.push(data.records[i].id);
					$scope.data.DocRefID_AccountReport_Name_Individual.push(data.records[i].DocRefID_AccountReport);
					$scope.data.FirstName.push(data.records[i].FirstName);
					$scope.data.NameType_Name_Individual.push('DJP202');			
				}
				//console.log($scope.data.DocRefID_AccountReport_TIN_individual);
					$scope.gridOptions.data = data.records;
					console.log($scope.gridOptions.data);
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
		//console.log($scope.data.Timestamp);
		//console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	$scope.hapus = function(Item){
		var data = { 'crud' : '3','id': Item, 'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/hapusAccRepName-Individual.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);
		$scope.data.idTempAccRep_Name_Individual = [];
		$scope.data.DocRefID_AccountReport_Name_Individual = [];
		$scope.data.NameType_Name_Individual = [];
		$scope.data.FirstName = [];		
				for(var i=0, length=data.records.length;i<length;i++){	
					$scope.data.idTempAccRep_Name_Individual.push(data.records[i].id);
					$scope.data.DocRefID_AccountReport_Name_Individual.push(data.records[i].DocRefID_AccountReport);
					$scope.data.FirstName.push(data.records[i].FirstName);
					$scope.data.NameType_Name_Individual.push('DJP202');			
				}				
				$scope.getListData();

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
		
		
		
	}	
	
    $scope.add = function () {
        $state.go('AccRepName-IndividualForm', { data: { crud: '1' } });
    }	
	
    $scope.edit = function (Item) {
		//alert(Item.id);
		//return false;
        $state.go('AccRepName-IndividualForm', { data: {crud: '2', id: Item.id, FirstName:Item.FirstName,DocRefID_AccountReport : Item.DocRefID_AccountReport, NameType_Name_Individual : Item.NameType_Name_Individual  } });
		
	
		}		
	$scope.init = function(){
		$scope.getListData();
	}
	$scope.init();
	 
}]); 
