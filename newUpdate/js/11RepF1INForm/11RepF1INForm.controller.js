app.controller('11RepF1INFormCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

$scope.jenisLaporan = function(Item){
	if(Item == "ID06"){
		$scope.dataLocal.IN = $scope.data.npwp_no;
	}
	else{
		$scope.dataLocal.IN = '';
	}	
}


$scope.cancel = function(){
	$state.go('RepF1IN');
}

$scope.save = function(){
	if( confirm ("Are you sure want to save ?") == true ) {
	
	$scope.data.IN = $scope.dataLocal.IN;
	$scope.data.INType = $scope.dataLocal.INType;
		$state.go('RepF1IN');
	}
}


  $scope.init = function(){ 
	    if ($stateParams.data != null) {
            //$scope.dataLocal = $scope.data;
			$scope.dataLocal = JSON.parse(JSON.stringify($scope.data));

        } else {
            $state.go('homepage');
        }
	  
	  
  }
  $scope.init();
	
	 
}]); 
