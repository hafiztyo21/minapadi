app.controller('30ControllingPersonPageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

  $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 
	 

	 
	 $scope.id = [];
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_AccountReport', field: 'DocRefID_AccountReport' }, 
            { name: 'DocRefID_ControllingPerson', field: 'DocRefID_ControllingPerson' },
			{ name: 'ResCountryCode', field: 'ResCountryCode' },
			{ name: 'Nationality', field: 'Nationality' },
			{ name: 'BirthInfo_BirthDate', field: 'BirthInfo_BirthDate' },
			{ name: 'BirthInfo_City', field: 'BirthInfo_City' },
			{ name: 'BirthInfo_CountryCode', field: 'BirthInfo_CountryCode' },
			{ name: 'CtrlPersonType', field: 'CtrlPersonType' },
            {
                name: 'Action', field: 'npwp_no',
                cellTemplate: '<button type="button" ng-click="grid.appScope.edit(row.entity)" class="btn btn-primary btn-sm" "> Edit </button> <button type="button" ng-click="grid.appScope.hapus(row.entity.id)" class="btn btn-primary btn-sm" "> Hapus </button>'
            }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
		if($scope.data.IsIndividual == 'true' || $scope.data.IsIndividual == '' ){
			return false;
		}
			$http({
            method: "POST",
            url: webservicesUrl + "/getControllingPerson.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
		$scope.data.DocRefID_AccountReport_cp = [];
		$scope.data.DocRefID_ControllingPerson_cp = [];
		$scope.data.ResCountryCode_cp = [];
		$scope.data.Nationality_cp = [];
		$scope.data.BirthInfo_BirthDate_cp = [];
		$scope.data.BirthInfo_City_cp = [];
		$scope.data.BirthInfo_CountryCode_cp = [];
		$scope.data.CtrlPersonType_cp = [];
		$scope.data.idTemp_cp = [];			
				for(var i=0, length=data.records.length;i<length;i++){	
		$scope.data.DocRefID_AccountReport_cp.push(data.records[i].DocRefID_AccountReport);
		$scope.data.DocRefID_ControllingPerson_cp.push(data.records[i].DocRefID_ControllingPerson);
		$scope.data.ResCountryCode_cp.push(data.records[i].ResCountryCode);
		$scope.data.Nationality_cp.push(data.records[i].Nationality);
		$scope.data.BirthInfo_BirthDate_cp.push(data.records[i].BirthInfo_BirthDate);
		$scope.data.BirthInfo_City_cp.push(data.records[i].BirthInfo_City);
		$scope.data.BirthInfo_CountryCode_cp.push(data.records[i].BirthInfo_CountryCode);
		$scope.data.CtrlPersonType_cp.push(data.records[i].CtrlPersonType);
		$scope.data.idTemp_cp.push(data.records[i].id);	
				}
				//console.log($scope.data.DocRefID_AccountReport_TIN_individual);
					$scope.gridOptions.data = data.records;
					console.log($scope.data.DocRefID_ControllingPerson_cp);
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
		//console.log($scope.data.Timestamp);
		//console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	$scope.hapus = function(Item){
		var data = { 'crud' : '3','id': Item, 'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/hapusControllingPerson.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);
		$scope.data.DocRefID_AccountReport_cp = [];
		$scope.data.DocRefID_ControllingPerson_cp = [];
		$scope.data.ResCountryCode_cp = [];
		$scope.data.Nationality_cp = [];
		$scope.data.BirthInfo_BirthDate_cp = [];
		$scope.data.BirthInfo_City_cp = [];
		$scope.data.BirthInfo_CountryCode_cp = [];
		$scope.data.CtrlPersonType_cp = [];
		$scope.data.idTemp_cp = [];			
				for(var i=0, length=data.records.length;i<length;i++){	
		$scope.data.DocRefID_AccountReport_cp.push(data.records[i].DocRefID_AccountReport);
		$scope.data.DocRefID_ControllingPerson_cp.push(data.records[i].DocRefID_ControllingPerson);
		$scope.data.ResCountryCode_cp.push(data.records[i].ResCountryCode);
		$scope.data.Nationality_cp.push(data.records[i].Nationality);
		$scope.data.BirthInfo_BirthDate_cp.push(data.records[i].BirthInfo_BirthDate);
		$scope.data.BirthInfo_City_cp.push(data.records[i].BirthInfo_City);
		$scope.data.BirthInfo_CountryCode_cp.push(data.records[i].BirthInfo_CountryCode);
		$scope.data.CtrlPersonType_cp.push(data.records[i].CtrlPersonType);
		$scope.data.idTemp_cp.push(data.records[i].id);	
				}			
				$scope.getListData();

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
		
		
		
	}	
	
    $scope.add = function () {
        $state.go('ControllingPersonForm', { data: { crud: '1' } });
    }	
	
    $scope.edit = function (Item) {
		//alert(Item.id);
		//return false;
        $state.go('ControllingPersonForm', { 
												data: {crud: '2'
														, id: Item.id 
														,DocRefID_AccountReport : Item.DocRefID_AccountReport
														,DocRefID_ControllingPerson: Item.DocRefID_ControllingPerson
														,ResCountryCode: Item.ResCountryCode
														,Nationality: Item.Nationality
														,BirthInfo_BirthDate: Item.BirthInfo_BirthDate
														,BirthInfo_City: Item.BirthInfo_City
														,BirthInfo_CountryCode: Item.BirthInfo_CountryCode
														,CtrlPersonType: Item.CtrlPersonType
												
												
												
												} });
		
	
		}		
	$scope.init = function(){
		$scope.getListData();
	}
	$scope.init();
	 
}]); 
