app.controller('20AccountReportPageCtrl', ['$state', '$scope', '$http', '$timeout', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants,$stateParams) {

    $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 
	 
	 
	 
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ReportingFI', field: 'DocRefID_ReportingFI' },
            { name: 'DocTypeIndic_AccountReport', field: 'DocTypeIndic_AccountReport' },
			{ name: 'DocRefID_AccountReport', field: 'DocRefID_AccountReport' },
			{ name: 'CorrMessageRefID_AccountReport', field: 'CorrMessageRefID_AccountReport' },
			{ name: 'CorrDocRefID_AccountReport', field: 'CorrDocRefID_AccountReport' },
			{ name: 'AccountNumber', field: 'AccountNumber' },
			{ name: 'AccountNumberType', field: 'AccountNumberType' },
			{ name: 'UndocumentedAccount', field: 'UndocumentedAccount' },
			{ name: 'ClosedAccount', field: 'ClosedAccount' },
			{ name: 'DormantAccount', field: 'DormantAccount' },
			{ name: 'IsIndividual', field: 'IsIndividual' },
			{ name: 'AcctHolderType', field: 'AcctHolderType' },
			{ name: 'ResCountryCode', field: 'ResCountryCode' },
			{ name: 'Nationality', field: 'Nationality' },
			{ name: 'CurrencyCodeAccountBalance', field: 'CurrencyCodeAccountBalance' },
			{ name: 'AccountBalance', field: 'AccountBalance' },
			{ name: 'BirthInfo_BirthDate', field: 'BirthInfo_BirthDate' },
			{ name: 'BirthInfo_City', field: 'BirthInfo_City' },
			{ name: 'BirthInfo_CountryCode', field: 'BirthInfo_CountryCode' },

        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
		if($scope.data.IsIndividual == 'false'){
			alert('123');
			return false;
		}
			$http({
            method: "POST",
            url: webservicesUrl + "/getAccountReport.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {

			//	console.log($scope.data.DocRefID_AccountReport_TIN_individual);
					$scope.gridOptions.data = data.records;
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	
	
	$scope.refresh = function(){
		$scope.getListData();
		
		//console.log($scope.data.Timestamp);
		//console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	
    $scope.edit = function (data) {
        $state.go('AccountReportForm', { data: { npwp_no: data.npwp_no } });
    }	
	
	//$scope.init = function(){
	//	$scope.getListData();
	//}
	//$scope.init();
	
	
	/*
				$scope.data.idTempAccRepAccRep = [];
				$scope.data.cus_name = [];
				$scope.data.DocTypeIndic_AccountReport = [];
				$scope.data.DocRefID_AccountReport = [];
				$scope.data.CorrMessageRefID_AccountReport = [];
				$scope.data.CorrDocRefID_AccountReport = [];
				$scope.data.AccountNumber = [];
				$scope.data.AccountNumberType = [];
				$scope.data.UndocumentedAccount = [];
				$scope.data.ClosedAccount = [];
				$scope.data.DormantAccount = [];
				$scope.data.IsIndividual = [];
				$scope.data.AcctHolderType = [];
				$scope.data.ResCountryCode = [];
				$scope.data.Nationality = [];
				$scope.data.CurrencyCodeAccountBalance = [];
				$scope.data.AccountBalance = [];
				$scope.data.BirthInfo_BirthDate = [];
				$scope.data.BirthInfo_City = [];
				$scope.data.BirthInfo_CountryCode = [];
				$scope.data.BirthInfo_CountryCode = [];	
	
	
	*/
	
	
	
	 
}]); 
