app.controller('25AccRepAddressPageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

  $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 

	 $scope.id = [];
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_AccountReport', field: 'DocRefID_AccountReport',width:200 }, 
            { name: 'legalAddressType', field: 'legalAddressType',width:200 },
			{ name: 'CountryCode', field: 'CountryCode',width:200 },
			{ name: 'AddressFree', field: 'AddressFree',width:200 },
			{ name: 'Street', field: 'Street',width:200 },
			{ name: 'BuildingIdentifier', field: 'BuildingIdentifier',width:200 },
			{ name: 'SuiteIdentifier', field: 'SuiteIdentifier',width:200 },
			{ name: 'FloorIdentifier', field: 'FloorIdentifier',width:200 },
			{ name: 'DistrictName', field: 'DistrictName',width:200 },
			{ name: 'POB', field: 'POB',width:200 },
			{ name: 'PostCode', field: 'PostCode',width:200 },
			{ name: 'City', field: 'City',width:200 },
			{ name: 'CountrySubentity', field: 'CountrySubentity',width:200 },
            {
                name: 'Action', field: 'npwp_no',width:200,
                cellTemplate: '<button type="button" ng-click="grid.appScope.edit(row.entity)" class="btn btn-primary btn-sm" "> Edit </button> <button type="button" ng-click="grid.appScope.hapus(row.entity.id)" class="btn btn-primary btn-sm" "> Hapus </button>'
            }
        ],
        data: []
    }; 
	
	
	
	$scope.localData = [];
	$scope.getListData = function(){
	//	if($scope.data.IsIndividual == 'true' || $scope.data.IsIndividual == '' ){
	//		return false;
	//	}
			$http({
            method: "POST",
            url: webservicesUrl + "/getAccRepAddress.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				
					 /*
		$scope.data.DocRefID_AccountReport_AccRepAddress =[];
		$scope.data.legalAddressType_AccRepAddress =[];
		$scope.data.CountryCode_AccRepAddress =[];
		$scope.data.AddressFree_AccRepAddress =[];
		$scope.data.Street_AccRepAddress =[];
		$scope.data.BuildingIdentifier_AccRepAddress =[];
		$scope.data.SuiteIdentifier_AccRepAddress =[];
		$scope.data.FloorIdentifier_AccRepAddress =[];
		$scope.data.DistrictName_AccRepAddress =[];
		$scope.data.POB_AccRepAddress =[];
		$scope.data.PostCode_AccRepAddress =[];
		$scope.data.City_AccRepAddress =[];
		$scope.data.CountrySubentity_AccRepAddress =[];	 
	 
	 */
		$scope.data.DocRefID_AccountReport_AccRepAddress =[];
		$scope.data.legalAddressType_AccRepAddress =[];
		$scope.data.CountryCode_AccRepAddress =[];
		$scope.data.AddressFree_AccRepAddress =[];
		$scope.data.idTemp_AccRepAddress = [];
				for(var i=0, length=data.records.length;i<length;i++){	
					$scope.data.idTemp_AccRepAddress.push(data.records[i].id);

		$scope.data.DocRefID_AccountReport_AccRepAddress.push(data.records[i].DocRefID_AccountReport);
		$scope.data.legalAddressType_AccRepAddress.push(data.records[i].legalAddressType);
		$scope.data.CountryCode_AccRepAddress.push(data.records[i].CountryCode);
		$scope.data.AddressFree_AccRepAddress.push(data.records[i].AddressFree);

					
				}
				//console.log($scope.data.DocRefID_AccountReport_TIN_individual);
					$scope.gridOptions.data = data.records;
					console.log($scope.gridOptions.data);
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		$scope.getListData();
		
		//console.log($scope.data.Timestamp);
		//console.log($scope.gridOptions.data.Timestamp);
		
		
		
	}
	$scope.hapus = function(Item){
		var data = { 'crud' : '3','id': Item, 'dataGlobal': $scope.data};
	  			$http({
            method: "POST",
            url: webservicesUrl + "/hapusAccRepAddress.php",
            data: { 'data': data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {
				//console.log(data);

		$scope.data.DocRefID_AccountReport_AccRepAddress =[];
		$scope.data.legalAddressType_AccRepAddress =[];
		$scope.data.CountryCode_AccRepAddress =[];
		$scope.data.AddressFree_AccRepAddress =[];
		$scope.data.idTemp_AccRepAddress = [];
				for(var i=0, length=data.records.length;i<length;i++){	
					$scope.data.idTemp_AccRepAddress.push(data.records[i].id);

		$scope.data.DocRefID_AccountReport_AccRepAddress.push(data.records[i].DocRefID_AccountReport);
		$scope.data.legalAddressType_AccRepAddress.push(data.records[i].legalAddressType);
		$scope.data.CountryCode_AccRepAddress.push(data.records[i].CountryCode);
		$scope.data.AddressFree_AccRepAddress.push(data.records[i].AddressFree);

					
				}				
				$scope.getListData();

            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });		
		
		
		
	}	
	
    $scope.add = function () {
        $state.go('AccRepAddressForm', { data: { crud: '1' } });
    }	
	
    $scope.edit = function (Item) {
		//alert(Item.id);
		//return false;
        $state.go('AccRepAddressForm', { data: {crud: '2',DocRefID_AccountReport : Item.DocRefID_AccountReport, id: Item.id, legalAddressType:Item.legalAddressType, CountryCode:Item.CountryCode, AddressFree : Item.AddressFree  } });
		
	
		}		
	$scope.init = function(){
		$scope.getListData();
	}
	$scope.init();
	 
}]); 
