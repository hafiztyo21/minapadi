app.controller('31CtrlgPersonTINPageCtrl', ['$state', '$scope', '$http', '$timeout', 'uiGridConstants', '$stateParams', function ($state, $scope, $http, $timeout, uiGridConstants, $stateParams) {

      $scope.gridIsLoading = false;
    $scope.getHeight = function(){
	return window.innerHeight - 180;
     }
	 
	 /*
		DocRefID_ControllingPerson_cpt : '',
		TIN_cpt : '',
		IssuedBy_cpt : '',
		TINType_cpt : '',	 
	 
	 
	 */
	 
	 
	  $scope.gridOptions = {
        enableSorting: true,
	showColumnFooter: true,
	enableColumnResizing: true,
	onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
	rowTemplate:'templates/rowTemplate.html',
        columnDefs: [
            { name: 'DocRefID_ControllingPerson', field: 'DocRefID_ControllingPerson_cpt' },
            { name: 'TIN', field: 'TIN_cpt'},
			{ name: 'IssuedBy', field: 'IssuedBy_cpt' },
			{ name: 'TINType', field: 'TINType_cpt' }
        ],
        data: []
    }; 
	
$scope.datas = {
	
	records : '',
	
	
};	

$scope.datas.DocRefID_ControllingPerson_cpt = [];
$scope.datas.TIN_cpt = [];
$scope.datas.IssuedBy_cpt = [];
$scope.datas.TINType_cpt = [];
	
	$scope.localData = [];
	$scope.getListData = function(){

			$http({
            method: "POST",
            url: webservicesUrl + "/get_31.php",
            data: { 'data': $scope.data },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
//			console.log(response.data);
            //data = CRYPTO.decrypt(response.data.data);
			var data = response.data;
			console.log(data);
            if (data.status.toLowerCase() == 'ok') {

			//	console.log($scope.data.DocRefID_AccountReport_TIN_individual);
					$scope.gridOptions.data = data.records;
            } else {
                alert(data.message);
            }
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });			

	console.log($scope.localData);	
	$scope.gridIsLoading = false;
	$scope.gridOptions.data = $scope.localData;
	console.log($scope.gridOptions.data);
	}
	
	$scope.refresh = function(){
		//alert('123');
		console.log($scope.data);
		$scope.getListData();
		
		
		//console.log($scope.data.Timestamp);
		console.log($scope.gridOptions.data);
		console.log($scope.data);
		
		
	}
	

	
	$scope.init = function(){

		if($scope.data.IsIndividual == 'true'){
			return false;
			
			
		}
		$scope.data.DocRefID_ControllingPerson_cpt = $scope.data.DocRefID_ControllingPerson_cp[0];
		$scope.data.DocRefID_ControllingPerson_cpa = $scope.data.DocRefID_ControllingPerson_cpt;
		$scope.data.DocRefID_ControllingPerson_cpn = $scope.data.DocRefID_ControllingPerson_cpt;
		$scope.getListData();
	}
	$scope.init();
	
}]); 
