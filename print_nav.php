<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
$data = new globalFunction;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>
</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="1500" cellpadding="0" cellspacing="0"><tr>
<td >
<ul class="activeVid" id="videoList">


<?php if($data->auth_boolean(1710,$_SESSION['pk_id'])){ ?>
<li> <a href="cash_repo.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Cash</a></li>

<?php } ?>
<?php if($data->auth_boolean(1711,$_SESSION['pk_id'])){ ?>
<li> <a href="report_print.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Report print</a></li>
<?php } ?>

<?php if($data->auth_boolean(1712,$_SESSION['pk_id'])){ ?>
<li> <a href="report_obligasi_edit.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Report Bonds Edit Print</a></li>
<?php } ?>

<?php if($data->auth_boolean(1713,$_SESSION['pk_id'])){ ?>
<li> <a href="report_pn_edit.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Report PN Edit Print</a></li>
<?php } ?>
<?php if($data->auth_boolean(1714,$_SESSION['pk_id'])){ ?>
<li> <a href="report_deposit_edit.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Report Deposito Edit Print</a></li>
<?php } ?>

<?php if($data->auth_boolean(1715,$_SESSION['pk_id'])){ ?>
<li> <a href="report_saham_edit.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Report Saham Edit Print</a></li>
<?php } ?>

<?php if($data->auth_boolean(1715,$_SESSION['pk_id'])){ ?>
<li> <a href="report_print_total_aktiva.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Rata-rata Total Aktiva</a></li>
<?php } ?>

<?php if($data->auth_boolean(1716,$_SESSION['pk_id'])){ ?>
<li> <a href="report_limit_share.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Report Limit Share</a></li>
<?php } ?>

<?php if($data->auth_boolean(1717,$_SESSION['pk_id'])){ ?>
<li> <a href="report_dealer.php?cek=1" target="contentTabFrame" onClick="activeLink(this);">Report Dealer</a></li>
<?php } ?>

<?php if($data->auth_boolean(1718,$_SESSION['pk_id'])){ ?>
<li> <a href="deposit_hist.php" target="contentTabFrame" onClick="activeLink(this);">Deposit History</a></li>
<?php } ?>

<?php if($data->auth_boolean(1719,$_SESSION['pk_id'])){ ?>
<li> <a href="obligasi_hist.php" target="contentTabFrame" onClick="activeLink(this);">Obligasi History</a></li>
<?php } ?>

<?php if($data->auth_boolean(1716,$_SESSION['pk_id'])){ ?>
<li> <a href="ddot_hist.php" target="contentTabFrame" onClick="activeLink(this);">DTTOT</a></li>
<?php } ?>

</ul>
</td>
</table>
</body>
</html>
