<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('profile_static_data_add.html');

$imCode = '';
$imName = '';
$ownership = 'P';
$wmi = '0';
$address1 = '';

$address2 = '';
$city = '';
$postalCode = '';
$phone = '';
$fax = '';

$email = '';
$npwp = '';
$contact1 = '';
$contact2 = '';

$errorArr = array('','','','',''
    ,'','','','',''
    ,'','','','');
$otherError='';

if ($_POST['btnSave']=='save'){
	$flag = true;
	try {
		/*if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}*/
 		
        $imCode = trim(htmlentities($_POST['imCode']));
        $imName = trim(htmlentities($_POST['imName']));
        $ownership = trim(htmlentities($_POST['ownership']));
        $wmi = trim(htmlentities($_POST['wmi']));
        $address1 = trim(htmlentities($_POST['address1']));

        $address2 = trim(htmlentities($_POST['address2']));
        $city = trim(htmlentities($_POST['city']));
        $postalCode = trim(htmlentities($_POST['postalCode']));
        $phone = trim(htmlentities($_POST['phone']));
        $fax = trim(htmlentities($_POST['fax']));

        $email = trim(htmlentities($_POST['email']));
        $npwp = trim(htmlentities($_POST['npwp']));
        $contact1 = trim(htmlentities($_POST['contact1']));
        $contact2 = trim(htmlentities($_POST['contact2']));
		
		$gotError = false;
		/*if($fundCode==''){
			$errorArr[0] = "Fund Code must be filled";
			$gotError = true;
		}
		if(strlen($fundCode)<16){
			$errorArr[0] = "Invalid Fund Code";
			$gotError = true;
		}*/
		
        
		if (!$gotError){
			
			$query = "INSERT INTO tbl_kr_im_profile_static_data (
					im_code,
					im_name,
					ownership,
					wmi,
                    address1,
                    address2,
                    city,
                    postal_code,
                    phone,
                    fax,
                    email,
                    npwp,
                    contact1,
                    contact2,
					created_date,
					created_by,
					last_updated_date,
					last_updated_by,
					is_deleted
				)VALUES(
                    '$imCode',
                    '$imName',
                    '$ownership',
                    '$wmi',
                    '$address1',
                    '$address2',
                    '$city',
                    '$postalCode',
                    '$phone',
                    '$fax',
                    '$email',
                    '$npwp',
                    '$contact1',
                    '$contact2',
                    now(),
                    '".$_SESSION['pk_id']."',
                    now(),
                    '".$_SESSION['pk_id']."',
                    '0')";

			if (!$data->inpQueryReturnBool($query)){
				//throw new Exception($data->err_report('s02'));
				$otherError = "Error : ".mysql_error();
			}else{
				echo "<script>alert('Save Success');window.location='profile_static_data.php';</script>";
				exit(0);
			}
		}
		
		
		/*if (!$data->inpQueryReturnBool($sql_saham2)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}*/
		
	}catch (Exception $e1){
		#$data->rollbackTrans();
		//mysql_query("ROLLBACK");
		$err_msg = $e1->getMessage();
		//echo "<script>alert('err');</script>";
		$otherError = 'Error : '.$err_msg;
	}
}

$tittle = "ADD - IM PROFILE STATIC DATA";
$dataRows = array (
	    'TEXT' => array(
            'IM Code','IM Name','Ownership', 'WMI', 'Alamat 1'
            , 'Alamat 2', 'Kota', 'Kode Pos', 'Telepon', 'Fax'
            , 'Email', 'NPWP', 'Kontak 1', 'Kontak 2'),
  	    'DOT'  => array (':',':',':'),
	    'FIELD' => array (
            "<input type=text size='50' maxlength=5 name=imCode value='$imCode'>",
		    "<input type=text size='50' name=imName value='$imName'>",
            $data->cb_ownership('ownership',$ownership),
            "<input type=number step=1 name=wmi value='$wmi'>",
            "<input type=text size='50' name=address1 value='$address1'>",
            "<input type=text size='50' name=address2 value='$address2'>",
            "<input type=text size='50' name=city value='$city'>",
            "<input type=text size='50' name=postalCode value='$postalCode'>",
            "<input type=text size='50' name=phone value='$phone'>",
            "<input type=text size='50' name=fax value='$fax'>",
            "<input type=text size='50' name=email value='$email'>",
            "<input type=text size='50' name=npwp value='$npwp'>",
            "<input type=text size='50' name=contact1 value='$contact1'>",
            "<input type=text size='50' name=contact2 value='$contact2'>",
	    ),
		'ERROR' => $errorArr
);
$button = array ('SUBMIT' => "<input type=submit name=btnSave value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='profile_static_data.php';\">");

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

//$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
if($otherError!='')
	$otherError = '<div style="color:#ff3333;background-color:#ff9999; padding:4px; margin-top:5px; margin-bottom:5px;">'.$otherError.'</div>';

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVar('page','othererror',$otherError);
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');



?>