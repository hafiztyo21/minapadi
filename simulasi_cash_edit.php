<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('cash_add.html');
$tablename = 'tbl_kr_cash';


if ($_POST['btn_save']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
		$txt_allocation 	= trim(htmlentities($_POST['txt_allocation']));
		$txt_tgl = trim(htmlentities($_POST['txt_tgl']));
 		$txt_kas = trim(htmlentities($_POST['txt_kas']));
		$txt_total_piutang = trim(htmlentities($_POST['txt_total_piutang']));
        $txt_aktiva_lain = trim(htmlentities($_POST['txt_aktiva_lain']));
        $txt_total_kewajiban = trim(htmlentities($_POST['txt_total_kewajiban']));
        $txt_jumlah_up = trim(htmlentities($_POST['txt_jumlah_up']));
		$txt_nab = trim(htmlentities($_POST['txt_nab']));
		if($txt_kas==''){
			echo "<script>alert('Kas Giro is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_piutang==''){
			echo "<script>alert('Total Piutang is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
        if($txt_aktiva_lain==''){
			echo "<script>alert('Aktiva lain is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_kewajiban==''){
			echo "<script>alert('Total kewajiban is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_jumlah_up==''){
			echo "<script>alert('Jumlah up is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$sql = "INSERT INTO tbl_kr_cash (
			kas_giro,
			total_piutang,
			aktiva_lain,
			total_kewajiban,
			jumlah_up,
			nab,
			allocation,
			create_dt
		)VALUES(
			'$txt_kas',
			'$txt_total_piutang',
			'$txt_aktiva_lain',
			'$txt_total_kewajiban',
			'$txt_jumlah_up',
			'$txt_nab',
			'$txt_allocation',
			'$txt_tgl'
			)";
			//print_r($sql);
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='cash_repo.php';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}


if ($_GET['add']==1){
	$dataRows = array (
		'TEXT' =>  array(
						'Allocation',
						'Tanggal Cash Di input',
						'Kas pada Giro BII RD Keraton (Rp.)',
						'Total Piutang (Rp.)',
						'Aktiva Lain-lain (Rp.)',
						'Total Kewajiban (Rp.)',
						'Jumlah Up Beredar (Unit)',
						'Nab per Saham/ Unit Penyertaan Hari sebelumnya',
						'Redemption Hari ini'
						),
		'DOT'  => array (':',':',':',':',':',':',':'),
		'FIELD' => array (
		$data->cb_allocation('txt_allocation',$_POST[txt_allocation]," "),
		$data->datePicker('txt_tgl', $rows[txt_tgl],''),
		"<input type=text name=txt_kas value='".$_POST[txt_kas]."'>",
		"<input type=text name=txt_total_piutang value='".$_POST[txt_total_piutang]."'>",
		"<input type=text name=txt_aktiva_lain value='".$_POST[txt_aktiva_lain]."'>",
		"<input type=text name=txt_total_kewajiban value='".$_POST[txt_total_kewajiban]."'>",
		"<input type=text name=txt_jumlah_up value='".$_POST[txt_jumlah_up]."'>",
		"<input type=text name=txt_nab value='".$_POST[txt_nab]."'>",
		"<input type=text name=txt_red value='".$_POST[txt_red]."'>",
	)
		);
	$tittle = "CASH ADD";
    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.location='cash_repo.php';\">"
	);
}


if ($_POST['btn_save_edit']=='save'){
	$flag = true;
	try {
		if (!mysql_query("BEGIN"))  {
			throw new Exception($data->err_report('beginTrans_failed'));
		}
			$txt_tgl = trim(htmlentities($_POST['txt_tgl']));
		$txt_kas = trim(htmlentities($_POST['txt_kas']));
		$txt_total_piutang = trim(htmlentities($_POST['txt_total_piutang']));
        $txt_aktiva_lain = trim(htmlentities($_POST['txt_aktiva_lain']));
        $txt_total_kewajiban = trim(htmlentities($_POST['txt_total_kewajiban']));
        $txt_jumlah_up = trim(htmlentities($_POST['txt_jumlah_up']));
		 $txt_nab = trim(htmlentities($_POST['txt_nab']));
		  $txt_redemption = trim(htmlentities($_POST['txt_red']));
		
		if($txt_kas==''){
			echo "<script>alert('Kas Giro is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_piutang==''){
			echo "<script>alert('Total Piutang is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
        if($txt_aktiva_lain==''){
			echo "<script>alert('Aktiva lain is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_total_kewajiban==''){
			echo "<script>alert('Total kewajiban is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_jumlah_up==''){
			echo "<script>alert('Jumlah up is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		if($txt_redemption==''){
			echo "<script>alert('Redemption up is Empty!');</script>";
			throw new Exception($data->err_report('s02'));
		}
		$sql = "UPDATE tbl_kr_simulasi_cash SET
			kas_giro = '".$txt_kas."',
			total_piutang = '".$txt_total_piutang."',
			aktiva_lain = '".$txt_aktiva_lain."',
			total_kewajiban = '".$txt_total_kewajiban."',
			jumlah_up = '".$txt_jumlah_up."',
			nab = '".$txt_nab."',
			redemption = '".$txt_redemption."'
			 WHERE allocation = '".$_GET[id]."'";
		if (!$data->inpQueryReturnBool($sql)){
			throw new Exception($data->err_report('s02'));
		}
		if (!mysql_query("COMMIT")) {
			throw new Exception($data->err_report('commitTrans_failed'));
		}
		echo "<script>alert('".$data->err_report('s01')."');window.location='simulasi_report.php?cek=1';</script>";
	}catch (Exception $e1){
		#$data->rollbackTrans();
		mysql_query("ROLLBACK");
		$flag = false;
		$err_msg = $e1->getMessage();
		echo "<script>alert('err');</script>";
	}
}

if ($_GET['edit']==1){
	$rows = $data->get_row("select * from tbl_kr_simulasi_cash where allocation = '".$_GET[id]."'");
	$A			=	$rows['allocation'];
	$rowo		= $data->get_row("select allocation as A from tbl_kr_allocation where pk_id = '".$_GET[id]."'");
	$allocation2 = $rowo['A'];
	$tittle = "CASH EDIT ".$allocation2."";
	$dataRows = array (
			'TEXT' =>  array(
						'Allocation',
						'Kas pada Giro BII RD Keraton (Rp.)',
						'Total Piutang (Rp.)',
						'Aktiva Lain-lain (Rp.)',
						'Total Kewajiban (Rp.)',
						'Jumlah Up Beredar (Rp.)',
						'Nab per Saham/ Unit Penyertaan Hari sebelumnya',
						'Redemption Hari ini'
						),
		'DOT'  => array (':',':',':',':',':',':'),
	'FIELD' => array (
	"&nbsp;&nbsp;".$allocation2."",
	"<input type=text name=txt_kas value='".$rows[kas_giro]."'>",
	"<input type=text name=txt_total_piutang value='".$rows[total_piutang]."'>",
	"<input type=text name=txt_aktiva_lain value='".$rows[aktiva_lain]."'>",
	"<input type=text name=txt_total_kewajiban value='".$rows[total_kewajiban]."'>",
	"<input type=text name=txt_jumlah_up value='".$rows[jumlah_up]."'>",
		"<input type=text name=txt_nab value='".$rows[nab]."'>",
		"<input type=text name=txt_red value='".$rows[redemption]."'>"
			)
	);
    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.location='cash_repo.php';\">"
	);
}



$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>